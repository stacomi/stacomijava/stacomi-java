/*
 **********************************************************************
 *
 * Nom fichier :        MethodeObtention.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */


package migration;



/**
 * M�thode d'obtention
 * @author Samuel Gaudey 
 */
public class MethodeObtention {

  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////


    private static final String[] methodesLot = {"MESURE", "CALCULE", "EXPERT", "PONCTUEL"} ;
    private static final String[] methodesCar = {"MESURE", "CALCULE", "EXPERT"} ;



  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    

    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////

    /**
     * Retourne les methodes d'obtention d'un lot de poissons
     * @return les methodes
     */
        public static String[] getMethodesLot() {    
            return MethodeObtention.methodesLot ;
            
        } // end getMethodesLot   
        
        
    /**
     * Retourne les methodes d'obtention d'une caracteristque de lot
     * @return les methodes
     */
        public static String[] getMethodesCaracteristiques() {    
            return MethodeObtention.methodesCar ;
            
        } // end getMethodesCaracteristiques           
        
 } // end MethodeObtention



