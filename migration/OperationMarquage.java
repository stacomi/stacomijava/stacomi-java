/*
 **********************************************************************
 *
 * Nom fichier :        OperationMarquage.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import commun.*;

/**
 * Operation de marquage
 * @author Samuel Gaudey
 */
public class OperationMarquage implements IBaseDonnees {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	private String reference;

	private String commentaires;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une operation de marquage avec uniquement une reference 
	 * @param _reference la reference de l'operation de marquage
	 */
	public OperationMarquage(String _reference) {
		this(_reference, null);
	} // end OperationMarquage

	/**
	 * Construit une operation de marquage avec tous ses attributs
	 * @param _reference r�f�rence de l'op�ration de marquage
	 * @param _commentaires commentaires sur l'op�ration
	 */
	public OperationMarquage(String _reference, String _commentaires) {
		this.setReference(_reference);
		this.setCommentaires(_commentaires);
	} // end OperationMarquage   

	///////////////////////////////////////
	// interfaces
	///////////////////////////////////////

	/**
	 * Insertion d'une op�ration de marquage dans la BDD
	 */
	public void insertObjet() throws SQLException, ClassNotFoundException
	{
		String table = OperationMarquage.getNomTable();
		ArrayList<String> nomAttributs = OperationMarquage.getNomAttributs();
		ArrayList<Object> valeurAttributs = this.getValeurAttributs();
		
		ConnexionBD.getInstance().executeInsert(table, nomAttributs, valeurAttributs);
	}

	/**
	 * Mise � jour d'une op�ration de marquage dans la BDD
	 */
	public void majObjet() throws SQLException, ClassNotFoundException
	{
		String table = OperationMarquage.getNomTable();
		ArrayList<String> nomAttributs = OperationMarquage.getNomAttributs();
		ArrayList<Object> valeurAttributs = this.getValeurAttributs();
		
		String[] nomID = OperationMarquage.getNomID();
		Object[] valeurID = this.getValeurID();
		
		ConnexionBD.getInstance().executeUpdate(table, nomID, valeurID, nomAttributs, valeurAttributs);
	}
	
	/**
	 * Mise � jour d'une op�ration de marquage dans la BDD
	 * @param old ancienne op�ration
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public void majObjet(OperationMarquage old) throws SQLException, ClassNotFoundException
	{
		String table = OperationMarquage.getNomTable();
		ArrayList<String> nomAttributs = OperationMarquage.getNomAttributs();
		ArrayList<Object> valeurAttributs = this.getValeurAttributs();
		
		String[] nomID = OperationMarquage.getNomID();
		Object[] valeurID = old.getValeurID();
		
		ConnexionBD.getInstance().executeUpdate(table, nomID, valeurID, nomAttributs, valeurAttributs);
	}

	/**
	 * Suppression d'une op�ration de marquage dans la BDD
	 */
	public void effaceObjet() throws SQLException, ClassNotFoundException
	{
		String table = OperationMarquage.getNomTable();
		String[] nomID = OperationMarquage.getNomID();
		Object[] valeurID = this.getValeurID();
		
		ConnexionBD.getInstance().executeDelete(table, nomID, valeurID);
	}

	/**
	 * V�rification des attributs d'une op�ration de marquage
	 */
	public boolean verifAttributs() throws DataFormatException
	{
		// v�rification de la r�f�rence
		if(!Verification.isText(this.reference, 1, true))
			throw new DataFormatException(Erreur.S16000);

		// v�rification du commentaire
		if(!Verification.isText(this.commentaires, false))
			throw new DataFormatException(Erreur.S16001);

		return true;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Retourne la reference de l'operation
	 * @return la reference
	 */
	public String getReference() {
		return this.reference;
	} // end getReference        

	/**
	 * Retourne les commentaires sur l'operation
	 * @return les commentaires
	 */
	public String getCommentaires() {
		return this.commentaires;
	} // end getCommentaires        

	/**
	 * Initialise la reference de l'operation
	 * @param _reference la reference
	 */
	public void setReference(String _reference) {
		this.reference = _reference;
	} // end setReference     

	/**
	 * Initialise les commentaires sur l'operation
	 * @param _commentaires les commentaires
	 */
	public void setCommentaires(String _commentaires) {
		this.commentaires = _commentaires;
	}// end setCommentaires   

	/**
	 * Retourne le nom de la table correspondante
	 * @return le nom de la table correspondante
	 */
	public static String getNomTable() {
		return "t_operationmarquage_omq";
	}
	
	/**
	 * Retourne le nom de la cl� primaire
	 * @return le nom de la cl� primaire
	 */
	public static String[] getNomID()
	{
		String[] res = new String[1];
		res[0] = "omq_reference";
		return res;
	}
	
	/**
	 * Retourne la valeur de la cl� primaire
	 * @return la valeur de la cl� primaire
	 */
	public Object[] getValeurID() {
		Object[] res = new Object[1];
		res[0] = this.getReference();
		return res;
	} // end getValeurID

	/**
	 * Acc�s aux noms des colonnes
	 * @return les noms des colonnes
	 */
	public static ArrayList<String> getNomAttributs()
	{
		ArrayList<String> res = new ArrayList<String>();
		res.add("omq_reference");
		res.add("omq_commentaires");
		res.add("omq_org_code");

		return res;
	}

	/**
	 * Acc�s aux valeurs des attributs
	 * @return les valeurs des attributs
	 */
	public ArrayList<Object> getValeurAttributs() 
	{
		ArrayList<Object> res = new ArrayList<Object>();
		res.add(this.reference);
		res.add(this.commentaires);
		return res;
	}

	/**
	 * Retourne les attributs d'une op�ration sous forme textuelle
	 * @return ret un string
	 */
	public String toString()
	{
		return this.reference;
	}
} // end OperationMarquage
