/*
 **********************************************************************
 *
 * Nom fichier :        PathologieConstatee.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;
import migration.referenciel.RefImportancePathologie;
import migration.referenciel.RefLocalisation;
import migration.referenciel.RefPathologie;

/**
 * Pathologie constatee sur un lot
 * 
 * @author C�dric Briand
 */
public class PathologieConstatee implements IBaseDonnees {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	private Lot lot;
	private RefPathologie pathologie;
	private RefLocalisation localisation;
	private RefImportancePathologie importance; // ajout 2016 cedric

	private String commentaires;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une pathologie constatee avec uniquement un lot, une pathologie
	 * et une localisation
	 * 
	 * @param _lot
	 *            le lot sur lequel est constatee la pathologie
	 * @param _pathologie
	 *            la pathologie constatee sur le lot
	 * @param _localisation
	 *            la localisation de la pathologie
	 */
	public PathologieConstatee(Lot _lot, RefPathologie _pathologie, RefLocalisation _localisation) {
		this(_lot, _pathologie, _localisation, null, null);
	} // end PathologieConstatee

	/**
	 * Construit une pathologie constatee avec tous ses attributs
	 * 
	 * @param _lot
	 *            le lot sur lequel est constatee la pathologie
	 * @param _pathologie
	 *            la pathologie constatee sur le lot
	 * @param _localisation
	 *            la localisation de la pathologie
	 * @param _commentaires
	 *            les commentaires sur la pathologie constatee sur le lot
	 */
	public PathologieConstatee(Lot _lot, RefPathologie _pathologie, RefLocalisation _localisation,
			RefImportancePathologie _imp, String _commentaires) {

		this.setLot(_lot);
		this.setPathologie(_pathologie);
		this.setLocalisation(_localisation);
		this.setImportance(_imp);
		this.setCommentaires(_commentaires);

	} // end PathologieConstatee

	///////////////////////////////////////
	// interfaces
	///////////////////////////////////////

	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		ArrayList nomAttributs = PathologieConstatee.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();

		ConnexionBD.getInstance().executeInsert(table, nomAttributs, valAttributs);
	}

	public void majObjet() {
	}

	/**
	 * @param old
	 *            ancienne pathologie constat�e
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void majObjet(PathologieConstatee old) throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		ArrayList nomAttributs = PathologieConstatee.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		String[] nomClefPrimaire = PathologieConstatee.getNomID();
		Object[] valeurClefPrimaire = old.getValeurID();

		ConnexionBD.getInstance().executeUpdate(table, nomClefPrimaire, valeurClefPrimaire, nomAttributs, valAttributs);
	}

	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		String[] nomClefPrimaire = PathologieConstatee.getNomID();
		Object[] valeurClefPrimaire = this.getValeurID();
		ConnexionBD.getInstance().executeDelete(table, nomClefPrimaire, valeurClefPrimaire);
	}

	public boolean verifAttributs() throws DataFormatException {
		// verification de l'identifiant du lot
		if (!Verification.isInteger(this.getLot().getIdentifiant(), 0, true))
			throw new DataFormatException(Erreur.S14001);

		// verification de la pathologie
		if (!Verification.isText(this.getPathologie().getCode(), 1, 4, true))
			throw new DataFormatException(Erreur.S14002);

		// verification de la localisation de la pathologie
		if (!Verification.isText(this.getLocalisation().getCode(), 1, 4, true))
			throw new DataFormatException(Erreur.S14003);

		// verification de l'importance
		if (!Verification.isText(this.getImportance().getCode(), 1, 1, true))
			throw new DataFormatException(Erreur.S14005);

		// verification du commentaire
		if (!Verification.isText(this.getCommentaires(), 0, false)) {
			throw new DataFormatException(Erreur.S14004);
		}

		return true;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Retourne le lot sur lequel est constatee la pathologie
	 * 
	 * @return le lot
	 */
	public Lot getLot() {
		return this.lot;
	} // end getLot

	/**
	 * Retourne la pathologie constatee
	 * 
	 * @return la pathologie
	 */
	public RefPathologie getPathologie() {
		return this.pathologie;
	} // end getPathologie

	/**
	 * Retourne la localisation de la pathologie
	 * 
	 * @return la localisation
	 */
	public RefLocalisation getLocalisation() {
		return this.localisation;
	} // end getLocalisation

	/**
	 * Retourne les commentaires sur la pathologie constatee
	 * 
	 * @return les commentaires
	 */
	public String getCommentaires() {
		return this.commentaires;
	} // end getCommentaires

	/**
	 * Initialise le lot sur lequel est constatee la pathologie
	 * 
	 * @param _lot
	 *            le lot
	 */
	public void setLot(Lot _lot) {
		this.lot = _lot;
	} // end setLot

	/**
	 * Initialise la pathologie constatee
	 * 
	 * @param _pathologie
	 *            la pathologie
	 */
	public void setPathologie(RefPathologie _pathologie) {
		this.pathologie = _pathologie;
	} // end setPathologie

	/**
	 * Initialise la localisation de la pathologie
	 * 
	 * @param _localisation
	 *            la localisation
	 */
	public void setLocalisation(RefLocalisation _localisation) {
		this.localisation = _localisation;
	} // end setLocalisation

	/**
	 * @return importance
	 */
	public RefImportancePathologie getImportance() {
		return importance;
	}

	/**
	 * @param code
	 *            d'importance de la pathologie
	 */
	public void setImportance(RefImportancePathologie importance) {
		this.importance = importance;
	}

	/**
	 * Initialise les commentaires sur la pathologie constatee
	 * 
	 * @param _commentaires
	 *            les commentaires
	 */
	public void setCommentaires(String _commentaires) {
		this.commentaires = _commentaires;
	}

	/**
	 * Acces aux valeurs des attributs qui devront etre �crits dans la table
	 * 
	 * @return les valeurs des attributs qui devront etre �crits dans la table
	 */
	private ArrayList getValeurAttributs() {
		ArrayList<Object> res = new ArrayList<Object>();
		res.add(this.getLot().getIdentifiant());
		res.add(this.getPathologie().getCode());
		res.add(this.getLocalisation().getCode());
		res.add(this.getImportance().getCode());
		res.add(this.getCommentaires());
		return res;
	}

	/**
	 * Acces aux noms des attributs qui devront etre �crits dans la table
	 * 
	 * @return les des attributs qui devront etre �crits dans la table
	 */
	private static ArrayList<String> getNomAttributs() {
		ArrayList<String> res = new ArrayList<String>();
		res.add("pco_lot_identifiant");
		res.add("pco_pat_code");
		res.add("pco_loc_code");
		res.add("pco_imp_code");
		res.add("pco_commentaires");
		res.add("pco_org_code");

		return res;
	}

	/**
	 * Acces au nom de la table dans laquelle sont stock�es les pathologies
	 * constat�es
	 * 
	 * @return le nom de la table des pathologies constat�es
	 */
	private String getNomTable() {
		String res = "tj_pathologieconstatee_pco";
		return res;
	}

	/**
	 * Retourne les attributs du lot sous forme textuelle
	 * 
	 * @return l'affichage de la pathologie
	 */
	public String toString() {
		String res = "";
		String sep = "-";
		res += this.getPathologie().getCode() + sep;
		res += this.getLocalisation().getCode() + sep;
		res += this.getImportance().getCode();

		return res;
	}

	/**
	 * Acces a la cl� primaire d'une pathologie constat�e
	 * 
	 * @return la cl� primaire d'une pathologie constat�e
	 */
	private Object[] getValeurID() {
		Object[] res = new Object[3];
		res[0] = this.getLot().getIdentifiant();
		res[1] = this.getPathologie().getCode();
		res[2] = this.getLocalisation().getCode();
		return res;
	}

	/**
	 * Acc�s � la valeur de la cl� primaire de la pathologie constat�e
	 * 
	 * @return la valeur de la cl� primaire de la pathologie constat�e
	 */
	private static String[] getNomID() {
		String[] res = new String[3];
		res[0] = "pco_lot_identifiant";
		res[1] = "pco_pat_code";
		res[2] = "pco_loc_code";
		return res;
	}

	// end setCommentaires

} // end PathologieConstatee
