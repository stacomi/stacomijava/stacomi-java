/*
 **********************************************************************
 *
 * Nom fichier :        LigneVideo.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            C�dric Briand
 * Date de creation :   Septembre 2008
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */
package migration;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;
import commun.Erreur;

/**
 * Cette classe sert notamment à vérifier l'int�grit� des champs
 * du fichier Vidéo par appel d'un constructeur
 * @author Cédric Briand 
 */
public class LigneVideo {

	// /////////////////////////////////////
	// attributes
	// /////////////////////////////////////
	private String fichierVideo;

	private Integer ligne;

	private Integer effectif;

	private String taxonVideo;

	private Integer sens;

	private Date date;

	private String distance;

	private Float tailleEnPixel;

	private String remarque;

	// /////////////////////////////////////
	// constructors
	// /////////////////////////////////////
	/**
	 * Initialisation d'une ligne vid�o
	 * @param fichierVideo le fichier vid�o
	 * @param ligne la ligne vid�o
	 * @param effectif l'effectif
	 * @param taxonVideo le taxon vid�o
	 * @param sens le sens de migration
	 * @param date la date
	 * @param distance la distance
	 * @param tailleEnPixel la taille en pixel
	 * @param remarque remarque
	 */
	public LigneVideo(String fichierVideo, Integer ligne, Integer effectif,
			String taxonVideo, Integer sens, Date date, String distance,
			Float tailleEnPixel, String remarque) {
		this.setFichierVideo(fichierVideo);
		this.setLigne(ligne);
		this.setTaxonVideo(taxonVideo);
		this.setEffectif(effectif);
		this.setSens(sens);
		this.setDate(date);
		this.setDistance(distance);
		this.setTailleEnPixel(tailleEnPixel);
		this.setRemarque(remarque);
	}

	/**
	 * Initialise une ligne (mais pas ses attributs). L'initialisation des
	 * attributs se fera par appel de la fonction "lireLigne()"
	 */
	public LigneVideo() {
	}

	// /////////////////////////////////////
	// operations
	// /////////////////////////////////////

	/**
	 * Lis une ligne d'un fichier SSM contenant les donn�es de la capture vid�o
	 * @param line la ligne avec ses informations correspondantes
	 * @throws IllegalStateException
	 * @throws InputMismatchException 
	 */
	public void lireLigne(String line) throws IllegalStateException,
			InputMismatchException {

		Scanner scan_line = null;
		String strdate = null;
		String heure = null;
		String effectif = null;
		try {
			// lecture des champs de la ligne
			scan_line = new Scanner(line);
			scan_line.useDelimiter(" ");// normalement whitespace
			if (scan_line.hasNext()) {
				this.setFichierVideo(scan_line.next());
			} else
				throw new InputMismatchException(Erreur.V1001);// String
																// attendu
			if (scan_line.hasNextInt()) {
				this.setLigne(Integer.valueOf(scan_line.nextInt()));
			} else
				throw new InputMismatchException(Erreur.V1002);// String
																// attendu
			if (scan_line.hasNext()) {
				this.setTaxonVideo(scan_line.next());
			} else
				throw new InputMismatchException(Erreur.V1001);
			if (scan_line.hasNext()) {
				effectif = scan_line.next();
				if (effectif.contains("+")) {
					this.setSens(Integer.valueOf(1));
					effectif = effectif.replace("+", "");
					int a = Integer.parseInt(effectif);
					this.setEffectif(a);
				} else if (effectif.contains("-")) {
					this.setSens(Integer.valueOf(-1));
					effectif.replace("-", "");
					int a = Integer.parseInt(effectif);
					this.setEffectif(a);
				} else if (effectif.contentEquals("0")) {
					this.setEffectif(Integer.valueOf(0));
				} else {// bug certains effectifs sont mis a la main et n'ont pas le "+"
					this.setSens(Integer.valueOf(1));
					int a = Integer.parseInt(effectif);
					this.setEffectif(a);
				}

			} else
				throw new InputMismatchException(Erreur.V1001);
			if (scan_line.hasNext()) {
				strdate = scan_line.next();
			} else
				throw new InputMismatchException(Erreur.V1001);
			if (scan_line.hasNext()) {
				heure = scan_line.next();
			} else
				throw new InputMismatchException(Erreur.V1001);
			if (scan_line.hasNext()) {
				scan_line.next();
			} else
				throw new InputMismatchException(Erreur.V1001);
			SimpleDateFormat formatter;
			formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = formatter.parse(strdate +" "+ heure);
			this.setDate(date);
			if (scan_line.hasNext()) {
				this.setDistance(scan_line.next());
			} else
				throw new InputMismatchException(Erreur.V1001);
			if (scan_line.hasNextFloat()) {
				this.setTailleEnPixel(Float.valueOf(scan_line.nextFloat()));
			} else
				throw new InputMismatchException(Erreur.V1003);
			if(scan_line.hasNext()){
				String com = getCommentaires(scan_line);
				this.setRemarque(com);
			} else
				throw new InputMismatchException(Erreur.V1001);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			scan_line.close();
		}

	}

	private String getCommentaires(Scanner scan_line) {
		String res = "";
		while (scan_line.hasNext()) {
			String coms = scan_line.next();
			coms = coms.replaceAll("\"", "");
			if(coms.length()!=0)
				res += coms + " ";
		}
		return res;
	}

	/**
	 * @return la date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date la date
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return la distance � la vitre
	 */
	public String getDistance() {
		return distance;
	}

	/**
	 * @param distance la distance � la vitre
	 */
	public void setDistance(String distance) {
		this.distance = distance;
	}

	/**
	 * @return l'effectif
	 */
	public Integer getEffectif() {
		return effectif;
	}

	/**
	 * @param effectif l'effectif
	 */
	public void setEffectif(Integer effectif) {
		this.effectif = effectif;
	}

	/**
	 * @return le fichier vid�o
	 */
	public String getFichierVideo() {
		return fichierVideo;
	}

	/**
	 * @param fichierVideo
	 */
	public void setFichierVideo(String fichierVideo) {
		this.fichierVideo = fichierVideo;
	}

	/**
	 * @return la remarque sur le taxon concern�
	 */
	public String getRemarque() {
		return remarque;
	}

	/**
	 * @param remarque remarque sur le taxon concern�
	 */
	public void setRemarque(String remarque) {
		this.remarque = remarque;
	}

	/**
	 * @return le sens de migration
	 */
	public Integer getSens() {
		return sens;
	}

	/**
	 * @param sens le sens de migration
	 */
	public void setSens(Integer sens) {
		this.sens = sens;
	}

	/**
	 * @return la taille en pixel
	 */
	public Float getTailleEnPixel() {
		return tailleEnPixel;
	}

	/**
	 * @param tailleEnPixel la taille en pixel
	 */
	public void setTailleEnPixel(Float tailleEnPixel) {
		this.tailleEnPixel = tailleEnPixel;
	}

	/**
	 * @return le taxon vid�o
	 */
	public String getTaxonVideo() {
		return taxonVideo;
	}

	/**
	 * @param taxonVideo taxon vid�o
	 */
	public void setTaxonVideo(String taxonVideo) {
		this.taxonVideo = taxonVideo;
	}

	/**
	 * @return la ligne du fichier
	 */
	public Integer getLigne() {
		return ligne;
	}

	/**
	 * @param ligne la ligne du fichier
	 */
	public void setLigne(Integer ligne) {
		this.ligne = ligne;
	}
}
