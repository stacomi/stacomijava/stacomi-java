/*
 **********************************************************************
 *
 * Nom fichier :        Prelevement.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             S�bastien Laigre
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   29 mai 2009
 * Compatibilite :      Java6/windows XP/ postgreSQL 8.3
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */
package migration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;
import migration.referenciel.RefLocalisation;
import migration.referenciel.RefPrelevement;

/**
 * Pr�l�vement
 * 
 * @author S�bastien Laigre
 */
public class Prelevement implements IBaseDonnees {
	/** Le type de pr�l�vement */
	private RefPrelevement typePrelevement;

	/** Le lot */
	private Lot lot;

	/** Le code du pr�l�vement */
	private String code;

	/** L'op�rateur */
	private String operateur;

	/** La localisation */
	private RefLocalisation localisation;

	/** Les commentaires */
	private String commentaires;

	/**
	 * Construction d'un pr�l�vement avec seulement le type de pr�l�vement et le
	 * lot
	 * 
	 * @param typePrelevement
	 *            : le type de pr�l�vement
	 * @param lot
	 *            : le lot de rattachement
	 */
	public Prelevement(RefPrelevement typePrelevement, Lot lot) {
		this(typePrelevement, lot, null, null, null, null);
	}

	/**
	 * Construction d'un pr�l�vement avec tous les attributs
	 * 
	 * @param typePrelevement
	 *            : le type de pr�l�vement
	 * @param lot
	 *            : le lot
	 * @param operateur
	 *            : l'op�rateur
	 * @param code
	 *            : le code du pr�l�vement
	 * @param localisation
	 *            : la localisation
	 * @param commentaires
	 *            : les commentaires
	 */
	public Prelevement(RefPrelevement typePrelevement, Lot lot, String operateur, String code,
			RefLocalisation localisation, String commentaires) {
		this.setTypePrelevement(typePrelevement);
		this.setLot(lot);
		this.setOperateur(operateur);
		this.setCode(code);
		this.setLocalisation(localisation);
		this.setCommentaires(commentaires);
	}

	/**
	 * Acc�s au type de pr�l�vement
	 * 
	 * @return le type de pr�l�vement
	 */
	public RefPrelevement getTypePrelevement() {
		return this.typePrelevement;
	}

	/**
	 * Modifie le type de pr�l�vement
	 * 
	 * @param typePrelevement
	 *            : le nouveau type de pr�l�vement
	 */
	public void setTypePrelevement(RefPrelevement typePrelevement) {
		this.typePrelevement = typePrelevement;
	}

	/**
	 * Acc�s au lot
	 * 
	 * @return le lot
	 */
	public Lot getLot() {
		return this.lot;
	}

	/**
	 * Modifie le lot
	 * 
	 * @param lot
	 *            : le nouveau lot
	 */
	public void setLot(Lot lot) {
		this.lot = lot;
	}

	/**
	 * Acc�s � l'op�rateur
	 * 
	 * @return l'op�rateur
	 */
	public String getOperateur() {
		return this.operateur;
	}

	/**
	 * Modification de l'op�rateur
	 * 
	 * @param operateur
	 *            : le nouvel op�rateur
	 */
	public void setOperateur(String operateur) {
		this.operateur = operateur;
	}

	/**
	 * Acc�s au code du pr�l�vement
	 * 
	 * @return le code du pr�l�vement
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * Modifie le code du pr�l�vement
	 * 
	 * @param code
	 *            : le nouveau code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Acc�s � la localisation
	 * 
	 * @return la localisation
	 */
	public RefLocalisation getLocalisation() {
		return this.localisation;
	}

	/**
	 * Modifie la localisation
	 * 
	 * @param localisation
	 *            : la nouvelle localisation
	 */
	public void setLocalisation(RefLocalisation localisation) {
		this.localisation = localisation;
	}

	/**
	 * Acc�s aux commentaires
	 * 
	 * @return les commentaires
	 */
	public String getCommentaires() {
		return this.commentaires;
	}

	/**
	 * Modifie les commentaires
	 * 
	 * @param commentaires
	 *            : les nouveaux commentaires
	 */
	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}

	/**
	 * Insertion d'un pr�l�vement dans la base
	 */
	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = Prelevement.getNomTable();
		ArrayList nomAttributsDC = Prelevement.getNomAttributs();
		ArrayList valAttributsDC = this.getValeurAttributs();
		ConnexionBD.getInstance().executeInsert(table, nomAttributsDC, valAttributsDC);
		System.out.println("Prelevement : insertObjet()");
	}

	public void majObjet() throws SQLException, ClassNotFoundException {
	}

	/**
	 * Mise a jour d'un pr�l�vement dans la base
	 * 
	 * @param old
	 *            ancien pr�l�vement
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void majObjet(Prelevement old) throws SQLException, ClassNotFoundException {
		String table = Prelevement.getNomTable();
		ArrayList nomAttributsDC = Prelevement.getNomAttributs();
		ArrayList valAttributsDC = this.getValeurAttributs();

		String[] nomID = Prelevement.getNomID();
		Object[] valeurID = old.getValeurID();
		ConnexionBD.getInstance().executeUpdate(table, nomID, valeurID, nomAttributsDC, valAttributsDC);
		System.out.println("Prelevement : majObjet()");
	}

	/**
	 * Suppression d'un pr�l�vement dans la base
	 */
	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = Prelevement.getNomTable();
		String[] nomID = Prelevement.getNomID();
		Object[] valeurID = this.getValeurID();

		ConnexionBD.getInstance().executeDelete(table, nomID, valeurID);
		System.out.println("Prelevement : effaceObjet()");
	}

	/**
	 * Verification de la valeur des attributs
	 */
	public boolean verifAttributs() throws DataFormatException {
		// type de pr�l�vement
		if (!Verification.isText(this.getTypePrelevement().getTypePrelevement(), 1, 15, true))
			throw new DataFormatException(Erreur.S23001);

		if (!Verification.isInteger(this.getLot().getIdentifiant(), true))
			throw new DataFormatException(Erreur.S23002);

		if (!Verification.isText(this.getCode(), 1, 12, true))
			throw new DataFormatException(Erreur.S23004);

		if (!Verification.isText(this.getOperateur(), 0, 35, false))
			throw new DataFormatException(Erreur.S23004);

		if (!Verification.isText(this.getLocalisation().getCode(), 1, 4, false))
			throw new DataFormatException(Erreur.S23005);

		if (!Verification.isText(this.getCommentaires(), 0, false))
			throw new DataFormatException(Erreur.S23006);

		return true;
	}

	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return le nom de la table
	 */
	public static String getNomTable() {
		return "tj_prelevementlot_prl";
	}

	/**
	 * Retourne le nom de l'identifiant caract�re de la table
	 * 
	 * @return un vecteur de string de longueur 1 contenant le nom de
	 *         l'identifiant de la table
	 */
	public static String[] getNomID() {
		String[] ret = new String[2];
		ret[0] = "prl_pre_typeprelevement";
		ret[1] = "prl_lot_identifiant";
		return ret;
	} // end getNomID

	/**
	 * Retourne la valeur de l'indenfiant string de la classe
	 * 
	 * @return un vecteur d'objet de longueur 1 contenant le code
	 */
	public Object[] getValeurID() {
		Object[] ret = new Object[2];
		ret[0] = this.getTypePrelevement();
		ret[1] = this.getLot().getIdentifiant();
		return ret;
	} // end getValeurID

	/**
	 * Retourne le nom des attributs de la table correspondant a la classe
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		// impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
		// est inconnu
		ArrayList<String> ret = new ArrayList<String>();
		ret.add("prl_pre_typeprelevement");
		ret.add("prl_lot_identifiant");
		ret.add("prl_code");
		ret.add("prl_operateur");
		ret.add("prl_loc_code");
		ret.add("prl_commentaires");
		ret.add("prl_org_code");
		return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de la classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret = new ArrayList<Object>();
		ret.add(this.getTypePrelevement().getTypePrelevement());
		ret.add(this.getLot().getIdentifiant());
		ret.add(this.getCode());
		ret.add(this.getOperateur());
		ret.add(this.getLocalisation().getCode());
		ret.add(this.getCommentaires());
		// organisme ajout� lors de l'insertion update par connexion bd
		return ret;
	} // end
		// getValeurAttributs

	/**
	 * Affichage d'un pr�l�vement
	 */
	public String toString() {
		String res = "";
		String sep = "  |  ";

		res += this.getTypePrelevement() + sep;
		res += this.getLot().getIdentifiant() + sep;
		res += this.getCode() + sep;
		res += this.getOperateur() + sep;
		res += this.getLocalisation().getCode() + sep;
		res += this.getCommentaires() + "    ";

		return res;
	}
}
