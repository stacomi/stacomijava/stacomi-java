/*
 **********************************************************************
 *
 * Nom fichier :        ImporterVideo.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             C�dric briand/S�bastien Laigre
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   4 mai 2009
 * Compatibilite :      Java6/Windows XP professionnel/PostgreSQL 7.4
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;
import java.util.zip.DataFormatException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;

import analyse.PasDeTemps;
import commun.ConnexionBD;
import commun.Erreur;
import commun.Lanceur;
import commun.Message;
import commun.RechercheFichier;
import infrastructure.DC;
import infrastructure.DF;
import infrastructure.Ouvrage;
import infrastructure.Station;
import infrastructure.referenciel.ListeDF;
import infrastructure.referenciel.ListeStations;
import infrastructure.referenciel.SousListeDC;
import infrastructure.referenciel.SousListeDF;
import infrastructure.referenciel.SousListeOuvrages;
import migration.Caracteristique;
import migration.LigneVideo;
import migration.Lot;
import migration.Operation;
import migration.TailleVideo;
import migration.Taxonvideo;
import migration.referenciel.ListeTailleVideo;
import migration.referenciel.ListeTaxonVideo;
import migration.referenciel.RefDevenir;
import migration.referenciel.RefParametreQuantitatif;
import migration.referenciel.RefStade;
import migration.referenciel.RefTaxon;
import migration.referenciel.RefTypeQuantite;
import migration.referenciel.SousListeCaracteristiques;
import migration.referenciel.SousListeLots;
import migration.referenciel.SousListeMarquages;
import migration.referenciel.SousListePathologieConstatee;

/**
 * Import dans la base de fichiers vid�os
 * 
 * @author C�dric Briand, S�bastien Laigre
 */
public class ImporterVideo extends JPanel {

	private static final long serialVersionUID = 1L;
	private JPanel top = null;
	private JPanel center = null;
	private JPanel bottom = null;

	private File fichier;

	private ListeTaxonVideo listeTaxonVideo = null;
	private ListeTailleVideo listeTailleVideo = null;
	private ListeStations lesStations;
	private SousListeOuvrages lesOuvrages;
	private SousListeDF lesDF; // @jve:decl-index=0:
	private SousListeDC lesDC; // @jve:decl-index=0:
	private SousListeDF leDFselectionne; // @jve:decl-index=0:

	private String[] lesPas;

	String _organisme = null;
	String _operateur = null;

	private JComboBox jComboBoxStation = null;
	private JComboBox jComboBoxOuvrage = null;
	private JComboBox jComboBoxDF = null;
	private JComboBox jComboBoxDC = null;
	private JComboBox jComboBoxPasDeTemps = null;

	private JLabel Titre = null;
	private JLabel jLabelStation = null;
	private JLabel jLabelOuvrage = null;
	private JLabel jLabelDF = null;
	private JLabel jLabelDC = null;
	private JLabel jLabelOrganisme = null;
	private JLabel resultat = null;
	private JLabel jLabelPasDeTemps = null;
	private JLabel jLabel1 = null;
	private JLabel JLabelOperateur = null;

	private JTextField jTextFieldOrganisme = null;
	private JTextField jTextFieldNomFichier = null;
	private JTextField jTextFieldOperateur = null;

	private JButton importer = null;
	private JButton Annuler = null;
	private JButton jButtonRechercheFichier = null;

	private JProgressBar jProgressBar = null;

	// les num�ros de s�quence des lot et op�rations
	int sequencelot;
	int sequenceOperation;
	Float precision;

	// listes des op�rations, lots et caracat�ristiques des lots
	ArrayList<Operation> ensembleOperations = new ArrayList<Operation>();
	ArrayList<Lot> ensembleLots = new ArrayList<Lot>();
	ArrayList<Caracteristique> ensembleCaracteristique = new ArrayList<Caracteristique>();

	// les op�ration vid�o (lignes du fichier)
	private ArrayList lesOperationsVideo = new ArrayList();

	/**
	 * constructeur
	 */
	public ImporterVideo() {
		super();

		// chargement des stations
		this.chargeListeStations();
		this.listeTaxonVideo = Lanceur.getListeTaxonVideo();
		try {
			// r�cup�ration des valeurs courantes des s�quences pour pouvoir
			// "simuler"
			// leur comportement lors de l'insertion et pouvoir collecter les
			// toutes les
			// informations (� propos des op�rations et des Lots)
			// avant d'�crire dans la base...
			// ATTENTION : ceci suppose que les s�quences soient � jour !
			sequencelot = 1 + ConnexionBD.getInstance().getCurrentKey(Lot.getNomSequences()[0]);
			sequenceOperation = 1 + ConnexionBD.getInstance().getCurrentKey(Operation.getNomSequences()[0]);
		} catch (Exception e) {
			this.resultat.setText(Erreur.I1011);
		}

		initialize();
	}

	/**
	 * Reinitialise les composants du jPanel courant
	 */
	private void reInitComponents() {
		this.removeAll();
		this.chargeListeStations();
		this.initialize();
		this.validate();
		this.repaint();
	}

	/**
	 * This method initializes this
	 */
	private void initialize() {
		BorderLayout borderLayout = new BorderLayout();
		this.setLayout(borderLayout);
		this.setBackground(new java.awt.Color(255, 255, 255));
		this.setPreferredSize(new java.awt.Dimension(600, 400));
		this.add(getTop(), BorderLayout.NORTH);
		this.add(getBottom(), BorderLayout.SOUTH);
		this.add(getCenter(), BorderLayout.CENTER);
	}

	/**
	 * Charge la liste des stations
	 */
	private void chargeListeStations() {
		this.lesStations = new ListeStations();
		try {
			this.lesStations.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1003);
		}
	}

	/**
	 * Charge la sous liste des ouvrages rattach�s � la station
	 */
	private void chargeSousListeOuvrages() {
		Station station; // la station selectionnee

		// Recupere la station selectionne dans le combo
		station = (Station) this.jComboBoxStation.getSelectedItem();

		// Vide la liste des station pour garder uniquement la station
		// selectionnee
		this.lesStations = new ListeStations();
		this.lesStations.put(station.getCode(), station);

		// charge la sous liste des ouvrages
		this.lesOuvrages = new SousListeOuvrages(station);

		try {
			this.lesOuvrages.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1004);
		}

		// Affectation de la sous liste des ouvrages a la station
		station.setLesElementsConstitutifs(this.lesOuvrages);

		// actualiste les combo
		this.jComboBoxOuvrage.setModel(new DefaultComboBoxModel(this.lesOuvrages.toArray()));

		this.jComboBoxDF.setModel(new DefaultComboBoxModel());
		this.jComboBoxDC.setModel(new DefaultComboBoxModel());
		this.jComboBoxOuvrage.setEnabled(true);
		this.jLabelOuvrage.setForeground(SystemColor.textHighlight);
		this.jLabelStation.setForeground(SystemColor.text);
		this.jComboBoxDF.setEnabled(false);
		this.jComboBoxDC.setEnabled(false);
		this.jLabelDF.setForeground(SystemColor.textInactiveText);
		this.jLabelDC.setForeground(SystemColor.textInactiveText);
	}

	/**
	 * Charge la sous liste des DF rattach�s � l'ouvrage
	 */
	private void chargeSousListeDF() {
		Ouvrage ouvrage; // l'ouvrage selectionne

		// Recupere l'ouvrage selectionne dans le combo
		ouvrage = (Ouvrage) this.jComboBoxOuvrage.getSelectedItem();

		// Vide la liste des ouvrages pour garder uniquement celui selectionne
		this.lesOuvrages = new SousListeOuvrages();
		this.lesOuvrages.put(ouvrage.getIdentifiant(), ouvrage);

		// charge la sous liste des df
		this.lesDF = new SousListeDF(ouvrage);

		try {
			this.lesDF.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1005);
		}

		// Affectation de la sous liste des df a l'ouvrage
		ouvrage.setLesElementsConstitutifs(this.lesDF);

		// actualiste les combo
		this.jComboBoxDF.setModel(new DefaultComboBoxModel(this.lesDF.toArray()));
		this.jLabelDF.setForeground(SystemColor.textHighlight);
		this.jLabelOuvrage.setForeground(SystemColor.text);
		this.jComboBoxDC.setModel(new DefaultComboBoxModel());
		this.jComboBoxDF.setEnabled(true);
		this.jComboBoxDC.setEnabled(false);
		this.jLabelDC.setForeground(SystemColor.textInactiveText);

		// Essai de renvoi de la valeur dans le combo
		// C'est dans ouvrage que je dois activer l'affichage
		// charge les le d�tail des autres champs du DF
		DF df; // le DF s�lectionn� dans le combo
		df = (DF) this.jComboBoxDF.getSelectedItem();
		// Vide la liste des df pour garder uniquement celui selectionne

		this.leDFselectionne = new SousListeDF();
		this.leDFselectionne.put(df.getIdentifiant(), df);
		// Charge les caract�ristiques du DF, dont celles r�cup�r�es dans la
		// liste abstaite dispositif
		try {
			this.leDFselectionne.chargeObjets();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1005);
		}
		df = (DF) this.leDFselectionne.elements().nextElement();
		this.jComboBoxDF.setToolTipText(df.getCommentaires());
	}

	/**
	 * Charge la sous liste des DC rattach�s au DF
	 */
	private void chargeSousListeDC() {
		DF df; // le df selectionne
		// Recupere le df selectionne dans le combo
		df = (DF) this.jComboBoxDF.getSelectedItem();
		// Vide la liste des df pour garder uniquement celui selectionne
		this.lesDF = new SousListeDF();
		this.lesDF.put(df.getIdentifiant(), df);
		// Charge les caract�ristiques du DF, dont celles r�cup�r�es dans la
		// liste abstaite dispositif

		ListeDF ldf = new ListeDF();
		ldf.put(df.getCode(), df);

		// chargement des infos
		try {
			ldf.chargeObjets();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1016);
		}
		df = (DF) ldf.get(0);

		this.jComboBoxDF.setToolTipText(df.getCommentaires());

		// charge la sous liste des dc
		this.lesDC = new SousListeDC(df);

		try {
			this.lesDC.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1006);
		}

		// Affectation de la sous liste des dc au df
		df.setLesDC(this.lesDC);

		// actualiste le combo
		this.jComboBoxDC.setModel(new DefaultComboBoxModel(this.lesDC.toArray()));
		this.jComboBoxDC.setEnabled(true);
		this.jLabelDF.setForeground(SystemColor.text);
		this.jLabelDC.setForeground(SystemColor.textHighlight);
	}

	/**
	 * Vide la liste des dc pour ne garder que celui selectionn�. Charge la
	 * liste des pas de temps
	 */
	private void chargePasdeTemps() {
		DC dc; // le dc selectionne

		// Recupere le dc selectionne dans le combo
		dc = (DC) this.jComboBoxDC.getSelectedItem();

		dc.chargeListeTailleVideo();
		listeTailleVideo = dc.getListeTailleVideo();

		// charge la sous liste des pas de temps
		String[] tousLesPas = PasDeTemps.getLibellesPas();

		// on ne conserve que les pas qui sont inf�rieurs ou �gaux � 1h
		this.lesPas = RetirePasSuppA1heure(tousLesPas);

		// Vide la liste des pas de temps pour garder uniquement celui
		// selectionne
		this.jComboBoxPasDeTemps.setModel(new DefaultComboBoxModel(this.lesPas));
		this.lesDC = new SousListeDC();
		this.lesDC.put(dc.getIdentifiant(), dc);

		// Actualise les combos
		this.jLabelDC.setForeground(SystemColor.text);
		this.JLabelOperateur.setForeground(SystemColor.textHighlight);
		this.jLabelOrganisme.setForeground(SystemColor.textHighlight);
		this.jLabelPasDeTemps.setForeground(SystemColor.textHighlight);
		this.jComboBoxPasDeTemps.setEnabled(true);
	}

	/**
	 * Retire les pas qui ont une dur�e sup�rieure � 1h
	 * 
	 * @param touslespas
	 *            la liste des pas disponibles
	 * @return la liste des pas de dur�e inf�rieure ou �gale � 1h
	 */
	private String[] RetirePasSuppA1heure(String[] touslespas) {
		String[] res = new String[6];

		for (int i = 0; i < 6; i++)
			res[i] = touslespas[i];

		return res;
	}

	/**
	 * juste une modif graphique
	 * 
	 */
	private void videPasdeTemps() {
		this.JLabelOperateur.setForeground(SystemColor.text);
		this.jLabelOrganisme.setForeground(SystemColor.text);
		this.jLabelPasDeTemps.setForeground(SystemColor.text);
	}

	/**
	 * Convertit la liste de taxonVideo en un vecteur de string contenant les
	 * code des taxonvideo (utilis� pour y effectuer une recherche d'indice)
	 * 
	 * @param lrt
	 *            : la liste de taxonVideo
	 * @return un vecteur de string contenant les code des taxonvideo
	 */
	private Vector<String> ConvertToVector(ListeTaxonVideo lrt) {
		Vector<String> v = new Vector<String>();
		for (int i = 0; i < lrt.size(); i++) {
			v.add(((Taxonvideo) lrt.get(i)).gettaxonvideo());
		}
		return v;
	}

	/**
	 * Initialise les vecteurs d'op�rations de lots et de caractristiques.
	 */
	private void calculeOperationsEtLots() {
		// on parcourt l'ensemble des op�rations video pour les mettre dans un
		// vecteur qui permettra de les �crire...
		for (int i = 0; i < this.lesOperationsVideo.size(); i++) {
			HashMap operationVideo = (HashMap) lesOperationsVideo.get(i);
			Operation operation = null;
			try {
				// l'op�ration vid�o
				operation = enregistreOperation(operationVideo, (Date) operationVideo.get("datedebutpas"),
						(Date) operationVideo.get("datefinpas"));

				operationVideo.remove("commentaires");
				operationVideo.remove("datedebutpas");
				operationVideo.remove("datefinpas");

				// si la cr�ation de l'op�ration s'est d�roul�e correctement
				try {
					// enregistrement des lots et sous-lots
					enregistreLots(operationVideo, operation);
				} catch (Exception e) {
					this.resultat.setText(e.getMessage());
				}

				// on ajoute l'op�ration au vecteur d'op�rations
				ensembleOperations.add(operation);
			} catch (Exception e) {
				this.resultat.setText(e.getMessage());
			}
		}
	}

	/**
	 * Supprime les caract�ristiques, lots et op�rations. Est appel�e si
	 * plantage durant enregistrement... Remarque : la suppression s'effectue
	 * dans le sens inverse de l'insertion pour �viter les cl� �trang�re
	 * pointant sur une case nulle !
	 * 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void supprime() throws ClassNotFoundException, SQLException {
		// suppression des caracteristiques
		for (int i = 0; i < this.ensembleCaracteristique.size(); i++) {
			Caracteristique caracteristique = this.ensembleCaracteristique.get(i);
			caracteristique.effaceObjet();
		}

		// suppression des lots
		for (int i = 0; i < this.ensembleLots.size(); i++) {
			Lot lot = this.ensembleLots.get(i);
			lot.effaceObjet();
		}

		// suppression des op�rations
		for (int i = 0; i < this.ensembleOperations.size(); i++) {
			Operation operation = this.ensembleOperations.get(i);
			operation.effaceObjet();
		}
	}

	/**
	 * Ajoute une op�ration dans la liste des op�rations (liste qui servira lors
	 * de l'�criture) Elle v�rifie les attributs de l'op�ration avant
	 * l'insertion
	 * 
	 * @param operationVideo
	 * @param date_debut
	 * @param date_fin
	 * @return operation ou null si plantage
	 */
	private Operation enregistreOperation(HashMap operationVideo, Date date_debut, Date date_fin)
			throws Exception, DataFormatException {

		// Resultat affiche a l'utilisateur lors d'une erreur
		String result = null;

		// Objet a creer
		Operation operation;

		// Attributs de l'objet a creer
		Integer identifiant = sequenceOperation;
		sequenceOperation++;
		DC dc = null;
		String commentaires = null;
		SousListeLots lesLots = null;

		// Initialisation des attributs d'apres les saisies de l'utilisateur et
		// verification que les valeurs sont bien du type attendu

		// le DC
		try {
			dc = (DC) this.jComboBoxDC.getSelectedItem();
		} catch (Exception e) {
			result = Erreur.S11001 + this.jLabelDC.getText();
		}

		// date de debut
		try {
			Calendar caldateDebut = Calendar.getInstance();
			caldateDebut.setTimeInMillis(date_debut.getTime());
			date_debut = caldateDebut.getTime();
			// date_debut.setHours(date_debut.getHours()+1) ; // BUG : oblige de
			// mettre + 1 ???
			// Ce n'est pas un bug pour 09:00 renvoit Mon Nov 12 08:00:00
			// GMT+01:00 2007 parce qu'on est
			// en GMT +01:00
		} catch (Exception e) {
			result = Erreur.S12008;
		}

		// date de fin
		try {
			Calendar caldateFin = Calendar.getInstance();
			caldateFin.setTimeInMillis(date_fin.getTime());
			date_fin = caldateFin.getTime();
		} catch (Exception e) {
			result = Erreur.S12009;
		}

		try {
			commentaires = (String) operationVideo.get("commentaires");
		} catch (Exception e) {
			result = Erreur.S12005;
		}

		// Si aucune erreur jusque la, les champs sont soit null soit du bon
		// type, mais les contraintes de domaine ne sont pas forcement verifiees
		if (result == null) {
			try {
				// Creation de l'objet et v�rification de sa validite
				operation = new Operation(dc, identifiant, date_debut, date_fin, _organisme, _operateur, commentaires,
						lesLots);

				// Ajout d'une liste vide de lots
				operation.setLesLots(new SousListeLots(operation));
				operation.verifAttributs();
				return operation;
			} catch (DataFormatException e) {
				result = e.getMessage(); // erreur
				throw new DataFormatException(result);
			}
		} else {
			// Affichage du resultat sans reinitialisation
			throw new Exception(result);
		}
	}

	/**
	 * Ajoute un lot et ses sous-lots dans la liste des lots (liste qui servira
	 * � l'�criture). Elle v�rifie les attributs des lots/sous-lots avant
	 * l'insertion
	 * 
	 * @param operationVideo
	 *            l'op�ration courante
	 * @param operationRattachement
	 *            l'op�ration � laquelle est rattach�e le lot
	 */
	private void enregistreLots(HashMap operationVideo, Operation operationRattachement)
			throws Exception, DataFormatException {
		// Objet a creer
		Lot lotAEnregistrer;

		// Resultat affiche a l'utilisateur
		String result = null;

		// Attributs de l'objet a creer
		Integer identifiant = null;
		RefTaxon taxon = null;
		RefStade stadeDeveloppement = null;
		Float effectif = null;
		Float valeurQuantite = null; // null pour vid�ocomptage
		RefTypeQuantite typeQuantite = null;// null pour vid�ocomptage
		String methodeObtention = "MESURE"; // MESURE
		Lot lotParent = null; // lot pere
		RefDevenir devenir = new RefDevenir("1");// 1
		String commentaires = null;// commentaire du lot
		SousListePathologieConstatee lesPathologies = null; // null
		SousListeMarquages lesMarquages = null;// null
		SousListeCaracteristiques lesCaracteristiques = null;// null pour le
																// lot, tailles
																// pour les
																// sous-lots

		// Initialisation des attributs d'apres les saisies de l'utilisateur et
		// verification que
		// les valeurs sont bien du type attendu
		if (operationRattachement == null) // normalement il y a une operation
			result = Erreur.S12006;

		// on r�cup�re la s�quence du lot pour avoir
		// l'identifiant qu'aura le lot apr�s insertion
		identifiant = sequencelot;
		// mise a jour de la s�quence
		sequencelot++;

		Vector<String> vecteurTaxonVideo = ConvertToVector(listeTaxonVideo);

		// parcours de l'ensemble des taxons pour l'op�ration courante
		Object[] taxonsDeLoperationCourante = operationVideo.keySet().toArray();
		for (int i = 0; i < taxonsDeLoperationCourante.length; i++) {
			// r�cup�ration du taxon
			String taxonVideoCode = (String) taxonsDeLoperationCourante[i];
			int indiceTaxonVideoCode = vecteurTaxonVideo.indexOf(taxonVideoCode);
			if (indiceTaxonVideoCode == -1) // si le taxon n'est pas dans le
											// vecteur => erreur
				result = Erreur.B1024;

			// le taxonvideo
			Taxonvideo taxonvideo = (Taxonvideo) listeTaxonVideo.get(indiceTaxonVideoCode);
			HashMap lotvideo = (HashMap) operationVideo.get(taxonVideoCode);
			effectif = new Float(((Integer) lotvideo.get("effectif")).floatValue());

			// normalement le taxon a un de RefTaxon/RefStadeDeveloppement (
			// v�rifi� + haut (dans FichierVideoScanner) )
			try {
				taxon = taxonvideo.getrefTaxon();
			} catch (Exception e) {
				result = Erreur.S12007;
			}

			try {
				stadeDeveloppement = taxonvideo.getRefStade();
			} catch (Exception e) {
				result = Erreur.S12014;
			}

			try {
				commentaires = (String) lotvideo.get("remarque");
			} catch (Exception e) {
				result = Erreur.S12005;
			}

			// Si aucune erreur jusque la, les champs sont soit null soit du bon
			// type,
			// mais les contraintes de domaine ne sont pas forcement verifiees
			if (result == null) {
				try {
					// Creation de l'objet et verification de sa validite
					lotAEnregistrer = new Lot(identifiant, operationRattachement, taxon, stadeDeveloppement, effectif,
							valeurQuantite, typeQuantite, methodeObtention, lotParent/* =null */, devenir, commentaires,
							lesPathologies/* null */, lesMarquages/* null */, lesCaracteristiques/* null */);

					lotAEnregistrer.verifAttributs();

					// ajout au vecteur r�sultat
					ensembleLots.add(lotAEnregistrer);

					// parcours de tous les sous-lots du lot courant pour les
					// collecter
					ArrayList sousLotArrayList = (ArrayList) lotvideo.get("souslot");
					if (sousLotArrayList != null) {
						for (int j = 0; j < sousLotArrayList.size(); j++) {
							// le sous-lot courant
							HashMap sousLotCourant = (HashMap) sousLotArrayList.get(j);

							// on lis les informations du sous-lot
							Lot sousLot = getSouslot(sousLotCourant, lotAEnregistrer);
							ensembleLots.add(sousLot);
						}
					}
				} catch (DataFormatException e) {
					throw new DataFormatException(e.getMessage()); // erreur
				}
			} else {
				// Affichage du resultat sans reinitialisation
				throw new Exception(result);
			}
		}
	}

	/**
	 * Retourne le sous-lot rattach� � un lot p�re
	 * 
	 * @param sousLotCourant
	 *            : le sous-lot � analyser, stock� dans la HashMap (donn�e
	 *            brute)
	 * @param lotPere
	 *            : le lot p�re
	 * @return le sous-lot dans sa forme correcte (sous forme d'objet de type
	 *         'Lot')
	 * @throws Exception
	 */
	private Lot getSouslot(HashMap sousLotCourant, Lot lotPere) throws DataFormatException {
		// 1 sous-lot peut avoir :
		// - seulement 1 remarque
		// - 1 remarque et 1 taille
		Lot lot = null;

		// r�cup�ration de la taille
		Float taille = (Float) sousLotCourant.get("taille");

		// r�cup�ration de la remarque du Souslot
		String remarque = (String) sousLotCourant.get("remarque");

		// l'identifiant du sous-lot est un nouveau (pas de lien avec
		// l'identifiant du lot pere)
		Integer identifiant = sequencelot;
		sequencelot++;
		RefTaxon refTaxonLotPere = lotPere.getTaxon();// idem lot pere
		RefStade refStadeLotPere = lotPere.getStadeDeveloppement();// idem lot
																	// pere
		Float effectifLotPere = 1.0f;// toujours 1 dans le cas de l'import vid�o
		RefDevenir devenir = new RefDevenir("1");// relach� au droit de la
													// station
		RefTypeQuantite rtq = null;// null
		Float qte = null;// null
		String methodeObtention = "MESURE";
		// l'op�ration courante
		Operation operationCourante = lotPere.getOperation();

		// information de taille
		String parametreTaille = "C001";// C001 = longueur vid�o
		RefParametreQuantitatif paramTaille = new RefParametreQuantitatif(parametreTaille);

		// cr�ation du sous-lot
		lot = new Lot(identifiant, operationCourante, refTaxonLotPere, refStadeLotPere, effectifLotPere, qte, rtq,
				methodeObtention, lotPere, devenir, remarque, null, null, null);

		// v�rification
		lot.verifAttributs();

		if (taille != null) {
			// la caract�ristique
			Caracteristique caracteristique = new Caracteristique(lot, paramTaille, "MESURE", taille, null, precision,
					null);
			SousListeCaracteristiques slc = new SousListeCaracteristiques(lotPere);
			String cleprimaire = caracteristique.getLot().getIdentifiant() + caracteristique.getParametre().getCode();
			slc.put(cleprimaire, caracteristique);

			caracteristique.verifAttributs();
			lot.setLesCaracteristiques(slc);
			ensembleCaracteristique.add(caracteristique);
		}

		return lot;
	}

	//////////////////////////////////////////
	/////// ActionEvents
	//////////////////////////////////////////

	/**
	 * Ouverture de la fenetre pour chercher le fichier
	 */
	private void jButton_RechercheFichierActionPerformed(ActionEvent e) {
		String[] extension = { "ssm" };
		RechercheFichier rf = new RechercheFichier(extension);
		fichier = rf.getFile();
	}

	/**
	 * Initialise l'op�rateur en fonction du texte contenu dans le champs de
	 * texte
	 * 
	 * @throws Exception
	 *             champ invalide
	 */
	private void setOperateur() throws Exception {
		_operateur = this.jTextFieldOperateur.getText();

		if (_operateur.length() == 0)
			throw new Exception(Erreur.S12004 + this.JLabelOperateur.getText());
	}

	/**
	 * Initialise l'organisme en fonction du texte contenu dans le champs de
	 * texte
	 * 
	 * @throws Exception
	 *             champ invalide
	 */
	private void setOrganisme() throws Exception {
		_organisme = this.jTextFieldOrganisme.getText();

		if (_organisme.length() == 0)
			throw new Exception(Erreur.S12003 + this.jLabelOrganisme.getText());
	}

	/**
	 * Action effectu�e lorsque l'on clique sur le bouton 'importer'
	 * 
	 * @param e
	 *            le clic
	 */
	private void importerActionPerformed(ActionEvent e) {
		// r�initialisation des listes qui serviront � �crire dans la base
		// permet de faire l'import des plusieurs fichiers vid�o sans avoir �
		// recharger la page
		this.ensembleCaracteristique = new ArrayList<Caracteristique>();
		this.ensembleLots = new ArrayList<Lot>();
		this.ensembleOperations = new ArrayList<Operation>();

		final String pasDeTemps = (String) jComboBoxPasDeTemps.getSelectedItem();

		// 2�me thread pour parcourir le fichier vid�o
		new SwingWorker<ArrayList, Object>() {
			// les op�rations vid�o (lignes du fichier)
			private ArrayList<HashMap<String, Serializable>> lesOpeVideo = new ArrayList<HashMap<String, Serializable>>();

			/**
			 * Acces a la liste des op�rations vid�o
			 * 
			 * @return la liste des op�rations vid�o
			 */
			public ArrayList getLesOperationVideo() {
				return this.lesOpeVideo;
			}

			/**
			 * Lecture du fichier � l'aide d'un scanner, les �l�ments du fichier
			 * vid�o sont stock�s dans un objet operationVideo, une arborescence
			 * cr�e par la m�thode ajout. Les p�riodes sont divis�es en pas de
			 * temps r�gulier, chacun correspondant � une op�ration vid�o La
			 * premi�re et la derni�re p�riode qui sont plus courtes que les
			 * autres font l'objet d'un traitement particulier. Cette m�thode
			 * passe tout le fichier au scan et sort un arrayList
			 * d'op�rationVideo Ce dernier sera ensuite trait� par la m�thode
			 * enregistre. VOIR :
			 * http://java.developpez.com/faq/java/?page=langage_fichiers#
			 * LANGAGE_FICHIER_lireAvecDelimiteur"
			 * 
			 * @param fichier
			 * @return une chaine de caractere avec le r�sultat du parcours
			 * @throws Exception
			 */
			private String scan(File fichier) throws Exception {

				// Le scanner d'un fichier
				Scanner scanner = null;

				// L'import des �l�ments d'une ligne de fichier vid�o
				LigneVideo ligne = null;

				// Une collection d'objets stockant la somme des lignes d'un pas
				// de temps
				HashMap<String, Serializable> operationVideo = new HashMap<String, Serializable>();

				Date currentdate = null; // La date de la ligne courante
				Date datedebutpas = null; // la date de d�but d'un pas de temps
				Date datefinpas = null; // la date de fin d'un pas de temps
				PasDeTemps pt = null; //
				int countope = 1; // compteur du nombre d'op�rations
				String result = null;
				int compteurmax = 1;

				try {
					compteurmax = nblignes(fichier);
				} catch (IOException e) {
					result = e.getMessage();
				}
				try {
					scanner = new Scanner(fichier);
					scanner.useDelimiter(Pattern.compile("[\t\n;]")); // \\s*;\\s*
					// L'expression r�guli�re qui d�limite les champs
				} catch (FileNotFoundException e) {
					result = e.getMessage();
				}

				if (!pasDeTemps.equals("0 min")) {
					try {
						for (int i = 1; i <= compteurmax; i++) { // Toutes les
																	// lignes du
																	// fichier

							ligne = new LigneVideo();
							ligne.lireLigne(scanner.nextLine());
							currentdate = (Date) ligne.getDate(); // nouvelle
																	// date
							setProgress((int) Math.round((((double) i) / compteurmax) * 100));
							if (i == 1) {
								// pour la premi�re ligne
								// recuperation d'un objet pas de temps (qui ne
								// poss�de pas
								// de nombre de pas)
								pt = new PasDeTemps(currentdate, pasDeTemps);
								// utilisation de la methode arrondisup pour
								// r�cup�rer la
								// premi�re horodate
								// ex 10:37:08 => 10:40:00 pour un pas de 10
								// minutes
								datefinpas = pt.arrondisup();
								operationVideo = new HashMap<String, Serializable>();
								datedebutpas = pt.getDateDebut();
								operationVideo.put("datedebutpas", datedebutpas);
								operationVideo.put("datefinpas", datefinpas);
							}
							// Passage � une autre op�ration lorsqu'on d�passe
							// le premier pas de temps arrondi
							while (currentdate.getTime() > datefinpas.getTime()) {
								// 1. INCREMENTATION
								countope += 1;

								// on ajoute dans la liste des op�ration si et
								// seulement si il y a un taxon associ� (taxon
								// ou remarque)
								// donc si l'op�ration vid�o poss�de 1 date de
								// d�but, 1 date de fin, et autre chose en +
								// (taxon ou remarque)
								// cad operationVideo.size()>2
								if (operationVideo.size() > 2)
									this.lesOpeVideo.add(operationVideo);

								// 2. CREATION DE LA NOUVELLE OPERATION ET
								// INCREMENTATION DU PAS DE TEMPS
								// deuxi�me boucle il faut modifier l'objet pas
								// de temps pour le
								// remettre avec une heure ronde
								if (countope == 2) {
									// Pour la deuxi�me op�ration, on commence
									// au pas de
									// temps de fin et on calculera pt.next pour
									// toutes les autres
									datedebutpas = datefinpas;
									pt = new PasDeTemps(datedebutpas, pasDeTemps);
								} else
									pt.next(); // avance d'un pas de temps

								datefinpas = pt.currentDateFin();
								datedebutpas = pt.currentDateDebut();
								operationVideo = new HashMap<String, Serializable>();// remise
																						// �
																						// z�ro
																						// de
																						// operationVid�o
								operationVideo.put("datedebutpas", datedebutpas);
								operationVideo.put("datefinpas", datefinpas);
							}
							operationVideo = ajout(operationVideo, ligne);

							if (i == compteurmax) {
								operationVideo.put("datedebutpas", datedebutpas);
								datefinpas.setTime(currentdate.getTime());
								operationVideo.put("datefinpas", datefinpas);
								this.lesOpeVideo.add(operationVideo);
							}
						}
					} catch (IOException e) {
						result = e.getMessage();
					} finally {
						if (scanner != null)
							scanner.close();
					}
				} else // pas de temps nul
				{
					// parcours de tout le fichier
					for (int i = 1; i <= compteurmax; i++) {
						ligne = new LigneVideo();
						ligne.lireLigne(scanner.nextLine());
						currentdate = (Date) ligne.getDate(); // nouvelle date
						setProgress((int) Math.round((((double) i) / compteurmax) * 100));

						if (i != 1 && ((Date) ((HashMap) lesOpeVideo.get(lesOpeVideo.size() - 1)).get("datedebutpas"))
								.equals(currentdate)) {
							operationVideo = this.lesOpeVideo.remove(lesOpeVideo.size() - 1);
							operationVideo = ajout(operationVideo, ligne);
							this.lesOpeVideo.add(operationVideo);
						} else {
							operationVideo = new HashMap<String, Serializable>();
							operationVideo.put("datedebutpas", currentdate);
							operationVideo.put("datefinpas", currentdate);
							operationVideo = ajout(operationVideo, ligne);
							this.lesOpeVideo.add(operationVideo);
						}
					}
				}
				return result;
			}

			/**
			 * T�che de fond = parcourir le fichier
			 */
			@Override
			protected ArrayList doInBackground() throws Exception {
				jProgressBar.setString("Lecture du fichier");
				this.addPropertyChangeListener(new PropertyChangeListener() {
					public void propertyChange(PropertyChangeEvent evt) {
						if ("progress".equals(evt.getPropertyName())) {
							jProgressBar.setValue((Integer) evt.getNewValue());
						}
					}
				});

				this.scan(fichier);

				return getLesOperationVideo();
			}

			@Override
			protected void done() {
				try {
					lesOperationsVideo = get();
					setProgress(0);
					// r�cup�ration des op�rateur et organisme 1 fois pour
					// toutes
					setOperateur();
					setOrganisme();

					// nouveau swingworker pour mettre a jour la barre de
					// progression lors de l'enregistrement dans la base
					new SwingWorker<String, Void>() {
						// resultat de l'�criture dans la BDD
						String res;

						/**
						 * Recup�re l'arraylist des operationVideo et cr�e pour
						 * chaque operationVideo, les lots, les op�rations et
						 * les caract�risques de lots correspondantes stocke au
						 * fur et � mesure un vecteur de lots et d'op�ration
						 * pour les supprimer si il y a plantage � un moment
						 * donn�
						 */
						public void enregistrement() throws ClassNotFoundException, SQLException {
							// parcours des op�rations pour les ins�rer dans la
							// table
							jProgressBar.setString(Messages.getString("ImporterVideo.EcritureDesOperations"));
							for (int i = 0; i < ensembleOperations.size(); i++) {
								Operation o = ensembleOperations.get(i);
								o.insertObjet();
								setProgress(100 * i / ensembleOperations.size());
							}

							jProgressBar.setString(Messages.getString("ImporterVideo.EcritureDesLots"));
							// parcours des lots pour les ins�rer dans la table
							for (int j = 0; j < ensembleLots.size(); j++) {
								Lot l = ensembleLots.get(j);
								l.insertObjet();
								setProgress(100 * j / ensembleLots.size());
							}

							jProgressBar.setString(Messages.getString("ImporterVideo.EcritureDesCaracteristiques"));
							// parcours des caract�ristiques des lots pour les
							// ins�rer dans la table
							for (int k = 0; k < ensembleCaracteristique.size(); k++) {
								Caracteristique c = ensembleCaracteristique.get(k);
								c.insertObjet();
								setProgress(100 * k / ensembleCaracteristique.size());
							}
							setProgress(100);
							res = Message.I1001;
						}

						protected String doInBackground() throws Exception {
							// les op�rations sont toutes dans
							// lesOperationsVideo
							calculeOperationsEtLots();

							jProgressBar.setString(Messages.getString("ImporterVideo.EnregistrementDansLaBase"));
							this.addPropertyChangeListener(new PropertyChangeListener() {
								public void propertyChange(PropertyChangeEvent evt) {
									if ("progress".equals(evt.getPropertyName())) {
										jProgressBar.setValue((Integer) evt.getNewValue());
									}
								}
							});

							// enregistrement dans la BDD
							enregistrement();
							jProgressBar.setString(Messages.getString("ImporterVideo.Termine"));

							return getResultat();
						}

						private String getResultat() {
							return res;
						}

						protected void done() { // appel�e une fois que
												// l'enregistrement est fini...
							try {
								String res = get();

								// succ�s
								resultat.setText(res);
							} catch (InterruptedException e) {// Le thread a �t�
																// interrompu
								resultat.setText(e.getMessage());
							} catch (ExecutionException e) {// La m�thode
															// doInBackground a
															// g�n�r� une
															// exception
								try {
									supprime();
									resultat.setText(Message.S1007 + " " + e.getMessage()); // suppression
																							// =
																							// succ�s

								} catch (Exception exc)// erreur � la
														// suppression
								{
									resultat.setText(Erreur.S12011 + " " + exc.getMessage());
								}
							}
						}
					}.execute();
				} catch (InterruptedException e) {// Le thread a �t� interrompu
					resultat.setText(e.getMessage());
				} catch (ExecutionException e) {// La m�thode doInBackground a
												// g�n�r� une exception
					resultat.setText(e.getMessage());
				} catch (Exception e) // l'ecriture s'est mal pass�e
				{
					resultat.setText(e.getMessage());
				}
			}
		}.execute();

	}

	/**
	 * Compte le nombre de lignes contenus dans le fichier texte Cette m�thode
	 * est utilis�e pour la barre d'�tat
	 * 
	 * @param fichier
	 *            le fichier contenant les captures vid�o
	 * @return le nombre de ligne que contient le fichier
	 * @throws IOException
	 * @throws Exception
	 */
	public static int nblignes(File fichier) throws IOException {
		int compteur = 0;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fichier));

			while (br.readLine() != null) {
				compteur += 1;
			}
			br.close();
			return compteur;

		} finally {
			if (br != null)
				br.close();
		}
	}

	/**
	 * Cette fonction permet le remplissage de la premi�re ligne d'un arraylist
	 * correspondant � la somme des esp�ces observ�es sur un pas de temps Elles
	 * utilise la structure suivante : HashMap operationVideo("codeTaxon",
	 * HashMap lot) ou ("commentaires", String) ou ("datedebut" Date) ou
	 * ("datefin" Date) |-> HashMap lot("effectif",Integer) ou
	 * ("souslot",Arraylist souslot) |-> Arraylist lessouslot
	 * (0,vecteur[2]souslot1) (1,vecteur[2]souslot2).... |-> Hashmap
	 * souslot("taille",Float=f(TaillePixel,distance)ou ("remarque"=String
	 * nomfichier+date+remarque)
	 * 
	 * @param operationVideo
	 *            un Hashmap
	 * @param ligneVideo
	 *            La ligne d'un fichier vid�o
	 * @return HasMap (cl� taxon) Contenant une ligne avec la somme des
	 *         effectifs rencontr�s pour une esp�ce pendant un pas de temps
	 *         (lots) et les sous lots rattach�s � cette op�ration
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String, Serializable> ajout(HashMap<String, Serializable> operationVideo, LigneVideo ligneVideo)
			throws Exception {

		HashMap lot;
		ArrayList lessouslot;
		HashMap souslot;
		String taxonVideo = ligneVideo.getTaxonVideo();
		if (taxonVideo.equals("REM")) {
			// cas ou une remarque est ajout�e au milieu du fichier de taxons
			// si l'op�ration courante a d�j� un commentaire, on concat�ne avec
			// l'ancien
			if (operationVideo.containsKey("commentaires")) {
				String s = (String) operationVideo.remove("commentaires");
				s += ", " + ligneVideo.getDate() + ":" + ligneVideo.getRemarque();
				operationVideo.put("commentaires", s);
			} else // sinon on ajoute le commentaire
				operationVideo.put("commentaires", ligneVideo.getDate() + ":" + ligneVideo.getRemarque());

		} else {
			// La r�f�rence est-elle dans la base pour permettre de convertir en
			// taxon ?
			ListeTaxonVideo ltv = Lanceur.getListeTaxonVideo();
			Vector<String> v = ConvertToVector(ltv);
			if (v.indexOf(taxonVideo) == -1)
				throw new Exception(Erreur.B1024 + ": " + taxonVideo + " ligne : " + ligneVideo.getFichierVideo());

			Taxonvideo txv = (Taxonvideo) ltv.get(v.indexOf(taxonVideo));
			RefTaxon reftaxon = txv.getrefTaxon();
			if (reftaxon == null)
				throw new Exception(Erreur.B1023 + " : " + taxonVideo + ligneVideo.getFichierVideo());

			if (operationVideo.containsKey(taxonVideo)) {
				// le taxon a d�j� �t� rencontr�
				lot = (HashMap) operationVideo.get(taxonVideo);
				Integer effectif_stocked = (Integer) lot.get("effectif");

				Integer effectif_new = (Integer) ligneVideo.getEffectif();
				effectif_stocked = new Integer(effectif_stocked.intValue() + effectif_new.intValue());
				lot.put("effectif", effectif_stocked);
				String nomfichier = ligneVideo.getFichierVideo();
				String tmpRem = (String) lot.remove("remarque");
				lot.put("remarque", tmpRem + " / " + nomfichier + ligneVideo.getDate());
			} else {
				// nouveau taxon
				// effectif
				String nomfichier = ligneVideo.getFichierVideo();
				lot = new HashMap();
				lot.put("effectif", ligneVideo.getEffectif());
				lot.put("remarque", nomfichier + ligneVideo.getDate());
				operationVideo.put(taxonVideo, lot);
			}

			// Sous lot
			if (ligneVideo.getTailleEnPixel().floatValue() > 0 || ligneVideo.getRemarque().length() != 0) {
				// cr�e ou r�cup�re les souslots d'un lot
				if (lot.containsKey("souslot"))
					lessouslot = (ArrayList) lot.get("souslot");
				else
					lessouslot = new ArrayList();

				if (ligneVideo.getTailleEnPixel().floatValue() > 0 && ligneVideo.getRemarque().length() != 0) {
					// Il existe une remarque et une taille en pixel
					// recup�ration de la taille par multiplication par le coeff
					// de conversion
					// correspondant � la distance I L ou P de la ligne d'import
					Float taille = ligneVideo.getTailleEnPixel();
					String distance = ligneVideo.getDistance();
					TailleVideo tav = getTailleVideo(distance);
					if (getTailleVideo("PRE") != null)
						precision = getTailleVideo("PRE").getCoefConversion();
					else
						precision = null;
					if (tav != null) {
						Float coefconversion = tav.getCoefConversion();
						// on prend l'entier le + proche plutot que de laisser
						// un chiffre � virgule...
						Float tmpValue = taille.floatValue() * coefconversion.floatValue();
						int entierPlusProche = (int) (tmpValue + 0.5);
						taille = new Float(entierPlusProche);
						String rem = ligneVideo.getRemarque();
						String nomfichier = ligneVideo.getFichierVideo();
						souslot = new HashMap();
						souslot.put("taille", taille);
						souslot.put("remarque", nomfichier + ligneVideo.getDate() + rem);
						lessouslot.add(souslot);
						lot.put("souslot", lessouslot);
					} else
						throw new Exception(Erreur.S12015);
				} else if (ligneVideo.getTailleEnPixel().floatValue() > 0) {
					// il existe une taille en pixel mais pas de remarque
					Float taille = ligneVideo.getTailleEnPixel();
					String distance = ligneVideo.getDistance();
					TailleVideo tav = getTailleVideo(distance);
					if (tav != null) {
						String rem = ligneVideo.getRemarque();
						String nomfichier = ligneVideo.getFichierVideo();
						Float coefconversion = tav.getCoefConversion();
						// on prend l'entier le + proche plutot que de laisser
						// un chiffre � virgule...
						Float tmpValue = taille.floatValue() * coefconversion.floatValue();
						int entierPlusProche = (int) (tmpValue + 0.5);
						taille = new Float(entierPlusProche);
						souslot = new HashMap();
						souslot.put("remarque", nomfichier + ligneVideo.getDate() + rem);
						souslot.put("taille", taille);
						lessouslot.add(souslot);
						lot.put("souslot", lessouslot);
					} else
						throw new Exception(Erreur.S12015);
				} else {
					// il existe une remarque mais pas de taille en pixels
					String rem = ligneVideo.getRemarque();
					String nomfichier = ligneVideo.getFichierVideo();
					String remarque = nomfichier + ligneVideo.getDate() + rem;
					// s'il y a 1 commentaire au niveau du lot, on le d�place au
					// niveau du sous-lot
					if ((String) lot.get("remarque") != null) {
						souslot = new HashMap();
						String rema = (String) lot.remove("remarque");
						souslot.put("remarque", rema);
						lessouslot.add(souslot);

						HashMap sousLot2 = new HashMap();
						sousLot2.put("remarque", remarque);
						lessouslot.add(sousLot2);
						lot.put("souslot", lessouslot);
					} else // sinon on le met ici
						lot.put("remarque", remarque);
				}
			}
		}
		return operationVideo;
	}

	/**
	 * Acc�s � la TailleVideo du DC courant, donc la distance est 'distance'
	 * 
	 * @param distance
	 *            la distance � rechercher dans la liste des TailleVideo
	 * @return la TailleVideo associ�e � distance si elle existe, null sinon
	 */
	private TailleVideo getTailleVideo(String distance) {
		TailleVideo tv = null;
		for (int i = 0; i < listeTailleVideo.size(); i++) {
			if (((TailleVideo) listeTailleVideo.get(i)).getDistance().equals(distance))
				tv = (TailleVideo) listeTailleVideo.get(i);
		}
		return tv;
	}

	/*
	 * Composants graphiques
	 */
	/**
	 * This method initializes top
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getTop() {
		if (top == null) {
			top = new JPanel();
			top.setLayout(new java.awt.BorderLayout());
			top.setBackground(new java.awt.Color(0, 51, 153));
			// top.setBackground(new Color(35, 149, 10));
			top.setFont(new java.awt.Font("Dialog", 1, 14)); //$NON-NLS-1$
			top.setForeground(new java.awt.Color(255, 255, 255));

			resultat = new JLabel();
			resultat.setBackground(new java.awt.Color(255, 255, 255));
			resultat.setForeground(new java.awt.Color(255, 0, 0));
			resultat.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			resultat.setOpaque(true);
			Titre = new JLabel();
			Titre.setText(Messages.getString("ImporterVideo.Titre"));
			Titre.setForeground(Color.white);
			Titre.setFont(new Font("Dialog", Font.BOLD, 14));
			Titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			top.add(Titre, java.awt.BorderLayout.CENTER);
			top.add(resultat, java.awt.BorderLayout.SOUTH);
		}
		return top;
	}

	/**
	 * This method initializes center
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getCenter() {
		if (center == null) {
			GridBagConstraints gridBagConstraints31 = new GridBagConstraints();
			gridBagConstraints31.gridx = 0;
			gridBagConstraints31.gridy = 0;
			jLabel1 = new JLabel();
			jLabel1.setText("");
			jLabel1.setPreferredSize(new Dimension(100, 16));
			GridBagConstraints gridBagConstraints15 = new GridBagConstraints();
			gridBagConstraints15.gridx = 1;
			gridBagConstraints15.anchor = GridBagConstraints.WEST;
			gridBagConstraints15.insets = new Insets(3, 10, 3, 3);
			gridBagConstraints15.gridy = 8;
			jLabelPasDeTemps = new JLabel();
			jLabelPasDeTemps.setText(Messages.getString("ImporterVideo.PasDeTemps"));
			jLabelPasDeTemps.setForeground(SystemColor.textInactiveText);
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints.gridy = 8;
			gridBagConstraints.weightx = 1.0;
			gridBagConstraints.anchor = GridBagConstraints.WEST;
			gridBagConstraints.insets = new Insets(2, 2, 2, 2);
			gridBagConstraints.gridx = 2;
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints2.gridy = 7;
			gridBagConstraints2.weightx = 1.0;
			gridBagConstraints2.anchor = GridBagConstraints.WEST;
			gridBagConstraints2.insets = new Insets(2, 2, 2, 2);
			gridBagConstraints2.gridx = 2;
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.gridx = 1;
			gridBagConstraints1.anchor = GridBagConstraints.WEST;
			gridBagConstraints1.insets = new Insets(3, 10, 3, 3);
			gridBagConstraints1.gridy = 7;
			jLabelOrganisme = new JLabel();
			jLabelOrganisme.setText(Messages.getString("ImporterVideo.Organisme"));
			GridBagConstraints gridBagConstraints14 = new GridBagConstraints();
			gridBagConstraints14.gridx = 1;
			gridBagConstraints14.anchor = GridBagConstraints.WEST;
			gridBagConstraints14.insets = new Insets(3, 10, 3, 3);
			gridBagConstraints14.gridy = 4;
			jLabelDC = new JLabel();
			jLabelDC.setText(Messages.getString("ImporterVideo.DC"));
			jLabelDC.setForeground(SystemColor.textInactiveText);
			GridBagConstraints gridBagConstraints13 = new GridBagConstraints();
			gridBagConstraints13.gridx = 1;
			gridBagConstraints13.anchor = GridBagConstraints.WEST;
			gridBagConstraints13.insets = new Insets(3, 10, 3, 3);
			gridBagConstraints13.gridy = 3;
			jLabelDF = new JLabel();
			jLabelDF.setText(Messages.getString("ImporterVideo.DF"));
			jLabelDF.setForeground(SystemColor.textInactiveText);
			GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
			gridBagConstraints12.gridx = 1;
			gridBagConstraints12.anchor = GridBagConstraints.WEST;
			gridBagConstraints12.insets = new Insets(3, 10, 3, 3);
			gridBagConstraints12.gridy = 1;
			jLabelOuvrage = new JLabel();
			jLabelOuvrage.setText(Messages.getString("ImporterVideo.Ouvrage"));
			jLabelOuvrage.setForeground(SystemColor.textInactiveText);
			GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
			gridBagConstraints11.gridx = 1;
			gridBagConstraints11.anchor = GridBagConstraints.WEST;
			gridBagConstraints11.insets = new Insets(3, 10, 3, 3);
			gridBagConstraints11.gridy = 0;
			jLabelStation = new JLabel();
			jLabelStation.setText(Messages.getString("ImporterVideo.Station"));
			jLabelStation.setForeground(SystemColor.textHighlight);
			GridBagConstraints gridBagConstraints10 = new GridBagConstraints();
			gridBagConstraints10.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints10.gridy = 4;
			gridBagConstraints10.weightx = 1.0;
			gridBagConstraints10.anchor = GridBagConstraints.WEST;
			gridBagConstraints10.insets = new Insets(2, 2, 2, 2);
			gridBagConstraints10.gridx = 2;
			GridBagConstraints gridBagConstraints9 = new GridBagConstraints();
			gridBagConstraints9.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints9.gridy = 3;
			gridBagConstraints9.weightx = 1.0;
			gridBagConstraints9.anchor = GridBagConstraints.WEST;
			gridBagConstraints9.insets = new Insets(2, 2, 2, 2);
			gridBagConstraints9.gridx = 2;
			GridBagConstraints gridBagConstraints8 = new GridBagConstraints();
			gridBagConstraints8.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints8.gridy = 1;
			gridBagConstraints8.weightx = 1.0;
			gridBagConstraints8.anchor = GridBagConstraints.WEST;
			gridBagConstraints8.insets = new Insets(2, 2, 2, 2);
			gridBagConstraints8.gridx = 2;
			GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
			gridBagConstraints7.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints7.gridy = 0;
			gridBagConstraints7.weightx = 1.0;
			gridBagConstraints7.anchor = GridBagConstraints.WEST;
			gridBagConstraints7.insets = new Insets(2, 2, 2, 2);
			gridBagConstraints7.gridx = 2;
			GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
			gridBagConstraints6.gridx = 1;
			gridBagConstraints6.anchor = GridBagConstraints.WEST;
			gridBagConstraints6.insets = new Insets(3, 10, 3, 3);
			gridBagConstraints6.gridy = 6;
			JLabelOperateur = new JLabel();
			JLabelOperateur.setText(Messages.getString("ImporterVideo.Operateur"));
			GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
			gridBagConstraints5.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints5.gridy = 6;
			gridBagConstraints5.weightx = 1.0;
			gridBagConstraints5.insets = new Insets(2, 2, 2, 0);
			gridBagConstraints5.anchor = GridBagConstraints.WEST;
			gridBagConstraints5.gridx = 2;
			GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
			gridBagConstraints4.fill = GridBagConstraints.VERTICAL;
			gridBagConstraints4.gridy = 9;
			gridBagConstraints4.weightx = 1.0;
			gridBagConstraints4.insets = new Insets(2, 2, 2, 0);
			gridBagConstraints4.anchor = GridBagConstraints.WEST;
			gridBagConstraints4.gridx = 2;
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			gridBagConstraints3.gridx = 1;
			gridBagConstraints3.insets = new Insets(3, 10, 3, 3);
			gridBagConstraints3.anchor = GridBagConstraints.WEST;
			gridBagConstraints3.gridy = 9;
			center = new JPanel();
			center.setLayout(new GridBagLayout());
			center.setBackground(new Color(230, 230, 232));
			center.setPreferredSize(new Dimension(600, 400));
			center.add(getJButtonRechercheFichier(), gridBagConstraints3);
			center.add(getJTextFieldNomFichier(), gridBagConstraints4);
			center.add(getJTextFieldOperateur(), gridBagConstraints5);
			center.add(JLabelOperateur, gridBagConstraints6);
			center.add(getJComboBoxStation(), gridBagConstraints7);
			center.add(getJComboBoxOuvrage(), gridBagConstraints8);
			center.add(getJComboBoxDF(), gridBagConstraints9);
			center.add(getJComboBoxDC(), gridBagConstraints10);
			center.add(jLabelStation, gridBagConstraints11);
			center.add(jLabelOuvrage, gridBagConstraints12);
			center.add(jLabelDF, gridBagConstraints13);
			center.add(jLabelDC, gridBagConstraints14);
			center.add(jLabelOrganisme, gridBagConstraints1);
			center.add(getJTextFieldOrganisme(), gridBagConstraints2);
			center.add(getJComboBoxPasDeTemps(), gridBagConstraints);
			center.add(jLabelPasDeTemps, gridBagConstraints15);
			center.add(jLabel1, gridBagConstraints31);
		}
		return center;
	}

	/**
	 * This method initializes center
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getBottom() {
		if (bottom == null) {
			bottom = new JPanel();
			bottom.setLayout(new FlowLayout());
			bottom.setPreferredSize(new Dimension(176, 36));
			bottom.setBackground(new Color(191, 191, 224));
			bottom.add(getImporter(), null);
			bottom.add(getAnnuler(), null);
			bottom.add(getJProgressBar(), null);
		}
		return bottom;
	}

	/**
	 * This method initializes importer
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getImporter() {
		if (importer == null) {
			importer = new JButton();
			importer.setText(Messages.getString("ImporterVideo.Importer"));
			importer.setToolTipText("Lancer l'import");
			importer.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					importerActionPerformed(e);
				}
			});
		}
		return importer;
	}

	/**
	 * This method initializes Annuler
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getAnnuler() {
		if (Annuler == null) {
			Annuler = new JButton();
			Annuler.setText(Messages.getString("ImporterVideo.Annuler"));
			Annuler.setToolTipText("ImporterVideo.AnnulerToolTip");
			Annuler.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					annulerActionPerformed(e);
				}
			});
		}
		return Annuler;
	}

	/**
	 * Annulation de l'insertion, et remise a z�ro des champs
	 * 
	 * @param e
	 */
	private void annulerActionPerformed(ActionEvent e) {
		try {
			this.supprime();
			this.resultat.setText(Message.S1008); // suppression = succes
		} catch (Exception exc) {
			this.resultat.setText(exc.getMessage()); // echec � la suppression
		}

		this.reInitComponents();
	}

	/**
	 * This method initializes jProgressBar
	 * 
	 * @return javax.swing.JProgressBar
	 */
	private JProgressBar getJProgressBar() {
		if (jProgressBar == null) {
			jProgressBar = new JProgressBar(0, 100);
			jProgressBar.setStringPainted(true);
			jProgressBar.setToolTipText(Messages.getString("ImporterVideo.ProgressionImport"));
			jProgressBar.setString(Messages.getString("ImporterVideo.ProgressionImport"));
			jProgressBar.setBounds(320, 240, 250, 100);
			jProgressBar.setVisible(true);
			jProgressBar.setPreferredSize(new Dimension(200, 25));
			jProgressBar.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					jProgressBarStateChanged(e);
				}
			});
		}
		return jProgressBar;
	}

	/**
	 * @param e
	 *            ev�nement
	 */
	public void jProgressBarStateChanged(ChangeEvent e) {
	}

	/**
	 * This method initializes jButtonRechercheFichier
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getJButtonRechercheFichier() {
		if (jButtonRechercheFichier == null) {
			jButtonRechercheFichier = new JButton();
			jButtonRechercheFichier.setText(Messages.getString("ImporterVideo.Fichier"));
			jButtonRechercheFichier.setToolTipText("Fichier concat�n� au format .ssm");
			jButtonRechercheFichier.setIcon(new ImageIcon(getClass().getResource("/images/Open16.gif")));
			jButtonRechercheFichier.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jButton_RechercheFichierActionPerformed(e);
					if (fichier != null) {
						jTextFieldNomFichier.setText(fichier.getAbsolutePath());
						jTextFieldNomFichier.setEditable(false);
					}
				}
			});
		}
		return jButtonRechercheFichier;
	}

	/**
	 * This method initializes jTextFieldNomFichier
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldNomFichier() {
		if (jTextFieldNomFichier == null) {
			jTextFieldNomFichier = new JTextField();
			jTextFieldNomFichier.setPreferredSize(new Dimension(450, 20));
			jTextFieldNomFichier.setToolTipText("Un fichier d'import au format .ssm");
		}
		return jTextFieldNomFichier;
	}

	/**
	 * This method initializes jTextFieldOperateur
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldOperateur() {
		if (jTextFieldOperateur == null) {
			jTextFieldOperateur = new JTextField();
			jTextFieldOperateur.setPreferredSize(new Dimension(150, 20));
			jTextFieldOperateur.setToolTipText("ex : Denis");
			jTextFieldOperateur.setText("");
		}
		return jTextFieldOperateur;
	}

	/**
	 * This method initializes jComboBoxStation
	 * 
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJComboBoxStation() {
		if (jComboBoxStation == null) {
			jComboBoxStation = new JComboBox();
			jComboBoxStation.setToolTipText("La station de contr�le");
			jComboBoxStation.setModel(new DefaultComboBoxModel(this.lesStations.toArray()));
			jComboBoxStation.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jComboBoxStationactionPerformed(e);
				}

			});
			jComboBoxStation.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent e) {
					jComboBoxStationMouseClicked(e);
				}

			});
		}
		return jComboBoxStation;
	}

	private void jComboBoxStationactionPerformed(ActionEvent e) {
		this.chargeSousListeOuvrages();
	}

	private void jComboBoxStationMouseClicked(MouseEvent e) {
		this.chargeSousListeOuvrages();
	}

	/**
	 * This method initializes jComboBoxOuvrage
	 * 
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJComboBoxOuvrage() {
		if (jComboBoxOuvrage == null) {
			jComboBoxOuvrage = new JComboBox();
			jComboBoxOuvrage.setToolTipText("L'ouvrage de la station");
			jComboBoxOuvrage.setEnabled(false);
			jComboBoxOuvrage.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent e) {
					jComboBoxOuvrageMouseClicked(e);
				}
			});
			jComboBoxOuvrage.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jComboBoxOuvrageactionPerformed(e);
				}
			});
		}
		return jComboBoxOuvrage;
	}

	private void jComboBoxOuvrageMouseClicked(MouseEvent e) {
		this.chargeSousListeDF();
	}

	private void jComboBoxOuvrageactionPerformed(ActionEvent e) {
		this.chargeSousListeDF();
	}

	/**
	 * This method initializes jComboBoxDF
	 * 
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJComboBoxDF() {
		if (jComboBoxDF == null) {
			jComboBoxDF = new JComboBox();
			jComboBoxDF.setEnabled(false);
			jComboBoxDF.setToolTipText("Le dispositif de franchissement");
			jComboBoxDF.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jComboBoxDFactionPerformed(e);
				}

			});
			jComboBoxDF.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent e) {
					jComboBoxDFmouseClicked(e);
				}

			});
		}
		return jComboBoxDF;
	}

	private void jComboBoxDFactionPerformed(ActionEvent e) {
		this.chargeSousListeDC();
	}

	private void jComboBoxDFmouseClicked(MouseEvent e) {
		this.chargeSousListeDC();
	}

	/**
	 * This method initializes jComboBoxDC
	 * 
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJComboBoxDC() {
		if (jComboBoxDC == null) {
			jComboBoxDC = new JComboBox();
			jComboBoxDC.setEnabled(false);
			jComboBoxDC.setToolTipText("Le dispositif de comptage");
			jComboBoxDC.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent e) {
					jComboBoxDCmouseclicked(e);
				}

			});
			jComboBoxDC.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jComboBoxDCactionPerformed(e);
				}

			});
		}
		return jComboBoxDC;
	}

	/**
	 * Si click de souris, charge la sousliste des pas de temps, et vide la
	 * liste des DC
	 * 
	 * @param e
	 */
	private void jComboBoxDCmouseclicked(MouseEvent e) {
		this.chargePasdeTemps();
	}

	/**
	 * Si modif combo, charge la sousliste des pas de temps, et vide la liste
	 * des DC
	 * 
	 * @param e
	 */
	private void jComboBoxDCactionPerformed(ActionEvent e) {
		this.chargePasdeTemps();
	}

	/**
	 * This method initializes jTextFieldOrganisme
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextFieldOrganisme() {
		if (jTextFieldOrganisme == null) {
			jTextFieldOrganisme = new JTextField();
			jTextFieldOrganisme.setPreferredSize(new Dimension(150, 20));
			jTextFieldOrganisme.setToolTipText("ex: IAV");
		}
		return jTextFieldOrganisme;
	}

	/**
	 * This method initializes jComboBoxPasDeTemps
	 * 
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJComboBoxPasDeTemps() {
		if (jComboBoxPasDeTemps == null) {
			jComboBoxPasDeTemps = new JComboBox();
			jComboBoxPasDeTemps.setEnabled(false);

			jComboBoxPasDeTemps.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jComboBoxPasdeTempsactionPerformed(e);
				}

			});
		}
		return jComboBoxPasDeTemps;
	}

	/**
	 * Si modif combo, charge la sousliste des pas de temps, et vide la liste
	 * des DC
	 * 
	 * @param e
	 */
	private void jComboBoxPasdeTempsactionPerformed(ActionEvent e) {
		this.videPasdeTemps();

	}
}
