package migration.ihm;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Messages affich�s dans l'application 
 * @author Samuel Gaudey
 */
public class Messages {
	private static final String BUNDLE_NAME = "migration.ihm.messages"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private Messages() {
	}

	/**
	 * Retourne la valeur associée à la clé
	 * @param key la clé
	 * @return la valeur associée à la clé
	 */
	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
