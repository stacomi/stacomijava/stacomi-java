/*
 * AMSOperationMarquage.java
 * Created on 25 mai 2009, 10:23
 */

/*
 **********************************************************************
 *
 * Nom fichier :        AMSOperationMarquage.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             S�bastien Laigre
 * Date de creation :   25 mai 2009, 10:23
 * Compatibilite :      Windows XP, Java6
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package migration.ihm;

import commun.*;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;

import migration.OperationMarquage;
import migration.referenciel.ListeOperationMarquage;

/**
 * Classe permettant d'ajouter/modifier/supprimer une op�ration de marquage
 * @author S�bastien Laigre
 */
public class AMSOperationMarquage extends javax.swing.JPanel {

	private static final long serialVersionUID = 7967981010117495061L;

	/** Creates new form AMSOperationMarquage */
	public AMSOperationMarquage() {
		this.initComponents();
		this.chargeListeOperationMarquage();
	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() 
	{
		top = new javax.swing.JPanel(new java.awt.BorderLayout());
		center = new javax.swing.JPanel(new java.awt.GridBagLayout());
		bottom = new javax.swing.JPanel();
		
		titre = new javax.swing.JLabel();
		resultat = new javax.swing.JLabel();
		
	    jLabel_operationsMarquage = new javax.swing.JLabel();
	    jScrollPane_operationMarquage = new javax.swing.JScrollPane();
	    jList_operationMarquage = new javax.swing.JList();
	    jLabel_codeOperationMarquage = new javax.swing.JLabel();
	    jTextField_operationMarquage = new JTextFieldDataType();
	    jLabel_commentairesOperationMarquage = new javax.swing.JLabel();
	    jScrollPane_commentairesOperationMarquage = new javax.swing.JScrollPane();
	    jTextAreaDataType_commentaireOperationMarquage = new JTextAreaDataType();
	    jButton_AMOperationMarquage = new javax.swing.JButton();
	    jButton_SOperationMarquage = new javax.swing.JButton();
	    jButton_annulerOperationMarquage = new javax.swing.JButton();

        this.setLayout(new java.awt.BorderLayout());
        this.setBackground(new java.awt.Color(255, 255, 255));
        this.setPreferredSize(new java.awt.Dimension(800, 600));
        
        this.titre.setBackground(new java.awt.Color(0, 51, 153));
        this.titre.setFont(new java.awt.Font("Dialog", 1, 14)); //$NON-NLS-1$
        this.titre.setForeground(new java.awt.Color(255, 255, 255));
        this.titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        this.titre.setText(Messages.getString("AMSOperationMarquage.Titre")); //$NON-NLS-1$
        this.titre.setOpaque(true);
        
        this.resultat.setBackground(new java.awt.Color(255, 255, 255));
        this.resultat.setForeground(new java.awt.Color(255, 0, 0));
        this.resultat.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        this.resultat.setOpaque(true);
        
        this.top.add(this.titre, java.awt.BorderLayout.CENTER);
        this.top.add(this.resultat, java.awt.BorderLayout.SOUTH);

        this.center.setBackground(new java.awt.Color(230, 230, 230));
        this.center.setPreferredSize(new java.awt.Dimension(600, 400));
        
        this.bottom.setBackground(new java.awt.Color(191, 191, 224));
        
        this.add(this.top, java.awt.BorderLayout.NORTH);
        this.add(this.center, java.awt.BorderLayout.CENTER);
        this.add(this.bottom, java.awt.BorderLayout.SOUTH);

        this.jLabel_operationsMarquage.setText(Messages.getString("AjouterLot.116"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 5, 3, 5);
        this.center.add(this.jLabel_operationsMarquage, gridBagConstraints);

        this.jScrollPane_operationMarquage.setPreferredSize(new Dimension(300, 100));
        this.jScrollPane_operationMarquage.setViewportView(jList_operationMarquage);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
        this.center.add(this.jScrollPane_operationMarquage, gridBagConstraints);

        this.jLabel_codeOperationMarquage.setText(Messages.getString("AjouterLot.117"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 5, 3, 5);
        this.center.add(this.jLabel_codeOperationMarquage, gridBagConstraints);

        this.jTextField_operationMarquage.setPreferredSize(new Dimension(200, 25));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
        this.center.add(this.jTextField_operationMarquage, gridBagConstraints);

        this.jLabel_commentairesOperationMarquage.setText(Messages.getString("AjouterLot.118"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 5, 3, 5);
        this.center.add(jLabel_commentairesOperationMarquage, gridBagConstraints);

        jTextAreaDataType_commentaireOperationMarquage.setColumns(20);
        jTextAreaDataType_commentaireOperationMarquage.setLineWrap(true);
        jTextAreaDataType_commentaireOperationMarquage.setRows(4);
        
        jTextAreaDataType_commentaireOperationMarquage.setColumns(40);
        jTextAreaDataType_commentaireOperationMarquage.setLineWrap(true);
        jTextAreaDataType_commentaireOperationMarquage.setRows(4);
        this.jScrollPane_commentairesOperationMarquage.setViewportView(jTextAreaDataType_commentaireOperationMarquage);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
        this.center.add(jScrollPane_commentairesOperationMarquage, gridBagConstraints);

        this.jButton_AMOperationMarquage.setText(Messages.getString("AjouterLot.16"));
        this.jButton_SOperationMarquage.setText(Messages.getString("AjouterLot.17"));
        this.jButton_annulerOperationMarquage.setText(Messages.getString("AjouterLot.18"));
        this.bottom.add(this.jButton_AMOperationMarquage);
        this.bottom.add(this.jButton_SOperationMarquage);
        this.bottom.add(this.jButton_annulerOperationMarquage);
        
        jList_operationMarquage.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				listePathologiesMouseClicked(evt);
			}
		
			private void listePathologiesMouseClicked(MouseEvent evt) {
				try{
					AMSOperationMarquage.chargeInfoOperationMarquage(
							AMSOperationMarquage.this.jList_operationMarquage,
							AMSOperationMarquage.this.jTextField_operationMarquage,
							AMSOperationMarquage.this.jTextAreaDataType_commentaireOperationMarquage);
				}
				catch (Exception ex) {
					Logger.getLogger(AMSOperationMarquage.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});
        
        jButton_AMOperationMarquage.setToolTipText(Messages.getString("AjouterLot.1022"));
        jButton_AMOperationMarquage.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				// enregistre dans la base
				AMSOperationMarquage.AMOperationMarquage(
						AMSOperationMarquage.this.jList_operationMarquage, 
						AMSOperationMarquage.this.jTextField_operationMarquage, 
						AMSOperationMarquage.this.jTextAreaDataType_commentaireOperationMarquage);
				
				// vide les champs
				AMSOperationMarquage.annulerOperationMarquage( 
						AMSOperationMarquage.this.jTextField_operationMarquage, 
						AMSOperationMarquage.this.jTextAreaDataType_commentaireOperationMarquage);
				
				// met a jour la liste
				AMSOperationMarquage.this.chargeListeOperationMarquage();
			}
		});

        jButton_SOperationMarquage.setToolTipText(Messages.getString("AjouterLot.1033"));
        jButton_SOperationMarquage.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				// supprime de la base
				AMSOperationMarquage.SOperationMarquage(
						AMSOperationMarquage.this.jList_operationMarquage);
				
				// vide les champs
				AMSOperationMarquage.annulerOperationMarquage( 
						AMSOperationMarquage.this.jTextField_operationMarquage, 
						AMSOperationMarquage.this.jTextAreaDataType_commentaireOperationMarquage);
				
				// met a jour la liste
				AMSOperationMarquage.this.chargeListeOperationMarquage();
			}
		});

        jButton_annulerOperationMarquage.setToolTipText(Messages.getString("AjouterLot.104"));
        jButton_annulerOperationMarquage.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AMSOperationMarquage.annulerOperationMarquage(
						AMSOperationMarquage.this.jTextField_operationMarquage, 
						AMSOperationMarquage.this.jTextAreaDataType_commentaireOperationMarquage);
				AMSOperationMarquage.this.chargeListeOperationMarquage();
			}
		});
        
	}// </editor-fold>//GEN-END:initComponents

	private javax.swing.JPanel top;
	private javax.swing.JPanel center;
	private javax.swing.JPanel bottom;
	
	private javax.swing.JLabel titre;
	private javax.swing.JLabel resultat;
	
    private javax.swing.JLabel jLabel_operationsMarquage;
    private javax.swing.JScrollPane jScrollPane_operationMarquage;
    private javax.swing.JList jList_operationMarquage;
    private javax.swing.JLabel jLabel_codeOperationMarquage;
    private JTextFieldDataType jTextField_operationMarquage;
    private javax.swing.JLabel jLabel_commentairesOperationMarquage;
    private javax.swing.JScrollPane jScrollPane_commentairesOperationMarquage;
    private JTextAreaDataType jTextAreaDataType_commentaireOperationMarquage;
    private javax.swing.JButton jButton_AMOperationMarquage;
    private javax.swing.JButton jButton_SOperationMarquage;
    private javax.swing.JButton jButton_annulerOperationMarquage;
	

    private ListeOperationMarquage lesOperationsMarquages;
    
    
    /**
     * Chargement des op�rations de marquage
     */
    private void chargeListeOperationMarquage()
    {
    	this.lesOperationsMarquages = new ListeOperationMarquage();
    	
    	try
    	{
    		this.lesOperationsMarquages.chargeSansFiltreDetails();
    		this.jList_operationMarquage.setModel(new DefaultComboBoxModel(this.lesOperationsMarquages.toArray()));
    	}
    	catch(Exception e)
    	{
    		this.resultat.setText(e.getMessage());
    	}
    }
    
    /**
     * Ajout/modification d'une op�ration de marquage recapture dans la base
     * @param listeOpe : la liste des op�rations (JList)
     * @param ref : le textField contenant la r�f�rence de l'op�ration
     * @param commentaires : le textField contenant le commentaire
     * @return le r�sultat sous forme de string
     */
    public static String AMOperationMarquage(
    		JList listeOpe, 
    		JTextFieldDataType ref,
    		JTextAreaDataType commentaires)
    {
    	String result = null;
    	
    	String reference = null;
		String commentaire = null;
		
    	try { reference = ref.getText(); }
    	catch(Exception e) { result = Erreur.S20000; }
		
    	try { commentaire = commentaires.getText(); }
    	catch(Exception e) { result = Erreur.S20001;  }

    	OperationMarquage om = (OperationMarquage)listeOpe.getSelectedValue();
    	
    	if(result==null)
    	{
			try
			{
				OperationMarquage newOpe = new OperationMarquage(reference,commentaire);
				newOpe.verifAttributs();
				
		    	if(om==null)// insertion
		    	{
		    		newOpe.insertObjet();
		    		result = Message.A1013;
		    	}
		    	else // modification
		    	{
		    		newOpe.majObjet(om);
		    		result = Message.M1013;
		    	}
			}
			catch(Exception e)
			{
				result = e.getMessage();
			}
    	}
    	
    	return result;
    }
    
    /**
     * Suppression d'une op�ration de marquage recapture dans la base
     * @param listeOpe : la liste des op�rations (JList)
     * @return le r�sultat de la suppression sous forme de string
     */
    public static String SOperationMarquage( JList listeOpe )
    {
    	String result = null;
    	
    	if(listeOpe.getSelectedValue()==null)
    		result = Erreur.S20002;
    	else
    	{
	    	OperationMarquage om = (OperationMarquage)listeOpe.getSelectedValue();

			try
			{
				om.effaceObjet();
		    	result = Message.S1011;
			}
			catch(Exception e)
			{
				result = e.getMessage();
			}
    	}
    	
    	return result;
    }
    
    /**
     * Annulation d'une saisie d'op�ration de marquage recapture
     * @param ref : le textField contenant la r�f�rence de l'op�ration
     * @param commentaires : le textField contenant le commentaire
     */
    public static void annulerOperationMarquage(
    		JTextFieldDataType ref,
    		JTextAreaDataType commentaires)
    {
    	ref.setText("");
    	commentaires.setText("");
    }
    
    /**
     * @param listeOpe
     * @param ref
     * @param commentaires
     */
    public static void chargeInfoOperationMarquage(
    		JList listeOpe, 
    		JTextFieldDataType ref,
    		JTextAreaDataType commentaires)
	{
    	OperationMarquage om = (OperationMarquage)listeOpe.getSelectedValue();
    	
    	ref.setText(om.getReference());
    	commentaires.setText(om.getCommentaires());
	}

}


