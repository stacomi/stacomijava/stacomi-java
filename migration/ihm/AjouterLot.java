/*
 * AjouterLot.java
 *
 * Created on 26 mai 2004, 09:42
 * Modified on 09 mai 2009
 */

package migration.ihm;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.zip.DataFormatException;

import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.apache.commons.lang3.StringUtils;

import commun.EnabledJComboBoxRenderer;
import commun.Erreur;
import commun.IhmAppli;
import commun.JTextAreaDataType;
import commun.JTextFieldDataType;
import commun.Lanceur;
import commun.LotTreeCellRenderer;
import commun.Message;
import commun.Ref;
import commun.SousListeRefValeurParametre;
import infrastructure.DC;
import infrastructure.ihm.AMSPeriodeFonctionnement;
import infrastructure.referenciel.SousListeDC;
import migration.Caracteristique;
import migration.Lot;
import migration.Marquage;
import migration.Marque;
import migration.Operation;
import migration.OperationMarquage;
import migration.PathologieConstatee;
import migration.Prelevement;
import migration.referenciel.ListeOperationMarquage;
import migration.referenciel.ListeRefDevenir;
import migration.referenciel.ListeRefImportancePathologie;
import migration.referenciel.ListeRefLocalisation;
import migration.referenciel.ListeRefNatureMarque;
import migration.referenciel.ListeRefParamQualBio;
import migration.referenciel.ListeRefParamQuantBio;
import migration.referenciel.ListeRefPathologie;
import migration.referenciel.ListeRefPrelevement;
import migration.referenciel.ListeRefStade;
import migration.referenciel.ListeRefTaxon;
import migration.referenciel.ListeRefTypeQuantite;
import migration.referenciel.RefDevenir;
import migration.referenciel.RefImportancePathologie;
import migration.referenciel.RefLocalisation;
import migration.referenciel.RefNatureMarque;
import migration.referenciel.RefParametre;
import migration.referenciel.RefParametreQualitatif;
import migration.referenciel.RefParametreQuantitatif;
import migration.referenciel.RefPathologie;
import migration.referenciel.RefPrelevement;
import migration.referenciel.RefStade;
import migration.referenciel.RefTaxon;
import migration.referenciel.RefTypeQuantite;
import migration.referenciel.RefValeurParametre;
import migration.referenciel.SousListeCaracteristiques;
import migration.referenciel.SousListeLots;
import migration.referenciel.SousListeMarquages;
import migration.referenciel.SousListeOperations;
import migration.referenciel.SousListePathologieConstatee;
import migration.referenciel.SousListePrelevement;
import systeme.AffichageMasqueLot;
import systeme.CaracteristiqueMasqueLot;
import systeme.Masque;
import systeme.MasqueLot;
import systeme.referenciel.ListeMasque;
import systeme.referenciel.ListeMasqueLot;
import systeme.referenciel.SousListeAffichageMasqueLot;
import systeme.referenciel.SousListeCaracteristiqueMasqueLot;

/**
 * Ajout d'un lot et toutes ses caract�ristiques associ�es
 * 
 * @author Samuel Gaudey, C�dric Briand, S�bastien Laigre
 */
/**
 * @author cedric.briand
 *
 */
public class AjouterLot extends javax.swing.JPanel {

	/**  */
	private static final long serialVersionUID = 2767141671029651817L;
	private final static Logger logger = Logger.getLogger(AjouterLot.class.getName());
	// Elements graphiques
	private javax.swing.JPanel panel_prelevement;
	private javax.swing.JPanel panel_marques;
	private javax.swing.JPanel panel_operationmarquage;
	private javax.swing.JPanel panel_pathologies;
	private javax.swing.JPanel panel_top;
	private javax.swing.JPanel panel_center;
	private javax.swing.JPanel panel_bottom;
	private javax.swing.JPanel panel_onglets;
	private javax.swing.JPanel panel_boutonsCaracteristiques;
	private javax.swing.JPanel panel_caracteristiques;
	private javax.swing.JPanel panel_boutonsPathologies;
	private javax.swing.JPanel panel_listePathologies;
	private javax.swing.JPanel panel_comboPathologies;
	private javax.swing.JPanel panel_infosLots;
	private javax.swing.JPanel panel_infosOperation;
	private javax.swing.JPanel panel_boutonMarque;
	private javax.swing.JPanel panel_marqueTop;
	private javax.swing.JPanel panel_marqueLeft;
	private javax.swing.JPanel panel_marqueRight;
	private javax.swing.JPanel panel_topOperationMarquage;
	private javax.swing.JPanel panel_bottomOperationMarquage;
	private javax.swing.JPanel panel_topPrelevement;
	private javax.swing.JPanel panel_bottomPrelevement;
	private javax.swing.JTabbedPane TabbedPaneOngletsLots;
	private javax.swing.JTree arbreLots;

	private javax.swing.JScrollPane jScrollPane;
	private javax.swing.JScrollPane jScrollPaneArbreLot;
	private javax.swing.JScrollPane jScrollPane_lot_commentaires;
	private javax.swing.JScrollPane jScrollPaneCarCommentaires;
	private javax.swing.JScrollPane jScrollPaneJlistcar;
	private javax.swing.JScrollPane scrollpane_commentaires;
	private javax.swing.JScrollPane scrollpane_listeMarques;
	private javax.swing.JScrollPane scrollPane_CommentaireMarquage;
	private javax.swing.JScrollPane jScrollPane_commentairesMarque;
	private javax.swing.JScrollPane jScrollPane_commentairesPrelevement;
	private javax.swing.JScrollPane jScrollPane_operationMarquage;
	private javax.swing.JScrollPane jScrollPane_commentairesOperationMarquage;
	private javax.swing.JScrollPane jScrollPane_listePrelevements;

	private javax.swing.JButton button_Modifier;
	private javax.swing.JButton button_a_caracteristique;
	private javax.swing.JButton button_ajouterEchantillon;
	private javax.swing.JButton button_ajouterLot;
	private javax.swing.JButton button_ajouterOperation;
	private javax.swing.JButton button_annuler_caracteristique;
	private javax.swing.JButton button_lotNext;
	private javax.swing.JButton button_lotPrev;
	private javax.swing.JButton button_effacer;
	private javax.swing.JButton button_incrementerOperation;
	private javax.swing.JButton button_opeNext;
	private javax.swing.JButton button_opePrev;
	private javax.swing.JButton button_s_caracteristique;
	private javax.swing.JButton button_supprimerLot;
	private javax.swing.JButton button_AMPathologie;
	private javax.swing.JButton button_SPathologie;
	private javax.swing.JButton button_AnnulerPathologie;
	private javax.swing.JButton button_AMMarque;
	private javax.swing.JButton button_SMarque;
	private javax.swing.JButton button_annulerMarque;
	private javax.swing.JButton button_AMOperationMarquage;
	private javax.swing.JButton button_SOperationMarquage;
	private javax.swing.JButton button_annulerOperationMarquage;
	private javax.swing.JButton button_AMPrelevement;
	private javax.swing.JButton button_SPrelevement;
	private javax.swing.JButton button_annulerPrelevement;
	private javax.swing.ButtonGroup buttonGroup_type_parametre;
	private javax.swing.ButtonGroup buttonGroup_actionMarquage;
	private javax.swing.JRadioButton radio_actionMarquage_lecture;
	private javax.swing.JRadioButton radio_actionMarquage_pose;
	private javax.swing.JRadioButton radio_actionMarquage_retrait;
	private javax.swing.JRadioButton radio_actionMarquage_posemultiple;
	private javax.swing.JRadioButton radio_car_type_qualitatif;
	private javax.swing.JRadioButton radio_car_type_quantitatif;

	private JLabel label_car_commentaires;
	private JLabel label_car_liste;
	private JLabel label_car_methodeObtention;
	private JLabel label_car_parametre;
	private JLabel label_car_precision;
	private JLabel label_car_type;
	private JLabel label_car_valeur;

	private javax.swing.JList<Caracteristique> jlist_car;
	private DefaultListModel<Caracteristique> listmodelcaracteristique;

	private DefaultListModel<Prelevement> listmodelprelevement;
	private javax.swing.JList<Prelevement> jList_listePrelevements;
	private DefaultListModel<PathologieConstatee> listmodelpathologieconstatees;
	private javax.swing.JList<PathologieConstatee> jList_PathologiesConstatees;
	private DefaultListModel<Marquage> listmodelMarquage;
	private javax.swing.JList<Marquage> jList_marquages;
	private DefaultListModel<OperationMarquage> listmodelOperationMarquage;
	private javax.swing.JList<OperationMarquage> jList_operationMarquage;

	private JComboBox<String> combo_car_methodeObtention;
	private JComboBox<RefParametre> combo_car_parametre;
	private JComboBox<RefValeurParametre> combo_car_valeurQualitative;
	private JComboBox<String> combo_lot_methode_obtention;
	private JComboBox<RefDevenir> combo_tr_devenirlot_dev;
	private JComboBox<RefStade> combo_tr_stade_std;
	private DefaultComboBoxModel<RefStade> comboModelstademasque;
	private DefaultComboBoxModel<RefStade> comboModelstadecomplet;
	private JComboBox<RefTaxon> combo_tr_taxon_tax;
	private DefaultComboBoxModel<RefTaxon> comboModeltaxonmasque;
	private DefaultComboBoxModel<RefTaxon> comboModeltaxoncomplet;
	private JComboBox<RefTypeQuantite> combo_tr_typequantitelot_qte;
	private JComboBox<RefPathologie> combo_pathologies;
	private DefaultComboBoxModel<RefPathologie> comboModelpathomasque;
	private DefaultComboBoxModel<RefPathologie> comboModelpathocomplet;
	private JComboBox<RefLocalisation> combo_localisationPathologie;
	private DefaultComboBoxModel<RefLocalisation> comboModellocalisationpathomasque;
	private DefaultComboBoxModel<RefLocalisation> comboModellocalisationpathocomplet;
	private JComboBox<RefImportancePathologie> combo_importancePathologie;
	private JComboBox<OperationMarquage> combo_OperationMarquage;
	private JComboBox<Marque> combo_referencesMarques;
	private JComboBox<RefLocalisation> combo_localisationMarque;
	private JComboBox<RefNatureMarque> combo_natureMarque;
	private JComboBox<String> combo_typePrelevement;
	private JComboBox<RefLocalisation> combo_localisationPrelevement;

	private commun.JTextAreaDataType textarea_car_commentaires;
	private commun.JTextAreaDataType textarea_lot_commentaires;
	private commun.JTextAreaDataType textarea_commentairesPathologies;
	private commun.JTextAreaDataType textarea_CommentaireMarquage;
	private commun.JTextAreaDataType textarea_commentairesMarque;
	private commun.JTextAreaDataType textarea_commentaireOperationMarquage;
	private commun.JTextAreaDataType textarea_commentairesPrelevement;

	private commun.JTextFieldDataType textfielddt_car_precision;
	private commun.JTextFieldDataType textfielddt_car_valeurQuantitative;
	private commun.JTextFieldDataType textfielddt_car_valeurTaille;
	private commun.JTextFieldDataType textfielddt_lot_quantite;
	private commun.JTextFieldDataType textfielddt_refMarque;
	private commun.JTextFieldDataType textfielddt_operationMarquage;
	private javax.swing.JTextField textfield_codePrelevement;
	private javax.swing.JTextField textfield_operateurPrelevement;

	private JLabel label_commentaireslot;
	private JLabel label_devenirlot;
	private JLabel label_methodelot;
	private JLabel label_navOpe;
	private JLabel label_navigLots;
	private JLabel label_operation;
	private JLabel label_quantite;
	private JTextArea resultat;
	private JLabel label_stade;
	private JLabel label_taxon;
	private JLabel label_titre;
	private JLabel label_pathologie;
	private JLabel label_importancePathologie;
	private JLabel label_LocalisationPathologie;
	private JLabel label_listePathologie;
	private JLabel label_commentairesPathologies;
	private JLabel label_marques;
	private JLabel label_refMarque;
	private JLabel label_OperationMarquage;
	private JLabel label_actionMarquage;
	private JLabel label_CommentaireMarquage;
	private JLabel label_localisationMarque;
	private JLabel label_natureMarque;
	private JLabel label_commentairesMarque;
	private JLabel label_operationsMarquage;
	private JLabel label_codeOperationMarquage;
	private JLabel label_commentairesOperationMarquage;
	private JLabel label_codePrelevement;
	private JLabel label_operateurPrelevement;
	private JLabel label_typePrelevement;
	private JLabel label_localisationPrelevement;
	private JLabel label_commentairesPrelevement;
	private JLabel label_listePrelevements;
	private JLabel label_labeffectif;
	private JLabel label_effectif;
	private JLabel label_effectifope;
	private JLabel label_labeleffectifope;

	// Listes pour les combos onglet lots
	private ListeRefTaxon lesTaxons;
	private ListeRefStade lesStades;
	private ListeRefDevenir lesDevenirs;
	private ListeRefTypeQuantite lesTypeQuantite;
	private String[] methodesObtentionLot;

	// Listes pour les combos onglet caracteristiques
	private ListeRefParamQualBio lesParametreQualitatifs;
	private ListeRefParamQuantBio lesParametreQuantitatifs;
	private SousListeRefValeurParametre lesValeursParametre;
	private String[] methodesObtentionCaracteristique;

	// Listes pour les combos onglet taille poids stade
	private ListeRefLocalisation lesLocalisations;
	private ListeRefPathologie lesPathologies;
	private ListeRefImportancePathologie lesimportancespathologie;
	private SousListePathologieConstatee lesPathologiesConstatees;
	private SousListeMarquages lesMarquages;
	private ListeOperationMarquage lesOperationsMarquages;
	private ListeRefNatureMarque lesNaturesMarques;
	private ListeOperationMarquage listeDesOperationsMarquages;
	private ListeRefPrelevement lesPrelevements;
	private SousListePrelevement lesPrelevementsDuLot;
	private Vector<String> vectorLocalisation;
	private Vector<String> vectorPathologies;
	private Vector<String> vectorImportance;
	private Vector<String> vectorNatureMarque;
	private Vector<String> vectorOperationMarquage;
	private Vector<String> vectorPrelevement;

	// Pour l'affichage des lots de l'operation courante
	private Hashtable lesNoeuds;
	private Operation ope; // l'operation de rattachement pour le lot courant
	private Lot lot; // le lot courant
	// private Caracteristique car ; // la caracteristique courante
	private static final String quantiteIndividus = Messages.getString("AjouterLot.0"); //$NON-NLS-1$

	// affichage des �l�ments du masque
	private Masque masque;
	private MasqueLot masquelot;
	private String[] valeurdefaut = new String[13];
	private Boolean[] affichage = new Boolean[20];
	private ListeMasqueLot listemasquelotenbase;
	private SousListeAffichageMasqueLot affichagemasquelotenbase;
	private SousListeCaracteristiqueMasqueLot caracteristiquemasquelotenbase;

	// liste pour stocker les composants correspondant au caracteristiques

	private ArrayList<JComponent> listofcomponent;
	private boolean[] vect_estqualitatif;
	private SousListeRefValeurParametre[] vect_lesvaleurparametre;
	private DefaultTreeModel treeModel;

	/**
	 * Constructeur
	 * 
	 * @param _operation
	 *            l'op�ration � laquelle est rattach� le lot
	 */
	public AjouterLot(Operation _operation) {

		this.ope = _operation;
		this.lot = null;
		// this.car = null ;
		this.chargemasqueenbase();
		// chargement de listemasquelotenbase
		// caracteristiquemasquelotenbase
		// affichagemasquelotenbase
		this.initListesCombos();
		this.initComponents();
		this.chargeListeOperationsMarquage();
		this.chargeMasque();
		this.initCaracteristiquesduMasque();
		this.initInfosLots();

	}

	/**
	 * M�thode de chargement intial attention � lancer apr�s chargemasqueenbase
	 * 
	 * 
	 */
	private void initComponents() {// GEN-BEGIN:initComponents
		java.awt.GridBagConstraints gridBagConstraints;

		this.buttonGroup_type_parametre = new javax.swing.ButtonGroup();
		this.panel_top = new javax.swing.JPanel();
		this.label_titre = new JLabel();
		this.resultat = new JTextArea();
		this.panel_center = new javax.swing.JPanel();
		this.panel_infosOperation = new javax.swing.JPanel();
		this.label_operation = new JLabel();
		this.jScrollPaneArbreLot = new javax.swing.JScrollPane();
		this.arbreLots = new javax.swing.JTree();
		this.panel_onglets = new javax.swing.JPanel();
		this.TabbedPaneOngletsLots = new javax.swing.JTabbedPane();
		this.panel_infosLots = new javax.swing.JPanel();

		this.jScrollPane_lot_commentaires = new javax.swing.JScrollPane();
		this.textarea_lot_commentaires = new commun.JTextAreaDataType();
		this.textfielddt_car_valeurTaille = new commun.JTextFieldDataType();
		this.panel_caracteristiques = new javax.swing.JPanel();
		this.label_car_type = new JLabel();
		this.radio_car_type_quantitatif = new javax.swing.JRadioButton();
		this.radio_car_type_qualitatif = new javax.swing.JRadioButton();
		this.label_car_parametre = new JLabel();
		this.combo_car_parametre = new JComboBox<RefParametre>();
		this.combo_car_valeurQualitative = new JComboBox<RefValeurParametre>();
		this.textfielddt_car_valeurQuantitative = new commun.JTextFieldDataType();
		this.label_car_precision = new JLabel();
		this.label_car_valeur = new JLabel();
		this.textfielddt_car_precision = new commun.JTextFieldDataType();
		this.label_car_methodeObtention = new JLabel();
		this.combo_car_methodeObtention = new JComboBox<String>();
		this.label_car_commentaires = new JLabel();
		this.jScrollPaneCarCommentaires = new javax.swing.JScrollPane();
		this.textarea_car_commentaires = new commun.JTextAreaDataType();
		this.label_car_liste = new JLabel();
		this.jScrollPaneJlistcar = new javax.swing.JScrollPane();
		this.listmodelcaracteristique = new DefaultListModel<>();
		this.jlist_car = new javax.swing.JList<Caracteristique>(listmodelcaracteristique);
		this.panel_boutonsCaracteristiques = new javax.swing.JPanel();
		this.button_a_caracteristique = new javax.swing.JButton();
		this.button_s_caracteristique = new javax.swing.JButton();
		this.button_annuler_caracteristique = new javax.swing.JButton();
		this.panel_pathologies = new javax.swing.JPanel();
		this.panel_marques = new javax.swing.JPanel();
		this.panel_bottom = new javax.swing.JPanel();
		this.button_Modifier = new javax.swing.JButton();
		this.button_effacer = new javax.swing.JButton();
		this.button_supprimerLot = new javax.swing.JButton();
		this.button_ajouterLot = new javax.swing.JButton();
		this.button_ajouterEchantillon = new javax.swing.JButton();
		this.button_incrementerOperation = new javax.swing.JButton();
		this.button_ajouterOperation = new javax.swing.JButton();
		this.button_lotPrev = new javax.swing.JButton();
		this.label_navigLots = new JLabel();
		this.button_lotNext = new javax.swing.JButton();
		this.button_opePrev = new javax.swing.JButton();
		this.label_navOpe = new JLabel();
		this.button_opeNext = new javax.swing.JButton();
		this.label_labeffectif = new JLabel();
		this.label_effectif = new JLabel();
		this.label_labeleffectifope = new JLabel();
		this.label_effectifope = new JLabel();

		// pathologies
		this.combo_pathologies = new JComboBox<RefPathologie>();
		this.combo_localisationPathologie = new JComboBox<RefLocalisation>();
		this.combo_importancePathologie = new JComboBox<RefImportancePathologie>();
		this.jScrollPane = new javax.swing.JScrollPane();
		this.listmodelpathologieconstatees = new DefaultListModel<PathologieConstatee>();
		this.jList_PathologiesConstatees = new javax.swing.JList<>(listmodelpathologieconstatees);
		this.panel_boutonsPathologies = new javax.swing.JPanel();
		this.panel_listePathologies = new javax.swing.JPanel();
		this.panel_comboPathologies = new javax.swing.JPanel();
		this.button_AMPathologie = new javax.swing.JButton();
		this.button_SPathologie = new javax.swing.JButton();
		this.button_AnnulerPathologie = new javax.swing.JButton();
		this.label_pathologie = new JLabel();
		this.label_LocalisationPathologie = new JLabel();
		this.label_listePathologie = new JLabel();
		this.label_importancePathologie = new JLabel();

		this.textarea_commentairesPathologies = new commun.JTextAreaDataType();
		this.label_commentairesPathologies = new JLabel();
		this.scrollpane_commentaires = new javax.swing.JScrollPane();

		// marques
		this.label_marques = new JLabel();
		this.scrollpane_listeMarques = new javax.swing.JScrollPane();
		this.listmodelMarquage = new DefaultListModel<Marquage>();
		this.jList_marquages = new javax.swing.JList<Marquage>(listmodelMarquage);
		this.label_actionMarquage = new JLabel();
		this.buttonGroup_actionMarquage = new ButtonGroup();
		this.radio_actionMarquage_lecture = new JRadioButton();
		this.radio_actionMarquage_pose = new JRadioButton();
		this.radio_actionMarquage_retrait = new JRadioButton();
		this.radio_actionMarquage_posemultiple = new JRadioButton();
		this.label_CommentaireMarquage = new JLabel();
		this.textarea_CommentaireMarquage = new JTextAreaDataType();
		this.scrollPane_CommentaireMarquage = new javax.swing.JScrollPane();
		this.label_refMarque = new JLabel();
		this.combo_referencesMarques = new JComboBox<Marque>();
		this.textfielddt_refMarque = new JTextFieldDataType();
		this.label_OperationMarquage = new JLabel();
		this.combo_OperationMarquage = new JComboBox<OperationMarquage>();
		this.label_localisationMarque = new JLabel();
		this.combo_localisationMarque = new JComboBox<RefLocalisation>();
		this.label_natureMarque = new JLabel();
		this.combo_natureMarque = new JComboBox<RefNatureMarque>();
		this.label_commentairesMarque = new JLabel();
		this.jScrollPane_commentairesMarque = new javax.swing.JScrollPane();
		this.textarea_commentairesMarque = new JTextAreaDataType();
		this.panel_boutonMarque = new javax.swing.JPanel();
		this.button_AMMarque = new javax.swing.JButton();
		this.button_SMarque = new javax.swing.JButton();
		this.button_annulerMarque = new javax.swing.JButton();

		// pr�levements
		panel_topPrelevement = new javax.swing.JPanel();
		panel_bottomPrelevement = new javax.swing.JPanel();
		this.panel_prelevement = new JPanel();
		this.label_codePrelevement = new JLabel();
		this.textfield_codePrelevement = new commun.JTextFieldDataType();
		this.label_operateurPrelevement = new JLabel();
		this.textfield_operateurPrelevement = new commun.JTextFieldDataType();
		this.label_typePrelevement = new JLabel();
		this.combo_typePrelevement = new JComboBox<String>();
		this.label_localisationPrelevement = new JLabel();
		this.combo_localisationPrelevement = new JComboBox<RefLocalisation>();
		this.label_commentairesPrelevement = new JLabel();
		this.jScrollPane_commentairesPrelevement = new javax.swing.JScrollPane();
		this.textarea_commentairesPrelevement = new commun.JTextAreaDataType();
		this.label_listePrelevements = new JLabel();
		this.jScrollPane_listePrelevements = new JScrollPane();
		this.listmodelprelevement = new DefaultListModel<Prelevement>();
		this.jList_listePrelevements = new javax.swing.JList<Prelevement>(listmodelprelevement);
		this.button_AMPrelevement = new javax.swing.JButton();
		this.button_SPrelevement = new javax.swing.JButton();
		this.button_annulerPrelevement = new javax.swing.JButton();

		// op�ration de marquage
		this.label_operationsMarquage = new JLabel();
		this.jScrollPane_operationMarquage = new javax.swing.JScrollPane();
		this.listmodelOperationMarquage = new DefaultListModel<OperationMarquage>();
		this.jList_operationMarquage = new javax.swing.JList<OperationMarquage>(listmodelOperationMarquage);
		this.label_codeOperationMarquage = new JLabel();
		this.textfielddt_operationMarquage = new JTextFieldDataType();
		this.label_commentairesOperationMarquage = new JLabel();
		this.jScrollPane_commentairesOperationMarquage = new javax.swing.JScrollPane();
		this.textarea_commentaireOperationMarquage = new JTextAreaDataType();
		this.button_AMOperationMarquage = new javax.swing.JButton();
		this.button_SOperationMarquage = new javax.swing.JButton();
		this.button_annulerOperationMarquage = new javax.swing.JButton();
		this.panel_topOperationMarquage = new javax.swing.JPanel();
		this.panel_bottomOperationMarquage = new javax.swing.JPanel();

		this.panel_operationmarquage = new javax.swing.JPanel();

		this.setLayout(new java.awt.BorderLayout());
		this.setBackground(new java.awt.Color(255, 255, 255));
		this.setPreferredSize(new java.awt.Dimension(800, 600));
		this.panel_top.setLayout(new java.awt.BorderLayout());

		this.label_titre.setBackground(new java.awt.Color(0, 51, 153));
		this.label_titre.setFont(new java.awt.Font("Dialog", 1, 14)); //$NON-NLS-1$
		this.label_titre.setForeground(new java.awt.Color(255, 255, 255));
		this.label_titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

		this.label_titre.setOpaque(true);
		this.panel_top.add(this.label_titre, java.awt.BorderLayout.CENTER);

		this.resultat.setAlignmentX(javax.swing.SwingConstants.CENTER);

		// this.resultat.setBackground(new java.awt.Color(255, 255, 255));
		this.resultat.setForeground(Color.RED);
		this.panel_top.add(this.resultat, java.awt.BorderLayout.SOUTH);

		this.add(this.panel_top, java.awt.BorderLayout.NORTH);

		this.panel_center.setLayout(new java.awt.BorderLayout());

		this.panel_center.setBackground(new java.awt.Color(230, 230, 230));
		this.panel_center.setPreferredSize(new java.awt.Dimension(700, 400));

		///////////////////////////////////////////////////////////////
		// ARBRE
		////////////////////////////////////////////////////////////////////

		this.panel_infosOperation.setLayout(new java.awt.GridBagLayout());
		this.panel_infosOperation.setPreferredSize(new Dimension(700, 210));
		this.label_operation.setText(Messages.getString("AjouterLot.3")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 10);
		this.panel_infosOperation.add(this.label_operation, gridBagConstraints);

		this.jScrollPaneArbreLot.setPreferredSize(new Dimension(600, 200)); // changer
		// ici

		this.arbreLots.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent me) {
				AjouterLot.this.arbreLotsselectedValueChanged(me);
			}
		});

		// arbreLots.setFont(new java.awt.Font("Dialog", 1, 10));

		this.arbreLots.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				liste_stationsMouseClicked(evt);
			}

			private void liste_stationsMouseClicked(MouseEvent evt) {
				Float eff = getEffectif();
				if (eff != null)
					AjouterLot.this.label_effectif.setText(eff.toString());
			}
		});

		this.jScrollPaneArbreLot.setViewportView(this.arbreLots);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridheight = 5;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
		this.panel_infosOperation.add(this.jScrollPaneArbreLot, gridBagConstraints);

		label_labeffectif.setPreferredSize(new Dimension(190, 15));
		label_labeffectif.setText(Messages.getString("AjouterLot.EffectifDeLechantillon"));
		label_labeffectif.setFont(new java.awt.Font("Dialog", 1, 10)); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHWEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
		this.panel_infosOperation.add(this.label_labeffectif, gridBagConstraints);

		label_effectif.setText("");
		label_effectif.setFont(new java.awt.Font("Dialog", 1, 10)); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
		this.panel_infosOperation.add(this.label_effectif, gridBagConstraints);

		label_labeleffectifope.setPreferredSize(new Dimension(190, 15));
		label_labeleffectifope.setText(Messages.getString("AjouterLot.Effectifope"));
		label_labeleffectifope.setFont(new java.awt.Font("Dialog", 1, 10)); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHWEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
		label_labeleffectifope.setToolTipText(Messages.getString("AjouterLot.TooltipEffectifope"));
		this.panel_infosOperation.add(this.label_labeleffectifope, gridBagConstraints);

		label_effectifope.setText("");
		label_effectifope.setFont(new java.awt.Font("Dialog", 1, 10)); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
		this.panel_infosOperation.add(this.label_effectifope, gridBagConstraints);

		this.panel_center.add(this.panel_infosOperation, java.awt.BorderLayout.NORTH);

		this.panel_onglets.setLayout(new java.awt.GridBagLayout());

		this.panel_onglets.setBackground(new java.awt.Color(230, 230, 230));
		this.panel_onglets.setPreferredSize(new java.awt.Dimension(700, 380));
		this.TabbedPaneOngletsLots.setPreferredSize(new java.awt.Dimension(700, 370));
		this.panel_infosLots.setLayout(new java.awt.GridBagLayout());
		this.panel_infosLots.setPreferredSize(new java.awt.Dimension(600, 400));

		// this.panel_infosLots.addMouseListener(new MouseAdapter() {
		// @Override
		// public void mouseClicked(MouseEvent e) {
		// panel_infosLots.requestFocusInWindow();
		// }
		// });

		// le combo est initialis� plus loin avec les valeurs du masque
		// DefaultComboBoxModel<RefTaxon> comboModeltaxon = new
		// DefaultComboBoxModel<RefTaxon>();
		// int i = 0;
		// while (i < this.lesTaxons.size()) {
		// comboModeltaxon.addElement((RefTaxon) this.lesTaxons.get(i));
		// i++;
		// }
		// this.combo_tr_taxon_tax.setModel(comboModeltaxon);

		////////////////////////////////////////////
		// Panel info_lots
		//////////////////////////////////////////
		////////////////////////////////////
		/// TAXON
		////////////////////////////////////
		this.label_taxon = new JLabel();
		this.label_taxon.setText(Messages.getString("AjouterLot.4")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);// tlbr
		this.panel_infosLots.add(this.label_taxon, gridBagConstraints);

		this.combo_tr_taxon_tax = new JComboBox<RefTaxon>();
		this.combo_tr_taxon_tax.setPreferredSize(new java.awt.Dimension(200, 24));

		this.combo_tr_taxon_tax.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.tr_taxon_taxActionPerformed(evt);
			}
		});

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new Insets(0, 0, 5, 5);// tlbr
		// top left bottom right

		this.panel_infosLots.add(this.combo_tr_taxon_tax, gridBagConstraints);
		this.initcombotaxon(false);// initialisation � partir des valeurs du
		// masque (complet = false)
		combo_tr_taxon_tax.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_tr_taxon_tax.isEnabled()) {
						combo_tr_taxon_tax.setEnabled(false);
						label_taxon.setForeground(SystemColor.textInactiveText);
					} else {
						combo_tr_taxon_tax.setEnabled(true);
						label_taxon.setForeground(SystemColor.black);
					}
				}
			}
		});

		//////////////////////////////////////////////////////
		// STADE
		////////////////////////////////////////////////////
		this.label_stade = new JLabel();

		this.label_stade.setText(Messages.getString("AjouterLot.5")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 10);
		this.panel_infosLots.add(this.label_stade, gridBagConstraints);

		this.combo_tr_stade_std = new JComboBox<RefStade>();
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new Insets(5, 0, 5, 5);// tlbr
		this.combo_tr_stade_std.setPreferredSize(new java.awt.Dimension(200, 25));
		this.initcombostade(false);// initialisation � partir des valeurs du
		// masque (complet = false)
		this.panel_infosLots.add(this.combo_tr_stade_std, gridBagConstraints);

		combo_tr_stade_std.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_tr_stade_std.isEnabled()) {
						combo_tr_stade_std.setEnabled(false);
						label_stade.setForeground(SystemColor.textInactiveText);
					} else {
						combo_tr_stade_std.setEnabled(true);
						label_stade.setForeground(SystemColor.black);
					}
				}
			}
		});

		////////////////////////////////////
		// quantite ou effectif
		////////////////////////////////////
		this.label_quantite = new JLabel();
		this.label_quantite.setText(Messages.getString("AjouterLot.6")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 10);
		this.panel_infosLots.add(this.label_quantite, gridBagConstraints);

		this.textfielddt_lot_quantite = new commun.JTextFieldDataType();
		textfielddt_lot_quantite.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (textfielddt_lot_quantite.isEnabled()) {
						textfielddt_lot_quantite.setEnabled(false);
						label_quantite.setForeground(SystemColor.textInactiveText);
					} else {
						textfielddt_lot_quantite.setEnabled(true);
						label_quantite.setForeground(SystemColor.black);
					}
				}
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(5, 0, 5, 5);// tlbr
		this.textfielddt_lot_quantite.setColumns(4);
		this.panel_infosLots.add(this.textfielddt_lot_quantite, gridBagConstraints);
		DefaultComboBoxModel<RefTypeQuantite> comboModelTypeQuantite = new DefaultComboBoxModel<RefTypeQuantite>();
		for (int i = 0; i < this.lesTypeQuantite.size(); i++) {
			comboModelTypeQuantite.addElement((RefTypeQuantite) this.lesTypeQuantite.get(i));
		}

		this.combo_tr_typequantitelot_qte = new JComboBox<RefTypeQuantite>();
		this.combo_tr_typequantitelot_qte.setModel(comboModelTypeQuantite);
		this.combo_tr_typequantitelot_qte.setPreferredSize(new java.awt.Dimension(200, 25));
		this.combo_tr_typequantitelot_qte.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.tr_typequantitelot_qteActionPerformed(evt);
			}
		});

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(5, 5, 5, 0);// tlbr
		this.panel_infosLots.add(this.combo_tr_typequantitelot_qte, gridBagConstraints);

		combo_tr_typequantitelot_qte.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_tr_typequantitelot_qte.isEnabled()) {
						combo_tr_typequantitelot_qte.setEnabled(false);
						// label_quantite.setForeground(SystemColor.textInactiveText);
					} else {
						combo_tr_typequantitelot_qte.setEnabled(true);
						// label_quantite.setForeground(SystemColor.black);
					}
				}
			}
		});

		////////////////////////////////////
		// M�thode obtention
		////////////////////////////////////

		this.label_methodelot = new JLabel();
		this.label_methodelot.setText(Messages.getString("AjouterLot.8")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 10);
		this.panel_infosLots.add(this.label_methodelot, gridBagConstraints);

		DefaultComboBoxModel<String> comboModelMethodeObtention = new DefaultComboBoxModel<String>();
		for (String lamethode : methodesObtentionLot) {
			comboModelMethodeObtention.addElement(lamethode);
		}
		this.combo_lot_methode_obtention = new JComboBox<String>();
		this.combo_lot_methode_obtention.setModel(comboModelMethodeObtention);
		this.combo_lot_methode_obtention.setPreferredSize(new java.awt.Dimension(150, 24));
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.insets = new Insets(5, 0, 5, 5);// tlbr
		this.panel_infosLots.add(this.combo_lot_methode_obtention, gridBagConstraints);

		combo_lot_methode_obtention.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_lot_methode_obtention.isEnabled()) {
						combo_lot_methode_obtention.setEnabled(false);
						label_methodelot.setForeground(SystemColor.textInactiveText);
					} else {
						combo_lot_methode_obtention.setEnabled(true);
						label_methodelot.setForeground(SystemColor.black);
					}
				}
			}
		});
		////////////////////////////////////
		// Devenir
		////////////////////////////////////

		this.label_devenirlot = new JLabel();
		this.label_devenirlot.setText(Messages.getString("AjouterLot.7")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 10);
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		this.panel_infosLots.add(this.label_devenirlot, gridBagConstraints);
		DefaultComboBoxModel<RefDevenir> comboModelDevenir = new DefaultComboBoxModel<RefDevenir>();
		for (int i = 0; i < this.lesDevenirs.size(); i++) {
			comboModelDevenir.addElement((RefDevenir) this.lesDevenirs.get(i));
		}
		this.combo_tr_devenirlot_dev = new JComboBox<RefDevenir>();
		this.combo_tr_devenirlot_dev.setModel(comboModelDevenir);
		this.combo_tr_devenirlot_dev.setPreferredSize(new java.awt.Dimension(250, 25));
		// this.combo_tr_devenirlot_dev.setPrototypeDisplayValue((RefDevenir)
		// this.lesDevenirs.get(1));
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.insets = new Insets(5, 0, 5, 5);// tlbr
		this.panel_infosLots.add(this.combo_tr_devenirlot_dev, gridBagConstraints);

		combo_tr_devenirlot_dev.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_tr_devenirlot_dev.isEnabled()) {
						combo_tr_devenirlot_dev.setEnabled(false);
						label_devenirlot.setForeground(SystemColor.textInactiveText);
					} else {
						combo_tr_devenirlot_dev.setEnabled(true);
						label_devenirlot.setForeground(SystemColor.black);
					}
				}
			}
		});

		////////////////////////////////////
		// Commentaires
		////////////////////////////////////

		this.label_commentaireslot = new JLabel();
		this.label_commentaireslot.setText(Messages.getString("AjouterLot.9")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
		gridBagConstraints.insets = new Insets(5, 0, 5, 10);
		this.panel_infosLots.add(this.label_commentaireslot, gridBagConstraints);

		label_commentaireslot.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (jScrollPane_lot_commentaires.isEnabled()) {
						jScrollPane_lot_commentaires.setEnabled(false);
						textarea_lot_commentaires.setEnabled(false);
						label_commentaireslot.setForeground(SystemColor.textInactiveText);
					} else {
						jScrollPane_lot_commentaires.setEnabled(true);
						textarea_lot_commentaires.setEnabled(true);
						label_commentaireslot.setForeground(SystemColor.black);
					}
				}
			}
		});

		textarea_lot_commentaires.setTabSize(6);
		this.textarea_lot_commentaires.setColumns(40);
		this.textarea_lot_commentaires.setLineWrap(true);
		this.textarea_lot_commentaires.setRows(2);
		this.jScrollPane_lot_commentaires.setViewportView(textarea_lot_commentaires);
		GridBagConstraints gbc_textarea_lot_commentaires = new GridBagConstraints();
		gbc_textarea_lot_commentaires.gridwidth = 3;
		gbc_textarea_lot_commentaires.insets = new Insets(0, 0, 5, 5);
		gbc_textarea_lot_commentaires.gridx = 1;
		gbc_textarea_lot_commentaires.gridy = 4;
		gbc_textarea_lot_commentaires.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gbc_textarea_lot_commentaires.anchor = java.awt.GridBagConstraints.WEST;
		this.panel_infosLots.add(this.jScrollPane_lot_commentaires, gbc_textarea_lot_commentaires);
		this.TabbedPaneOngletsLots.addTab(Messages.getString("AjouterLot.10"), this.panel_infosLots); //$NON-NLS-1$

		/// ajout d'une r�gle de parcours

		// panel_infosLots.setFocusTraversalPolicy(new FocusTraversalOnArray(new
		// Component[] { combo_tr_taxon_tax,
		// combo_tr_stade_std, textfielddt_lot_quantite,
		// combo_tr_typequantitelot_qte, combo_lot_methode_obtention,
		// combo_tr_devenirlot_dev, textarea_lot_commentaires }));

		/*
		 * ci dessous permet de se ballader avec entr�e mais on ne peut plus
		 * utiliser entr�e comme validation
		 */
		/*
		 * Set forwardKeys =
		 * getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS);
		 * Set newForwardKeys = new HashSet(forwardKeys);
		 * newForwardKeys.add(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0));
		 * setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,
		 * newForwardKeys);
		 */

		// ci dessous il ne semble pas qu'on puisse utiliser ce truc
		// jScrollPane_lot_commentaires.addMouseListener(new
		// java.awt.event.MouseAdapter() {
		// public void mouseClicked(java.awt.event.MouseEvent e) {
		// if (e.getClickCount() == 2 && !e.isConsumed()) {
		// e.consume();
		// if (jScrollPane_lot_commentaires.isEnabled()) {
		// jScrollPane_lot_commentaires.setEnabled(false);
		// textarea_lot_commentaires.setEnabled(false);
		// label_commentaireslot.setForeground(SystemColor.textInactiveText);
		// } else {
		// jScrollPane_lot_commentaires.setEnabled(true);
		// textarea_lot_commentaires.setEnabled(true);
		// label_commentaireslot.setForeground(SystemColor.black);
		// }
		// }
		// }
		// });

		////////////////////////////////////////////
		// Panel CARACTERISTIQUES
		//////////////////////////////////////////

		this.panel_caracteristiques.setLayout(new java.awt.GridBagLayout());

		this.panel_caracteristiques.setMinimumSize(new java.awt.Dimension(340, 134));
		this.panel_caracteristiques.setPreferredSize(new java.awt.Dimension(600, 400));
		this.label_car_type.setText(Messages.getString("AjouterLot.29")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		this.panel_caracteristiques.add(this.label_car_type, gridBagConstraints);

		this.radio_car_type_quantitatif.setFont(new java.awt.Font("Dialog", 0, 12)); //$NON-NLS-1$
		this.radio_car_type_quantitatif.setText(Messages.getString("AjouterLot.31")); //$NON-NLS-1$
		this.buttonGroup_type_parametre.add(this.radio_car_type_quantitatif);
		this.radio_car_type_quantitatif.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.car_type_quantitatifActionPerformed(evt);
			}
		});

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		this.panel_caracteristiques.add(this.radio_car_type_quantitatif, gridBagConstraints);

		this.radio_car_type_qualitatif.setFont(new java.awt.Font("Dialog", 0, 12)); //$NON-NLS-1$
		this.radio_car_type_qualitatif.setText(Messages.getString("AjouterLot.33")); //$NON-NLS-1$
		this.buttonGroup_type_parametre.add(this.radio_car_type_qualitatif);
		this.radio_car_type_qualitatif.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.car_type_qualitatifActionPerformed(evt);
			}
		});

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		this.panel_caracteristiques.add(this.radio_car_type_qualitatif, gridBagConstraints);

		this.label_car_parametre.setText(Messages.getString("AjouterLot.34")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		this.panel_caracteristiques.add(this.label_car_parametre, gridBagConstraints);

		this.combo_car_parametre.setEnabled(false);
		this.combo_car_parametre.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.car_parametreActionPerformed(evt);
			}
		});
		this.combo_car_parametre.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				AjouterLot.this.chargeListeRefValeurParametre();
			}
		});

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		this.panel_caracteristiques.add(this.combo_car_parametre, gridBagConstraints);

		this.label_car_valeur.setText(Messages.getString("AjouterLot.36")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		this.panel_caracteristiques.add(this.label_car_valeur, gridBagConstraints);

		this.combo_car_valeurQualitative.setEnabled(false);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		this.panel_caracteristiques.add(this.combo_car_valeurQualitative, gridBagConstraints);

		this.textfielddt_car_valeurQuantitative.setColumns(10);
		this.textfielddt_car_valeurQuantitative.setEnabled(false);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(4, 2, 4, 2);
		this.panel_caracteristiques.add(this.textfielddt_car_valeurQuantitative, gridBagConstraints);

		this.label_car_precision.setText(Messages.getString("AjouterLot.35")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		this.panel_caracteristiques.add(this.label_car_precision, gridBagConstraints);

		this.textfielddt_car_precision.setColumns(4);
		this.textfielddt_car_precision.addCaretListener(new javax.swing.event.CaretListener() {
			public void caretUpdate(javax.swing.event.CaretEvent evt) {
				AjouterLot.this.car_precisionCaretUpdate(evt);
			}
		});

		textfielddt_car_precision.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (textfielddt_car_precision.isEnabled()) {
						textfielddt_car_precision.setEnabled(false);
						label_car_precision.setForeground(SystemColor.textInactiveText);
					} else {
						textfielddt_car_precision.setEnabled(true);
						label_car_precision.setForeground(SystemColor.black);
					}
				}
			}
		});

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		this.panel_caracteristiques.add(this.textfielddt_car_precision, gridBagConstraints);

		this.label_car_methodeObtention.setText(Messages.getString("AjouterLot.37")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		this.panel_caracteristiques.add(this.label_car_methodeObtention, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		this.panel_caracteristiques.add(this.combo_car_methodeObtention, gridBagConstraints);

		combo_car_methodeObtention.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_car_methodeObtention.isEnabled()) {
						combo_car_methodeObtention.setEnabled(false);
						label_car_methodeObtention.setForeground(SystemColor.textInactiveText);
					} else {
						combo_car_methodeObtention.setEnabled(true);
						label_car_methodeObtention.setForeground(SystemColor.black);
					}
				}
			}
		});

		this.label_car_commentaires.setText(Messages.getString("AjouterLot.38")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		this.panel_caracteristiques.add(this.label_car_commentaires, gridBagConstraints);

		this.jScrollPaneCarCommentaires.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.jScrollPaneCarCommentaires.setMinimumSize(new java.awt.Dimension(400, 48));
		this.jScrollPaneCarCommentaires.setPreferredSize(new java.awt.Dimension(453, 48));
		this.textarea_car_commentaires.setColumns(50);
		this.textarea_car_commentaires.setLineWrap(true);
		this.textarea_car_commentaires.setRows(3);
		this.jScrollPaneCarCommentaires.setViewportView(this.textarea_car_commentaires);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		this.panel_caracteristiques.add(this.jScrollPaneCarCommentaires, gridBagConstraints);

		textarea_car_commentaires.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (textarea_car_commentaires.isEnabled()) {
						textarea_car_commentaires.setEnabled(false);
						label_car_commentaires.setForeground(SystemColor.textInactiveText);
					} else {
						textarea_car_commentaires.setEnabled(true);
						label_car_commentaires.setForeground(SystemColor.black);
					}
				}
			}
		});

		this.label_car_liste.setText(Messages.getString("AjouterLot.39")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		this.panel_caracteristiques.add(this.label_car_liste, gridBagConstraints);

		this.jScrollPaneJlistcar.setMinimumSize(new java.awt.Dimension(400, 48));
		this.jScrollPaneJlistcar.setPreferredSize(new java.awt.Dimension(553, 48));
		this.jScrollPaneJlistcar.setAutoscrolls(true);
		this.jlist_car.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		this.jlist_car.setVisibleRowCount(3);
		this.jlist_car.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
			public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
				AjouterLot.this.car_listeValueChanged(evt);
			}
		});
		// pour d�selectionner en double clic
		jlist_car.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					jlist_car.clearSelection();
				}
			}
		});

		this.jScrollPaneJlistcar.setViewportView(this.jlist_car);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
		this.panel_caracteristiques.add(this.jScrollPaneJlistcar, gridBagConstraints);

		this.button_a_caracteristique.setFont(new java.awt.Font("Dialog", 1, 10)); //$NON-NLS-1$
		this.button_a_caracteristique.setText(Messages.getString("AjouterLot.41")); //$NON-NLS-1$
		this.button_a_caracteristique.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.a_caracteristiqueActionPerformed(evt);
			}
		});

		this.panel_boutonsCaracteristiques.add(this.button_a_caracteristique);

		this.button_s_caracteristique.setFont(new java.awt.Font("Dialog", 1, 10)); //$NON-NLS-1$
		this.button_s_caracteristique.setText(Messages.getString("AjouterLot.43")); //$NON-NLS-1$
		this.button_s_caracteristique.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.s_caracteristiqueActionPerformed(evt);
			}
		});

		this.panel_boutonsCaracteristiques.add(this.button_s_caracteristique);

		this.button_annuler_caracteristique.setFont(new java.awt.Font("Dialog", 1, 10)); //$NON-NLS-1$
		this.button_annuler_caracteristique.setText(Messages.getString("AjouterLot.45")); //$NON-NLS-1$
		this.button_annuler_caracteristique.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.annuler_caracteristiqueActionPerformed(evt);
			}
		});

		this.panel_boutonsCaracteristiques.add(this.button_annuler_caracteristique);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.gridwidth = 4;
		this.panel_caracteristiques.add(this.panel_boutonsCaracteristiques, gridBagConstraints);

		this.TabbedPaneOngletsLots.addTab(Messages.getString("AjouterLot.50"), this.panel_caracteristiques);

		this.TabbedPaneOngletsLots.addTab(Messages.getString("AjouterLot.51"), this.panel_pathologies);

		this.TabbedPaneOngletsLots.addTab(Messages.getString("AjouterLot.52"), this.panel_marques);

		this.TabbedPaneOngletsLots.addTab(Messages.getString("AjouterLot.61"), this.panel_prelevement);

		this.TabbedPaneOngletsLots.addTab(Messages.getString("AjouterLot.60"), this.panel_operationmarquage);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		gridBagConstraints.gridheight = java.awt.GridBagConstraints.REMAINDER;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.ipadx = 90;
		gridBagConstraints.ipady = 120;
		this.panel_onglets.add(this.TabbedPaneOngletsLots, gridBagConstraints);

		this.panel_center.add(this.panel_onglets, java.awt.BorderLayout.CENTER);

		this.add(this.panel_center, java.awt.BorderLayout.CENTER);

		////////////////////////////////////////////
		/// PATHOLOGIES
		////////////////////////////////////////////

		this.panel_pathologies.setLayout(new GridBagLayout());

		this.panel_listePathologies.setLayout(new GridBagLayout());

		label_listePathologie.setText(Messages.getString("AjouterLot.100"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_listePathologies.add(label_listePathologie, gridBagConstraints);

		this.jScrollPane.setPreferredSize(new java.awt.Dimension(290, 160));
		this.jList_PathologiesConstatees.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		// quand on selectionne une patho dans l'arbre, il faut pouvoir
		// d�selectionner un des combos
		jList_PathologiesConstatees.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					jList_PathologiesConstatees.clearSelection();
				}
			}
		});
		this.jScrollPane.setViewportView(jList_PathologiesConstatees);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		panel_listePathologies.add(jScrollPane, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(3, 20, 3, 20);
		this.panel_pathologies.add(panel_listePathologies, gridBagConstraints);

		this.panel_comboPathologies.setLayout(new GridBagLayout());

		label_pathologie.setText(Messages.getString("AjouterLot.98"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_comboPathologies.add(label_pathologie, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 6, 5);

		this.panel_comboPathologies.add(combo_pathologies, gridBagConstraints);

		combo_pathologies.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_pathologies.isEnabled()) {
						combo_pathologies.setEnabled(false);
						label_pathologie.setForeground(SystemColor.textInactiveText);
					} else {
						combo_pathologies.setEnabled(true);
						label_pathologie.setForeground(SystemColor.black);
					}
				}
			}
		});
		// this.combo_pathologies.setModel(new
		// DefaultComboBoxModel(this.lesPathologies.toArray()));
		this.initcombopathologie(false);// initialisation � partir des valeurs
										// du
		// masque (complet = false)

		label_LocalisationPathologie.setText(Messages.getString("AjouterLot.99"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_comboPathologies.add(label_LocalisationPathologie, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 6, 5);

		this.panel_comboPathologies.add(combo_localisationPathologie, gridBagConstraints);
		this.initcombolocalisationpatho(false);
		combo_localisationPathologie.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_localisationPathologie.isEnabled()) {
						combo_localisationPathologie.setEnabled(false);
						label_LocalisationPathologie.setForeground(SystemColor.textInactiveText);
					} else {
						combo_localisationPathologie.setEnabled(true);
						label_LocalisationPathologie.setForeground(SystemColor.black);
					}
				}
			}
		});
		// importance pathologie
		label_importancePathologie.setText("Importance");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_comboPathologies.add(label_importancePathologie, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 6, 5);
		this.combo_importancePathologie.setModel(new DefaultComboBoxModel(this.lesimportancespathologie.toArray()));
		this.panel_comboPathologies.add(combo_importancePathologie, gridBagConstraints);

		combo_importancePathologie.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_importancePathologie.isEnabled()) {
						combo_importancePathologie.setEnabled(false);
						label_LocalisationPathologie.setForeground(SystemColor.textInactiveText);
					} else {
						combo_importancePathologie.setEnabled(true);
						label_LocalisationPathologie.setForeground(SystemColor.black);
					}
				}
			}
		});
		label_commentairesPathologies.setText(Messages.getString("AjouterLot.101"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_comboPathologies.add(label_commentairesPathologies, gridBagConstraints);

		scrollpane_commentaires.setPreferredSize(new Dimension(250, 60));
		scrollpane_commentaires.setViewportView(textarea_commentairesPathologies);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_comboPathologies.add(scrollpane_commentaires, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_pathologies.add(panel_comboPathologies, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_pathologies.add(panel_boutonsPathologies, gridBagConstraints);

		this.button_AMPathologie.setFont(new java.awt.Font("Dialog", 1, 10)); //$NON-NLS-1$
		this.button_AMPathologie.setText(Messages.getString("AjouterLot.16")); //$NON-NLS-1$

		this.button_SPathologie.setFont(new java.awt.Font("Dialog", 1, 10)); //$NON-NLS-1$
		this.button_SPathologie.setText(Messages.getString("AjouterLot.43")); //$NON-NLS-1$

		this.button_AnnulerPathologie.setFont(new java.awt.Font("Dialog", 1, 10)); //$NON-NLS-1$
		this.button_AnnulerPathologie.setText(Messages.getString("AjouterLot.18")); //$NON-NLS-1$

		panel_boutonsPathologies.add(button_AMPathologie);
		panel_boutonsPathologies.add(button_SPathologie);
		panel_boutonsPathologies.add(button_AnnulerPathologie);

		jList_PathologiesConstatees.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				listePathologiesMouseClicked(evt);
			}

			private void listePathologiesMouseClicked(MouseEvent evt) {
				try {
					affichePathologies();
				} catch (Exception ex) {
					Logger.getLogger(AMSPeriodeFonctionnement.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});

		button_AMPathologie.setToolTipText(Messages.getString("AjouterLot.102"));
		button_AMPathologie.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterModifierPathologie();
			}
		});

		button_SPathologie.setToolTipText(Messages.getString("AjouterLot.103"));
		button_SPathologie.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerPathologie();
			}
		});

		button_AnnulerPathologie.setToolTipText(Messages.getString("AjouterLot.104"));
		button_AnnulerPathologie.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annulerPathologie();
			}
		});

		////////////////////////////////////////////
		/// MARQUES
		////////////////////////////////////////////

		this.panel_marques.setLayout(new GridBagLayout());

		panel_marqueTop = new JPanel();
		panel_marqueTop.setLayout(new GridBagLayout());
		panel_marqueLeft = new JPanel();
		panel_marqueLeft.setLayout(new GridBagLayout());
		panel_marqueRight = new JPanel();
		panel_marqueRight.setLayout(new GridBagLayout());

		label_marques.setText(Messages.getString("AjouterLot.105"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_marqueLeft.add(label_marques, gridBagConstraints);

		scrollpane_listeMarques.setPreferredSize(new Dimension(300, 80));
		this.jList_marquages.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		scrollpane_listeMarques.setViewportView(jList_marquages);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 4;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_marqueLeft.add(scrollpane_listeMarques, gridBagConstraints);

		this.label_actionMarquage.setText(Messages.getString("AjouterLot.111"));
		this.label_actionMarquage.setToolTipText(Messages.getString("AjouterLot.132"));

		this.radio_actionMarquage_lecture.setFont(new java.awt.Font("Dialog", 0, 12)); //$NON-NLS-1$
		this.radio_actionMarquage_lecture.setText(Messages.getString("AjouterLot.129")); //$NON-NLS-1$
		this.buttonGroup_actionMarquage.add(this.radio_actionMarquage_lecture);
		this.radio_actionMarquage_lecture.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.lectureRetraitActionperformed(evt);
			}
		});

		this.radio_actionMarquage_pose.setFont(new java.awt.Font("Dialog", 0, 12)); //$NON-NLS-1$
		this.radio_actionMarquage_pose.setText(Messages.getString("AjouterLot.130")); //$NON-NLS-1$
		this.buttonGroup_actionMarquage.add(this.radio_actionMarquage_pose);
		this.radio_actionMarquage_pose.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.poseActionPerformed(evt);
			}
		});

		this.radio_actionMarquage_retrait.setFont(new java.awt.Font("Dialog", 0, 12)); //$NON-NLS-1$
		this.radio_actionMarquage_retrait.setText(Messages.getString("AjouterLot.131")); //$NON-NLS-1$
		this.buttonGroup_actionMarquage.add(this.radio_actionMarquage_retrait);
		this.radio_actionMarquage_retrait.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.lectureRetraitActionperformed(evt);
			}
		});
		this.radio_actionMarquage_posemultiple.setFont(new java.awt.Font("Dialog", 0, 12)); //$NON-NLS-1$
		this.radio_actionMarquage_posemultiple.setText(Messages.getString("AjouterLot.133")); //$NON-NLS-1$
		this.buttonGroup_actionMarquage.add(this.radio_actionMarquage_posemultiple);
		this.radio_actionMarquage_posemultiple.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.lectureRetraitActionperformed(evt);
			}
		});

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(8, 5, 3, 5);
		this.panel_marqueLeft.add(label_actionMarquage, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(8, 5, 3, 5);
		this.panel_marqueLeft.add(radio_actionMarquage_lecture, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(8, 5, 3, 5);
		this.panel_marqueLeft.add(radio_actionMarquage_pose, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(8, 5, 3, 5);
		this.panel_marqueLeft.add(radio_actionMarquage_retrait, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(8, 5, 3, 5);
		this.panel_marqueLeft.add(radio_actionMarquage_posemultiple, gridBagConstraints);

		label_refMarque.setText(Messages.getString("AjouterLot.106"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.gridwidth = 4;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(8, 5, 3, 5);
		this.panel_marqueLeft.add(label_refMarque, gridBagConstraints);

		this.textfielddt_refMarque.setPreferredSize(new Dimension(150, 25));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.gridwidth = 4;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_marqueLeft.add(textfielddt_refMarque, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.gridwidth = 4;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(8, 5, 3, 5);
		this.panel_marqueLeft.add(combo_referencesMarques, gridBagConstraints);
		combo_referencesMarques.setVisible(false);

		this.label_CommentaireMarquage.setText(Messages.getString("AjouterLot.112"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.gridwidth = 4;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(8, 5, 3, 5);
		this.panel_marqueLeft.add(label_CommentaireMarquage, gridBagConstraints);

		textarea_CommentaireMarquage.setColumns(25);
		textarea_CommentaireMarquage.setLineWrap(true);
		textarea_CommentaireMarquage.setRows(2);
		this.scrollPane_CommentaireMarquage.setViewportView(textarea_CommentaireMarquage);
		this.scrollPane_CommentaireMarquage.setPreferredSize(new Dimension(300, 50));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.gridwidth = 4;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_marqueLeft.add(scrollPane_CommentaireMarquage, gridBagConstraints);

		label_OperationMarquage.setText(Messages.getString("AjouterLot.110"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(8, 5, 3, 5);
		this.panel_marqueRight.add(label_OperationMarquage, gridBagConstraints);

		// this.combo_OperationMarquage.setModel(new
		// DefaultComboBoxModel(this.lesOperationsMarquages.toArray()));
		this.combo_OperationMarquage.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.chargeReferencemarques();
			}
		});

		combo_OperationMarquage.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_OperationMarquage.isEnabled()) {
						combo_OperationMarquage.setEnabled(false);
						label_OperationMarquage.setForeground(SystemColor.textInactiveText);
					} else {
						combo_OperationMarquage.setEnabled(true);
						label_OperationMarquage.setForeground(SystemColor.black);
					}
				}
			}
		});

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_marqueRight.add(combo_OperationMarquage, gridBagConstraints);

		label_localisationMarque.setText(Messages.getString("AjouterLot.107"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(8, 5, 3, 5);
		this.panel_marqueRight.add(label_localisationMarque, gridBagConstraints);

		combo_localisationMarque.setModel(new DefaultComboBoxModel(lesLocalisations.toArray()));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_marqueRight.add(combo_localisationMarque, gridBagConstraints);

		combo_localisationMarque.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_localisationMarque.isEnabled()) {
						combo_localisationMarque.setEnabled(false);
						label_localisationMarque.setForeground(SystemColor.textInactiveText);
					} else {
						combo_localisationMarque.setEnabled(true);
						label_localisationMarque.setForeground(SystemColor.black);
					}
				}
			}
		});

		label_natureMarque.setText(Messages.getString("AjouterLot.108"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(8, 5, 3, 5);
		this.panel_marqueRight.add(label_natureMarque, gridBagConstraints);

		combo_natureMarque.setModel(new DefaultComboBoxModel(lesNaturesMarques.toArray()));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_marqueRight.add(combo_natureMarque, gridBagConstraints);

		combo_natureMarque.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_natureMarque.isEnabled()) {
						combo_natureMarque.setEnabled(false);
						label_natureMarque.setForeground(SystemColor.textInactiveText);
					} else {
						combo_natureMarque.setEnabled(true);
						label_natureMarque.setForeground(SystemColor.black);
					}
				}
			}
		});

		label_commentairesMarque.setText(Messages.getString("AjouterLot.109"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(8, 5, 3, 5);
		this.panel_marqueRight.add(label_commentairesMarque, gridBagConstraints);

		textarea_commentairesMarque.setColumns(25);
		textarea_commentairesMarque.setLineWrap(true);
		textarea_commentairesMarque.setRows(2);
		jScrollPane_commentairesMarque.setPreferredSize(new Dimension(250, 50));
		jScrollPane_commentairesMarque.setViewportView(textarea_commentairesMarque);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 8;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_marqueRight.add(jScrollPane_commentairesMarque, gridBagConstraints);

		textarea_commentairesMarque.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (textarea_commentairesMarque.isEnabled()) {
						textarea_commentairesMarque.setEnabled(false);
						label_commentairesMarque.setForeground(SystemColor.textInactiveText);
					} else {
						textarea_commentairesMarque.setEnabled(true);
						label_commentairesMarque.setForeground(SystemColor.black);
					}
				}
			}
		});
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 10);
		this.panel_marqueTop.add(panel_marqueLeft, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(3, 10, 3, 5);
		this.panel_marqueTop.add(panel_marqueRight, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_marques.add(panel_marqueTop, gridBagConstraints);

		this.button_AMMarque.setFont(new java.awt.Font("Dialog", 1, 10));
		this.button_AMMarque.setText(Messages.getString("AjouterLot.16"));
		this.button_SMarque.setFont(new java.awt.Font("Dialog", 1, 10));
		this.button_SMarque.setText(Messages.getString("AjouterLot.43"));
		this.button_annulerMarque.setFont(new java.awt.Font("Dialog", 1, 10));
		this.button_annulerMarque.setText(Messages.getString("AjouterLot.18"));

		this.panel_boutonMarque.add(button_AMMarque);
		this.panel_boutonMarque.add(button_SMarque);
		this.panel_boutonMarque.add(button_annulerMarque);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_marques.add(panel_boutonMarque, gridBagConstraints);

		jList_marquages.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				listeMarquesMouseClicked(evt);
			}

			private void listeMarquesMouseClicked(MouseEvent evt) {
				try {
					AjouterLot.this.afficheMarque();
				} catch (Exception ex) {
					Logger.getLogger(AMSPeriodeFonctionnement.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});

		button_AMMarque.setToolTipText(Messages.getString("AjouterLot.113"));
		button_AMMarque.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.ajouterModifierMarque();
			}
		});

		button_SMarque.setToolTipText(Messages.getString("AjouterLot.114"));
		button_SMarque.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.supprimerMarque();
			}
		});

		button_annulerMarque.setToolTipText(Messages.getString("AjouterLot.115"));
		button_annulerMarque.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				try {
					AjouterLot.this.afficheLot(); // one ne demande pas le
					// focus sur le lot
					AjouterLot.this.resetaffichageMarque();
				} catch (Exception e) {
					AjouterLot.this.resultat.setText(e.getMessage());
					logger.log(Level.SEVERE, " annuler Marque ", e);
				}

			}
		});

		/////////////////////////////////////
		///// PANEL OPERATION DE MARQUAGE
		///////////////////////////////////

		this.panel_operationmarquage.setLayout(new GridBagLayout());

		panel_topOperationMarquage.setLayout(new GridBagLayout());
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_operationmarquage.add(panel_topOperationMarquage, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_operationmarquage.add(panel_bottomOperationMarquage, gridBagConstraints);

		this.label_operationsMarquage.setText(Messages.getString("AjouterLot.116"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(10, 5, 3, 5);
		this.panel_topOperationMarquage.add(this.label_operationsMarquage, gridBagConstraints);

		this.jScrollPane_operationMarquage.setPreferredSize(new Dimension(300, 100));
		this.jScrollPane_operationMarquage.setViewportView(jList_operationMarquage);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_topOperationMarquage.add(this.jScrollPane_operationMarquage, gridBagConstraints);

		this.label_codeOperationMarquage.setText(Messages.getString("AjouterLot.117"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(10, 5, 3, 5);
		this.panel_topOperationMarquage.add(this.label_codeOperationMarquage, gridBagConstraints);

		this.textfielddt_operationMarquage.setPreferredSize(new Dimension(200, 25));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_topOperationMarquage.add(this.textfielddt_operationMarquage, gridBagConstraints);

		this.label_commentairesOperationMarquage.setText(Messages.getString("AjouterLot.118"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(10, 5, 3, 5);
		this.panel_topOperationMarquage.add(label_commentairesOperationMarquage, gridBagConstraints);

		textarea_commentaireOperationMarquage.setColumns(20);
		textarea_commentaireOperationMarquage.setLineWrap(true);
		textarea_commentaireOperationMarquage.setRows(4);
		textarea_commentaireOperationMarquage.setToolTipText("Editer dans le panneau op�ration de marquage");

		this.jScrollPane_commentairesOperationMarquage.setViewportView(textarea_commentaireOperationMarquage);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_topOperationMarquage.add(jScrollPane_commentairesOperationMarquage, gridBagConstraints);

		this.button_AMOperationMarquage.setText(Messages.getString("AjouterLot.16"));
		this.button_SOperationMarquage.setText(Messages.getString("AjouterLot.17"));
		this.button_annulerOperationMarquage.setText(Messages.getString("AjouterLot.18"));
		this.panel_bottomOperationMarquage.add(this.button_AMOperationMarquage);
		this.panel_bottomOperationMarquage.add(this.button_SOperationMarquage);
		this.panel_bottomOperationMarquage.add(this.button_annulerOperationMarquage);

		jList_operationMarquage.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				listeOperationMarquageMouseClicked(evt);
			}

			private void listeOperationMarquageMouseClicked(MouseEvent evt) {
				try {
					// la fonction appel�e est une fonction impl�ment�e dans
					// AMSOperationMarquage.java
					// c'est un fonction statique qui collecte les info
					// n�c�ssaires et remple les champs
					AMSOperationMarquage.chargeInfoOperationMarquage(AjouterLot.this.jList_operationMarquage,
							AjouterLot.this.textfielddt_operationMarquage,
							AjouterLot.this.textarea_commentaireOperationMarquage);
				} catch (Exception e) {
					logger.log(Level.SEVERE, " listeOperationMarquageMouseClicked ", e);

				}
			}
		});

		button_AMOperationMarquage.setToolTipText(Messages.getString("AjouterLot.1022"));
		button_AMOperationMarquage.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				// enregistre dans la base. Fonction impl�ment�e dans
				// AMSOperationMarquage.java
				String result = AMSOperationMarquage.AMOperationMarquage(AjouterLot.this.jList_operationMarquage,
						AjouterLot.this.textfielddt_operationMarquage,
						AjouterLot.this.textarea_commentaireOperationMarquage);

				// vide les champs. Fonction impl�ment�e dans
				// AMSOperationMarquage.java
				AMSOperationMarquage.annulerOperationMarquage(AjouterLot.this.textfielddt_operationMarquage,
						AjouterLot.this.textarea_commentaireOperationMarquage);

				// met a jour la liste. Fonction impl�ment�e dans
				// AMSOperationMarquage.java
				AjouterLot.this.chargeListeOperationsMarquage();

				AjouterLot.this.resultat.setText(result);
			}
		});

		button_SOperationMarquage.setToolTipText(Messages.getString("AjouterLot.1033"));
		button_SOperationMarquage.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				// supprime de la base. Fonction impl�ment�e dans
				// AMSOperationMarquage.java
				String result = AMSOperationMarquage.SOperationMarquage(AjouterLot.this.jList_operationMarquage);

				// vide les champs. Fonction impl�ment�e dans
				// AMSOperationMarquage.java
				AMSOperationMarquage.annulerOperationMarquage(AjouterLot.this.textfielddt_operationMarquage,
						AjouterLot.this.textarea_commentaireOperationMarquage);

				// met a jour la liste
				AjouterLot.this.chargeListeOperationsMarquage();

				AjouterLot.this.resultat.setText(result);
			}
		});

		button_annulerOperationMarquage.setToolTipText(Messages.getString("AjouterLot.1044"));
		button_annulerOperationMarquage.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AMSOperationMarquage.annulerOperationMarquage(AjouterLot.this.textfielddt_operationMarquage,
						AjouterLot.this.textarea_commentaireOperationMarquage);
				AjouterLot.this.chargeListeOperationsMarquage();
			}
		});

		//////////////////////////////////////////////////////
		//// PRELEVEMENTS
		/////////////////////////////////////////////////////

		this.panel_prelevement.setLayout(new GridBagLayout());

		panel_topPrelevement.setLayout(new GridBagLayout());
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_prelevement.add(panel_topPrelevement, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_prelevement.add(panel_bottomPrelevement, gridBagConstraints);

		label_codePrelevement.setText(Messages.getString("AjouterLot.120"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		panel_topPrelevement.add(label_codePrelevement, gridBagConstraints);

		textfield_codePrelevement.setColumns(15);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		panel_topPrelevement.add(textfield_codePrelevement, gridBagConstraints);

		textfield_codePrelevement.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (textfield_codePrelevement.isEnabled()) {
						textfield_codePrelevement.setEnabled(false);
						label_codePrelevement.setForeground(SystemColor.textInactiveText);
					} else {
						textfield_codePrelevement.setEnabled(true);
						label_codePrelevement.setForeground(SystemColor.black);
					}
				}
			}
		});

		label_operateurPrelevement.setText(Messages.getString("AjouterLot.121"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		panel_topPrelevement.add(label_operateurPrelevement, gridBagConstraints);

		textfield_operateurPrelevement.setColumns(15);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		panel_topPrelevement.add(textfield_operateurPrelevement, gridBagConstraints);

		textfield_operateurPrelevement.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (textfield_operateurPrelevement.isEnabled()) {
						textfield_operateurPrelevement.setEnabled(false);
						label_operateurPrelevement.setForeground(SystemColor.textInactiveText);
					} else {
						textfield_operateurPrelevement.setEnabled(true);
						label_operateurPrelevement.setForeground(SystemColor.black);
					}
				}
			}
		});

		label_typePrelevement.setText(Messages.getString("AjouterLot.122"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		panel_topPrelevement.add(label_typePrelevement, gridBagConstraints);

		this.combo_typePrelevement.setModel(new DefaultComboBoxModel(this.lesPrelevements.toArray()));

		combo_typePrelevement.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_typePrelevement.isEnabled()) {
						combo_typePrelevement.setEnabled(false);
						label_typePrelevement.setForeground(SystemColor.textInactiveText);
					} else {
						combo_typePrelevement.setEnabled(true);
						label_typePrelevement.setForeground(SystemColor.black);
					}
				}
			}
		});

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		panel_topPrelevement.add(combo_typePrelevement, gridBagConstraints);

		label_localisationPrelevement.setText(Messages.getString("AjouterLot.123"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		panel_topPrelevement.add(label_localisationPrelevement, gridBagConstraints);

		combo_localisationPrelevement.setModel(new DefaultComboBoxModel(this.lesLocalisations.toArray()));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		panel_topPrelevement.add(combo_localisationPrelevement, gridBagConstraints);

		combo_localisationPrelevement.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (combo_localisationPrelevement.isEnabled()) {
						combo_localisationPrelevement.setEnabled(false);
						label_localisationPrelevement.setForeground(SystemColor.textInactiveText);
					} else {
						combo_localisationPrelevement.setEnabled(true);
						label_localisationPrelevement.setForeground(SystemColor.black);
					}
				}
			}
		});

		label_commentairesPrelevement.setText(Messages.getString("AjouterLot.124"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		panel_topPrelevement.add(label_commentairesPrelevement, gridBagConstraints);

		textarea_commentairesPrelevement.setColumns(28);
		textarea_commentairesPrelevement.setRows(3);
		textarea_commentairesPrelevement.setLineWrap(true);
		jScrollPane_commentairesPrelevement.setViewportView(textarea_commentairesPrelevement);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		panel_topPrelevement.add(jScrollPane_commentairesPrelevement, gridBagConstraints);

		textarea_commentairesPrelevement.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (textarea_commentairesPrelevement.isEnabled()) {
						textarea_commentairesPrelevement.setEnabled(false);
						label_commentairesPrelevement.setForeground(SystemColor.textInactiveText);
					} else {
						textarea_commentairesPrelevement.setEnabled(true);
						label_commentairesPrelevement.setForeground(SystemColor.black);
					}
				}
			}
		});

		label_listePrelevements.setText(Messages.getString("AjouterLot.125"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		panel_topPrelevement.add(label_listePrelevements, gridBagConstraints);

		jList_listePrelevements.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		jList_listePrelevements.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				listeprelevementmouseclicked(evt);
			}

			private void listeprelevementmouseclicked(MouseEvent evt) {
				try {
					chargeInfoPrelevement();
				} catch (Exception e) {
					logger.log(Level.SEVERE, " listeprelevementmouseclicked ", e);
				}
			}
		});

		jScrollPane_listePrelevements.setPreferredSize(new Dimension(312, 70));
		jScrollPane_listePrelevements.setViewportView(jList_listePrelevements);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		panel_topPrelevement.add(jScrollPane_listePrelevements, gridBagConstraints);

		this.button_annulerPrelevement.setText(Messages.getString("AjouterLot.18"));
		this.panel_bottomPrelevement.add(this.button_AMPrelevement);
		this.panel_bottomPrelevement.add(this.button_SPrelevement);
		this.panel_bottomPrelevement.add(this.button_annulerPrelevement);

		this.button_AMPrelevement.setText(Messages.getString("AjouterLot.16"));
		button_AMPrelevement.setToolTipText(Messages.getString("AjouterLot.126"));
		button_AMPrelevement.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterModifierPrelevement();
			}
		});

		this.button_SPrelevement.setText(Messages.getString("AjouterLot.17"));
		button_SPrelevement.setToolTipText(Messages.getString("AjouterLot.127"));
		button_SPrelevement.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerPrelevement();
			}
		});

		button_annulerPrelevement.setText(Messages.getString("AjouterLot.18"));
		button_annulerPrelevement.setToolTipText(Messages.getString("AjouterLot.128"));
		button_annulerPrelevement.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annulerPrelevement();
			}
		});

		/////////////////////////////
		/// PANEL BOTTOM
		//////////////////////////////

		this.panel_bottom.setLayout(new java.awt.GridBagLayout());
		this.panel_bottom.setBackground(new java.awt.Color(191, 191, 224));
		this.button_Modifier.setText(Messages.getString("AjouterLot.53")); //$NON-NLS-1$
		this.button_Modifier.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.ModifierLotActionPerformed(evt);
			}
		});
		this.button_Modifier.setFocusable(false);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_bottom.add(this.button_Modifier, gridBagConstraints);

		this.button_effacer.setText(Messages.getString("AjouterLot.54")); //$NON-NLS-1$
		this.button_effacer.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.effacerActionPerformed(evt);
			}
		});
		this.button_effacer.setFocusable(false);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_bottom.add(this.button_effacer, gridBagConstraints);

		this.button_supprimerLot.setText(Messages.getString("AjouterLot.55")); //$NON-NLS-1$
		this.button_supprimerLot.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.supprimerLotActionPerformed(evt);
			}
		});
		this.button_supprimerLot.setFocusable(false);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_bottom.add(this.button_supprimerLot, gridBagConstraints);
		this.button_ajouterLot.isFocusOwner();
		this.button_ajouterLot.setText(Messages.getString("AjouterLot.56")); //$NON-NLS-1$
		this.button_ajouterLot.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.ajouterLotActionPerformed(evt);
			}
		});

		// Ci dessous d�clenche une action lorsque le button a le focus et qu'on
		// appui sur entr�e
		AbstractAction enterPressed = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (button_ajouterLot.isFocusOwner() & !e.getActionCommand().equals("Ajouter lot")) {
					AjouterLot.this.ajouterLotActionPerformed(e);
				} else if (button_ajouterEchantillon.isFocusOwner() & !e.getActionCommand().equals("Ajouter lot")) {
					AjouterLot.this.ajouterEchantillonActionPerformed(e);
				}
			}
		};
		this.button_ajouterLot.addActionListener(enterPressed);
		this.button_ajouterLot.getInputMap(javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW)
				.put(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ENTER, 0), "Enter_pressed");
		this.button_ajouterLot.getActionMap().put("Enter_pressed", enterPressed);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_bottom.add(this.button_ajouterLot, gridBagConstraints);

		this.button_ajouterEchantillon.setText(Messages.getString("AjouterLot.57")); //$NON-NLS-1$
		this.button_ajouterEchantillon.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.ajouterEchantillonActionPerformed(evt);
				// on remet le focus sur l'onglet 'Informations'
				// ongletsLots.setSelectedIndex(0);
			}
		});

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_bottom.add(this.button_ajouterEchantillon, gridBagConstraints);

		// Ci dessous d�clenche une action lorsque le button a le focus et qu'on
		// appui sur entr�e

		this.button_incrementerOperation.setText(Messages.getString("AjouterLot.58")); //$NON-NLS-1$
		this.button_incrementerOperation.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.incrementerOperationActionPerformed(evt);
			}
		});
		this.button_incrementerOperation.setFocusable(false);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_bottom.add(this.button_incrementerOperation, gridBagConstraints);

		this.button_ajouterOperation.setText(Messages.getString("AjouterLot.59")); //$NON-NLS-1$
		this.button_ajouterOperation.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.ajouterOperationActionPerformed(evt);
			}
		});
		this.button_ajouterOperation.setFocusable(false);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_bottom.add(this.button_ajouterOperation, gridBagConstraints);

		this.button_lotPrev.setText("<"); //$NON-NLS-1$
		this.button_lotPrev.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.lotPrevActionPerformed(evt);
			}
		});
		this.button_lotPrev.setFocusable(false);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 5;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_bottom.add(this.button_lotPrev, gridBagConstraints);

		this.label_navigLots.setFont(new java.awt.Font("Dialog", 0, 10)); //$NON-NLS-1$
		this.label_navigLots.setText(Messages.getString("AjouterLot.62")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 4;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
		gridBagConstraints.insets = new java.awt.Insets(3, 20, 3, 5);
		this.panel_bottom.add(this.label_navigLots, gridBagConstraints);

		this.button_lotNext.setText(">"); //$NON-NLS-1$
		this.button_lotNext.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.lotNextActionPerformed(evt);
			}
		});
		this.button_lotNext.setFocusable(false);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 6;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_bottom.add(this.button_lotNext, gridBagConstraints);

		this.button_opePrev.setText("<"); //$NON-NLS-1$
		this.button_opePrev.setEnabled(true);
		this.button_opePrev.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.OpePrevActionPerformed(evt);
			}
		});
		this.button_opePrev.setFocusable(false);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 5;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_bottom.add(this.button_opePrev, gridBagConstraints);

		this.label_navOpe.setFont(new java.awt.Font("Dialog", 0, 10)); //$NON-NLS-1$
		this.label_navOpe.setText(Messages.getString("AjouterLot.66")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 4;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
		gridBagConstraints.insets = new java.awt.Insets(3, 20, 3, 5);
		this.panel_bottom.add(this.label_navOpe, gridBagConstraints);

		this.button_opeNext.setText(">"); //$NON-NLS-1$
		this.button_opeNext.setEnabled(true);
		this.button_opeNext.setFocusable(false);
		this.button_opeNext.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AjouterLot.this.OpeNextActionPerformed(evt);
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 6;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
		this.panel_bottom.add(this.button_opeNext, gridBagConstraints);

		this.add(this.panel_bottom, java.awt.BorderLayout.SOUTH);
		// panel_bottom.setFocusTraversalPolicy(
		// new FocusTraversalOnArray(new Component[] { button_ajouterLot,
		// button_ajouterEchantillon }));
		// setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[] {
		// TabbedPaneOngletsLots, panel_bottom }));
		// ci dessous marche pas
		this.TabbedPaneOngletsLots.setSelectedIndex(0);
		// this.panel_infosLots.setFocusable(false);
		this.arbreLots.setFocusable(false);
		// this.combo_tr_taxon_tax.setFocusCycleRoot(true);
		// this.TabbedPaneOngletsLots.transferFocusDownCycle();
	}// GEN-END:initComponents

	@SuppressWarnings("unchecked")
	private void car_precisionCaretUpdate(javax.swing.event.CaretEvent evt) {// GEN-FIRST:event_car_precisionCaretUpdate
		Lanceur.getElementsSelectionnes().put("car_precision", this.textfielddt_car_precision.getText()); //$NON-NLS-1$
	}// GEN-LAST:event_car_precisionCaretUpdate

	@SuppressWarnings("unchecked")
	private void tr_taxon_taxActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_tr_taxon_taxActionPerformed
		Lanceur.getElementsSelectionnes().put("tr_taxon_tax", new Integer(this.combo_tr_taxon_tax.getSelectedIndex())); //$NON-NLS-1$
	}// GEN-LAST:event_tr_taxon_taxActionPerformed

	@SuppressWarnings("unchecked")
	private void tr_typequantitelot_qteActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_tr_typequantitelot_qteActionPerformed
		Lanceur.getElementsSelectionnes().put("tr_typequantitelot_qte", //$NON-NLS-1$
				new Integer(this.combo_tr_typequantitelot_qte.getSelectedIndex()));
	}// GEN-LAST:event_tr_typequantitelot_qteActionPerformed

	/**
	 * Evennement correspondant au changement d'une valeur s�lectionn�e dans le
	 * lot
	 * 
	 * @param evt
	 */
	private void arbreLotsselectedValueChanged(MouseEvent me) {

		arbrelot_selected();

	}

	private void ajouterEchantillonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_ajouterEchantillonActionPerformed
		Lot lotParent;

		// Si le lot courant n'est pas definit, il ne peut y avoir de sous lot
		if (this.lot != null) {

			// Si le lot de rattachement de l'echantillon a deja un lot pere,
			// lengthnouvel echantillon aura le meme pere
			if (this.lot.getLotParent() != null) {
				lotParent = this.lot.getLotParent();
			}
			// sinon, c'est le lot courant qui est le pere
			else {
				lotParent = this.lot;
			}

			// Oublie le lot actuel
			this.lot = null;
			try {
				this.enregistreLotCar(lotParent);
			} catch (Exception e) {
				this.resultat.setText(e.getMessage());
				logger.log(Level.SEVERE, " enregistreLotCar ", e);
			}

		} else {
			this.resultat.setText(Erreur.S4009);
			logger.log(Level.SEVERE, " enregistreLotCar " + Erreur.S4009);
		}

	}// GEN-LAST:event_ajouterEchantillonActionPerformed

	private void car_listeValueChanged(javax.swing.event.ListSelectionEvent evt) {// GEN-FIRST:event_car_listeValueChanged
		this.afficheCaracteristique();
	}// GEN-LAST:event_car_listeValueChanged

	private void s_caracteristiqueActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_s_caracteristiqueActionPerformed
		this.supprimeCaracteristique();
	}// GEN-LAST:event_s_caracteristiqueActionPerformed

	private void a_caracteristiqueActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_a_caracteristiqueActionPerformed
		this.chargeSaveCaracteristiqueOnglet();
	}// GEN-LAST:event_a_caracteristiqueActionPerformed

	private void annuler_caracteristiqueActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_annuler_caracteristiqueActionPerformed
		this.initCaracteristiques();
	}// GEN-LAST:event_annuler_caracteristiqueActionPerformed

	private void car_parametreActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_car_parametreActionPerformed
		this.chargeListeRefValeurParametre();
	}// GEN-LAST:event_car_parametreActionPerformed

	private void car_type_qualitatifActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_car_type_qualitatifActionPerformed
		this.chargeListeRefParametreQual();
	}// GEN-LAST:event_car_type_qualitatifActionPerformed

	private void car_type_quantitatifActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_car_type_quantitatifActionPerformed
		this.chargeListeRefParametreQuan();
	}// GEN-LAST:event_car_type_quantitatifActionPerformed

	private void supprimerLotActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_supprimerLotActionPerformed
		// Seulement si un lot est selectionne
		if (this.lot != null) {

			this.supprimerLot();
			this.selectionne_dans_larbre(this.lot);
			this.resultat.setText("Lot supprim�");
		} else {
			this.resultat.setText(Erreur.S4009);
		}

	}// GEN-LAST:event_supprimerLotActionPerformed

	private void lotPrevActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_lotPrevActionPerformed
		this.previousLot();
	}// GEN-LAST:event_lotPrevActionPerformed

	private void lotNextActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_lotNextActionPerformed
		this.nextLot();
	}// GEN-LAST:event_lotNextActionPerformed

	private void OpePrevActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_lotPrevActionPerformed
		this.previousOperation();
	}// GEN-LAST:event_lotPrevActionPerformed

	private void OpeNextActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_lotNextActionPerformed
		this.nextOperation();
	}// GEN-LAST:event_lotNextActionPerformed

	private void ajouterLotActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_ajouterLotActionPerformed

		// Oublie le lot actuel ainsi que la caracteristique
		this.lot = null;
		// this.car = null ;
		try {
			this.enregistreLotCar(null);
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " ajouterLotActionPerformed ", e);
		}
	}// GEN-LAST:event_ajouterLotActionPerformed

	private void ajouterOperationActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_ajouterOperationActionPerformed
		// avant d'ajouter une nouvelle operation, enregistrement du lot actuel
		// this.enregistreLot() ;

		// Recherche du parent qui est un IhmAppli
		Component ihm = this.getParent();
		while ((ihm != null) && !(ihm instanceof IhmAppli)) {
			ihm = ihm.getParent();
		}

		// Passe a l'ecran d'ajout d'une operation
		((IhmAppli) ihm).changeContenu(new AjouterOperation(this.ope));

	}// GEN-LAST:event_ajouterOperationActionPerformed

	private void incrementerOperationActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_incrementerOperationActionPerformed
		// avant d'ajouter une nouvelle operation, enregistrement du lot actuel
		// this.enregistreLot() ;

		this.incrementerOpe();

	}// GEN-LAST:event_incrementerOperationActionPerformed

	private void ModifierLotActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_ModifierActionPerformed

		// Seulement si un lot est selectionne
		if (this.lot != null) {

			try {
				this.enregistreLotCar(this.lot.getLotParent());
			} catch (Exception e) {
				this.resultat.setText(e.getMessage());
				logger.log(Level.SEVERE, " ModifierLotActionPerformed ", e);
			}
		} else {
			this.resultat.setText(Erreur.S4009);
		}

		// this.lot ne change pas
	}// GEN-LAST:event_ModifierActionPerformed

	private void effacerActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_effacerActionPerformed

		this.reInitComponents();

	}// GEN-LAST:event_effacerActionPerformed

	/*
	 * Reinitialise les composants du jPanel courant
	 * 
	 */
	private void reInitComponents() {

		this.lot = null;
		// this.car = null ;

		this.removeAll();
		this.initListesCombos();
		this.initComponents();
		this.chargeListeOperationsMarquage();
		this.chargeMasque();
		this.initCaracteristiquesduMasque();
		this.initInfosLots();

		this.validate();
		this.repaint();

	}

	/**
	 * Methode de chargement des valeurs en base du masque. A lancer avant le
	 * initcomponent
	 */
	private void chargemasqueenbase() {
		///////// CHARGEMENT DU MASQUE//////////////////////
		Preferences preference = Preferences.userRoot();
		// r�cup�ration des pr�f�rences, par d�faut si vide "lot_defaut"
		// un trigger emp�che la suppression de ce lot dans la base de donn�es
		masquelot = new MasqueLot(new Masque(preference.get("masquelot", "lot_defaut")));
		// r�cup�ration des informations du masque en cours
		ListeMasque listemasque = new ListeMasque();
		try {
			listemasque.chargeFiltre(masquelot.getMasque().getCode());
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " chargeMasque, le masque s�l�ctionn�s  ", e);
		}
		this.masque = (Masque) listemasque.get(0);
		// la liste contient plus d'�l�ments que le masque qui n'est construit
		// qu'a partir du code
		// Je remplace la valeur de masqueope.
		this.masquelot = new MasqueLot(masque);
		this.listemasquelotenbase = new ListeMasqueLot();
		try {
			listemasquelotenbase.chargeFiltre(masquelot.getMasque().getCode());
		} catch (Exception e) {
			logger.log(Level.SEVERE, "chargeMasque", e);
			this.resultat.setText(e.getMessage());
		}
		this.caracteristiquemasquelotenbase = new SousListeCaracteristiqueMasqueLot(masquelot);
		try {
			caracteristiquemasquelotenbase.chargeSansFiltreDetails();
			// la m�thode chargeSansFiltre charge tous les objets de la sous
			// liste du masqueope
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " chargeMasque  ", e);
		}
		this.affichagemasquelotenbase = new SousListeAffichageMasqueLot(masquelot);
		try {
			affichagemasquelotenbase.chargeSansFiltre();

		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " chargeMasque  ", e);
		}
	}

	/**
	 * M�thode pour le chargement du Masque Lot le masque est stock� dans trois
	 * table,
	 * 
	 * A) la table masque lot avec des vecteurs pour affichage et les valeurs
	 * par d�faut ts_masquelot_mal( mal_mas_id integer, mal_affichage boolean[],
	 * -- affichage des champs stocke sous forme de vecteur mal_valeurdefaut
	 * text[], -- Valeurs par defaut stockee sous forme de vecteur)
	 * 
	 * B) la table iav.ts_masqueordreaffichage_maa qui stocke les �l�ments
	 * stock�es dans les combos et leur ordre d'affichage create table
	 * iav.ts_masqueordreaffichage_maa( maa_id serial PRIMARY KEY, maa_mal_id
	 * integer, maa_table character varying(40) NOT NULL,
	 * --tr_taxon_tax,tr_stadedeveloppement_std,tr_pathologie_pat,
	 * tr_localisationanatomique_loc maa_valeur text NOT NULL, maa_champdumasque
	 * character varying (30), --peut �tre null sinon pr�cise le champ de
	 * destination, car peut �tre multiple (cas de localisation) maa_rang
	 * integer, --ordre d'affichage de la table de reference. Si une valeur
	 * n'est pas referencee, le taxon n'est pas affiche dans le masque
	 * 
	 * C)la table ts_masquecaracteristiquelot_mac La table
	 * ts_caracteristique_mac contient les elements relatifs aux
	 * caracteristiques Elles sont au nombre de 3, si c'est une caracteristique
	 * quantitative, l'affichage correspond a� "valeur" "precision"
	 * "commentaires" si c'est une caracteristique qualitative, l'affichage
	 * correspond a� "valeur" "valeur par defaut" "commentaires"
	 * ts_masquecaracteristiquelot_mac( mac_id serial, mac_mal_id integer,
	 * mac_par_code character varying(5) NOT NULL, mac_affichagevaleur boolean,
	 * -- affichage des champs mac_affichageprecision boolean,
	 * mac_affichagemethodeobtention boolean, mac_affichagecommentaire boolean,
	 * mac_valeurquantitatifdefaut numeric, -- Valeurs par defaut
	 * mac_valeurqualitatifdefaut integer, -- code de la valeur dans la table
	 * ref.tr_valeurparametrequalitatif_val mac_precisiondefaut numeric,
	 * mac_methodeobtentiondefaut character varying(10), mac_commentairedefaut
	 * text
	 */
	private void chargeMasque() {

		// si il y aune valeur, le masque existe en base
		if (listemasquelotenbase.size() == 1) {
			int index; // indice de la valeur s�lectionn�e dans le masque pour
			this.masquelot = (MasqueLot) this.listemasquelotenbase.get(0);
			// je dois quand m�me aller r�cup�rer l'identifiant integer du
			// masque
			// sinon plante a l'update ou � l'insertion.
			// il a �t� pass� par masque...
			this.masquelot.setMasque(this.masque);
			this.affichage = masquelot.getAffichage();
			this.valeurdefaut = masquelot.getValeurDefaut();
			this.label_titre.setText("Ajouter Modifier Lot (masque" + masquelot.getMasque().getCode() + ")");

			// this.label_masquelot.setText(this.masquelot.getMasque().getCode()
			// + " : " + masque.getDescription());
			// ---------------------------------------------------------------------
			// mise � jour des combos et textfields � partir des valeurs par
			// d�faut
			// ---------------------------------------------------------------------
			// effectif(stock� dans lot_quantite puis test pour savoir si
			// quantit� ou effectif
			// if (valeurdefaut[12] != null) {
			// this.textfielddt_lot_quantite.setText((valeurdefaut[12]));
			// }
			// quantite
			if (valeurdefaut[0] != null) {
				this.textfielddt_lot_quantite.setText(valeurdefaut[0]);
			}
			// typequantite
			if (valeurdefaut[1] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[1], this.combo_tr_typequantitelot_qte);
				this.combo_tr_typequantitelot_qte.setSelectedIndex(index);
			}
			// methodeobtention
			if (valeurdefaut[2] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[2], this.combo_lot_methode_obtention);
				this.combo_lot_methode_obtention.setSelectedIndex(index);
			}
			// devenir
			if (valeurdefaut[3] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[3], this.combo_tr_devenirlot_dev);
				this.combo_tr_devenirlot_dev.setSelectedIndex(index);
			}
			// code prelevement
			if (valeurdefaut[4] != null) {
				this.textfield_codePrelevement.setText(valeurdefaut[4]);
			}
			// operateur prelevement
			if (valeurdefaut[5] != null) {
				this.textfield_operateurPrelevement.setText(valeurdefaut[5]);
			}
			// type pr�l�vemement
			if (valeurdefaut[6] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[6], this.combo_typePrelevement);
				this.combo_typePrelevement.setSelectedIndex(index);
			}
			// localisation pr�l�vemement
			if (valeurdefaut[7] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[7], this.combo_localisationPrelevement);
				this.combo_localisationPrelevement.setSelectedIndex(index);
			}
			// operation marquage
			if (valeurdefaut[8] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[8], this.combo_OperationMarquage);
				this.combo_OperationMarquage.setSelectedIndex(index);
			}
			// localisation marquage
			if (valeurdefaut[9] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[9], this.combo_localisationMarque);
				this.combo_localisationMarque.setSelectedIndex(index);
			}
			// naturemarque
			if (valeurdefaut[10] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[10], this.combo_natureMarque);
				this.combo_natureMarque.setSelectedIndex(index);
			}
			// action marquage
			if (valeurdefaut[11] != null) {
				switch (valeurdefaut[11]) {
				case "Pose":
					radio_actionMarquage_pose.setSelected(true);
					;
					break;
				case "Lecture":
					radio_actionMarquage_lecture.setSelected(true);
					break;
				case "Retrait":
					radio_actionMarquage_retrait.setSelected(true);
					break;
				case "PoseMultiple":
					radio_actionMarquage_posemultiple.setSelected(true);
					break;
				}
			}
			if (valeurdefaut[13] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[13], this.combo_importancePathologie);
				this.combo_importancePathologie.setSelectedIndex(index);
			}

			// ---------------------------------------------------------------------
			// charge les valeus des checkboxes
			// ---------------------------------------------------------------------
			this.combo_tr_taxon_tax.setEnabled(affichage[0]);
			if (affichage[0]) {
				this.label_taxon.setForeground(SystemColor.black);
				// this.combo_tr_taxon_tax.requestFocus();
			} else {
				this.label_taxon.setForeground(SystemColor.textInactiveText);
			}
			this.combo_tr_stade_std.setEnabled(affichage[1]);
			if (affichage[1]) {
				this.label_stade.setForeground(SystemColor.black);
			} else {
				this.label_stade.setForeground(SystemColor.textInactiveText);
			}
			// le lot est soit une quantit� soit un effectif
			// this.textfielddt_lot_quantite.setEnabled(affichage[19]);
			this.textfielddt_lot_quantite.setEnabled(affichage[2]);
			if (affichage[2]) {
				this.label_quantite.setForeground(SystemColor.black);
			} else {
				this.label_quantite.setForeground(SystemColor.textInactiveText);
			}

			this.combo_tr_typequantitelot_qte.setEnabled(affichage[3]);
			if (affichage[3]) {
				this.label_effectif.setForeground(SystemColor.black);
			} else {
				this.label_effectif.setForeground(SystemColor.textInactiveText);
			}
			this.combo_lot_methode_obtention.setEnabled(affichage[4]);
			if (affichage[4]) {
				this.label_methodelot.setForeground(SystemColor.black);
			} else {
				this.label_methodelot.setForeground(SystemColor.textInactiveText);
			}
			this.combo_tr_devenirlot_dev.setEnabled(affichage[5]);
			if (affichage[5]) {
				this.label_devenirlot.setForeground(SystemColor.black);
			} else {
				this.label_devenirlot.setForeground(SystemColor.textInactiveText);
			}
			this.textarea_lot_commentaires.setEnabled(affichage[6]);
			if (affichage[6]) {
				this.label_commentaireslot.setForeground(SystemColor.black);
			} else {
				this.label_commentaireslot.setForeground(SystemColor.textInactiveText);
			}
			this.combo_pathologies.setEnabled(affichage[7]);
			if (affichage[7]) {
				this.label_pathologie.setForeground(SystemColor.black);
			} else {
				this.label_pathologie.setForeground(SystemColor.textInactiveText);
			}
			this.combo_localisationPathologie.setEnabled(affichage[8]);
			if (affichage[8]) {
				this.label_LocalisationPathologie.setForeground(SystemColor.black);
			} else {
				this.label_LocalisationPathologie.setForeground(SystemColor.textInactiveText);
			}
			this.combo_importancePathologie.setEnabled(affichage[20]);
			if (affichage[20]) {
				this.label_importancePathologie.setForeground(SystemColor.black);
			} else {
				this.label_importancePathologie.setForeground(SystemColor.textInactiveText);
			}
			this.textfield_codePrelevement.setEnabled(affichage[9]);
			if (affichage[9]) {
				this.label_codePrelevement.setForeground(SystemColor.black);
			} else {
				this.label_codePrelevement.setForeground(SystemColor.textInactiveText);
			}
			this.textfield_operateurPrelevement.setEnabled(affichage[10]);
			if (affichage[10]) {
				this.label_operateurPrelevement.setForeground(SystemColor.black);
			} else {
				this.label_operateurPrelevement.setForeground(SystemColor.textInactiveText);
			}
			this.combo_typePrelevement.setEnabled(affichage[11]);
			if (affichage[11]) {
				this.label_typePrelevement.setForeground(SystemColor.black);
			} else {
				this.label_typePrelevement.setForeground(SystemColor.textInactiveText);
			}
			this.combo_localisationPrelevement.setEnabled(affichage[12]);
			if (affichage[12]) {
				this.label_localisationPrelevement.setForeground(SystemColor.black);
			} else {
				this.label_localisationPrelevement.setForeground(SystemColor.textInactiveText);
			}
			this.textarea_commentairesPrelevement.setEnabled(affichage[13]);
			if (affichage[13]) {
				this.label_commentairesPrelevement.setForeground(SystemColor.black);
			} else {
				this.label_commentairesPrelevement.setForeground(SystemColor.textInactiveText);
			}
			this.combo_OperationMarquage.setEnabled(affichage[14]);
			if (affichage[14]) {
				this.label_OperationMarquage.setForeground(SystemColor.black);
			} else {
				this.label_OperationMarquage.setForeground(SystemColor.textInactiveText);
			}
			this.combo_localisationMarque.setEnabled(affichage[15]);
			if (affichage[15]) {
				this.label_localisationMarque.setForeground(SystemColor.black);
			} else {
				this.label_localisationMarque.setForeground(SystemColor.textInactiveText);
			}
			this.combo_natureMarque.setEnabled(affichage[16]);
			// TODO pb button marquage
			// this.buttonGroup_actionMarquage.setEnabled(affichage[17]);
			this.textarea_CommentaireMarquage.setEnabled(affichage[18]);
			if (affichage[18]) {
				this.label_CommentaireMarquage.setForeground(SystemColor.black);
			} else {
				this.label_CommentaireMarquage.setForeground(SystemColor.textInactiveText);
			}

		} else

		{
			logger.log(Level.SEVERE, " chargeMasque : erreur le masque n'existe pas en base");
		}

	}

	/**
	 * Initialise les r�f�rentiels pour le remplissage des combos
	 */
	private void initListesCombos() {

		this.lesTaxons = Lanceur.getListeRefTaxon();
		this.lesStades = Lanceur.getListeRefStade();
		this.lesDevenirs = Lanceur.getListeRefDevenir();
		this.lesLocalisations = Lanceur.getListeRefLocalisationPathologie();
		this.lesPathologies = Lanceur.getListeRefPathologie();
		this.lesimportancespathologie = Lanceur.getListeRefImportancePathologie();
		this.lesNaturesMarques = Lanceur.getListeRefNatureMarque();
		this.lesPrelevements = Lanceur.getListeRefPrelevement();
		/*
		 * ticket # 190 if (this.lesDevenirs.get("") == null) {
		 * this.lesDevenirs.put("", new RefDevenir(null)); } // pour // que //
		 * l'utilisateur // puisse // ne // rien // choisir
		 */
		this.lesTypeQuantite = Lanceur.getListeRefTypeQuantite();
		if (this.lesTypeQuantite.get(quantiteIndividus) == null) {
			this.lesTypeQuantite.putFirst(quantiteIndividus, new RefTypeQuantite(quantiteIndividus, quantiteIndividus));
		} // Sert pour l'affichage mais n'existe pas dans la base

		this.methodesObtentionLot = Lanceur.getMethodesObtentionLot();
	}

	/**
	 * Initialise les valeurs des combos � partir des valeurs du masque
	 * 
	 * @author Cedric 2016
	 * 
	 * @param complet
	 *            = faut il charger la liste compl�te ou seulement le combo du
	 *            masque
	 * 
	 */
	private void initcombotaxon(boolean complet) {
		///////////////////
		//// TAXON
		///////////////////
		// extrait les �l�ments de affichagemasquelotenbase seulement pour
		// le combo_taxon, normalement
		// ils sont ordonn�s par rang asc dans la requ�te de la sous liste.
		SousListeAffichageMasqueLot saml = SousListeAffichageMasqueLot.get_elements_list("duallist_taxon",
				affichagemasquelotenbase);
		// dans le combo on a remplit un listModeltaxon a partir de
		// listReftaxon qui contient tous les details
		// les objets RefTaxons qui remplissent le combo sont donc complets,
		// et il faut aller les chercher a partir
		// de leur code qui est stock� dans maa_valeur
		// si un lot utilisant un taxon qui n'est pas dans le masque est
		// s�lectionn�
		// l'interface basculera vers un affichage complet
		comboModeltaxoncomplet = new DefaultComboBoxModel<RefTaxon>();
		comboModeltaxonmasque = new DefaultComboBoxModel<RefTaxon>();
		DefaultListSelectionModel model = new DefaultListSelectionModel();
		//// Pour l'affichage du masque
		for (int i = 0; i < saml.size(); i++) {
			String code = ((AffichageMasqueLot) saml.get(i)).getValeur();
			boolean match = false;
			// on va chercher les �l�ments de lesTaxons qui sont les objets
			// ayant servi � remplir le combo
			for (int j = 0; j < this.lesTaxons.size(); j++) {
				match = ((RefTaxon) this.lesTaxons.get(j)).getCode().equals(code);
				if (match)
					comboModeltaxonmasque.addElement((RefTaxon) this.lesTaxons.get(j));

			}
		}
		// pour l'affichage complet
		// ajout des �l�ments � la suite

		model.addSelectionInterval(0, comboModeltaxonmasque.getSize() - 1);
		// selection des combos du masque (entre 0 et et la taille de
		// comboModeltaxonmasque)
		for (int j = 0; j < comboModeltaxonmasque.getSize(); j++) {
			comboModeltaxoncomplet.addElement((RefTaxon) comboModeltaxonmasque.getElementAt(j));
		}
		// ajout des taxons manquants au reste du combo
		for (int j = 0; j < this.lesTaxons.size(); j++) {
			if (comboModeltaxoncomplet.getIndexOf(this.lesTaxons.get(j)) == -1)
				comboModeltaxoncomplet.addElement((RefTaxon) this.lesTaxons.get(j));
		}
		if (complet) {
			// si le combo est complet utilisation d'un renderer pour "masquer"
			// les �l�ments du combo dans l'affichage
			EnabledJComboBoxRenderer enableRenderer = new EnabledJComboBoxRenderer(model);
			this.combo_tr_taxon_tax.setModel(comboModeltaxoncomplet);
			combo_tr_taxon_tax.setRenderer(enableRenderer);
		} else {

			this.combo_tr_taxon_tax.setModel(comboModeltaxonmasque);

		}
	}

	/**
	 * Initialise les valeurs des combos � partir des valeurs du masque voir
	 * initcombotaxon pour commentaires
	 * 
	 * @author Cedric 2016
	 * @param complet
	 *            = faut il charger la liste compl�te ou seulement le combo du
	 *            masque le chargement utilise un renderer pour rendre
	 *            "en gris�" les valeurs des taxons non s�lectionn�s dans le
	 *            masque
	 */
	private void initcombostade(boolean complet) {
		SousListeAffichageMasqueLot saml = SousListeAffichageMasqueLot.get_elements_list("duallist_stade",
				affichagemasquelotenbase);
		comboModelstadecomplet = new DefaultComboBoxModel<RefStade>();
		comboModelstademasque = new DefaultComboBoxModel<RefStade>();
		DefaultListSelectionModel model = new DefaultListSelectionModel();
		for (int i = 0; i < saml.size(); i++) {
			String code = ((AffichageMasqueLot) saml.get(i)).getValeur();
			boolean match = false;
			for (int j = 0; j < this.lesStades.size(); j++) {
				match = ((RefStade) this.lesStades.get(j)).getCode().equals(code);
				if (match)
					comboModelstademasque.addElement((RefStade) this.lesStades.get(j));
			}
		}
		model.addSelectionInterval(0, comboModelstademasque.getSize() - 1);
		for (int j = 0; j < comboModelstademasque.getSize(); j++) {
			comboModelstadecomplet.addElement((RefStade) comboModelstademasque.getElementAt(j));
		}
		for (int j = 0; j < this.lesStades.size(); j++) {
			if (comboModelstadecomplet.getIndexOf(this.lesStades.get(j)) == -1)
				comboModelstadecomplet.addElement((RefStade) this.lesStades.get(j));
		}
		if (complet) {
			EnabledJComboBoxRenderer enableRenderer = new EnabledJComboBoxRenderer(model);
			this.combo_tr_stade_std.setModel(comboModelstadecomplet);
			combo_tr_stade_std.setRenderer(enableRenderer);
		} else {
			this.combo_tr_stade_std.setModel(comboModelstademasque);
		}
	}

	/**
	 * Initialise les valeurs des combos � partir des valeurs du masque
	 * 
	 * @author Cedric 2016
	 * 
	 * @param complet
	 *            = faut il charger la liste compl�te ou seulement le combo du
	 *            masque
	 * 
	 */
	private void initcombopathologie(boolean complet) {

		// extrait les �l�ments de affichagemasquelotenbase seulement pour
		// le combo pathologie, normalement
		// ils sont ordonn�s par rang asc dans la requ�te de la sous liste.
		SousListeAffichageMasqueLot saml = SousListeAffichageMasqueLot.get_elements_list("duallist_pathologie",
				affichagemasquelotenbase);
		// dans le combo on a remplit un listModelpatho a partir de
		// listRefpathologie qui contient tous les details
		// les objets RefPathologie qui remplissent le combo sont donc complets,
		// et il faut aller les chercher a partir
		// de leur code (correspondant � patcode) qui est stock� dans maa_valeur
		// si un lot utilisant une patho qui n'est pas dans le masque est
		// s�lectionn�
		// l'interface basculera vers un affichage complet
		comboModelpathocomplet = new DefaultComboBoxModel<RefPathologie>();
		comboModelpathomasque = new DefaultComboBoxModel<RefPathologie>();
		DefaultListSelectionModel model = new DefaultListSelectionModel();
		//// Pour l'affichage du masque
		for (int i = 0; i < saml.size(); i++) {
			String code = ((AffichageMasqueLot) saml.get(i)).getValeur();
			boolean match = false;
			// on va chercher les �l�ments de lesPathologies qui sont les objets
			// ayant servi � remplir le combo
			for (int j = 0; j < this.lesPathologies.size(); j++) {
				match = ((RefPathologie) this.lesPathologies.get(j)).getCode().equals(code);
				if (match)
					comboModelpathomasque.addElement((RefPathologie) this.lesPathologies.get(j));

			}
		}
		// pour l'affichage complet
		// ajout des �l�ments � la suite

		model.addSelectionInterval(0, comboModelpathomasque.getSize() - 1);
		// selection des combos du masque (entre 0 et et la taille de
		// comboModelpathomasque)
		for (int j = 0; j < comboModelpathomasque.getSize(); j++) {
			comboModelpathocomplet.addElement((RefPathologie) comboModelpathomasque.getElementAt(j));
		}
		// ajout des patho manquantes au reste du combo
		for (int j = 0; j < this.lesPathologies.size(); j++) {
			if (comboModelpathocomplet.getIndexOf(this.lesPathologies.get(j)) == -1)
				comboModelpathocomplet.addElement((RefPathologie) this.lesPathologies.get(j));
		}
		if (complet) {
			// si le combo est complet utilisation d'un renderer pour "masquer"
			// les �l�ments du combo dans l'affichage
			EnabledJComboBoxRenderer enableRenderer = new EnabledJComboBoxRenderer(model);
			this.combo_pathologies.setModel(comboModelpathocomplet);
			combo_pathologies.setRenderer(enableRenderer);
		} else {
			// masque tel que demand� par l'utilisateur
			this.combo_pathologies.setModel(comboModelpathomasque);

		}
	}

	/**
	 * Initialise les valeurs du combo localisation pathologie � partir des
	 * valeurs du masque
	 * 
	 * @author Cedric 2016
	 * 
	 * @param complet
	 *            = faut il charger la liste compl�te ou seulement le combo du
	 *            masque
	 * 
	 */
	private void initcombolocalisationpatho(boolean complet) {

		SousListeAffichageMasqueLot saml = SousListeAffichageMasqueLot.get_elements_list("duallist_localisationpatho",
				affichagemasquelotenbase);

		comboModellocalisationpathocomplet = new DefaultComboBoxModel<RefLocalisation>();
		comboModellocalisationpathomasque = new DefaultComboBoxModel<RefLocalisation>();
		DefaultListSelectionModel model = new DefaultListSelectionModel();
		//// Pour l'affichage du masque
		for (int i = 0; i < saml.size(); i++) {
			String code = ((AffichageMasqueLot) saml.get(i)).getValeur();
			boolean match = false;
			for (int j = 0; j < this.lesLocalisations.size(); j++) {
				match = ((RefLocalisation) this.lesLocalisations.get(j)).getCode().equals(code);
				if (match)
					comboModellocalisationpathomasque.addElement((RefLocalisation) this.lesLocalisations.get(j));
			}
		}
		// pour l'affichage complet
		// ajout des �l�ments � la suite

		model.addSelectionInterval(0, comboModellocalisationpathomasque.getSize() - 1);
		// selection des combos du masque (entre 0 et et la taille de
		// comboModellocalisationpathomasque)
		for (int j = 0; j < comboModellocalisationpathomasque.getSize(); j++) {
			comboModellocalisationpathocomplet
					.addElement((RefLocalisation) comboModellocalisationpathomasque.getElementAt(j));
		}
		// ajout des localisationpatho manquantes au reste du combo
		for (int j = 0; j < this.lesLocalisations.size(); j++) {
			if (comboModellocalisationpathocomplet.getIndexOf(this.lesLocalisations.get(j)) == -1)
				comboModellocalisationpathocomplet.addElement((RefLocalisation) this.lesLocalisations.get(j));
		}
		if (complet) {
			// si le combo est complet utilisation d'un renderer pour "masquer"
			// les �l�ments du combo dans l'affichage
			EnabledJComboBoxRenderer enableRenderer = new EnabledJComboBoxRenderer(model);
			this.combo_localisationPathologie.setModel(comboModellocalisationpathocomplet);
			combo_localisationPathologie.setRenderer(enableRenderer);
		} else {
			// masque tel que demand� par l'utilisateur
			this.combo_localisationPathologie.setModel(comboModellocalisationpathomasque);

		}
	}

	/**
	 * Pour reselectionne les meme elements dans les combo que ceux qui sont ete
	 * selectionnes la derniere fois concerne la precision, lengthtaxon, et
	 * lengthtype de quantite suppression de cette fonctionnalit�
	 * 
	 * @author cedric
	 */
	// private void initElementsSelectionnes() {
	// Integer valsel;
	//
	// // System.out.println("initElementsSelectionnes") ;
	//
	// // Type de quantite du lot
	// Integer quantlot = ((Integer)
	// Lanceur.getElementsSelectionnes().get("tr_typequantitelot_qte"));
	// if (quantlot != null) {
	// this.combo_tr_typequantitelot_qte.setSelectedIndex(quantlot);
	// }
	//
	// // r�cup�ration de la derni�re valeur utilis�e dans l'interface
	// valsel = (Integer) Lanceur.getElementsSelectionnes().get("tr_taxon_tax");
	// if (valsel != null) {
	// this.combo_tr_taxon_tax.setSelectedIndex(valsel);
	// }
	//
	// valsel = (Integer) Lanceur.getElementsSelectionnes().get("tr_stade_std");
	// if (valsel != null) {
	// this.combo_tr_stade_std.setSelectedIndex(valsel);
	// }
	//
	// // Precision d'une caracteristique
	// String car_precision = (String)
	// Lanceur.getElementsSelectionnes().get("car_precision");
	// if (car_precision != null) {
	// this.textfielddt_car_precision.setText(car_precision); //
	// }
	//
	// // Precision d'une caracteristique Taille
	// String car_precisionTaille = (String)
	// Lanceur.getElementsSelectionnes().get("car_precisionTaille");
	// if (car_precisionTaille != null) {
	// this.textfielddt_car_precisionTaille.setText(car_precisionTaille);
	// }
	// // Precision d'une caracteristique Poids
	// String car_precisionPoids = (String)
	// Lanceur.getElementsSelectionnes().get("car_precisionPoids");
	// if (car_precisionPoids != null) {
	// this.textfielddt_car_precisionPoids.setText(car_precisionPoids);
	// }
	// }
	/**
	 * Pour les informations du lot (g�n�r�es par le constructeur, remplacement
	 * des valeurs par d�faut � l'aide de chargeMasque Pour les
	 * caract�ristiques, il faut aller les chercher et remettre les valeurs par
	 * d�faut, on ne peut pas appeller la m�thode initCaracteristiquesduMasque()
	 * car cela conduit � doubler les composants. La m�thode est proche de
	 * initCaracteristiquesduMasquePourLot() mais au lieu de charger les valeurs
	 * du lot, on charge les valeurs du masque.
	 */
	private void clearvalues_lot() {
		this.chargeMasque();
		for (int i = 0; i < listofcomponent.size(); i++) {
			CaracteristiqueMasqueLot car = (CaracteristiqueMasqueLot) caracteristiquemasquelotenbase.get(i);
			RefParametre param = car.getCaracteristiquedefaut().getParametre();
			String codepar = param.getCode();
			// si qualitatif on reprend la caract�ristique par d�faut du masque
			if (vect_estqualitatif[i]) {
				// r�cup�ration du combo dans la liste
				JComboBox<RefValeurParametre> combo = (JComboBox<RefValeurParametre>) listofcomponent.get(i);
				// r�cup�ration de la sous liste des valeurs param�tre dans la
				// liste
				SousListeRefValeurParametre lesValeurQualitatives = (SousListeRefValeurParametre) vect_lesvaleurparametre[i];
				RefValeurParametre valeur_selectionnee_defaut = car.getCaracteristiquedefaut()
						.getValeurParametreQualitatif();
				int index_val_select_defaut = -1; // par d�faut rien n'est
				// s�lectionn�
				combo.setSelectedIndex(index_val_select_defaut);
			} else {
				// si quantitatif affichage de la valeur par d�faut ou null
				// aux �l�ments avec i
				JFormattedTextField textfield_valeur = (JFormattedTextField) listofcomponent.get(i);
				Float valeur_defaut = car.getCaracteristiquedefaut().getValeurParametreQuantitatif();
				if (valeur_defaut != null & valeur_defaut != 0) {
					textfield_valeur.setValue(valeur_defaut);
				} else {
					textfield_valeur.setValue(null);
				}

			}
		}
	}

	/**
	 * Affiche les informations du lot courant
	 * 
	 * @throws Exception
	 */
	private void afficheLot() throws Exception {
		// seulement si un est definit
		String res = "";
		if (this.lot != null) {
			int index = this.comboModeltaxonmasque.getIndexOf(this.lot.getTaxon());
			if (index == -1) {
				initcombotaxon(true);

				res = "Chargement de tous les taxons ";
			}
			this.combo_tr_taxon_tax.setSelectedItem(this.lot.getTaxon());
			index = this.comboModelstademasque.getIndexOf(this.lot.getStadeDeveloppement());
			if (index == -1) {
				initcombostade(true);
				res = res + "chargement de tous les stades";
			}
			this.combo_tr_stade_std.setSelectedItem(this.lot.getStadeDeveloppement());
			this.combo_tr_devenirlot_dev.setSelectedItem(this.lot.getDevenir());

			// Si le type de quantite est null, il s'agit d'un effectif, donc
			// affichage de 'Individus' (le 1er de la liste)
			if (this.lot.getTypeQuantite() == null) {
				this.combo_tr_typequantitelot_qte.setSelectedIndex(0);
			} else {
				this.combo_tr_typequantitelot_qte.setSelectedItem(this.lot.getTypeQuantite());
			}

			this.combo_lot_methode_obtention.setSelectedItem(this.lot.getMethodeObtention());
			// affichage soit de l'effectif, soit de la quantite
			if (this.lot.getValeurQuantite() != null) {
				this.textfielddt_lot_quantite.setText(this.lot.getValeurQuantite().toString());
			} else if (this.lot.getEffectif() != null) {
				this.textfielddt_lot_quantite.setText(this.lot.getEffectif().toString());
			}
			this.textarea_lot_commentaires.setText(this.lot.getCommentaires());

			lesPathologiesConstatees = new SousListePathologieConstatee(this.lot);
			lesPathologiesConstatees.chargeSansFiltreDetails(); // chargement
			this.lot.setLesPathologiesConstatees(lesPathologiesConstatees);
			// des
			// RefLocalosation
			// et
			// refPathologies
			this.listmodelpathologieconstatees.clear();
			for (int i = 0; i < lesPathologiesConstatees.size(); i++) {
				PathologieConstatee pathologieconstatee = (PathologieConstatee) lesPathologiesConstatees.get(i);
				((DefaultListModel<PathologieConstatee>) this.listmodelpathologieconstatees)
						.addElement((PathologieConstatee) pathologieconstatee);
			}
			this.jList_PathologiesConstatees.setModel(listmodelpathologieconstatees);

			lesMarquages = new SousListeMarquages(this.lot);
			lesMarquages.chargeSansFiltreDetails();
			this.listmodelMarquage.clear();
			this.lot.setLesMarquages(lesMarquages);
			for (int i = 0; i < lesMarquages.size(); i++) {
				((DefaultListModel<Marquage>) this.listmodelMarquage).addElement((Marquage) this.lesMarquages.get(i));
			}

			this.jList_marquages.setModel(listmodelMarquage);
			this.textfielddt_refMarque.setEditable(true);

			lesPrelevementsDuLot = new SousListePrelevement(this.lot);
			lesPrelevementsDuLot.chargeSansFiltreDetails();
			// TODO
			// this.lot.setLesPrelevements(lesPrelevementsDuLot);
			this.listmodelprelevement.clear();
			for (int i = 0; i < lesPrelevementsDuLot.size(); i++) {
				((DefaultListModel<Prelevement>) this.listmodelprelevement)
						.addElement((Prelevement) this.lesPrelevementsDuLot.get(i));
			}

			this.jList_listePrelevements.setModel(listmodelprelevement);
			this.resetaffichageMarque();
			this.initCaracteristiques();
			this.initCaracteristiquesduMasquePourLot(); // ongletlot
			this.resultat.setText(res);
		} // end this.lot==null

	}

	private void expandAll(JTree tree, TreePath parent) {
		TreeNode node = (TreeNode) parent.getLastPathComponent();
		if (node.getChildCount() >= 0) {
			for (Enumeration e = node.children(); e.hasMoreElements();) {
				TreeNode n = (TreeNode) e.nextElement();
				TreePath path = parent.pathByAddingChild(n);
				expandAll(tree, path);
			}
		}
		tree.expandPath(parent);
		// tree.collapsePath(parent);
	}

	/**
	 * Selectionne le lot dans l'arbre
	 */
	private TreePath selectionne_dans_larbre(Lot lot) {
		TreePath path = null;
		DefaultMutableTreeNode noeudSelectionne;

		// Si lengthlot courant est definit
		if (this.lot != null) {
			// this.TabbedPaneOngletsLots.setSelectedComponent(panel_infosLots);
			// Si le lot n'est pas un echantillon
			// if (this.lot.getLotParent() == null) {
			noeudSelectionne = (DefaultMutableTreeNode) this.lesNoeuds.get(this.lot.getIdentifiant());
			// }
			// Si le lot est un echantillon
			// else {
			// noeudSelectionne =
			// (DefaultMutableTreeNode)this.lesNoeuds.get(this.lot.getLotParent().getIdentifiant())
			// ;
			// }
			path = new TreePath(((DefaultTreeModel) this.arbreLots.getModel()).getPathToRoot(noeudSelectionne));
			//
			this.arbreLots.setSelectionPath(path);
			this.expandAll(arbreLots, path);
			this.arbreLots.scrollPathToVisible(path);

		}
		return (path);
	}

	/**
	 * Selectionne la caracterisique dans l'arbre
	 */
	private void selectionne_dans_larbre(Caracteristique car) {
		DefaultMutableTreeNode noeudcar = (DefaultMutableTreeNode) this.lesNoeuds
				.get(car.getLot().getIdentifiant() + "car" + car.getParametre().getCode());
		TreePath tpathcar = new TreePath(((DefaultTreeModel) this.arbreLots.getModel()).getPathToRoot(noeudcar));
		this.arbreLots.getSelectionModel().setSelectionPath(tpathcar);
		this.arbreLots.expandPath(tpathcar);
		this.lot = car.getLot();
		this.arbrelot_selected();
	}

	/**
	 * Selectionne la pathologie dans l'arbre
	 */
	private void selectionne_dans_larbre(PathologieConstatee patho) {
		DefaultMutableTreeNode noeudpatho = (DefaultMutableTreeNode) this.lesNoeuds
				.get(patho.getLot().getIdentifiant() + "patho" + patho.toString());
		TreePath tpathpatho = new TreePath(((DefaultTreeModel) this.arbreLots.getModel()).getPathToRoot(noeudpatho));
		this.arbreLots.getSelectionModel().setSelectionPath(tpathpatho);
		this.arbreLots.expandPath(tpathpatho);
		this.lot = patho.getLot();
		this.arbrelot_selected();
	}

	/**
	 * Selectionne le marquage dans l'arbre
	 */
	private void selectionne_dans_larbre(Marquage marquage) {
		DefaultMutableTreeNode noeudmarquage = (DefaultMutableTreeNode) this.lesNoeuds
				.get(marquage.getLot().getIdentifiant() + "mqe" + marquage.getMarque().getReference());
		TreePath tpathmarquage = new TreePath(
				((DefaultTreeModel) this.arbreLots.getModel()).getPathToRoot(noeudmarquage));
		this.arbreLots.getSelectionModel().setSelectionPath(tpathmarquage);
		this.arbreLots.expandPath(tpathmarquage);
		this.lot = marquage.getLot();
		this.arbrelot_selected();
	}

	/**
	 * @author cedric M�thode lanc�e lorsqu'on clique sur le Jtree, affiche les
	 *         infos du lot courant, de la caract�ristique, patho ou du marquage
	 *         selectionn�, set la valeur du lot au lot des �l�ments
	 *         s�lectionn�s (patho ou caract ou marquage).
	 */
	private void arbrelot_selected() {
		try {
			// Seulement si ce n'est pas le noeud racine
			if (this.arbreLots.getMinSelectionRow() > 0) {
				// seulement si une operation courante est definie
				if ((this.ope != null) && (this.ope.getLesLots() != null) && (this.ope.getLesLots().size() > 0)) {
					// SousListeLots lesLots = this.ope.getLesLots() ;
					DefaultMutableTreeNode node = (DefaultMutableTreeNode) this.arbreLots.getSelectionPath()
							.getLastPathComponent();
					Object objet = (Object) node.getUserObject();
					if (objet instanceof Lot) {
						this.lot = (Lot) objet;
						this.afficheLot();
						this.TabbedPaneOngletsLots.setSelectedComponent(panel_infosLots);
						// DefaultMutableTreeNode noeudSelectionne =
						// (DefaultMutableTreeNode) this.lesNoeuds
						// .get(this.lot.getIdentifiant());
						// TreePath path = new TreePath(
						// ((DefaultTreeModel)
						// this.arbreLots.getModel()).getPathToRoot(noeudSelectionne));
						// // this.expandAll(arbreLots, path);

					} else {
						this.lot = (Lot) ((DefaultMutableTreeNode) node.getParent()).getUserObject();
						this.afficheLot();
						if (objet instanceof Caracteristique) {
							Caracteristique car = (Caracteristique) objet;
							this.jlist_car.setSelectedValue(car, true);
							// affiche caracteristique affiche la caract
							// selectionnee dans le combo
							this.afficheCaracteristique(); // onglet
							// caracteristique
							this.TabbedPaneOngletsLots.setSelectedComponent(panel_caracteristiques);

						} else if (objet instanceof Marquage) {
							Marquage marquage = (Marquage) objet;

							Integer index = this.lesMarquages
									.getindice(this.lot.getIdentifiant() + marquage.getMarque().getReference());
							this.jList_marquages.setSelectedIndex(index);

							this.afficheMarque();
							this.TabbedPaneOngletsLots.setSelectedComponent(panel_marques);
							this.validate();
							this.repaint();

						} else if (objet instanceof PathologieConstatee) {
							PathologieConstatee patho = (PathologieConstatee) objet;
							Integer index = this.lesPathologiesConstatees.getindice(this.lot.getIdentifiant()
									+ patho.getPathologie().getCode() + patho.getLocalisation().getCode());
							this.jList_PathologiesConstatees.setSelectedIndex(index);
							this.affichePathologies();
							this.TabbedPaneOngletsLots.setSelectedComponent(panel_pathologies);
						}
					}
				} else {
					this.reInitComponents();
				}
			} else {
				// le clic sur la valeur racine am�ne � effacer les valeurs du
				// combo
				this.clearvalues_lot();
			}
		} catch (Exception e) {
			this.resultat.setText(Erreur.I1005); // si echec
			logger.log(Level.SEVERE, " arbreLotValueChanged ", e);
		}
	}

	/**
	 * @author cedric Pour l'affichage des informations sur les lots de
	 *         l'operation en cours met a jour le treepath cr�e des vecteurs et
	 *         affiche les informations de l'operation
	 */

	private void initInfosLots() {

		try {
			Lot lotTemp;

			this.lesNoeuds = new Hashtable();

			DefaultMutableTreeNode root = new DefaultMutableTreeNode(this.ope.toStringDetails());
			treeModel = new DefaultTreeModel(root, true);

			// String esp = " | ";
			// String taxon = StringUtils.rightPad(" taxon", 34, " ");
			// String stade = StringUtils.rightPad(" stade", 22, " ");
			// String effectif = StringUtils.rightPad("nb/qte", 6, " ");
			// String idlot = StringUtils.rightPad(" lot", 15, " ");
			// String idlotpere = StringUtils.rightPad(" lot p�re", 15, " ");
			//
			// DefaultMutableTreeNode legende = new DefaultMutableTreeNode(
			// taxon + esp + stade + esp + effectif + esp + idlot + esp +
			// idlotpere, false);
			// treeModel.insertNodeInto(legende, root, 0);

			// Premier parcours : uniquement les lot qui ne sont pas des
			// echantillons
			// Ceci pour etre sur que les noeuds parents sont crees avant les
			// noeuds enfants
			for (Enumeration<Object> e = this.ope.getLesLots().elements(); e.hasMoreElements();) {

				lotTemp = (Lot) e.nextElement();

				// Si le lot n'est pas un echantillon, creation d'un noeud
				if (lotTemp.getLotParent() == null) {

					DefaultMutableTreeNode noeud = new DefaultMutableTreeNode(lotTemp, true);
					this.lesNoeuds.put(lotTemp.getIdentifiant(), noeud);
					treeModel.insertNodeInto(noeud, root, treeModel.getChildCount(root));

					SousListeCaracteristiques lescarac = lotTemp.getLesCaracteristiques();
					if (!lescarac.isEmpty()) {
						for (int i = 0; i < lescarac.size(); i++) {
							Caracteristique carac = (Caracteristique) lescarac.get(i);
							DefaultMutableTreeNode noeudcar = new DefaultMutableTreeNode(carac, true);
							treeModel.insertNodeInto(noeudcar, noeud, treeModel.getChildCount(noeud));
							this.lesNoeuds.put(lotTemp.getIdentifiant() + "car" + carac.getParametre().getCode(),
									noeudcar);
						}

					}
					SousListeMarquages lesmarquages = lotTemp.getLesMarquages();
					if (!lesmarquages.isEmpty()) {
						for (int i = 0; i < lesmarquages.size(); i++) {
							Marquage marquage = (Marquage) lesmarquages.get(i);
							DefaultMutableTreeNode noeudmarquage = new DefaultMutableTreeNode(marquage, true);
							treeModel.insertNodeInto(noeudmarquage, noeud, treeModel.getChildCount(noeud));
							this.lesNoeuds.put(lotTemp.getIdentifiant() + "mqe" + marquage.getMarque().getReference(),
									noeudmarquage);
						}
					}
					SousListePathologieConstatee lespatho = lotTemp.getLesPathologiesConstatees();
					if (!lespatho.isEmpty()) {
						for (int i = 0; i < lespatho.size(); i++) {
							PathologieConstatee patho = (PathologieConstatee) lespatho.get(i);
							DefaultMutableTreeNode noeudpatho = new DefaultMutableTreeNode(patho, true);
							treeModel.insertNodeInto(noeudpatho, noeud, treeModel.getChildCount(noeud));
							this.lesNoeuds.put(lotTemp.getIdentifiant() + "patho" + patho.toString(), noeudpatho);
						}
					}

				}
			}

			// Second parcours : uniquement les sous-lots
			for (Enumeration<Object> e = this.ope.getLesLots().elements(); e.hasMoreElements();) {

				lotTemp = (Lot) e.nextElement();

				// Si le lot est un echantillon, rattachement au noeud parent
				if (lotTemp.getLotParent() != null) {

					DefaultMutableTreeNode noeud = new DefaultMutableTreeNode(lotTemp, true);

					DefaultMutableTreeNode noeudPere = (DefaultMutableTreeNode) this.lesNoeuds
							.get(lotTemp.getLotParent().getIdentifiant());
					this.lesNoeuds.put(lotTemp.getIdentifiant(), noeud);

					treeModel.insertNodeInto(noeud, noeudPere, treeModel.getChildCount(noeudPere));

					SousListeCaracteristiques lescarac = lotTemp.getLesCaracteristiques();
					if (!lescarac.isEmpty()) {
						for (int i = 0; i < lescarac.size(); i++) {
							Caracteristique carac = (Caracteristique) lescarac.get(i);
							DefaultMutableTreeNode noeudcar = new DefaultMutableTreeNode(carac, true);
							treeModel.insertNodeInto(noeudcar, noeud, treeModel.getChildCount(noeud));
							this.lesNoeuds.put(lotTemp.getIdentifiant() + "car" + carac.getParametre().getCode(),
									noeud);
						}
					}

					SousListeMarquages lesmarquages = lotTemp.getLesMarquages();
					if (!lesmarquages.isEmpty()) {
						for (int i = 0; i < lesmarquages.size(); i++) {
							Marquage marquage = (Marquage) lesmarquages.get(i);
							DefaultMutableTreeNode noeudmarquage = new DefaultMutableTreeNode(marquage, true);
							treeModel.insertNodeInto(noeudmarquage, noeud, treeModel.getChildCount(noeud));
							this.lesNoeuds.put(lotTemp.getIdentifiant() + "mqe" + marquage.getMarque().getReference(),
									noeudmarquage);
						}
					}

					SousListePathologieConstatee lespatho = lotTemp.getLesPathologiesConstatees();
					if (!lespatho.isEmpty()) {
						for (int i = 0; i < lespatho.size(); i++) {
							PathologieConstatee patho = (PathologieConstatee) lespatho.get(i);
							DefaultMutableTreeNode noeudpatho = new DefaultMutableTreeNode(patho, true);
							treeModel.insertNodeInto(noeudpatho, noeud, treeModel.getChildCount(noeud));
							this.lesNoeuds.put(lotTemp.getIdentifiant() + "patho" + patho.toString(), noeudpatho);
						}
					}
				}
			}

			// Affectation du modele de donnees a l'objet graphique
			this.arbreLots.setModel(treeModel);
			this.arbreLots.setCellRenderer(new LotTreeCellRenderer());

			// Selection d'un seul element de l'arbre a la fois
			this.arbreLots.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

			// Initialise la liste des caracteristiques du lot courant
			this.initCaracteristiques();

			// // initialise la liste des pathologies constat�es sur le lot
			// this.initPathologies();

			this.vectorLocalisation = convertToVector(this.lesLocalisations);
			this.vectorPathologies = convertToVector(this.lesPathologies);
			this.vectorImportance = convertToVector(this.lesimportancespathologie);
			this.vectorNatureMarque = convertToVector(this.lesNaturesMarques);
			this.vectorOperationMarquage = convertToVector(this.lesOperationsMarquages);
			this.vectorPrelevement = convertToVector(this.lesPrelevements);
			// affiche les effectifs des lots de l'op�ration
			this.afficheeffectifope();
		} catch (Exception e) {
			this.resultat.setText(Erreur.I1003); // Echec
			logger.log(Level.SEVERE, " initInfosLots  ", e);
			// System.out.println(e.getMessage()) ;
			// e.printStackTrace() ;
		}
	}

	/**
	 * Methode a charger apr�s le constructeur pour rappeler le focus au bon
	 * endroit de la fen�tre Comme un requestfocus sur un composant inactif ne
	 * marche pas il faut tous les appeller.
	 */
	private void resetfocusonlot() {

		if (affichage[0]) {
			this.combo_tr_taxon_tax.requestFocus();
		} else if (affichage[1]) {
			this.combo_tr_stade_std.requestFocus();
		} else if (affichage[3]) {
			this.textfielddt_lot_quantite.requestFocus();
		} else if (affichage[4]) {
			this.combo_lot_methode_obtention.requestFocus();
		} else if (affichage[5]) {
			this.combo_tr_devenirlot_dev.requestFocus();
		} else if (affichage[6]) {
			this.textarea_lot_commentaires.requestFocus();
		} else {
			boolean caracteristique_focus_realise = false;
			// caracteristique de lot
			for (int i = 0; i < caracteristiquemasquelotenbase.size(); i++) {
				CaracteristiqueMasqueLot car = (CaracteristiqueMasqueLot) caracteristiquemasquelotenbase.get(i);
				Boolean affichage_valeur = car.isAffichage_valeur();
				if (affichage_valeur & !caracteristique_focus_realise) {
					this.listofcomponent.get(i).requestFocus();
					caracteristique_focus_realise = true;
				}
			}
		}
	}

	/**
	 * Enregistre � la fois le lot et les caract�ristiques s�lectionn�es dans le
	 * masque
	 * 
	 * @param _lotParent
	 * @throws Exception
	 */
	private void enregistreLotCar(Lot _lotParent) throws Exception {
		String result;
		result = this.enregistreLot(_lotParent);
		result = result + " " + this.chargeSaveCaracteristiqueMasque();
		this.initInfosLots(); // remet � jour l'arbre
		if (!(_lotParent == null)) {
			// affichage du lot p�re dans l'arbre
			this.selectionne_dans_larbre(lot);
		} else if (this.lot != null) {
			// le lot est alors le lot en cours, l'affichage se fait pour
			// amener le curseur dans le Jtree
			this.selectionne_dans_larbre(this.lot);
		}
		this.resetfocusonlot(); // remet le focus (curseur courant) au bon
		// dans le panel infolots
		this.clearvalues_lot();
		this.resultat.setText(result);

	}

	//
	/**
	 * Enregistre le lot courant
	 * 
	 * @param _lotParent
	 */
	private String enregistreLot(Lot _lotParent) throws Exception {

		// Objet a creer
		Lot lotAEnregistrer;

		// Resultat affiche a l'utilisateur
		String result = null;

		// Attributs de l'objet a creer
		Integer identifiant = null;
		Operation operationRattachement = null;
		RefTaxon taxon = null;
		RefStade stadeDeveloppement = null;
		Float effectif = null;
		Float valeurQuantite = null;
		RefTypeQuantite typeQuantite = null;
		String methodeObtention = null;
		Lot lotParent = null;
		RefDevenir devenir = null;
		String commentaires = null;
		SousListeCaracteristiques lesCaracteristiques = null;
		SousListePathologieConstatee lesPathologiesConstatees = null;
		SousListeMarquages lesMarquages = null;

		// Initialisation des attributs d'apres les saisies de l'utilisateur et
		// verification que les valeurs sont bien du type attendu

		// identifiant : autoincremente

		try {
			operationRattachement = this.ope;
			if (operationRattachement == null) { // normalement il y a une
				// operation
				throw new Exception(Erreur.B1028);
			}
		} catch (Exception e) {
			result = e.getMessage();
			logger.log(Level.SEVERE, " enregistreLot  ", e);
		}

		try {
			taxon = (RefTaxon) this.combo_tr_taxon_tax.getSelectedItem();
		} catch (Exception e) {
			result = Erreur.S0100 + this.label_taxon.getText();
		}

		try {
			stadeDeveloppement = (RefStade) this.combo_tr_stade_std.getSelectedItem();
		} catch (Exception e) {
			result = Erreur.S0100 + this.label_stade.getText();
			logger.log(Level.SEVERE, " enregistreLot " + Erreur.S0100, e);
		}

		try {
			typeQuantite = (RefTypeQuantite) this.combo_tr_typequantitelot_qte.getSelectedItem();
			// si l'utilisateur a choisi un effectif (type nombre d'individus),
			// initialise le float effectif
			if (typeQuantite.getCode() == quantiteIndividus) {
				effectif = this.textfielddt_lot_quantite.getFloat();
				typeQuantite = null;
			}
			// sinon, l'utilisateur a choisit un autre type de quantite
			else {
				valeurQuantite = this.textfielddt_lot_quantite.getFloat();
			}
		} catch (Exception e) {
			result = Erreur.S0100 + this.label_quantite.getText();
			logger.log(Level.SEVERE, " enregistreLot " + Erreur.S0100, e);
		}

		try {
			methodeObtention = (String) this.combo_lot_methode_obtention.getSelectedItem();
		} catch (Exception e) {
			result = Erreur.S0100 + this.label_methodelot.getText();
			logger.log(Level.SEVERE, " enregistreLot " + Erreur.S0100, e);
		}

		try {
			lotParent = _lotParent;
		} catch (Exception e) {
			result = Erreur.S4006;
		}

		try {
			devenir = (RefDevenir) this.combo_tr_devenirlot_dev.getSelectedItem();
		} catch (Exception e) {
			result = Erreur.S0100 + this.label_devenirlot.getText();
			logger.log(Level.SEVERE, " enregistreLot " + Erreur.S0100, e);
		}

		try {
			commentaires = this.textarea_lot_commentaires.getString();
		} catch (Exception e) {
			result = Erreur.S0100 + this.label_commentaireslot.getText();
			logger.log(Level.SEVERE, " enregistreLot " + Erreur.S0100, e);
		}

		// Si aucune erreur jusque la, les champs sont soit null soit du bon
		// type, mais les contraintes de domaine ne sont pas forcement verifiees
		if (result == null) {
			try {
				// Si modification du lot courant, recuperation de l'identifiant
				// de ce lot et des listes
				if (this.lot != null) {
					identifiant = this.lot.getIdentifiant();
					lesMarquages = this.lot.getLesMarquages();
					lesCaracteristiques = this.lot.getLesCaracteristiques();
					lesPathologiesConstatees = this.lot.getLesPathologiesConstatees();
					lesMarquages = this.lot.getLesMarquages();
				}
				// Sinon, l'identifiant reste null et sera autoincremente, et
				// creation de listes vides
				else {
					identifiant = null;
					lesPathologiesConstatees = new SousListePathologieConstatee(this.lot);
					lesMarquages = new SousListeMarquages(this.lot);
					lesCaracteristiques = new SousListeCaracteristiques(this.lot);
					lesMarquages = new SousListeMarquages(this.lot);
				}

				// Creation de l'objet et verification de sa validite
				lotAEnregistrer = new Lot(identifiant, operationRattachement, taxon, stadeDeveloppement, effectif,
						valeurQuantite, typeQuantite, methodeObtention, lotParent, devenir, commentaires,
						lesPathologiesConstatees, lesMarquages, lesCaracteristiques);

				lotAEnregistrer.verifAttributs();

				// Enregistrement dans la BD
				// Si modification du lot courant
				if (this.lot != null) {
					lotAEnregistrer.majObjet();

					// Mise a jour de ce lot dans la liste des lots de
					// l'operation
					if (this.ope.getLesLots() != null) {
						this.ope.getLesLots().replace(lotAEnregistrer.getIdentifiant(), lotAEnregistrer);
					}
					result = Message.M1000; // reussite
					logger.log(Level.INFO, " enregistreLot " + Message.M1000);
				}
				// Sinon, ajout du lot
				else {
					// try{
					lotAEnregistrer.insertObjet();

					// ajout du lot a la liste des lots de l'operation
					// courante
					if (this.ope.getLesLots() == null) {
						this.ope.setLesLots(new SousListeLots(this.ope));
					}
					;
					this.ope.getLesLots().put(lotAEnregistrer.getIdentifiant(), lotAEnregistrer);

					result = Message.A1002; // reussite
					this.resultat.setText(result);
					logger.log(Level.INFO, " enregistreLot " + Message.A1002);
				}

				// Met a jour la reference sur le lot courant
				this.lot = lotAEnregistrer;

				// Reinitialisation de la fenetre
				// this.reInitComponents() ;

				// } catch (Exception e) {
				// result = e.getMessage(); // erreur
				// } finally {
				// // Affichage du resultat
				// this.resultat.setText(result);
				// }
			} catch (DataFormatException e) {
				result = e.getMessage(); // erreur
				logger.log(Level.SEVERE, " enregistreLot ", e);

				// Affichage du resultat sans reinitialisation
				this.resultat.setText(result);
			}
		} else {
			// Affichage du resultat sans reinitialisation
			this.resultat.setText(result);
		}
		return (result);

	}

	/**
	 * Supprime le lot courant. Cela supprime aussi toutes les infos rattachees
	 * au lot : caracteristiques, marquages, pathologies
	 */
	private void supprimerLot() {
		Lot lotAEffacer = this.lot;

		// seulement si un lot est definit
		if (lotAEffacer != null) {
			try {
				// efface l'enregistrement de la BD
				// suppression des lots fils
				SousListeLots listelot = new SousListeLots(null);
				listelot.chargeFiltreParentDetails(lotAEffacer);
				// ici la fonction souslistelot est programm�e pour faire une
				// sous liste par rapport � un
				// objet p�re op�ration. On la charge avec un objet null et on
				// utilise la fonction chargeFiltreParent
				// qui va chercher les lots fils.

				for (int i = 0; i < listelot.size(); i++) {
					Lot echantillon = (Lot) listelot.get(i);
					SousListeCaracteristiques slc = echantillon.getLesCaracteristiques();
					for (int j = 0; j < slc.size(); j++)
						((Caracteristique) slc.get(j)).effaceObjet();
					SousListeMarquages mrq = echantillon.getLesMarquages();
					for (int j = 0; j < mrq.size(); j++)
						((Marquage) mrq.get(j)).effaceObjet();
					SousListePathologieConstatee pco = echantillon.getLesPathologiesConstatees();
					for (int j = 0; j < pco.size(); j++)
						((PathologieConstatee) pco.get(j)).effaceObjet();
					// SousListePrelevement
					SousListePrelevement prel = new SousListePrelevement(echantillon);
					prel.chargeSansFiltreDetails();
					for (int j = 0; j < prel.size(); j++)
						((Prelevement) prel.get(j)).effaceObjet();

					echantillon.effaceObjet();
					// on enl�ve les souslot de l'op�ration
					this.ope.getLesLots().removeObject(echantillon.getIdentifiant());
				}
				// supression du lot p�re
				SousListeCaracteristiques slc = lotAEffacer.getLesCaracteristiques();
				for (int j = 0; j < slc.size(); j++)
					((Caracteristique) slc.get(j)).effaceObjet();
				SousListeMarquages mrq = lotAEffacer.getLesMarquages();
				for (int j = 0; j < mrq.size(); j++)
					((Marquage) mrq.get(j)).effaceObjet();
				SousListePathologieConstatee pco = lotAEffacer.getLesPathologiesConstatees();
				for (int j = 0; j < pco.size(); j++)
					((PathologieConstatee) pco.get(j)).effaceObjet();

				// SousListePrelevement
				SousListePrelevement prel = new SousListePrelevement(lotAEffacer);
				prel.chargeSansFiltreDetails();
				for (int j = 0; j < prel.size(); j++)
					((Prelevement) prel.get(j)).effaceObjet();
				lotAEffacer.effaceObjet();
				this.previousLot(); // pour l'affichage, decale au lot precedant

				// Si le lot courant n'a pas ete decale, cela signifie que
				// lengthlot supprime et etait le dernier
				if (this.lot == lotAEffacer) {
					// il n'y a donc plus de lot courant
					// this.lot = null ;
					this.reInitComponents();
				}
				// Mise a jour de la liste des lots de l'operation de
				// rattachement
				if (this.ope.getLesLots() != null) {
					this.ope.getLesLots().removeObject(lotAEffacer.getIdentifiant());
				}

				this.resultat.setText(Message.S1000); // reussite
			} catch (Exception e) {
				this.resultat.setText(e.getMessage()); // si echec
			}
		}
	}

	/**
	 * Ajoute une operation avec les memes infos que l'operation courante mais
	 * une periode decalee
	 */
	@SuppressWarnings("deprecation")
	private void incrementerOpe() {

		// Resultat affiche a l'utilisateur
		String result = null;

		// Objet a creer
		Operation nouvelleOpe;

		// Attributs de l'objet a creer
		Integer identifiant = null;
		DC dc = null;
		Date date_debut = null;
		Date date_fin = null;
		String organisme = null;
		String operateur = null;
		String commentaires = null;
		SousListeLots lesLots = null;

		// Initialisation des attributs d'apres l'operation courante

		// identifiant : autoincremente

		try {
			dc = this.ope.getDC();

			// preparation des dates de la nouvelle operation
			Calendar calDebut = Calendar.getInstance();
			calDebut.setTime(this.ope.getDateFin());

			// la nouvelle heure de debut est l'heure de fin de la derniere
			// operation
			date_debut = new Date((calDebut.get(Calendar.YEAR) - 1900), calDebut.get(Calendar.MONTH),
					calDebut.get(Calendar.DAY_OF_MONTH), calDebut.get(Calendar.HOUR_OF_DAY),
					calDebut.get(Calendar.MINUTE), calDebut.get(Calendar.SECOND));

			// la nouvelle heure de fin est la date de fin de la derniere
			// operation + sa duree
			Date dateFin = new Date(this.ope.getDateFin().getTime()
					+ (this.ope.getDateFin().getTime() - this.ope.getDateDebut().getTime()));

			Calendar calFin = Calendar.getInstance();
			calFin.setTime(dateFin);

			date_fin = new Date((calFin.get(Calendar.YEAR) - 1900), calFin.get(Calendar.MONTH),
					calFin.get(Calendar.DAY_OF_MONTH), calFin.get(Calendar.HOUR_OF_DAY), calFin.get(Calendar.MINUTE),
					calFin.get(Calendar.SECOND));

			organisme = this.ope.getOrganisme();
			operateur = this.ope.getOperateur();
			commentaires = this.ope.getCommentaires();

			// System.out.println(date_fin) ;
			// System.out.println(date_debut) ;

		} catch (Exception e) {
			result = Erreur.I1004;
		}

		// Si aucune erreur jusque la, les champs sont soit null soit du bon
		// type, mais les contraintes de domaine ne sont pas forcement verifiees
		if (result == null) {
			try {
				// Creation de l'objet et verification de sa validite
				nouvelleOpe = new Operation(dc, identifiant, date_debut, date_fin, organisme, operateur, commentaires,
						lesLots);

				nouvelleOpe.setLesLots(new SousListeLots(nouvelleOpe));

				nouvelleOpe.verifAttributs();

				// Enregistrement dans la BD
				try {
					nouvelleOpe.insertObjet();

					// informe le lanceur de la nouvelle ope
					Lanceur.setOperationCourante(nouvelleOpe);
					this.ope = nouvelleOpe;

					result = Message.A1001; // reussite

					this.reInitComponents();
					// Reinitialisation des infos de l'operation
					// this.initInfosLots() ;
				} catch (Exception e) {
					result = e.getMessage(); // erreur
				} finally {
					// Affichage du resultat
					this.resultat.setText(result);
				}

			} catch (DataFormatException e) {
				result = e.getMessage(); // erreur

				// Affichage du resultat sans reinitialisation
				this.resultat.setText(result);
			}
		} else {
			// Affichage du resultat sans reinitialisation
			this.resultat.setText(result);
		}
	}

	/**
	 * Passe au lot precedent, en restant dans la meme operation. Si le lot
	 * courant est le premier, ca reboucle sur le dernier lot
	 */
	private void previousLot() {
		try {

			// seulement si un lot courant et une operation courante sont
			// definis
			if ((this.ope != null) && (this.ope.getLesLots() != null) && (this.lot != null)) {
				SousListeLots lesLots = this.ope.getLesLots();
				int index;

				// recherche l'indice auquel se trouve le lot courant dans la
				// liste
				index = lesLots.indexOf(this.lot);

				// recupere le lot precedent pour le placer comme lot courant
				// si le lot est le premier, reboucle sur le dernier
				if (index == 0) {
					index = (lesLots.size() - 1);
				} else {
					index--;
				}
				this.lot = (Lot) lesLots.get(index);
				this.afficheLot();
				this.selectionne_dans_larbre(this.lot);

				// Oublie la caracteristique actuelle
				// this.car = null ;
			}
		} catch (Exception e) {
			this.resultat.setText(Erreur.I1005); // si echec
		}
	}

	//
	/**
	 * Passe au lot suivant, en restant dans la meme operation. Si le lot
	 * courant est le dernier, ca reboucle sur le premier lot
	 */
	private void nextLot() {
		try {

			// seulement si un lot courant et une operation courante sont
			// definis
			if ((this.ope != null) && (this.ope.getLesLots() != null) && (this.ope.getLesLots().size() > 0)) {
				SousListeLots lesLots = this.ope.getLesLots();
				int index;

				// Si aucun lot courant, debute au 1er lot de la liste
				if (this.lot != null) {

					// recherche l'indice auquel se trouve le lot courant dans
					// la liste
					index = lesLots.indexOf(this.lot);

					// recupere le lot suivant pour le placer comme lot courant
					// si le lot est le dernier, reboucle sur le premier
					if (index == (lesLots.size() - 1)) {
						index = 0;
					} else {
						index++;
					}
				} else {
					index = 0;
				}
				this.lot = (Lot) lesLots.get(index);

				this.afficheLot();
				this.selectionne_dans_larbre(this.lot);

				// Oublie la caracteristique actuelle
				// this.car = null ;
			}
			// sinon, reinitialise l'ecran
			else {
				this.reInitComponents();
			}

		} catch (Exception e) {
			this.resultat.setText(Erreur.I1005); // si echec
		}
	}

	/**
	 * @author cedric Passe � l'op�ration pr�c�dente, en parcourant la liste du
	 *         DC.
	 * 
	 */
	private void previousOperation() {
		try {

			// seulement si une operation courante est definie
			if (this.ope != null) {
				// charge depuis le lancer la sous liste des op�rations
				// parcourues dans l'ann�e
				SousListeOperations souslisteOperationParcourue = Lanceur.getSouslisteOperationParcourue();
				int index;

				// recherche l'indice auquel se trouve le lot courant dans la
				// liste
				index = souslisteOperationParcourue.indexOf(this.ope);

				// recupere l'operation pr�c�dente pour la placer comme
				// operationcourante
				// si l'op�ration est la premi�re, reboucle sur le dernier
				if (index == 0) {
					index = (souslisteOperationParcourue.size() - 1);
				} else {
					index--;
				}

				SousListeOperations souslisteOperation = new SousListeOperations(ope.getDC());
				this.ope = (Operation) souslisteOperationParcourue.get(index);
				// modification de ope par la methode chargeObjet
				souslisteOperation.chargeObjet(ope);

				SousListeDC sldc = new SousListeDC();
				sldc.put(ope.getDC().getIdentifiant(), ope.getDC());

				sldc.chargeObjets();

				// Informe le lanceur de l'operation courante
				Lanceur.setOperationCourante(ope);
				this.reInitComponents();

				AjouterLot.this.chargeListeOperationsMarquage();
				// Oublie la caracteristique actuelle
				// this.car = null ;
			}
		} catch (Exception e) {
			this.resultat.setText(Erreur.I1006); // si echec
		}
	}

	/**
	 * Passe � l'op�ration suivante, en parcourant la liste du DC
	 * 
	 * @author cedric
	 */
	private void nextOperation() {
		try {

			// seulement si une operation courante sont definis
			if (this.ope != null) {
				// charge depuis le lanceur la sous liste des op�rations
				// parcourues dans l'ann�e
				SousListeOperations souslisteOperationParcourue = Lanceur.getSouslisteOperationParcourue();
				int index;

				// recherche l'indice auquel se trouve le lot courant dans la
				// liste
				index = souslisteOperationParcourue.indexOf(this.ope);

				// recupere le lot suivant pour le placer comme lot courant
				// si le lot est le dernier, reboucle sur le premier
				if (index == (souslisteOperationParcourue.size() - 1)) {
					index = 0;
				} else {
					index++;
				}
				SousListeOperations souslisteOperation = new SousListeOperations(ope.getDC());
				this.ope = (Operation) souslisteOperationParcourue.get(index);
				// modification de ope par la methode chargeObjet
				souslisteOperation.chargeObjet(ope);

				SousListeDC sldc = new SousListeDC();
				sldc.put(ope.getDC().getIdentifiant(), ope.getDC());

				sldc.chargeObjets();

				// Informe le lanceur de l'operation courante
				Lanceur.setOperationCourante(ope);
				this.reInitComponents();
				AjouterLot.this.chargeListeOperationsMarquage();
			}
		} catch (Exception e) {
			this.resultat.setText(Erreur.I1006); // si echec
		}
	}

	// ***********************************************
	//
	// ONGLET CARACTERISTIQUES
	//
	// ***********************************************

	/**
	 * Reinitialise l'affichage de l'onglet caracteristiques
	 */
	private void initCaracteristiques() {

		// this.resultat.setText("") ;

		// Reselectionne les dernieres valeurs selectionnees
		// change cedric 2016
		// this.initElementsSelectionnes();

		try {

			this.initCaracteristiquesNiv1(false);
			this.initCaracteristiquesNiv2(false);
			this.initCaracteristiquesNiv3(false);
		} catch (Exception e) {
			this.resultat.setText(Erreur.I1007);
			logger.log(Level.SEVERE, " initElementsSelectionnes  ", e);
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			// Oublie la caracteristique actuelle
			// this.car = null ;

			this.validate();
			this.repaint();
		}
	}

	/**
	 * lancement au d�part de l'initialisation des combo de l'onglet
	 * caracteristiques
	 * 
	 * @param _enable
	 *            false pour recharger et "d�selectionner" les radiobuttons
	 * @throws Exception
	 */
	private void initCaracteristiquesNiv1(boolean _enable) throws Exception {
		// Reinitialise les combos, etc
		this.radio_car_type_qualitatif.setSelected(_enable);
		this.radio_car_type_quantitatif.setSelected(_enable);

		this.methodesObtentionCaracteristique = Lanceur.getMethodesObtentionCaracteristiques();
		this.combo_car_methodeObtention.setModel(new DefaultComboBoxModel(this.methodesObtentionCaracteristique));

		// this.car_precision.setText(null) ;
		this.textarea_car_commentaires.setText(null);

		// Si le lot est definit et a des caracteristiques
		if ((this.lot != null)) {
			this.listmodelcaracteristique.removeAllElements();
			for (int i = 0; i < this.lot.getLesCaracteristiques().size(); i++) {
				((DefaultListModel<Caracteristique>) this.listmodelcaracteristique)
						.addElement((Caracteristique) this.lot.getLesCaracteristiques().get(i));
			}
			this.jlist_car.setModel(listmodelcaracteristique);
		}

	}

	/**
	 * affichage ou non du combo_car_parametre *
	 * 
	 * @param _enable
	 *            false pour r�initialiser la fen�tre, true pour permettre
	 *            l'affichage apr�s la s
	 * @throws Exception
	 */
	private void initCaracteristiquesNiv2(boolean _enable) throws Exception {
		// Reinitialise les combos, etc
		this.combo_car_parametre.setEnabled(_enable);
		// this.car_parametre.setSelectedIndex(-1) ;
		this.lesParametreQualitatifs = new ListeRefParamQualBio();
		this.lesParametreQuantitatifs = new ListeRefParamQuantBio();
		// this.car_parametre.setModel(new DefaultComboBoxModel()) ;
	}

	/**
	 * affichage ou non des valeurs quantitatives et qualitatives
	 * 
	 * @param _enable
	 * @throws Exception
	 */
	private void initCaracteristiquesNiv3(boolean _enable) throws Exception {
		// Reinitialise les combos, etc
		this.textfielddt_car_valeurQuantitative.setEnabled(_enable);
		this.textfielddt_car_valeurQuantitative.setText(null);
		this.combo_car_valeurQualitative.setEnabled(_enable);
		// this.car_valeurQualitative.setSelectedIndex(-1) ;
		this.lesValeursParametre = new SousListeRefValeurParametre();
		this.combo_car_valeurQualitative.setModel(new DefaultComboBoxModel());

	}

	private void initCaracteristiquesNiv3a() throws Exception {
		// Reinitialise les combos, etc
		this.textfielddt_car_valeurQuantitative.setEnabled(true);
		this.textfielddt_car_valeurQuantitative.setText(null);
		this.combo_car_valeurQualitative.setEnabled(false);
		// this.car_valeurQualitative.setSelectedIndex(-1) ;
		this.lesValeursParametre = new SousListeRefValeurParametre();
		this.combo_car_valeurQualitative.setModel(new DefaultComboBoxModel());

	}

	private void initCaracteristiquesNiv3b() throws Exception {
		// Reinitialise les combos, etc
		this.textfielddt_car_valeurQuantitative.setEnabled(false);
		this.textfielddt_car_valeurQuantitative.setText(null);
		this.combo_car_valeurQualitative.setEnabled(true);
		// this.car_valeurQualitative.setSelectedIndex(-1) ;
		// this.lesValeursParametre = new SousListeRefValeurParametre() ;
		// this.car_valeurQualitative.setModel(new
		// DefaultComboBoxModel((Object[])null)) ;

	}

	/**
	 * Intitialisation des caracteristiques � partir de celles du masque, les
	 * caract�ristiques sont g�n�r�es dynamiquement � partir des valeurs du
	 * masque. Sert lors du premier chargement
	 * 
	 * @author Cedric
	 */
	private void initCaracteristiquesduMasque() {

		// initiatilisation des valeurs des param�tres, qualitatifs ou
		// quantitatifs
		listofcomponent = new ArrayList<JComponent>();
		vect_estqualitatif = new boolean[caracteristiquemasquelotenbase.size()];
		vect_lesvaleurparametre = new SousListeRefValeurParametre[caracteristiquemasquelotenbase.size()];
		logger.log(Level.INFO, "traitement de " + caracteristiquemasquelotenbase.size() + " param�tres");
		DecimalFormat decimalformatter = new DecimalFormat("###.###");
		DecimalFormatSymbols unpoint = new DecimalFormatSymbols();
		unpoint.setDecimalSeparator('.');
		decimalformatter.setDecimalFormatSymbols(unpoint);
		// NumberFormat numberFieldFormatter =
		// NumberFormat.getNumberInstance();
		int colonne = 0;
		for (int i = 0; i < caracteristiquemasquelotenbase.size(); i++) {

			CaracteristiqueMasqueLot car = (CaracteristiqueMasqueLot) caracteristiquemasquelotenbase.get(i);
			RefParametre param = car.getCaracteristiquedefaut().getParametre();
			String codepar = param.getCode();
			Boolean affichage_valeur = car.isAffichage_valeur();

			try {
				boolean estqualitatif = RefParametre.estQualitatif(codepar);
				vect_estqualitatif[i] = estqualitatif;
			} catch (Exception e) {
				this.resultat.setText(e.getMessage());
				logger.log(Level.SEVERE, " AjouterLot. teste si la valeur est qualitative  ", e);
			}
			String unite;
			if (!vect_estqualitatif[i]) {
				unite = " (" + StringUtils.abbreviate(param.getUnite(), 10) + ")";
			} else {
				unite = "";
			}
			String carlab = StringUtils.abbreviate(car.getCaracteristiquedefaut().getParametre().getLibelle(), 25);
			JLabel label_car = new JLabel(carlab + unite);
			GridBagConstraints gbc_titrepar = new java.awt.GridBagConstraints();
			gbc_titrepar.anchor = java.awt.GridBagConstraints.WEST;
			gbc_titrepar.insets = new Insets(5, 5, 5, 5);// tlbr

			GridBagConstraints gbc_valeurpar = new java.awt.GridBagConstraints();
			gbc_valeurpar.anchor = java.awt.GridBagConstraints.WEST;
			gbc_valeurpar.insets = new Insets(5, 5, 5, 5);// tlbr

			int val = (int) Math.floor(i / 4); // 0123 =0*2// 4567=1*2 ....
			val = 2 * val;
			if ((i % 4) != 0) {
				colonne = colonne + 1;
			} else {
				colonne = 0;
			}

			gbc_titrepar.gridx = colonne;
			gbc_titrepar.gridy = val + 5;
			gbc_valeurpar.gridx = colonne;
			gbc_valeurpar.gridy = val + 6;

			panel_infosLots.add(label_car, gbc_titrepar);
			if (vect_estqualitatif[i]) {

				// Cas ou la station de mesure correspond � un caract�re
				// qualitatif
				JComboBox<RefValeurParametre> combo = new JComboBox<RefValeurParametre>();
				SousListeRefValeurParametre lesValeurQualitatives = new SousListeRefValeurParametre(param);
				try {
					lesValeurQualitatives.chargeFiltreDetails();
				} catch (Exception e) {
					this.resultat.setText(e.getMessage());
					logger.log(Level.SEVERE, " EditerCaracteristiqueMasqueLot chargevaleurqualitative  ", e);
				}
				vect_lesvaleurparametre[i] = lesValeurQualitatives;
				DefaultComboBoxModel<RefValeurParametre> combomodelparm = new DefaultComboBoxModel<RefValeurParametre>();
				// ajout d'un �l�ment vide en t�te de combo
				RefValeurParametre valeur_selectionnee_defaut = car.getCaracteristiquedefaut()
						.getValeurParametreQualitatif();
				int index_val_select_defaut = -1; // par d�faut rien n'est
				// s�lectionn�
				for (int j = 0; j < lesValeurQualitatives.size(); j++) {
					RefValeurParametre refvaleurparm = (RefValeurParametre) lesValeurQualitatives.get(j);
					combomodelparm.addElement(refvaleurparm);
					if (refvaleurparm.equals(valeur_selectionnee_defaut)) {
						index_val_select_defaut = j;
					}
				}
				combo.setModel(combomodelparm);
				combo.setSelectedIndex(index_val_select_defaut);
				panel_infosLots.add(combo, gbc_valeurpar);
				listofcomponent.add(combo);
				combo.setEnabled(affichage_valeur);
				combo.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent e) {
						if (e.getClickCount() == 2 && !e.isConsumed()) {
							e.consume();
							if (combo.isEnabled()) {
								combo.setEnabled(false);
								label_car.setForeground(SystemColor.textInactiveText);
							} else {
								combo.setEnabled(true);
								label_car.setForeground(SystemColor.black);
							}
						}
					}
				});

			} else {
				// Cas ou le param�tre est
				// quantitatif
				vect_lesvaleurparametre[i] = null;
				// on veut garder la structure et acc�der
				// aux �l�ments avec i
				JFormattedTextField textfield_valeur = new JFormattedTextField(decimalformatter);
				textfield_valeur.setColumns(6);
				panel_infosLots.add(textfield_valeur, gbc_valeurpar);

				Float valeur_defaut = car.getCaracteristiquedefaut().getValeurParametreQuantitatif();
				if (valeur_defaut != null & valeur_defaut != 0) {
					textfield_valeur.setValue(valeur_defaut);
				}
				listofcomponent.add(textfield_valeur);
				textfield_valeur.setEnabled(affichage_valeur);
				textfield_valeur.addMouseListener(new java.awt.event.MouseAdapter() {

					public void mouseClicked(java.awt.event.MouseEvent e) {
						if (e.getClickCount() == 2 && !e.isConsumed()) {
							e.consume();
							if (textfield_valeur.isEnabled()) {
								textfield_valeur.setEnabled(false);
								label_car.setForeground(SystemColor.textInactiveText);
							} else {
								textfield_valeur.setEnabled(true);
								label_car.setForeground(SystemColor.black);
							}
						}
					}

				});
			} // else caract quant

			if (affichage_valeur) {
				label_car.setForeground(SystemColor.black);
			} else {
				label_car.setForeground(SystemColor.textInactiveText);
			}
		} // boucle
			// sur
			// les
			// composants
	}

	/**
	 * Initie les caracteristiques de l'onglet panel_infosLots La M�thode
	 * initCaracteristiqueduMasque charge les valeurs par d�faut quand aucun lot
	 * n'est s�lectionn� Ci dessous cette m�thode est d�clench�e quand on
	 * s�lectionne un lot dans le panel onglet
	 */
	private void initCaracteristiquesduMasquePourLot() {
		// extraction des caracteristiques de la sous liste � partir
		// de la cle
		for (int i = 0; i < this.listofcomponent.size(); i++) {
			CaracteristiqueMasqueLot car = (CaracteristiqueMasqueLot) caracteristiquemasquelotenbase.get(i);
			RefParametre param = car.getCaracteristiquedefaut().getParametre();
			// String codepar = param.getCode();
			// je vais chercher la caract�ristique dans listelot a partir de sa
			// cl�
			Caracteristique car_du_lot = (Caracteristique) lot.getLesCaracteristiques()
					.get(lot.getIdentifiant() + car.getCaracteristiquedefaut().getParametre().getCode());

			if (!vect_estqualitatif[i]) {// param�tre quantitatif
				JFormattedTextField textfield_valeur = (JFormattedTextField) listofcomponent.get(i);
				if (car_du_lot != null) {// la caract�ristique correspond � une
					// des
					// caract�ristiques du masque
					textfield_valeur.setValue(car_du_lot.getValeurParametreQuantitatif());
				} else {
					textfield_valeur.setValue(null);
				}

			} else {// param�tre qualitatif
				@SuppressWarnings("unchecked")
				JComboBox<RefValeurParametre> combo = (JComboBox<RefValeurParametre>) listofcomponent.get(i);
				DefaultComboBoxModel<RefValeurParametre> combomodelparm = (DefaultComboBoxModel<RefValeurParametre>) combo
						.getModel();
				// l'objet n'est pas le m�me je peux pas faire get objet
				for (int j = 0; j < combomodelparm.getSize(); j++) {
					RefValeurParametre refpar = combomodelparm.getElementAt(j);
					if (car_du_lot != null) {// la caract�ristique correspond �
						// une des
						boolean match = refpar.getCode().equals(car_du_lot.getValeurParametreQualitatif().getCode());
						if (match) {
							combo.setSelectedIndex(j);
							break;
						}
					} else { // cardulot est null
						combo.setSelectedIndex(-1);
					}
				} // end for
			} // else caractere qual
		} // for
	}
	/*
	 * private ListeRefParamQualBio lesParametresQual ; private
	 * ListeRefParamQuantBio lesParametresQuan ; private
	 * SousListeRefValeurParametre lesValeursParametre ; private String[]
	 * methodesObtentionCaracteristique ;
	 * 
	 */

	//
	/**
	 * charge la liste des parametre quantitatifs
	 */
	private void chargeListeRefParametreQuan() {

		// Si le lot n'est pas definit, erreur
		if ((this.lot == null) || (this.lot.getIdentifiant() == null)) {
			this.initCaracteristiques();
			this.resultat.setText(Erreur.S4009);
		} else {
			try {
				// actualiste les combos
				try {
					this.initCaracteristiquesNiv2(true);
					this.initCaracteristiquesNiv3(false);
				} catch (Exception e) {
					this.resultat.setText(Erreur.I1008);
					logger.log(Level.SEVERE, " chargeListeRefParametreQuan ", e);
				}

				// charge la liste
				this.lesParametreQuantitatifs = new ListeRefParamQuantBio();
				this.lesParametreQuantitatifs.chargeSansFiltre();

				// Mise a jour du combo parametre
				this.combo_car_parametre.setModel(new DefaultComboBoxModel(this.lesParametreQuantitatifs.toArray()));

			} catch (Exception e) {
				logger.log(Level.SEVERE, " chargeListeRefParametreQuan ", e);
				this.resultat.setText(Erreur.B1010);
			}
		}
	}

	/**
	 * charge la liste des parametre qualitatifs
	 */
	private void chargeListeRefParametreQual() {

		// Si le lot n'est pas definit, erreur
		if ((this.lot == null) || (this.lot.getIdentifiant() == null)) {
			this.initCaracteristiques();
			this.resultat.setText(Erreur.S4009);
		} else {
			try {

				// actualiste les combos
				try {
					this.initCaracteristiquesNiv2(true);
					this.initCaracteristiquesNiv3(false);
				} catch (Exception e) {
					this.resultat.setText(Erreur.I1008);
					logger.log(Level.SEVERE, " chargeListeRefParametreQual ", e);
				}

				// charge la liste
				this.lesParametreQualitatifs = new ListeRefParamQualBio();
				this.lesParametreQualitatifs.chargeSansFiltre();

				// Mise a jour du combo parametre
				this.combo_car_parametre.setModel(new DefaultComboBoxModel(this.lesParametreQualitatifs.toArray()));

			} catch (Exception e) {
				this.resultat.setText(Erreur.B1010);
				logger.log(Level.SEVERE, " chargeListeRefParametreQual ", e);
			}
		}
	}

	//
	/**
	 * charge la liste des valeur d'un parametre charge �lalement l'ensemble des
	 * valeurs du masque si elles existent (affichage et valeurs defaut)
	 */
	private void chargeListeRefValeurParametre() {

		// Recupere le parametre selectionne dans le combo
		RefParametre param = (RefParametre) this.combo_car_parametre.getSelectedItem();

		// r�cup�re �ventuellement le masque lot correspondant � la
		// caract�ristique
		CaracteristiqueMasqueLot carmasquelotselectionnee = null;
		for (int i = 0; i < this.caracteristiquemasquelotenbase.size(); i++) {
			CaracteristiqueMasqueLot carmasquelottemp = (CaracteristiqueMasqueLot) caracteristiquemasquelotenbase
					.get(i);
			Boolean test = carmasquelottemp.getCaracteristiquedefaut().getParametre().getCode().equals(param.getCode());
			if (test) {
				carmasquelotselectionnee = carmasquelottemp;
				break;
			}

		}
		// a ce stade la caracteristiquemasque lot est soit nulle (les valeurs
		// du masque ne rapportent pas de valeur
		// Mise � jour si existe des affichages du masque
		if (carmasquelotselectionnee != null) {
			this.combo_car_valeurQualitative.setEnabled(carmasquelotselectionnee.isAffichage_valeur());
			this.textfielddt_car_valeurQuantitative.setEnabled(carmasquelotselectionnee.isAffichage_valeur());
			this.textarea_car_commentaires.setEnabled(carmasquelotselectionnee.isAffichage_commentaire());
			this.textfielddt_car_precision.setEnabled(carmasquelotselectionnee.isAffichage_precision());
			this.textfielddt_car_precision
					.setText(carmasquelotselectionnee.getCaracteristiquedefaut().getPrecision().toString());
			if (carmasquelotselectionnee.isAffichage_valeur()) {
				this.label_car_valeur.setForeground(SystemColor.black);
			} else {
				this.label_car_valeur.setForeground(SystemColor.textInactiveText);
			}
			if (carmasquelotselectionnee.isAffichage_commentaire()) {
				this.label_car_commentaires.setForeground(SystemColor.black);
			} else {
				this.label_car_commentaires.setForeground(SystemColor.textInactiveText);
			}
			this.textfielddt_car_precision.setEnabled(carmasquelotselectionnee.isAffichage_commentaire());

			if (carmasquelotselectionnee.isAffichage_precision()) {
				this.label_car_precision.setForeground(SystemColor.black);
			} else {
				this.label_car_precision.setForeground(SystemColor.textInactiveText);
			}

			this.combo_car_methodeObtention.setEnabled(carmasquelotselectionnee.isAffichage_methodeobtention());
			String methodeObtDefaut = carmasquelotselectionnee.getCaracteristiquedefaut().getMethodeObtention();
			if (methodeObtDefaut != null) {
				this.combo_car_methodeObtention.setSelectedItem(methodeObtDefaut);
			}
			if (carmasquelotselectionnee.isAffichage_methodeobtention()) {
				this.label_car_methodeObtention.setForeground(SystemColor.black);
			} else {
				this.label_car_methodeObtention.setForeground(SystemColor.textInactiveText);
			}
		}

		// pour la caract�ristiques s�lectionn�e, soit initialis�e
		// Si le parametre est qualitatif
		if (this.radio_car_type_qualitatif.isSelected() == true) {
			// actualiste les combo
			try {
				this.initCaracteristiquesNiv3b();

				// charge la liste des valeurs possibles
				this.lesValeursParametre = new SousListeRefValeurParametre(param);
				try {
					this.lesValeursParametre.chargeFiltreDetails();
					this.combo_car_valeurQualitative
							.setModel(new DefaultComboBoxModel(this.lesValeursParametre.toArray()));
				} catch (Exception e) {
					this.resultat.setText(Erreur.B1011);
					logger.log(Level.SEVERE, " chargeListeRefValeurParametre ", e);
				}
				if (carmasquelotselectionnee != null) {
					RefValeurParametre valeurqualmasque = carmasquelotselectionnee.getCaracteristiquedefaut()
							.getValeurParametreQualitatif();
					if (valeurqualmasque != null) {
						for (int i = 0; i < lesValeursParametre.size(); i++) {
							String code_i = ((RefValeurParametre) lesValeursParametre.get(i)).getParametre().getCode();
							if (valeurqualmasque.getCode().equals(code_i)) {
								this.combo_car_valeurQualitative.setSelectedIndex(i);
								break;
							}
						}

					}
				}

			} catch (Exception e) {
				this.resultat.setText(Erreur.I1008);
				logger.log(Level.SEVERE, " chargeListeRefValeurParametre ", e);
			}
		}
		// Sinon, le parametre est quantitatif
		else {
			// actualiste les combo

			try {
				this.initCaracteristiquesNiv3a();
				// chargement de la valeur par d�faut du masque
				if (carmasquelotselectionnee != null) {
					Float valeurquantmasque = carmasquelotselectionnee.getCaracteristiquedefaut()
							.getValeurParametreQuantitatif();
					if (valeurquantmasque != null) {
						this.textfielddt_car_valeurQuantitative.setText(valeurquantmasque.toString());
					}

				}
			} catch (Exception e) {
				this.resultat.setText(Erreur.I1008);
				logger.log(Level.SEVERE, " chargeListeRefValeurParametre ", e);
			}
		}

	}

	/**
	 * Affiche une caracteristique selectionnee par l'utilisateur dans la liste
	 */
	private void afficheCaracteristique() {

		Caracteristique carSelectionnee;

		// Recuperation de la caracteristique selectionnee
		carSelectionnee = (Caracteristique) this.jlist_car.getSelectedValue();

		// Si la selection n'est pas vide, affichage de celle ci
		if (carSelectionnee != null) {

			try {

				// Si le parametre est qualitatif
				if (carSelectionnee.getParametre() instanceof RefParametreQualitatif) {

					// affichage du type de parametre
					this.radio_car_type_qualitatif.setSelected(true);
					this.radio_car_type_quantitatif.setSelected(false);

					// Chargement de la liste des parametres puis selection du
					// parametre
					this.chargeListeRefParametreQual();
					this.combo_car_parametre.setSelectedItem(
							this.lesParametreQualitatifs.get(carSelectionnee.getParametre().getCode()));

					// Chargement de la liste des valeurs puis selection d'une
					// valeur
					this.chargeListeRefValeurParametre();
					this.combo_car_valeurQualitative.setSelectedItem(this.lesValeursParametre
							.get(new Integer(carSelectionnee.getValeurParametreQualitatif().getCode())));

				}
				// Si le parametre est quantitatif
				else if (carSelectionnee.getParametre() instanceof RefParametreQuantitatif) {

					// affichage du type de parametre
					this.radio_car_type_qualitatif.setSelected(false);
					this.radio_car_type_quantitatif.setSelected(true);

					// Chargement de la liste des parametres puis selection du
					// parametre
					this.chargeListeRefParametreQuan();
					this.combo_car_parametre.setSelectedItem(
							this.lesParametreQuantitatifs.get(carSelectionnee.getParametre().getCode()));

					// Affichage de la valeur
					this.chargeListeRefValeurParametre();
					if (carSelectionnee.getValeurParametreQuantitatif() != null) {
						this.textfielddt_car_valeurQuantitative
								.setText(carSelectionnee.getValeurParametreQuantitatif().toString());
					}
				}
				// Sinon, parametre inconnu
				else {
					throw new Exception("Le param�tre est inconnu");
				}

				this.combo_car_methodeObtention.setSelectedItem(carSelectionnee.getMethodeObtention());

				if (carSelectionnee.getPrecision() != null) {
					this.textfielddt_car_precision.setText(carSelectionnee.getPrecision().toString());
				}

				this.textarea_car_commentaires.setText(carSelectionnee.getCommentaires());

			} catch (Exception e) {
				this.initCaracteristiques();
				this.resultat.setText(Erreur.I1007);
				logger.log(Level.SEVERE, " afficheCaracteristique ", e);
			}
		}

	}

	/**
	 * Construit les caracterisitiques g�n�r�es par le masque g�n�re aussi les
	 * valeurs par d�faut pour les pr�cisions et les methodes puis enregistre
	 * les valeurs des cact�ristiques pour le lot en cours
	 */
	private String chargeSaveCaracteristiqueMasque() {
		for (int i = 0; i < this.listofcomponent.size(); i++) {
			String result = null;
			Caracteristique caraenregistrer;
			Float valeurParametreQuantitatif = null;
			RefValeurParametre valeurParametreQualitatif = null;
			Float precision = null;
			String methodeObtention = null;
			String commentaires = null;
			RefParametre parametre = null;

			try {
				CaracteristiqueMasqueLot carmasquelot = (CaracteristiqueMasqueLot) caracteristiquemasquelotenbase
						.get(i);
				parametre = carmasquelot.getCaracteristiquedefaut().getParametre();

				if (!vect_estqualitatif[i]) {// param�tre quantitatif
					JFormattedTextField textfield_valeur = (JFormattedTextField) listofcomponent.get(i);
					if (textfield_valeur.getValue() != null) {
						valeurParametreQuantitatif = ((Number) textfield_valeur.getValue()).floatValue();
					}

				} else {// param�tre qualitatif

					JComboBox<RefValeurParametre> combo = (JComboBox<RefValeurParametre>) listofcomponent.get(i);
					combo.getSelectedIndex();
					// DefaultComboBoxModel<RefValeurParametre> combomodelparm =
					// (DefaultComboBoxModel<RefValeurParametre>) combo
					// .getModel();
					valeurParametreQualitatif = (RefValeurParametre) combo.getSelectedItem();
				} // else caractere qual
					// r�cup�ration des valeurs par d�faut du masque
				precision = carmasquelot.getCaracteristiquedefaut().getPrecision();
				methodeObtention = carmasquelot.getCaracteristiquedefaut().getMethodeObtention();
			} catch (Exception e) {
				result = e.getMessage();
				logger.log(Level.SEVERE, " chargeSaveCaracteristique ", e);
			}
			boolean test = valeurParametreQualitatif != null | valeurParametreQuantitatif != null;
			// un des combo peut ne pas avoir �t� s�lectionn�
			if (result == null && test) {
				// Creation de l'objet et verification de sa validite
				caraenregistrer = new Caracteristique(lot, parametre, methodeObtention, valeurParametreQuantitatif,
						valeurParametreQualitatif, precision, commentaires);
				enregistreCarateristique(caraenregistrer);
			} else {
				// Affichage du resultat sans reinitialisation
				this.resultat.setText(result);
			}
		} // for
			// si pas d'exception on va ici
		String resultfinal = this.listofcomponent.size() + " Caract�ristiques enregistr�es";
		return (resultfinal);
	}

	/**
	 * Construit une caract�ristiques � partir des �lements de l'affichage de
	 * l'onglet lot puis appelle enregistreCaracteristique pour l'enregistrer en
	 * base. D�compos� en deux m�thodes soit onlet soit masque pour appeler
	 * enregistrecaracteristique
	 */
	private void chargeSaveCaracteristiqueOnglet() {

		// Objet a creer
		Caracteristique carAEnregistrer;

		// Resultat affiche a l'utilisateur
		String result = null;

		// Attributs de l'objet a creer
		Lot lotRattachement = null;
		RefParametre parametre = null;
		String methodeObtention = null;
		Float valeurParametreQuantitatif = null;
		RefValeurParametre valeurParametreQualitatif = null;
		Float precision = null;
		String commentaires = null;

		// Initialisation des attributs d'apres les saisies de l'utilisateur et
		// verification que les valeurs sont bien du type attendu

		try {
			lotRattachement = this.lot;
			if (lotRattachement == null) { // normalement il y a un lot
				throw new Exception("erreur interne, il devrait y avoir un lot");
			}
		} catch (Exception e) {
			result = Erreur.S4009;
		}

		try {
			parametre = (RefParametre) this.combo_car_parametre.getSelectedItem();
		} catch (Exception e) {
			result = Erreur.S0100 + this.label_car_parametre.getText();
		}

		try {
			methodeObtention = (String) this.combo_car_methodeObtention.getSelectedItem();
		} catch (Exception e) {
			result = Erreur.S0100 + this.label_car_methodeObtention.getText();
		}

		// Si le parametre est qualitatif
		if (this.radio_car_type_qualitatif.isSelected() == true) {
			try {
				valeurParametreQualitatif = (RefValeurParametre) this.combo_car_valeurQualitative.getSelectedItem();
			} catch (Exception e) {
				result = Erreur.S0100 + this.label_car_valeur.getText();
			}
		}
		// Sinon, il est quantitatif
		else {
			try {
				valeurParametreQuantitatif = this.textfielddt_car_valeurQuantitative.getFloat();
			} catch (Exception e) {
				result = Erreur.S0100 + this.label_car_valeur.getText();
			}
		}

		try {
			precision = this.textfielddt_car_precision.getFloat();
		} catch (Exception e) {
			result = Erreur.S0100 + this.label_car_precision.getText();
		}

		try {
			commentaires = this.textarea_car_commentaires.getString();
		} catch (Exception e) {
			result = Erreur.S0100 + this.label_car_commentaires.getText();
		}

		// Si aucune erreur jusque la, les champs sont soit null soit du bon
		// type, mais les contraintes de domaine ne sont pas forcement verifiees
		if (result == null) {
			// Creation de l'objet et verification de sa validite
			carAEnregistrer = new Caracteristique(lotRattachement, parametre, methodeObtention,
					valeurParametreQuantitatif, valeurParametreQualitatif, precision, commentaires);
			result = enregistreCarateristique(carAEnregistrer);
			// Affichage du resultat
			reInitComponents();
			this.selectionne_dans_larbre(carAEnregistrer);
		} else {
			// Affichage du resultat sans reinitialisation
			this.resultat.setText(result);
		}
	}

	/**
	 * 
	 * methode appell�e en commun soit apr�s inititalisation carMasque
	 * 
	 * @param carAEnregistrer
	 */
	private String enregistreCarateristique(Caracteristique carAEnregistrer) {
		String result = null;
		try {
			carAEnregistrer.verifAttributs();

			// Enregistrement dans la BD
			try {
				// l'identifiant d'une caracteristique est le couple :
				// lot-parametre
				String identifiantCar = this.lot.getIdentifiant().toString() + carAEnregistrer.getParametre().getCode();

				// Si la caracteristique existe deja, elle est modifiee

				// System.out.println("getLesCaracteristiques() " +
				// this.lot.getLesCaracteristiques());
				// System.out.println("getLesCaracteristiques() " +
				// this.lot.getLesCaracteristiques().get(identifiantCar));
				// System.out.println("identifiantCar " + identifiantCar) ;

				if ((this.lot.getLesCaracteristiques() != null)
						&& (this.lot.getLesCaracteristiques().get(identifiantCar) != null)) {
					carAEnregistrer.majObjet();

					this.lot.getLesCaracteristiques().replace(identifiantCar, carAEnregistrer);

					// mise a jour de la liste des caracteristiques du lot
					// remplacement des valeurs de la liste ()
					listmodelcaracteristique.removeAllElements();
					for (int i = 0; i < this.lot.getLesCaracteristiques().size(); i++) {
						((DefaultListModel<Caracteristique>) this.listmodelcaracteristique)
								.addElement((Caracteristique) this.lot.getLesCaracteristiques().get(i));
					}
					this.jlist_car.setModel(listmodelcaracteristique);

					result = Message.M1002; // reussite
				}
				// Sinon, ajout de la caracteristique
				else {
					carAEnregistrer.insertObjet();

					// ajout de la caracteristique a la liste des
					// caracteristiques du lot courant
					if (this.lot.getLesCaracteristiques() == null) {
						this.lot.setLesCaracteristiques(new SousListeCaracteristiques(this.lot));
					}

					this.lot.getLesCaracteristiques().put(identifiantCar, carAEnregistrer);

					// mise a jour de la liste des caracteristiques du lot
					for (int i = 0; i < this.lot.getLesCaracteristiques().size(); i++) {
						((DefaultListModel<Caracteristique>) this.listmodelcaracteristique)
								.addElement((Caracteristique) this.lot.getLesCaracteristiques().get(i));
					}
					/////////////////////////////////////////////////////
					// mise � jour de l'affichage du noeud dans l'arbre
					///////////////////////////////////////////////////////
					// dans la m�thode m�re chargeSaveCaracteristiqueOnglet
					result = Message.A1003; // reussite
				}

				// Met a jour la reference sur le lot courant
				// this.car = carAEnregistrer ;

			} catch (Exception e) {
				result = e.getMessage(); // erreur
				logger.log(Level.SEVERE, " enregirtreCaracteristique ", e);
			} finally {
				this.resultat.setText(result);
			}

		} catch (DataFormatException e) {
			result = e.getMessage(); // erreur

			// Affichage du resultat sans reinitialisation
			this.resultat.setText(result);
		}
		return (result);

	}

	// Supprime de la base de donnee la caracteristique selectionnee
	private void supprimeCaracteristique() {

		Caracteristique carASupprimer;

		// Recuperation de la caracteristique selectionnee
		carASupprimer = (Caracteristique) this.jlist_car.getSelectedValue();

		// Si la selection n'est pas vide, suppression de celle ci
		if (carASupprimer != null) {

			try {
				// efface l'enregistrement de la BD
				carASupprimer.effaceObjet();

				// Mise a jour de la liste des caracteristiques du lot
				if (this.lot.getLesCaracteristiques() != null) {

					// l'identifiant de la caracteristique est le couple
					// lot-parametre
					String identifiantCar = carASupprimer.getLot().getIdentifiant().toString()
							+ carASupprimer.getParametre().getCode();

					this.lot.getLesCaracteristiques().removeObject(identifiantCar);
				}

				reInitComponents();

				this.resultat.setText(Message.S1002); // reussite
			} catch (Exception e) {
				this.resultat.setText(e.getMessage()); // si echec
				logger.log(Level.SEVERE, " supprimeCaracteristique ", e);
			}
		}

	}

	// ***********************************************
	//
	// ONGLET PATHOLOGIES
	//
	// ***********************************************

	/**
	 * Chargement des informations a propos de la pathologie selectionn�e
	 */
	private void affichePathologies() {

		// la pathologie constat�e sur le lot courant
		PathologieConstatee pc = (PathologieConstatee) jList_PathologiesConstatees.getSelectedValue();

		if (pc != null) {
			String res = "";
			// les comentaires sur la pathologie
			String commentaires = pc.getCommentaires();

			// la pathologie
			// je suis oblig� d'aller chercher le r�f�rentiel ayant conduit �
			// construire le combo (stock� dans lesPathologies)
			RefPathologie pat = (RefPathologie) lesPathologies.get(pc.getPathologie().getCode());

			// la localisation de la pathologie
			// idem
			RefLocalisation loc = (RefLocalisation) lesLocalisations.get(pc.getLocalisation().getCode());

			// l'importance de la pathologie
			RefImportancePathologie imp = pc.getImportance();

			/////////////////
			// pathologie
			/////////////////

			// remplissage du combo avec les valeurs par d�faut du masque
			int index = this.comboModelpathomasque.getIndexOf(pat);

			// si la valeur de la pathologie n'est pas pr�sente, il faut charger
			// tout
			if (index == -1) {
				initcombopathologie(true);
				res = "Chargement de toutes les pathologies ";
			} else {
				// le pr�c�dent masque peut �tre un masque complet
				initcombopathologie(false);
			} //
			this.combo_pathologies.setSelectedItem(pat);

			/////////////////
			// localisations
			/////////////////

			index = this.comboModellocalisationpathomasque.getIndexOf(loc);
			if (index == -1) {
				initcombolocalisationpatho(true);
				res = "Chargement de toutes les localisations ";

			} else {
				// le pr�c�dent masque peut �tre un masque complet
				initcombolocalisationpatho(false);
			}

			/////////////////
			// Importance
			/////////////////

			this.combo_localisationPathologie.setSelectedItem(loc);

			int indexImportance = this.vectorImportance.indexOf(imp.getCode());
			combo_importancePathologie.setSelectedIndex(indexImportance);

			textarea_commentairesPathologies.setText(commentaires);

			this.resultat.setText(res);
		}
	}

	/**
	 * Conversion d'une ListeRefPathologie en vecteur pour pouvoir pr�-remplir
	 * les combos
	 * 
	 * @param lesPathologies
	 *            la liste � convertir
	 * @return le vecteur de String (String = code de la pathologie)
	 */
	private Vector<String> convertToVector(ListeRefPathologie lesPathologies) {
		Vector<String> res = new Vector<String>();

		for (int i = 0; i < lesPathologies.size(); i++)
			res.add(((RefPathologie) lesPathologies.get(i)).getCode());

		return res;
	}

	/**
	 * Conversion d'une ListeRefLocalisation en vecteur pour pouvoir pr�-remplir
	 * les combos
	 * 
	 * @param lesLocalisations
	 *            la liste � convertir
	 * @return le vecteur de String (String = localisation de la pathologie)
	 */
	private Vector<String> convertToVector(ListeRefLocalisation lesLocalisations) {
		Vector<String> res = new Vector<String>();

		for (int i = 0; i < lesLocalisations.size(); i++)
			res.add(((RefLocalisation) lesLocalisations.get(i)).getCode());

		return res;
	}

	/**
	 * Conversion d'une ListeRefImportance en vecteur pour pouvoir pr�-remplir
	 * les combos
	 * 
	 * @param lesLocalisations
	 *            la liste � convertir
	 * @return le vecteur de String (String = importance de la pathologie)
	 */
	private Vector<String> convertToVector(ListeRefImportancePathologie lesImportances) {
		Vector<String> res = new Vector<String>();

		for (int i = 0; i < lesImportances.size(); i++)
			res.add(((RefImportancePathologie) lesImportances.get(i)).getCode());

		return res;
	}

	/**
	 * Conversion d'une ListeRefNatureMarque en vecteur pour pouvoir pr�-remplir
	 * les combos
	 * 
	 * @param lesNaturesMarques
	 *            la liste � convertir
	 * @return le vecteur de String (String = code de la nature de la marque)
	 */
	private Vector<String> convertToVector(ListeRefNatureMarque lesNaturesMarques) {
		Vector<String> res = new Vector<String>();

		for (int i = 0; i < lesNaturesMarques.size(); i++)
			res.add(((Ref) lesNaturesMarques.get(i)).getCode());

		return res;
	}

	/**
	 * Conversion d'une ListeRefPathologie en vecteur pour pouvoir pr�-remplir
	 * les combos
	 * 
	 * @param lesPathologies
	 *            la liste � convertir
	 * @return le vecteur de String (String = code de la pathologie)
	 */
	private Vector<String> convertToVector(ListeOperationMarquage lesOperationsMarquages) {
		Vector<String> res = new Vector<String>();

		for (int i = 0; i < lesOperationsMarquages.size(); i++)
			res.add(((OperationMarquage) lesOperationsMarquages.get(i)).getReference());

		return res;
	}

	/**
	 * Conversion d'une ListeRefPathologie en vecteur pour pouvoir pr�-remplir
	 * les combos
	 * 
	 * @param lesPathologies
	 *            la liste � convertir
	 * @return le vecteur de String (String = code de la pathologie)
	 */
	private Vector<String> convertToVector(ListeRefPrelevement lesPrelevements) {
		Vector<String> res = new Vector<String>();

		for (int i = 0; i < lesPrelevements.size(); i++)
			res.add(((RefPrelevement) lesPrelevements.get(i)).getTypePrelevement());

		return res;
	}

	/**
	 * Ajout/Modification d'une pathologie constat�e. Ajout si aucune pathologie
	 * n'est selectionn�e, modification dans le cas contraire
	 */
	private void ajouterModifierPathologie() {
		String result = null;

		// les attributs de la pathologie constat�e selectionn�e (mis a jour
		RefPathologie rp = (RefPathologie) combo_pathologies.getSelectedItem();
		RefLocalisation rl = (RefLocalisation) combo_localisationPathologie.getSelectedItem();
		RefImportancePathologie imp = (RefImportancePathologie) combo_importancePathologie.getSelectedItem();
		String commentaires = textarea_commentairesPathologies.getText();

		try {
			// s'il n'y a pas de pathologie selectionn�e => ajout
			if (this.lot == null) {
				result = "S�lectionnez un lot";
			} else {
				if (jList_PathologiesConstatees.getSelectedValue() == null) {

					// la nouvelle pathologie
					PathologieConstatee pc = new PathologieConstatee(this.lot, rp, rl, imp, commentaires);

					// v�rification puis mise � jour
					pc.verifAttributs();
					pc.insertObjet();

					// mise a jour de l'affichage
					afficheLot();
					reInitComponents();
					this.selectionne_dans_larbre(pc);
					result = Message.A1010;
				} else // sinon => modification
				{
					// l'ancienne pathologie
					PathologieConstatee old_pc = (PathologieConstatee) jList_PathologiesConstatees.getSelectedValue();

					// la nouvelle pathologie
					PathologieConstatee pc = new PathologieConstatee(this.lot, rp, rl, imp, commentaires);

					// v�rification des attributs puis mise a jour dans la base
					pc.verifAttributs();
					pc.majObjet(old_pc);

					// mise a jour de l'affichage
					afficheLot();
					reInitComponents();
					this.selectionne_dans_larbre(pc);
					result = Message.M1010;
				}

			} // else le lot n'est pas null
		} catch (Exception e) {
			result = e.getMessage();
			logger.log(Level.SEVERE, " ajouterModifierPathologie ", e);
		} finally {
			this.resultat.setText(result);
		}
	}

	/**
	 * Suppression d'une pathologie constat�e
	 */
	private void supprimerPathologie() {
		String result = null;
		try {
			// on s'assure qu'une pathologie est selectionn�e
			if (jList_PathologiesConstatees.getSelectedValue() != null) {
				// la pathologie selectionn�e
				PathologieConstatee pc = (PathologieConstatee) jList_PathologiesConstatees.getSelectedValue();

				// v�rification (?) des attributs puis suppression
				pc.verifAttributs();
				pc.effaceObjet();

				if (this.lot.getLesPathologiesConstatees() != null) {

					// l'identifiant de la pathologie est le couple
					// lot-parametre
					// cle_primaire = (Lot.getIdentifiant() + path_code +
					// loc_code
					String identifiantpatho = pc.getLot().getIdentifiant() + pc.getPathologie().getCode()
							+ pc.getLocalisation().getCode();

					this.lot.getLesPathologiesConstatees().removeObject(identifiantpatho);
				}
				// mise a jour de l'affichage

				result = Message.S1006;
				reInitComponents();
			} else // sinon => erreur
			{
				result = Erreur.S14000;
			}
		} catch (Exception e) {
			result = e.getMessage();
			logger.log(Level.SEVERE, " supprimerPathologie ", e);
		} finally {
			this.resultat.setText(result);
		}
	}

	/**
	 * Reset des propri�t�s des 'pathololgies constat�es' entr�es jusque l�
	 */
	private void annulerPathologie() {
		try {
			afficheLot();
			combo_localisationPathologie.setSelectedIndex(0);
			combo_pathologies.setSelectedIndex(0);
			combo_importancePathologie.setSelectedIndex(0);
			textarea_commentairesPathologies.setText("");
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, "annulerPathologie", e);
		}
	}

	// ***********************************************
	//
	// ONGLET MARQUES
	//
	// ***********************************************

	/**
	 * Pr�-rempli les combo et champs de texte
	 */
	private void afficheMarque() {
		Marquage mq = (Marquage) this.jList_marquages.getSelectedValue();
		if (mq != null) {
			Marque marque = mq.getMarque();

			String action = mq.getAction().toUpperCase();
			if (action.equals("LECTURE"))
				this.radio_actionMarquage_lecture.setSelected(true);
			else if (action.equals("POSE"))
				this.radio_actionMarquage_pose.setSelected(true);
			else if (action.equals("RETRAIT"))
				this.radio_actionMarquage_retrait.setSelected(true);

			this.textarea_CommentaireMarquage.setText(mq.getCommentaires());
			this.textarea_commentairesMarque.setText(marque.getCommentaires());
			this.textfielddt_refMarque.setText(marque.getReference());
			this.textfielddt_refMarque.setEditable(false);

			// pr�-remplissage des combo
			int i = vectorLocalisation.indexOf(marque.getLocalisation().getCode());
			this.combo_localisationMarque.setSelectedIndex(i);

			int j = vectorNatureMarque.indexOf(marque.getNature().getCode());
			this.combo_natureMarque.setSelectedIndex(j);

			int k = vectorOperationMarquage.indexOf(marque.getOperationMarquage().getReference());
			this.combo_OperationMarquage.setSelectedIndex(k);

		}
	}

	/**
	 * Ajout/Modification d'une marque Ajout si aucune marque n'est
	 * selectionn�e, modification dans le cas contraire
	 */
	private void ajouterModifierMarque() {
		String result = null;

		String action = null;
		String commentairesMarquage = null;
		String refMarque = null;
		String commentairesMarque = null;
		OperationMarquage operationMarquage = null;
		RefLocalisation localisation = null;
		RefNatureMarque nature = null;

		// l'action
		try {
			if (this.radio_actionMarquage_lecture.isSelected())
				action = "LECTURE";
			else if (this.radio_actionMarquage_pose.isSelected())
				action = "POSE";
			else if (this.radio_actionMarquage_retrait.isSelected())
				action = "RETRAIT";
			else if (this.radio_actionMarquage_posemultiple.isSelected())
				action = "POSEMULTIPLE";
		} catch (Exception e) {
			result = Erreur.S19000;
			logger.log(Level.SEVERE, " ajouterModifierMarque ", e);
		}

		// le commentaire � propos du marquage
		try {
			commentairesMarquage = this.textarea_CommentaireMarquage.getText();
		} catch (Exception e) {
			result = Erreur.S19001;
		}

		// la marque
		try {
			if (textfielddt_refMarque.isVisible()) {
				refMarque = this.textfielddt_refMarque.getText();
			} else {
				refMarque = this.combo_referencesMarques.getSelectedItem().toString();
			}

		} catch (Exception e) {
			result = Erreur.S19002;

		}

		// le commentaire sur la marque
		try {
			commentairesMarque = this.textarea_commentairesMarque.getText();
		} catch (Exception e) {
			result = Erreur.S19003;
		}

		// l'op�ration de marquage
		try {
			operationMarquage = (OperationMarquage) this.combo_OperationMarquage.getSelectedItem();
		} catch (Exception e) {
			result = Erreur.S19003;
		}

		// la localisation anatomique
		try {
			localisation = (RefLocalisation) this.combo_localisationMarque.getSelectedItem();
		} catch (Exception e) {
			result = Erreur.S19003;
		}

		// la nature de la marque
		try {
			nature = (RefNatureMarque) this.combo_natureMarque.getSelectedItem();
		} catch (Exception e) {
			result = Erreur.S19003;
		}

		// le lot
		if (this.lot == null)
			result = Erreur.S19006;

		if (result == null) {
			// la marque
			Marque m = new Marque(refMarque, localisation, nature, operationMarquage, commentairesMarque);
			// le marquage
			Marquage marquage = new Marquage(this.lot, m, action, commentairesMarquage);

			try {
				// v�rification de la marque et du marquage
				m.verifAttributs();

				// insertion si aucune marque n'est selectionn�e
				if (this.jList_marquages.getSelectedValue() == null) {
					// insertion de la marque si c'est un pose seulement
					if (action == null)
						throw new DataFormatException("Erreur il faut s�lectionner une action de marquage");
					if (action.equals("POSE"))
						m.insertObjet();

					// si l'action est une pose multiple, on remet POSE dedans
					// avant �criture dans la base
					if (action.equals("POSEMULTIPLE"))
						marquage.setAction("POSE");

					// �criture du marquage dans la liste
					marquage.verifAttributs();
					marquage.insertObjet();

					// mise a jour de l'affichage
					afficheLot();
					reInitComponents();
					this.selectionne_dans_larbre(marquage);
					result = Message.A1012;
				} else // mise a jour sinon
				{
					// mises a jour
					m.majObjet();
					Marquage old = (Marquage) this.jList_marquages.getSelectedValue();
					marquage.majObjet(old);

					// mise a jour de l'affichage
					afficheLot();
					reInitComponents();
					this.selectionne_dans_larbre(marquage);
					result = Message.M1012;
				}
			} catch (Exception e) {
				result = e.getMessage();
				logger.log(Level.SEVERE, " ajouterModifierMarque ", e);
			} finally {
				this.resultat.setText(result);
			}
		} else
			this.resultat.setText(result);
	}

	/**
	 * Suppression d'une marque
	 */
	private void supprimerMarque() {
		// la marque selectionn�e
		Marquage m = (Marquage) this.jList_marquages.getSelectedValue();

		if (m != null) {
			String result = null;
			try {
				// suppression
				m.effaceObjet();

				if (this.lot.getLesMarquages() != null) {

					// l'identifiant du marquage est le couple
					// lot-parametre
					// cle_primaire = lot_identifiant+mqe_reference
					String identifiantmarquage = m.getLot().getIdentifiant() + m.getMarque().getReference();

					this.lot.getLesMarquages().removeObject(identifiantmarquage);
				}
				// mise a jour de l'affichage
				reInitComponents();

				// affichage = succ�s
				result = Message.S1010;
			} catch (Exception e) {
				result = e.getMessage();
				logger.log(Level.SEVERE, " supprimerMarque ", e);
			} finally {
				this.resultat.setText(result);
			}
		} else
			this.resultat.setText(Erreur.S19005);
	}

	/**
	 * Reset des propri�t�s des 'marques' entr�es jusque l�
	 */
	private void resetaffichageMarque() {
		this.textarea_CommentaireMarquage.setText("");
		this.textarea_commentairesMarque.setText("");
		this.textfielddt_refMarque.setText("");
		this.textfielddt_refMarque.setEditable(true);
		this.combo_localisationMarque.setSelectedIndex(0);

		this.combo_natureMarque.setSelectedIndex(0);
		if (this.listeDesOperationsMarquages.size() != 0)
			this.combo_OperationMarquage.setSelectedIndex(0);
		else
			this.combo_OperationMarquage.setSelectedIndex(-1);

	}

	// ////////////////////////////////////////////////////////
	// ONGLET OPERATIONS DE MARQUAGE
	// ////////////////////////////////////////////////////////

	/**
	 * Charge la liste des op�rations de marquage
	 */

	private void chargeListeOperationsMarquage() {
		this.listeDesOperationsMarquages = new ListeOperationMarquage();
		listmodelOperationMarquage.clear();
		try {
			this.listeDesOperationsMarquages.chargeSansFiltreDetails();
			for (int i = 0; i < listeDesOperationsMarquages.size(); i++) {
				((DefaultListModel<OperationMarquage>) this.listmodelOperationMarquage)
						.addElement((OperationMarquage) this.listeDesOperationsMarquages.get(i));
			}

			this.jList_operationMarquage.setModel(listmodelOperationMarquage);
			this.lesOperationsMarquages = new ListeOperationMarquage();
			this.lesOperationsMarquages.chargeSansFiltreDetails();
			this.combo_OperationMarquage.setModel(new DefaultComboBoxModel(this.lesOperationsMarquages.toArray()));
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " chargeListeOperationsMarquage ", e);
		}
	}

	// ////////////////////////////////////////////////////////
	// ONGLET OPERATIONS DE MARQUAGE
	// ////////////////////////////////////////////////////////

	/**
	 * Ajout/modification d'un pr�l�vement
	 */
	private void ajouterModifierPrelevement() {
		String result = null;

		RefPrelevement prelevement = null;
		Lot lot = null;
		String code = null;
		String operateur = null;
		RefLocalisation localisation = null;
		String commentaires = null;

		try {
			prelevement = (RefPrelevement) this.combo_typePrelevement.getSelectedItem();
		} catch (Exception e) {
			result = Erreur.S23007;
		}

		if (this.lot != null) {
			lot = this.lot;
		} else {
			result = Erreur.S23008;
		}

		try {
			code = textfield_codePrelevement.getText();
		} catch (Exception e) {
			result = Erreur.S23009;
		}

		try {
			operateur = textfield_operateurPrelevement.getText();
		} catch (Exception e) {
			result = Erreur.S23012;
		}

		try {
			localisation = (RefLocalisation) combo_localisationPrelevement.getSelectedItem();
		} catch (Exception e) {
			result = Erreur.S23010;
		}

		try {
			commentaires = textarea_commentairesPrelevement.getText();
		} catch (Exception e) {
			result = Erreur.S23011;
		}

		if (result == null) {
			Prelevement p = new Prelevement(prelevement, lot, operateur, code, localisation, commentaires);
			try {
				p.verifAttributs();

				if (jList_listePrelevements.getSelectedValue() == null) {
					p.insertObjet();

					result = Message.A1016;
				} else {
					Prelevement old = (Prelevement) jList_listePrelevements.getSelectedValue();
					p.majObjet(old);
					result = Message.M1016;
				}
				afficheLot();
			} catch (Exception e) {
				result = e.getMessage();
				logger.log(Level.SEVERE, " ajouterModifierPrelevement ", e);
			} finally {
				this.resultat.setText(result);
			}
		} else
			this.resultat.setText(result);
	}

	/**
	 * Suppression d'un pr�l�vement
	 */
	private void supprimerPrelevement() {
		if (this.jList_listePrelevements.getSelectedValue() != null) {
			Prelevement p = (Prelevement) this.jList_listePrelevements.getSelectedValue();
			String result = null;
			try {
				p.effaceObjet();
				result = Message.S1014;
				if (this.lot.getLesPrelevements() != null) {

					// cle=prl_pre_typeprelevement+prl_lot_identifiant
					String identifiantprelevement = p.getTypePrelevement().toString() + p.getLot().getIdentifiant();
					this.lot.getLesPrelevements().removeObject(identifiantprelevement);
				}
				// mise a jour de l'affichage

				afficheLot();
				reInitComponents();

			} catch (Exception e) {
				result = e.getMessage();
				logger.log(Level.SEVERE, " supprimerPrelevement ", e);
			} finally {
				this.resultat.setText(result);

			}
		} else
			this.resultat.setText(Erreur.S23000);
	}

	/**
	 * Reset des champs de texte
	 */
	private void annulerPrelevement() {
		this.textfield_codePrelevement.setText("");
		this.textarea_commentairesPrelevement.setText("");
		this.textfield_operateurPrelevement.setText("");
		this.combo_localisationPrelevement.setSelectedIndex(0);
		this.combo_typePrelevement.setSelectedIndex(0);
		try {
			afficheLot();
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " annulerPrelevement ", e);
		}
	}

	/**
	 * Chargement des infos du pr�l�vement
	 */
	private void chargeInfoPrelevement() {
		Prelevement p = (Prelevement) jList_listePrelevements.getSelectedValue();
		if (p != null) {

			this.textfield_codePrelevement.setText(p.getCode());
			this.textarea_commentairesPrelevement.setText(p.getCommentaires());
			this.textfield_operateurPrelevement.setText(p.getOperateur());

			int i = vectorLocalisation.indexOf(p.getLocalisation().getCode());
			combo_localisationPrelevement.setSelectedIndex(i);

			int j = vectorPrelevement.indexOf(p.getTypePrelevement().getTypePrelevement());
			combo_typePrelevement.setSelectedIndex(j);
		}
	}

	/**
	 * Focus sur le premier JtextField
	 */
	private void focusTocar_valeurTaille() {
		this.textfielddt_car_valeurTaille.requestFocus();
	}

	/**
	 * Retourne l'effectif du lot (ou �chantillon) s�lectionn�. change 2016
	 * C�dric teste la cat�gorie de l'objet du node pour aller chercher le lot
	 * p�re quand une caract�ristique, un marquage ou une pathologie est
	 * s�lectionn�e
	 * 
	 * @return l'effectif du lot (ou �chantillon) s�lectionn�.
	 * 
	 */
	private Float getEffectif() {
		Lot lot;
		DefaultMutableTreeNode node;

		node = (DefaultMutableTreeNode) arbreLots.getLastSelectedPathComponent();

		if (node == null) {
			return null;
		} else if (node.isRoot()) {
			return null;
		} else {
			Object objetselectionne = (Object) node.getUserObject();
			if (!(objetselectionne instanceof Lot)) {
				lot = (Lot) ((DefaultMutableTreeNode) node.getParent()).getUserObject();
			} else {
				lot = (Lot) node.getUserObject();
			}

			// si c'est un �chantillon, on retourne l'effectif de l'�chantillon
			if (lot.getLotParent() != null) {
				this.label_labeffectif.setText(Messages.getString("AjouterLot.EffectifDeLechantillon"));
				return lot.getEffectif();
			} else {
				this.label_labeffectif.setText(Messages.getString("AjouterLot.SommeEffectifDuLot"));
				return getEffectif(lot);
			}
		}
	}

	/**
	 * Retourne l'effectif d'un lot.
	 * 
	 * @pre lot poss�de des fils
	 * @param lot
	 *            le lot concern�
	 * @return l'effectif d'un lot
	 */
	private Float getEffectif(Lot lotPere) {
		SousListeLots sll = new SousListeLots(null);
		try {
			sll.chargeFiltreParent(lotPere);

			Float effectif_total = 0.0f;
			for (int i = 0; i < sll.size(); i++)

				effectif_total += ((Lot) sll.get(i)).getEffectif();

			return effectif_total;
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " getEffectif(lotpere) ", e);
			return null;
		}
	}

	/**
	 * Retourne l'effetif de l'op�ration courante Ne prend pas en compte les
	 * effectifs des sousLots
	 * 
	 * @return l'effetif de l'op�ration courante
	 */
	private float geteffectifope() {
		SousListeLots souslistelots = new SousListeLots(this.ope);
		float sumeffectif = 0;
		Float effectif = null;
		try {
			souslistelots.chargeFiltreDetails(true);
			for (int i = 0; i < souslistelots.size(); i++) {
				effectif = ((Lot) souslistelots.get(i)).getEffectif(); // change
				// C�dric
				// 2016
				// l'effectif
				// peut
				// �tre
				// null;
				if (effectif != null) {
					sumeffectif += effectif;
				}
			}
		} catch (Exception e) {
			// affichage de l'erreur dans le bordereau
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " geteffectifope ", e);
		}
		return sumeffectif;

	}

	/**
	 * Affiche l'effectif des lots dans l'op�ration courante
	 */
	private void afficheeffectifope() {
		Float effectif = this.geteffectifope();
		this.label_effectifope.setText(effectif.toString());
	}

	/**
	 * Action effectu�e lorsque les boutons 'lecture' ou 'retrait' sont cliqu�s
	 * 
	 * @param evt
	 *            l'�v�nement
	 */
	private void lectureRetraitActionperformed(java.awt.event.ActionEvent evt) {
		this.actionLectureRetrait();
	}

	/**
	 * Action effectu�e lorsque la lecture ou le retrait est activ�
	 *
	 */
	private void actionLectureRetrait() {
		this.textfielddt_refMarque.setVisible(false);
		this.combo_referencesMarques.setVisible(true);
		this.chargeReferencemarques();
	}

	/**
	 * Action effectu�e lorsque le bouton 'pose' est cliqu�
	 * 
	 * @param evt
	 *            l'�v�nement
	 */
	private void poseActionPerformed(java.awt.event.ActionEvent evt) {
		this.actionPose();
	}

	/**
	 * Action effectu�e lorsque le bouton 'pose' est cliqu�
	 */
	private void actionPose() {
		this.textfielddt_refMarque.setVisible(true);
		this.combo_referencesMarques.setVisible(false);
	}

	/**
	 * Chargement des marques rattach�es � une op�ration de marquage
	 */
	private void chargeReferencemarques() {
		// la sous liste des marques pour charger les marques � partir d'une
		// op�ration de marquage
		OperationMarquage opeMarquage = (OperationMarquage) combo_OperationMarquage.getSelectedItem();
		SousListeMarquages slm = new SousListeMarquages(null);
		try {
			// chargement des marques � partir de l'op�ration de marquage
			slm.chargeFiltreOperationMarquage(opeMarquage);
			this.combo_referencesMarques.setModel(new DefaultComboBoxModel(slm.toArray()));
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " chargeReferencemarques ", e);
		}
	}

}
