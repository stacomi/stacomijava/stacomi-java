/*
 * electionnerOperation.java
 *
 * Created on 26 mai 2004, 09:42
 */

package migration.ihm;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.SystemColor;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;

import commun.Accueil;
import commun.Erreur;
import commun.IhmAppli;
import commun.Lanceur;
import infrastructure.DC;
import infrastructure.DF;
import infrastructure.Ouvrage;
import infrastructure.Station;
import infrastructure.referenciel.ListeStations;
import infrastructure.referenciel.SousListeDC;
import infrastructure.referenciel.SousListeDF;
import infrastructure.referenciel.SousListeOuvrages;
import migration.Operation;
import migration.referenciel.SousListeOperations;
import systeme.Masque;
import systeme.MasqueOpe;
import systeme.referenciel.ListeMasque;
import systeme.referenciel.ListeMasqueOpe;
import systeme.referenciel.SousListeCondEnvOpe;

/**
 * Selection d'une op�ration
 * 
 * @author Samuel Gaudey
 * @author C�dric Briand
 */
public class SelectionnerOperation extends javax.swing.JPanel {

	/**  */

	private static final long serialVersionUID = -4195056582288045099L;

	private final static Logger logger = Logger.getLogger(SelectionnerOperation.class.getName());

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JLabel label_annee;

	private javax.swing.JComboBox anneeOpe;

	private javax.swing.JPanel bottom;

	private javax.swing.JPanel center;

	private javax.swing.JLabel label_dc;

	private javax.swing.JLabel label_df;

	private javax.swing.JButton annuler;

	private javax.swing.JScrollPane jScrollPane1;

	private javax.swing.JLabel label_liste;

	private javax.swing.JComboBox liste_dc;

	private javax.swing.JComboBox liste_df;

	private javax.swing.JList liste_operations;

	private javax.swing.JComboBox liste_ouvrages;

	private javax.swing.JComboBox liste_stations;

	private javax.swing.JButton m_lots;

	private javax.swing.JButton m_ope;

	private javax.swing.JLabel label_ouvrage;

	private javax.swing.JLabel resultat;

	private javax.swing.JLabel label_station;

	private javax.swing.JLabel titre;

	private javax.swing.JPanel top;

	// End of variables declaration//GEN-END:variables

	// Listes pour la selection de la station, puis de l'ouvrage, puis...
	private ListeStations lesStations;

	private SousListeOuvrages lesOuvrages;

	private SousListeDF lesDF;

	private SousListeDC lesDC;

	private SousListeOperations lesOperations;

	private JLabel label_masqueope;
	private JLabel lblMasque;
	private MasqueOpe masqueope;
	private SousListeCondEnvOpe les_condenvope_en_base;
	private Boolean[] affichage; // valeurs r�cup�r�es depuis le masqueope
	private String[] valeurdefaut; // valeurs r�cup�r�es depuis le masqueope

	// Remarque : pour la jList contenant la liste des operation de controle, on
	// utilise un modele de type DefaultComboBoxModel et non pas
	// DefaultListModel car cette derniere n'a pas de constructeur prenant un
	// tableau d'objets en parametre

	/** Creates new form AjouterOperation */
	public SelectionnerOperation() {
		this.chargeListeStations();
		this.initComponents();
		this.chargeMasque();

	}

	/**
	 * Init
	 */
	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		top = new javax.swing.JPanel();
		titre = new javax.swing.JLabel();
		resultat = new javax.swing.JLabel();
		center = new javax.swing.JPanel();
		label_station = new javax.swing.JLabel();
		liste_stations = new javax.swing.JComboBox();
		label_ouvrage = new javax.swing.JLabel();
		liste_ouvrages = new javax.swing.JComboBox();
		label_df = new javax.swing.JLabel();
		liste_df = new javax.swing.JComboBox();
		label_dc = new javax.swing.JLabel();
		liste_dc = new javax.swing.JComboBox();
		label_annee = new javax.swing.JLabel();
		anneeOpe = new javax.swing.JComboBox();
		label_liste = new javax.swing.JLabel();
		jScrollPane1 = new javax.swing.JScrollPane();
		liste_operations = new javax.swing.JList();
		bottom = new javax.swing.JPanel();
		m_ope = new javax.swing.JButton();
		m_lots = new javax.swing.JButton();
		annuler = new javax.swing.JButton();

		setBackground(new java.awt.Color(255, 255, 255));
		setPreferredSize(new java.awt.Dimension(600, 400));
		setLayout(new java.awt.BorderLayout());

		top.setLayout(new java.awt.BorderLayout());

		titre.setBackground(new java.awt.Color(0, 51, 153));
		titre.setFont(new java.awt.Font("Dialog", 1, 14)); //$NON-NLS-1$
		titre.setForeground(new java.awt.Color(255, 255, 255));
		titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		titre.setText(Messages.getString("SelectionnerOperation.1")); //$NON-NLS-1$
		titre.setOpaque(true);
		top.add(titre, java.awt.BorderLayout.CENTER);

		resultat.setBackground(new java.awt.Color(255, 255, 255));
		resultat.setForeground(new java.awt.Color(255, 0, 0));
		resultat.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		resultat.setOpaque(true);
		top.add(resultat, java.awt.BorderLayout.SOUTH);

		add(top, java.awt.BorderLayout.NORTH);

		center.setBackground(new java.awt.Color(230, 230, 230));
		center.setPreferredSize(new java.awt.Dimension(600, 400));
		center.setLayout(new java.awt.GridBagLayout());

		// MASQUE
		////////////////////////////////////////////////////////////////////

		lblMasque = new JLabel("masque"); //$NON-NLS-1$
		lblMasque.setForeground(SystemColor.textInactiveText);
		GridBagConstraints gbc_lblMasque = new GridBagConstraints();
		gbc_lblMasque.insets = new Insets(0, 0, 5, 5);
		gbc_lblMasque.gridx = 0;
		gbc_lblMasque.gridy = 0;
		gbc_lblMasque.anchor = java.awt.GridBagConstraints.WEST;
		center.add(lblMasque, gbc_lblMasque);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		label_masqueope = new JLabel(Messages.getString("AjouterOperationM.label.text")); //$NON-NLS-1$
		label_masqueope.setForeground(SystemColor.textInactiveText);
		// Border border = BorderFactory.createLoweredBevelBorder();
		// labelmasqueope.setBorder(border);

		label_masqueope.setFont(new Font("Tahoma", Font.ITALIC, 11));
		gbc.insets = new Insets(0, 0, 5, 5);
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.anchor = java.awt.GridBagConstraints.CENTER;
		center.add(label_masqueope, gbc);
		gbc.gridx = 3;

		label_station.setText(Messages.getString("SelectionnerOperation.2")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		center.add(label_station, gridBagConstraints);

		liste_stations.setModel(new DefaultComboBoxModel(this.lesStations.toArray()));
		liste_stations.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent evt) {
				liste_stationsMouseReleased(evt);
			}
		});
		liste_stations.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				liste_stationsActionPerformed(evt);
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		center.add(liste_stations, gridBagConstraints);

		label_ouvrage.setText(Messages.getString("SelectionnerOperation.3")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		center.add(label_ouvrage, gridBagConstraints);

		liste_ouvrages.setToolTipText(Messages.getString("SelectionnerOperation.4")); //$NON-NLS-1$
		liste_ouvrages.setEnabled(false);
		liste_ouvrages.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				liste_ouvragesMouseClicked(evt);
			}
		});
		liste_ouvrages.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				liste_ouvragesActionPerformed(evt);
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		center.add(liste_ouvrages, gridBagConstraints);

		label_df.setText(Messages.getString("SelectionnerOperation.5")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		center.add(label_df, gridBagConstraints);

		liste_df.setEnabled(false);
		liste_df.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				liste_dfMouseClicked(evt);
			}
		});
		liste_df.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				liste_dfActionPerformed(evt);
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		center.add(liste_df, gridBagConstraints);

		label_dc.setText(Messages.getString("SelectionnerOperation.6")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		center.add(label_dc, gridBagConstraints);

		liste_dc.setEnabled(false);
		liste_dc.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				liste_dcMouseClicked(evt);
			}
		});
		liste_dc.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				liste_dcActionPerformed(evt);
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		center.add(liste_dc, gridBagConstraints);

		label_annee.setText(Messages.getString("SelectionnerOperation.7")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		center.add(label_annee, gridBagConstraints);

		anneeOpe.setEnabled(false);
		anneeOpe.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				anneeOpeMouseClicked(evt);
			}
		});
		anneeOpe.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				anneeOpeActionPerformed(evt);
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		center.add(anneeOpe, gridBagConstraints);

		label_liste.setText(Messages.getString("SelectionnerOperation.23")); //$NON-NLS-1$
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 6;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(20, 0, 0, 0);
		center.add(label_liste, gridBagConstraints);

		jScrollPane1.setPreferredSize(new java.awt.Dimension(320, 180));

		liste_operations.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		jScrollPane1.setViewportView(liste_operations);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 7;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 0);
		center.add(jScrollPane1, gridBagConstraints);

		add(center, java.awt.BorderLayout.CENTER);

		bottom.setBackground(new java.awt.Color(191, 191, 224));

		m_ope.setText(Messages.getString("SelectionnerOperation.24")); //$NON-NLS-1$
		m_ope.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				m_opeActionPerformed(evt);
			}
		});
		bottom.add(m_ope);

		m_lots.setText(Messages.getString("SelectionnerOperation.25")); //$NON-NLS-1$
		m_lots.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				m_lotsActionPerformed(evt);
			}
		});
		bottom.add(m_lots);

		annuler.setText(Messages.getString("SelectionnerOperation.26")); //$NON-NLS-1$
		annuler.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annulerActionPerformed(evt);
			}
		});
		bottom.add(annuler);

		add(bottom, java.awt.BorderLayout.SOUTH);
	}// </editor-fold>//GEN-END:initComponents

	private void m_opeActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_m_opeActionPerformed
		this.valide("ModifierOperation"); //$NON-NLS-1$
	}// GEN-LAST:event_m_opeActionPerformed

	private void anneeOpeActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_anneeOpeActionPerformed
		this.chargeListeOperations();
	}// GEN-LAST:event_anneeOpeActionPerformed

	private void liste_dcActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_liste_dcActionPerformed
		this.videListeDC();
	}// GEN-LAST:event_liste_dcActionPerformed

	private void liste_dfActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_liste_dfActionPerformed
		this.chargeSousListeDC();
	}// GEN-LAST:event_liste_dfActionPerformed

	private void liste_ouvragesActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_liste_ouvragesActionPerformed
		this.chargeSousListeDF();
	}// GEN-LAST:event_liste_ouvragesActionPerformed

	private void liste_stationsActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_liste_stationsActionPerformed
		this.chargeSousListeOuvrages();
	}// GEN-LAST:event_liste_stationsActionPerformed

	private void m_lotsActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_m_lotsActionPerformed
		// this.valide(new AjouterLot(ope)) ;
		this.valide("AjouterLot"); //$NON-NLS-1$
	}// GEN-LAST:event_m_lotsActionPerformed

	private void annulerActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_effacerActionPerformed

		this.quitte();

	}// GEN-LAST:event_effacerActionPerformed
		// Ajout de cédric

	private void liste_ouvragesMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_liste_ouvragesMouseClicked
		this.chargeSousListeDF();
	}// GEN-LAST:event_liste_ouvragesMouseClicked
		// Ajout de cédric

	private void liste_dfMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_liste_dfMouseClicked
		this.chargeSousListeDC();
	}// GEN-LAST:event_liste_dfMouseClicked

	private void liste_dcMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_liste_dcMouseClicked
		this.videListeDC();
	}// GEN-LAST:event_liste_dcMouseClicked

	private void anneeOpeMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_anneeOpeMouseClicked
		this.chargeListeOperations();
	}// GEN-LAST:event_anneeOpeMouseClicked

	private void liste_stationsMouseReleased(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_liste_stationsMouseReleased
		this.chargeSousListeOuvrages();
	}// GEN-LAST:event_liste_stationsMouseReleased

	/*
	 * Reinitialise les composants du jPanel courant
	 */
	private void reInitComponents() {
		this.chargeListeStations();
		this.removeAll();
		this.initComponents();
		this.validate();
		this.repaint();

	}

	private void chargeMasque() {
		// mao_affichage boolean[],
		// 1 Station (defaut 0)
		// 2 Ouvrage (defaut 0)
		// 3 DF (defaut 0)
		// 4 DC (defaut 0)
		// 5 heuredebut (defaut 1)
		// 6 heurefin (defaut 1)
		// 7 operateur (defaut 0)
		// 8 organisme (defaut 0)
		// 9 heurerearmement (defaut 0)
		// -----------------------------------------------------
		// les �lements logiques
		// --------------------------------------------------
		// 10 is_debut_auto (defaut 0) (oui non)
		// 11 is_fin_auto (defaut 0)(valeur oui non dans le combo)
		// 12 is_rearmement (defaut 0)(La date de r�armement sert � renseigner
		// le d�but de l'op�ration suivante (si debutauto) et le fonctionnement
		// du dc)
		// 13 is_rearmementDF (defaut 0) la valeur sert aussi � renseigner le DF
		// par seulement le DC
		// -----------------------------------------------------
		// les �lements qui n'ont pas de valeur pas d�faut
		// --------------------------------------------------
		// 14 datedebut (defaut 1)
		// 15 datefin (defaut 1)
		// 16 commentaires (defaut 0)
		//
		logger.log(Level.INFO, "chargement du masque OPE");
		Preferences preference = Preferences.userRoot();
		// retourne la pr�f�rence et la valeur par d�faut si la pr�f�rence est
		// vide
		masqueope = new MasqueOpe(new Masque(preference.get("masqueope", "ope_defaut")));
		ListeMasque listemasque = new ListeMasque();
		try {
			listemasque.chargeFiltre(masqueope.getMasque().getCode());
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " chargeMasque  ", e);
		}
		Masque masque = (Masque) listemasque.get(0);
		// la liste contient plus d'�l�ments que le masque qui n'est construit
		// qu'a partir du code
		// Je remplace la valeur de masqueope.
		this.masqueope = new MasqueOpe(masque);
		ListeMasqueOpe listemasqueope = new ListeMasqueOpe();
		try {
			listemasqueope.chargeFiltre(masqueope.getMasque().getCode());
		} catch (Exception e) {
			logger.log(Level.SEVERE, "chargeMasque", e);
			this.resultat.setText(e.getMessage());
		}

		// si il y aune valeur, le masque existe en base
		if (listemasqueope.size() == 1) {
			int index; // indice de la valeur s�lectionn�e dans le masque pour
			// chaque combo

			this.masqueope = (MasqueOpe) listemasqueope.get(0);
			// je dois quand m�me aller r�cup�rer l'identifiant integer du
			// masque
			// sinon plante a l'update ou � l'insertion.
			// il a �t� pass� par masque...
			affichage = masqueope.getAffichage();
			valeurdefaut = masqueope.getValeurDefaut();
			// ---------------------------------------------------------------------
			// mise � jour du code du masque
			// ---------------------------------------------------------------------

			// r�cup�ration des donn�es du masque en cours
			this.label_masqueope.setText(this.masqueope.getMasque().getCode() + " : " + masque.getDescription());
			// ---------------------------------------------------------------------
			// mise � jour des combos et textfields � partir des valeurs par
			// d�faut
			// ---------------------------------------------------------------------
			if (valeurdefaut[0] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[0], this.liste_stations);
				this.liste_stations.setSelectedIndex(index);
				this.chargeSousListeOuvrages();
				if (valeurdefaut[1] != null) {
					index = IhmAppli.getIndexforString(valeurdefaut[1], this.liste_ouvrages);
					this.liste_ouvrages.setSelectedIndex(index);
					this.chargeSousListeDF();
					if (valeurdefaut[2] != null) {
						index = IhmAppli.getIndexforString(valeurdefaut[2], this.liste_df);
						this.liste_df.setSelectedIndex(index);
						this.chargeSousListeDC();
						if (valeurdefaut[3] != null) {
							index = IhmAppli.getIndexforString(valeurdefaut[3], this.liste_dc);
							this.liste_dc.setSelectedIndex(index);
						}
					}
				}
			}

			this.liste_stations.setEnabled(affichage[0]);
			if (affichage[0]) {
				this.label_station.setForeground(SystemColor.black);
			} else {
				this.label_station.setForeground(SystemColor.textInactiveText);
			}
			this.liste_ouvrages.setEnabled(affichage[1]);
			if (affichage[1]) {
				this.label_ouvrage.setForeground(SystemColor.black);
			} else {
				this.label_ouvrage.setForeground(SystemColor.textInactiveText);
			}

			this.liste_df.setEnabled(affichage[2]);
			if (affichage[2]) {
				this.label_df.setForeground(SystemColor.black);
			} else {
				this.label_df.setForeground(SystemColor.textInactiveText);
			}

			this.liste_dc.setEnabled(affichage[3]);
			if (affichage[3]) {
				this.label_dc.setForeground(SystemColor.black);
			} else {
				this.label_dc.setForeground(SystemColor.textInactiveText);
			}
			if (valeurdefaut[3] != null && !affichage[3]) {
				this.videListeDC();
			}

		} else {
			this.resultat.setText("Erreur de chargement du masque pour les op�rations");
		}
	}

	// charge la liste des stations
	private void chargeListeStations() {
		this.lesStations = new ListeStations();
		try {
			this.lesStations.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1003);
		}

	}

	// charge la sous liste des ouvrages
	private void chargeSousListeOuvrages() {
		Station station; // la station selectionnee

		// Recupere la station selectionne dans le combo
		station = (Station) this.liste_stations.getSelectedItem();

		// Vide la liste des station pour garder uniquement la station
		// selectionnee
		this.lesStations = new ListeStations();
		this.lesStations.put(station.getCode(), station);

		// charge la sous liste des ouvrages
		this.lesOuvrages = new SousListeOuvrages(station);

		try {
			this.lesOuvrages.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1004);
		}

		// Affectation de la sous liste des ouvrages a la station
		station.setLesElementsConstitutifs(this.lesOuvrages);

		// actualiste les combo
		this.liste_ouvrages.setModel(new DefaultComboBoxModel(this.lesOuvrages.toArray()));

		this.liste_df.setModel(new DefaultComboBoxModel());
		this.liste_dc.setModel(new DefaultComboBoxModel());
		this.liste_operations.setModel(new DefaultComboBoxModel());
		this.liste_ouvrages.setEnabled(true);
		this.liste_df.setEnabled(false);
		this.liste_dc.setEnabled(false);
		this.anneeOpe.setEnabled(false);
	}

	// charge la sous liste des DF
	private void chargeSousListeDF() {
		Ouvrage ouvrage; // l'ouvrage selectionne

		// Recupere l'ouvrage selectionne dans le combo
		ouvrage = (Ouvrage) this.liste_ouvrages.getSelectedItem();

		// Vide la liste des ouvrages pour garder uniquement celui selectionne
		this.lesOuvrages = new SousListeOuvrages();
		this.lesOuvrages.put(ouvrage.getIdentifiant(), ouvrage);

		// charge la sous liste des df
		this.lesDF = new SousListeDF(ouvrage);

		try {
			this.lesDF.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1005);
		}

		// Affectation de la sous liste des df a l'ouvrage
		ouvrage.setLesElementsConstitutifs(this.lesDF);

		// Affectation de la station a l'ouvrage
		ouvrage.setStation((Station) this.liste_stations.getSelectedItem());

		// actualiste les combo
		this.liste_df.setModel(new DefaultComboBoxModel(this.lesDF.toArray()));

		this.liste_dc.setModel(new DefaultComboBoxModel());
		this.liste_operations.setModel(new DefaultComboBoxModel());
		this.liste_df.setEnabled(true);
		this.liste_dc.setEnabled(false);
		this.anneeOpe.setEnabled(false);
	}

	// charge la sous liste des DC
	private void chargeSousListeDC() {
		DF df; // le df selectionne

		// Recupere le df selectionne dans le combo
		df = (DF) this.liste_df.getSelectedItem();

		// Vide la liste des df pour garder uniquement celui selectionne
		this.lesDF = new SousListeDF();
		this.lesDF.put(df.getIdentifiant(), df);

		// charge la sous liste des dc
		this.lesDC = new SousListeDC(df);

		try {
			this.lesDC.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1006);
		}

		// Affectation de la sous liste des dc au df
		df.setLesDC(this.lesDC);

		// Affectation de l'ouvrage au df
		df.setOuvrage((Ouvrage) this.liste_ouvrages.getSelectedItem());

		// actualiste le combo
		this.liste_dc.setModel(new DefaultComboBoxModel(this.lesDC.toArray()));
		this.liste_operations.setModel(new DefaultComboBoxModel());
		this.liste_dc.setEnabled(true);
		this.anneeOpe.setEnabled(false);

	}

	// vide la liste des dc pour ne garder que ceului selectionne
	private void videListeDC() {
		DC dc; // le dc selectionne

		// Recupere le dc selectionne dans le combo
		dc = (DC) this.liste_dc.getSelectedItem();

		// Vide la liste des dc pour garder uniquement celui selectionne
		this.lesDC = new SousListeDC();
		this.lesDC.put(dc.getIdentifiant(), dc);

		// Affectation du df au dc
		dc.setDF((DF) this.liste_df.getSelectedItem());

		this.liste_operations.setModel(new DefaultComboBoxModel());

		//// changements 2016 je veux charger la vraie liste des ann�es des
		//// op�rations

		this.lesOperations = new SousListeOperations((DC) this.lesDC.get(0));
		boolean chargementanneeok;
		// la m�thode ci dessous rapporte la premi�re et la derni�re op�ration
		// rattach�e au DC
		Integer[] rangeyear;
		try {
			rangeyear = lesOperations.chargeRangeOperation();
			Integer anneedebut = rangeyear[0];
			Integer anneefin = rangeyear[1];
			List<Integer> range = IntStream.rangeClosed(anneedebut, anneefin).boxed().collect(Collectors.toList());
			String[] lesannees = new String[(range.size() + 1)];
			lesannees[0] = "toutes";
			for (int i = 0; i < range.size(); i++) {
				lesannees[i + 1] = range.get(i).toString();
			}

			// chargement de la liste des ann�es possibles
			this.anneeOpe.setModel(new javax.swing.DefaultComboBoxModel(lesannees));

			this.anneeOpe.setEnabled(true);
		} catch (Exception e) {
			this.resultat.setText("le chargement des ann�es des op�rations du DC a �chou�");
			logger.log(Level.SEVERE, "chargeRangeOperation", e);
		}
	}

	private void chargeListeOperations() {
		String annee; // l'annee selectionne
		Integer anneeInt;

		// Recupere l'annee selectionnee dans le combo
		annee = (String) this.anneeOpe.getSelectedItem();

		// nnee a null pour
		// recuperer toutes les operations
		if (this.anneeOpe.getSelectedIndex() != 0) {
			anneeInt = new Integer(Integer.parseInt(annee));
		} else
			anneeInt = null;
		// charge la sous liste des operations
		this.lesOperations = new SousListeOperations((DC) this.lesDC.get(0));

		try {
			this.lesOperations.chargeFiltre(anneeInt);
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1008);
			System.out.println(e.getMessage());
		}

		this.liste_operations.setModel(new DefaultComboBoxModel(this.lesOperations.toArray()));
		int lastIndex = liste_operations.getModel().getSize() - 1;
		if (lastIndex >= 0) {
			liste_operations.ensureIndexIsVisible(lastIndex);
		}

	}

	/**
	 * Vide le contenu et charge le panneau d'accueil
	 */
	private void quitte() {
		Component ihm = this.getParent();
		while ((ihm != null) && !(ihm instanceof IhmAppli)) {
			ihm = ihm.getParent();
		}
		// Passe a l'ecran d'ajout d'une operation
		((IhmAppli) ihm).removeContenu();
		((IhmAppli) ihm).changeContenu(new Accueil());

	}

	private void valide(String _jPanelALancer) {
		// charge l'operation selectionnee
		try {

			// Recupere l'operation selectionnee dans la liste

			Operation ope = (Operation) (this.liste_operations.getSelectedValue());

			// Si aucune operation n'est selectionnee dans la liste : erreur
			if (ope == null) {
				this.resultat.setText(Erreur.S3012);
			}
			// sinon, une operation est selectionnee
			else {
				// Cedric modif 2009
				// informe avant de vider le lanceur de la liste des op�rations
				// en cours
				Lanceur.setSouslisteOperationParcourue(this.lesOperations);

				// Vide la liste des operations pour garder uniquement celle
				// selectionnee
				this.lesOperations = new SousListeOperations((DC) this.lesDC.get(0));
				this.lesOperations.put(ope.getIdentifiant(), ope);

				// chargement des details de l'operation dans la liste

				this.lesOperations.chargeObjet(ope);

				// Affectation du dc a l'operation
				ope.setDC((DC) this.liste_dc.getSelectedItem());

				// Informe le lanceur de l'operation courante
				Lanceur.setOperationCourante(ope);

				// Passe a l'ecran d'ajout des lots
				// Recherche du parent qui est un IhmAppli
				Component ihm = this.getParent();
				while ((ihm != null) && !(ihm instanceof IhmAppli)) {
					ihm = ihm.getParent();
				}

				// Passe a l'ecran de modification
				if (_jPanelALancer == "AjouterLot") {
					((IhmAppli) ihm).changeContenu(new AjouterLot(ope));
				} else if (_jPanelALancer == "ModifierOperation") {
					((IhmAppli) ihm).changeContenu(new ModifierOperation(ope));
				}
			}
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1009);
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
}
