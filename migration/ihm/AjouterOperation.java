/*
 * AjouterOperation.java
 *
 * Created on 26 mai 2004, 09:42
 */

package migration.ihm;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.zip.DataFormatException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DateEditor;
import javax.swing.SpinnerDateModel;
import javax.swing.UIManager;

import org.apache.commons.lang3.time.DateUtils;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import commun.DateLabelFormatter;
import commun.Erreur;
import commun.IhmAppli;
import commun.JTextFieldDataType;
import commun.Lanceur;
import commun.Message;
import commun.SousListeRefValeurParametre;
import infrastructure.ConditionEnvironnementale;
import infrastructure.DC;
import infrastructure.DF;
import infrastructure.Ouvrage;
import infrastructure.PeriodeFonctionnement;
import infrastructure.Station;
import infrastructure.StationMesure;
import infrastructure.referenciel.ListeStations;
import infrastructure.referenciel.RefTypeFonctionnement;
import infrastructure.referenciel.SousListeConditionsEnv;
import infrastructure.referenciel.SousListeDC;
import infrastructure.referenciel.SousListeDF;
import infrastructure.referenciel.SousListeOuvrages;
import infrastructure.referenciel.SousListePeriodes;
import migration.Operation;
import migration.referenciel.ListeOperations;
import migration.referenciel.RefParametre;
import migration.referenciel.RefValeurParametre;
import migration.referenciel.SousListeLots;
import systeme.CondEnvOpe;
import systeme.Masque;
import systeme.MasqueOpe;
import systeme.referenciel.ListeMasque;
import systeme.referenciel.ListeMasqueOpe;
import systeme.referenciel.SousListeCondEnvOpe;

/**
 * Ajout d'un op�ration
 * 
 * @author Samuel Gaudey
 */

public class AjouterOperation extends javax.swing.JPanel {

	/**
	 * D�claration des variables
	 */
	private static final long serialVersionUID = 4171757258979510601L;
	private javax.swing.JButton a_lot;
	private javax.swing.JPanel bottom;
	private javax.swing.JPanel center;
	private javax.swing.JLabel label_commentaires;
	private javax.swing.JLabel label_datedebut;
	private JDatePanelImpl datePaneldatedebut;
	private JSpinner timeSpinnerheuredebut;
	private JDatePickerImpl ope_date_debut_picker;
	private DateEditor ope_heure_debut;
	private UtilDateModel modeldatedebut;
	private javax.swing.JLabel label_datefin;
	private JDatePanelImpl datePaneldatefin;
	private JSpinner timeSpinnerheurefin;
	private JDatePickerImpl ope_date_fin_picker;
	private UtilDateModel modeldatefin;
	private DateEditor ope_heure_fin;
	private JSpinner timeSpinnerheure_rearmement;
	private DateEditor heure_rearmement;
	private javax.swing.JLabel label_heurerearmement;
	private javax.swing.JLabel label_dc;
	private javax.swing.JLabel label_df;
	private javax.swing.JButton effacer;
	private javax.swing.JLabel label_heuredebut;
	private javax.swing.JLabel label_heurefin;
	private javax.swing.JButton inc_operation;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JComboBox liste_dc;
	private javax.swing.JComboBox liste_df;
	private javax.swing.JComboBox liste_ouvrages;
	private javax.swing.JComboBox liste_stations;
	private javax.swing.JButton ok;
	private commun.JTextAreaDataType ope_commentaires;
	private commun.JTextFieldDataType ope_operateur;
	private commun.JTextFieldDataType ope_organisme;
	private javax.swing.JLabel label_operateur;
	private javax.swing.JLabel label_organisme;
	private javax.swing.JLabel label_ouvrage;
	private javax.swing.JTextArea resultat;
	private javax.swing.JLabel label_station;
	private javax.swing.JLabel titre;
	private javax.swing.JPanel top;
	private JLabel label_masqueope;
	private JLabel lblMasque;
	private JSeparator separator;
	private JSeparator separator_1;

	// Listes pour la selection de la station, puis de l'ouvrage, puis...

	private ListeStations lesStations;
	private SousListeOuvrages lesOuvrages;
	private SousListeDF lesDF;
	private SousListeDC lesDC;
	private SousListeDF leDFselectionne;
	private MasqueOpe masqueope;
	private SousListeCondEnvOpe les_condenvope_en_base;
	private Boolean[] affichage; // valeurs r�cup�r�es depuis le masqueope
	private String[] valeurdefaut; // valeurs r�cup�r�es depuis le masqueope
	private boolean[] vect_estqualitatif;
	private SousListeRefValeurParametre[] vect_lesvaleursqualitatives;
	private SousListeConditionsEnv lesConditionsEnvironnementales;
	private Date datedebut;
	private Date datefin;
	private Date daterearmement;
	// liste pour stocker les composants correspondant aux stations de mesure
	// JTextFieldDataType ou JComboBox
	private ArrayList<JComponent> listofcomponent = new ArrayList<JComponent>();
	private Date dateFinDerniereOpe;
	private final static Logger logger = Logger.getLogger(AjouterOperation.class.getName());

	/** Creates new form AjouterOperation */
	public AjouterOperation() {
		this.chargeListeStations();
		this.initComponents();

		// a cette �tape les date et heure de d�but et de fin sont d�termin�es �
		// partir de la derni�re op�ration
		this.chargeMasque();
		// Recupere les infos de la derniere operation enregistree pour
		// l'afficher
		this.afficheOpeSuivante();
		// initialise les combo avec les cond env en se basant sur la date de
		// d�but de l'op�ration pour aller chercher les condenv, et les valeurs
		// par d�faut du masque
		// pour l'ensemble des �l�ments s�lectionn�s
		this.chargeCondEnvdujour();
		this.recalcule_horodate_si_armement();
	}

	/**
	 * M�thode lanc�e � partir de la saisie des lots pour passer � l'op�ration
	 * suivante
	 * 
	 * @param ope_
	 *            L'op�ration a partir de laquelle est incr�ment�e l'op�ration
	 *            suivante
	 */
	public AjouterOperation(Operation ope_) {
		this.chargeListeStations();
		this.initComponents();

		// a cette �tape les date et heure de d�but et de fin sont d�termin�es �
		// partir de la derni�re op�ration
		this.chargeMasque();
		// Recupere les infos de la derniere operation enregistree pour
		// l'afficher
		this.afficheOpeSuivante(ope_);
		// initialise les combo avec les cond env en se basant sur la date de
		// d�but de l'op�ration pour aller chercher les condenv, et les valeurs
		// par d�faut du masque
		// pour l'ensemble des �l�ments s�lectionn�s
		this.chargeCondEnvdujour();
		this.recalcule_horodate_si_armement();
	}

	/**
	 * InitComponents, also loads initstationsmesure
	 */

	/**
	 * 
	 */
	/**
	 * 
	 */
	private void initComponents() {// GEN-BEGIN:initComponents

		this.top = new javax.swing.JPanel();
		this.titre = new javax.swing.JLabel();
		this.resultat = new javax.swing.JTextArea();
		this.center = new javax.swing.JPanel();
		this.label_station = new javax.swing.JLabel();
		this.bottom = new javax.swing.JPanel();
		this.ok = new javax.swing.JButton();
		this.effacer = new javax.swing.JButton();
		this.a_lot = new javax.swing.JButton();
		this.inc_operation = new javax.swing.JButton();
		this.jLabel1 = new javax.swing.JLabel();

		// d�finition des layout des jPanels

		this.setLayout(new java.awt.BorderLayout());
		this.setBackground(new java.awt.Color(255, 255, 255));
		this.setPreferredSize(new java.awt.Dimension(600, 400));
		this.top.setLayout(new java.awt.BorderLayout());
		this.titre.setBackground(new java.awt.Color(0, 51, 153));
		this.titre.setFont(new java.awt.Font("Dialog", 1, 14)); //$NON-NLS-1$
		this.titre.setForeground(new java.awt.Color(255, 255, 255));
		this.titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		this.titre.setText(Messages.getString("AjouterOperation.1")); //$NON-NLS-1$
		this.titre.setOpaque(true);
		this.top.add(this.titre, java.awt.BorderLayout.CENTER);
		this.resultat.setBackground(new java.awt.Color(255, 255, 255));
		this.resultat.setForeground(new java.awt.Color(255, 0, 0));
		this.resultat.setAlignmentX(javax.swing.SwingConstants.CENTER);
		this.resultat.setOpaque(true);
		this.top.add(this.resultat, java.awt.BorderLayout.SOUTH);
		this.add(this.top, java.awt.BorderLayout.NORTH);

		// d�finition de center
		GridBagLayout gbl_center = new GridBagLayout();
		gbl_center.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		gbl_center.columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_center.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_center.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		this.center.setLayout(gbl_center);
		this.center.setBackground(new java.awt.Color(230, 230, 230));
		this.center.setPreferredSize(new java.awt.Dimension(600, 400));

		////////////////////////////////////////////////////////////////////
		// MASQUE
		////////////////////////////////////////////////////////////////////

		lblMasque = new JLabel(Messages.getString("AjouterOperationM.lblMasque.text")); //$NON-NLS-1$
		lblMasque.setForeground(SystemColor.textInactiveText);
		GridBagConstraints gbc_lblMasque = new GridBagConstraints();
		gbc_lblMasque.insets = new Insets(0, 0, 5, 5);
		gbc_lblMasque.gridx = 0;
		gbc_lblMasque.gridy = 0;
		gbc_lblMasque.anchor = java.awt.GridBagConstraints.WEST;
		center.add(lblMasque, gbc_lblMasque);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		label_masqueope = new JLabel(Messages.getString("AjouterOperationM.label.text")); //$NON-NLS-1$
		label_masqueope.setForeground(SystemColor.textInactiveText);
		// Border border = BorderFactory.createLoweredBevelBorder();
		// labelmasqueope.setBorder(border);

		label_masqueope.setFont(new Font("Tahoma", Font.ITALIC, 11));
		gbc.insets = new Insets(0, 0, 5, 5);
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.anchor = java.awt.GridBagConstraints.CENTER;
		center.add(label_masqueope, gbc);
		gbc.gridx = 3;

		separator = new JSeparator();
		separator.setPreferredSize(new Dimension(0, 50));
		separator.setOpaque(true);
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.gridwidth = 5;
		gbc_separator.insets = new Insets(0, 0, 5, 5);
		gbc_separator.gridx = 0;
		gbc_separator.gridy = 1;
		center.add(separator, gbc_separator);

		////////////////////////////////////////////////////////////////////
		// ligne station
		////////////////////////////////////////////////////////////////////
		this.label_station.setText(Messages.getString("AjouterOperation.2")); //$NON-NLS-1$
		this.label_station.setToolTipText(Messages.getString("AjouterOperation.3")); //$NON-NLS-1$
		GridBagConstraints gbc0 = new GridBagConstraints();
		gbc0.insets = new Insets(0, 0, 5, 5);
		gbc0.gridx = 0;
		gbc0.gridy = 2;
		gbc0.anchor = java.awt.GridBagConstraints.WEST;
		this.center.add(this.label_station, gbc0);

		this.liste_stations = new javax.swing.JComboBox();
		GridBagConstraints gbc1 = new GridBagConstraints();
		gbc1.fill = GridBagConstraints.HORIZONTAL;
		gbc1.insets = new Insets(0, 0, 5, 5);
		gbc1.gridx = 1;
		gbc1.gridy = 2;
		gbc1.gridwidth = 2;
		this.center.add(this.liste_stations, gbc1);
		this.liste_stations.setModel(new DefaultComboBoxModel(this.lesStations.toArray()));

		liste_stations.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (liste_stations.isEnabled()) {
						liste_stations.setEnabled(true);
						label_station.setForeground(SystemColor.textInactiveText);
					} else {
						label_station.setForeground(SystemColor.black);
						liste_stations.setEnabled(false);
					}
				} else {
					AjouterOperation.this.liste_stationsmouseClicked(e);

				}

			}
		});

		label_station.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (liste_stations.isEnabled()) {
						liste_stations.setEnabled(true);
						label_station.setForeground(SystemColor.textInactiveText);
					} else {
						label_station.setForeground(SystemColor.black);
					}
				}
			}
		});
		this.liste_stations.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AjouterOperation.this.liste_stationsActionPerformed(e);
			}
		});

		////////////////////////////////////////////////////////////////////
		// ouvrage
		////////////////////////////////////////////////////////////////////

		this.label_ouvrage = new javax.swing.JLabel();
		label_ouvrage.setFont(UIManager.getFont("Label.font"));
		GridBagConstraints gbc2 = new GridBagConstraints();
		gbc2.gridx = 3;
		gbc2.gridy = 2;
		gbc2.anchor = java.awt.GridBagConstraints.WEST;
		gbc2.insets = new Insets(2, 5, 5, 0);
		this.center.add(this.label_ouvrage, gbc2);
		this.label_ouvrage.setText(Messages.getString("AjouterOperation.8")); //$NON-NLS-1$
		this.label_ouvrage.setToolTipText(Messages.getString("AjouterOperation.9")); //$NON-NLS-1$

		this.liste_ouvrages = new javax.swing.JComboBox();
		GridBagConstraints gbc3 = new GridBagConstraints();
		gbc3.fill = GridBagConstraints.HORIZONTAL;
		gbc3.insets = new Insets(0, 0, 5, 5);
		gbc3.gridx = 4;
		gbc3.gridy = 2;
		this.center.add(this.liste_ouvrages, gbc3);
		this.liste_ouvrages.setEnabled(false);
		this.liste_ouvrages.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				AjouterOperation.this.liste_ouvragesActionPerformed(evt);
			}
		});
		this.liste_ouvrages.setToolTipText(Messages.getString("AjouterOperation.10")); //$NON-NLS-1$

		liste_ouvrages.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (liste_ouvrages.isEnabled()) {
						liste_ouvrages.setEnabled(false);
						label_ouvrage.setForeground(SystemColor.textInactiveText);
					} else {
						liste_ouvrages.setEnabled(true);
						label_ouvrage.setForeground(SystemColor.black);
					}
				} else {
					AjouterOperation.this.liste_ouvragesMouseClicked(e);
				}
			}
		});

		label_ouvrage.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (liste_ouvrages.isEnabled()) {
						liste_ouvrages.setEnabled(false);
						label_ouvrage.setForeground(SystemColor.textInactiveText);
					} else {
						liste_ouvrages.setEnabled(true);
						label_ouvrage.setForeground(SystemColor.black);
					}
				}
			}
		});

		////////////////////////////////////////////////////////////////////
		// DF
		////////////////////////////////////////////////////////////////////
		this.label_df = new javax.swing.JLabel();
		label_df.setFont(UIManager.getFont("Label.font"));
		GridBagConstraints gbc4 = new GridBagConstraints();
		gbc4.gridx = 0;
		gbc4.gridy = 3;
		gbc4.anchor = java.awt.GridBagConstraints.WEST;
		gbc4.insets = new Insets(2, 0, 5, 5);
		this.center.add(this.label_df, gbc4);
		this.label_df.setText(Messages.getString("AjouterOperation.13")); //$NON-NLS-1$
		this.label_df.setToolTipText(Messages.getString("AjouterOperation.14")); //$NON-NLS-1$

		this.liste_df = new javax.swing.JComboBox();
		GridBagConstraints gbc5 = new GridBagConstraints();
		gbc5.fill = GridBagConstraints.HORIZONTAL;
		gbc5.insets = new Insets(0, 0, 5, 5);
		gbc5.gridx = 1;
		gbc5.gridy = 3;
		gbc5.gridwidth = 2;
		this.center.add(this.liste_df, gbc5);
		this.liste_df.setEnabled(false);
		// ===================================================================
		liste_df.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (liste_df.isEnabled()) {
						liste_df.setEnabled(false);
						label_df.setForeground(SystemColor.textInactiveText);
					} else {
						liste_df.setEnabled(true);
						label_df.setForeground(SystemColor.black);
					}
				} else {
					AjouterOperation.this.liste_dfMouseClicked(e);
				}
			}
		});

		label_df.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (liste_df.isEnabled()) {
						liste_df.setEnabled(false);
						label_df.setForeground(SystemColor.textInactiveText);
					} else {
						liste_df.setEnabled(true);
						label_df.setForeground(SystemColor.black);
					}
				}
			}
		});

		this.liste_df.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				AjouterOperation.this.liste_dfActionPerformed(evt);
			}
		});
		// ===================================================================
		// DC
		// ===================================================================
		this.label_dc = new javax.swing.JLabel();
		GridBagConstraints gbc6 = new GridBagConstraints();
		gbc6.gridx = 3;
		gbc6.gridy = 3;
		gbc6.anchor = java.awt.GridBagConstraints.WEST;
		gbc6.insets = new Insets(2, 5, 5, 0);
		this.center.add(this.label_dc, gbc6);
		this.label_dc.setText(Messages.getString("AjouterOperation.17")); //$NON-NLS-1$
		this.label_dc.setToolTipText(Messages.getString("AjouterOperation.18")); //$NON-NLS-1$

		this.liste_dc = new javax.swing.JComboBox();
		GridBagConstraints gbc7 = new GridBagConstraints();
		gbc7.fill = GridBagConstraints.HORIZONTAL;
		gbc7.insets = new Insets(0, 0, 5, 0);
		gbc7.gridx = 4;
		gbc7.gridy = 3;

		this.center.add(this.liste_dc, gbc7);
		// ===================================================================
		liste_dc.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (liste_dc.isEnabled()) {
						liste_dc.setEnabled(false);
						label_dc.setForeground(SystemColor.textInactiveText);
					} else {
						liste_dc.setEnabled(true);
						label_dc.setForeground(SystemColor.black);
					}
				} else {
					AjouterOperation.this.liste_dcMouseClicked(e);
				}
			}
		});
		this.liste_dc.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				AjouterOperation.this.liste_dcActionPerformed(evt);
			}
		});

		label_dc.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (liste_dc.isEnabled()) {
						liste_dc.setEnabled(false);
						label_dc.setForeground(SystemColor.textInactiveText);
					} else {
						liste_dc.setEnabled(true);
						label_dc.setForeground(SystemColor.black);
					}
				}
			}
		});

		this.liste_dc.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent evt) {
				videListeDC();
			}
		});
		// ===================================================================

		////////////////////////////////////////////////////////////////////////////
		// date d�but
		////////////////////////////////////////////////////////////////////////////

		this.label_datedebut = new javax.swing.JLabel();
		GridBagConstraints gbc8 = new GridBagConstraints();
		gbc8.gridx = 0;
		gbc8.gridy = 4;
		gbc8.anchor = java.awt.GridBagConstraints.WEST;
		gbc8.insets = new Insets(2, 0, 5, 5);
		this.center.add(this.label_datedebut, gbc8);
		this.label_datedebut.setText("Date d\u00E9but");
		this.label_datedebut.setToolTipText("jj/mm/aaaa");

		modeldatedebut = new UtilDateModel();
		Properties p = new Properties();
		p.put("text.today", "Aujourd'hui");
		p.put("text.month", "Mois");
		p.put("text.year", "Annee");

		datePaneldatedebut = new JDatePanelImpl(modeldatedebut, p);
		ope_date_debut_picker = new JDatePickerImpl(datePaneldatedebut, new DateLabelFormatter());
		ope_date_debut_picker.setTextEditable(true);
		GridBagConstraints gbc9 = new GridBagConstraints();
		gbc9.fill = GridBagConstraints.HORIZONTAL;
		gbc9.insets = new Insets(0, 0, 5, 5);
		gbc9.gridx = 1;
		gbc9.gridy = 4;
		gbc9.gridwidth = 2;
		this.center.add(ope_date_debut_picker, gbc9);

		ope_date_debut_picker.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (ope_date_debut_picker.isEnabled()) {
						ope_date_debut_picker.setEnabled(false);
						label_datedebut.setForeground(SystemColor.textInactiveText);
					} else {
						ope_date_debut_picker.setEnabled(true);
						label_datedebut.setForeground(SystemColor.black);
					}
				}
			}
		});

		label_datedebut.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (ope_date_debut_picker.isEnabled()) {
						ope_date_debut_picker.setEnabled(false);
						label_datedebut.setForeground(SystemColor.textInactiveText);
					} else {
						ope_date_debut_picker.setEnabled(true);
						label_datedebut.setForeground(SystemColor.black);
					}
				}
			}
		});

		ope_date_debut_picker.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				chargeCondEnvdujour();

			}
		});

		///////////////////////////////////////////////////////////////////////////////////
		// heure d�but
		///////////////////////////////////////////////////////////////////////////////////

		this.label_heuredebut = new javax.swing.JLabel();
		GridBagConstraints gbc12 = new GridBagConstraints();
		gbc12.insets = new Insets(2, 5, 5, 0);
		gbc12.gridx = 3;
		gbc12.gridy = 4;
		gbc12.anchor = java.awt.GridBagConstraints.WEST;
		this.center.add(this.label_heuredebut, gbc12);
		this.label_heuredebut.setText(Messages.getString("AjouterOperation.23"));
		this.label_heuredebut.setForeground(SystemColor.textInactiveText);
		this.label_heuredebut.setToolTipText(Messages.getString("AjouterOperation.24"));

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 9);
		cal.set(Calendar.MINUTE, 0);
		Date date = cal.getTime();
		timeSpinnerheuredebut = new JSpinner(new SpinnerDateModel(date, null, null, Calendar.MINUTE));
		this.ope_heure_debut = new JSpinner.DateEditor(timeSpinnerheuredebut, "HH:mm");

		timeSpinnerheuredebut.setEditor(ope_heure_debut);

		label_heuredebut.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (timeSpinnerheuredebut.isEnabled()) {
						timeSpinnerheuredebut.setEnabled(false);
						label_heuredebut.setForeground(SystemColor.textInactiveText);

					} else {
						timeSpinnerheuredebut.setEnabled(true);
						label_heuredebut.setForeground(SystemColor.black);

					}
				}
			}
		});

		GridBagConstraints gbc11 = new GridBagConstraints();
		gbc11.anchor = GridBagConstraints.WEST;
		gbc11.insets = new Insets(0, 0, 5, 5);
		gbc11.gridx = 4;
		gbc11.gridy = 4;
		this.center.add(this.timeSpinnerheuredebut, gbc11);

		///////////////////////////////////////////////////////////////////////////////////
		// date fin
		///////////////////////////////////////////////////////////////////////////////////

		this.label_datefin = new javax.swing.JLabel();
		label_datefin.setFont(UIManager.getFont("Label.font"));
		GridBagConstraints gbc10 = new GridBagConstraints();
		gbc10.gridx = 0;
		gbc10.gridy = 5;
		gbc10.anchor = java.awt.GridBagConstraints.WEST;
		gbc10.insets = new Insets(2, 0, 5, 5);
		this.center.add(this.label_datefin, gbc10);
		this.label_datefin.setText("Date fin");
		this.label_datefin.setForeground(SystemColor.textInactiveText);
		this.label_datefin.setToolTipText(Messages.getString("jj/mm/aaaa"));

		modeldatefin = new UtilDateModel();
		datePaneldatefin = new JDatePanelImpl(modeldatefin, p);
		ope_date_fin_picker = new JDatePickerImpl(datePaneldatefin, new DateLabelFormatter());
		ope_date_fin_picker.setTextEditable(true);
		// datePaneldatefin.setDoubleClickAction(true);
		GridBagConstraints gbc13 = new GridBagConstraints();
		gbc13.fill = GridBagConstraints.HORIZONTAL;
		gbc13.insets = new Insets(0, 0, 5, 5);
		gbc13.gridx = 1;
		gbc13.gridy = 5;
		gbc13.gridwidth = 2;
		this.center.add(ope_date_fin_picker, gbc13);

		ope_date_fin_picker.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (ope_date_fin_picker.isEnabled()) {
						ope_date_fin_picker.setEnabled(false);
						label_datefin.setForeground(SystemColor.textInactiveText);
					} else {
						ope_date_fin_picker.setEnabled(true);
						label_datefin.setForeground(SystemColor.black);
					}
				}
			}
		});

		label_datefin.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (ope_date_fin_picker.isEnabled()) {
						ope_date_fin_picker.setEnabled(false);
						label_datefin.setForeground(SystemColor.textInactiveText);
					} else {
						ope_date_fin_picker.setEnabled(true);
						label_datefin.setForeground(SystemColor.black);
					}
				}
			}
		});

		///////////////////////////////////////////////////////////////////////////////////
		// heurefin
		/////////////////////////////////////////////////////////////////////////////////

		this.label_heurefin = new javax.swing.JLabel();
		GridBagConstraints gbc14 = new GridBagConstraints();
		gbc14.insets = new Insets(2, 5, 5, 0);
		gbc14.gridx = 3;
		gbc14.gridy = 5;
		gbc14.anchor = java.awt.GridBagConstraints.WEST;
		this.center.add(this.label_heurefin, gbc14);
		this.label_heurefin.setText(Messages.getString("AjouterOperation.27")); //$NON-NLS-1$
		this.label_heurefin.setForeground(SystemColor.textInactiveText);
		this.label_heurefin.setToolTipText(Messages.getString("AjouterOperation.28"));
		label_heurefin.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (timeSpinnerheurefin.isEnabled()) {
						timeSpinnerheurefin.setEnabled(false);
						label_heurefin.setForeground(SystemColor.textInactiveText);

					} else {
						timeSpinnerheurefin.setEnabled(true);
						label_heurefin.setForeground(SystemColor.black);

					}
				}
			}
		});

		timeSpinnerheurefin = new JSpinner(new SpinnerDateModel(date, null, null, Calendar.MINUTE));
		this.ope_heure_fin = new JSpinner.DateEditor(timeSpinnerheurefin, "HH:mm");
		timeSpinnerheurefin.setEditor(ope_heure_fin);
		GridBagConstraints gbc15 = new GridBagConstraints();
		gbc15.anchor = GridBagConstraints.WEST;
		gbc15.insets = new Insets(0, 0, 5, 5);
		gbc15.gridx = 4;
		gbc15.gridy = 5;
		this.center.add(this.timeSpinnerheurefin, gbc15);

		///////////////////////////////////////////////////////////////////////////////////
		// organisme
		//////////////////////////////////////////////////////////////////////////////////

		this.label_organisme = new javax.swing.JLabel();
		label_organisme.setForeground(SystemColor.textInactiveText);
		GridBagConstraints gbc16 = new GridBagConstraints();
		gbc16.gridx = 0;
		gbc16.gridy = 6;
		gbc16.anchor = java.awt.GridBagConstraints.WEST;
		gbc16.insets = new Insets(2, 0, 5, 5);
		this.center.add(this.label_organisme, gbc16);
		this.label_organisme.setText(Messages.getString("AjouterOperation.29")); //$NON-NLS-1$
		this.ope_organisme = new commun.JTextFieldDataType();

		GridBagConstraints gbc17 = new GridBagConstraints();
		gbc17.gridwidth = 2;
		gbc17.fill = GridBagConstraints.HORIZONTAL;
		gbc17.insets = new Insets(0, 0, 5, 5);
		gbc17.gridx = 1;
		gbc17.gridy = 6;
		this.center.add(this.ope_organisme, gbc17);
		this.ope_organisme.setColumns(10);
		ope_organisme.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (ope_organisme.isEnabled()) {
						ope_organisme.setEnabled(false);

						label_organisme.setForeground(SystemColor.textInactiveText);
					} else {
						ope_organisme.setEnabled(true);
						label_organisme.setForeground(SystemColor.black);

					}
				}
			}
		});

		label_organisme.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (ope_organisme.isEnabled()) {
						ope_organisme.setEnabled(false);
						label_organisme.setForeground(SystemColor.textInactiveText);
					} else {
						ope_organisme.setEnabled(true);
						label_organisme.setForeground(SystemColor.black);

					}
				}
			}
		});

		// operateur

		this.label_operateur = new javax.swing.JLabel();
		label_operateur.setForeground(SystemColor.textInactiveText);
		GridBagConstraints gbc18 = new GridBagConstraints();
		gbc18.insets = new Insets(2, 5, 5, 0);
		gbc18.gridx = 3;
		gbc18.gridy = 6;
		gbc18.anchor = java.awt.GridBagConstraints.WEST;
		this.center.add(this.label_operateur, gbc18);
		this.label_operateur.setText(Messages.getString("AjouterOperation.30")); //$NON-NLS-1$
		this.ope_operateur = new commun.JTextFieldDataType();
		GridBagConstraints gbc19 = new GridBagConstraints();
		gbc19.fill = GridBagConstraints.HORIZONTAL;
		gbc19.insets = new Insets(0, 0, 5, 5);
		gbc19.gridx = 4;
		gbc19.gridy = 6;
		this.ope_operateur.setColumns(10);
		this.center.add(this.ope_operateur, gbc19);

		ope_operateur.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (ope_operateur.isEnabled()) {
						ope_operateur.setEnabled(false);

						label_operateur.setForeground(SystemColor.textInactiveText);
					} else {
						ope_operateur.setEnabled(true);
						label_operateur.setForeground(SystemColor.black);

					}
				}
			}
		});

		label_operateur.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (ope_operateur.isEnabled()) {
						ope_operateur.setEnabled(false);

						label_operateur.setForeground(SystemColor.textInactiveText);
					} else {
						ope_operateur.setEnabled(true);
						label_operateur.setForeground(SystemColor.black);

					}
				}
			}
		});
		////////////////////////////////////////////////////////////////////
		// commentaires
		////////////////////////////////////////////////////////////////////

		this.label_commentaires = new javax.swing.JLabel();
		label_commentaires.setFont(UIManager.getFont("Label.font"));
		label_commentaires.setForeground(SystemColor.textInactiveText);
		GridBagConstraints gbc20 = new GridBagConstraints();
		gbc20.fill = GridBagConstraints.VERTICAL;
		gbc20.gridx = 0;
		gbc20.gridy = 7;
		gbc20.anchor = GridBagConstraints.WEST;
		gbc20.insets = new Insets(2, 0, 5, 5);
		this.center.add(this.label_commentaires, gbc20);
		this.label_commentaires.setText(Messages.getString("AjouterOperation.31")); //$NON-NLS-1$
		this.jScrollPane1 = new javax.swing.JScrollPane();

		GridBagConstraints gbc21 = new GridBagConstraints();
		gbc21.gridwidth = 2;
		gbc21.fill = GridBagConstraints.BOTH;
		gbc21.gridx = 1;
		gbc21.gridy = 7;
		gbc21.insets = new Insets(2, 0, 5, 5);
		this.center.add(this.jScrollPane1, gbc21);
		this.ope_commentaires = new commun.JTextAreaDataType();
		jScrollPane1.setViewportView(ope_commentaires);
		this.ope_commentaires.setLineWrap(true);
		this.ope_commentaires.setRows(2);

		ope_commentaires.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (ope_commentaires.isEnabled()) {
						ope_commentaires.setEnabled(false);
						label_commentaires.setForeground(SystemColor.textInactiveText);

					} else {
						ope_commentaires.setEnabled(true);
						label_commentaires.setForeground(SystemColor.black);

					}
				}
			}
		});

		label_commentaires.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					if (ope_commentaires.isEnabled()) {
						ope_commentaires.setEnabled(false);
						label_commentaires.setForeground(SystemColor.textInactiveText);

					} else {
						ope_commentaires.setEnabled(true);
						label_commentaires.setForeground(SystemColor.black);

					}
				}
			}
		});
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[] { liste_stations, liste_ouvrages, liste_df,
				liste_dc, ope_date_debut_picker.getJFormattedTextField(), timeSpinnerheuredebut,
				ope_date_fin_picker.getJFormattedTextField(), timeSpinnerheurefin, ope_organisme, ope_operateur,
				ope_commentaires, ok, a_lot, inc_operation, effacer }));

		this.add(this.center, java.awt.BorderLayout.CENTER);

		separator_1 = new JSeparator();
		separator_1.setPreferredSize(new Dimension(0, 50));
		GridBagConstraints gbc_separator_1 = new GridBagConstraints();
		gbc_separator_1.gridwidth = 5;
		gbc_separator_1.insets = new Insets(0, 0, 0, 5);
		gbc_separator_1.gridx = 0;
		gbc_separator_1.gridy = 8;
		center.add(separator_1, gbc_separator_1);

		this.bottom.setLayout(new java.awt.GridBagLayout());

		this.bottom.setBackground(new java.awt.Color(191, 191, 224));
		this.ok.setText(Messages.getString("AjouterOperation.32")); //$NON-NLS-1$
		this.ok.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				AjouterOperation.this.okActionPerformed(evt);
			}
		});
		GridBagConstraints gbc22 = new GridBagConstraints();
		gbc22.insets = new java.awt.Insets(2, 2, 2, 2);
		this.bottom.add(this.ok, gbc22);
		this.ok.setEnabled(true);

		this.effacer.setText(Messages.getString("AjouterOperation.33")); //$NON-NLS-1$
		this.effacer.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				AjouterOperation.this.effacerActionPerformed(evt);
			}
		});
		GridBagConstraints gbc23 = new GridBagConstraints();
		gbc23.gridx = 0;
		gbc23.gridy = 1;
		gbc23.insets = new java.awt.Insets(2, 2, 2, 2);
		this.bottom.add(this.effacer, gbc23);

		this.a_lot.setText(Messages.getString("AjouterOperation.34")); //$NON-NLS-1$
		this.a_lot.setEnabled(false);
		this.a_lot.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				AjouterOperation.this.a_lotActionPerformed(evt);
			}
		});
		GridBagConstraints gbc24 = new GridBagConstraints();
		gbc24.gridx = 2;
		gbc24.gridy = 0;
		gbc24.anchor = java.awt.GridBagConstraints.WEST;
		gbc24.insets = new java.awt.Insets(2, 2, 2, 2);
		this.bottom.add(this.a_lot, gbc24);

		this.inc_operation.setText(Messages.getString("AjouterOperation.35")); //$NON-NLS-1$
		this.inc_operation.setEnabled(false);
		this.inc_operation.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				AjouterOperation.this.inc_operationActionPerformed(evt);
			}
		});
		GridBagConstraints gbc25 = new GridBagConstraints();
		gbc25.gridx = 2;
		gbc25.gridy = 1;
		gbc25.anchor = java.awt.GridBagConstraints.WEST;
		gbc25.insets = new java.awt.Insets(2, 2, 2, 2);
		this.bottom.add(this.inc_operation, gbc25);

		this.jLabel1.setFont(new java.awt.Font("Dialog", 0, 10)); //$NON-NLS-1$
		this.jLabel1.setText(Messages.getString("AjouterOperation.37")); //$NON-NLS-1$
		GridBagConstraints gbc26 = new GridBagConstraints();
		gbc26.gridx = 1;
		gbc26.gridy = 0;
		gbc26.insets = new java.awt.Insets(2, 50, 2, 2);
		this.bottom.add(this.jLabel1, gbc26);
		this.add(this.bottom, java.awt.BorderLayout.SOUTH);

	}// GEN-END:initComponents

	private void inc_operationActionPerformed(ActionEvent evt) {// GEN-FIRST:event_inc_operationActionPerformed
		// Recherche du parent qui est un IhmAppli
		Component ihm = this.getParent();
		while ((ihm != null) && !(ihm instanceof IhmAppli)) {
			ihm = ihm.getParent();
		}

		// Passe a l'ecran d'ajout d'une operation
		((IhmAppli) ihm).changeContenu(new IncrementerOperation(Lanceur.getOperationCourante()));

	}

	private void a_lotActionPerformed(ActionEvent evt) {// GEN-FIRST:event_a_lotActionPerformed
		// Recherche du parent qui est un IhmAppli
		Component ihm = this.getParent();
		while ((ihm != null) && !(ihm instanceof IhmAppli)) {
			ihm = ihm.getParent();
		}
		// Passe a l'ecran d'ajout d'une operation
		((IhmAppli) ihm).changeContenu(new AjouterLot(Lanceur.getOperationCourante()));
	}

	private void liste_dcActionPerformed(ActionEvent evt) {// GEN-FIRST:event_liste_dcActionPerformed
		this.videListeDC();
	}

	protected void liste_dcMouseClicked(MouseEvent e) {
		this.videListeDC();
	}

	private void liste_dfActionPerformed(ActionEvent evt) {// GEN-FIRST:event_liste_dfActionPerformed
		this.chargeSousListeDC();
	}

	private void liste_dfMouseClicked(MouseEvent e) {
		this.chargeSousListeDC();

	}

	private void liste_ouvragesActionPerformed(ActionEvent e) {// GEN-FIRST:event_liste_ouvragesActionPerformed
		this.chargeSousListeDF();
	}

	private void liste_ouvragesMouseClicked(MouseEvent e) {
		this.chargeSousListeDF();

	}

	private void liste_stationsActionPerformed(ActionEvent e) {
		this.chargeSousListeOuvrages();
	}

	private void liste_stationsmouseClicked(MouseEvent e) {
		this.chargeSousListeOuvrages();
	}

	/*
	 * public void keyPressed(KeyEvent k) { liste_stationsKeyPressed(k); }
	 * 
	 * private void liste_stationsKeyPressed(java.awt.event.KeyEvent k){
	 * this.chargeSousListeOuvrages() ; }
	 */

	private void okActionPerformed(ActionEvent evt) {// GEN-FIRST:event_okActionPerformed
		this.valide();
	}// GEN-LAST:event_okActionPerformed

	private void effacerActionPerformed(ActionEvent evt) {// GEN-FIRST:event_effacerActionPerformed

		this.reInitComponents();

	}// GEN-LAST:event_effacerActionPerformed

	/*
	 * Reinitialise les composants du jPanel courant
	 */
	private void reInitComponents() {

		this.chargeListeStations();

		this.removeAll();
		this.initComponents();
		this.validate();
		this.repaint();

		// Recupere les infos de la derniere operation enregistree pour
		// l'afficher
		this.afficheOpeSuivante();
		this.chargeMasque();
	}

	private void chargeMasque() {
		// mao_affichage boolean[],
		// 1 Station (defaut 0)
		// 2 Ouvrage (defaut 0)
		// 3 DF (defaut 0)
		// 4 DC (defaut 0)
		// 5 heuredebut (defaut 1)
		// 6 heurefin (defaut 1)
		// 7 operateur (defaut 0)
		// 8 organisme (defaut 0)
		// 9 heurerearmement (defaut 0)
		// -----------------------------------------------------
		// les �lements logiques
		// --------------------------------------------------
		// 10 is_debut_auto (defaut 0) (oui non)
		// 11 is_fin_auto (defaut 0)(valeur oui non dans le combo)
		// 12 is_rearmement (defaut 0)(La date de r�armement sert � renseigner
		// le d�but de l'op�ration suivante (si debutauto) et le fonctionnement
		// du dc)
		// 13 is_rearmementDF (defaut 0) la valeur sert aussi � renseigner le DF
		// par seulement le DC
		// -----------------------------------------------------
		// les �lements qui n'ont pas de valeur pas d�faut
		// --------------------------------------------------
		// 14 datedebut (defaut 1)
		// 15 datefin (defaut 1)
		// 16 commentaires (defaut 0)
		//
		logger.log(Level.INFO, "chargement du masque OPE");
		Preferences preference = Preferences.userRoot();
		// retourne la pr�f�rence et la valeur par d�faut si la pr�f�rence est
		// vide
		masqueope = new MasqueOpe(new Masque(preference.get("masqueope", "ope_defaut")));
		ListeMasque listemasque = new ListeMasque();
		try {
			listemasque.chargeFiltre(masqueope.getMasque().getCode());
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " chargeMasque  ", e);
		}
		Masque masque = (Masque) listemasque.get(0);
		// la liste contient plus d'�l�ments que le masque qui n'est construit
		// qu'a partir du code
		// Je remplace la valeur de masqueope.
		this.masqueope = new MasqueOpe(masque);
		ListeMasqueOpe listemasqueope = new ListeMasqueOpe();
		try {
			listemasqueope.chargeFiltre(masqueope.getMasque().getCode());
		} catch (Exception e) {
			logger.log(Level.SEVERE, "chargeMasque", e);
			this.resultat.setText(e.getMessage());
		}

		// si il y aune valeur, le masque existe en base
		if (listemasqueope.size() == 1) {
			int index; // indice de la valeur s�lectionn�e dans le masque pour
			// chaque combo

			this.masqueope = (MasqueOpe) listemasqueope.get(0);
			// je dois quand m�me aller r�cup�rer l'identifiant integer du
			// masque
			// sinon plante a l'update ou � l'insertion.
			// il a �t� pass� par masque...
			affichage = masqueope.getAffichage();
			valeurdefaut = masqueope.getValeurDefaut();
			// ---------------------------------------------------------------------
			// mise � jour du code du masque
			// ---------------------------------------------------------------------

			// r�cup�ration des donn�es du masque en cours
			this.label_masqueope.setText(this.masqueope.getMasque().getCode() + " : " + masque.getDescription());
			// ---------------------------------------------------------------------
			// mise � jour des combos et textfields � partir des valeurs par
			// d�faut
			// ---------------------------------------------------------------------
			if (valeurdefaut[0] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[0], this.liste_stations);
				this.liste_stations.setSelectedIndex(index);
				this.chargeSousListeOuvrages();
				if (valeurdefaut[1] != null) {
					index = IhmAppli.getIndexforString(valeurdefaut[1], this.liste_ouvrages);
					this.liste_ouvrages.setSelectedIndex(index);
					this.chargeSousListeDF();
					if (valeurdefaut[2] != null) {
						index = IhmAppli.getIndexforString(valeurdefaut[2], this.liste_df);
						this.liste_df.setSelectedIndex(index);
						this.chargeSousListeDC();
						if (valeurdefaut[3] != null) {
							index = IhmAppli.getIndexforString(valeurdefaut[3], this.liste_dc);
							this.liste_dc.setSelectedIndex(index);
						}
					}
				}
			}

			this.liste_stations.setEnabled(affichage[0]);
			if (affichage[0]) {
				this.label_station.setForeground(SystemColor.black);
			} else {
				this.label_station.setForeground(SystemColor.textInactiveText);
			}
			this.liste_ouvrages.setEnabled(affichage[1]);
			if (affichage[1]) {
				this.label_ouvrage.setForeground(SystemColor.black);
			} else {
				this.label_ouvrage.setForeground(SystemColor.textInactiveText);
			}

			this.liste_df.setEnabled(affichage[2]);
			if (affichage[2]) {
				this.label_df.setForeground(SystemColor.black);
			} else {
				this.label_df.setForeground(SystemColor.textInactiveText);
			}

			this.liste_dc.setEnabled(affichage[3]);
			if (affichage[3]) {
				this.label_dc.setForeground(SystemColor.black);
			} else {
				this.label_dc.setForeground(SystemColor.textInactiveText);
			}

			this.ope_date_debut_picker.setEnabled(affichage[13]);
			this.datePaneldatedebut.setEnabled(affichage[13]);
			this.ope_date_fin_picker.setTextEditable(affichage[13]);
			if (affichage[13]) {
				this.label_datedebut.setForeground(SystemColor.black);
			} else {
				this.label_datedebut.setForeground(SystemColor.textInactiveText);
			}

			this.ope_date_fin_picker.setEnabled(affichage[14]);
			this.ope_date_fin_picker.setTextEditable(affichage[14]);

			this.datePaneldatefin.setEnabled(affichage[14]);
			if (affichage[14]) {
				this.label_datefin.setForeground(SystemColor.black);
			} else {
				this.label_datefin.setForeground(SystemColor.textInactiveText);
			}

			this.ope_heure_debut.getTextField().setEditable(affichage[4]);

			if (affichage[4]) {
				this.label_heuredebut.setForeground(SystemColor.black);
			} else {
				this.label_heuredebut.setForeground(SystemColor.textInactiveText);
			}
			this.ope_heure_fin.getTextField().setEditable(affichage[5]);
			if (affichage[5]) {
				this.label_heurefin.setForeground(SystemColor.black);
			} else {
				this.label_heurefin.setForeground(SystemColor.textInactiveText);
			}
			this.ope_operateur.setEnabled(affichage[6]);
			if (affichage[6]) {
				this.label_operateur.setForeground(SystemColor.black);
			} else {
				this.label_operateur.setForeground(SystemColor.textInactiveText);
			}
			this.ope_organisme.setEnabled(affichage[7]);
			if (affichage[7]) {
				this.label_organisme.setForeground(SystemColor.black);
			} else {
				this.label_organisme.setForeground(SystemColor.textInactiveText);
			}
			this.ope_commentaires.setEnabled(affichage[15]);
			if (affichage[15]) {
				this.ope_commentaires.setForeground(SystemColor.black);
			} else {
				this.ope_commentaires.setForeground(SystemColor.textInactiveText);
			}

			// si le d�but est auto, et que la valeur de is_rearment=FALSE
			// (affichage[11]) on ne fait rien, le combo a d�j� �t� initialis�
			// par afficheDerniereOpe
			// si le d�but est auto, et que la valeur de is_rearmement=TRUE
			// (affichage[11] le combo doit �tre initialis� par is_rearmement
			// si le d�but auto est false, et qu'il existe une valeur par defaut
			// [4] on prend une heure fixe
			// si le d�but auto est false et qu'il n'y a pas de valeur,
			// normalement il y a eut un warning et la valeur choisie sera quand
			// m�me celle de l'op�ration.

			// si le d�but n'est pas auto, on change la valeur par d�faut
			// calcul�e par afficheoperationsuivante
			// et on met celle du masque
			if (!affichage[9]) {// !is d�but auto
				String str_heuredebut = valeurdefaut[4];
				if (!(str_heuredebut == null)) {
					try {
						int intheuredebut = Integer.parseInt(str_heuredebut.split(":")[0]);
						int intminutedebut = Integer.parseInt(str_heuredebut.split(":")[1]);
						Calendar cal = Calendar.getInstance();
						cal.set(Calendar.HOUR_OF_DAY, intheuredebut);
						cal.set(Calendar.MINUTE, intminutedebut);
						Date date = cal.getTime();
						timeSpinnerheuredebut.setValue(date);
					} catch (IllegalArgumentException e) {
						logger.log(Level.SEVERE, " chargeMasque  ", e);
						resultat.setText(e.toString());
					}
				}
			}
			// si la fin n'est pas auto, on change la valeur par d�faut calcul�e
			// par afficheoperationsuivante
			// et on met celle du masque
			if (!affichage[10]) {// !is fin auto
				String str_heurefin = valeurdefaut[5];
				if (!(str_heurefin == null)) {
					try {
						int intheurefin = Integer.parseInt(str_heurefin.split(":")[0]);
						int intminutefin = Integer.parseInt(str_heurefin.split(":")[1]);
						Calendar cal = Calendar.getInstance();
						cal.set(Calendar.HOUR_OF_DAY, intheurefin);
						cal.set(Calendar.MINUTE, intminutefin);
						Date date = cal.getTime();
						timeSpinnerheurefin.setValue(date);
					} catch (IllegalArgumentException e) {
						resultat.setText(e.toString());
						logger.log(Level.SEVERE, " chargeMasque  ", e);
					}
				}
			}
			// ---------------------------------------------------------------------
			// // Chargement des �l�ments n�cessaires pour les pi�ges
			// // si r�armement = "oui"
			// ---------------------------------------------------------------------
			// on cr�e alors le combo (contrairement aux deux autres au dessus
			// qui ont d�j� �t� cr��s
			if (affichage[11]) { // isrearmement
				String str_heurerearmement = valeurdefaut[8];
				if (!(str_heurerearmement == null)) {
					try {
						int intheurerearmement = Integer.parseInt(str_heurerearmement.split(":")[0]);
						int intminuterearmement = Integer.parseInt(str_heurerearmement.split(":")[1]);
						Calendar cal = Calendar.getInstance();
						cal.set(Calendar.HOUR_OF_DAY, intheurerearmement);
						cal.set(Calendar.MINUTE, intminuterearmement);
						Date date = cal.getTime();
						timeSpinnerheure_rearmement = new JSpinner(
								new SpinnerDateModel(date, null, null, Calendar.MINUTE));
						heure_rearmement = new JSpinner.DateEditor(timeSpinnerheure_rearmement, "HH:mm");
						GridBagConstraints gbc = new GridBagConstraints();
						gbc.insets = new Insets(2, 5, 5, 0);
						gbc.gridx = 4;
						gbc.gridy = 7;
						gbc.anchor = GridBagConstraints.WEST;

						timeSpinnerheure_rearmement.setEditor(heure_rearmement);
						this.label_heurerearmement = new javax.swing.JLabel("Heure r�armement");
						this.label_heurerearmement.setForeground(SystemColor.black);
						this.center.add(this.timeSpinnerheure_rearmement, gbc);
						gbc.insets = new Insets(0, 0, 5, 5);
						gbc.gridx = 3;
						gbc.gridy = 7;
						gbc.anchor = java.awt.GridBagConstraints.WEST;
						this.center.add(this.label_heurerearmement, gbc);
						this.timeSpinnerheure_rearmement.setToolTipText(Messages.getString("Pour les pi�ges"));

					} catch (IllegalArgumentException e) {
						resultat.setText(e.toString());
						logger.log(Level.SEVERE, " chargeMasque  ", e);
					}
				}
				// si la fin est auto, et qu'on a un r�-armement, la fin de
				// l'op�raton doit �tre renseign�e
				// a partir de la p�riode de fonctionnement
				// mais ce travail est journalier il est lanc� dans une m�thode
				// propre
				// lanc�e dans le constructeur et au rechargement
				// (recalcule_datefin_avec_armement)

			} // affichage[11]

			String str_operateur = valeurdefaut[6];
			if (str_operateur != null) {
				this.ope_operateur.setText(str_operateur);
			}
			String str_organisme = valeurdefaut[7];
			this.ope_organisme.setText(str_organisme);

		} else {
			this.resultat.setText("Erreur de chargement du masque pour les op�rations");
		}

		// ---------------------------------------------------------------------
		// // Chargement des stations selectionn�es dans le masque
		// //
		// ---------------------------------------------------------------------
		this.les_condenvope_en_base = new SousListeCondEnvOpe(masqueope);
		try {
			les_condenvope_en_base.chargeSansFiltreDetails();
			// la m�thode chargeSansFiltre charge tous les objets de la sous
			// liste du masqueope
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " chargeMasque  ", e);
		}
		vect_estqualitatif = new boolean[les_condenvope_en_base.size()];
		vect_lesvaleursqualitatives = new SousListeRefValeurParametre[les_condenvope_en_base.size()];
		logger.log(Level.INFO, "traitement de " + les_condenvope_en_base.size() + " conditions environnementales");
		for (int i = 0; i < les_condenvope_en_base.size(); i++) {

			CondEnvOpe condenvope = (CondEnvOpe) les_condenvope_en_base.get(i);
			boolean affiche = condenvope.getAffichage();
			JLabel labelstation = new JLabel(condenvope.getStationmesure().getLibelle());
			labelstation.setToolTipText(condenvope.getStationmesure().getParametre().getLibelle());
			GridBagConstraints gbcc = new GridBagConstraints();
			if ((i % 2) == 0) {
				gbcc.gridx = 0;
			} else {
				gbcc.gridx = 3;
			}
			if ((i % 2) == 0) {
				gbcc.gridy = 9 + i;
			} else {
				gbcc.gridy = 9 + i - 1;
			}
			gbcc.anchor = java.awt.GridBagConstraints.NORTHWEST;
			gbcc.insets = new Insets(5, 5, 5, 5);
			this.center.add(labelstation, gbcc);

			StationMesure stm = condenvope.getStationmesure();

			if ((i % 2) == 0) {// even
				gbcc.gridx = 1;
			} else {// odd
				gbcc.gridx = 4;
			}
			if ((i % 2) == 0) {
				gbcc.gridy = 9 + i;
			} else {
				gbcc.gridy = 9 + i - 1;
			}
			gbcc.anchor = java.awt.GridBagConstraints.NORTHWEST;
			gbcc.insets = new Insets(2, 0, 5, 5);
			try {
				RefParametre param = stm.getParametre();
				String codepar = param.getCode();
				try {
					boolean estqualitatif = RefParametre.estQualitatif(codepar);
					vect_estqualitatif[i] = estqualitatif;
					if (estqualitatif) {

						// Cas ou la station de mesure correspond � un caract�re
						// qualitatif
						JComboBox combo = new JComboBox();
						SousListeRefValeurParametre lesValeurQualitatives = new SousListeRefValeurParametre(param);
						lesValeurQualitatives.chargeFiltreDetails();
						vect_lesvaleursqualitatives[i] = lesValeurQualitatives;
						combo.setModel(new DefaultComboBoxModel(lesValeurQualitatives.toArray()));
						listofcomponent.add(combo);
						center.add(combo, gbcc);
						combo.setEnabled(affiche);
						// Evennement double click pour activer ou d�sactiver le
						// combo
						combo.addMouseListener(new java.awt.event.MouseAdapter() {

							public void mouseClicked(java.awt.event.MouseEvent e) {
								if (e.getClickCount() == 2 && !e.isConsumed()) {
									e.consume();
									if (combo.isEnabled()) {
										combo.setEnabled(false);
										labelstation.setForeground(SystemColor.textInactiveText);

									} else {
										combo.setEnabled(true);
										labelstation.setForeground(SystemColor.black);

									}
								}
							}
						});

					} else {
						// Cas ou la station de mesure est un caract�re
						// quantitatif
						vect_lesvaleursqualitatives[i] = null; // on veut garder
						// la structure
						// et acc�der
						// aux �l�mens
						// avec i
						JTextFieldDataType textfield = new JTextFieldDataType();
						listofcomponent.add(textfield);
						center.add(textfield, gbcc);
						textfield.setColumns(10);
						textfield.setEnabled(affiche);
						textfield.addMouseListener(new java.awt.event.MouseAdapter() {

							public void mouseClicked(java.awt.event.MouseEvent e) {
								if (e.getClickCount() == 2 && !e.isConsumed()) {
									e.consume();
									if (textfield.isEnabled()) {
										textfield.setEnabled(false);
										labelstation.setForeground(SystemColor.textInactiveText);

									} else {
										textfield.setEnabled(true);
										labelstation.setForeground(SystemColor.black);

									}
								}
							}
						});
					}

					if (affiche) {// affichage du combo
						labelstation.setForeground(SystemColor.black);
					} else {
						labelstation.setForeground(SystemColor.textInactiveText);
					}

				} catch (

				Exception e)

				{
					logger.log(Level.SEVERE, "chargeMasque, chargement des conditions env", e);
					this.resultat.setText(e.getMessage());
				}
			} catch (

			Exception e)

			{
				logger.log(Level.SEVERE, "chargeMasque, chargement stm", e);
				this.resultat.setText(e.getMessage());
			}

		}

	}

	/**
	 * On remet l'heure de fin � partir de l'heure de r�armement de la derni�re
	 * op�ration Cette derni�re suppose que le masque ait �t� utilis� lors de la
	 * pr�c�dente ope pour v�rifier on regarde si la derni�re date de d�but de
	 * fonctionnement du dc correspond � la derni�re date d�but d'op�. Si c'est
	 * le cas on applique l'heure du r�armement = heure de fonctionnement du dc
	 * dans le timespinnerdatefin
	 * 
	 * @throws Exception
	 */
	private void recalcule_horodate_si_armement() {
		// Si le r�armememnt est utilis�
		// ce dernier n'est pas stock� avec l'op�ration
		// On va chercher la derni�re date de fonctionnement du dc
		// si c'est le m�me jour que la date de fin, on utilise pour mettre �
		// jour
		// le combo.
		if (affichage[10] & affichage[11]) {
			try {
				DC dc = (DC) this.liste_dc.getSelectedItem();
				PeriodeFonctionnement pf = SousListePeriodes.chargeFiltreDerniere(dc);
				Date rearmement_lastope = pf.getDateFin();
				Date finope_lastope = pf.getDateDebut();
				Date dateronde_rearmement = DateUtils.truncate(rearmement_lastope, Calendar.DATE);
				Date dateronde_datefin = DateUtils.truncate(dateFinDerniereOpe, Calendar.DATE);
				// dans ce cas l'heure de d�but est l'heure de r�armement du
				// pi�ge de la
				// derni�re op�ration
				if (dateronde_rearmement.equals(dateronde_datefin)) {
					timeSpinnerheuredebut.setValue(rearmement_lastope);
					timeSpinnerheurefin.setValue(finope_lastope);
				}
			} catch (Exception e) {
				resultat.setText(e.toString());
				logger.log(Level.SEVERE, " recalcule_datefin_avec_armement  ", e);
			}
		}
	}

	/**
	 * chargement des conditions environnementales correspondant � la date de
	 * d�but de l'op�ration lorsque la date est modifi�e ou au moment de
	 * l'import initial
	 */
	private void chargeCondEnvdujour() {
		logger.log(Level.INFO, "chargeCondEnvdujour");
		// cette m�thode doit etre lanc�e apr�s charge Masque
		// Quand les composants ont �t� initialis�s
		// La liste des conditions environnementales du masque a �t� charg�e
		// (les_condenv_ope_en_base)
		// Il faut quand m�me passer en boucle sur les combos, les valeurs des
		// combos
		// ont �t� stock�es dans listofcomponents
		// les valeurs qualitatives ou quantitatives sont stock�es dans
		// vect_estqualitatif
		// r�cup�ration de la date de d�but de l'op�ration en cours dans le como
		String result = null;
		try {
			datedebut = datedebut();
		} catch (Exception e) {
			result = e.toString();
			logger.log(Level.SEVERE, " chargeMasque  ", e);
		}
		// boucle sur les stations de mesure du masque
		// vect_estnull = new Boolean[les_condenvope_en_base.size()];
		for (int i = 0; i < les_condenvope_en_base.size(); i++) {
			logger.log(Level.INFO, "station mesure =" + i);

			CondEnvOpe condenvope = (CondEnvOpe) les_condenvope_en_base.get(i);
			StationMesure stm = condenvope.getStationmesure();
			// ensemble des conditions environnementales correspondant � la
			// station de mesure
			// exemple liste de codes pour phase lunaire ou temperature
			lesConditionsEnvironnementales = new SousListeConditionsEnv(stm);
			// ci dessous ne renvoit que la ligne correspondant � la date et la
			// station de mesure
			// la m�thode chargefiltre(date) aura renvoy� une erreur si ce n'est
			// pas le cas.
			try {
				boolean isMoreThanOneRow = lesConditionsEnvironnementales.chargeFiltre(datedebut);
				if (isMoreThanOneRow) {
					this.resultat.setText(stm.getLibelle()
							+ "Attention plus d'une ligne renvoy�e pour la date, seule la premi�re est affich�e");
				}
			} catch (Exception e) {
				result = e.toString();
				logger.log(Level.SEVERE, " chargeMasque  ", e);
			} finally {
				this.resultat.setText(result);
			}
			if (result == null) {// pas d'erreur
				if (!lesConditionsEnvironnementales.isEmpty()) {
					// vect_estnull[i] = false; // utilis� pour insert ou update
					// apr�es
					ConditionEnvironnementale condenv = (ConditionEnvironnementale) lesConditionsEnvironnementales
							.elementAt(0);
					if (vect_estqualitatif[i]) {
						// Cas ou la station de mesure correspond � un caract�re
						// qualitatif r�cup�ration du combo qui a �t� charg�
						// dans une lsite par ChargeMasque
						JComboBox combo = (JComboBox) listofcomponent.get(i);
						// r�cup�ration de la sous liste qui a �t� pass�e au
						// combo
						// ce vecteur est soit null si quantitatif, soit renvoit
						// les SousListeRefValeurParametre qui a servi a
						// construire le combo
						// il faut selectionner le combo a partir du m�me type
						// d'objet qui a servi � le construire
						SousListeRefValeurParametre lesparmducombo = (SousListeRefValeurParametre) vect_lesvaleursqualitatives[i];
						// Je r�cup�re le vecteur des codes du combo
						Vector<String> lescodesducombo = convertToVector(lesparmducombo);
						// On va chercher la condition environnementale
						// correspondant � la date du jour et on compare son
						// code � la valeur du combo
						int indiceducode = lescodesducombo.indexOf(condenv.getValeurParametreQualitatif().getCode());
						combo.setSelectedIndex(indiceducode);
					} else {
						// Cas ou la station de mesure est un caract�re
						// quantitatif
						JTextFieldDataType textfield = (JTextFieldDataType) listofcomponent.get(i);
						textfield.setText(condenv.getValeurParametreQuantitatif().toString());
					} // test quant/qual
				} else {
					// vect_estnull[i] = true;// end result is null
				}
			} // boucle for
		} // lalistecompl�te est vide

	}

	// charge la liste des stations
	private void chargeListeStations() {
		this.lesStations = new ListeStations();
		try {
			this.lesStations.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1003);
			logger.log(Level.SEVERE, " chargeListeStations  ", e);
		}

	}

	// charge la sous liste des ouvrages
	private void chargeSousListeOuvrages() {
		Station station; // la station selectionnee

		// Recupere la station selectionne dans le combo
		station = (Station) this.liste_stations.getSelectedItem();

		// Vide la liste des station pour garder uniquement la station
		// selectionnee
		this.lesStations = new ListeStations();
		this.lesStations.put(station.getCode(), station);

		// charge la sous liste des ouvrages
		this.lesOuvrages = new SousListeOuvrages(station);

		try {
			this.lesOuvrages.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1004);
			logger.log(Level.SEVERE, " chargeSousListeOuvrages  " + Erreur.B1004, e);
		}

		// Affectation de la sous liste des ouvrages a la station
		station.setLesElementsConstitutifs(this.lesOuvrages);

		// actualiste les combo
		this.liste_ouvrages.setModel(new DefaultComboBoxModel(this.lesOuvrages.toArray()));

		this.liste_ouvrages.setEnabled(true);
		this.label_ouvrage.setForeground(SystemColor.black);
		this.label_station.setForeground(SystemColor.textInactiveText);
		this.liste_df.setEnabled(false);
		this.liste_dc.setEnabled(false);
		this.label_df.setForeground(SystemColor.textInactiveText);
		this.label_dc.setForeground(SystemColor.textInactiveText);
		this.datePaneldatedebut.setForeground(SystemColor.textInactiveText);
		this.label_datefin.setForeground(SystemColor.textInactiveText);
		this.label_datedebut.setForeground(SystemColor.textInactiveText);
		this.label_heuredebut.setForeground(SystemColor.textInactiveText);
		this.label_heurefin.setForeground(SystemColor.textInactiveText);
	}

	// charge la sous liste des DF
	// Lanc� par OK ouvrage ou modif combo box ouvrage
	private void chargeSousListeDF() {
		Ouvrage ouvrage; // l'ouvrage selectionne

		// Recupere l'ouvrage selectionne dans le combo
		ouvrage = (Ouvrage) this.liste_ouvrages.getSelectedItem();

		// Vide la liste des ouvrages pour garder uniquement celui selectionne
		this.lesOuvrages = new SousListeOuvrages();
		this.lesOuvrages.put(ouvrage.getIdentifiant(), ouvrage);

		// charge la sous liste des df
		this.lesDF = new SousListeDF(ouvrage);

		try {
			this.lesDF.chargeSansFiltre();
		} catch (Exception e) {
			logger.log(Level.SEVERE, " chargeSousListeOuvrages  " + Erreur.B1005, e);
			this.resultat.setText(Erreur.B1005);
		}

		// Affectation de la sous liste des df a l'ouvrage
		ouvrage.setLesElementsConstitutifs(this.lesDF);

		// actualiste les combo
		this.liste_df.setModel(new DefaultComboBoxModel(this.lesDF.toArray()));
		this.label_df.setForeground(SystemColor.black);
		this.label_ouvrage.setForeground(SystemColor.textInactiveText);
		this.liste_df.setEnabled(true);
		this.liste_dc.setEnabled(false);
		this.label_dc.setForeground(SystemColor.textInactiveText);
		this.label_datedebut.setForeground(SystemColor.textInactiveText);
		this.label_heuredebut.setForeground(SystemColor.textInactiveText);
		this.label_datefin.setForeground(SystemColor.textInactiveText);
		this.label_heurefin.setForeground(SystemColor.textInactiveText);

		// Essai de renvoi de la valeur dans le combo
		// C'est dans ouvrage que je dois activer l'affichage
		// charge les le d�tail des autres champs du DF
		DF df = (DF) this.liste_df.getSelectedItem();
		// Vide la liste des df pour garder uniquement celui selectionne

		this.leDFselectionne = new SousListeDF();
		this.leDFselectionne.put(df.getIdentifiant(), df);
		// Charge les caract�ristiques du DF, dont celles r�cup�r�es dans la
		// liste abstaite dispositif
		try {
			this.leDFselectionne.chargeObjets();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1005);
			logger.log(Level.SEVERE, " chargeSousListeOuvrages  " + Erreur.B1005, e);
		}
		df = (DF) this.leDFselectionne.elements().nextElement();
		this.liste_df.setToolTipText(df.getCommentaires());
	}

	// charge la sous liste des DC
	private void chargeSousListeDC() {
		// le DF selectionn�
		DF df = (DF) this.liste_df.getSelectedItem();

		// charge la sous liste des dc
		this.lesDC = new SousListeDC(df);

		try {
			this.lesDC.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1006);
			logger.log(Level.SEVERE, " chargeSousListeDC  " + Erreur.B1006, e);
		}

		// Affectation de la sous liste des dc au df
		df.setLesDC(this.lesDC);

		// actualiste le combo
		this.liste_dc.setModel(new DefaultComboBoxModel(this.lesDC.toArray()));
		this.liste_dc.setEnabled(true);
		this.label_df.setForeground(SystemColor.textInactiveText);
		this.label_dc.setForeground(SystemColor.black);
		this.label_datedebut.setForeground(SystemColor.textInactiveText);
		this.label_datefin.setForeground(SystemColor.textInactiveText);
		this.label_heuredebut.setForeground(SystemColor.textInactiveText);
		this.label_heurefin.setForeground(SystemColor.textInactiveText);
	}

	// vide la liste des dc pour ne garder que ceului selectionne
	private void videListeDC() {
		DC dc; // le dc selectionne

		// Recupere le dc selectionne dans le combo
		dc = (DC) this.liste_dc.getSelectedItem();

		// Vide la liste des dc pour garder uniquement celui selectionne
		this.lesDC = new SousListeDC();
		this.lesDC.put(dc.getIdentifiant(), dc);
		//
		this.afficheOpeSuivante();
		// Actualise les combos
		this.label_dc.setForeground(SystemColor.textInactiveText);
		this.label_datedebut.setForeground(SystemColor.black);
		this.label_datefin.setForeground(SystemColor.black);
		this.label_heuredebut.setForeground(SystemColor.black);
		this.label_heurefin.setForeground(SystemColor.black);

	}

	/**
	 * Charge la liste des conditions environnementales rattach�es � la
	 * stationMesure selectionn�e
	 *
	 */

	/**
	 * Conversion d'une SousListeRefValeurParametre en un vecteur de String
	 * (pour le pr�-remplissage des combo)
	 * 
	 * @param la
	 *            liste
	 * @return le vecteur de String avec les libell�s
	 */
	private Vector<String> convertToVector(SousListeRefValeurParametre liste) {
		Vector<String> res = new Vector<String>();

		for (int i = 0; i < liste.size(); i++)
			res.add(((RefValeurParametre) liste.get(i)).getCode());

		return res;
	}

	/**
	 * Ecrit operation
	 * 
	 * @param operation
	 * @param result
	 * @return
	 */
	private StringBuffer ecritope(Operation operation, StringBuffer result)
			throws DataFormatException, SQLException, ClassNotFoundException, Exception {
		operation.verifAttributs();
		boolean b = ListeOperations.exists(operation);
		if (b) {
			operation.majObjet();
		} else {
			operation.insertObjet();
			result = result.append(Message.A1001 + System.lineSeparator());

			// Reinitialisation de la fenetre

		}
		// informe le lanceur de la nouvelle ope

		// this.reInitComponents() ;
		return (result);
	}

	/**
	 * En cas d'erreur efface l'op�ration qui a �t� rentr�e dans la base
	 * 
	 * @param operation
	 * @param result
	 * @return
	 */
	private StringBuffer effaceope(Operation operation, StringBuffer result)
			throws SQLException, ClassNotFoundException {
		// dc n'est pas valide, il faut supprimer l'op�ration et les condenvope
		// suppression de l'op�ration
		this.resultat.setText(result.toString());
		operation.effaceObjet();
		result.append("Suppression de l'op�ration" + System.lineSeparator());
		return (result);
	}

	/**
	 * 
	 * En cas d'erreur efface les conditions environnementales qui ont �t�
	 * rentr�es dans la base
	 * 
	 * @param vectcondenv
	 *            vecteur des conditions environnementales
	 * @param result
	 * @return
	 */
	private StringBuffer ecritcondenv(ConditionEnvironnementale[] vectcondenv, StringBuffer result) throws Exception {
		StationMesure stationMesure = null;
		String methodeObtention = "MESURE";
		Float valeurQuant = null;
		RefValeurParametre valeurQual = null;
		for (int i = 0; i < les_condenvope_en_base.size(); i++) {

			CondEnvOpe condenvope = (CondEnvOpe) les_condenvope_en_base.get(i);
			stationMesure = condenvope.getStationmesure();
			// test de deux cas, soit le param�tre est quantitatif, soit
			// qualitatif.
			if (vect_estqualitatif[i]) {
				JComboBox combo = (JComboBox) listofcomponent.get(i);
				if (combo.isEnabled()) {
					try {
						valeurQual = (RefValeurParametre) combo.getSelectedItem();

					} catch (Exception e) {
						result = result.append(Erreur.S15006 + System.lineSeparator());
						logger.log(Level.SEVERE, " ecritcondenv", e);
						throw new DataFormatException(Erreur.S15006 + System.lineSeparator());

					}
				}
				// dans le deuxi�me cas le param�tre est quantitatif
			} else {
				JTextFieldDataType textfield = (JTextFieldDataType) listofcomponent.get(i);
				try {
					valeurQuant = textfield.getFloat();
				} catch (Exception e) {
					result = result.append(Erreur.S15006 + System.lineSeparator());
					logger.log(Level.SEVERE, " ecritcondenv", e);
					throw new DataFormatException(Erreur.S15006 + System.lineSeparator());

				}
			}
			// Les dates de d�but et de fin correspondent aux horodates de
			// d�but
			// et de fin de l'op�ration.

			ConditionEnvironnementale ce = new ConditionEnvironnementale(datedebut, datefin, methodeObtention,
					stationMesure, valeurQual, valeurQuant);

			if (!(ce.getValeurParametreQuantitatif() == null && ce.getValeurParametreQualitatif() == null)) {
				vectcondenv[i] = ce;
				ce.verifAttributs();
				// pas utilis�
				boolean b = SousListeConditionsEnv.exists(ce);
				if (b) {
					ce.majObjet();
					result = result.append(Message.M1011 + stationMesure.getLibelle() + System.lineSeparator());
					logger.log(Level.INFO, result.toString());

				} else {
					ce.insertObjet();
					result = result.append(Message.A1011 + stationMesure.getLibelle() + System.lineSeparator());
					logger.log(Level.INFO, result.toString());
				}

			} else {// erreur � l'�tape de parse des combos
				logger.log(Level.INFO, "condition envionnementale",
						ce.getStationMesure().getLibelle() + "Condition environnementale, aucune valeur � inserer");

			} // end else
		} // end for

		return (result);
	}

	/**
	 * 
	 * En cas d'erreur efface les conditions environnementales qui ont �t�
	 * rentr�es dans la base
	 * 
	 * @param vectcondenv
	 *            vecteur des conditions environnementales
	 * @param result
	 * @return
	 */
	private StringBuffer effacecondenv(ConditionEnvironnementale[] vectcondenv, StringBuffer result)
			throws SQLException, ClassNotFoundException {
		for (int i = 0; i < vectcondenv.length; i++) {
			ConditionEnvironnementale ce = vectcondenv[i];
			if (!(ce == null)) {
				ce.effaceObjet();
				result.append("Suppression de la condition :" + vectcondenv[i].getStationMesure().getLibelle()
						+ System.lineSeparator());

			} // end if
		}
		return (result);
	}

	/**
	 * 
	 * Ecrit les p�riodes de fonctionnement rentr�es dans la base
	 * 
	 * @param le
	 *            vecteur des
	 * @param result
	 * @return
	 */
	private StringBuffer ecritperiodefonctionnement(PeriodeFonctionnement periodefonctionnement, StringBuffer result)
			throws Exception {

		periodefonctionnement.verifAttributs();
		boolean b = SousListePeriodes.exists(periodefonctionnement);
		if (b) {
			periodefonctionnement.majObjet();
			result = result.append(Message.M1007 + System.lineSeparator());
		} else {
			periodefonctionnement.insertObjet();
			result = result.append(Message.A1007 + System.lineSeparator());

		}
		return (result);
	}

	/**
	 * 
	 * En cas d'erreur efface les p�riodes de fonctionnement du dc rentr�es dans
	 * la base
	 * 
	 * @param le
	 *            vecteur des
	 * @param result
	 * @return
	 */
	private StringBuffer effaceperiodefonctionnement(StringBuffer result, PeriodeFonctionnement[] vectperiode) {
		for (int i = 0; i < vectperiode.length; i++) {
			try {
				vectperiode[i].effaceObjet();
				result.append("Fonctionnement supprim�" + vectperiode[i].getDispositif().getIdentifiant()
						+ System.lineSeparator());
			} catch (Exception e) {
				result = result.append(e.getMessage() + System.lineSeparator());
				logger.log(Level.SEVERE, "effaceperiodefonctionnement", e);
			}

		}
		return (result);
	}

	/**
	 * Recupere les infos de la derniere operation enregistree pour l'afficher
	 *
	 */
	private void afficheOpeSuivante() {
		logger.log(Level.INFO, "afficheOpeSuivante");
		Integer idDerniereOpe;
		Operation ope;

		// creation d'une liste d'operations et chargement de la derniere
		ListeOperations listeOperations = new ListeOperations();

		try {
			idDerniereOpe = listeOperations.chargeFiltreDerniere((DC) this.lesDC.get(0));

			// Si une operation existe
			if (idDerniereOpe != null) {
				// recuperation de l'operation puis affichage de chaque champ
				ope = (Operation) listeOperations.get(idDerniereOpe);

				// Base GMT offset: +1:00
				// DST starts: at 1:00am in UTC time
				// on the last Sunday in March
				// DST ends: at 1:00am in UTC time
				// on the last Sunday in October
				// Save: 1 hour
				/*
				 * SimpleTimeZone tz = new SimpleTimeZone(3600000,
				 * "Europe/Paris", Calendar.MARCH, -1, Calendar.SUNDAY, 3600000,
				 * SimpleTimeZone.UTC_TIME, Calendar.OCTOBER, -1,
				 * Calendar.SUNDAY, 3600000, SimpleTimeZone.UTC_TIME, 3600000) ;
				 */

				// preparation des dates de la nouvelle operation
				dateFinDerniereOpe = ope.getDateFin();
				Calendar cal = Calendar.getInstance();
				cal.setTime(dateFinDerniereOpe);

				// la nouvelle heure de debut est l'heure de fin de la derniere
				// operation
				// La date

				this.ope_date_debut_picker.getModel().setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
						cal.get(Calendar.DAY_OF_MONTH));
				this.ope_date_debut_picker.getModel().setSelected(true);
				// l'heure
				timeSpinnerheuredebut.setValue(dateFinDerniereOpe);

				// la nouvelle heure de fin est la date de fin de la derniere
				// operation + sa duree

				Date dateFin = new Date(
						dateFinDerniereOpe.getTime() + (dateFinDerniereOpe.getTime() - ope.getDateDebut().getTime()));

				cal.setTime(dateFin);
				// la nouvelle heure de debut est l'heure de fin de la derniere
				// operation
				// La date

				this.ope_date_fin_picker.getModel().setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
						cal.get(Calendar.DAY_OF_MONTH));
				this.ope_date_fin_picker.getModel().setSelected(true);
				timeSpinnerheurefin.setValue(dateFin);

				this.ope_organisme.setText(ope.getOrganisme());
				this.ope_operateur.setText(ope.getOperateur());
			}
			// sinon, rien a faire
		} catch (Exception e) {
			// Affichage de l'erreur
			this.resultat.setText(Erreur.B1007);
			logger.log(Level.SEVERE, " chargeMasque  " + Erreur.B1007, e);
		}
	}

	/**
	 * Recupere les infos de la derniere operation enregistree pour l'afficher
	 * 
	 * @param ope_
	 *
	 */
	private void afficheOpeSuivante(Operation ope) {
		logger.log(Level.INFO, "afficheOpeSuivante a partir d'une op�ration");
		try {
			// preparation des dates de la nouvelle operation
			dateFinDerniereOpe = ope.getDateFin();
			Calendar cal = Calendar.getInstance();
			cal.setTime(dateFinDerniereOpe);

			// la nouvelle heure de debut est l'heure de fin de la derniere
			// operation
			// La date

			this.ope_date_debut_picker.getModel().setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
					cal.get(Calendar.DAY_OF_MONTH));
			this.ope_date_debut_picker.getModel().setSelected(true);
			// l'heure
			timeSpinnerheuredebut.setValue(dateFinDerniereOpe);

			// la nouvelle heure de fin est la date de fin de la derniere
			// operation + sa duree

			Date dateFin = new Date(
					dateFinDerniereOpe.getTime() + (dateFinDerniereOpe.getTime() - ope.getDateDebut().getTime()));

			cal.setTime(dateFin);
			// la nouvelle heure de debut est l'heure de fin de la derniere
			// operation
			// La date

			this.ope_date_fin_picker.getModel().setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
					cal.get(Calendar.DAY_OF_MONTH));
			this.ope_date_fin_picker.getModel().setSelected(true);
			timeSpinnerheurefin.setValue(dateFin);

			this.ope_organisme.setText(ope.getOrganisme());
			this.ope_operateur.setText(ope.getOperateur());

			// sinon, rien a faire
		} catch (Exception e) {
			// Affichage de l'erreur
			this.resultat.setText(Erreur.B1007);
			logger.log(Level.SEVERE, " chargeMasque afficheoperationSuivante(Operation)  " + Erreur.B1007, e);
		}
	}

	/**
	 * calcule la date de d�but
	 * 
	 * @return la date de debut initialis�e � partir de la date et de l'heure du
	 *         combo
	 * @throws ParseException
	 * @throws DataFormatException
	 */
	private Date datedebut() throws ParseException, DataFormatException {
		Date date_debut = null;
		Date heure_debut = null;
		// Format de date attendu a la saisie
		SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy"); //$NON-NLS-1$
		String result = null;

		try {
			String s = this.ope_date_debut_picker.getJFormattedTextField().getText();
			date_debut = simpleDate.parse(s);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " chargeMasque  " + Erreur.B1005, e);
		}

		heure_debut = (Date) this.ope_heure_debut.getSpinner().getValue();

		// combine la date et l'heure dans date_debut

		Calendar caldatedebut = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
		// initialisation de la date � partir du timespinner pour r�cup�rer
		// minutes et heures
		caldatedebut.setTime(heure_debut);
		int hour = caldatedebut.get(Calendar.HOUR_OF_DAY);
		int minute = caldatedebut.get(Calendar.MINUTE);
		// r�initialisation du calendar � partir du champ date, et remise �
		// niveau des heures et minutes
		caldatedebut.setTime(date_debut);
		caldatedebut.set(Calendar.HOUR_OF_DAY, hour);
		caldatedebut.set(Calendar.MINUTE, minute);
		// caldatedebut.roll(Calendar.HOUR_OF_DAY, 1);
		date_debut = caldatedebut.getTime();

		return (date_debut);
	}

	/**
	 * calcule la date de fin
	 * 
	 * @return la date de fin initialis�e � partir de la date et de l'heure du
	 *         combo
	 * @throws ParseException
	 * @throws DataFormatException
	 */
	private Date datefin() throws ParseException, DataFormatException {
		Date date_fin = null;
		Date heure_fin = null;
		// Format de date attendu a la saisie
		SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy"); //$NON-NLS-1$
		String result = null;

		try {
			String s = this.ope_date_fin_picker.getJFormattedTextField().getText();
			date_fin = simpleDate.parse(s);
		} catch (Exception e) {
			// parse exception va �tre pass�e, ici j'en ajoute une
			logger.log(Level.SEVERE, " chargeMasque,Erreur d'�valuation de la date fin  " + Erreur.B1005, e);
			this.resultat.setText("Erreur d'�valuation de la date fin");
		}
		heure_fin = (Date) this.ope_heure_fin.getSpinner().getValue();

		// combine la date et l'heure dans date_fin

		Calendar caldatefin = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
		// initialisation de la date � partir du timespinner pour r�cup�rer
		// minutes et heures
		caldatefin.setTime(heure_fin);
		int hour = caldatefin.get(Calendar.HOUR_OF_DAY);
		int minute = caldatefin.get(Calendar.MINUTE);
		// r�initialisation du calendar � partir du champ date, et remise �
		// niveau des heures et minutes
		caldatefin.setTime(date_fin);
		caldatefin.set(Calendar.HOUR_OF_DAY, hour);
		caldatefin.set(Calendar.MINUTE, minute);
		// caldatefin.roll(Calendar.HOUR_OF_DAY, 1);
		date_fin = caldatefin.getTime();

		return (date_fin);
	}

	/**
	 * calcule la date de fin
	 * 
	 * @return la date de fin initialis�e � partir de la date et de l'heure du
	 *         combo
	 * @throws ParseException
	 * @throws DataFormatException
	 */
	private Date daterearmement() throws ParseException, DataFormatException {
		Date date_rearmement = null;
		Date heure_rearmement = null;
		// Format de date attendu a la saisie
		SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy"); //$NON-NLS-1$
		String result = null;

		try {
			String s = this.ope_date_fin_picker.getJFormattedTextField().getText();
			date_rearmement = simpleDate.parse(s);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " chargeMasque, Erreur d'�valuation de la date rearmement  " + Erreur.B1005, e);
			this.resultat.setText("Erreur d'�valuation de la date fin");
		}
		heure_rearmement = (Date) this.heure_rearmement.getSpinner().getValue();

		// combine la date et l'heure dans date_rearmement

		Calendar caldaterearmement = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
		// initialisation de la date � partir du timespinner pour r�cup�rer
		// minutes et heures
		caldaterearmement.setTime(heure_rearmement);
		int hour = caldaterearmement.get(Calendar.HOUR_OF_DAY);
		int minute = caldaterearmement.get(Calendar.MINUTE);
		// r�initialisation du calendar � partir du champ date, et remise �
		// niveau des heures et minutes
		caldaterearmement.setTime(date_rearmement);
		caldaterearmement.set(Calendar.HOUR_OF_DAY, hour);
		caldaterearmement.set(Calendar.MINUTE, minute);
		// caldaterearmement.roll(Calendar.HOUR_OF_DAY, 1);
		date_rearmement = caldaterearmement.getTime();

		return (date_rearmement);
	}

	private void valide() {
		this.resultat.setText("");

		// Resultat affiche a l'utilisateur
		StringBuffer result = new StringBuffer();

		// Objet a creer
		Operation operation;

		// Attributs de l'objet a creer
		Integer identifiant = null;
		DC dc = null;
		DF df = null;
		String organisme = null;
		String operateur = null;
		String commentaires = null;
		SousListeLots lesLots = null;
		boolean opevalide = true;
		boolean condenvvalide = true;
		boolean dfvalide = true;
		boolean dcvalide = true;
		ConditionEnvironnementale[] vectcondenv;
		PeriodeFonctionnement pf;
		PeriodeFonctionnement[] periodefonctionnementdc = new PeriodeFonctionnement[2];
		PeriodeFonctionnement[] periodefonctionnementdf = new PeriodeFonctionnement[2];

		// Initialisation des attributs d'apres les saisies de l'utilisateur et
		// verification que les valeurs sont bien du type attendu

		// identifiant : autoincremente
		try {
			dc = (DC) this.liste_dc.getSelectedItem(); // TODO verifier ici
			if (dc == null) { // normalement il y a un et un seul dc
				throw new Exception();
			}
		} catch (Exception e) {
			result = result.append(Erreur.S0100 + this.label_dc.getText());
			logger.log(Level.SEVERE, " valide", e);

		}

		try {
			df = (DF) this.liste_df.getSelectedItem(); // TODO verifier ici
			if (df == null) { // normalement il y a un et un seul dc
				throw new Exception("Pas de df selectionn�");
			}
		} catch (Exception e) {
			result = result.append(Erreur.S0100 + this.label_dc.getText());
			logger.log(Level.SEVERE, " valide", e);

		}

		try {
			datedebut = datedebut();
		} catch (Exception e) {
			result = result.append(e.toString());
			logger.log(Level.SEVERE, " valide", e);
		}

		try {
			datefin = datefin();
		} catch (Exception e) {
			result = result.append(e.toString());
			logger.log(Level.SEVERE, " valide", e);
		}

		if (datefin.getTime() < datedebut.getTime()) {
			try {
				throw new Exception("La fin de l'op�ration doit �tre sup�rieure au d�but");
			} catch (Exception e) {
				result = result.append(e.getMessage());
				logger.log(Level.SEVERE, " valide", e);
			}
		}
		if (affichage[11]) {
			try {
				daterearmement = daterearmement();
			} catch (ParseException e1) {
				logger.log(Level.SEVERE, "valide et insert r�armement DF", e1);
				result = result.append(e1.toString());
			} catch (DataFormatException e1) {
				logger.log(Level.SEVERE, "valide et insert r�armement DF", e1);
				result = result.append(e1.toString());
			} // throws exception
			if (daterearmement.getTime() < datefin.getTime()) {
				try {
					throw new Exception("L'heure de r�armement doit �tre sup�rieure � l'heure de fin");
				} catch (Exception e) {
					result = result.append(e.getMessage());
					logger.log(Level.SEVERE, " valide", e);
				}
			}
		}

		try {
			organisme = this.ope_organisme.getString();
		} catch (Exception e) {
			result = result.append(Erreur.S0100 + this.label_organisme.getText());
			logger.log(Level.SEVERE, " valide", e);
		}

		try {
			organisme = this.ope_organisme.getString();
		} catch (Exception e) {
			result = result.append(Erreur.S0100 + this.label_organisme.getText());
			logger.log(Level.SEVERE, " valide", e);
		}

		try {
			operateur = this.ope_operateur.getString();
		} catch (Exception e) {
			result = result.append(Erreur.S0100 + this.label_operateur.getText());
			logger.log(Level.SEVERE, " valide", e);
		}

		try {
			commentaires = this.ope_commentaires.getString();
		} catch (Exception e) {
			result = result.append(Erreur.S0100 + this.label_commentaires.getText());
			logger.log(Level.SEVERE, " valide", e);
		}

		// =================================================
		// (1) Ecriture de l'op�ration
		// =================================================
		// Si aucune erreur jusque la, les champs sont soit null soit du bon
		// type, mais les contraintes de domaine ne sont pas forcement verifiees
		if (result.length() == 0) {

			// Creation de l'objet et verification de sa validite
			operation = new Operation(dc, identifiant, datedebut, datefin, organisme, operateur, commentaires, lesLots);
			// Ajout d'une liste vide de lots
			operation.setLesLots(new SousListeLots(operation));
			try {
				result = ecritope(operation, result);
				opevalide = true; // reussite
				// Activation des boutons pour passer a d'autres ecrans
				this.a_lot.setEnabled(true);
				this.inc_operation.setEnabled(true);
				this.resultat.setText(result.toString());
				Lanceur.setOperationCourante(operation);
			} catch (Exception e) {
				result = result.append(e.toString()); // erreur
				logger.log(Level.SEVERE, " valide ", e);
				opevalide = false;
			}

		} else {
			// pb de formatage des champs dans l'�tape d'avant
			// Affichage du resultat sans reinitialisation
			opevalide = false;
		}

		// ========================================================
		// R�cup�ration des conditions environnementales
		// ====================================================
		// lancement seulement si l'import de l'op�ration a fonctionn�
		if (opevalide) {
			// boucle sur les conditions environnementales
			vectcondenv = new ConditionEnvironnementale[les_condenvope_en_base.size()];
			try {
				result = ecritcondenv(vectcondenv, result);
				condenvvalide = true;
				this.resultat.setText(result.toString());
			} catch (Exception e) {
				result = result.append(e.getMessage() + System.lineSeparator());
				logger.log(Level.SEVERE, "valide", e);
				condenvvalide = false;

			}
			// =================================================
			// Ecriture du fonctionnement DC
			// =================================================
			if (condenvvalide) {
				if (affichage[11]) { // l'operation renseigne le dc
					try {
						pf = new PeriodeFonctionnement(dc, datefin, daterearmement, null, false,
								new RefTypeFonctionnement("2"));
						result = ecritperiodefonctionnement(pf, result);
						periodefonctionnementdc[0] = pf;
						dcvalide = true;
						this.resultat.setText(result.toString());
					} catch (Exception e) {
						logger.log(Level.SEVERE, "valide et insert r�armement", e);
						result = result.append(e.getMessage() + "dc = " + dc.getCode() + System.lineSeparator());
						dcvalide = false;
					}
					if (dcvalide) {
						try {
							pf = new PeriodeFonctionnement(dc, datedebut, datefin, null, true,
									new RefTypeFonctionnement("1"));
							result = ecritperiodefonctionnement(pf, result);
							periodefonctionnementdc[1] = pf;
							dcvalide = true;
						} catch (Exception e) {
							logger.log(Level.SEVERE, "valide et insert r�armement DC ", e);
							result = result.append(e.getMessage() + System.lineSeparator());
							dcvalide = false;
							try {
								// effacement de la premi�re periode
								periodefonctionnementdc[0].effaceObjet();
							} catch (Exception e1) {
								result = result.append(e1.getMessage() + System.lineSeparator());
							}

						}
					} // dcvalide2
					this.resultat.setText(result.toString());
				} // affichage[11]

				// periode de fonctionnnement durant l'op�ration

				/*
				 * Ecriture de fonctionnementDC pour la p�riode correspondant au
				 * fonctionnement normal durant l'op�ration
				 */

				// =================================================
				// Ecriture du fonctionnement DF
				// =================================================
				if (dcvalide) {
					if (affichage[12] & affichage[11]) { // is_r�armement et
															// is_r�armementDF
						pf = new PeriodeFonctionnement(df, datefin, daterearmement, null, false,
								new RefTypeFonctionnement("2"));
						try {
							result = ecritperiodefonctionnement(pf, result);
							dfvalide = true;
							periodefonctionnementdf[0] = pf;
						} catch (Exception e) {
							logger.log(Level.SEVERE, "valide et insert r�armement DF", e);
							result = result.append(e.getMessage() + System.lineSeparator());
							dfvalide = false;
						}
						if (dfvalide) {
							try {
								pf = new PeriodeFonctionnement(df, datedebut, datefin, null, true,
										new RefTypeFonctionnement("1"));
								periodefonctionnementdf[1] = pf;
								try {
									result = ecritperiodefonctionnement(pf, result);
									dfvalide = true;
								} catch (Exception e) {
									logger.log(Level.SEVERE, "valide et insert r�armement DF", e);
									result = result.append(e.getMessage());
									dfvalide = false;
									try {
										// effacement de la premi�re periode
										periodefonctionnementdf[0].effaceObjet();
									} catch (Exception e1) {
										result = result.append(e1.getMessage() + System.lineSeparator());
									}
								}

							} catch (Exception e) {
								logger.log(Level.SEVERE, "valide", e);
								result = result.append(e.getMessage());
							}
						} // dfvalide
						this.resultat.setText(result.toString());
					} // affichage[12]&affichage[11]
						//////////////////////////////////////////
						// CORRECTION DES ERREURS
						//////////////////////////////////////////
					if (!dfvalide) {
						operation = Lanceur.getOperationCourante();
						try {
							// les periodes fonctionnement df sont d�j�
							// supprim�es si on passe par ici
							result = effaceope(operation, result);
							result = effacecondenv(vectcondenv, result);
							periodefonctionnementdc[0].effaceObjet();
							periodefonctionnementdc[1].effaceObjet();
						} catch (Exception e) {
							result = result.append(e.getMessage());
							logger.log(Level.SEVERE, "efface", e);
						} finally {
							this.resultat.setText(result.toString());
						}
					}
				} else {// !dcvalide
					// suppression de l'op�ration
					operation = Lanceur.getOperationCourante();
					try {
						result = effaceope(operation, result);
						result = effacecondenv(vectcondenv, result);
					} catch (Exception e) {
						result = result.append(e.getMessage());
						logger.log(Level.SEVERE, "efface", e);
					} finally {
						this.resultat.setText(result.toString());
					}
				}

			} else { // !condenvvalide
				// la condenv n'est pas valide, il faut supprimer
				// l'op�ration
				operation = Lanceur.getOperationCourante();
				try {
					result = effaceope(operation, result);
				} catch (Exception e) {
					result = result.append(e.getMessage());
					logger.log(Level.SEVERE, "efface", e);
				} finally {
					this.resultat.setText(result.toString());
				}

			}
		} else

		{// !opevalide
			// il y a eu une erreur dans l'op�ration, on affiche le texte dans
			// le string resultat
			this.resultat.setText(result.toString());
		} // end else opevalide
		if (opevalide && condenvvalide && dfvalide && dcvalide) {
			operation = Lanceur.getOperationCourante();
			result = result.append("Ecriture compl�te");
			this.afficheOpeSuivante();
			this.chargeCondEnvdujour();
			this.recalcule_horodate_si_armement();
			this.resultat.setText(result.toString());
		}

	}
}
