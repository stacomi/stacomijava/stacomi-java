/*
 **********************************************************************
 *
 * Nom fichier :        Caracteristique.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */


package migration;

import commun.* ;
import migration.referenciel.RefParametre;
import migration.referenciel.RefValeurParametre;

import java.sql.* ;
import java.util.zip.DataFormatException ;
import java.util.ArrayList;
//import java.util.Vector ;

/**
 * Caracteristique de lot
 * @author Samuel Gaudey
 */
public class Caracteristique implements IBaseDonnees {

    
  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////
    

    private Lot                 lot                         ; 
    private RefParametre        parametre                   ; 
    private String              methodeObtention            ; 
    private Float               valeurParametreQuantitatif  ; 
    private RefValeurParametre  valeurParametreQualitatif   ; 
    private Float               precision                   ; 
    private String              commentaires                ; 


    
  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    
    
/**
 * Construit une caracteristique avec un lot, un parametre, une valeur qualitative ou une valeur quantitative 
 * @param _lot
 * @param _parametre
 * @param _valeurParametreQuantitatif
 * @param _valeurParametreQualitatif
 */    
    public Caracteristique(Lot _lot, RefParametre _parametre, Float _valeurParametreQuantitatif, RefValeurParametre _valeurParametreQualitatif) {   
        this(_lot, _parametre, null, _valeurParametreQuantitatif, _valeurParametreQualitatif, null, null) ;
    } // end Marque
    
/**
 * Construit une caracteristique avec tous ses attributs
 * @param _lot
 * @param _parametre
 * @param _methodeObtention
 * @param _valeurParametreQuantitatif
 * @param  _valeurParametreQualitatif    
 * @param _precision
 * @param _commentaires
 */  
    public Caracteristique(Lot _lot, RefParametre _parametre, String _methodeObtention, Float _valeurParametreQuantitatif, RefValeurParametre _valeurParametreQualitatif, Float _precision, String _commentaires) {   

        this.setLot(_lot) ;
        this.setParametre(_parametre) ;
        this.setMethodeObtention(_methodeObtention) ;
        this.setValeurParametreQualitatif(_valeurParametreQualitatif) ;
        this.setValeurParametreQuantitatif(_valeurParametreQuantitatif) ;
        this.setPrecision(_precision) ;
        this.setCommentaires(_commentaires) ; 
        
    } // end Marque   
    
    
    
  ///////////////////////////////////////
  // interfaces
  ///////////////////////////////////////
    
    
    public void insertObjet() throws SQLException, ClassNotFoundException {
 
        String table        = Caracteristique.getNomTable()     ;
        ArrayList<String> nomAttributs = Caracteristique.getNomAttributs() ;
        @SuppressWarnings("rawtypes")
		ArrayList valAttributs = this.getValeurAttributs() ; 
        
        System.out.println("Caracteristique : insertObjet()") ;  
        
        ConnexionBD.getInstance().executeInsert(table, nomAttributs, valAttributs) ; 
    }
    
    
    public void majObjet() throws SQLException, ClassNotFoundException {
        
        String   table                   = Caracteristique.getNomTable();
        String[] nomAttributsSelection   = Caracteristique.getNomID();
        Object[] valAttributsSelection   = this.getValeurID();
        ArrayList<String>   nomAttributs = Caracteristique.getNomAttributs();
        @SuppressWarnings("rawtypes")
		ArrayList   valAttributs = this.getValeurAttributs(); 
 
        System.out.println("Caracteristique : majObjet()") ;       
        
        ConnexionBD.getInstance().executeUpdate(table, nomAttributsSelection, valAttributsSelection, nomAttributs, valAttributs) ; 
    }
    
    
    public void effaceObjet() throws SQLException, ClassNotFoundException {
        
        String   table                   = Caracteristique.getNomTable()     ;
        String[] nomAttributsSelection   = Caracteristique.getNomID()        ;
        Object[] valAttributsSelection   = this.getValeurID()    ;
 
        System.out.println("Caracteristique : effaceObjet()") ;       
        
        ConnexionBD.getInstance().executeDelete(table, nomAttributsSelection, valAttributsSelection) ; 
    }

    public boolean verifAttributs() throws DataFormatException {
    
        if (!Verification.isLot(this.lot, true))
            throw new DataFormatException (Erreur.S5000) ;

        if (!Verification.isRef(this.parametre, true))
            throw new DataFormatException (Erreur.S5001) ;

        if (!Verification.isMethodeObtentionCaracteristique(this.methodeObtention, false))
            throw new DataFormatException (Erreur.S5002) ;
        
        if (!Verification.isValeurParametre(this.valeurParametreQuantitatif, this.valeurParametreQualitatif, true))
            throw new DataFormatException (Erreur.S5003) ;

        if (!Verification.isFloat(this.precision, false))
            throw new DataFormatException (Erreur.S5004) ;

        if (!Verification.isText(this.commentaires, false))
            throw new DataFormatException (Erreur.S5005) ;
        
        return true ;
    }    
    
    
    
    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////



/**
 * Retourne le nom de la table correspondante a la classe
 * @return un le nom
 */
    public static String getNomTable() {  
        return "tj_caracteristiquelot_car" ;
    }
    
    
/**
 * Retourne le nom des attributs identifiants de la table correspondant a cette classe
 * @return un Vector de String avec les attributs dans l'ordre de la table dans la BD
 */
    public static String[] getNomID() {  
        
        String[] ret = new String[2] ;
        
        ret[0] = "car_lot_identifiant" ;
        ret[1] = "car_par_code" ;
        
        return ret ;
    } // end getNomID        
    
/**
 * Retourne la valeur des attributs identifiants de cette classe
 * @return un Vector d'Object avec les attributs dans l'ordre de la table dans la BD
 */
    public Object[] getValeurID() {  
        
        Object[] ret = new Object[2] ;
        
        ret[0] = this.lot.getIdentifiant() ;
        ret[1] = this.parametre.getCode() ;
        
        return ret ;
    } // end getValeurID  
    
    
/**
 * Retourne le nom des attributs de la table correspondant a cette classe
 * @return un Vector de String avec les attributs dans l'ordre de la table dans la BD
 */
    public static ArrayList<String> getNomAttributs() {  
        // impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau est inconnu
    
    	ArrayList<String> ret = new ArrayList<String>() ;

        ret.add("car_lot_identifiant") ;    // autoincremente
        ret.add("car_par_code") ;
        ret.add("car_methode_obtention") ;
        ret.add("car_val_identifiant") ;
        ret.add("car_valeur_quantitatif ") ;
        ret.add("car_precision") ;
        ret.add("car_commentaires") ;
        ret.add("car_org_code");
        
        return ret ;
    } // end getNomAttributs        
    
    
/**
 * Retourne la valeur des attributs de cette classe
 * @return un Vector d'Object avec les attributs dans l'ordre de la table dans la BD
 */
    public ArrayList<Object> getValeurAttributs() {  
         
    	ArrayList<Object> ret = new ArrayList<Object>() ;
        
        if (this.lot != null) { ret.add(this.lot.getIdentifiant()) ; }
        else { ret.add(null) ; }
        if (this.parametre != null) { ret.add(this.parametre.getCode()) ;  }
        else { ret.add(null) ; }
        ret.add(this.methodeObtention) ;   
        if (this.valeurParametreQualitatif != null) { ret.add(this.valeurParametreQualitatif.getCode()) ;  }
        else { ret.add(null) ; }      
        ret.add(this.valeurParametreQuantitatif) ; 
        ret.add(this.precision) ; 
        ret.add(this.commentaires) ;  
        
        return ret ;
    } // end getValeurAttributs   
    
    
    
    
    
    
    
    

    /**
     * Retourne le lot sur lequel est obtenu la caracteristique
     * @return le lot
     */
        public Lot getLot() {        
            return this.lot;
        } // end getLot        

    /**
     * Retourne le parametre sur lequel porte la caracteristique
     * @return le parametre 
     */
        public RefParametre getParametre() {        
            return this.parametre;
        } // end getParametre        

    /**
     * Retourne la methode d'obtention de la caracteristique
     * @return la methode d'obtention (MESURE | CALCULE)
     */
        public String getMethodeObtention() {        
            return this.methodeObtention;
        } // end getMethodeObtention        

    /**
     * Retourne la valeur du parametre qualitatif
     * @return la valeur du parametre ou null si le parametre est quantitatif
     */
        public RefValeurParametre getValeurParametreQualitatif() {        
            return this.valeurParametreQualitatif ;
        } // end getValeurParametreQualitatif   

    /**
     * Retourne la valeur du parametre quantitatif
     * @return la valeur du parametre ou null si le parametre est qualitatif
     */
        public Float getValeurParametreQuantitatif() {        
            return this.valeurParametreQuantitatif;
        } // end getValeurParametreQuantitatif       

    /**
     * Retourne la precision pour l'obtention de la valeur. Par exemple, une precision de '1' pour un parametre taille (dont l'unite est le gramme) signifie "a 1 gramme pres"
     * @return la precision
     */
        public Float getPrecision() {        
            return this.precision;
        } // end getPrecision     

    /**
     * Retourne les commentaires sur la caracteristique
     * @return les commentaires
     */
        public String getCommentaires() {        
            return this.commentaires;
        } // end getCommentaires        




     /**
     * Initialise le lot sur lequel est obtenu la caracteristique
     * @param _lot le lot
     */
        public void setLot(Lot _lot) {        
            this.lot = _lot;
        } // end setLot         

     /**
     * Initialise le parametre sur lequel porte la caracteristique
     * @param _parametre le parametre 
     */
        public void setParametre(RefParametre _parametre) {        
            this.parametre = _parametre;
        } // end setParametre           

     /**
     * Initialise la methode d'obtention de la caracteristique
     * @param _methodeObtention la methode d'obtention (MESURE | CALCULE)
     */
        public void setMethodeObtention(String _methodeObtention) {        
            this.methodeObtention = _methodeObtention;
        } // end setMethodeObtention        

    /**
     * Initialise la valeur du parametre qualitatif
     * @param _valeurParametreQualitatif la valeur du parametre ou null si le parametre est quantitatif
     */
        public void setValeurParametreQualitatif(RefValeurParametre _valeurParametreQualitatif) {        
            this.valeurParametreQualitatif = _valeurParametreQualitatif;
        } // end setValeurParametreQualitatif            

    /**
     * Initialise la valeur du parametre quantitatif
     * @param _valeurParametreQuantitatif la valeur du parametre ou null si le parametre est qualitatif
     */
        public void setValeurParametreQuantitatif(Float _valeurParametreQuantitatif) {        
            this.valeurParametreQuantitatif = _valeurParametreQuantitatif;
        } // end setValeurParametreQuantitatif        

    /**
     * Initialise la precision pour l'obtention de la valeur
     * @param _precision la precision
     */
        public void setPrecision(Float _precision) {        
            this.precision = _precision;
        } // end setPrecision      

    /**
     * Initialise les commentaires sur la caracteristique
     * @param _commentaires les commentaires
     */
        public void setCommentaires(String _commentaires) {        
            this.commentaires = _commentaires;
        } // end setCommentaires        


        
     /**
     * Retourne les attributs de la caracteristique de lot sous forme textuelle
     * @return ret les attributs de la caracteristique de lots sous forme textuelle
     */
        public String toString() {        
            String ret ;
            String complementUnite = "" ;
            String complementPrecision = "" ;
            String valeur = "" ;
            
            if ((this.parametre.getUnite() != null) && (this.parametre.getUnite().length() > 0)) {
                complementUnite = " (" + this.parametre.getUnite() + ")" ;
            }
            
            if (this.precision != null) {
                complementPrecision = " +/- " + this.precision.toString() ;
            }
            
            // valeur soit qualitative, soit quantitative
            if (this.valeurParametreQualitatif  != null) {
                valeur = this.valeurParametreQualitatif.getLibelle() ;
            }
            else if (this.valeurParametreQuantitatif  != null){
                valeur = this.valeurParametreQuantitatif.toString() ;
            }
        
            ret = this.parametre.getLibelle() + " : " + valeur + complementUnite + complementPrecision ;
        
            return ret ;
        }
     

 } // end Caracteristique



