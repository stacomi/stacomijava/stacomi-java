/*
 **********************************************************************
 *
 * Nom fichier :        Marquage.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */
package migration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;

/**
 * Action de marquage effectuee sur un lot
 * 
 * @author S�bastien Laigre
 */
public class Marquage implements IBaseDonnees {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	private Lot lot;

	private Marque marque;

	private String action;

	private String commentaires;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit un marquage constatee avec uniquement un lot, une marque et une
	 * action
	 * 
	 * @param _lot
	 *            le lot sur lequel est constatee la pathologie
	 * @param _marque
	 *            la marque concernee par le marquage
	 * @param _action
	 *            l'action de marquage
	 */
	public Marquage(Lot _lot, Marque _marque, String _action) {
		this(_lot, _marque, _action, null);
	} // end Marquage

	/**
	 * Construit un marquage avec tous ses attributs
	 * 
	 * @param _lot
	 *            le lot sur lequel est constatee la pathologie
	 * @param _marque
	 *            la marque concernee par le marquage
	 * @param _action
	 *            l'action de marquage
	 * @param _commentaires
	 *            le commentaire sur le marquage
	 */
	public Marquage(Lot _lot, Marque _marque, String _action, String _commentaires) {

		this.setLot(_lot);
		this.setMarque(_marque);
		this.setAction(_action);
		this.setCommentaires(_commentaires);

	} // end Marquage

	///////////////////////////////////////
	// interfaces
	///////////////////////////////////////

	/**
	 * Insertion d'un marquage dans la BDD
	 */
	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = Marquage.getNomTable();
		ArrayList<String> nomAttributs = Marquage.getNomAttributs();
		ArrayList<Object> valeurAttributs = this.getValeurAttributs();

		ConnexionBD.getInstance().executeInsert(table, nomAttributs, valeurAttributs);
	}

	public void majObjet() throws SQLException, ClassNotFoundException {
	}

	/**
	 * Mise � jour d'un marquage dans la BDD
	 * 
	 * @param old
	 *            l'ancienne marque
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void majObjet(Marquage old) throws SQLException, ClassNotFoundException {
		String table = Marquage.getNomTable();
		ArrayList<String> nomAttributs = Marquage.getNomAttributs();
		ArrayList<Object> valeurAttributs = this.getValeurAttributs();

		String[] nomID = Marquage.getNomID();
		Object[] valeurID = old.getValeurID();

		ConnexionBD.getInstance().executeUpdate(table, nomID, valeurID, nomAttributs, valeurAttributs);
	}

	/**
	 * Suppression d'un marquage dans la BDD
	 */
	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = Marquage.getNomTable();
		String[] nomID = Marquage.getNomID();
		Object[] valeurID = this.getValeurID();

		ConnexionBD.getInstance().executeDelete(table, nomID, valeurID);
	}

	/**
	 * V�rification des attributs d'un marquage
	 */
	public boolean verifAttributs() throws DataFormatException {
		// v�rification de l'identifiant du lot
		if (!Verification.isInteger(this.lot.getIdentifiant(), true))
			throw new DataFormatException(Erreur.S17000);

		// v�rification de la r�f�rence de la marque
		if (!Verification.isText(this.marque.getReference(), 1, 30, true))
			throw new DataFormatException(Erreur.S17001);

		// v�rification de la r�f�rence de la marque
		if (!Verification.isActionMarquage(this.action, true))
			throw new DataFormatException(Erreur.S17002);

		// v�rification de la r�f�rence de la marque
		if (!Verification.isText(this.commentaires, 0, false))
			throw new DataFormatException(Erreur.S17003);

		return true;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Retourne le lot concerne par le marquage
	 * 
	 * @return le lot
	 */
	public Lot getLot() {
		return this.lot;
	} // end getLot

	/**
	 * Retourne la marque concernee par le marquage
	 * 
	 * @return la marque
	 */
	public Marque getMarque() {
		return this.marque;
	} // end getMarque

	/**
	 * Retourne l'action de marquage
	 * 
	 * @return l'action (LECTURE | POSE | RETRAIT)
	 */
	public String getAction() {
		return this.action;
	} // end getAction

	/**
	 * Retourne les commentaires sur le marquage
	 * 
	 * @return les commentaires
	 */
	public String getCommentaires() {
		return this.commentaires;
	} // end getCommentaires

	/**
	 * Initialise le lot concerne par le marquage
	 * 
	 * @param _lot
	 *            le lot
	 */
	public void setLot(Lot _lot) {
		this.lot = _lot;
	} // end setLot

	/**
	 * Initialise la marque concernee par le marquage
	 * 
	 * @param _marque
	 *            la marque
	 */
	public void setMarque(Marque _marque) {
		this.marque = _marque;
	} // end setMarque

	/**
	 * Initialise l'action de marquage
	 * 
	 * @param _action
	 *            l'action (LECTURE | POSE | RETRAIT)
	 */
	public void setAction(String _action) {
		this.action = _action;
	} // end setAction

	/**
	 * Initialise les commentaires sur le marquage
	 * 
	 * @param _commentaires
	 *            les commentaires
	 */
	public void setCommentaires(String _commentaires) {
		this.commentaires = _commentaires;
	} // end setCommentaires

	/**
	 * Retourne le nom de la table correspondante
	 * 
	 * @return le nom de la table correspondante
	 */
	public static String getNomTable() {
		return "tj_actionmarquage_act";
	}

	/**
	 * Retourne le nom de la cl� primaire
	 * 
	 * @return le nom de la cl� primaire
	 */
	public static String[] getNomID() {
		String[] res = new String[2];
		res[0] = "act_lot_identifiant";
		res[1] = "act_mqe_reference";
		return res;
	}

	/**
	 * Retourne la valeur de la cl� primaire
	 * 
	 * @return la valeur de la cl� primaire
	 */
	public Object[] getValeurID() {
		Object[] res = new Object[2];
		res[0] = this.getLot().getIdentifiant();
		res[1] = this.getMarque().getReference();
		return res;
	} // end getValeurID

	/**
	 * Acc�s aux noms des colonnes
	 * 
	 * @return les noms des colonnes
	 */
	public static ArrayList<String> getNomAttributs() {
		ArrayList<String> res = new ArrayList<String>();
		res.add("act_lot_identifiant");
		res.add("act_mqe_reference");
		res.add("act_action");
		res.add("act_commentaires");
		res.add("act_org_code");
		return res;
	}

	/**
	 * Acc�s aux valeurs des attributs
	 * 
	 * @return les valeurs des attributs
	 */
	public ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> res = new ArrayList<Object>();
		res.add(this.getLot().getIdentifiant());
		res.add(this.getMarque().getReference());
		res.add(this.getAction());
		res.add(this.getCommentaires());
		return res;
	}

	/**
	 * Affichage d'un Marquage
	 */
	public String toString() {
		String res = "";

		String sep = "  ";
		// res += this.lot.getIdentifiant() + sep;
		res += this.action + sep;
		res += this.marque.getReference() + sep;
		res += this.marque.getLocalisation().getLibelle() + sep;
		res += this.marque.getNature().getLibelle() + sep;

		return res;
	}

} // end Marquage
