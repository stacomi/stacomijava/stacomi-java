/**
 * 
 */
package migration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;

/**
 * Classe servant � �crire / modifier acceder la table 
 * syteme des coefficients de conversion taille en 
 * pixel / taille du logiciel WPOIS
 * @author C�dric Briand
 */
public class TailleVideo implements IBaseDonnees {

	// /////////////////////////////////////
	// attributes
	// /////////////////////////////////////
	//  tav_distance character varying(1)NOT NULL, I P ou L
	//  tav_coefficientconversion numeric NOT NULL, 
	private String distance;
	private Float coefconversion;
	private Integer codeDC;


	// /////////////////////////////////////
	// constructors
	// /////////////////////////////////////
	/**
	 * Construit une classe tous les attributs, la distance et le coefficient de conversion
	 * @param _DCcode code du DC
	 * @param _distance distance
	 * @param _coefconversion coefficient de conversion
	 */
	public TailleVideo(Integer _DCcode, String _distance, Float _coefconversion) {
		this.setDistance(_distance);
		this.setCoefConversion(_coefconversion);
		this.setCodeDC(_DCcode);
	}

	// /////////////////////////////////////
	// interfaces
	// /////////////////////////////////////

	/* (non-Javadoc)
	 * insertion des objets de la table 
	 * @see commun.IBaseDonnees#insertObjet()
	 */
	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		ArrayList nomAttributs = this.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		System.out.println("TailleVideo : insertObjet()");
		ConnexionBD.getInstance().executeInsert(table, nomAttributs,
				valAttributs);
	}
	  
	/**
	 * Inutilisable car la cl� primaire est constitu�e du doublon (tav_dic_identifiant, tav_distance)
	 * donc on a besoin de l'ancienne valeur pour pouvoir identifier 1 ligne de la table...
	 */
	public void majObjet() throws SQLException, ClassNotFoundException {
		// � faire => pour modifier sur chaque station de contr�le les coeff de conversion taille pixel!
		// certains codes sont en effet carr�ment locaux
	}
	
	/**
	 * Mise a jour d'un taille video
	 * @param old : l'ancienne taille vid�o
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void majObjet(TailleVideo old) throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		String[] nomID = TailleVideo.getNomID();
		Object[] valueID = this.getValeurID();
		ArrayList<String> nomsAttributs = this.getNomAttributs();
		ArrayList<Object> valeursAttributs  =this.getValeurAttributs();

		ConnexionBD.getInstance().executeUpdate(table, nomID, valueID, nomsAttributs, valeursAttributs);
	}

	/**
	 * Suppression d'un objet de la table
	 */
	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		String[] nomID = TailleVideo.getNomID();
		Object[] valueID = this.getValeurID();
		ConnexionBD.getInstance().executeDelete(table, nomID, valueID);
	}
	
	/**
	 * Acc�s aux valeurs des attributs qui forment la cl� primaire
	 * @return un tableau avec les valeurs des attributs qui forment la cl� primaire
	 */
	private Object[] getValeurID() {
		Object[] res = new Object[2];
		res[0] = this.codeDC;
		res[1] = this.distance;
		return res;
	}

	/**
	 * Acc�s aux noms des attributs qui forment la cl� primaire
	 * @return un tableau avec les noms des attributs qui forment la cl� primaire
	 */
	private static String[] getNomID() {
		String[] res = new String[2];
		res[0] = "tav_dic_identifiant";
		res[1] = "tav_distance";
		return res;
	}


	/* verifie les attributs des champs
	 * @see commun.IBaseDonnees#verifAttributs()
	 * 
	 */
	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;
		if (!Verification.isInteger(this.codeDC, true)) {
			throw new DataFormatException (Erreur.S1008) ;
		}
		if (!Verification.isText(this.distance,1, 3, true)) {
		    throw new DataFormatException (Erreur.S1009) ;
		    }               
		if (!Verification.isFloat(this.coefconversion, true)) {
		    throw new DataFormatException (Erreur.S1010) ;
		}
        return(ret);
	}
	
    // /////////////////////////////////////
    // operations
    // /////////////////////////////////////
	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return le nom
	 */
	private String getNomTable() {
		return "ts_taillevideo_tav";
	}

	/**
	 * Retourne le nom des champs de la table
	 * @return le noms des champs de la table dans l'ordre d'insertion dans la base de donnee
	 */
	private ArrayList<String> getNomAttributs() {
		ArrayList<String> ret =new ArrayList<String>();
		ret.add( "tav_dic_identifiant");
		ret.add("tav_coefconversion");
		ret.add( "tav_distance");
		ret.add("tav_org_code");
		return ret;
	}
	
	 /**
	 * Retourne la valeur des attributs de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	private ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret =new ArrayList<Object>();
		ret.add(this.codeDC);
		ret.add(this.coefconversion);
		ret.add(this.distance);
		return ret;
	}

	/**
	 * retourne la distance � la vitre de comptage
	 * 
	 * @return la distance � la vitre de comptage
	 */
	public String getDistance() {
		return this.distance;
	}

	/**
	 * retourne le coefficient de conversion pixel taille
	 * 
	 * @return le coefficien de conversion
	 */
	public Float getCoefConversion() {
		return this.coefconversion;
	}
	
	/**
	 * retourne le code du DC
	 * @return le code du DC
	 */
	public Integer getCodeDC() {
		return this.codeDC;
	}


	/**
	 * Initialise la  distance � la vitre
	 * 
	 * @param distance
	 */
	public void setDistance(String distance) {
		this.distance = distance;
	}

	/**
	 * Initialise le coefficient de conversion taille en pixel - taille
	 * 
	 * @param coefconversion
	 */
	public void setCoefConversion(Float coefconversion) {
		this.coefconversion = coefconversion;
	}

	/**
	 * Initialise le code du DC
	 * @param dcCode le code du DC
	 */
	public void setCodeDC(Integer dcCode)
	{
		this.codeDC = dcCode;
	}

}
