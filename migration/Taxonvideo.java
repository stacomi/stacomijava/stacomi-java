/**
 * 
 */
package migration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import migration.referenciel.RefStade;
import migration.referenciel.RefTaxon;
import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;

/**
 * Classe servant � �crire / modifier acceder la table 
 * syteme entre les taxons et les taxons video (en trois 
 * caract�re tels qu'inscrits les fichiers de d�pouillement du 
 * logiciel WPOIS
 * @author Cedric Briand
 */
public class Taxonvideo implements IBaseDonnees {

	// /////////////////////////////////////
	// attributes
	// /////////////////////////////////////
	//  txv_code character varying(3)NOT NULL,
	//  txv_tax_code character varying(6) NOT NULL,
	private String taxonvideo;
	private RefTaxon refTaxon;
	private RefStade refStade;


	// /////////////////////////////////////
	// constructors
	// /////////////////////////////////////
	/**
	 * Construit une classe tous les attributs, le taxonvideo et le taxon
	 * 
	 * @param _taxonvideo (3 lettres)
	 * @param _refTaxon taxon
	 * @param _refStade stade
	 */
	public Taxonvideo(String _taxonvideo, RefTaxon _refTaxon,RefStade _refStade) {
		
		this.settaxonvideo(_taxonvideo);
		this.setrefTaxon(_refTaxon);
		this.setRefStade(_refStade);
	}

	// /////////////////////////////////////
	// interfaces
	// /////////////////////////////////////

	/**
	 * Suppression d'un taxon vid�o dans la BDD
	 * @see commun.IBaseDonnees#majObjet()
	 */
	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		String[] nomID = Taxonvideo.getNomID();
		Object[] valeurID = this.getValeurID();
		ConnexionBD.getInstance().executeDelete(table, nomID, valeurID);
	}

	/**
	 * Insertion des objets de la table 
	 * @see commun.IBaseDonnees#insertObjet()
	 */
	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		ArrayList nomAttributs = this.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		System.out.println("TaxonVideo : insertObjet()");
		ConnexionBD.getInstance().executeInsert(table, nomAttributs, valAttributs);
	}
	  
	/**
	 * Mise a jour d'un taxon vid�o dans la BDD
	 * @see commun.IBaseDonnees#majObjet()
	 */
	public void majObjet() throws SQLException, ClassNotFoundException {
		// � faire => notamment pour modifier la liste des taxons de la table!
		// certains codes sont en effet carr�ment locaux
		
		String table = this.getNomTable();
		String[] nomID = Taxonvideo.getNomID();
		Object[] valeurID = this.getValeurID();
		ArrayList<String> attributeNames = this.getNomAttributs();
		ArrayList<Object> attributeValues = this.getValeurAttributs();
		ConnexionBD.getInstance().executeUpdate(table, nomID, valeurID, attributeNames, attributeValues);
	}

	/**
	 * Acces � la valeur de la cl� primaire de la table
	 * @return la valeur de la cl� primaire de la table
	 */
	private Object[] getValeurID() {
		Object[] res = new Object[1];
		res[0] = this.taxonvideo;
		return res;
	}

	/**
	 * Acces au nom de la cl� primaire de la table
	 * @return le nom de la cl� primaire de la table
	 */
	private static String[] getNomID() {
		String[] res = new String[1];
		res[0] = "txv_code";
		return res;
	}

	/**
	 * V�rifie les attributs des champs
	 * @see commun.IBaseDonnees#verifAttributs()
	 */
	public boolean verifAttributs() throws DataFormatException {
       boolean ret = false;
		if (!Verification.isText(this.taxonvideo,1, 3, true)) {
            throw new DataFormatException (Erreur.V1004) ;
            }               
        if (!Verification.isText(this.refTaxon.getCode().toString(), 1, 6, true)) {
            throw new DataFormatException (Erreur.V1005) ;
        }
        return(ret);
	}
	
    // /////////////////////////////////////
    // operations
    // /////////////////////////////////////
	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return le nom
	 */
	private String getNomTable() {
		return "ts_taxonvideo_txv";
	}

	/**
	 * Retourne le nom des champs de la table
	 * @return le noms des champs de la table dans l'ordre d'insertion dans la base de donnee
	 */
	private ArrayList<String> getNomAttributs() {
		ArrayList<String> ret =new ArrayList<String>();
		ret.add( "txv_code");
		ret.add("txv_tax_code");
		ret.add("txv_std_code");
		ret.add("txv_org_code");
		return ret;
	}
	
	 /**
	 * Retourne la valeur des attributs de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	private ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret =new ArrayList<Object>();
		ret.add(this.taxonvideo);
		ret.add(this.refTaxon.getCode());
		ret.add(this.refStade.getCode());
		return ret;
	}

	/**
	 * retourne l'indenfifiant du taxon dans l'interface vid�o
	 * 
	 * @return l'indentifiant en trois lettres du taxon
	 */
	public String gettaxonvideo() {
		return this.taxonvideo;
	}

	/**
	 * retourne le ref�rentiel des taxons
	 * 
	 * @return le taxon de r�f�rence
	 */
	public RefTaxon getrefTaxon() {
		return this.refTaxon;
	}


	/**
	 * Initialise la  r�f�rence du taxon
	 * @param refTaxon le taxon
	 */
	public void setrefTaxon(RefTaxon refTaxon) {
		this.refTaxon = refTaxon;
	}

	/**
	 * Initialise le taxonvideo en trois lettres
	 * 
	 * @param taxonvideo
	 */
	public void settaxonvideo(String taxonvideo) {
		this.taxonvideo = taxonvideo;
	}

	/**
	 * @return la r�f�rence du stade
	 */
	public RefStade getRefStade() {
		return refStade;
	}

	/**
	 * @param refStade Initialise la r�f�rence du stade
	 */
	public void setRefStade(RefStade refStade) {
		this.refStade = refStade;
	}

	/**
	 * @return l'affichage d'un taxon vid�o
	 */
	public String toString()
	{
		String res = "";
		res += this.taxonvideo +"    |    "+ this.refTaxon +"    |    "+ this.refStade;
		return res;
	}

}
