/*
 **********************************************************************
 *
 * Nom fichier :        RefStade.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */
package migration.referenciel;

import commun.Ref;

/**
 * Stade de developpement
 * 
 * @author Samuel Gaudey
 */
public class RefStade extends Ref {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une reference avec un code mais sans libelle
	 * 
	 * @param _code
	 *            le code
	 */
	public RefStade(String _code) {
		super(_code, null);
	} // end RefStade

	/**
	 * Construit une reference avec un code et un libelle
	 * 
	 * @param _code
	 *            le code
	 * @param _libelle
	 *            le libelle
	 */
	public RefStade(String _code, String _libelle) {
		super(_code, _libelle);
	} // end RefStade

	/**
	 * Construit une reference avec un code et un libelle et un statut
	 * 
	 * @param _code
	 *            le code
	 * @param _libelle
	 *            le libelle
	 */
	public RefStade(String _code, String _libelle, String statut) {
		super(_code, _libelle, statut);
	} // end RefStade

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	public void insertObjet() {
		super.insertObjet("ref.tr_stadedeveloppement_std");
	}

	public void majObjet() {
		super.majObjet("ref.tr_stadedeveloppement_std");
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

} // end RefStade
