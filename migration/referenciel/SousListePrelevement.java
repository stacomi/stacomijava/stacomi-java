/*
 **********************************************************************
 *
 * Nom fichier :        SousListePrelevement.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Sébastien Laigre
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   29 mai 2009
 * Compatibilite :      Java6/windows XP/ postgreSQL 8.3
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */
package migration.referenciel;

import java.sql.ResultSet;

import migration.Lot;
import migration.Prelevement;

import commun.ConnexionBD;
import commun.SousListe;

/**
 * Sous liste pour stocker les prélèvements
 * @author Sébastien Laigre
 */
public class SousListePrelevement extends SousListe {

	/** */
	private static final long serialVersionUID = 1L;

	/**
	 * Initialisation de le sous liste sans paramètre
	 */
	public SousListePrelevement()
	{
		super();
	}
	
	/**
	 * Initialisation de la sous liste
	 * @param lot : le lot de rattachement
	 */
	public SousListePrelevement(Lot lot)
	{
		super(lot);
	}
	
	/* (non-Javadoc)
	 * @see commun.SousListe#chargeObjet(java.lang.Object)
	 */
	@Override
	protected void chargeObjet(Object _objet) throws Exception {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see commun.SousListe#chargeSansFiltre()
	 */
	@Override
	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		String sql = "SELECT prl_pre_typeprelevement, prl_lot_identifiant, " +
				"FROM " + ConnexionBD.getSchema() + "tj_prelevementlot_prl" +
				"WHERE prl_lot_identifiant='"+getObjetDeRattachementID()[0]+"'";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while(rs.next())
		{
			String prl_pre_typeprelevement = rs.getString("prl_pre_typeprelevement");
			Integer prl_lot_identifiant = rs.getInt("prl_lot_identifiant");

			RefPrelevement rtpre = new RefPrelevement(prl_pre_typeprelevement);
			Lot lot = new Lot(prl_lot_identifiant, null, null, null, null, null);
			Prelevement p = new Prelevement(rtpre,lot);

			this.put(prl_pre_typeprelevement+prl_lot_identifiant, p);
		}
	}

	/* (non-Javadoc)
	 * @see commun.SousListe#chargeSansFiltreDetails()
	 */
	@Override
	public void chargeSansFiltreDetails() throws Exception {
		ResultSet rs = null;

		String sql = "SELECT prl_pre_typeprelevement, prl_lot_identifiant, prl_code, prl_loc_code, prl_commentaires, prl_operateur " +
				"FROM " + ConnexionBD.getSchema() + "tj_prelevementlot_prl " +
				"WHERE prl_lot_identifiant='"+getObjetDeRattachementID()[0]+"'";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while(rs.next())
		{
			String prl_pre_typeprelevement = rs.getString("prl_pre_typeprelevement");
			Integer prl_lot_identifiant = rs.getInt("prl_lot_identifiant");
			String prl_code = rs.getString("prl_code");
			String prl_loc_code = rs.getString("prl_loc_code");
			String prl_commentaires = rs.getString("prl_commentaires");
			String prl_operateur = rs.getString("prl_operateur");
			
			RefPrelevement rtpre = new RefPrelevement(prl_pre_typeprelevement);
			Lot lot = new Lot(prl_lot_identifiant, null, null, null, null, null);
			RefLocalisation loca = new RefLocalisation(prl_loc_code);
			Prelevement p = new Prelevement(rtpre,lot,prl_operateur,prl_code,loca,prl_commentaires);

			this.put(prl_pre_typeprelevement+prl_lot_identifiant, p);
		}
	}

	/* (non-Javadoc)
	 * @see commun.SousListe#getObjetDeRattachementID()
	 */
	@Override
	public String[] getObjetDeRattachementID() {
		String [] res = new String[1];
		res[0] = ((Lot)super.objetDeRattachement).getIdentifiant().toString();
		return res;
	}

}
