/*
 **********************************************************************
 *
 * Nom fichier :        ListeOperations.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package migration.referenciel;

import java.sql.ResultSet;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
import java.util.Date;
//import java.sql.Timestamp ;

import commun.ConnexionBD;
import commun.Liste;
import infrastructure.DC;
import migration.Operation;

/**
 * Liste pour les operations de controle des migrations
 * 
 * @author C�dric Briand
 */

public class ListeOperations extends Liste {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5381499010345716843L;

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	public void chargeSansFiltre() throws Exception {
		// A FAIRE
	}

	public void chargeSansFiltreDetails() throws Exception {
		// A FAIRE
	}

	protected void chargeObjet(Object _objet) throws Exception {
		// A FAIRE
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Charge dans la liste les infos de la derniere operations, en se basant
	 * sur la date de fin la plus elevee
	 * 
	 * @return l'identifiant de l'operation qui a ete chargee ou Null s'il n'y a
	 *         aucune operation
	 * @throws Exception
	 */
	public Integer chargeFiltreDerniere() throws Exception {
		Integer ret = this.chargeFiltreDerniere(null);
		return ret;
	} // end chargeFiltreDerniere

	/**
	 * Charge dans la liste les infos de la derniere operations d'un dc, en se
	 * basant sur la date de fin la plus elevee
	 * 
	 * @param _dc
	 *            le DC
	 * @return l'identifiant de l'operation qui a ete chargee ou Null s'il n'y a
	 *         aucune operation
	 * @throws Exception
	 */
	public Integer chargeFiltreDerniere(DC _dc) throws Exception {
		Integer ret = null;
		Operation ope = null;
		ResultSet rs = null;

		String complementWhere = "";

		if (_dc != null) {
			complementWhere = " WHERE ope_dic_identifiant='" + _dc.getIdentifiant().toString() + "' ";
		}

		// Extraction de l'enregistrement concerne
		// avec ou sans le compl�ment where
		String sql = "SELECT ope_identifiant, ope_dic_identifiant, ope_date_debut, ope_date_fin, ope_organisme, ope_operateur, ope_commentaires "
				+ "FROM " + ConnexionBD.getSchema() + "t_operation_ope " + complementWhere
				+ "ORDER BY ope_date_fin desc limit 1;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Lecture du premier enregistrement (en principe, il y a 0 ou 1
		// enregistrement)
		// si plus d'un tuple trouve, on ne tient compte que du premier

		if (rs.next()) {

			// Lecture des champs dans le ResultSet ;
			Integer identifiant = new Integer(rs.getInt("ope_identifiant"));
			DC dc = new DC(new Integer(rs.getInt("ope_dic_identifiant")));

			// Pour la date, utilisation de timestamp, sinon ne recupere pas les
			// heures, minutes, secondes
			// autre possibilite, mais deconseille : Date dateDebut=
			// rs.getTimestamp("ope_date_debut") ;
			Date dateDebut = new Date(rs.getTimestamp("ope_date_debut").getTime());
			Date dateFin = new Date(rs.getTimestamp("ope_date_fin").getTime());

			// rq cedric c'est mieux que le bricolage ci-dessous
			// Date dateDebut=stringtoDate(rs.getString("ope_date_debut"),
			// "yyyy-MM-dd HH:mm:ss");
			// Date dateFin= stringtoDate(rs.getString("ope_date_fin"),
			// "yyyy-MM-dd HH:mm:ss"); ;
			// Date dateDebut=rs.getTimestamp("ope_date_debut");
			// Date dateFin=rs.getTimestamp("ope_date_fin");

			String organisme = rs.getString("ope_organisme");
			String operateur = rs.getString("ope_operateur");
			String commentaires = rs.getString("ope_commentaires");

			// Initialisation de l'objet d'apres les champs lus et ajout a la
			// liste
			ope = new Operation(dc, identifiant, dateDebut, dateFin, organisme, operateur, commentaires, null);
			this.put(identifiant, ope);

			ret = identifiant;
		} else {
			ret = null;
		}

		return ret;
	} // end chargeFiltreDerniere

	/**
	 * 
	 * Teste si l'op�ration (dc, datedebut,datefin) est d�j� en base.
	 * 
	 * @author cedric.briand
	 * @param op
	 *            une op�ration
	 * @return boolean true si existe d�j�
	 * @throws Exception
	 */
	public static boolean exists(Operation op) throws Exception {

		ResultSet rs = null;
		String sql = "SELECT ope_identifiant " + "FROM " + ConnexionBD.getSchema() + "t_operation_ope "
				+ " WHERE ope_dic_identifiant = " + op.getDC().getIdentifiant() + " AND ope_date_debut = '"
				+ op.getDateDebut() + "' AND ope_date_fin ='" + op.getDateFin() + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		if (rs.next()) {
			return (true);
		} else {
			return (false);
		}
	}

	// /**
	// * @autor cedric
	// * @param String sDate
	// * @param String sFormat simpledateFormat "yyyy-MM-dd HH:mm:ss"
	// * @return Date
	// * @throws Exception
	// * @see
	// http://java.sun.com/javase/6/docs/api/java/text/SimpleDateFormat.html
	// */
	// private static Date stringtoDate(String sDate, String sFormat) throws
	// Exception {
	// SimpleDateFormat sdf = new SimpleDateFormat(sFormat);
	// return sdf.parse(sDate);
	// }
} // end ListeOperations
