/*
 **********************************************************************
 *
 * Nom fichier :        RefTypeQuantite.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;

import commun.* ;

/**
 * Type de quantite pour un lot
 * @author Samuel Gaudey
 */
public class RefTypeQuantite extends Ref {
    
  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////
    
    
    
  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    
    
/**
 * Construit une reference avec un code mais sans libelle
 * @param _code le code
 */    
    public RefTypeQuantite(String _code) {
        super(_code, null) ;
    } // end RefTypeQuantite
        
    
/**
 * Construit une reference avec un code et un libelle
 * @param _code le code
 * @param _libelle le libelle
 */    
    public RefTypeQuantite(String _code, String _libelle) {
        super(_code, _libelle) ;
    } // end RefTypeQuantite
    
   
    
    
    
  ///////////////////////////////////////
  // operations heritees
  ///////////////////////////////////////
    
    public void insertObjet() {
        super.insertObjet("ref.tr_typequantitelot_qte") ;
    }
    
    
    public void majObjet() {
        super.majObjet("ref.tr_typequantitelot_qte") ;
    }
    
    
    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////    
    
    
       
 
 } // end RefTypeQuantite



