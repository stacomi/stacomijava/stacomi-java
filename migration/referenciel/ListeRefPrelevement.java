/*
 **********************************************************************
 *
 * Nom fichier :        ListeRefPrelevement.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Sébastien Laigre
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   29 mai 2009
 * Compatibilite :      Java6/Windows XP/PostgreSQL 8.3
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package migration.referenciel;

import commun.* ;
import java.sql.* ;

/**
 * Liste pour les prélèvements
 */
public class ListeRefPrelevement extends Liste 
{
  ///////////////////////////////////////
  // operations heritees
   ///////////////////////////////////////

    /** */
	private static final long serialVersionUID = -5504497676008141032L;

	public void chargeSansFiltre() throws Exception 
	{
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT pre_typeprelevement FROM ref.tr_prelevement_pre ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) 
        {
            // Lecture des champs dans le ResultSet
            String prl_typeprelevement = rs.getString("pre_typeprelevement") ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            RefPrelevement refPre = new RefPrelevement(prl_typeprelevement);
            this.put(prl_typeprelevement, refPre) ;
        } // end while
    }

    public void chargeSansFiltreDetails() throws Exception 
    {
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT pre_typeprelevement, pre_definition FROM ref.tr_prelevement_pre ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) 
        {
            // Lecture des champs dans le ResultSet
            String prl_typeprelevement = rs.getString("pre_typeprelevement") ;
            String pre_description = rs.getString("pre_definition");
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            RefPrelevement refPre = new RefPrelevement(prl_typeprelevement, pre_description);
            this.put(prl_typeprelevement, refPre) ;
        } // end while
    }

    protected void chargeObjet(Object _objet) throws Exception 
    {
        ResultSet   rs   = null ;
        
        // Si l'identifiant de l'objet n'est pas definit, erreur
        String typePrelevement = ((RefPrelevement)_objet).getTypePrelevement() ;
        if (typePrelevement == null) {
            throw new NullPointerException(Erreur.I1000) ;
        }
        
        // Extraction de l'enregistrement concerne
        String sql = "SELECT pre_description FROM ref.tr_prelevement_pre WHERE pre_typeprelevement = '" + typePrelevement + "' ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // En principe, il y a un et un seul enregistrement
        if (rs.first() == false)
            throw new Exception(Erreur.I1001) ;

        // Lecture des champs dans le ResultSet
        String pre_description = rs.getString("pre_definition") ;
        
        // Initialisation de l'objet d'apres les champs lus
        ((RefPrelevement)_objet).setDefinition(pre_description) ;
    }
 
 } // end ListeRefPrelevement



