/*
 **********************************************************************
 *
 * Nom fichier :        ListeRefLocalisation.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package migration.referenciel;

import commun.*;
import java.sql.*;

/**
 * Liste pour les localisations de marques
 * @author Samuel Gaudey
 */
public class ListeRefLocalisation extends Liste {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/** */
	private static final long serialVersionUID = 4601383095699774325L;

	// (MARQUE | PATHOLOGIE)
	private String type;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une liste avec un type de localisation
	 * @param _type le type (MARQUE | PATHOLOGIE)
	 */
	public ListeRefLocalisation(String _type) {
		super();

		this.setType(_type);
	} // end ListeRefLocalisation

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT loc_code, loc_libelle FROM ref.tr_localisationanatomique_loc ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			String code = rs.getString("loc_code");
			String libelle = rs.getString("loc_libelle");

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			RefLocalisation ref = new RefLocalisation(code, libelle, this.type);
			this.put(code, ref);

		} // end while

	}

	public void chargeSansFiltreDetails() throws Exception {
		// Pas d'attribut a charger en plus 
		this.chargeSansFiltre();
	}

	protected void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		// Si l'identifiant de l'objet n'est pas definit, erreur
		String code = ((RefLocalisation) _objet).getCode();
		if (code == null) {
			throw new NullPointerException(Erreur.I1000);
		}

		// Extraction de l'enregistrement concerne
		String sql = "SELECT loc_libelle FROM ref.tr_localisationanatomique_loc WHERE loc_code = '"
				+ code + "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// En principe, il y a un et un seul enregistrement
		if (rs.first() == false) {
			throw new Exception(Erreur.I1001);
		}

		// Lecture des champs dans le ResultSet
		String libelle = rs.getString("loc_libelle");

		// Initialisation de l'objet d'apres les champs lus
		((RefLocalisation) _objet).setLibelle(libelle);

	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////      

	/**
	 * Retourne le type de la localisation
	 * @return le type (MARQUE | PATHOLOGIE)
	 */
	public String getType() {
		return this.type;
	} // end getType        

	/**
	 * Initialise le type de la localisation
	 * @param _type le type (MARQUE | PATHOLOGIE)
	 */
	public void setType(String _type) {
		this.type = _type;
	} // end setType        

} // end ListeRefLocalisationMarque

