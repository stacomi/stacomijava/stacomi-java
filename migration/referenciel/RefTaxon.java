/*
 **********************************************************************
 *
 * Nom fichier :        RefTaxon.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;

import commun.*;

import java.sql.SQLException;
import java.util.zip.DataFormatException;

/**
 * Taxon
 * @author Samuel Gaudey
 */
public class RefTaxon extends Ref {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	//super.libelle est le nom latin du taxon 
	private String nomCommun;
	private RefNiveauTaxonomique niveauTaxonomique;
	private RefTaxon taxonParent;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une reference avec seulement un code
	 * @param _code le code
	 */
	public RefTaxon(String _code) {
		this(_code, null, null, null, null);
	} // end RefTaxon

	/**
	 * Construit une reference avec un code et un libelle
	 * @param _code le code
	 * @param _nomLatin le nom latin du taxon
	 */
	public RefTaxon(String _code, String _nomLatin) {
		this(_code, _nomLatin, null, null, null);
	} // end RefTaxon

	/**
	 * Construit une reference avec un code et tous les autres attributs
	 * @param _code le code
	 * @param _nomLatin le nom latin du taxon
	 * @param _nomCommun le nom commun
	 * @param _niveauTaxonomique le niveau taxonomique
	 * @param _taxonParent le taxon parent
	 */
	public RefTaxon(String _code, String _nomLatin, String _nomCommun,
			RefNiveauTaxonomique _niveauTaxonomique, RefTaxon _taxonParent) {
		super(_code, _nomLatin);

		this.setNomCommun(_nomCommun);
		this.setNiveauTaxonomique(_niveauTaxonomique);
		this.setTaxonParent(_taxonParent);
	} // end RefTaxon

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////
	public void insertObjet() throws SQLException, ClassNotFoundException {
		// impossible d'appeler la methode dans la super classe car il y a des attributs en plus
		//super.insertObjet("tr_taxon_tax") ;

		String sql = "INSERT INTO ref.tr_taxon_tax (tax_code, tax_nom_latin, tax_nom_commun, tax_ntx_code, tax_tax_code) VALUES "
				+ super.code + ", "
				+ super.libelle + ", "
				+ this.nomCommun + ", "
				+ this.niveauTaxonomique.getCode() + ", "
				+ this.taxonParent.code + " ;";

		ConnexionBD.getInstance().getStatement().execute(sql);
	}

	public void majObjet() throws SQLException, ClassNotFoundException {
		// impossible d'appeler la methode dans la super classe car il y a des attributs en plus

		String sql;
		sql = "UPDATE ref.tr_taxon_tax SET tax_nom_latin = " + super.libelle
				+ ", tax_nom_commun = " + this.nomCommun + ", tax_ntx_code = "
				+ this.niveauTaxonomique.getCode() + ", tax_tax_code = "
				+ this.taxonParent.code + " WHERE code = " + super.code + " ;";

		ConnexionBD.getInstance().getStatement().execute(sql);
	}

	// Surcharge de la methode de la super classe pour verifier le niveau taxonomique
	public boolean verifAttributs() throws DataFormatException {

		// verification du code et du libelle
		boolean ret = super.verifAttributs();

		// verification de presence du niveau taxonomique
		if ((this.niveauTaxonomique == null)
				|| (this.niveauTaxonomique.getCode() == "")) {
			throw new DataFormatException(Erreur.S1003);
		}

		return ret;
	} // end verifAttributs  

	///////////////////////////////////////
	// operations
	///////////////////////////////////////    

	/**
	 * Retourne le nom commun du taxon
	 * @return le nom commun
	 */
	public String getNomCommun() {
		return this.nomCommun;
	} // end getNomCommun        

	/**
	 * Retourne le niveau taxonomique du taxon
	 * @return le niveau taxonomique
	 */
	public RefNiveauTaxonomique getNiveauTaxonomique() {
		return this.niveauTaxonomique;
	} // end getNiveauTaxonomique       

	/**
	 * Retourne le taxon parent
	 * @return le taxon parent
	 */
	public RefTaxon getTaxonParent() {
		return this.taxonParent;
	} // end getTaxonParent        

	/**
	 * Initialise le nom commun du taxon
	 * @param _nomCommun le nom commun
	 */
	public void setNomCommun(String _nomCommun) {
		this.nomCommun = _nomCommun;
	} // end setNomCommun        

	/**
	 * Initialise le niveau taxonomique du taxon
	 * @param _niveauTaxonomique le niveau taxonomique
	 */
	public void setNiveauTaxonomique(RefNiveauTaxonomique _niveauTaxonomique) {
		this.niveauTaxonomique = _niveauTaxonomique;
	} // end setNiveauTaxonomique        

	/**
	 * Initialise le taxon parent
	 * @param _taxonParent le taxon parent
	 */
	public void setTaxonParent(RefTaxon _taxonParent) {
		this.taxonParent = _taxonParent;
	} // end setTaxonParent       

} // end RefTaxon

