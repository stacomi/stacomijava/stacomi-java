/*
 **********************************************************************
 *
 * Nom fichier :        RefImportancePathologie.java
 * Projet :             StacomiJAVA0.5
 * Organisme :          IAV
 * Auteur :             C�dric Briand
 * Contact :             cedric.briand@lavilaine.com
 * Date de creation :   09/06/2016
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;

import commun.Ref;

/**
 * Importance des pathologies
 * 
 * @author C�dric Briand
 */
public class RefImportancePathologie extends Ref {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une reference avec un code mais sans libelle
	 * 
	 * @param _code
	 *            le code
	 */
	public RefImportancePathologie(String _code) {
		super(_code, null);
	} // end RefPathologie

	/**
	 * Construit une reference avec un code et un libelle
	 * 
	 * @param _code
	 *            le code
	 * @param _libelle
	 *            le libelle
	 */
	public RefImportancePathologie(String _code, String _libelle) {
		super(_code, _libelle);
	} // end RefPathologie

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	public void insertObjet() {
		super.insertObjet("ref.tr_importancepatho_imp");
	}

	public void majObjet() {
		super.majObjet("ref.tr_importancepatho_imp");
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

} // end RefImportancePathologie
