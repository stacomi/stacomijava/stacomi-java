/*
 **********************************************************************
 *
 * Nom fichier :        ListeRefImportancePathologie.java
 * Projet :             stacomi JAVA0.5
 * Organisme :          IAV
 * Auteur :             C�dric Briand
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   09/06/2016
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package migration.referenciel;

import java.sql.ResultSet;

import commun.ConnexionBD;
import commun.Erreur;
import commun.Liste;

/**
 * Liste pour le code d'importance de la pathologie
 * 
 * @author C�dric Briand
 */
public class ListeRefImportancePathologie extends Liste {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = -1070052101762947482L;

	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT imp_code, imp_libelle FROM ref.tr_importancepatho_imp ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			String code = rs.getString("imp_code");
			String libelle = rs.getString("imp_libelle");

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			RefImportancePathologie ref = new RefImportancePathologie(code, libelle);
			this.put(code, ref);

		} // end while

	}

	public void chargeSansFiltreDetails() throws Exception {
		// Pas d'attribut a charger en plus
		this.chargeSansFiltre();
	}

	protected void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		// Si l'identifiant de l'objet n'est pas definit, erreur
		String code = ((RefImportancePathologie) _objet).getCode();
		if (code == null) {
			throw new NullPointerException(Erreur.I1000);
		}

		// Extraction de l'enregistrement concerne
		String sql = "SELECT imp_libelle FROM ref.tr_importancepatho_imp WHERE imp_code = '" + code + "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// En principe, il y a un et un seul enregistrement
		if (rs.first() == false) {
			throw new Exception(Erreur.I1001);
		}

		// Lecture des champs dans le ResultSet
		String libelle = rs.getString("imp_libelle");

		// Initialisation de l'objet d'apres les champs lus
		((RefImportancePathologie) _objet).setLibelle(libelle);

	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

} // end ListeRefImportancePathologie
