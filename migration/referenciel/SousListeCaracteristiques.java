/*
 **********************************************************************
 *
 * Nom fichier :        SousListeCaracteristiques.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;

import java.sql.ResultSet;

import commun.ConnexionBD;
import commun.Erreur;
import commun.Lanceur;
import commun.SousListe;
import commun.SousListeRefValeurParametre;
import migration.Caracteristique;
import migration.Lot;

/**
 * Sous liste pour les caracteristiques d'un lot
 * 
 * @author Samuel Gaudey
 */
public class SousListeCaracteristiques extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = -6171183495929532894L;

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * 
	 * @param _objetDeRattachement
	 *            l'objet de rattachement
	 */
	public SousListeCaracteristiques(Lot _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeCaracteristiques

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	public void chargeSansFiltre() {

		// A FAIRE

	}

	public void chargeSansFiltreDetails() {

		// A FAIRE

	}

	protected void chargeObjet(Object _objet) {

		// A FAIRE

	}

	/**
	 * Retourne l'identifiant du lot
	 * 
	 * @return Tableau de taille 1 : (0) identifiant
	 */
	public String[] getObjetDeRattachementID() {

		// L'identifiant de Lot est : identifiant
		String ret[] = new String[1];
		ret[0] = ((Lot) super.objetDeRattachement).getIdentifiant().toString();

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Charge tous les attributs des caracteristiques d'un lot
	 * 
	 * @throws Exception
	 */
	public void chargeFiltreDetails() throws Exception {

		ResultSet rs = null;
		ListeRefParamQualBio lesParametresQual = Lanceur.getListeRefParamQualBio();
		ListeRefParamQuantBio lesParametresQuant = Lanceur.getListeRefParamQuantBio();
		// String[] lesMethodesObtention=
		// Lanceur.getMethodesObtentionCaracteristiques() ;
		SousListeRefValeurParametre lesValeurs = null;

		// Si l''objet de rattachement n'est pas definit, erreur
		if ((super.objetDeRattachement == null) || (((Lot) super.objetDeRattachement).getIdentifiant() == null)) {
			throw new NullPointerException(Erreur.I1000);
		}

		// Extraction des enregistrements concernes
		String sql = "SELECT car_par_code, car_methode_obtention, car_val_identifiant, car_valeur_quantitatif, car_precision, car_commentaires "
				+ "FROM " + ConnexionBD.getSchema() + "tj_caracteristiquelot_car " + "WHERE car_lot_identifiant = '"
				+ ((Lot) super.objetDeRattachement).getIdentifiant().toString() + "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet ;

			// Le parametre est soit qualitatif, soit quantitatif
			Float valeurQuan = null;
			RefValeurParametre valeurQual = null;
			RefParametre parametre = (RefParametre) lesParametresQual.get(rs.getString("car_par_code"));

			// Si parametre quantitatif
			if (parametre == null) {
				parametre = (RefParametre) lesParametresQuant.get(rs.getString("car_par_code"));
				valeurQuan = new Float(rs.getFloat("car_valeur_quantitatif"));
			}
			// Sinon, parametre qualitatif
			else {
				// Recherche des valeurs possibles pour ce parametre
				lesValeurs = new SousListeRefValeurParametre(parametre);
				lesValeurs.chargeFiltreDetails();

				valeurQual = (RefValeurParametre) lesValeurs.get(new Integer(rs.getInt("car_val_identifiant")));
			}

			String methodeObtention = rs.getString("car_methode_obtention");
			Float precision = new Float(rs.getFloat("car_precision"));
			if (precision.floatValue() == 0.0)
				precision = null;

			String commentaires = rs.getString("car_commentaires");

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			Caracteristique car = new Caracteristique((Lot) super.objetDeRattachement, parametre, methodeObtention,
					valeurQuan, valeurQual, precision, commentaires);
			String id = ((Lot) super.objetDeRattachement).getIdentifiant().toString() + parametre.getCode();
			this.put(id, car);

		}

	} // end chargeFiltreDetails

} // end SousListeCaracteristiques
