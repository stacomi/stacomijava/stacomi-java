/*
 **********************************************************************
 *
 * Nom fichier :        RefParametreQualitatif.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */


package migration.referenciel;

import commun.SousListeRefValeurParametre;

/**
 * 
 */
public class RefParametreQualitatif extends RefParametre {

  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////


    private String                      expressionValeurs   ; 
    private SousListeRefValeurParametre lesValeursPossibles ; 

 
    
    
  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    
    
/**
 * Construit une reference avec un code mais sans libelle
 * @param _code le code
 */    
    public RefParametreQualitatif(String _code) {
        this(_code, null, null, null, null, null, null) ;
    } // end RefParametreQualitatif
        
    
/**
 * Construit une reference avec un code, un nom et une unite
 * @param _code le code
 * @param _nom le nom
 * @param _unite l'unite
 */    
    public RefParametreQualitatif(String _code, String _nom, String _unite) {
        this(_code, _nom, _unite, null, null, null, null) ;
    } // end RefParametreQualitatif
    
    
/**
 * Construit une reference avec un code et tous les autres attributs
 * @param _code le code
 * @param _nom le nom
 * @param _unite l'unite
 * @param _nature la nature (BIOLOGIQUE | ENVIRONNEMENTAL)
 * @param _definition la definition
 * @param _expressionValeurs l'expression des valeurs possibles pour le parametre
 * @param _lesValeursPossibles la sous liste des valeurs possibles
 */    
    public RefParametreQualitatif(String _code, String _nom, String _unite, String _nature, String _definition, String _expressionValeurs, SousListeRefValeurParametre _lesValeursPossibles) {
        
        super(_code, _nom, _unite, _nature, _definition) ;
        
        this.setExpressionValeurs(_expressionValeurs)       ;
        this.setLesValeursPossibles(_lesValeursPossibles)   ;
        
    } // end RefParametreQualitatif  
    
    
    
  ///////////////////////////////////////
  // operations heritees
  ///////////////////////////////////////
    
    public void insertObjet() {
        // super.insertObjet("tr_parametrequalitatif_qal") ;
        
        // A FAIRE             !!!!!!!!!!!!!!!!!!!!!!!!!
        
    }
    
    
    public void majObjet() {
        //super.majObjet("tr_parametrequalitatif_qal") ;
       
        // A FAIRE             !!!!!!!!!!!!!!!!!!!!!!!!!
        
    }
    
    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////

/**
 * Retourne l'expression des valeurs possibles pour le parametre
 * @return l'expression des valeurs possibles
 */
    public String getExpressionValeurs() {        
        return this.expressionValeurs;
    } // end getExpressionValeurs        

/**
 * Retourne la sous liste des valeurs possibles pour le parametre
 * @return la sous liste
 */
    public SousListeRefValeurParametre getLesValeursPossibles() {        
        return this.lesValeursPossibles;
    } // end getLesValeursPossibles    
    
    
    
    
/**
 * Initialise l'expression des valeurs possibles pour le parametre
 * @param _expressionValeurs l'expression des valeurs possibles
 */
    public void setExpressionValeurs(String _expressionValeurs) {        
        this.expressionValeurs = _expressionValeurs;
    } // end setExpressionValeurs        
    
/**
 * Initialise la sous liste des valeurs possibles pour le parametre
 * @param _lesValeursPossibles la sous liste
 */
    public void setLesValeursPossibles(SousListeRefValeurParametre _lesValeursPossibles) {        
        this.lesValeursPossibles = _lesValeursPossibles;
    } // end setLesValeursPossibles        

    
    
 } // end RefParametreQualitatif



