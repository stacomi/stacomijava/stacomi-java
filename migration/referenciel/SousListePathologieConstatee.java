/*
 **********************************************************************
 *
 * Nom fichier :        SousListePathologies.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;

import java.sql.ResultSet;
import java.sql.SQLException;

import commun.ConnexionBD;
import commun.SousListe;
import migration.Lot;
import migration.PathologieConstatee;

/**
 * Sous liste pour les pathologies d'un lot
 * 
 * @author Samuel Gaudey
 */
public class SousListePathologieConstatee extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
		 * 
		 */
	private static final long serialVersionUID = -6099929172286530700L;

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * 
	 * @param _objetDeRattachement
	 *            l'objet de rattachement
	 */
	public SousListePathologieConstatee(Lot _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListePathologies

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	public void chargeSansFiltre() throws SQLException, ClassNotFoundException {
		ResultSet rs = null;
		String sql = "SELECT pco_pat_code, pco_loc_code, pco_imp_code, pco_commentaires " + "FROM "
				+ ConnexionBD.getSchema() + "tj_pathologieconstatee_pco " + "WHERE pco_lot_identifiant='"
				+ ((Lot) getObjetDeRattachement()).getIdentifiant() + "'";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while (rs.next()) {
			String path_code = rs.getString("pco_pat_code");
			String loc_code = rs.getString("pco_loc_code");
			String imp_code = rs.getString("pco_imp_code");
			String commentaires = rs.getString("pco_commentaires");

			RefPathologie rp = new RefPathologie(path_code);
			RefLocalisation rl = new RefLocalisation(loc_code);
			RefImportancePathologie imp = new RefImportancePathologie(imp_code);
			PathologieConstatee pc = new PathologieConstatee((Lot) getObjetDeRattachement(), rp, rl, imp, commentaires);

			String cle_primaire = ((Lot) getObjetDeRattachement()).getIdentifiant() + path_code + loc_code;
			this.put(cle_primaire, pc);
		}

	}

	public void chargeSansFiltreDetails() throws SQLException, ClassNotFoundException {
		ResultSet rs = null;
		String sql = "SELECT pco_pat_code, pco_loc_code, pco_imp_code, pco_commentaires, loc_libelle, pat_libelle,imp_libelle "
				+ "FROM " + ConnexionBD.getSchema() + "tj_pathologieconstatee_pco "
				+ "JOIN ref.tr_localisationanatomique_loc loc ON pco_loc_code=loc.loc_code "
				+ "JOIN ref.tr_importancepatho_imp imp ON imp.imp_code=pco_imp_code "
				+ "JOIN ref.tr_pathologie_pat pat ON pco_pat_code=pat.pat_code " + "WHERE pco_lot_identifiant='"
				+ ((Lot) getObjetDeRattachement()).getIdentifiant() + "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while (rs.next()) {
			String path_code = rs.getString("pco_pat_code");
			String loc_code = rs.getString("pco_loc_code");
			String imp_code = rs.getString("pco_imp_code");

			String commentaires = rs.getString("pco_commentaires");

			RefPathologie rp = new RefPathologie(path_code, rs.getString("pat_libelle"));
			RefLocalisation rl = new RefLocalisation(loc_code, rs.getString("loc_libelle"), "Pathologie");
			RefImportancePathologie imp = new RefImportancePathologie(imp_code, rs.getString("imp_libelle"));

			PathologieConstatee pc = new PathologieConstatee((Lot) getObjetDeRattachement(), rp, rl, imp, commentaires);

			String cle_primaire = ((Lot) getObjetDeRattachement()).getIdentifiant() + path_code + loc_code;
			this.put(cle_primaire, pc);
		}
	}

	protected void chargeObjet(Object _objet) throws SQLException, ClassNotFoundException {
		PathologieConstatee pc = (PathologieConstatee) _objet;
		Integer pc_identifiant = pc.getLot().getIdentifiant();
		String pc_pat_code = pc.getPathologie().getCode();
		String pc_loc_code = pc.getLocalisation().getCode();
		String pc_imp_code = pc.getImportance().getCode();

		ResultSet rs = null;
		String sql = "SELECT loc_libelle, loc_type, pat_libelle " + "FROM " + ConnexionBD.getSchema()
				+ "tj_pathologieconstatee_pco "
				+ "JOIN ref.tr_localisationanatomique_loc loc ON pco_loc_code=loc.loc_code "
				+ "JOIN ref.tr_pathologie_pat pat ON pco_pat_code=pat.pat_code " + "WHERE pco_lot_identifiant='"
				+ pc_identifiant + "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while (rs.next()) {
			RefPathologie rp = new RefPathologie(pc_pat_code, rs.getString("pat_libelle"));
			RefLocalisation rl = new RefLocalisation(pc_loc_code, rs.getString("loc_libelle"),
					rs.getString("loc_type"));
			RefImportancePathologie imp = new RefImportancePathologie(pc_imp_code);

			((PathologieConstatee) _objet).setPathologie(rp);
			((PathologieConstatee) _objet).setLocalisation(rl);
			((PathologieConstatee) _objet).setImportance(imp);
		}

	}

	/**
	 * Retourne l'identifiant du lot
	 * 
	 * @return Tableau de taille 1 : (0) identifiant
	 */
	public String[] getObjetDeRattachementID() {

		// L'identifiant de Lot est : identifiant
		String ret[] = new String[1];
		ret[0] = ((Lot) super.objetDeRattachement).getIdentifiant().toString();

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Does ... ???????????????????????????????????
	 * 
	 */
	public void chargeFiltreDetails() {
		// your code here
	} // end chargeFiltreDetails

} // end SousListePathologies
