/*
 **********************************************************************
 *
 * Nom fichier :        RefValeurParametre.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;
import java.sql.SQLException;
import java.util.zip.DataFormatException ;

import commun.ConnexionBD;
import commun.Erreur;
import commun.Ref;

/**
 * Valeur pour un parametre qualitatif
 */
public class RefValeurParametre extends Ref {

    
  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////


    private RefParametreQualitatif  parametre   ; 
    private Short                   rang        ; 



  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    
 
/**
 * Construit une reference avec seulement un identifiant
 * @param _identifiant l'identifiant
 */    
    public RefValeurParametre(Integer _identifiant) {
        this(_identifiant, null, null, null) ;
    } // end RefValeurParametre
        
  
/**
 * Construit une reference avec un identifiant, un libelle et un rang
 * @param _identifiant l'identifiant
 * @param _libelle le libelle
 * @param _rang le rang pour l'affichage. La premiere valeur affichee est la plus petite
 */    
    public RefValeurParametre(Integer _identifiant, String _libelle, Short _rang) {
        this(_identifiant, _libelle, null, _rang) ;
    } // end RefValeurParametre
     
    
/**
 * Construit une reference avec un code et tous les autres attributs
 * @param _identifiant l'identifiant
 * @param _libelle le libelle
 * @param _parametre le parametre auquel est destine la valeur
 * @param _rang le rang pour l'affichage. La premiere valeur affichee est la plus petite
 */    
    public RefValeurParametre(Integer _identifiant, String _libelle, RefParametreQualitatif _parametre, Short _rang) {
        super(_identifiant.toString(), _libelle) ;
        
        this.setParametre(_parametre)   ;
        this.setRang(_rang)             ;
        
    } // end RefValeurParametre
    
   
    
    
    
  ///////////////////////////////////////
  // operations heritees
  ///////////////////////////////////////
    
    public void insertObjet() throws SQLException, ClassNotFoundException {
        // impossible d'appeler la methode dans la super classe car il y a des attributs en plus
        //super.insertObjet("tr_valeurparametrequalitatif_val") ;
        
        // on ne tient pas compte de l'identifiant car il est auto-incremente
        String sql ;
        sql = "INSERT INTO ref.tr_valeurparametrequalitatif_val (val_qal_code, val_rang, val_libelle) VALUES " + this.parametre.getCode() + ", " + this.rang + ", " + super.libelle + " ;" ;
        
        // executer requete !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ConnexionBD.getInstance().getStatement().execute(sql);
    }
    
    public void majObjet() throws SQLException, ClassNotFoundException {
        // impossible d'appeler la methode dans la super classe car il y a des attributs en plus
        //super.majObjet("tr_valeurparametrequalitatif_val") ;
        
        String sql ;
        sql = "UPDATE ref.tr_valeurparametrequalitatif_val SET val_qal_code = " + this.parametre.getCode() + ", val_rang = " + this.rang + ", val_libelle = " + super.libelle + " WHERE code = " + super.code + " ;" ; 
        ConnexionBD.getInstance().getStatement().execute(sql);
    }
    
    // Surcharge de la methode de la super classe pour verifier le rang et le parametre
     public boolean verifAttributs() throws DataFormatException {
          
        // verification du code et du libelle
        boolean ret = super.verifAttributs() ;
        
        // verification de presence du parametre
        if ( (this.parametre == null) || (this.parametre.getCode() == "") ) {
            throw new DataFormatException (Erreur.S1006) ;
        }
        // verification de presence du rang
        else if ( (this.rang == null) || (this.rang.toString() == "") ) {
            throw new DataFormatException (Erreur.S1007) ;
        }
        
        return ret ;
    } // end verifAttributs  
    
     
     
     
     
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////


/**
 * Retourne le parametre auquel est destine la valeur
 * @return le parametre 
 */
    public RefParametreQualitatif getParametre() {        
        return this.parametre;
    } // end getParametre        

/**
 * Retourne le rang de la valeur, utilise pour l'affichage. La premiere valeur affichee est la plus petite
 * @return le rang
 */
    public Short getRang() {        
        return this.rang;
    } // end getRang    
    
    
    
 
/**
 * Initialise le parametre auquel est destine la valeur
 * @param _parametre le parametre 
 */
    public void setParametre(RefParametreQualitatif _parametre) {        
        this.parametre = _parametre;
    } // end setParametre        

/**
 * Initialise le rang de la valeur, utilise pour l'affichage. La premiere valeur affichee est la plus petite
 * @param _rang le rang
 */
    public void setRang(Short _rang) {        
        this.rang = _rang;
    } // end setRang        
    

 } // end RefValeurParametre



