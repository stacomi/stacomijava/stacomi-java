/*
 **********************************************************************
 *
 * Nom fichier :        ListeRefTaxon.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */


package migration.referenciel;

import commun.* ;

import java.sql.* ;

/**
 * Liste pour les taxons
 * @author Samuel Gaudey
 */
public class ListeRefTaxon extends Liste {

   ///////////////////////////////////////
   // attributes
   ///////////////////////////////////////
    
   /**
	 * 
	 */
	private static final long serialVersionUID = -3036271095704690432L;
ListeRefNiveauTaxonomique listeRefNiveauTaxonomique ; // la liste a utiliser pour les niveaux taxonomiques
   

   ///////////////////////////////////////
   // constructors
   ///////////////////////////////////////
   
    /**
     * Construit une reference avec seulement un code
     * @param _listeRefNiveauTaxonomique le code
     */    
    public ListeRefTaxon(ListeRefNiveauTaxonomique _listeRefNiveauTaxonomique) {
        super() ;
        
        this.setListeRefNiveauTaxonomique(_listeRefNiveauTaxonomique) ;
        
    } // end ListeRefTaxon
      
    
   
  ///////////////////////////////////////
  // operations heritees
   ///////////////////////////////////////

    public void chargeSansFiltre() throws Exception {
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT tax_code, tax_nom_latin FROM ref.tr_taxon_tax ORDER BY tax_rang ASC;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String code = rs.getString("tax_code") ;
            String libelle = rs.getString("tax_nom_latin") ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            RefTaxon ref = new RefTaxon(code, libelle) ;
            this.put(code, ref) ;
        
        } // end while

    }
    
    
    public void chargeSansFiltreDetails() throws Exception {
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT tax_code, tax_nom_latin, tax_nom_commun, tax_ntx_code, tax_tax_code FROM ref.tr_taxon_tax  ORDER BY tax_rang ASC ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String code                             = rs.getString("tax_code")       ;
            String libelle                          = rs.getString("tax_nom_latin")  ;
            String nomCommun                        = rs.getString("tax_nom_commun") ;
            
            // Recherche dans la liste le niveau taxonomique d'apres son code
            String code_niveauTaxonomique           = rs.getString("tax_ntx_code")   ;    
            RefNiveauTaxonomique niveauTaxonomique  = (RefNiveauTaxonomique)(this.listeRefNiveauTaxonomique.get(code_niveauTaxonomique)) ; 
            /*RefNiveauTaxonomique niveauTaxonomique  = null ; 
            for (Enumeration e = this.listeRefNiveauTaxonomique.elements() ; e.hasMoreElements() ; ) {
                niveauTaxonomique = (RefNiveauTaxonomique)e.nextElement() ;
                // arret quand l'objet a ete trouve
                if (niveauTaxonomique.getCode() == code_niveauTaxonomique) { break ; }
            }*/
            
            // Recherche dans la liste le taxon d'apres son code
            String code_taxonParent                 = rs.getString("tax_tax_code")   ; 
            
            if (code_taxonParent == null) { code_taxonParent = "" ; }
            RefTaxon taxonParent                    = (RefTaxon)(this.get(code_taxonParent)) ;            

            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            RefTaxon ref = new RefTaxon(code, libelle, nomCommun, niveauTaxonomique, taxonParent) ;
            this.put(code, ref) ;
        
        } // end while

    }
    
    
    
    protected void chargeObjet(Object _objet) throws Exception {
        ResultSet   rs   = null ;

        
        // Si l'identifiant de l'objet n'est pas definit, erreur
        String code = ((RefTaxon)_objet).getCode() ;
        if (code == null) {
            throw new NullPointerException(Erreur.I1000) ;
        }
        
        // Extraction de l'enregistrement concerne
        String sql = "SELECT tax_nom_latin, tax_nom_commun, tax_ntx_code, tax_tax_code FROM ref.tr_taxon_tax WHERE tax_code = '" + code + "' ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // En principe, il y a un et un seul enregistrement
        if (rs.first() == false) {
            throw new Exception(Erreur.I1001) ;
        }

        
        // Lecture des champs dans le ResultSet
        
        String libelle                          = rs.getString("tax_nom_latin") ;
        String nomCommun                        = rs.getString("tax_nom_commun") ;

        // Recherche dans la liste le niveau taxonomique d'apres son code
        String code_niveauTaxonomique           = rs.getString("tax_ntx_code")   ;    
        RefNiveauTaxonomique niveauTaxonomique  = (RefNiveauTaxonomique)(this.listeRefNiveauTaxonomique.get(code_niveauTaxonomique)) ; 
        
        // Recherche dans la liste le taxon d'apres son code
        String code_taxonParent                 = rs.getString("tax_tax_code")   ; 
        RefTaxon taxonParent                    = (RefTaxon)(this.get(code_taxonParent)) ;  
        
        // Initialisation de l'objet d'apres les champs lus
        ((RefTaxon)_objet).setLibelle(libelle)                      ;
        ((RefTaxon)_objet).setNomCommun(nomCommun)                  ;
        ((RefTaxon)_objet).setNiveauTaxonomique(niveauTaxonomique)  ;
        ((RefTaxon)_objet).setTaxonParent(taxonParent)              ;
              
    }
    
    

    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////    
    
 /**
 * Retourne la liste des niveaux taxonomiques
 * @return la liste
 */
    public ListeRefNiveauTaxonomique getListeRefNiveauTaxonomique() {        
        return this.listeRefNiveauTaxonomique ;
    } // end getListeRefNiveauTaxonomique        

 /**
 * Initialise la liste des niveaux taxonomiques
 * @param _listeRefNiveauTaxonomique la liste
 */
    public void setListeRefNiveauTaxonomique(ListeRefNiveauTaxonomique _listeRefNiveauTaxonomique) {        
        this.listeRefNiveauTaxonomique = _listeRefNiveauTaxonomique ;
    } // end setListeRefNiveauTaxonomique        
    
    
    
    
 } // end ListeRefTaxon



