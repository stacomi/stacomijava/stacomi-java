/*
 **********************************************************************
 *
 * Nom fichier :        ListeOperationMarquage.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */


package migration.referenciel;

import commun.* ;

import java.sql.* ;

import migration.OperationMarquage;

/**
 * Liste pour les operations de marquage
 * @author C�dric Briand
 */
public class ListeOperationMarquage extends Liste {

    
   ///////////////////////////////////////
   // attributes
   ///////////////////////////////////////


    
  ///////////////////////////////////////
  // operations heritees
  ///////////////////////////////////////

    /**
	 * 
	 */
	private static final long serialVersionUID = 8658568885554033051L;




	public void chargeSansFiltre() throws Exception {
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT omq_reference FROM " + ConnexionBD.getSchema() + "t_operationmarquage_omq ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String reference = rs.getString("omq_reference") ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            OperationMarquage operation = new OperationMarquage(reference) ;
            this.put(reference, operation) ;
        
        } // end while

    }
    
    
    public void chargeSansFiltreDetails() throws Exception {
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT omq_reference, omq_commentaires FROM " + ConnexionBD.getSchema() + "t_operationmarquage_omq ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            
            String reference      = rs.getString("omq_reference") ;
            String commentaires   = rs.getString("omq_commentaires") ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            OperationMarquage operation = new OperationMarquage(reference, commentaires) ;
            this.put(reference, operation) ;
        
        } // end while
        
    }
    
    
    
    protected void chargeObjet(Object _objet) throws Exception {
        ResultSet   rs   = null ;
        
        // Si l'identifiant de l'objet n'est pas definit, erreur
        String reference = ((OperationMarquage)_objet).getReference() ;
        if (reference == null) {
            throw new NullPointerException(Erreur.I1000) ;
        }
   
        // Extraction de l'enregistrement concerne
        String sql = "SELECT omq_commentaires " +
        		"FROM " + ConnexionBD.getSchema() + "t_operationmarquage_omq " +
        		"WHERE omq_reference = '" + reference + "' ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // En principe, il y a un et un seul enregistrement
        if (rs.first() == false) {
            throw new Exception(Erreur.I1001) ;
        }
        

        // Lecture des champs dans le ResultSet
        String commentaires          = rs.getString("mqe_commentaires") ;
            
            
        // Initialisation de l'objet d'apres les champs lus
        ((OperationMarquage)_objet).setCommentaires(commentaires)      ;
              
    }
    
    
    
    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////   
    
    
    
    /**
     * Charge dans la liste une operation de marquage d'apres sa reference
     * @param _reference r�f�rence de l'op�ration � charger
     * @throws Exception 
     */
    public void chargeFiltre(String _reference) throws Exception {        
        
        // creation d'une nouvelle operation avec sa reference et ajout a la liste
        OperationMarquage operation = new OperationMarquage(_reference) ;
        this.put(_reference, operation) ;
        
        // chargement de cette marque
        this.chargeObjet(operation) ;

    } // end chargeFiltre        

    
 } // end ListeOperationMarquage



