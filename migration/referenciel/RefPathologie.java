/*
 **********************************************************************
 *
 * Nom fichier :        RefPathologie.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;

import commun.Ref;

/**
 * Pathologie
 * 
 * @author Samuel Gaudey
 */
public class RefPathologie extends Ref {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une reference avec un code mais sans libelle
	 * 
	 * @param _code
	 *            le code
	 */
	public RefPathologie(String _code) {
		super(_code, null);
	} // end RefPathologie

	/**
	 * Construit une reference avec un code et un libelle
	 * 
	 * @param _code
	 *            le code
	 * @param _libelle
	 *            le libelle
	 */
	public RefPathologie(String _code, String _libelle) {
		super(_code, _libelle);
	} // end RefPathologie

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	public void insertObjet() {
		super.insertObjet("ref.tr_pathologie_pat");
	}

	public void majObjet() {
		super.majObjet("ref.tr_pathologie_pat");
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////
	/**
	 * Retourne les principaux attributs sous forme textuelle
	 * 
	 * @return le libell�
	 */
	public String toString() {

		String ret = this.code + " - " + this.libelle;

		return ret;
	}

} // end RefPathologie
