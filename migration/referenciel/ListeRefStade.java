/*
 **********************************************************************
 *
 * Nom fichier :        ListeRefStade.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package migration.referenciel;

import java.sql.ResultSet;

import commun.ConnexionBD;
import commun.Erreur;
import commun.Liste;

/**
 * Liste pour les stades de developpement
 * 
 * @author Samuel Gaudey
 */
public class ListeRefStade extends Liste {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 5946641228862324076L;

	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT std_code, std_libelle,std_statut FROM ref.tr_stadedeveloppement_std;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			String code = rs.getString("std_code");
			String libelle = rs.getString("std_libelle");
			String statut = rs.getString("std_statut");
			if (statut != null)
				libelle = libelle + "[!gel�]";

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			RefStade ref = new RefStade(code, libelle, statut);
			this.put(code, ref);

		} // end while

	}

	public void chargeSansFiltreDetails() throws Exception {
		// Pas d'attribut a charger en plus
		this.chargeSansFiltre();
	}

	protected void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		// Si l'identifiant de l'objet n'est pas definit, erreur
		String code = ((RefStade) _objet).getCode();
		if (code == null) {
			throw new NullPointerException(Erreur.I1000);
		}

		// Extraction de l'enregistrement concerne
		String sql = "SELECT std_libelle, std_statut FROM ref.tr_stadedeveloppement_std WHERE std_code = '" + code
				+ "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// En principe, il y a un et un seul enregistrement
		if (rs.first() == false) {
			throw new Exception(Erreur.I1001);
		}

		// Lecture des champs dans le ResultSet
		String libelle = rs.getString("std_libelle");
		String statut = rs.getString("std_statut");
		// Initialisation de l'objet d'apres les champs lus
		((RefStade) _objet).setLibelle(libelle);
		((RefStade) _objet).setStatut(statut);

	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

} // end ListeRefStade
