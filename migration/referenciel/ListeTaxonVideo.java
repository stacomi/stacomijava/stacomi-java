/*
 **********************************************************************
 *
 * Nom fichier :        ListeTaxonvideo.java
 * Projet :             Controle migrateurs 2008
 * Organisme :          IAV/CSP
 * Auteur :             cedric Briand
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2008
 * Compatibilite :      
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;

import commun.* ;

import java.sql.* ;

import migration.Taxonvideo;

/**
 * Liste pour la correspondance taxon code vid�os
 * @author C�dric Briand
 */
public class ListeTaxonVideo extends Liste {

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
  

   ///////////////////////////////////////
   // constructors
   ///////////////////////////////////////
   
    /**
     * Construit une reference par d�faut
     * 
     */    
    public ListeTaxonVideo() {
        super() ;
        // initialisation des listes utilisees
        
    } // end ListeRefTaxonVideo
      
    
    
  ///////////////////////////////////////
  // operation heritees
  ///////////////////////////////////////

    




	public void chargeSansFiltre() throws Exception {
    	 ResultSet       rs      = null ;
    	    ListeRefTaxon        lesTaxons          = Lanceur.getListeRefTaxon()        ;
            ListeRefStade        lesStades          = Lanceur.getListeRefStade()        ;
         // Extraction de toutes les enregistrements de la table concernee
         String sql = "SELECT txv_code, txv_tax_code, txv_std_code FROM " + ConnexionBD.getSchema()+"ts_taxonvideo_txv ORDER BY txv_code ASC;" ;

         rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
         
         // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
         while (rs.next()) {
             
             // Lecture des champs dans le ResultSet
             String code = rs.getString("txv_code") ;
             
             // Recherche dans la liste la reftxon d'apres le code taxon  
             String taxcode = rs.getString("txv_tax_code") ;
             RefTaxon refTaxon = (RefTaxon)(lesTaxons.get(taxcode)) ;
             // recherche dans la liste refstades d'apr�s le code stade
             String stdcode=rs.getString("txv_std_code");
             RefStade refStade=(RefStade)lesStades.get(stdcode);
             // Creation de l'objet d'apres les champs lus et ajout a la liste
             Taxonvideo txv = new Taxonvideo(code, refTaxon,refStade) ;
             this.put(code, txv) ;
         
         } // end while
    }
    
  
	protected void chargeObjet(Object _objet) throws Exception {
		// TODO Auto-generated method stub
	}



	public void chargeSansFiltreDetails() throws Exception {
		// TODO Auto-generated method stub
		
	}


 } // end SousListeDF


