/*
 **********************************************************************
 *
 * Nom fichier :        SousListeLots.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;

import java.sql.ResultSet;

import commun.ConnexionBD;
import commun.Erreur;
import commun.Lanceur;
import commun.SousListe;
import migration.Lot;
import migration.Operation;

/**
 * Sous-liste pour les lots d'une operation
 */
public class SousListeLots extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 1847833007513514306L;

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * 
	 * @param _objetDeRattachement
	 *            l'objet de rattachement
	 */
	public SousListeLots(Operation _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeDF

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	/**
	 * Charge les lots fils d'un lot
	 * 
	 * @param lotPere
	 *            le lot p�re dont on cherche les fils
	 * @throws Exception
	 *             Erreur avec la Base de donn�es
	 */
	public void chargeFiltreParent(Lot lotPere) throws Exception {
		ResultSet rs = null;

		String sql = "SELECT lot_identifiant, lot_effectif " + "FROM " + ConnexionBD.getSchema() + "t_lot_lot "
				+ "WHERE lot_lot_identifiant='" + lotPere.getIdentifiant() + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while (rs.next()) {
			Integer lot_identifiant = rs.getInt("lot_identifiant");
			Float eff = rs.getFloat("lot_effectif");
			Lot l = new Lot(lot_identifiant, null, null, eff, null, lotPere);
			this.put(l.getIdentifiant(), l);
		}
	}

	/**
	 * Charge les lots fils d'un lot avec tous leurs attributs pour l'instant ne
	 * charge que les caract�ristiques
	 * 
	 * @param lotPere
	 *            le lot p�re dont on cherche les fils
	 * @throws Exception
	 *             Erreur avec la Base de donn�es
	 */
	public void chargeFiltreParentDetails(Lot lotPere) throws Exception {
		ResultSet rs = null;

		String sql = "SELECT lot_identifiant, lot_effectif FROM " + ConnexionBD.getSchema() + "t_lot_lot"
				+ " WHERE lot_lot_identifiant='" + lotPere.getIdentifiant() + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while (rs.next()) {
			Integer lot_identifiant = rs.getInt("lot_identifiant");
			Float eff = rs.getFloat("lot_effectif");
			Lot l = new Lot(lot_identifiant, null, null, eff, null, lotPere);
			SousListeCaracteristiques souslistecar = new SousListeCaracteristiques(l);
			souslistecar.chargeFiltreDetails();
			l.setLesCaracteristiques(souslistecar);
			this.put(l.getIdentifiant(), l);
			// TODO charger les pathologies....
		}
	}

	public void chargeSansFiltre() {

		// A FAIRE

	}

	public void chargeSansFiltreDetails() {

		// A FAIRE

	}

	protected void chargeObjet(Object _objet) {

		// A FAIRE

	}

	/**
	 * Retourne l'identifiant de l'operation
	 * 
	 * @return Tableau de taille 1 : (0) identifiant
	 */
	public String[] getObjetDeRattachementID() {

		// L'identifiant de Operation est : identifiant
		String ret[] = new String[1];
		ret[0] = ((Operation) super.objetDeRattachement).getIdentifiant().toString();

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Charge la sous liste des lots d'une operation concernant un certain taxon
	 * et un certain stade
	 * 
	 * @param taxon
	 *            le taxon
	 * @param stade
	 *            le stade
	 */
	public void chargeFiltre(RefTaxon taxon, RefStade stade) {

		// A FAIRE

	} // end chargeFiltre

	/**
	 * Charge les lots d'une operation, y compris les dependances (pathologies
	 * constatees, marquages, caracteristiques) charge tous les lots et les
	 * souslots
	 * 
	 * @throws Exception
	 */
	public void chargeFiltreDetails() throws Exception {
		this.chargeFiltreDetails(false);

	} // end chargeFiltreDetails

	/**
	 * Charge les lots d'une operation, y compris les dependances (pathologies
	 * constatees, marquages, caracteristiques) Cette fonction est utilis�e avec
	 * un parm pour ne pas charger les souslots
	 * 
	 * @param sanssouslots
	 *            indique si les �chantillons doivent �tre charg�s ou non.
	 *            sanssouslots=VRAI si l'on ne veut que les lot
	 *            sanssouslots=FAUX si on veut charger tous les
	 *            lots/echantillons rattach�s � l'op�ration
	 * @throws Exception
	 * 
	 */
	public void chargeFiltreDetails(boolean sanssouslots) throws Exception {

		ResultSet rs = null;

		ListeRefTaxon lesTaxons = Lanceur.getListeRefTaxon();
		ListeRefStade lesStades = Lanceur.getListeRefStade();
		ListeRefTypeQuantite lesTypesQuantite = Lanceur.getListeRefTypeQuantite();
		ListeRefDevenir lesDevenirs = Lanceur.getListeRefDevenir();

		// Si l''objet de rattachement n'est pas definit, erreur
		if ((super.objetDeRattachement == null) || (((Operation) super.objetDeRattachement).getIdentifiant() == null)) {
			throw new NullPointerException(Erreur.I1000);
		}
		String sql = null;
		if (sanssouslots) {
			sql = "SELECT lot_identifiant, lot_tax_code, lot_std_code, lot_effectif, lot_quantite, lot_qte_code, lot_methode_obtention, lot_lot_identifiant, lot_dev_code, lot_commentaires "
					+ "FROM " + ConnexionBD.getSchema() + "t_lot_lot" + " WHERE lot_ope_identifiant = '"
					+ ((Operation) super.objetDeRattachement).getIdentifiant().toString()
					+ "' AND lot_lot_identifiant IS NULL" + " ORDER BY lot_identifiant ;";
		} else {
			// avec sous lot
			sql = "SELECT lot_identifiant, lot_tax_code, lot_std_code, lot_effectif, lot_quantite, lot_qte_code, lot_methode_obtention, lot_lot_identifiant, lot_dev_code, lot_commentaires "
					+ "FROM " + ConnexionBD.getSchema() + "t_lot_lot" + " WHERE lot_ope_identifiant = '"
					+ ((Operation) super.objetDeRattachement).getIdentifiant().toString()
					+ "' ORDER BY lot_identifiant ;";
			// Extraction des enregistrements concernes
		}
		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet ;
			Integer identifiant = new Integer(rs.getInt("lot_identifiant"));
			RefTaxon taxon = (RefTaxon) lesTaxons.get(rs.getString("lot_tax_code"));
			RefStade stadeDeveloppement = (RefStade) lesStades.get(rs.getString("lot_std_code"));
			Float effectif = new Float(rs.getFloat("lot_effectif"));
			if (effectif.floatValue() == 0) {
				effectif = null;
			}
			Float valeurQuantite = new Float(rs.getFloat("lot_quantite"));
			if (valeurQuantite.floatValue() == 0) {
				valeurQuantite = null;
			}
			RefTypeQuantite typeQuantite = (RefTypeQuantite) lesTypesQuantite.get(rs.getString("lot_qte_code"));
			String methodeObtention = rs.getString("lot_methode_obtention");

			Lot lotParent = null;
			// Si un lot parent est definit
			if (rs.getInt("lot_lot_identifiant") != 0) {
				lotParent = (Lot) this.get(new Integer(rs.getInt("lot_lot_identifiant")));

				// Si lotParent est null, c'est que le lot parent n'a pas encore
				// ete charge ;
				// En principe catch (n'arrive pas car les lots sont extrait
				// classes par identifiant.
				// Et comme un echantillon est cree apres sont lot pere, il a un
				// numero identifiant superieur a son pere
			}

			RefDevenir devenir = (RefDevenir) lesDevenirs.get(rs.getString("lot_dev_code"));
			String commentaires = rs.getString("lot_commentaires");

			SousListePathologieConstatee lesPathologies = null;
			SousListeMarquages lesMarquages = null;
			SousListeCaracteristiques lesCaracteristiques = null;

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			Lot lot = new Lot(identifiant, (Operation) super.objetDeRattachement, taxon, stadeDeveloppement, effectif,
					valeurQuantite, typeQuantite, methodeObtention, lotParent, devenir, commentaires, lesPathologies,
					lesMarquages, lesCaracteristiques);
			this.put(identifiant, lot);

			// charge les dependances de ce lot (caracteristiques, marques,
			// pathologies)

			lesPathologies = new SousListePathologieConstatee(lot);
			lesMarquages = new SousListeMarquages(lot);
			lesCaracteristiques = new SousListeCaracteristiques(lot);

			lesPathologies.chargeSansFiltreDetails();
			lesMarquages.chargeSansFiltreDetails();
			lesCaracteristiques.chargeFiltreDetails();

			lot.setLesPathologiesConstatees(lesPathologies);
			lot.setLesMarquages(lesMarquages);
			lot.setLesCaracteristiques(lesCaracteristiques);
		}

	} // end chargeFiltreDetails

} // end SousListeLots
