/*
 **********************************************************************
 *
 * Nom fichier :        SousListeOperations.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */
package migration.referenciel;

import commun.*;
import infrastructure.DC;

import java.sql.*;

import migration.Operation;
import migration.TailleVideo;

/**
 * Sous liste pour les tailles vid�o d'un DC
 * @author C�dric Briand
 */
public class SousListeTailleVideo extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////  

	/**
	 * 
	 */
	private static final long serialVersionUID = -6717614767563557036L;

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * @param _objetDeRattachement l'objet de rattachement
	 */
	public SousListeTailleVideo(DC _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeDF

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	public void chargeSansFiltre() throws SQLException, ClassNotFoundException
	{
		ResultSet rs = null;

		Integer identifiant = ((DC) super.objetDeRattachement).getIdentifiant();
		if(identifiant!=null)
		{
			// Extraction des enregistrements concernes
			//String sql = "SELECT ope_identifiant, ope_dic_identifiant, ope_date_debut, ope_date_fin, ope_organisme, ope_operateur, ope_commentaires FROM t_operation_ope WHERE ope_date_fin = (SELECT MAX(ope_date_fin) " + complementWhere + " FROM t_operation_ope) ;" ;
			String sql = "SELECT tav_dic_identifiant, tav_coefconversion, tav_distance " +
					"FROM "+ConnexionBD.getSchema()+"ts_taillevideo_tav WHERE tav_dic_identifiant = '" + identifiant + "' ;";
	
			rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
	
			// Boucle sur les enregistrements trouves
			while (rs.next()) {
				Integer DCCode = rs.getInt("tav_dic_identifiant");
				float coeff = rs.getFloat("tav_coefconversion");
				String dist = rs.getString("tav_distance");
	
				TailleVideo tv = new TailleVideo(DCCode, dist, coeff);
				this.put(identifiant.toString() + dist, tv);
			}
		}
	}

	public void chargeSansFiltreDetails() {
		// A FAIRE
	}

	/**
	 * Retourne l'identifiant du DC
	 * @return Tableau de taille 1 : (0) identifiant
	 */
	public String[] getObjetDeRattachementID() {

		// L'identifiant de DC est : identifiant
		String ret[] = new String[1];
		ret[0] = ((DC) super.objetDeRattachement).getIdentifiant().toString();

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Charge dans la sous-liste des tailles vid�o du DC 
	 * @throws Exception
	 */
	public void chargeFiltre() throws Exception {

	} // end chargeFiltre        

	public void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		// Si l'identifiant de l'objet n'est pas definit, erreur
		Integer identifiant = ((Operation) _objet).getIdentifiant();
		if (identifiant == null) {
			throw new NullPointerException(Erreur.I1000);
		}

		// Extraction de l'enregistrement concerne
		String sql = "SELECT tav_coefconversion, tav_distance " +
				"FROM "+ConnexionBD.getSchema()+"ts_taillevideo_tav WHERE tav_dic_identifiant = '"
				+ identifiant + "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// En principe, il y a un et un seul enregistrement
		if (!rs.first()) {
			throw new Exception(Erreur.I1001);
		}

		((TailleVideo) _objet).setDistance(rs.getString("tav_distance"));
		((TailleVideo) _objet).setCoefConversion(rs
				.getFloat("tav_coefconversion"));
	}

	/**
	 * Charge dans la sous-liste la derniere operation du DC
	 */
	public void chargeFiltreDerniere() {

		// A FAIRE

	} // end chargeFiltreDerniere        

} // end SousListeOperations

