/*
 **********************************************************************
 *
 * Nom fichier :        ListeMarques.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package migration.referenciel;

import commun.* ;
import migration.Marque;
import migration.OperationMarquage;
import java.sql.* ;

/**
 * Liste pour les marques
 * @author C�dric Briand
 */
public class ListeMarques extends Liste {


   ///////////////////////////////////////
   // attributes
   ///////////////////////////////////////
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -6152037697651177888L;
	ListeRefLocalisation    listeRefLocalisation    ;
    ListeRefNatureMarque    listeRefNatureMarque    ;
    ListeOperationMarquage  listeOperationMarquage  ;


    
   ///////////////////////////////////////
   // constructors
   ///////////////////////////////////////
   
    /**
     * Construit une reference avec seulement un code
     * @param _listeRefLocalisation
     * @param _listeRefNatureMarque
     * @param _listeOperationMarquage
     */
    public ListeMarques(ListeRefLocalisation _listeRefLocalisation, ListeRefNatureMarque _listeRefNatureMarque, ListeOperationMarquage _listeOperationMarquage) {
        super() ;
        
        // initialisation des listes utilisees
        this.setListeRefLocalisation(_listeRefLocalisation) ;
        this.setListeRefNatureMarque(_listeRefNatureMarque) ;
        this.setListeOperationMarquage(_listeOperationMarquage) ;
        
    } // end ListeMarques
      
    
    

  ///////////////////////////////////////
  // operations heritees
   ///////////////////////////////////////

    public void chargeSansFiltre() throws Exception {
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT mqe_reference FROM " + ConnexionBD.getSchema() + "t_marque_mqe ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String reference = rs.getString("mqe_reference") ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            Marque marque = new Marque(reference) ;
            this.put(reference, marque) ;
        
        } // end while

    }
    
    
    public void chargeSansFiltreDetails() throws Exception {
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT mqe_reference, mqe_loc_code, mqe_nmq_code, mqe_omq_reference, mqe_commentaires " +
        		"FROM " + ConnexionBD.getSchema() + "t_marque_mqe ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            
            String reference             = rs.getString("mqe_reference") ;
            
            // Recherche dans la liste la localisation d'apres son code  
            String code_localisation     = rs.getString("mqe_loc_code") ;
            RefLocalisation localisation = (RefLocalisation)(this.listeRefLocalisation.get(code_localisation)) ;
            
            // Recherche dans la liste la nature d'apres son code  
            String code_nature           = rs.getString("mqe_nmq_code") ;
            RefNatureMarque nature       = (RefNatureMarque)(this.listeRefNatureMarque.get(code_nature)) ;
            
            // Recherche dans la liste l'operation de marquage d'apres son code  
            String reference_operation   = rs.getString("mqe_omq_reference") ;
            OperationMarquage operation  = (OperationMarquage)(this.listeOperationMarquage.get(reference_operation)) ;
            
            String commentaires          = rs.getString("mqe_commentaires") ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            Marque marque = new Marque(reference, localisation, nature, operation, commentaires) ;
            this.put(reference, marque) ;
        
        } // end while
        
    }
    
    
    
    protected void chargeObjet(Object _objet) throws Exception {
        ResultSet   rs   = null ;
        
        // Si l'identifiant de l'objet n'est pas definit, erreur
        String reference = ((Marque)_objet).getReference() ;
        if (reference == null) {
            throw new NullPointerException(Erreur.I1000) ;
        }
   
        // Extraction de l'enregistrement concerne
        String sql = "SELECT mqe_loc_code, mqe_nmq_code, mqe_omq_reference, mqe_commentaires " +
        		"FROM " + ConnexionBD.getSchema() + "t_marque_mqe " +
        		"WHERE mqe_reference = '" + reference + "' ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // En principe, il y a un et un seul enregistrement
        if (rs.first() == false) {
            throw new Exception(Erreur.I1001) ;
        }

        // Lecture des champs dans le ResultSet
        
        // Recherche dans la liste la localisation d'apres son code  
        String code_localisation     = rs.getString("mqe_loc_code") ;
        RefLocalisation localisation = (RefLocalisation)(this.listeRefLocalisation.get(code_localisation)) ;
            
        // Recherche dans la liste la nature d'apres son code  
        String code_nature           = rs.getString("mqe_nmq_code") ;
        RefNatureMarque nature       = (RefNatureMarque)(this.listeRefNatureMarque.get(code_nature)) ;
            
        // Recherche dans la liste l'operation de marquage d'apres son code  
        String reference_operation   = rs.getString("mqe_omq_reference") ;
        OperationMarquage operation  = (OperationMarquage)(this.listeOperationMarquage.get(reference_operation)) ;
            
        String commentaires          = rs.getString("mqe_commentaires") ;
            
            
        // Initialisation de l'objet d'apres les champs lus
        ((Marque)_objet).setLocalisation(localisation)      ;
        ((Marque)_objet).setNature(nature)                  ;
        ((Marque)_objet).setOperationMarquage(operation)    ;
        ((Marque)_objet).setCommentaires(commentaires)      ;
              
    }
    
    

    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////  
    
    /**
     * Charge dans la liste une marque recherchee d'apres sa reference
     * @param _reference la reference de la marque a charger
     * @throws Exception
     */
    public void chargeFiltre(String _reference) throws Exception  {        
        
        // creation d'une nouvelle marque avec sa reference et ajout a la liste
        Marque marque = new Marque(_reference) ;
        this.put(_reference, marque) ;
        
        // chargement de cette marque
        this.chargeObjet(marque) ;

    } // end chargeFiltre        

    /**
     * Charge dans la liste les marques d'une certaine nature et d'une certaine localisation
     * @param _localisation localisation de la marque
     * @param _nature nature de la marque
     * @throws Exception
     */
    public void chargeFiltre(RefLocalisation _localisation, RefNatureMarque _nature) throws Exception {        
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT mqe_reference " +
        		"FROM " + ConnexionBD.getSchema() + "t_marque_mqe " +
        		"WHERE mqe_localisation = '" + _localisation.getCode() + "' AND mqe_nature = '" + _nature.getCode() + "' ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String reference = rs.getString("mqe_reference") ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            Marque marque = new Marque(reference) ;
            this.put(reference, marque) ;
        
        } // end while

    } // end chargeFiltre        

    
    
     
     /**
     * Retourne la liste des localisations
     * @return la liste
     */
        
        public ListeRefLocalisation getListeRefLocalisation() {        
            return this.listeRefLocalisation ;
        } // end getListeRefLocalisation        

             
     /**
     * Retourne la liste des natures de marques
     * @return la liste
     */
        public ListeRefNatureMarque getListeRefNatureMarque() {        
            return this.listeRefNatureMarque ;
        } // end getListeRefNatureMarque        

             
     /**
     * Retourne la liste des operations de marquage
     * @return la liste
     */
        public ListeOperationMarquage getListeOperationMarquage() {        
            return this.listeOperationMarquage ;
        } // end getListeOperationMarquage       

        
        
        
     /**
     * Initialise des localisations
     * @param _listeRefLocalisation la liste
     */
        public void setListeRefLocalisation(ListeRefLocalisation _listeRefLocalisation) {        
            this.listeRefLocalisation = _listeRefLocalisation ;
        } // end setListeRefLocalisation     
        
    /**
     * Initialise des localisations
     * @param _listeRefNatureMarque la liste
     */
        public void setListeRefNatureMarque(ListeRefNatureMarque _listeRefNatureMarque) {        
            this.listeRefNatureMarque = _listeRefNatureMarque ;
        } // end setListeRefNatureMarque      
        
    /**
     * Initialise des operations de marquage
     * @param _listeOperationMarquage la liste
     */
        public void setListeOperationMarquage(ListeOperationMarquage _listeOperationMarquage) {        
            this.listeOperationMarquage = _listeOperationMarquage ;
        } // end setListeOperationMarquage      
    
    
 } // end ListeMarques



