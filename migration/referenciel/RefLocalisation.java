/*
 **********************************************************************
 *
 * Nom fichier :        RefLocalisation.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;

import java.util.zip.DataFormatException;

import commun.Erreur;
import commun.Ref;

/**
 * Localisation anatomique pour les marques ou pour les pathologies
 * 
 * @author Samuel Gaudey
 */
public class RefLocalisation extends Ref {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	private String type;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une reference avec un code mais sans libelle et sans mnemonique
	 * 
	 * @param _code
	 *            le code
	 */
	public RefLocalisation(String _code) {
		this(_code, null, null);
	} // end RefLocalisation

	/**
	 * Construit une reference avec un code, un libelle et un type
	 * 
	 * @param _code
	 *            le code
	 * @param _libelle
	 *            le libelle
	 * @param _type
	 *            le type
	 */
	public RefLocalisation(String _code, String _libelle, String _type) {
		super(_code, _libelle);

		this.setType(_type);

	} // end RefLocalisation

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	public void insertObjet() {

		// executer requete !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	}

	public void majObjet() {

		// executer requete !!!!!!!!!!!!!!!!!!

	}

	// Surcharge de la methode de la super classe pour verifier le type
	public boolean verifAttributs() throws DataFormatException {

		// verification du code et du libelle
		boolean ret = super.verifAttributs();

		// verification de presence du type
		if ((this.type != "MONTEE") && (this.type != "DESCENTE")) {
			throw new DataFormatException(Erreur.S1004);
		}

		return ret;
	} // end verifAttributs

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Retourne le type de la localisation
	 * 
	 * @return le type (MARQUE | PATHOLOGIE)
	 */
	public String getType() {
		return this.type;
	} // end getType

	/**
	 * Initialise le type de la localisation
	 * 
	 * @param _type
	 *            le type (MARQUE | PATHOLOGIE)
	 */
	public void setType(String _type) {
		this.type = _type;
	} // end setType

	/**
	 * Retourne les principaux attributs sous forme textuelle
	 * 
	 * @return le libell�
	 */
	public String toString() {

		String ret = this.code + " - " + this.libelle;

		return ret;
	}

} // end RefLocalisation
