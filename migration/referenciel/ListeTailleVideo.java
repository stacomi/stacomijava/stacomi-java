/*
 **********************************************************************
 *
 * Nom fichier :        ListeTaxonvideo.java
 * Projet :             Controle migrateurs 2008
 * Organisme :          IAV/CSP
 * Auteur :             cedric Briand
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2008
 * Compatibilite :      
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;

import commun.*;

import java.sql.*;

import migration.TailleVideo;

/**
 * Liste pour la correspondance taille en pixel et taille
 * @author C�dric Briand
 */
public class ListeTailleVideo extends Liste {

	/** */
	private static final long serialVersionUID = 1L;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une reference avec seulement un code
	 * 
	 */
	public ListeTailleVideo() 
	{
		super();
		// initialisation des listes utilisees

	} // end ListeRefTaxonVideo

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT tav_dic_identifiant, tav_coefconversion, tav_distance " +
				"FROM " + ConnexionBD.getSchema() + "ts_taillevideo_tav ORDER BY tav_distance ASC;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			Integer DC_code = rs.getInt("tav_dic_identifiant");

			// Lecture des champs dans le ResultSet
			Float coefconversion = rs.getFloat("tav_coefconversion");

			// Recherche dans la liste la reftxon d'apres le code taxon
			String distance = rs.getString("tav_distance");

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			// [I , (I,coef1)]
			// [L, (L,coef1)]
			// [P, (P,coef1)]
			TailleVideo tav = new TailleVideo(DC_code, distance, coefconversion);
			this.put(distance, tav);
		} // end while
	}

	protected void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		String distance = ((TailleVideo)_objet).getDistance();
		Integer codeDC = ((TailleVideo)_objet).getCodeDC();
		
		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT tav_coefconversion FROM " + ConnexionBD.getSchema() + "ts_taillevideo_tav " +
				"WHERE tav_dic_identifiant='"+codeDC+"' AND tav_distance='"+distance+"' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			Float coefconversion = rs.getFloat("tav_coefconversion");

			// mise a jour de l'objet
			((TailleVideo)_objet).setCoefConversion(coefconversion);
		} // end while
	}

	public void chargeSansFiltreDetails() throws Exception {
		this.chargeSansFiltre();
	}

} // end SousListeDF
