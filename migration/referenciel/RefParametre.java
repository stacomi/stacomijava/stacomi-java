/*
 **********************************************************************
 *
 * Nom fichier :        RefParametre.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */


package migration.referenciel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.zip.DataFormatException ;

import commun.ConnexionBD;
import commun.Erreur;
import commun.Ref;

/**
 * Classe abstraite pour les parametres : biologique ou environnemental, et qualitatif ou quantitatif
 */
public abstract class RefParametre extends Ref {

    
  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////

    private String unite        ; 
    private String nature       ; 
    private String definition   ; 

    
    
  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    
/**
 * Construit une reference avec seulement un code
 * @param _code le code
 */    
    public RefParametre(String _code) {
        this(_code, null, null, null, null) ;
    } // end RefParametre
        
  
/**
 * Construit une reference avec un code un nom et une unite
 * @param _code le code
 * @param _nom le nom
 * @param _unite l'unite
 */    
    public RefParametre(String _code, String _nom, String _unite) {
        this(_code, _nom, _unite, null, null) ;
    } // end RefParametre
     
    
/**
 * Construit une reference avec un code et tous les autres attributs
 * @param _code le code
 * @param _nom le nom
 * @param _unite l'unite
 * @param _nature la nature (BIOLOGIQUE | ENVIRONNEMENTAL)
 * @param _definition la definition
 */    
    public RefParametre(String _code, String _nom, String _unite, String _nature, String _definition) {
        super(_code, _nom) ;
        
        this.setUnite(_unite)           ;
        this.setNature(_nature)         ;
        this.setDefinition(_definition) ;
    } // end RefParametre
    
   
    
    
    
  ///////////////////////////////////////
  // operations heritees
  ///////////////////////////////////////
    
    public void insertObjet() {

        // A FAIRE       !!!!!!!!!!!!!!!!!!!!
        // ou peut etre pas, vu que parametre est abstrait ?
    }
    
    
    public void majObjet() {

         // A FAIRE       !!!!!!!!!!!!!!!!!!!!
        // ou peut etre pas, vu que parametre est abstrait ?
    }
      
    
    // Surcharge de la methode de la super classe pour verifier les attributs supplementaires
     public boolean verifAttributs() throws DataFormatException {
          
        // verification du code et du libelle
        boolean ret = super.verifAttributs() ;
        
        // Rien d'autre a verifier pour le moment. Au cas ou, un exemple :
        // verification de presence de la nature
        if ( (this.nature != "BIOLOGIQUE") && (this.nature != "ENVIRONNEMENTAL") )
            throw new DataFormatException (Erreur.S1005) ;
        
        return ret ;
    } // end verifAttributs  
     

  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////

/**
 * Retourne l'unite du parametre
 * @return l'unite
 */
    public String getUnite() {        
        return this.unite;
    } // end getUnite        

/**
 * Retourne la nature du parametre
 * @return la nature (BIOLOGIQUE | ENVIRONNEMENTAL)
 */
    public String getNature() {        
        return this.nature;
    } // end getNature    

/**
 * Retourne la definition du parametre
 * @return la definition
 */
    public String getDefinition() {        
        return this.definition;
    } // end getDefinition        
    
    
    
/**
 * Initialise l'unite du parametre
 * @param _unite l'unite
 */
    public void setUnite(String _unite) {        
        this.unite = _unite;
    } // end setUnite        

/**
 * Initialise la nature du parametre
 * @param _nature la nature (BIOLOGIQUE | ENVIRONNEMENTAL)
 */
    public void setNature(String _nature) {        
        this.nature = _nature;
    } // end setNature        

/**
 * Initialise la definition du parametre
 * @param _definition la definition
 */
    public void setDefinition(String _definition) {        
        this.definition = _definition;
    } // end setDefinition        

    
 /**
 * Retourne les attributs du parametre de lot sous forme textuelle
 * @return l'affichage d'un parametre
 */
    public String toString() {        

        String ret ;
        String complement = "" ;
        if ((this.unite != null) && (this.unite.length() > 0)) {
            complement = " (" + this.unite + ")" ;
        }
       
        ret = super.libelle + complement ;

        return ret ;
    }
       
    /**
     * Retourne le type de parametre � partir de son code (qualitatif ou quantitatif)
     * @param code le code du param�tre
     * @return vrai si le parametre est qualitatif, faux sinon
     * @throws SQLException
     * @throws Exception
     */
    public static boolean estQualitatif(String code) throws SQLException, Exception
    {
    	boolean res = false; 
    	
    	ResultSet rs = null;
		
		// la requete
		String sql = 
			"SELECT qan_par_code IS NULL as quantitatif " +
	        "FROM ref.tr_parametrequantitatif_qan " +
	        "FULL OUTER JOIN " +
			"ref.tg_parametre_par ON qan_par_code=par_code " +
			"where par_code ='" + code + "';";
		
		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
		
		// en principe, il n'y a qu'un seul enregistrement
		if (rs.next()) 
			res = rs.getBoolean("quantitatif");
        else 
        	throw new Exception(Erreur.I1001);
		
		
    	return res;
    }
    
 } // end RefParametre



