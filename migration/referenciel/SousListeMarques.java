/**
 * 
 */
package migration.referenciel;

import java.sql.ResultSet;
import java.sql.SQLException;

import migration.Lot;
import migration.Marque;
import migration.OperationMarquage;
import migration.referenciel.RefLocalisation;
import migration.referenciel.RefNatureMarque;

import commun.*;

/**
 * @author cedric
 *
 */
public class SousListeMarques extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////  

	/**
	 * 
	 */
	private static final long serialVersionUID = -310604924990618752L;

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * @param _objetDeRattachement l'objet de rattachement
	 */
	public SousListeMarques(OperationMarquage _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeMarquages

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	public void chargeSansFiltre() throws SQLException, ClassNotFoundException {
		// A FAIRE
	}

	public void chargeSansFiltreDetails() throws SQLException,
			ClassNotFoundException {
		ResultSet rs = null;

		String sql = "SELECT mqe_reference, mqe_loc_code, mqe_nmq_code, mqe_omq_reference, mqe_commentaires, loc_libelle, nmq_libelle "
				+ "FROM "+ ConnexionBD.getSchema() + " t_marque_mqe mq "
				+ "JOIN ref.tr_localisationanatomique_loc loc ON mqe_loc_code=loc.loc_code "
				+ "JOIN ref.tr_naturemarque_nmq nmq ON mqe_nmq_code=nmq.nmq_code "
				+ "WHERE mqe_omq_reference = '"
				+ ((OperationMarquage) getObjetDeRattachement()).getReference()
				+ "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// parcours de touts les lignes r�sultat
		while (rs.next()) {

			String mqe_loc_code = rs.getString("mqe_loc_code");
			String loc_libelle = rs.getString("loc_libelle");
			// Constructeur appelant tout on avait pas forcement besoin
			RefLocalisation loc = new RefLocalisation(mqe_loc_code,
					loc_libelle, "MARQUE");
			String mqe_nmq_code = rs.getString("mqe_nmq_code");
			String nmq_libelle = rs.getString("nmq_libelle");
			RefNatureMarque nat = new RefNatureMarque(mqe_nmq_code, nmq_libelle);
			String mqe_omq_reference = rs.getString("mqe_omq_reference");
			OperationMarquage op = new OperationMarquage(mqe_omq_reference);
			String mqe_commentaires = rs.getString("mqe_commentaires");
			String mqe_reference = rs.getString("mqe_reference");
			Marque mq = new Marque(mqe_reference, loc, nat, op,
					mqe_commentaires);
			// m�thode de sam
			this.put(mqe_reference, mq);
		}
	}

	protected void chargeObjet(Object _objet) {
		// A FAIRE
	}

	/**
	 * Retourne l'indentifiant du lot de rattachement
	 * @return Tableau de taille 1 : (0) identifiant
	 */
	public String[] getObjetDeRattachementID() {

		// L'identifiant de Lot est : identifiant
		String ret[] = new String[1];
		ret[0] = ((Lot) super.objetDeRattachement).getIdentifiant().toString();

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Does ...??????????????????????????????
	 * 
	 */
	public void chargeFiltreDetails() {
		// your code here
	} // end chargeFiltreDetails   

} // end SousListeMarquages

