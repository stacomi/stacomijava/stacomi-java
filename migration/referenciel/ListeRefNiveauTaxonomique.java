/*
 **********************************************************************
 *
 * Nom fichier :        ListeRefNiveauTaxonomique.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */


package migration.referenciel;

import commun.* ;
import java.sql.* ;

/**
 * Liste pour les Niveaux taxonomiques
 * @author Samuel Gaudey
 */
public class ListeRefNiveauTaxonomique extends Liste {


   ///////////////////////////////////////
   // attributes
   ///////////////////////////////////////
    
   
  ///////////////////////////////////////
  // operations heritees
   ///////////////////////////////////////

    /**
	 * 
	 */
	private static final long serialVersionUID = -6384543909977951352L;



	public void chargeSansFiltre() throws Exception {
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT ntx_code, ntx_libelle FROM ref.tr_niveautaxonomique_ntx ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String code = rs.getString("ntx_code") ;
            String libelle = rs.getString("ntx_libelle") ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            RefNiveauTaxonomique ref = new RefNiveauTaxonomique(code, libelle) ;
            this.put(code, ref) ;
        
        } // end while

    }
    
    
    public void chargeSansFiltreDetails() throws Exception {
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT ntx_code, ntx_libelle, ntx_mnemonique FROM ref.tr_niveautaxonomique_ntx ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String code                             = rs.getString("ntx_code")       ;
            String libelle                          = rs.getString("ntx_libelle")  ;
            String mnemonique                       = rs.getString("ntx_mnemonique") ;

            // Creation de l'objet d'apres les champs lus et ajout a la liste
            RefNiveauTaxonomique ref = new RefNiveauTaxonomique(code, libelle, mnemonique) ;
            this.put(code, ref) ;
        
        } // end while

    }
    
    
    
    protected void chargeObjet(Object _objet) throws Exception {
        ResultSet   rs   = null ;

        
        // Si l'identifiant de l'objet n'est pas definit, erreur
        String code = ((RefNiveauTaxonomique)_objet).getCode() ;
        if (code == null) {
            throw new NullPointerException(Erreur.I1000) ;
        }
        
        // Extraction de l'enregistrement concerne
        String sql = "SELECT ntx_libelle, ntx_mnemonique FROM ref.tr_niveautaxonomique_ntx WHERE ntx_code = '" + code + "' ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // En principe, il y a un et un seul enregistrement
        if (rs.first() == false) {
            throw new Exception(Erreur.I1001) ;
        }

        
        // Lecture des champs dans le ResultSet
        
        String libelle     = rs.getString("ntx_libelle") ;
        String mnemonique  = rs.getString("ntx_mnemonique") ;

        
        // Initialisation de l'objet d'apres les champs lus
        ((RefNiveauTaxonomique)_objet).setLibelle(libelle)       ;
        ((RefNiveauTaxonomique)_objet).setMnemonique(mnemonique) ;
              
    }
    
    

    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////   
    
    
 } // end ListeRefNiveauTaxonomique



