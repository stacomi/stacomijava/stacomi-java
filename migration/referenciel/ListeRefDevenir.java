/*
 **********************************************************************
 *
 * Nom fichier :        ListeRefDevenir.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package migration.referenciel;

import commun.* ;
import java.sql.* ;

/**
 * Liste pour les devenirs de lots
 * @author Samuel Gaudey
 */
public class ListeRefDevenir extends Liste {

   ///////////////////////////////////////
   // attributes
   ///////////////////////////////////////
    



  ///////////////////////////////////////
  // operations heritees
   ///////////////////////////////////////

    /**
	 * 
	 */
	private static final long serialVersionUID = -5504497676008141032L;



	public void chargeSansFiltre() throws Exception {
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT dev_code, dev_libelle FROM ref.tr_devenirlot_dev ORDER BY dev_rang ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String code = rs.getString("dev_code") ;
            String libelle = rs.getString("dev_libelle") ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            RefDevenir ref = new RefDevenir(code, libelle) ;
            this.put(code, ref) ;
        
        } // end while

    }
    
    
    public void chargeSansFiltreDetails() throws Exception {
        // Pas d'attribut a charger en plus 
        this.chargeSansFiltre() ;
    }
    
    
    
    protected void chargeObjet(Object _objet) throws Exception {
        ResultSet   rs   = null ;
        
        // Si l'identifiant de l'objet n'est pas definit, erreur
        String code = ((RefDevenir)_objet).getCode() ;
        if (code == null) {
            throw new NullPointerException(Erreur.I1000) ;
        }
        
        // Extraction de l'enregistrement concerne
        String sql = "SELECT dev_libelle FROM ref.tr_devenirlot_dev WHERE dev_code = '" + code + "' ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // En principe, il y a un et un seul enregistrement
        if (rs.first() == false) {
            throw new Exception(Erreur.I1001) ;
        }

        // Lecture des champs dans le ResultSet
        String libelle = rs.getString("dev_libelle") ;

        
        // Initialisation de l'objet d'apres les champs lus
        ((RefDevenir)_objet).setLibelle(libelle) ;
              
    }
    
    

    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////    
    
 
 } // end ListeRefDevenir



