/*
 **********************************************************************
 *
 * Nom fichier :        ListeRefParamQuantBio.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package migration.referenciel;

import commun.* ;
import java.sql.* ;

/**
 * Liste pour les parametres biologiques qualitatifs
 * @author Samuel Gaudey
 */
public class ListeRefParamQuantBio extends Liste {


   ///////////////////////////////////////
   // attributes
   ///////////////////////////////////////
    
   /**
	 * 
	 */
	private static final long serialVersionUID = -924092268252664602L;
private static final String nature = "BIOLOGIQUE" ;


  ///////////////////////////////////////
  // operations heritees
   ///////////////////////////////////////

    public void chargeSansFiltre() throws Exception {
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT par_code, par_nom, par_unite FROM ref.tg_parametre_par INNER JOIN ref.tr_parametrequantitatif_qan ON par_code = qan_par_code WHERE UPPER(par_nature) = 'BIOLOGIQUE' ORDER BY par_nom ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String code     = rs.getString("par_code")  ;
            String nom      = rs.getString("par_nom")   ;
            String unite    = rs.getString("par_unite") ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            RefParametreQuantitatif ref = new RefParametreQuantitatif(code, nom, unite) ;
            this.put(code, ref) ;
        
        } // end while

    }
    
    
    public void chargeSansFiltreDetails() throws Exception {
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT par_code, par_nom, par_unite, par_definition FROM ref.tg_parametre_par INNER JOIN ref.tr_parametrequantitatif_qan ON par_code = qan_par_code WHERE UPPER(par_nature) = 'BIOLOGIQUE' ORDER BY par_nom ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String code             = rs.getString("par_code")              ;
            String nom              = rs.getString("par_nom")               ;
            String unite            = rs.getString("par_unite")             ;
            String definition       = rs.getString("par_definition")        ;
            String nature           = ListeRefParamQuantBio.nature          ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            RefParametreQuantitatif ref = new RefParametreQuantitatif(code, nom, unite, nature, definition) ;
            this.put(code, ref) ;
        
        } // end while

    }
    
    
    
    protected void chargeObjet(Object _objet) throws Exception {
        ResultSet   rs   = null ;
        
        // Si l'identifiant de l'objet n'est pas definit, erreur
        String code = ((RefParametreQuantitatif)_objet).getCode() ;
        if (code == null) {
            throw new NullPointerException(Erreur.I1000) ;
        }
        
        // Extraction de l'enregistrement concerne
        String sql = "SELECT par_code, par_nom, par_unite, par_definition FROM ref.tg_parametre_par INNER JOIN ref.tr_parametrequantitatif_qan ON par_code = qan_par_code WHERE qan_par_code = '" + code + "' ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // En principe, il y a un et un seul enregistrement
        if (rs.first() == false) {
            throw new Exception(Erreur.I1001) ;
        }

        // Lecture des champs dans le ResultSet
        String nom              = rs.getString("par_nom")               ;
        String unite            = rs.getString("par_unite")             ;
        String definition       = rs.getString("par_definition")        ;
        String nature           = ListeRefParamQuantBio.nature          ;

        
        // Initialisation de l'objet d'apres les champs lus
        ((RefParametreQuantitatif)_objet).setLibelle(nom)                        ;
        ((RefParametreQuantitatif)_objet).setUnite(unite)                        ;
        ((RefParametreQuantitatif)_objet).setDefinition(definition)              ;
        ((RefParametreQuantitatif)_objet).setNature(nature)                      ;
              
    }
    
    

    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////    
    
    
    /**
     * Charge une liste contenant seulement le parametre precise
     * @param _nomParametre le parametre a charger
     * @throws Exception 
     */
     public void chargeFiltre(String _nomParametre) throws Exception {
        ResultSet       rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT par_code, par_nom, par_unite FROM ref.tg_parametre_par INNER JOIN ref.tr_parametrequantitatif_qan ON par_code = qan_par_code WHERE UPPER(par_nature) = 'BIOLOGIQUE' AND UPPER(par_nom) LIKE '%" + _nomParametre.toUpperCase() + "%' ORDER BY par_nom ;" ;

 //System.out.println(sql);

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String code     = rs.getString("par_code")  ;
            String nom      = rs.getString("par_nom")   ;
            String unite    = rs.getString("par_unite") ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            RefParametreQuantitatif ref = new RefParametreQuantitatif(code, nom, unite) ;
            this.put(code, ref) ;
        
        } // end while

    }
     
     
    
 } // end ListeRefParamQuantBio



