/*
 **********************************************************************
 *
 * Nom fichier :        SousListeDfEstDestinea.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;

import java.sql.ResultSet;

import commun.* ;
import infrastructure.DF ;

/**
 * Sous liste pour les taxons d'un DF
 * @author Samuel Gaudey
 */
public class SousListeDfEstDestinea extends SousListe {

   ///////////////////////////////////////
   // attributes
   ///////////////////////////////////////
    
  
    
  ///////////////////////////////////////
  // operation heritees
  ///////////////////////////////////////
    
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 7937430686528139653L;


	public void chargeSansFiltre() {
        
        // A FAIRE
        
    }
    
    public void chargeSansFiltreDetails() {
    
        
        // A FAIRE
    
        
    }
    
    protected void chargeObjet(Object _objet) {
        
        // A FAIRE
        
    }
    
    
    /**
     * Retourne l'identifiant du DF
     * @return Tableau de taille 1 : (0) identifiant
     */
    public String[] getObjetDeRattachementID() {
        
        // L'identifiant de DF est : identifiant
        String ret[] = new String[1] ;
        ret[0] = ((DF)super.objetDeRattachement).getIdentifiant().toString() ;
        
        return ret ;
    }    
    
    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////
    
    /**
     * 
     * @param o le DF
     * @return la liste des taxons associ�s au DF
     * @throws Exception
     */
    public ListeRefTaxon chargeFiltreObjet(Object o) throws Exception
    {
    	// Le DF
    	DF df = (DF)o;

    	// la liste des types de DF du DF
    	ListeRefTaxon resultat = new ListeRefTaxon(new ListeRefNiveauTaxonomique());
    	
    	// le resultset
    	ResultSet rs = null;

    	if ((df == null) || ((df.getIdentifiant() == null)) )
            throw new NullPointerException(Erreur.I1000) ;

    	// Extraction de l'enregistrement concerne cle unique
		String sql = "SELECT dtx_tax_code "
				+ " FROM " + ConnexionBD.getSchema() + "tj_dfestdestinea_dtx "
				+ " WHERE dtx_dif_identifiant= '"
				+ df.getIdentifiant().toString() +"' ; " ;

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// on parcourt l'ensemble des types de DF
		while(rs.next())
		{
			// Lecture des champs dans le ResultSet
			String codeTaxon = rs.getString("dtx_tax_code");

			RefTaxon taxon = new RefTaxon(codeTaxon);

			resultat.put(df.getIdentifiant()+codeTaxon , taxon);
		}
 
		return resultat;
    }
        
 } // end SousListeDfEstDestinea



