/*
 **********************************************************************
 *
 * Nom fichier :        RefPrelevement.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             S�bastien Laigre
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   29 mai 2009
 * Compatibilite :      Java6/windows XP/ postgreSQL 8.3
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */
package migration.referenciel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import migration.Prelevement;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;

/**
 * Pr�levement taxonomique
 * @author S�bastien Laigre
 */
public class RefPrelevement implements IBaseDonnees 
{
	/** Le type de pr�l�vement */
	private String typePrelevement;

	/** La d�finition du type de pr�l�vement */
	private String definition;

	/**
	 * Construction d'un RefTypePrelevement 
	 * @param typePrelevement : le type de pr�l�vement
	 * @param definition : la d�finition
	 */
	public RefPrelevement(String typePrelevement, String definition)
	{
		this.setTypePrelevement(typePrelevement);
		this.setDefinition(definition);
	}

	/**
	 * Construction d'un RefTypePrelevement
	 * @param typePrelevement
	 */
	public RefPrelevement(String typePrelevement)
	{
		this(typePrelevement, null);
	}

	/**
	 * Acc�s au type de pr�l�vement
	 * @return le type de pr�l�vement
	 */
	public String getTypePrelevement()
	{
		return this.typePrelevement;
	}

	/**
	 * Modification du type de pr�l�vement
	 * @param typePrelevement : le nouveau type de pr�l�vement
	 */
	public void setTypePrelevement(String typePrelevement)
	{
		this.typePrelevement = typePrelevement;
	}

	/**
	 * Acc�s � la d�finition
	 * @return la d�finition
	 */
	public String getDefinition()
	{
		return this.definition;
	}

	/**
	 * Modification de la d�finition
	 * @param definition : la nouvelle d�finition
	 */
	public void setDefinition(String definition)
	{
		this.definition = definition;
	}
	
	/**
	 * Affichage d'un refPrelevement = son type
	 */
	public String toString()
	{
		return this.typePrelevement;
	}

	/**
	 * Insertion d'un pr�l�vement dans la base
	 */
	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = Prelevement.getNomTable();
		ArrayList nomAttributsDC = Prelevement.getNomAttributs();
		ArrayList valAttributsDC = this.getValeurAttributs();
		ConnexionBD.getInstance().executeInsert(table, nomAttributsDC, valAttributsDC) ;
		System.out.println("Prelevement : insertObjet()");
	}

	public void majObjet() throws SQLException, ClassNotFoundException 
	{ }

	/**
	 * Mise a jour d'un pr�l�vement dans la base
	 * @param old l'ancienne r�f�rence du pr�l�vement
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public void majObjet(Prelevement old) throws SQLException, ClassNotFoundException 
	{
		String table = Prelevement.getNomTable();
		ArrayList nomAttributsDC = Prelevement.getNomAttributs();
		ArrayList valAttributsDC = this.getValeurAttributs();
		
		String[] nomID = Prelevement.getNomID();
		Object[] valeurID = old.getValeurID();
		ConnexionBD.getInstance().executeUpdate(table, nomID, valeurID, nomAttributsDC, valAttributsDC) ;
		System.out.println("Prelevement : majObjet()");
	}
	
	/**
	 * Suppression d'un pr�l�vement dans la base 
	 */
	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = Prelevement.getNomTable();
		String[] nomID = Prelevement.getNomID();
		Object[] valeurID = this.getValeurID();

		ConnexionBD.getInstance().executeDelete(table, nomID, valeurID);
		System.out.println("Prelevement : effaceObjet()");
	}
	
	/**
	 * Verification de la valeur des attributs
	 */
	public boolean verifAttributs() throws DataFormatException {
		
		// le type de pr�l�vement
		if(!Verification.isText(this.getTypePrelevement(), 1, 15, true))
			throw new DataFormatException(Erreur.S24000);
		
		// la d�finition
		if(!Verification.isText(this.getDefinition(), 1, false))
			throw new DataFormatException(Erreur.S24001);
		
		return true;
	}
	
	/**
	 * Retourne le nom de la table correspondante a la classe
	 * @return le nom de la table
	 */
     public static String getNomTable() 
     {  
        return "tj_prelevementlot_prl" ;
     }
      
	/**
	 * Retourne le nom de l'identifiant caract�re de la table
	 * @return un vecteur de string de longueur 1 contenant le nom de
	 *         l'identifiant de la table
	 */
	public static String[] getNomID() {
		String[] ret = new String[1];
		ret[0] = "pre_typeprelevement";
		return ret;
	} // end getNomID

	/**
	 * Retourne la valeur de l'indenfiant string de la classe
	 * @return un vecteur d'objet de longueur 1 contenant le code
	 */
	public Object[] getValeurID() {
		Object[] ret = new Object[1];
		ret[0] = this.getTypePrelevement();
		return ret;
	} // end getValeurID  
	
    /**
  	 * Retourne le nom des attributs de la table correspondant a la classe 
  	 * 
  	 * @return un Vector de String avec les attributs dans l'ordre de la table
  	 *         dans la BD
  	 */
  	public static ArrayList<String> getNomAttributs() {
  		// impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
  		// est inconnu
  		ArrayList<String> ret = new ArrayList<String>();
  		ret.add("pre_typeprelevement");
  		ret.add("pre_definition");
  	    return ret;
  	} // end getNomAttributs

  	/**
  	 * Retourne la valeur des attributs de la classe
  	 * 
  	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
  	 *         dans la BD
  	 */
  	public ArrayList<Object> getValeurAttributs() {
  		ArrayList<Object> ret = new ArrayList<Object>();
  		ret.add(this.getTypePrelevement());
  		ret.add(this.getDefinition());
  		return ret;
  	} // end getValeurAttributs
}
