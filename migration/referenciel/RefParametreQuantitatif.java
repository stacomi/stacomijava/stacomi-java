/*
 **********************************************************************
 *
 * Nom fichier :        RefParametreQuantitatif.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */


package migration.referenciel;


/**
 * Parametre biologique ou environnementla quantitatif
 */
public class RefParametreQuantitatif extends RefParametre {

    
  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////
    
    
    
  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    
    
/**
 * Construit une reference avec un code mais sans libelle
 * @param _code le code
 */    
    public RefParametreQuantitatif(String _code) {
        this(_code, null, null, null, null) ;
    } // end RefParametreQuantitatif
        
    
/**
 * Construit une reference avec un code, un nom et une unite
 * @param _code le code
 * @param _nom le nom
 * @param _unite l'unite
 */    
    public RefParametreQuantitatif(String _code, String _nom, String _unite) {
        this(_code, _nom, _unite, null, null) ;
    } // end RefParametreQuantitatif
    
    
/**
 * Construit une reference avec un code et tous les autres attributs
 * @param _code le code
 * @param _nom le nom
 * @param _unite l'unite
 * @param _nature la nature (BIOLOGIQUE | ENVIRONNEMENTAL)
 * @param _definition la definition
 */    
    public RefParametreQuantitatif(String _code, String _nom, String _unite, String _nature, String _definition) {
        
        super(_code, _nom, _unite, _nature, _definition) ;
        
    } // end RefParametreQuantitatif  
    
    
    
  ///////////////////////////////////////
  // operations heritees
  ///////////////////////////////////////
    
    public void insertObjet() {
        // super.insertObjet("ref.tr_parametrequantitatif_qan") ;
        
        // A FAIRE             !!!!!!!!!!!!!!!!!!!!!!!!!
        
    }
    
    
    public void majObjet() {
        //super.majObjet("ref.tr_parametrequantitatif_qan") ;
       
        // A FAIRE             !!!!!!!!!!!!!!!!!!!!!!!!!
        
    }
    
    
    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////    
    
    
    
 } // end RefParametreQuantitatif



