/*
 **********************************************************************
 *
 * Nom fichier :        SousListeMarquages.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             S�bastien Laigre
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   15 mai 2009
 * Compatibilite :      Java6/ Windows XP/ PostgreSQL 8.3
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;

import java.sql.ResultSet;
import java.sql.SQLException;

import migration.Lot;
import migration.Marquage;
import migration.Marque;
import migration.OperationMarquage;

import commun.*;

/**
 * Sous liste pour les marquages d'un lot
 * @author Samuel Gaudey
 */
public class SousListeMarquages extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////  

	/**
	 * 
	 */
	private static final long serialVersionUID = -310604924990618752L;

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * @param _objetDeRattachement l'objet de rattachement
	 */
	public SousListeMarquages(Lot _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeMarquages

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	public void chargeSansFiltre() throws SQLException, ClassNotFoundException
	{
		// A FAIRE
	}

	public void chargeSansFiltreDetails() throws SQLException, ClassNotFoundException
	{
		ResultSet rs = null;
		
		String sql = 
				"SELECT act_lot_identifiant, act_mqe_reference, act_action, act_commentaires, " +
						"mqe_loc_code, mqe_nmq_code, mqe_omq_reference, mqe_commentaires, loc_libelle, nmq_libelle " +
				"FROM " + ConnexionBD.getSchema() + "tj_actionmarquage_act " +
						"JOIN " + ConnexionBD.getSchema() + "t_marque_mqe mq ON mq.mqe_reference=act_mqe_reference " +
						"JOIN ref.tr_localisationanatomique_loc loc ON mqe_loc_code=loc.loc_code " +
						"JOIN ref.tr_naturemarque_nmq nmq ON mqe_nmq_code=nmq.nmq_code " +
				"WHERE act_lot_identifiant = '" + ((Lot)getObjetDeRattachement()).getIdentifiant() + "';";
		
		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		
		// parcours de touts les lignes r�sultat
		while(rs.next())
		{
			Integer act_lot_identifiant = rs.getInt("act_lot_identifiant");
			String act_mqe_reference = rs.getString("act_mqe_reference");
			String act_action = rs.getString("act_action");
			String act_commentaires = rs.getString("act_commentaires");

			String mqe_loc_code = rs.getString("mqe_loc_code");
			String loc_libelle = rs.getString("loc_libelle");
			RefLocalisation loc = new RefLocalisation(mqe_loc_code, loc_libelle, "MARQUE");
			String mqe_nmq_code = rs.getString("mqe_nmq_code");
			String nmq_libelle = rs.getString("nmq_libelle");
			RefNatureMarque nat = new RefNatureMarque(mqe_nmq_code, nmq_libelle);
			String mqe_omq_reference = rs.getString("mqe_omq_reference");
			OperationMarquage op = new OperationMarquage(mqe_omq_reference);
			String mqe_commentaires = rs.getString("mqe_commentaires");
			Marque mq = new Marque(act_mqe_reference,loc,nat,op,mqe_commentaires);

			Lot l = new Lot(rs.getInt("act_lot_identifiant"), null, null, null, null, null);
			
			Marquage marquage = new Marquage(l, mq, act_action);
			marquage.setCommentaires(act_commentaires);
			
			this.put(act_lot_identifiant+act_mqe_reference, marquage);
		}
	}

	protected void chargeObjet(Object _objet) {
		// A FAIRE
	}

	/**
	 * Retourne l'identifiant du lot
	 * @return Tableau de taille 1 : (0) identifiant
	 */
	public String[] getObjetDeRattachementID() {

		// L'identifiant de Lot est : identifiant
		String ret[] = new String[1];
		ret[0] = ((Lot) super.objetDeRattachement).getIdentifiant().toString();

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Does ...??????????????????????????????
	 * 
	 */
	public void chargeFiltreDetails() {
		// your code here
	} // end chargeFiltreDetails   
	
	
	/**
	 * Chargement des marques pour une op�ration de marquage donn�e
	 * @param ope l'op�ration de marquage
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public void chargeFiltreOperationMarquage(OperationMarquage ope) throws SQLException, ClassNotFoundException
	{
		ResultSet rs = null;
		
		String refOpe = ope.getReference();
		
		String sql = "SELECT mqe_reference " +
				"FROM " + ConnexionBD.getSchema() + "t_marque_mqe " +
				"WHERE mqe_omq_reference='"+refOpe+"';";
		
		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		
		while(rs.next())
		{
			String mqe_reference = rs.getString("mqe_reference");
			
			Marque m = new Marque(mqe_reference);
			
			this.put(mqe_reference, m);
		}
	}

} // end SousListeMarquages

