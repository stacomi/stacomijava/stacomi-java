/*
 **********************************************************************
 *
 * Nom fichier :        SousListeOperations.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */
package migration.referenciel;

import java.sql.ResultSet;
//import java.util.Calendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import commun.ConnexionBD;
import commun.Erreur;
import commun.SousListe;
import infrastructure.DC;
import migration.Operation;

/**
 * Sous liste pour les operations d'un DC
 * 
 * @author Samuel Gaudey
 */
public class SousListeOperations extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
		 * 
		 */
	private static final long serialVersionUID = -6717614767563557036L;

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * 
	 * @param _objetDeRattachement
	 *            l'objet de rattachement
	 */
	public SousListeOperations(DC _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeDF

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	public void chargeSansFiltre() {

		// A FAIRE

	}

	public void chargeSansFiltreDetails() {

		// A FAIRE

	}

	/**
	 * Retourne l'identifiant du DC
	 * 
	 * @return Tableau de taille 1 : (0) identifiant
	 */
	public String[] getObjetDeRattachementID() {

		// L'identifiant de DC est : identifiant
		String ret[] = new String[1];
		ret[0] = ((DC) super.objetDeRattachement).getIdentifiant().toString();

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Charge dans la sous-liste les operations du DC pour une annee precise, ou
	 * pour toutes les annees
	 * 
	 * @param _annee
	 *            l'annee a rechercher, ou null pour charger toutes les
	 *            operations du DC
	 * @throws Exception
	 */
	public void chargeFiltre(Integer _annee) throws Exception {
		Operation ope = null;
		ResultSet rs = null;
		DC dc = ((DC) super.objetDeRattachement);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date datefin = new Date();
		Date datedebut = new Date();
		Calendar calAnnee = Calendar.getInstance();
		Calendar calAnneeSuivante = Calendar.getInstance();
		String complementWhere = "";

		if (_annee != null) {

			calAnnee.set(Calendar.YEAR, _annee.intValue());
			calAnnee.set(Calendar.MONTH, Calendar.JANUARY);
			calAnnee.set(Calendar.HOUR_OF_DAY, 0);
			calAnnee.set(Calendar.MINUTE, 0);
			calAnnee.set(Calendar.SECOND, 0);
			calAnnee.set(Calendar.DAY_OF_MONTH, 1);

			// cette ecriture peut aussi s'�crire :
			calAnneeSuivante.set(_annee.intValue(), 0, 1, 0, 0, 0);
			calAnneeSuivante.roll(Calendar.YEAR, 1);

			datedebut.setTime(calAnnee.getTimeInMillis());

			datefin.setTime(calAnneeSuivante.getTimeInMillis());
			complementWhere = " AND ope_date_debut >='" + dateFormat.format(datedebut) + "' AND ope_date_debut <'"
					+ dateFormat.format(datefin) + "'";
		}

		// Extraction des enregistrements concernes
		// String sql = "SELECT ope_identifiant, ope_dic_identifiant,
		// ope_date_debut, ope_date_fin, ope_organisme, ope_operateur,
		// ope_commentaires FROM t_operation_ope WHERE ope_date_fin = (SELECT
		// MAX(ope_date_fin) " + complementWhere + " FROM t_operation_ope) ;" ;
		String sql = "SELECT ope_identifiant, ope_date_debut, ope_date_fin " + "FROM " + ConnexionBD.getSchema()
				+ "t_operation_ope " + "WHERE ope_dic_identifiant='" + dc.getIdentifiant().toString() + "' "
				+ complementWhere + " ORDER BY ope_date_debut;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur les enregistrements trouves
		while (rs.next()) {

			// Lecture des champs dans le ResultSet ;
			Integer identifiant = new Integer(rs.getInt("ope_identifiant"));

			// Pour la date, utilisation de timestamp, sinon ne recupere pas les
			// heures, minutes, secondes
			// autre possibilite, mais deconseille : Date dateDebut=
			// rs.getTimestamp("ope_date_debut") ;
			Date dateDebut = new Date(rs.getTimestamp("ope_date_debut").getTime());
			Date dateFin = new Date(rs.getTimestamp("ope_date_fin").getTime());
			// Calendar dateDebut = Calendar.getInstance();
			// dateDebut.setTime(rs.getTime("ope_date_debut")) ;
			// Calendar dateFin = Calendar.getInstance();
			// dateFin.setTime(rs.getTime("ope_date_fin")) ;
			// //String organisme = rs.getString("ope_organisme") ;
			// String operateur = rs.getString("ope_operateur") ;
			// String commentaires = rs.getString("ope_commentaires") ;

			// Initialisation de l'objet d'apres les champs lus et ajout a la
			// liste
			ope = new Operation(dc, identifiant, dateDebut, dateFin, null, null, null, null);

			this.put(identifiant, ope);
		}

	} // end chargeFiltre

	/**
	 * Charge une seule operation puis appel de la methode
	 * chargesansfiltredetail sur les lots cette m�thode doit aussi afficher les
	 * d�tails du lot y compris les marquages et les pathologies
	 * 
	 * @see commun.SousListe#chargeObjet(java.lang.Object)
	 */
	public void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		// Si l'identifiant de l'objet n'est pas definit, erreur
		Integer identifiant = ((Operation) _objet).getIdentifiant();
		if (identifiant == null) {
			throw new NullPointerException(Erreur.I1000);
		}

		// Extraction de l'enregistrement concerne
		String sql = "SELECT ope_dic_identifiant, ope_date_debut, ope_date_fin, ope_organisme, ope_operateur, ope_commentaires "
				+ "FROM " + ConnexionBD.getSchema() + "t_operation_ope " + "WHERE ope_identifiant = '" + identifiant
				+ "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// En principe, il y a un et un seul enregistrement
		if (rs.first() == false) {
			throw new Exception(Erreur.I1001);
		}

		// Lecture des champs dans le ResultSet ;
		DC dc = new DC(new Integer(rs.getInt("ope_dic_identifiant")));

		// Pour la date, utilisation de timestamp, sinon ne recupere pas les
		// heures, minutes, secondes
		// autre possibilite, mais deconseille : Date dateDebut=
		// rs.getTimestamp("ope_date_debut") ;
		Date dateDebut = new Date(rs.getTimestamp("ope_date_debut").getTime());
		Date dateFin = new Date(rs.getTimestamp("ope_date_fin").getTime());
		// @deprecated passage au Calendar (C�dric)
		// Calendar dateDebut = Calendar.getInstance();
		// dateDebut.setTime(rs.getTime("ope_date_debut")) ;
		// Calendar dateFin = Calendar.getInstance();
		// dateFin.setTime(rs.getTime("ope_date_fin")) ;
		String organisme = rs.getString("ope_organisme");
		String operateur = rs.getString("ope_operateur");
		String commentaires = rs.getString("ope_commentaires");

		// Initialisation de l'objet d'apres les champs lus

		((Operation) _objet).setDC(dc);
		((Operation) _objet).setDateDebut(dateDebut);
		((Operation) _objet).setDateFin(dateFin);
		((Operation) _objet).setOrganisme(organisme);
		((Operation) _objet).setOperateur(operateur);
		((Operation) _objet).setCommentaires(commentaires);

		SousListeLots lesLots = new SousListeLots((Operation) _objet);

		// charge les lots de cette operation, y compris les dependance
		// (caracteristiques, marques, pathologies)
		lesLots.chargeFiltreDetails();

		((Operation) _objet).setLesLots(lesLots);
	}

	/**
	 * Charge dans la sous-liste la derniere operation du DC la premi�re et la
	 * derni�re op�ration sert pour le calcul de la valeur des ann�es
	 * 
	 * @return Un vecteur de deux entiers avec l'annee de d�but et l'annee de
	 *         fin
	 * @throws Exception
	 */
	public Integer[] chargeRangeOperation() throws Exception {
		Operation ope = null;
		ResultSet rs = null;
		DC dc = ((DC) super.objetDeRattachement);

		// Extraction des enregistrements concernes
		// String sql = "SELECT ope_identifiant, ope_dic_identifiant,
		// ope_date_debut, ope_date_fin, ope_organisme, ope_operateur,
		// ope_commentaires FROM t_operation_ope WHERE ope_date_fin = (SELECT
		// MAX(ope_date_fin) " + complementWhere + " FROM t_operation_ope) ;" ;
		String sql = "SELECT  extract(Year from min(ope_date_debut)) as minyear, extract(Year from max(ope_date_fin)) as maxyear  "
				+ "FROM " + ConnexionBD.getSchema() + "t_operation_ope " + "WHERE ope_dic_identifiant='"
				+ dc.getIdentifiant().toString() + "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Il faut au moins deux enregistrement

		boolean isEmpty = !rs.next();
		if (isEmpty) {
			// cette condition existe lors du premier passage dans l'interface
			// de cr�ation des masques
			throw new Exception("Pas d'op�rations pour le dc s�lectionn�");
		} else {
			Integer[] rangeyear = new Integer[2];
			rangeyear[0] = rs.getInt("minyear");
			rangeyear[1] = rs.getInt("maxyear");
			return (rangeyear);
		}

	} // end chargeFiltreRangeOperation

} // end SousListeOperations
