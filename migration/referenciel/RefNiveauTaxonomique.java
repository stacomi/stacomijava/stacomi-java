/*
 **********************************************************************
 *
 * Nom fichier :        RefNiveauTaxonomique.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration.referenciel;

import commun.*;
import java.util.zip.DataFormatException;

/**
 * Niveau taxonomique
 * @author Samuel Gaudey
 */
public class RefNiveauTaxonomique extends Ref {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	private String mnemonique;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une reference avec un code mais sans libelle et sans mnemonique
	 * @param _code le code
	 */
	public RefNiveauTaxonomique(String _code) {
		this(_code, null, null);
	} // end RefNiveauTaxonomique

	/**
	 * Construit une reference avec un code et un libelle
	 * @param _code le code
	 * @param _libelle le libelle
	 */
	public RefNiveauTaxonomique(String _code, String _libelle) {
		this(_code, _libelle, null);
	} // end RefNiveauTaxonomique

	/**
	 * Construit une reference avec un code, un libelle et une mnemonique
	 * @param _code le code
	 * @param _libelle le libelle
	 * @param _mnemonique la mnemonique
	 */
	public RefNiveauTaxonomique(String _code, String _libelle,
			String _mnemonique) {
		super(_code, _libelle);

		this.setMnemonique(_mnemonique);

	} // end RefNiveauTaxonomique

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	public void insertObjet() {

		// executer requete
	}

	public void majObjet() {

		// executer requete       
	}

	// Surcharge de la methode de la super classe pour verifier la mnemonique
	public boolean verifAttributs() throws DataFormatException {

		// verification du code et du libelle
		boolean ret = super.verifAttributs();

		// verification de presence de la mnemonique
		if ((this.mnemonique == null) || (this.mnemonique == "")) {
			throw new DataFormatException(Erreur.S1002);
		}

		return ret;
	} // end verifAttributs

	///////////////////////////////////////
	// operations
	///////////////////////////////////////    

	/**
	 * Retourne la mnemonique
	 * @return la mnemonique
	 */
	public String getMnemonique() {
		return this.mnemonique;
	} // end getMnemonique        

	/**
	 * Inistialise la mnemonique
	 * @param _mnemonique la mnemonique
	 */
	public void setMnemonique(String _mnemonique) {
		this.mnemonique = _mnemonique;
	} // end setMnemonique        

} // end RefNiveauTaxonomique

