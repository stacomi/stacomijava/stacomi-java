/*
 **********************************************************************
 *
 * Nom fichier :        Marque.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import commun.*;
import migration.referenciel.*;

/**
 * Marque
 * @author C�dric Briand
 */
public class Marque implements IBaseDonnees {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	private String reference;

	private RefLocalisation localisation;

	private RefNatureMarque nature;

	private OperationMarquage operationMarquage;

	private String commentaires;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une marque avec uniquement une reference 
	 * @param _reference la reference de la marque
	 */
	public Marque(String _reference) {
		this(_reference, null, null, null, null);
	} // end Marque

	/**
	 * Construit une marque avec tous ses attributs
	 * @param _reference r�f�rence de la marque
	 * @param _localisation localisation
	 * @param _nature nature
	 * @param _operation op�ration
	 * @param _commentaires commentaires
	 */
	public Marque(String _reference, RefLocalisation _localisation,
			RefNatureMarque _nature, OperationMarquage _operation,
			String _commentaires) {

		this.setReference(_reference);
		this.setLocalisation(_localisation);
		this.setNature(_nature);
		this.setOperationMarquage(_operation);
		this.setCommentaires(_commentaires);

	} // end Marque   

	///////////////////////////////////////
	// interfaces
	///////////////////////////////////////

	/**
	 * Insertion d'une marque dans la BDD
	 */
	public void insertObjet() throws SQLException, ClassNotFoundException
	{
		String table = Marque.getNomTable();
		ArrayList<String> nomAttributs = Marque.getNomAttributs();
		ArrayList<Object> valeurAttributs = this.getValeurAttributs();
		
		ConnexionBD.getInstance().executeInsert(table, nomAttributs, valeurAttributs);
	}

	/**
	 * Mise � jour d'une marque dans la BDD
	 */
	public void majObjet() throws SQLException, ClassNotFoundException
	{
		String table = Marque.getNomTable();
		ArrayList<String> nomAttributs = Marque.getNomAttributs();
		ArrayList<Object> valeurAttributs = this.getValeurAttributs();
		
		String[] nomID = Marque.getNomID();
		Object[] valeurID = this.getValeurID();
		
		ConnexionBD.getInstance().executeUpdate(table, nomID, valeurID, nomAttributs, valeurAttributs);
	}

	/**
	 * Suppression d'une marque dans la BDD
	 */
	public void effaceObjet() throws SQLException, ClassNotFoundException
	{
		String table = Marque.getNomTable();
		String[] nomID = Marque.getNomID();
		Object[] valeurID = this.getValeurID();
		
		ConnexionBD.getInstance().executeDelete(table, nomID, valeurID);
	}

	/**
	 * V�rification des attributs d'une marque
	 */
	public boolean verifAttributs() throws DataFormatException
	{
		// v�rification de la r�f�rence de la marque
		if(!Verification.isText(this.reference, 1, 30, true))
			throw new DataFormatException(Erreur.S18000);
			
		// v�rification du code de la localisation
		if(!Verification.isText(this.localisation.getCode(), 1, 4, true))
			throw new DataFormatException(Erreur.S18001);
		
		// v�rification du code de la nature de la marque
		if(!Verification.isText(this.nature.getCode(), 1, 4, true))
			throw new DataFormatException(Erreur.S18002);
		
		// v�rification de la r�f�rence de l'op�ration de marquage
		if(!Verification.isText(this.operationMarquage.getReference(), 1, 30, false))
			throw new DataFormatException(Erreur.S18003);
		
		// v�rification du commentaire
		if(!Verification.isText(this.commentaires, 0, false))
			throw new DataFormatException(Erreur.S18004);

		return true;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Retourne la reference de la marque
	 * @return la reference
	 */
	public String getReference() {
		return this.reference;
	} // end getReference        

	/**
	 * Retourne la localisation de la marque sur le taxon
	 * @return la localisation
	 */
	public RefLocalisation getLocalisation() {
		return this.localisation;
	} // end getLocalisation        

	/**
	 * Retourne la nature de la marque
	 * @return la nature
	 */
	public RefNatureMarque getNature() {
		return this.nature;
	} // end getNature        

	/**
	 * Retourne l'operation de marquage a laquelle la marque est liee
	 * @return l'operation de marquage
	 */
	public OperationMarquage getOperationMarquage() {
		return this.operationMarquage;
	} // end getOperationMarquage     

	/**
	 * Retourne les commentaires sur la marque
	 * @return les commentaires
	 */
	public String getCommentaires() {
		return this.commentaires;
	} // end getCommentaires        

	/**
	 * Initialise la reference de la marque
	 * @param _reference la reference
	 */
	public void setReference(String _reference) {
		this.reference = _reference;
	} // end setReference        

	/**
	 * Initialise la localisation de la marque sur le taxon
	 * @param _localisation la localisation
	 */
	public void setLocalisation(RefLocalisation _localisation) {
		this.localisation = _localisation;
	} // end setLocalisation        

	/**
	 * Initialise la nature de la marque
	 * @param _nature la nature
	 */
	public void setNature(RefNatureMarque _nature) {
		this.nature = _nature;
	} // end setNature        

	/**
	 * Initialise l'operation de marquage a laquelle la marque est liee
	 * @param _operationMarquage l'operation de marquage
	 */
	public void setOperationMarquage(OperationMarquage _operationMarquage) {
		this.operationMarquage = _operationMarquage;
	} // end setOperationMarquage        

	/**
	 * Initialise les commentaires sur la marque
	 * @param _commentaires les commentaires
	 */
	public void setCommentaires(String _commentaires) {
		this.commentaires = _commentaires;
	} // end setCommentaires        

	/**
	 * Retourne le nom de la table correspondante
	 * @return le nom de la table correspondante
	 */
	public static String getNomTable() {
		return "t_marque_mqe";
	}
	
	/**
	 * Retourne le nom de la cl� primaire
	 * @return le nom de la cl� primaire
	 */
	public static String[] getNomID()
	{
		String[] res = new String[1];
		res[0] = "mqe_reference";
		return res;
	}
	
	/**
	 * Retourne la valeur de la cl� primaire
	 * @return la valeur de la cl� primaire
	 */
	public Object[] getValeurID() {
		Object[] res = new Object[1];
		res[0] = this.getReference();
		return res;
	} // end getValeurID

	/**
	 * Acc�s aux noms des colonnes
	 * @return les noms des colonnes
	 */
	public static ArrayList<String> getNomAttributs()
	{
		ArrayList<String> res = new ArrayList<String>();
		res.add("mqe_reference");
		res.add("mqe_loc_code");
		res.add("mqe_nmq_code");
		res.add("mqe_omq_reference");
		res.add("mqe_commentaires");
		res.add("mqe_org_code");
		return res;
	}

	/**
	 * Acc�s aux valeurs des attributs
	 * @return les valeurs des attributs
	 */
	public ArrayList<Object> getValeurAttributs() 
	{
		ArrayList<Object> res = new ArrayList<Object>();
		res.add(this.getReference());
		res.add(this.getLocalisation().getCode());
		res.add(this.getNature().getCode());
		res.add(this.getOperationMarquage().getReference());
		res.add(this.getCommentaires());
		return res;
	}
	
	public String toString()
	{
		return this.reference;
	}
	
} // end Marque

