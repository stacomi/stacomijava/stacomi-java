/*
 **********************************************************************
 *
 * Nom fichier :        Operation.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
//import java.util.List;
//import java.util.Vector;
import java.util.zip.DataFormatException;

import org.apache.commons.lang3.StringUtils;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;
import infrastructure.DC;
import migration.referenciel.SousListeLots;

/**
 * Operation de controle des migrations
 * 
 * @author Samuel Gaudey
 */
public class Operation implements IBaseDonnees {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	private DC DC;
	private Integer identifiant;
	private Date dateDebut;
	private Date dateFin;
	private String organisme;
	private String operateur;
	private String commentaires;
	private SousListeLots lesLots;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une operation avec uniquement un identifiant, une date de
	 * debut, et une date de fin
	 * 
	 * @param _identifiant
	 * @param _dateDebut
	 * @param _dateFin
	 */
	public Operation(Integer _identifiant, Date _dateDebut, Date _dateFin) {
		this(null, _identifiant, _dateDebut, _dateFin, null, null, null, null);
	} // end Operation

	/**
	 * Construit une operation avec tous ses attributs
	 * 
	 * @param _DC
	 * @param _identifiant
	 * @param _dateDebut
	 * @param _dateFin
	 * @param _organisme
	 * @param _operateur
	 * @param _commentaires
	 * @param _lesLots
	 */
	public Operation(DC _DC, Integer _identifiant, Date _dateDebut, Date _dateFin, String _organisme, String _operateur,
			String _commentaires, SousListeLots _lesLots) {

		this.setDC(_DC);
		this.setIdentifiant(_identifiant);
		this.setDateDebut(_dateDebut);
		this.setDateFin(_dateFin);
		this.setOrganisme(_organisme);
		this.setOperateur(_operateur);
		this.setCommentaires(_commentaires);
		this.setLesLots(_lesLots);

	} // end Operation

	///////////////////////////////////////
	// interfaces
	///////////////////////////////////////

	public void insertObjet() throws SQLException, ClassNotFoundException {
		// identifiant = null pour pouvoir utiliser la s�quence
		// t_operation_ope_ope_identifiant_seq
		this.identifiant = null;

		String table = Operation.getNomTable();
		ArrayList nomAttributs = Operation.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();

		System.out.println("Operation : insertObjet()");

		ConnexionBD.getInstance().executeInsert(table, nomAttributs, valAttributs);

		// initialise l'identifiant de l'operation avec le nombre qui vient
		// d'etre autoincremente
		this.identifiant = ConnexionBD.getInstance().getGeneratedKey(Operation.getNomSequences()[0]);
	}

	public void majObjet() throws SQLException, ClassNotFoundException {

		String table = Operation.getNomTable();
		String[] nomAttributsSelection = Operation.getNomID();
		Object[] valAttributsSelection = this.getValeurID();
		ArrayList nomAttributs = Operation.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();

		System.out.println("Operation : majObjet()");
		ConnexionBD.getInstance().executeUpdate(table, nomAttributsSelection, valAttributsSelection, nomAttributs,
				valAttributs);
	}

	public void effaceObjet() throws SQLException, ClassNotFoundException {

		String table = Operation.getNomTable();
		String[] nomAttributsSelection = Operation.getNomID();
		Object[] valAttributsSelection = this.getValeurID();

		System.out.println("Operation : effaceObjet()");

		ConnexionBD.getInstance().executeDelete(table, nomAttributsSelection, valAttributsSelection);
	}

	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;

		if (!Verification.isDC(this.DC, true)) {
			throw new DataFormatException(Erreur.S3000);
		}
		if (!Verification.isInteger(this.identifiant, false)) {
			throw new DataFormatException(Erreur.S3010);
		}
		if (!Verification.isDateInf(this.dateDebut, this.dateFin, true, true)) {
			throw new DataFormatException(Erreur.S3011);
		}
		if (!Verification.isText(this.organisme, 1, 35, false)) {
			throw new DataFormatException(Erreur.S3005);
		}
		if (!Verification.isText(this.operateur, 1, 35, false)) {
			throw new DataFormatException(Erreur.S3005);
		}
		if (!Verification.isText(this.commentaires, false)) {
			throw new DataFormatException(Erreur.S3007);
		}
		// Rien a verifier
		// this.lesLots ;

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Retourne le nom de la table correspondant a la classe
	 * 
	 * @return un le nom
	 */
	public static String getNomTable() {
		return "t_operation_ope";
	}

	/**
	 * Retourne le nom des attributs identifiants de la table correspondant a
	 * cette classe
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public static String[] getNomID() {

		String[] ret = new String[1];

		ret[0] = "ope_identifiant";

		return ret;
	} // end getNomID

	/**
	 * Retourne les noms des sequences utilisee pour l'incrementation des
	 * attributs de la table
	 * 
	 * @return un Tableau de chaines avec les nom des sequences
	 */
	public static String[] getNomSequences() {

		String[] ret = new String[1];

		ret[0] = "t_operation_ope_ope_identifiant_seq";

		return ret;
	} // end getNomSequences

	/**
	 * Retourne la valeur des attributs identifiants de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public Object[] getValeurID() {

		Object[] ret = new Object[1];

		ret[0] = this.identifiant;

		return ret;
	} // end getValeurID

	/// **
	// * Retourne le nom des attributs de la table correspondant a cette classe
	// * @return un Vector de String avec les attributs dans l'ordre de la table
	/// dans la BD
	// */
	// public static Vector<String> getNomAttributs() {
	// // impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
	/// est inconnu
	//
	// Vector<String> ret = new Vector<String>() ;
	//
	// ret.add("ope_identifiant") ; // autoincremente
	// ret.add("ope_dic_identifiant") ;
	// ret.add("ope_date_debut") ;
	// ret.add("ope_date_fin") ;
	// ret.add("ope_organisme") ;
	// ret.add("ope_operateur") ;
	// ret.add("ope_commentaires") ;
	//
	// return ret ;
	// } // end getNomAttributs
	//
	/**
	 * Retourne le nom des attributs de la table correspondant a cette classe
	 * 
	 * @return un ArrayList de String avec les attributs dans l'ordre de la
	 *         table dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		// impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
		// est inconnu

		ArrayList<String> ret = new ArrayList<String>();

		ret.add("ope_identifiant"); // autoincremente
		ret.add("ope_dic_identifiant");
		ret.add("ope_date_debut");
		ret.add("ope_date_fin");
		ret.add("ope_organisme");
		ret.add("ope_operateur");
		ret.add("ope_commentaires");
		ret.add("ope_org_code");

		return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de cette classe
	 * 
	 * @return un Arraylist d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD (sous format Arrylist)
	 */
	public ArrayList<Object> getValeurAttributs() {

		ArrayList<Object> ret = new ArrayList<Object>();

		ret.add(this.identifiant); // normalement null car autoincremente
		if (this.DC != null) {
			ret.add(this.DC.getIdentifiant());
		} else {
			ret.add(null);
		}
		ret.add(new Timestamp(this.dateDebut.getTime()));
		ret.add(new Timestamp(this.dateFin.getTime()));
		ret.add(this.organisme);
		ret.add(this.operateur);
		ret.add(this.commentaires);

		return ret;
	} // end getValeurAttributs

	/**
	 * Retourne le DC sur lequel l'operation a eut lieu
	 * 
	 * @return le DC
	 */
	public DC getDC() {
		return this.DC;
	} // end getDC

	/**
	 * Retourne l'identifiant de l'operation
	 * 
	 * @return l'identifiant
	 */
	public Integer getIdentifiant() {
		return this.identifiant;
	} // end getIdentifiant

	/**
	 * Retourne la date de debut de l'operation
	 * 
	 * @return la date de debut
	 */
	public Date getDateDebut() {
		return this.dateDebut;
	} // end getDateDebut

	/**
	 * Retourne la date de fin de l'operation
	 * 
	 * @return la date de fin
	 */
	public Date getDateFin() {
		return this.dateFin;
	} // end getDateFin

	/**
	 * Retourne l'organisme qui a realise l'operation
	 * 
	 * @return l'organisme
	 */
	public String getOrganisme() {
		return this.organisme;
	} // end getOrganisme

	/**
	 * Retourne l'operateur qui a realise l'operation
	 * 
	 * @return l'operateur
	 */
	public String getOperateur() {
		return this.operateur;
	} // end getOperateur

	/**
	 * Retourne les commentaires sur l'operation
	 * 
	 * @return les commentaires
	 */
	public String getCommentaires() {
		return this.commentaires;
	} // end getCommentaires

	/**
	 * Retourne la sous liste des lots de l'operation
	 * 
	 * @return la sous liste des lots
	 */
	public SousListeLots getLesLots() {
		return this.lesLots;
	} // end getLesLots

	/**
	 * Initialise le DC sur lequel l'operation a eut lieu
	 * 
	 * @param _DC
	 *            le DC
	 */
	public void setDC(DC _DC) {
		this.DC = _DC;
	} // end setDC

	/**
	 * Initialise lidentifiant de l'operation
	 * 
	 * @param _identifiant
	 *            l'identifiant
	 */
	public void setIdentifiant(Integer _identifiant) {
		this.identifiant = _identifiant;
	} // end setIdentifiant

	/**
	 * Initialise la date de debut de l'operation
	 * 
	 * @param _dateDebut
	 *            la date de debut (format time stamp
	 */
	public void setDateDebut(Date _dateDebut) {
		this.dateDebut = _dateDebut;
	} // end setDateDebut

	/**
	 * Initialise la date de fin de l'operation
	 * 
	 * @param _dateFin
	 *            la date de fin (timestamp)
	 */
	public void setDateFin(Date _dateFin) {
		this.dateFin = _dateFin;
	} // end setDateFin

	/**
	 * Initialise l'organisme qui a realise l'operation
	 * 
	 * @param _organisme
	 *            l'organisme
	 */
	public void setOrganisme(String _organisme) {
		this.organisme = _organisme;
	} // end setOrganisme

	/**
	 * Initialise l'operateur qui a realise l'operation
	 * 
	 * @param _operateur
	 *            l'operateur
	 */
	public void setOperateur(String _operateur) {
		this.operateur = _operateur;
	} // end setOperateur

	/**
	 * Initialise les commentaires sur l'operation
	 * 
	 * @param _commentaires
	 *            les commentaires
	 */
	public void setCommentaires(String _commentaires) {
		this.commentaires = _commentaires;
	} // end setCommentaires

	/**
	 * Initialise la sous liste des lots de l'operation
	 * 
	 * @param _lesLots
	 *            la sous liste des lots
	 */
	public void setLesLots(SousListeLots _lesLots) {
		this.lesLots = _lesLots;
	}

	/**
	 * Retourne les attributs de l'operation sous forme textuelle
	 * 
	 * @return les attributs de l'operation sous forme textuelle
	 */
	public String toString() {

		SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

		String ret = simpleDate.format(this.dateDebut) + " -> " + simpleDate.format(this.dateFin);

		/*
		 * // precise le nombre de lots, si disponible try { ret = ret + " (" +
		 * this.getLesLots().size() +"lots )" ; } catch (Exception e) { // ne
		 * rajoute rien }
		 **/

		return ret;
	}

	/**
	 * Retourne les attributs de l'operation sous forme textuelle, avec plus de
	 * details
	 * 
	 * @return les attributs de l'operation sous forme textuelle, avec plus de
	 *         details
	 */

	public String toStringDetails() {

		String ret;

		// SimpleDateFormat simpleDate = new SimpleDateFormat("EEEE dd/MM/yy
		// HH:mm:ss ") ;
		SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yy HH:mm");

		// Recuperere les infos a afficher pour l'operation
		try {
			String libStation = StringUtils.abbreviate(this.getDC().getDF().getOuvrage().getStation().getLibelle(), 25);
			// String libOuvrage = this.getDC().getDF().getOuvrage().getCode() ;

			// String libDf = this.getDC().getDF().getCode(). ;
			String libDc = StringUtils.abbreviate(this.getDC().getCode(), 45);
			String libDateDeb = simpleDate.format(this.getDateDebut());
			String libDateFin = simpleDate.format(this.getDateFin());

			ret = libStation + " | "
			// + libOuvrage + ", "
			// + libDf+ " | "
					+ libDc + " (" + libDateDeb + " \u21D4 " + libDateFin + ")";

			return ret;
		} catch (Exception e) {
			System.out.println(Erreur.B1020 + " ou " + Erreur.B1021);
		}

		return null;
	}

	// end setLesLots

} // end Operation
