/*
 **********************************************************************
 *
 * Nom fichier :        Lot.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package migration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import org.apache.commons.lang3.StringUtils;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;
import migration.referenciel.RefDevenir;
import migration.referenciel.RefStade;
import migration.referenciel.RefTaxon;
import migration.referenciel.RefTypeQuantite;
import migration.referenciel.SousListeCaracteristiques;
import migration.referenciel.SousListeMarquages;
import migration.referenciel.SousListePathologieConstatee;
import migration.referenciel.SousListePrelevement;

//import java.util.Vector ;

/**
 * Lot pour une operation de controle
 * 
 * @author Samuel Gaudey
 */
public class Lot implements IBaseDonnees {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	private Integer identifiant;

	private Operation operation;

	private RefTaxon taxon;

	private RefStade stadeDeveloppement;

	private Float effectif;

	private Float valeurQuantite;

	private RefTypeQuantite typeQuantite;

	private String methodeObtention;

	private Lot lotParent;

	private RefDevenir devenir;

	private String commentaires;

	private SousListePathologieConstatee lesPathologies;

	private SousListeMarquages lesMarquages;

	private SousListeCaracteristiques lesCaracteristiques;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit un lot avec uniquement un identifiant, un taxon, un stade de
	 * developpement, un effectif, une quantite et le lot parent
	 * 
	 * @param _identifiant
	 *            l'identifiant du lot
	 * @param _taxon
	 *            le taxon
	 * @param _stadeDeveloppement
	 *            le stade de d�veloppement
	 * @param _effectif
	 *            l'effectif
	 * @param _valeurQuantite
	 *            la quantit�
	 * @param _lotParent
	 *            le lot parent
	 */
	public Lot(Integer _identifiant, RefTaxon _taxon, RefStade _stadeDeveloppement, Float _effectif,
			Float _valeurQuantite, Lot _lotParent) {
		this(_identifiant, null, _taxon, _stadeDeveloppement, _effectif, _valeurQuantite, null, null, _lotParent, null,
				null, null, null, null);
	} // end Lot

	/**
	 * Construit une marque avec tous ses attributs
	 * 
	 * @param _identifiant
	 *            l'identifiant du lot
	 * @param _operation
	 *            l'op�ration
	 * @param _taxon
	 *            le taxon
	 * @param _stadeDeveloppement
	 *            le stade de d�veloppement
	 * @param _effectif
	 *            l'effectif
	 * @param _valeurQuantite
	 *            la quantit�
	 * @param _typeQuantite
	 *            type de quantit�
	 * @param _methodeObtention
	 *            m�thode d'obtention
	 * @param _lotParent
	 *            le lot parent
	 * @param _devenir
	 *            devenir du lot
	 * @param _commentaires
	 *            commentaires
	 * @param _lesPathologies
	 *            pathologies
	 * @param _lesMarquages
	 *            marquages
	 * @param _lesCaracteristiques
	 *            caract�ristiques
	 */
	public Lot(Integer _identifiant, Operation _operation, RefTaxon _taxon, RefStade _stadeDeveloppement,
			Float _effectif, Float _valeurQuantite, RefTypeQuantite _typeQuantite, String _methodeObtention,
			Lot _lotParent, RefDevenir _devenir, String _commentaires, SousListePathologieConstatee _lesPathologies,
			SousListeMarquages _lesMarquages, SousListeCaracteristiques _lesCaracteristiques) {

		this.setIdentifiant(_identifiant);
		this.setOperation(_operation);
		this.setTaxon(_taxon);
		this.setStadeDeveloppement(_stadeDeveloppement);
		this.setEffectif(_effectif);
		this.setValeurQuantite(_valeurQuantite);
		this.setTypeQuantite(_typeQuantite);
		this.setMethodeObtention(_methodeObtention);
		this.setLotParent(_lotParent);
		this.setDevenir(_devenir);
		this.setCommentaires(_commentaires);
		this.setLesPathologiesConstatees(_lesPathologies);
		this.setLesMarquages(_lesMarquages);
		this.setLesCaracteristiques(_lesCaracteristiques);

	} // end Lot

	///////////////////////////////////////
	// interfaces
	///////////////////////////////////////

	public void insertObjet() throws SQLException, ClassNotFoundException {
		// identifiant = null pour pouvoir utiliser la s�quence
		// t_lot_lot_lot_identifiant_seq
		this.identifiant = null;
		String table = Lot.getNomTable();
		ArrayList nomAttributs = Lot.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();

		System.out.println("Lot : insertObjet()");

		ConnexionBD.getInstance().executeInsert(table, nomAttributs, valAttributs);

		// initialise l'identifiant du lot avec le nombre qui vient d'etre
		// autoincremente
		this.identifiant = ConnexionBD.getInstance().getGeneratedKey(Lot.getNomSequences()[0]);
	}

	public void majObjet() throws SQLException, ClassNotFoundException {

		String table = Lot.getNomTable();
		String[] nomAttributsSelection = Lot.getNomID();
		Object[] valAttributsSelection = this.getValeurID();
		ArrayList nomAttributs = Lot.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();

		System.out.println("Lot : majObjet()");

		ConnexionBD.getInstance().executeUpdate(table, nomAttributsSelection, valAttributsSelection, nomAttributs,
				valAttributs);

	}

	public void effaceObjet() throws SQLException, ClassNotFoundException {

		String table = Lot.getNomTable();
		String[] nomAttributsSelection = Lot.getNomID();
		Object[] valAttributsSelection = this.getValeurID();

		System.out.println("Lot : effaceObjet()");

		ConnexionBD.getInstance().executeDelete(table, nomAttributsSelection, valAttributsSelection);

	}

	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;

		if (!Verification.isOperation(this.operation, true)) {
			throw new DataFormatException(Erreur.S4001);
		}
		if (!Verification.isInteger(this.identifiant, false)) {
			throw new DataFormatException(Erreur.S4000);
		}
		if (!Verification.isRef(this.taxon, true)) {
			throw new DataFormatException(Erreur.S4002);
		}
		if (!Verification.isRef(this.stadeDeveloppement, true)) {
			throw new DataFormatException(Erreur.S4003);
		}
		if (!Verification.isRefTypeQuantite(this.effectif, this.valeurQuantite, this.typeQuantite, true)) {
			throw new DataFormatException(Erreur.S4004);
		}
		if (!Verification.isMethodeObtentionLot(this.methodeObtention, true)) {
			throw new DataFormatException(Erreur.S4005);
		}
		if (!Verification.isLot(this.lotParent, false)) {
			throw new DataFormatException(Erreur.S4006);
		}
		if (!Verification.isRef(this.devenir, false)) {
			throw new DataFormatException(Erreur.S4007);
		}
		if (!Verification.isText(this.commentaires, false)) {
			throw new DataFormatException(Erreur.S4008);
		}
		// Rien a verifier
		// this.lesPathologies ;
		// this.lesMarquages ;
		// this.lesCaracteristiques ;

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return un le nom
	 */
	public static String getNomTable() {
		return "t_lot_lot";
	}

	/**
	 * Retourne les noms des sequences utilisee pour l'incrementation des
	 * attributs de la table
	 * 
	 * @return un Tableau de chaines avec les nom des sequences
	 */
	public static String[] getNomSequences() {

		String[] ret = new String[1];

		ret[0] = "t_lot_lot_lot_identifiant_seq";

		return ret;
	} // end getNomSequences

	/**
	 * Retourne le nom des attributs identifiants de la table correspondant a
	 * cette classe
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public static String[] getNomID() {

		String[] ret = new String[1];

		ret[0] = "lot_identifiant";

		return ret;
	} // end getNomID

	/**
	 * Retourne la valeur des attributs identifiants de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public Object[] getValeurID() {

		Object[] ret = new Object[1];

		ret[0] = this.identifiant;

		return ret;
	} // end getValeurID

	/**
	 * Retourne le nom des attributs de la table correspondant a cette classe
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		// impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
		// est inconnu

		ArrayList<String> ret = new ArrayList<String>();

		ret.add("lot_identifiant"); // autoincremente
		ret.add("lot_ope_identifiant");
		ret.add("lot_tax_code");
		ret.add("lot_std_code");
		ret.add("lot_effectif ");
		ret.add("lot_quantite");
		ret.add("lot_qte_code");
		ret.add("lot_methode_obtention");
		ret.add("lot_lot_identifiant");
		ret.add("lot_dev_code");
		ret.add("lot_commentaires");
		ret.add("lot_org_code");

		return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public ArrayList getValeurAttributs() {

		ArrayList<Object> ret = new ArrayList<Object>();

		ret.add(this.identifiant); // normalement null car autoincremente
		if (this.operation != null) {
			ret.add(this.operation.getIdentifiant());
		} else {
			ret.add(null);
		}
		if (this.taxon != null) {
			ret.add(this.taxon.getCode());
		} else {
			ret.add(null);
		}
		if (this.stadeDeveloppement != null) {
			ret.add(this.stadeDeveloppement.getCode());
		} else {
			ret.add(null);
		}
		ret.add(this.effectif);
		ret.add(this.valeurQuantite);
		if (this.typeQuantite != null) {
			ret.add(this.typeQuantite.getCode());
		} else {
			ret.add(null);
		}
		ret.add(this.methodeObtention);
		if (this.lotParent != null) {
			ret.add(this.lotParent.getIdentifiant());
		} else {
			ret.add(null);
		}
		if (this.devenir != null) {
			ret.add(this.devenir.getCode());
		} else {
			ret.add(null);
		}
		ret.add(this.commentaires);

		return ret;
	} // end getValeurAttributs

	/**
	 * Retourne l'identifiant du lot
	 * 
	 * @return l'identifiant
	 */
	public Integer getIdentifiant() {
		return this.identifiant;
	} // end getIdentifiant

	/**
	 * Retourne l'operation a laquelle est rattache le lot
	 * 
	 * @return l'operation
	 */
	public Operation getOperation() {
		return this.operation;
	} // end getOperation

	/**
	 * Retourne le taxon sur lequel porte le lot
	 * 
	 * @return le taxon
	 */
	public RefTaxon getTaxon() {
		return this.taxon;
	} // end getTaxon

	/**
	 * Retourne le stade de developpement
	 * 
	 * @return le stade
	 */
	public RefStade getStadeDeveloppement() {
		return this.stadeDeveloppement;
	} // end getStadeDeveloppement

	/**
	 * Retourne l'effectif
	 * 
	 * @return l'effectif
	 */
	public Float getEffectif() {
		return this.effectif;
	} // end getEffectif

	/**
	 * Retourne la valeur de la quantite
	 * 
	 * @return la valeur de la quantite
	 */
	public Float getValeurQuantite() {
		return this.valeurQuantite;
	} // end getValeurQuantite

	/**
	 * Retourne le type de quantite (poids, volume, etc.)
	 * 
	 * @return le type de quantite
	 */
	public RefTypeQuantite getTypeQuantite() {
		return this.typeQuantite;
	} // end getTypeQuantite

	/**
	 * Retourne la methode d'obtention
	 * 
	 * @return la methode d'obtention
	 */
	public String getMethodeObtention() {
		return this.methodeObtention;
	} // end getMethodeObtention

	/**
	 * Retourne le lot parent, dans le cas d'un echantillon (sous-lot)
	 * 
	 * @return le lot parent
	 */
	public Lot getLotParent() {
		return this.lotParent;
	} // end getLotParent

	/**
	 * Retourne le devenir
	 * 
	 * @return le devenir
	 */
	public RefDevenir getDevenir() {
		return this.devenir;
	} // end getDevenir

	/**
	 * Retourne les commentaires
	 * 
	 * @return les commentaires
	 */
	public String getCommentaires() {
		return this.commentaires;
	} // end getCommentaires

	/**
	 * Retourne la sous liste des pathologies constatees sur le lot
	 * 
	 * @return la sous liste des pathologies
	 */
	public SousListePathologieConstatee getLesPathologiesConstatees() {
		return this.lesPathologies;
	} // end getLesPathologies

	/**
	 * Retourne la sous liste des marquages du lot
	 * 
	 * @return la sous liste des marquages
	 */
	public SousListeMarquages getLesMarquages() {
		return this.lesMarquages;
	} // end getLesMarquages

	/**
	 * Retourne la sous liste des caracteristiques du lot
	 * 
	 * @return la sous liste des caracteristiques
	 */
	public SousListeCaracteristiques getLesCaracteristiques() {
		return this.lesCaracteristiques;
	} // end getLesCaracteristiques

	/**
	 * Initialise l'identifiant du lot
	 * 
	 * @param _identifiant
	 *            l'identifiant
	 */
	public void setIdentifiant(Integer _identifiant) {
		this.identifiant = _identifiant;
	} // end setIdentifiant

	/**
	 * Initialise l'operation a laquelle est rattache le lot
	 * 
	 * @param _operation
	 *            l'operation
	 */
	public void setOperation(Operation _operation) {
		this.operation = _operation;
	} // end setOperation

	/**
	 * Initialise le taxon sur lequel porte le lot
	 * 
	 * @param _taxon
	 *            le taxon
	 */
	public void setTaxon(RefTaxon _taxon) {
		this.taxon = _taxon;
	} // end setTaxon

	/**
	 * Initialise le stade de developpement
	 * 
	 * @param _stadeDeveloppement
	 *            le stade
	 */
	public void setStadeDeveloppement(RefStade _stadeDeveloppement) {
		this.stadeDeveloppement = _stadeDeveloppement;
	} // end setStadeDeveloppement

	/**
	 * Initialise l'effectif
	 * 
	 * @param _effectif
	 *            l'effectif
	 */
	public void setEffectif(Float _effectif) {
		this.effectif = _effectif;
	} // end setEffectif

	/**
	 * Initialise la valeur de la quantite
	 * 
	 * @param _valeurQuantite
	 *            la valeur de la quantite
	 */
	public void setValeurQuantite(Float _valeurQuantite) {
		this.valeurQuantite = _valeurQuantite;
	} // end setValeurQuantite

	/**
	 * Initialise le type de quantite (poids, volume, etc.)
	 * 
	 * @param _typeQuantite
	 *            le type de quantite
	 */
	public void setTypeQuantite(RefTypeQuantite _typeQuantite) {
		this.typeQuantite = _typeQuantite;
	} // end setTypeQuantite

	/**
	 * Initialise la methode d'obtention
	 * 
	 * @param _methodeObtention
	 *            la methode d'obtention
	 */
	public void setMethodeObtention(String _methodeObtention) {
		this.methodeObtention = _methodeObtention;
	} // end setMethodeObtention

	/**
	 * Initialise le lot parent, dans le cas d'un echantillon (sous-lot)
	 * 
	 * @param _lotParent
	 *            le lot parent
	 */
	public void setLotParent(Lot _lotParent) {
		this.lotParent = _lotParent;
	} // end setLotParent

	/**
	 * Initialise le devenir
	 * 
	 * @param _devenir
	 *            le devenir
	 */
	public void setDevenir(RefDevenir _devenir) {
		this.devenir = _devenir;
	} // end setDevenir

	/**
	 * Initialise les commentaires
	 * 
	 * @param _commentaires
	 *            les commentaires
	 */
	public void setCommentaires(String _commentaires) {
		this.commentaires = _commentaires;
	} // end setCommentaires

	/**
	 * Initialise la sous liste des pathologies constatees sur le lot
	 * 
	 * @param _lesPathologies
	 *            la sous liste des pathologies
	 */
	public void setLesPathologiesConstatees(SousListePathologieConstatee _lesPathologies) {
		this.lesPathologies = _lesPathologies;
	} // end setLesPathologies

	/**
	 * Initialise la sous liste des marquages du lot
	 * 
	 * @param _lesMarquages
	 *            la sous liste des marquages
	 */
	public void setLesMarquages(SousListeMarquages _lesMarquages) {
		this.lesMarquages = _lesMarquages;
	} // end setLesMarquages

	/**
	 * Initialise la sous liste des caracteristiques du lot
	 * 
	 * @param _lesCaracteristiques
	 *            la sous liste des caracteristiques
	 */
	public void setLesCaracteristiques(SousListeCaracteristiques _lesCaracteristiques) {
		this.lesCaracteristiques = _lesCaracteristiques;
	}

	// end setLesCaracteristiques

	/**
	 * Retourne les attributs du lot sous forme textuelle
	 * 
	 * @return l'affichage du lot utilise les contraintes du treemap pour
	 *         l'affichage des string initInfosLots() AjouterLot
	 */
	public String toString() {
		String esp = " \t ";
		String taxon;
		String stade;
		String statutstade;
		String effectif;
		String quantite;
		String idParent;
		String idLot;

		String ret;

		taxon = this.getTaxon().getLibelle();
		taxon = StringUtils.abbreviate(taxon, 25);
		taxon = StringUtils.rightPad(taxon, 25, " ");
		stade = this.getStadeDeveloppement().getLibelle();
		statutstade = this.getStadeDeveloppement().getStatut();
		stade = StringUtils.abbreviate(stade, 15);
		stade = StringUtils.rightPad(stade, 15, " ");
		if (statutstade != null) {
			if (statutstade.equals("gel�")) {
				stade = stade + "[!gel�]";
			}
		}
		if (this.getEffectif() == null) {
			effectif = "";
		} else {
			effectif = ((Integer) Math.round(this.getEffectif())).toString();
		}

		if (this.getValeurQuantite() == null) {
			quantite = "";
		} else {
			quantite = this.getValeurQuantite().toString();
		}
		effectif = effectif + quantite;
		effectif = StringUtils.leftPad(effectif, 11, " ");

		idLot = this.getIdentifiant().toString();
		idLot = StringUtils.leftPad(idLot, 8, " ");
		// Si le lot est un echantillon, decalage de la ligne
		// if (this.getLotParent() != null) {
		// idParent = this.getLotParent().getIdentifiant().toString();
		// } else {
		// idParent = "\u2205";
		// }
		// idParent = StringUtils.leftPad(idParent, 8, " ");
		if (this.getLotParent() != null) {
			ret = effectif + esp + idLot;
		} else {
			ret = taxon + esp + stade + esp + effectif + esp + idLot;

		}
		return ret;
	}

	public SousListePrelevement getLesPrelevements() {
		// TODO Auto-generated method stub
		return null;
	}

} // end Lot
