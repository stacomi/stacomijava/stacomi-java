package analyse;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Messages affich�s dans l'application
 * @author sgaudey
 */
public class Messages {
	private static final String BUNDLE_NAME = "analyse.messages"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private Messages() {
	}

	/**
	 * Retourne la valeur associ�e a la cl�
	 * @param key la cl�
	 * @return la valeur associ�e a la cl�
	 */
	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
