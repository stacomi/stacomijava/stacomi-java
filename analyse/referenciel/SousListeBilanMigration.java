/*
 **********************************************************************
 *
 * Nom fichier :        SousListeBilanMigration.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   21 juillet 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *                      test réalisé 02 mars 2005, problème dans le 
 *                      calcul des effectifs
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package analyse.referenciel;

import analyse.Messages;
import analyse.PasDeTemps;
import commun.*;
import migration.referenciel.*;
import infrastructure.*;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.zip.DataFormatException;
import java.sql.*;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Attention, cette sous liste n'herite pas de la classe SousListe. C'est une variante concue pour les tableaux bilans
 * @author Samuel Gaudey
 */
public class SousListeBilanMigration {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/** Nom du fichier de sortie */
	public static final String fichierExport = Messages
			.getString("SousListeBilanMigration.0"); //$NON-NLS-1$

	private DC dc;

	private RefTaxon[] taxons;

	private RefStade[] stades;

	private PasDeTemps pasDeTemps;

	private String fichier;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit un bilan avec tous ses attributs
	 * @param _dc le DC
	 * @param _taxons les taxons
	 * @param _stades les stades
	 * @param _dateDebut la date de d�but
	 * @param _pasDeTemps le pas de temps
	 * @param _fichier le fichier
	 */
	public SousListeBilanMigration(DC _dc, RefTaxon[] _taxons,
			RefStade[] _stades, Date _dateDebut, PasDeTemps _pasDeTemps,
			String _fichier) {
		this.setDc(_dc);
		this.setTaxons(_taxons);
		this.setStades(_stades);
		this.setPasDeTemps(_pasDeTemps);
		this.setFichier(_fichier);
	} // end SousListeBilanMigration   

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * V�rification des attributs
	 * @return vrai si les attributs sont corrects, soul�ve une exception sinon
	 * @throws DataFormatException
	 */
	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;

		if (!Verification.isDC(this.dc, true))
			throw new DataFormatException(Erreur.S7005);

		if (!Verification.isRefs(this.taxons, true))
			throw new DataFormatException(Erreur.S7001);

		if (!Verification.isRefs(this.stades, true))
			throw new DataFormatException(Erreur.S7002);

		if (!Verification.isPasDeTemps(this.pasDeTemps, true))
			throw new DataFormatException(Erreur.S7011);

		// Si pas de nom de fichier precisie alors, on utilise le nom par defaut
		if (!Verification.isText(this.fichier, true))
			this.fichier = fichierExport;

		return ret;
	}

	/**
	 * Exporte un fichier contenant les donnees du bilan
	 * @return un fichier contenant les donn�es du bilan
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public File enregistre() throws Exception {

		ResultSet rs = null;
		String sql;
		SimpleDateFormat simpleDate = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm:ss"); //$NON-NLS-1$

		double tauxEch = 0; // le taux recalcule avec ponderation pour le pas de temps courant
		Date debutPas = null; // la date de debut du pas courant
		Date finPas = null; // la date de fin du pas courant
		Vector<Double> lesTauxEch = null; // les taux trouves pour un certain pas de temps
		Vector<Date> datesDebutTauxEch = null; // les dates de debut du taux pour le pas courant
		Vector<Date> datesFinTauxEch = null; // les dates de fin du taux pour le pas courant

		// *********************
		//
		// Preparation du fichier de sortie
		//
		// *********************          

		// Creation d'un fichier de sortie
		File fic = new File(this.fichier);

		// Flux de sortie
		PrintWriter out = new PrintWriter(new BufferedWriter(
				new FileWriter(fic)));

		// Infos sur le bilan
		out.println(this.toString());
		out
				.println("No pas;Debut pas;Fin pas;Mesur�;Calcul�;Expert;Ponctuel;Type de quantit�;Taux d'�chappement;Coef conversion;"); //$NON-NLS-1$

		// *********************
		//
		// Boucle sur chacune des periodes du pas de temps
		//
		// *********************            

		while (this.pasDeTemps.current() != -1) {

			debutPas = this.pasDeTemps.currentDateDebut();
			finPas = this.pasDeTemps.currentDateFin();

			// graphique java
			SimpleDateFormat jour = new SimpleDateFormat("dd");
			SimpleDateFormat mois = new SimpleDateFormat("MM");
			SimpleDateFormat annee = new SimpleDateFormat("yyyy");

			int day = Integer.parseInt(jour.format(debutPas));
			int month = Integer.parseInt(mois.format(debutPas));
			int year = Integer.parseInt(annee.format(debutPas));

			Calendar c = DateFormat.getInstance().getCalendar();
			c.set(Calendar.DAY_OF_MONTH, day);
			c.set(Calendar.MONTH, month);
			c.set(Calendar.YEAR, year);

			//System.out.println(this.pasDeTemps.current()) ;
			//System.out.println(simpleDate.format(debutPas) + " -> " + simpleDate.format(finPas)) ;

			// *********************
			//
			// Taux d'echappement pour le pas de temps courant
			//
			// *********************

			// Si un seul taxon et un seul stade recherche du taux d'echappement
			if ((this.taxons.length == 1) && (this.stades.length == 1)) {

				// recherche des taux qui recoupent la periode du pas de temps
				sql = " SELECT txe_date_debut, txe_date_fin, txe_valeur_taux " + //$NON-NLS-1$
						" FROM   tj_tauxechappement_txe "
						+ //$NON-NLS-1$
						" WHERE  txe_tax_code = '"
						+ this.taxons[0].getCode()
						+ "'" + //$NON-NLS-1$ //$NON-NLS-2$
						"        AND txe_std_code = '"
						+ this.stades[0].getCode()
						+ "'" + //$NON-NLS-1$ //$NON-NLS-2$
						"        AND txe_valeur_taux IS NOT NULL"
						+ //$NON-NLS-1$
						"        AND (txe_date_debut, txe_date_fin) OVERLAPS (TIMESTAMP '"
						+ simpleDate.format(debutPas)
						+ "', TIMESTAMP '" + simpleDate.format(finPas) + "')" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						" ORDER BY txe_date_debut " + //$NON-NLS-1$
						" ;"; //$NON-NLS-1$

				rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

				// Recherche des coefficients pour ponderer le taux et des dates d'application des taux
				lesTauxEch = new Vector<Double>();
				datesDebutTauxEch = new Vector<Date>();
				datesFinTauxEch = new Vector<Date>();

				while (rs.next()) {
					datesDebutTauxEch.add(new Date(rs.getTimestamp(
							"txe_date_debut").getTime())); //$NON-NLS-1$
					datesFinTauxEch.add(new Date(rs
							.getTimestamp("txe_date_fin").getTime())); //$NON-NLS-1$
					lesTauxEch.add(new Double(rs.getDouble("txe_valeur_taux"))); //$NON-NLS-1$
				}

				// Traitement special pour le premier et le dernier taux
				if (lesTauxEch.size() > 0) {
					// Si le premier taux commence avant la periode du pas, on le modifie pour qu'il commence en meme temps que le pas
					if (((Date) datesDebutTauxEch.get(0)).before(debutPas))
						datesDebutTauxEch.setElementAt(debutPas, 0);

					// Si le dernier taux termine apres la periode du pas, on le modifie pour qu'il termine en meme temps que le pas
					if (((Date) datesFinTauxEch.get(datesFinTauxEch.size() - 1))
							.after(finPas))
						datesFinTauxEch.setElementAt(finPas, datesFinTauxEch
								.size() - 1);
				}

				tauxEch = 0;
				long cumulPeriodes = 0; // les durees cumulees des periodes d'application des taux 
				long periodePas = 0; // var temporaire pour la duree d'application d'un certain taux

				// Boucle sur chaque taux et application d'un coeeficient de ponderation
				for (int i = 0; i < lesTauxEch.size(); i++) {
					// tauxI * dureeI
					periodePas = ((Date) datesFinTauxEch.get(i)).getTime()
							- ((Date) datesDebutTauxEch.get(i)).getTime();
					tauxEch = tauxEch
							+ (((Double) lesTauxEch.get(i)).doubleValue() * (periodePas));

					cumulPeriodes = cumulPeriodes + periodePas;
				}

				// Divise par la duree cumulee pour retomber sur un taux
				if (cumulPeriodes != 0) // Division par le cumul des periodes des taux et non par la periode du pas de temps pour le cas ou les taux ne seraient pas definis sur la totalite du pas
					tauxEch = tauxEch / cumulPeriodes;
				else
					// erreur
					tauxEch = -1;
			} else
				// Le taux n'est pas calculable
				tauxEch = -1;

			// *********************
			//
			// Coeff de conversion quantite-effectif pour le pas de temps courant et pour chaque type de quantite
			//
			// *********************

			Hashtable<String, Object> lesCoefs = new Hashtable<String, Object>(); // le taux correspondant a chaque type de quantite
			Object[] coefDuType; // un tableau de 2 nombres (1 tableau pour chaque type de quantite) contenant : le cumul des periodes des coefs de ce type et le cumul des valeurs des coef

			// Si un seul taxon et un seul stade recherche du coef
			if ((this.taxons.length == 1) && (this.stades.length == 1)) {

				// recherche des coef qui recoupent la periode du pas de temps
				sql = " SELECT coe_date_debut, coe_date_fin, coe_valeur_coefficient, qte_libelle" + //$NON-NLS-1$
						" FROM   tj_coefficientconversion_coe "
						+ //$NON-NLS-1$
						"        INNER JOIN ref.tr_typequantitelot_qte ON coe_qte_code = qte_code"
						+ //$NON-NLS-1$
						" WHERE  coe_tax_code = '"
						+ this.taxons[0].getCode()
						+ "'" + //$NON-NLS-1$ //$NON-NLS-2$
						"        AND coe_std_code = '"
						+ this.stades[0].getCode()
						+ "'" + //$NON-NLS-1$ //$NON-NLS-2$
						"        AND (coe_date_debut, coe_date_fin) OVERLAPS (DATE '"
						+ simpleDate.format(debutPas)
						+ "', DATE '" + simpleDate.format(finPas) + "')" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						" ORDER BY coe_date_debut, qte_libelle " + //$NON-NLS-1$
						" ;"; //$NON-NLS-1$

				//System.out.println("Requete SQL : \n" + sql) ;
				rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

				// Recherche des poids pour ponderer le coef et des dates d'application des coef

				Date dateDebutCoef; // date de debut d'application d'un taux
				Date dateFinCoef; // date de fin d'application d'un taux
				double coef; // la valeur du taux
				String type; // le type de quantite

				//long cumulPeriodes = 0 ; // les durees cumulees des periodes d'application des coef 
				long periode = 0; // var temporaire pour la duree d'application d'un certain coef

				while (rs.next()) {
					dateDebutCoef = new Date(rs
							.getTimestamp("coe_date_debut").getTime()); //$NON-NLS-1$
					dateFinCoef = new Date(rs
							.getTimestamp("coe_date_fin").getTime()); //$NON-NLS-1$
					coef = rs.getDouble("coe_valeur_coefficient"); //$NON-NLS-1$
					type = rs.getString("qte_libelle"); //$NON-NLS-1$

					// Traitement special pour le premier et le dernier taux
					// Si le coef commence avant le pas de temps courant, sa periode est reduite pour commencer au debut du pas
					if (dateDebutCoef.before(debutPas))
						dateDebutCoef = debutPas;

					// Si le coef se termine apres la fin du pas, sa periode est reduite pour se terminer a la fin du pas
					if (dateFinCoef.after(finPas))
						dateFinCoef = finPas;

					// application d'un coeeficient de ponderation
					periode = dateFinCoef.getTime() - dateDebutCoef.getTime();
					coef = coef * periode;

					// Recherche si le type de quantite a deja ete rencontre ou non
					coefDuType = (Object[]) lesCoefs.get(type);

					// S'il n'existe pas encore, creation d'un tableau pour ce type
					if (coefDuType == null) {
						// Tableau pour cumuler les coefs de ce type
						coefDuType = new Object[2];
						coefDuType[0] = new Double(0); // cumul des coeff
						coefDuType[1] = new Long(0); // cumul des periodes
						lesCoefs.put(type, coefDuType);
					}

					// Ajout du coef courant au coefs deja obtenus
					coefDuType[0] = new Double(((Double) coefDuType[0])
							.doubleValue()
							+ coef);

					// Ajout de la periode du coef courant aux periodes deja obtenues
					coefDuType[1] = new Long(((Long) coefDuType[1]).longValue()
							+ periode);

				}

				// Pour chaque type de quantite, divise le cumul des coefs par la duree cumulee pour retomber sur un coef

				for (Enumeration e = lesCoefs.keys(); e.hasMoreElements();) {

					type = (String) e.nextElement();
					coefDuType = (Object[]) lesCoefs.get(type);

					// remplace l'ancien tableau contenant les cumuls par le coef recalcule
					if (((Long) coefDuType[1]).longValue() != 0) {
						// Division par le cumul des periodes des coef et non par la periode du pas de temps pour le cas ou les coef ne seraient pas definis sur la totalite du pas
						coef = (double) (((Double) coefDuType[0]).doubleValue() / ((Long) coefDuType[1])
								.doubleValue());
					} else {
						// erreur
						coef = -1;
					}

					lesCoefs.put(type, new Double(coef));
					//System.out.println("Coef de conversion pondéré : " + coef + " (" + type + ")" ) ;
				}

			}
			/*else {
			 // Les coefs ne sont pas calculables
			 //System.out.println("Coef de conversion non calculables") ;
			 }
			 */

			// *********************
			//
			// Operations concernees par le pas de temps
			//
			// *********************              

			// Preparation des criteres de selection
			// DC
			String dcCode = this.dc.getIdentifiant().toString();

			// Taxon (1 ou plusieurs)
			String complementTax = " AND ( lot_tax_code='" + this.taxons[0].getCode() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			for (int i = 1; i < this.taxons.length; i++)
				complementTax = complementTax
						+ " OR lot_tax_code='" + this.taxons[i].getCode() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			complementTax = complementTax + ")"; //$NON-NLS-1$

			// Stade (1 ou plusieurs)
			String complementStade = " AND ( lot_std_code='" + this.stades[0].getCode() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			for (int i = 1; i < this.stades.length; i++)
				complementStade = complementStade
						+ " OR lot_std_code='" + this.stades[i].getCode() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			complementStade = complementStade + ")"; //$NON-NLS-1$

			// ---------------------
			//    
			//  Lots avec effectif
			//
			// ---------------------
			{
				// recherche des operations qui recoupent la periode du pas de temps
				// Calcul de la somme des effectifs
				// On ne prend pas les echantillons
				sql = " SELECT ope_date_debut, ope_date_fin, lot_methode_obtention, SUM(lot_effectif) AS \"effectif\" " + //$NON-NLS-1$
						" FROM   t_operation_ope "
						+ //$NON-NLS-1$
						"        INNER JOIN t_lot_lot ON ope_identifiant = lot_ope_identifiant "
						+ //$NON-NLS-1$
						" WHERE  ope_dic_identifiant ='"
						+ dcCode
						+ "' " + //$NON-NLS-1$ //$NON-NLS-2$
						"        AND lot_effectif IS NOT NULL "
						+ //$NON-NLS-1$
						"        AND lot_lot_identifiant IS NULL "
						+ //$NON-NLS-1$
						complementTax
						+ complementStade
						+ "        AND (ope_date_debut, ope_date_fin) OVERLAPS (TIMESTAMP '" + simpleDate.format(debutPas) + "', TIMESTAMP '" + simpleDate.format(finPas) + "')" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						" GROUP BY ope_date_debut, ope_date_fin, lot_methode_obtention"
						+ //$NON-NLS-1$
						" ORDER BY ope_date_debut " + //$NON-NLS-1$
						" ;"; //$NON-NLS-1$

				//System.out.println("Requete SQL : \n" + sql) ;
				rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

				// Hashtable pour cumuler les effectifs par methode d'obtention
				Hashtable<String, Float> effectifs = new Hashtable<String, Float>();
				effectifs.put("MESURE", new Float(0)); //$NON-NLS-1$
				effectifs.put("CALCULE", new Float(0)); //$NON-NLS-1$
				effectifs.put("EXPERT", new Float(0)); //$NON-NLS-1$
				effectifs.put("PONCTUEL", new Float(0)); //$NON-NLS-1$

				Date debutOpe;
				Date finOpe;

				// Boucle sur chaque operation
				while (rs.next()) {

					// Traitement special pour la premiere operation qui peut etre a cheval sur le debut de la periode du pas de temps
					debutOpe = new Date(rs
							.getTimestamp("ope_date_debut").getTime()); //$NON-NLS-1$
					finOpe = new Date(rs.getTimestamp("ope_date_fin").getTime()); //$NON-NLS-1$
					float effectifTemp = rs.getFloat("effectif"); //$NON-NLS-1$
					String methode = rs.getString("lot_methode_obtention"); //$NON-NLS-1$

					// Si l'operation commence avant le pas de temps courant, et ne se termine pas apres, il faut conserver une seule partie de l'operation
					if (debutOpe.before(debutPas) && !(finOpe.after(finPas)))
						// Repartition de l'effectif au prorata
						effectifTemp = effectifTemp
								* (float) ((double) (finOpe.getTime() - debutPas
										.getTime()) / (double) (finOpe
										.getTime() - debutOpe.getTime()));

					// Si l'operation se termine apres la fin du pas mais ne debute pas avant, alors traitement different : repartition de l'effectif
					if (finOpe.after(finPas) && !(debutOpe.before(debutPas)))
						effectifTemp = effectifTemp
								* (float) ((double) (finPas.getTime() - debutOpe
										.getTime()) / (double) (finOpe
										.getTime() - debutOpe.getTime()));

					// Si l'operation commence avant le pas de temps et se termine apres, on ne conserve qu'une partie de l'operation
					if (debutOpe.before(debutPas) && finOpe.after(finPas))
						effectifTemp = effectifTemp
								* (float) ((double) (finPas.getTime() - debutPas
										.getTime()) / (double) (finOpe
										.getTime() - debutOpe.getTime()));

					// Cas ou l'operation est inferieure ou egale au pas de temps : pas de probleme, on compte l'operation complete

					// Ajoute l'effectif courant avec les effectifs obtenus jusque la
					// Les effectifs sont classes par methode d'obtention
					effectifTemp = effectifTemp
							+ ((Float) effectifs.get(methode)).floatValue();

					effectifs.put(methode, new Float(effectifTemp));
					//System.out.println("effectifTemp apres correction : " + effectifTemp) ;
				}

				double effMes = effectifs.get("MESURE");
				double effCal = effectifs.get("CALCULE");
				double effExp = effectifs.get("EXPERT");
				double effPon = effectifs.get("PONCTUEL");

				// Enregistrement des donnees du pas courant dans le fichier de sortie
				out.print(this.pasDeTemps.current() + ";"); // No pas //$NON-NLS-1$
				out.print(simpleDate.format(debutPas) + ";"); // Debut pas //$NON-NLS-1$
				out.print(simpleDate.format(finPas) + ";"); // Fin pas //$NON-NLS-1$
				out.print(effMes + ";"); // Mesuré //$NON-NLS-1$ //$NON-NLS-2$
				out.print(effCal + ";"); // Calculé //$NON-NLS-1$ //$NON-NLS-2$
				out.print(effExp + ";"); // Expert //$NON-NLS-1$ //$NON-NLS-2$
				out.print(effPon + ";"); // Ponctuel //$NON-NLS-1$ //$NON-NLS-2$
				out.print("Individus;"); // Type de quantité //$NON-NLS-1$
				out.print(tauxEch + ";"); // Taux d'échappement //$NON-NLS-1$
				out.print("null;\n"); // Coef conversion //$NON-NLS-1$
			}

			// ---------------------
			//
			// Lots avec quantite
			//
			// ---------------------
			{
				// Hashtable contenant des hashtables pour chaque type de quantite
				Hashtable<String, Hashtable<String, Float>> lesQuantites = new Hashtable<String, Hashtable<String, Float>>();

				// recherche des operations qui recoupent la periode du pas de temps
				// On ne prend pas les echantillons
				sql = " SELECT ope_date_debut, ope_date_fin, lot_methode_obtention, qte_libelle, SUM(lot_quantite) AS \"quantite\" " + //$NON-NLS-1$
						" FROM   t_operation_ope "
						+ //$NON-NLS-1$
						"        INNER JOIN t_lot_lot ON ope_identifiant = lot_ope_identifiant "
						+ //$NON-NLS-1$
						"        INNER JOIN ref.tr_typequantitelot_qte ON lot_qte_code = qte_code"
						+ //$NON-NLS-1$
						" WHERE  ope_dic_identifiant ='"
						+ dcCode
						+ "' " + //$NON-NLS-1$ //$NON-NLS-2$
						"        AND lot_quantite IS NOT NULL "
						+ //$NON-NLS-1$
						"        AND lot_lot_identifiant IS NULL "
						+ //$NON-NLS-1$
						complementTax
						+ complementStade
						+ "        AND (ope_date_debut, ope_date_fin) OVERLAPS (TIMESTAMP '" + simpleDate.format(debutPas) + "', TIMESTAMP '" + simpleDate.format(finPas) + "')" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						" GROUP BY ope_date_debut, ope_date_fin, lot_methode_obtention, qte_libelle"
						+ //$NON-NLS-1$
						" ORDER BY ope_date_debut " + //$NON-NLS-1$
						" ;"; //$NON-NLS-1$

				//System.out.println("Requete SQL : \n" + sql) ;
				rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

				Date debutOpe;
				Date finOpe;

				// Boucle sur chaque operation
				while (rs.next()) {

					// Traitement special pour la premiere operation qui peut etre a cheval sur le debut de la periode du pas de temps
					debutOpe = new Date(rs
							.getTimestamp("ope_date_debut").getTime()); //$NON-NLS-1$
					finOpe = new Date(rs.getTimestamp("ope_date_fin").getTime()); //$NON-NLS-1$
					float quantiteTemp = rs.getFloat("quantite"); //$NON-NLS-1$
					String methode = rs.getString("lot_methode_obtention"); //$NON-NLS-1$
					String typeQte = rs.getString("qte_libelle"); //$NON-NLS-1$

					// Si l'operation commence avant le pas de temps courant mais ne se termine pas apres, il faut conserver une seule partie de l'operation
					if (debutOpe.before(debutPas) && !(finOpe.after(finPas)))
						// Repartition de l'effectif au prorata
						quantiteTemp = quantiteTemp
								* (float) ((double) (finOpe.getTime() - debutPas
										.getTime()) / (double) (finOpe
										.getTime() - debutOpe.getTime()));

					// Si l'operation se termine apres la fin du pas mais n'a pas commencé avant, alors traitement different : repartition de la quantite
					if (!(debutOpe.before(debutPas)) && finOpe.after(finPas))

						quantiteTemp = quantiteTemp
								* (float) ((double) (finPas.getTime() - debutOpe
										.getTime()) / (double) (finOpe
										.getTime() - debutOpe.getTime()));

					// Si l'operation commence avant le pas de temps et se termine apres, on ne conserve qu'une partie de l'operation
					if (debutOpe.before(debutPas) && finOpe.after(finPas))
						quantiteTemp = quantiteTemp
								* (float) ((double) (finPas.getTime() - debutPas
										.getTime()) / (double) (finOpe
										.getTime() - debutOpe.getTime()));

					// Ajoute la quantite courante avec les quantites du meme type obtenues jusque la
					// Les quantites sont classees par methode d'obtention et par type

					// Recherche si le type de quantite a deja ete rencontre ou non
					Hashtable<String, Float> qte = (Hashtable) lesQuantites
							.get(typeQte);

					// S'il n'existe pas encore, creation d'un hashtable pour ce type
					if (qte == null) {
						// Hashtable pour cumuler les quantites de ce type par methode d'obtention
						qte = new Hashtable<String, Float>();
						qte.put("MESURE", new Float(0)); //$NON-NLS-1$
						qte.put("CALCULE", new Float(0)); //$NON-NLS-1$
						qte.put("EXPERT", new Float(0)); //$NON-NLS-1$
						qte.put("PONCTUEL", new Float(0)); //$NON-NLS-1$

						lesQuantites.put(typeQte, qte);
					}
					quantiteTemp = quantiteTemp
							+ ((Float) qte.get(methode)).floatValue();

					qte.put(methode, new Float(quantiteTemp));
					//System.out.println("quantiteTemp apres correction : " + quantiteTemp) ;
				}

				Hashtable qte;
				String type;

				// Enregistrement des donnees du pas courant dans le fichier de sortie
				// Boucle pour chaque type de quantite
				for (Enumeration e = lesQuantites.keys(); e.hasMoreElements();) {
					type = (String) e.nextElement();
					qte = (Hashtable) lesQuantites.get(type);

//					double qteMes = (Float) qte.get("MESURE");
//					double qteCal = (Float) qte.get("CALCULE");
//					double qteExp = (Float) qte.get("EXPERT");
//					double qtePon = (Float) qte.get("PONCTUEL");

					out.print(this.pasDeTemps.current() + ";"); // No pas //$NON-NLS-1$
					out.print(simpleDate.format(debutPas) + ";"); // Debut pas //$NON-NLS-1$
					out.print(simpleDate.format(finPas) + ";"); // Fin pas //$NON-NLS-1$
					out.print(qte.get("MESURE") + ";"); // Mesuré //$NON-NLS-1$ //$NON-NLS-2$
					out.print(qte.get("CALCULE") + ";"); // Calculé //$NON-NLS-1$ //$NON-NLS-2$
					out.print(qte.get("EXPERT") + ";"); // Expert //$NON-NLS-1$ //$NON-NLS-2$
					out.print(qte.get("PONCTUEL") + ";"); // Ponctuel //$NON-NLS-1$ //$NON-NLS-2$
					out.print(type + ";"); // Type de quantité //$NON-NLS-1$
					out.print(tauxEch + ";"); // Taux d'échappement //$NON-NLS-1$
					out.print(lesCoefs.get(type) + ";\n"); // Coef conversion //$NON-NLS-1$
				}
			}

			// Avance au pas de temps suivant
			this.pasDeTemps.next();
		}

		out.flush();
		out.close();

		return fic;
	} // end enregistre    

	/**
	 * @return le DC
	 */
	public DC getDc() {
		return this.dc;
	}

	/**
	 * @return les taxons
	 */
	public RefTaxon[] getTaxons() {
		return this.taxons;
	}

	/**
	 * @return les stades
	 */
	public RefStade[] getStades() {
		return this.stades;
	}

	/**
	 * @return le pas de temps
	 */
	public PasDeTemps getPasDeTemps() {
		return this.pasDeTemps;
	}

	/**
	 * @return le fichier
	 */
	public String getFichier() {
		return this.fichier;
	}

	/**
	 * @param _dc le DC
	 */
	public void setDc(DC _dc) {
		this.dc = _dc;
	}

	/**
	 * @param _taxons les taxons
	 */
	public void setTaxons(RefTaxon[] _taxons) {
		this.taxons = _taxons;
	}

	/**
	 * @param _stades les stades
	 */
	public void setStades(RefStade[] _stades) {
		this.stades = _stades;
	}

	/**
	 * @param _pasDeTemps le pas de temps
	 */
	public void setPasDeTemps(PasDeTemps _pasDeTemps) {
		this.pasDeTemps = _pasDeTemps;
	}

	/**
	 * @param _fichier le fichier
	 */
	public void setFichier(String _fichier) {
		this.fichier = _fichier;
	}

	public String toString() {
		String ret;

		ret = Messages.getString("SousListeBilanMigration.116"); //$NON-NLS-1$
		ret = ret
				+ Messages.getString("SousListeBilanMigration.117") + this.dc.toString(); //$NON-NLS-1$
		ret = ret + Messages.getString("SousListeBilanMigration.118"); //$NON-NLS-1$
		for (int i = 0; i < this.taxons.length; i++) {
			ret = ret
					+ Messages.getString("SousListeBilanMigration.119") + this.taxons[i]; //$NON-NLS-1$
		}
		ret = ret + Messages.getString("SousListeBilanMigration.120"); //$NON-NLS-1$
		for (int i = 0; i < this.stades.length; i++) {
			ret = ret
					+ Messages.getString("SousListeBilanMigration.121") + this.stades[i]; //$NON-NLS-1$
		}
		ret = ret
				+ Messages.getString("SousListeBilanMigration.122") + this.pasDeTemps; //$NON-NLS-1$
		ret = ret + Messages.getString("SousListeBilanMigration.123"); //$NON-NLS-1$

		return ret;
	}

} // end 
