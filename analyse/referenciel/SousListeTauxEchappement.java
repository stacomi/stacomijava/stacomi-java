/** Java class "SousListeTauxEchappement.java" generated from Poseidon for UML.
 *  Poseidon for UML is developed by <A HREF="http://www.gentleware.com">Gentleware</A>.
 *  Generated with <A HREF="http://jakarta.apache.org/velocity/">velocity</A> template engine.
 */
package analyse.referenciel;

import java.sql.ResultSet;
import java.util.Date;

import analyse.TauxEchappement;
import commun.ConnexionBD;
import commun.SousListe;
import infrastructure.Station;
import migration.referenciel.RefStade;
import migration.referenciel.RefTaxon;

/**
 * Sous liste permettant de stocker les taux d'�chappement 2016 transformation
 * de ouvrage > station
 * 
 * @author Samuel Gaudey C�dric Briand
 * 
 */
public class SousListeTauxEchappement extends SousListe {

	///////////////////////////////////////
	// associations
	///////////////////////////////////////

	/** */
	private static final long serialVersionUID = 1L;

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Construit une sousliste
	 */
	public SousListeTauxEchappement() {
		super();
	} // end sousListeTauxEchappement

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * 
	 * @param station
	 *            l'objet de rattachement
	 */
	public SousListeTauxEchappement(Station station) {
		super(station);
	} // end sousListeTauxEchappement

	/**
	 * Charge les taux d'�chappement pendant une p�riode donn�e
	 * 
	 * @param dateDebut
	 *            : date de d�but de la selection
	 * @param dateFin
	 *            : date de fin de la selection
	 */
	public void chargeFiltre(Date dateDebut, Date dateFin) {

	} // end chargeFiltre

	protected void chargeObjet(Object _objet) throws Exception {
		// TODO Auto-generated method stub
	}

	public void chargeSansFiltre() throws Exception {

		ResultSet rs = null;

		String sql = "SELECT txe_sta_code, sta_nom , txe_tax_code, txe_std_code, txe_date_debut, txe_date_fin "
				+ "FROM tj_tauxechappement_txe " + "JOIN t_station_sta on sta_code=txe_sta_code "
				+ "WHERE txe_ouv_identifiant ='" + this.getObjetDeRattachementID()[0] + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while (rs.next()) {
			String txe_sta_code = rs.getString("txe_sta_code");
			String sta_nom = rs.getString("sta_nom");
			String txe_tax_code = rs.getString("txe_tax_code");
			String txe_std_code = rs.getString("txe_std_code");
			Date txe_date_debut = rs.getTimestamp("txe_date_debut");
			Date txe_date_fin = rs.getTimestamp("txe_date_fin");

			Station station = new Station(txe_sta_code, sta_nom);
			RefTaxon reftaxon = new RefTaxon(txe_tax_code);
			RefStade refstade = new RefStade(txe_std_code);

			TauxEchappement te = new TauxEchappement(station, reftaxon, refstade, txe_date_debut, txe_date_fin);

			this.put(station + txe_tax_code + txe_std_code + txe_date_debut + txe_date_fin, te);

		}
	}

	public void chargeSansFiltreDetails() throws Exception {
		ResultSet rs = null;

		String sql = "SELECT txe_sta_code, sta_nom, txe_tax_code, tax_nom_latin, txe_std_code, std_libelle, txe_date_debut, txe_date_fin,"
				+ "txe_methode_estimation, txe_ech_code, txe_valeur_taux, txe_commentaires "
				+ "FROM tj_tauxechappement_txe txe " + "JOIN ref.tr_taxon_tax tax ON tax.tax_code=txe.txe_tax_code "
				+ "JOIN ref.tr_stadedeveloppement_std std ON std.std_code=txe_std_code "
				+ "JOIN t_station_sta on sta_code=txe_sta_code " + "WHERE txe_sta_code ='"
				+ this.getObjetDeRattachementID()[0] + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// parcours de toutes les lignes
		while (rs.next()) {
			String txe_sta_code = rs.getString("txe_sta_code");
			String sta_nom = rs.getString("sta_nom");
			String txe_tax_code = rs.getString("txe_tax_code");
			String txe_std_code = rs.getString("txe_std_code");
			Date txe_date_debut = rs.getTimestamp("txe_date_debut");
			Date txe_date_fin = rs.getTimestamp("txe_date_fin");
			String txe_methode_estimation = rs.getString("txe_methode_estimation");
			String txe_ech_code = rs.getString("txe_ech_code");
			Integer txe_valeur_taux = rs.getInt("txe_valeur_taux");
			String txe_commentaires = rs.getString("txe_commentaires");
			String tax_nom_latin = rs.getString("tax_nom_latin");
			String std_libelle = rs.getString("std_libelle");

			Station station = new Station(txe_sta_code, sta_nom);
			RefTaxon reftaxon = new RefTaxon(txe_tax_code, tax_nom_latin);
			RefStade refstade = new RefStade(txe_std_code, std_libelle);
			RefNiveauEchappement refniveauechappement = new RefNiveauEchappement(txe_ech_code/* ,ech_libelle */);

			TauxEchappement te = new TauxEchappement(station, reftaxon, refstade, txe_date_debut, txe_date_fin,
					txe_methode_estimation, txe_valeur_taux, refniveauechappement, txe_commentaires);

			this.put(station + txe_tax_code + txe_std_code + txe_date_debut + txe_date_fin, te);
		}
	}

	public String[] getObjetDeRattachementID() {
		// L'identifiant de l'ouvrage est 'identifiant'
		String ret[] = new String[1];
		ret[0] = ((Station) super.objetDeRattachement).getCode();

		return ret;
	}

} // end SousListeTauxEchappement
