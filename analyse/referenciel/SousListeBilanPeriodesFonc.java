/*
 **********************************************************************
 *
 * Nom fichier :        SousListeBilanPeriodesFonc.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   21 juillet 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package analyse.referenciel;

import analyse.Messages;
import commun.*;
import infrastructure.*;
import java.util.Date;
import java.util.zip.DataFormatException;
import java.sql.*;
import java.io.*;
import java.text.SimpleDateFormat;

/**
 * Attention, cette sous liste n'herite pas de la classe SousListe. C'est une variante concue pour les tableaux bilans
 * @author Samuel Gaudey
 */
public class SousListeBilanPeriodesFonc {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/** Nom du fichier d'export */
	public static final String fichierExport = Messages
			.getString("SousListeBilanPeriodesFonc.0"); //$NON-NLS-1$

	private DC dc;

	private Date dateDebut;

	private Date dateFin;

	private boolean croiseDF;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit un bilan avec tous ses attributs
	 * @param _dc le DC
	 * @param _croiseDF indique si le bilan doit etre crois� avec le DF
	 * @param _dateDebut date de d�but
	 * @param _dateFin date de fin
	 */
	public SousListeBilanPeriodesFonc(DC _dc, boolean _croiseDF,
			Date _dateDebut, Date _dateFin) {

		this.setDC(_dc);
		this.setCroiseDF(_croiseDF);
		this.setDateDebut(_dateDebut);
		this.setDateFin(_dateFin);

	} // end SousListeBilanPeriodesFonc   

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * @return vrai si les attributs sont corrects, faux sinon
	 * @throws DataFormatException
	 */
	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;

		if (!Verification.isDC(this.dc, true)) {
			throw new DataFormatException(Erreur.S7005);
		}
		if (!Verification.isDateInf(this.dateDebut, this.dateFin, false, false)) {
			throw new DataFormatException(Erreur.S7004);
		}

		return ret;
	}

	/**
	 * Retourne un ResultSet contenant les donnees du bilan
	 * @return un ResultSet contenant les donnees du bilan
	 * @throws Exception 
	 */
	public ResultSet charge() throws Exception {

		ResultSet rs = null;
		String sql = ""; //$NON-NLS-1$

		SimpleDateFormat simpleDate = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm:ss"); //$NON-NLS-1$

		// Preparation des criteres de selection

		// DC
		String dcCode = this.dc.getIdentifiant().toString();

		// Si tableau du dc uniquement
		if (this.croiseDF == false) {

			// Date de debut (facultative)
			String complementDateDeb = ""; //$NON-NLS-1$
			if (this.dateDebut != null) {
				complementDateDeb = " AND per_date_fin >='" + simpleDate.format(this.dateDebut) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			}

			// Date de fin (facultative)
			String complementDateFin = ""; //$NON-NLS-1$
			if (this.dateFin != null) {
				complementDateFin = " AND per_date_debut <='" + simpleDate.format(this.dateFin) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			}

			// Requete sql
			sql = "" + //$NON-NLS-1$
					" SELECT  per_date_debut, per_date_fin, per_etat_fonctionnement, tar_libelle"
					+ //$NON-NLS-1$
					" FROM    t_periodefonctdispositif_per"
					+ //$NON-NLS-1$
					"         INNER JOIN ref.tr_typearretdisp_tar ON per_tar_code = tar_code"
					+ //$NON-NLS-1$
					" WHERE   per_dis_identifiant = " + dcCode
					+ //$NON-NLS-1$
					complementDateDeb + complementDateFin
					+ " ORDER BY per_date_debut" + //$NON-NLS-1$
					" ;"; //$NON-NLS-1$

		}
		// Sinon, tableau croise du dc avec son default:de rattachement
		else {

			// DF
			String dfCode = this.dc.getDF().getIdentifiant().toString();

			// Date de debut (facultative)
			String complementDateDeb = ""; //$NON-NLS-1$
			if (this.dateDebut != null) {
				complementDateDeb = " AND dc.per_date_fin >='" + simpleDate.format(this.dateDebut) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			}

			// Date de fin (facultative)
			String complementDateFin = ""; //$NON-NLS-1$
			if (this.dateFin != null) {
				complementDateFin = " AND dc.per_date_debut <='" + simpleDate.format(this.dateFin) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			}

			sql = "" + //$NON-NLS-1$
					" SELECT    dc.per_date_debut AS \"DC per_date_debut\", dc.per_date_fin AS \"DC per_date_fin\", df.per_date_debut AS \"DF debut\", df.per_date_fin AS \"DF fin\", dc.per_etat_fonctionnement AS \"DC per_etat_fonctionnement\", df.per_etat_fonctionnement AS \"DF per_etat_fonctionnement\""
					+ //$NON-NLS-1$
					" FROM      t_periodefonctdispositif_per dc"
					+ //$NON-NLS-1$
					"           ,"
					+ //$NON-NLS-1$
					"           t_periodefonctdispositif_per df"
					+ //$NON-NLS-1$
					" WHERE     dc.per_dis_identifiant = '"
					+ dcCode
					+ "'" + //$NON-NLS-1$ //$NON-NLS-2$
					"           AND"
					+ //$NON-NLS-1$
					"           df.per_dis_identifiant = '"
					+ dfCode
					+ "'" + //$NON-NLS-1$ //$NON-NLS-2$
					"           AND (dc.per_date_debut, dc.per_date_fin) OVERLAPS (df.per_date_debut, df.per_date_fin)"
					+ //$NON-NLS-1$
					complementDateDeb + complementDateFin
					+ " ORDER BY dc.per_date_debut, df.per_date_debut " + //$NON-NLS-1$
					" ;"; //$NON-NLS-1$
		}

		System.out.println("Requete SQL : \n" + sql); //$NON-NLS-1$

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		return rs;
	} // end charge    

	/**
	 * Extrait de la BD et ecrit le resultat dans un fichier
	 * @return le fichier exporte
	 * @throws Exception 
	 */
	public File enregistre() throws Exception {

		// Extraction
		ResultSet rs = this.charge();

		// Creation d'un fichier de sortie
		File fic = new File(fichierExport);

		// Enregistrement dans le fichier

		// Flux de sortie
		PrintWriter out = new PrintWriter(new BufferedWriter(
				new FileWriter(fic)));

		// Infos sur le bilan
		out.println(this.toString());

		// Passage par un model pour simplifier l'export, et eventuellement permettre l'affichage du resultat
		TableModelBD tm = new TableModelBD(rs);

		// boucle sur chaque colonne pour ecriture des en tete
		for (int j = 0; j < tm.getColumnCount(); j++) {
			out.print(tm.getColumnName(j) + ";"); //$NON-NLS-1$
		}
		out.print("\n"); //$NON-NLS-1$

		// boucle sur chaque ligne
		for (int i = 0; i < tm.getRowCount(); i++) {

			// boucle sur chaque colonne
			for (int j = 0; j < tm.getColumnCount(); j++) {
				out.print(tm.getValueAt(i, j) + ";"); //$NON-NLS-1$
			}
			out.print("\n"); //$NON-NLS-1$

		}
		out.flush();
		out.close();

		return fic;
	} // end enregistre    

	/**
	 * @return le DC
	 */
	public DC getDC() {
		return this.dc;
	}

	/**
	 * @return la date de d�but
	 */
	public Date getDateDebut() {
		return this.dateDebut;
	}

	/**
	 * @return la date de fin
	 */
	public Date getDateFin() {
		return this.dateFin;
	}

	/**
	 * @return crois� avec DF ?
	 */
	public boolean getCroiseDF() {
		return this.croiseDF;
	}

	/**
	 * @param _dc le DC
	 */
	public void setDC(DC _dc) {
		this.dc = _dc;
	}

	/**
	 * @param _dateDebut la date de d�but
	 */
	public void setDateDebut(Date _dateDebut) {
		this.dateDebut = _dateDebut;
	}

	/**
	 * @param _dateFin la date de fin
	 */
	public void setDateFin(Date _dateFin) {
		this.dateFin = _dateFin;
	}

	/**
	 * @param _croiseDF crois� avec DF
	 */
	public void setCroiseDF(boolean _croiseDF) {
		this.croiseDF = _croiseDF;
	}

	public String toString() {
		String ret;

		ret = Messages.getString("SousListeBilanPeriodesFonc.40"); //$NON-NLS-1$
		ret = ret + this.dc.toString();
		ret = ret
				+ Messages.getString("SousListeBilanPeriodesFonc.41") + this.croiseDF; //$NON-NLS-1$
		if (this.croiseDF == true) {
			ret = ret
					+ Messages.getString("SousListeBilanPeriodesFonc.42") + this.dc.getDF().getCode() + Messages.getString("SousListeBilanPeriodesFonc.43"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		ret = ret
				+ Messages.getString("SousListeBilanPeriodesFonc.44") + this.dateDebut; //$NON-NLS-1$
		ret = ret
				+ Messages.getString("SousListeBilanPeriodesFonc.45") + this.dateFin; //$NON-NLS-1$
		ret = ret + Messages.getString("SousListeBilanPeriodesFonc.46"); //$NON-NLS-1$

		return ret;
	}

} // end 
