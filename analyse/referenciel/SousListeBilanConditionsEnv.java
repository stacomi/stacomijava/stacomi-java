/*
 **********************************************************************
 *
 * Nom fichier :        SousListeBilanConditionsEnv.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   21 juillet 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package analyse.referenciel;

import analyse.Messages;
import commun.*;
import infrastructure.Station;
import java.util.Date;
import java.util.zip.DataFormatException;
import java.sql.*;
import java.io.*;
import java.text.SimpleDateFormat;

/**
 * Attention, cette sous liste n'herite pas de la classe SousListe. C'est une variante concue pour les tableaux bilans
 * @author Samuel Gaudey
 */
public class SousListeBilanConditionsEnv {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/** Nom du fichier d'export */
	public static final String fichierExport = Messages
			.getString("SousListeBilanConditionsEnv.0"); //$NON-NLS-1$

	private Station station;

	private Date dateDebut;

	private Date dateFin;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit un bilan avec tous ses attributs
	 * @param _station station
	 * @param _dateDebut date de d�but
	 * @param _dateFin date de fin
	 */
	public SousListeBilanConditionsEnv(Station _station, Date _dateDebut,
			Date _dateFin) {

		this.setStation(_station);
		this.setDateDebut(_dateDebut);
		this.setDateFin(_dateFin);

	} // end SousListeBilanConditionsEnv   

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * @return vrai si les attributs sont corrects, soul�ve une exception sinon
	 * @throws DataFormatException
	 */
	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;

		if (!Verification.isStation(this.station, true)) {
			throw new DataFormatException(Erreur.S7000);
		}
		if (!Verification.isDateInf(this.dateDebut, this.dateFin, false, false)) {
			throw new DataFormatException(Erreur.S7004);
		}

		return ret;
	}

	/**
	 * Retourne un ResultSet contenant les donnees du bilan
	 * @return un ResultSet contenant les donn�es du bilan
	 * @throws Exception 
	 */
	public ResultSet charge() throws Exception {

		ResultSet rs = null;
		SimpleDateFormat simpleDate = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm:ss"); //$NON-NLS-1$

		// Preparation des criteres de selection

		// Station
		String staCode = this.station.getCode();

		// Date de debut (facultative)
		String complementDateDeb = ""; //$NON-NLS-1$
		if (this.dateDebut != null) {
			complementDateDeb = " AND env_date_fin >='" + simpleDate.format(this.dateDebut) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		}

		// Date de fin (facultative)
		String complementDateFin = ""; //$NON-NLS-1$
		if (this.dateFin != null) {
			complementDateFin = " AND env_date_debut <='" + simpleDate.format(this.dateFin) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		}

		// Extraction des enregistrements concernes

		String sql = "" + //$NON-NLS-1$
				" SELECT    env_date_debut AS \"Date de d�but\", env_date_fin AS \"Date de fin\", stm_libelle AS \"Station de mesure\", env_methode_obtention AS \"methode d'obtention\", env_valeur_quantitatif AS \"Valeur param. quantitatif\", val_libelle AS \"Val param. qualitatif\"  "
				+ //$NON-NLS-1$
				" FROM      (tj_conditionenvironnementale_env"
				+ //$NON-NLS-1$
				"           LEFT JOIN"
				+ //$NON-NLS-1$
				"           tj_stationmesure_stm ON stm_identifiant = env_stm_identifiant)"
				+ //$NON-NLS-1$
				"           LEFT JOIN "
				+ //$NON-NLS-1$
				"           ref.tr_valeurparametrequalitatif_val ON env_val_identifiant = val_identifiant"
				+ //$NON-NLS-1$
				" WHERE "
				+ //$NON-NLS-1$
				"    stm_sta_code='" + staCode + "' " + //$NON-NLS-1$ //$NON-NLS-2$
				complementDateDeb + complementDateFin
				+ " ORDER BY env_date_debut, stm_libelle " + //$NON-NLS-1$
				" ; "; //$NON-NLS-1$

		System.out.println("Requete SQL : \n" + sql); //$NON-NLS-1$

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		return rs;
	} // end charge    

	/**
	 * Extrait de la BD et ecrit le resultat dans un fichier
	 * @return le fichier exporte
	 * @throws Exception 
	 */
	public File enregistre() throws Exception {

		// Extraction
		ResultSet rs = this.charge();

		// Creation d'un fichier de sortie
		File fic = new File(fichierExport);

		// Enregistrement dans le fichier

		// Flux de sortie
		PrintWriter out = new PrintWriter(new BufferedWriter(
				new FileWriter(fic)));

		// Infos sur le bilan
		out.println(this.toString());

		// Passage par un model pour simplifier l'export, et eventuellement permettre l'affichage du resultat
		TableModelBD tm = new TableModelBD(rs);

		// boucle sur chaque colonne pour ecriture des en tete
		for (int j = 0; j < tm.getColumnCount(); j++) {
			out.print(tm.getColumnName(j) + ";"); //$NON-NLS-1$
		}
		out.print("\n"); //$NON-NLS-1$

		// boucle sur chaque ligne
		for (int i = 0; i < tm.getRowCount(); i++) {

			// boucle sur chaque colonne
			for (int j = 0; j < tm.getColumnCount(); j++) {
				out.print(tm.getValueAt(i, j) + ";"); //$NON-NLS-1$
			}
			out.print("\n"); //$NON-NLS-1$

		}
		out.flush();
		out.close();

		return fic;
	} // end enregistre    

	/**
	 * @return la station
	 */
	public Station getStation() {
		return this.station;
	}

	/**
	 * @return la date de d�but
	 */
	public Date getDateDebut() {
		return this.dateDebut;
	}

	/**
	 * @return la date de fin
	 */
	public Date getDateFin() {
		return this.dateFin;
	}

	/**
	 * @param _station la station
	 */
	public void setStation(Station _station) {
		this.station = _station;
	}

	/**
	 * @param _dateDebut la date de d�but
	 */
	public void setDateDebut(Date _dateDebut) {
		this.dateDebut = _dateDebut;
	}

	/**
	 * @param _dateFin la date de fin
	 */
	public void setDateFin(Date _dateFin) {
		this.dateFin = _dateFin;
	}

	public String toString() {
		String ret;

		ret = Messages.getString("SousListeBilanConditionsEnv.25"); //$NON-NLS-1$
		ret = ret
				+ Messages.getString("SousListeBilanConditionsEnv.26") + this.station.getCode() + Messages.getString("SousListeBilanConditionsEnv.27") + this.station.getLibelle(); //$NON-NLS-1$ //$NON-NLS-2$
		ret = ret
				+ Messages.getString("SousListeBilanConditionsEnv.28") + this.dateDebut; //$NON-NLS-1$
		ret = ret
				+ Messages.getString("SousListeBilanConditionsEnv.29") + this.dateFin; //$NON-NLS-1$
		ret = ret + Messages.getString("SousListeBilanConditionsEnv.30"); //$NON-NLS-1$

		return ret;
	}

} // end 

