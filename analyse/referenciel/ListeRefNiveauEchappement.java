/*
 **********************************************************************
 *
 * Nom fichier :        ListeRefNiveauEchappement.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   21 juillet 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package analyse.referenciel;

import commun.* ;
import java.sql.* ;



/**
 * 
 */
public class ListeRefNiveauEchappement extends Liste {

    
   ///////////////////////////////////////
   // attributes
   ///////////////////////////////////////
    



  ///////////////////////////////////////
  // operations heritees
  ///////////////////////////////////////

    /**
	 * 
	 */
	private static final long serialVersionUID = -6463693849732099938L;


	public void chargeSansFiltre() throws Exception {
        this.chargeSansFiltreDetails() ;
    }
    
    
    public void chargeSansFiltreDetails() throws Exception 
    {
         ResultSet   rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT ech_code, ech_libelle, ech_equivalence_tx FROM ref.tr_niveauechappement_ech ORDER BY ech_code ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String code = rs.getString("ech_code") ;
            String libelle = rs.getString("ech_libelle") ;
            String equivalence = rs.getString("ech_equivalence_tx") ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            RefNiveauEchappement ref = new RefNiveauEchappement(code, libelle, equivalence) ;
            this.put(code, ref) ;
        
        } // end while
    }
    
    
    protected void chargeObjet(Object _objet) throws Exception {
 
    /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * !!!!!!!!!!!!!!!!!!!!     A FAIRE         !!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     */    
    }
    
    

    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////  
    
 } // end ListeRefNiveauEchappement



