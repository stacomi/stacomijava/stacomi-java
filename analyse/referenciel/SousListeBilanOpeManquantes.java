/*
 **********************************************************************
 *
 * Nom fichier :        SousListeBilanOpeManquantes.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   21 juillet 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package analyse.referenciel;

import analyse.Messages;
import commun.*;
import infrastructure.*;
import java.util.Date;
import java.util.zip.DataFormatException;
import java.sql.*;
import java.io.*;
import java.text.SimpleDateFormat;

/**
 * Attention, cette sous liste n'herite pas de la classe SousListe. C'est une variante concue pour les tableaux bilans
 * @author Samuel Gaudey
 */
public class SousListeBilanOpeManquantes {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/** Nom du fichier de sortie */
	public static final String fichierExport = Messages
			.getString("SousListeBilanOpeManquantes.0"); //$NON-NLS-1$

	private DC dc;

	private Date dateDebut;

	private Date dateFin;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit un bilan avec tous ses attributs
	 * @param _dc le DC
	 * @param _dateDebut la date de d�but 
	 * @param _dateFin la date de fin
	 */
	public SousListeBilanOpeManquantes(DC _dc, Date _dateDebut, Date _dateFin) {

		this.setDC(_dc);
		this.setDateDebut(_dateDebut);
		this.setDateFin(_dateFin);

	} // end SousListeBilanOpeManquantes   

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * V�rification des attributs 
	 * @return vrai si les attributs sont corrects, soul�ve une exception sinon
	 * @throws DataFormatException
	 */
	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;

		if (!Verification.isDC(this.dc, true)) {
			throw new DataFormatException(Erreur.S7005);
		}
		if (!Verification.isDateInf(this.dateDebut, this.dateFin, true, true)) {
			throw new DataFormatException(Erreur.S7004);
		}

		return ret;
	}

	/**
	 * Retourne un ResultSet contenant les donnees du bilan
	 * @return un ResultSet contenant les donn�es du bilan
	 * @throws Exception 
	 */
	public ResultSet charge() throws Exception {

		ResultSet rs = null;
		String sql = ""; //$NON-NLS-1$

		SimpleDateFormat simpleDate = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm:ss"); //$NON-NLS-1$

		// Preparation des criteres de selection

		// DC
		String dcCode = this.dc.getIdentifiant().toString();

		// Requete sql
		sql = "" + //$NON-NLS-1$
				" SELECT  ope_identifiant, ope_date_debut, ope_date_fin"
				+ //$NON-NLS-1$
				" FROM    t_operation_ope "
				+ //$NON-NLS-1$
				" WHERE   ope_dic_identifiant = '"
				+ dcCode
				+ "'" + //$NON-NLS-1$ //$NON-NLS-2$
				"         AND (ope_date_debut, ope_date_fin) OVERLAPS (TIMESTAMP '"
				+ simpleDate.format(this.dateDebut)
				+ "', TIMESTAMP '" + simpleDate.format(this.dateFin) + "')" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				" ORDER BY ope_date_debut, ope_date_fin" + //$NON-NLS-1$
				" ;"; //$NON-NLS-1$

		System.out.println("Requete SQL : \n" + sql); //$NON-NLS-1$

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		return rs;

	} // end charge    

	/**
	 * Extrait de la BD et ecrit le resultat dans un fichier
	 * @return le fichier exporte
	 * @throws Exception 
	 */
	public File enregistre() throws Exception {

		// Extraction
		ResultSet rs = this.charge();

		SimpleDateFormat simpleDate = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm:ss"); //$NON-NLS-1$

		// Creation d'un fichier de sortie
		File fic = new File(fichierExport);

		// Enregistrement dans le fichier

		// Flux de sortie
		PrintWriter out = new PrintWriter(new BufferedWriter(
				new FileWriter(fic)));

		// Infos sur le bilan
		out.println(this.toString());

		out.println("ope_identifiant;ope_date_debut;ope_date_fin;probleme;"); //$NON-NLS-1$

		Date finPreviousOpe = null;
		Date debutCurrentOpe = null;
		Date finCurrentOpe = null;
		String opeId = null;

		// --------------------
		// VERIFICATION QUE 
		// - LA 1ere OPERATION NE COMMENCE PAS APRES LA PERIODE DEMANDEE
		// - LA DERNIERE OPERATION NE SE TERMINE PAS AVANT LA PERIODE DEMANDEE
		// - LES OPERATIONS SONT CONTIGUES
		// --------------------

		// Positionnement sur le premier enregistrement
		if (rs.first()) {

			// Recuperation des donnees
			opeId = rs.getString("ope_identifiant"); //$NON-NLS-1$
			debutCurrentOpe = new Date(rs
					.getTimestamp("ope_date_debut").getTime()); //$NON-NLS-1$
			finCurrentOpe = new Date(rs.getTimestamp("ope_date_fin").getTime()); //$NON-NLS-1$

			// Test de la date
			if (debutCurrentOpe.after(this.dateDebut)) {
				out.print(opeId + ";"); //$NON-NLS-1$
				out.print(simpleDate.format(debutCurrentOpe) + ";"); //$NON-NLS-1$
				out.print(simpleDate.format(finCurrentOpe) + ";"); //$NON-NLS-1$
				out.print(Erreur.S8000 + ";\n"); //$NON-NLS-1$
			}

			// Si la premiere operation est aussi la derniere, verification qu'elle ne se termnie pas avant la date de fin de la periode demandee
			if (rs.isLast()) {
				// Test de la date
				if (finCurrentOpe.before(this.dateFin)) {
					out.print(opeId + ";"); //$NON-NLS-1$
					out.print(simpleDate.format(debutCurrentOpe) + ";"); //$NON-NLS-1$
					out.print(simpleDate.format(finCurrentOpe) + ";"); //$NON-NLS-1$
					out.print(Erreur.S8001 + ";\n"); //$NON-NLS-1$
				}
			}

			finPreviousOpe = finCurrentOpe;
		}
		// Sinon, il n'y a aucune operation dans cette periode
		else {
			out.println(";;;" + Erreur.S8003 + ";"); //$NON-NLS-1$ //$NON-NLS-2$
		}

		// Parcourt les enregistrement suivants
		while (rs.next()) {

			// Recuperation des donnees
			opeId = rs.getString("ope_identifiant"); //$NON-NLS-1$
			debutCurrentOpe = new Date(rs
					.getTimestamp("ope_date_debut").getTime()); //$NON-NLS-1$
			finCurrentOpe = new Date(rs.getTimestamp("ope_date_fin").getTime()); //$NON-NLS-1$

			// Si c'est la derniere operation, verification qu'elle ne se termnie pas avant la date de fin de la periode demandee
			if (rs.isLast()) {
				// Test de la date
				if (finCurrentOpe.before(this.dateFin)) {
					out.print(opeId + ";"); //$NON-NLS-1$
					out.print(simpleDate.format(debutCurrentOpe) + ";"); //$NON-NLS-1$
					out.print(simpleDate.format(finCurrentOpe) + ";"); //$NON-NLS-1$
					out.print(Erreur.S8001 + ";\n"); //$NON-NLS-1$
				}
			}

			// Si l'operation n'est pas contigues a l'operation anterieure, erreur
			if (!debutCurrentOpe.equals(finPreviousOpe)) {
				out.print(opeId + ";"); //$NON-NLS-1$
				out.print(simpleDate.format(debutCurrentOpe) + ";"); //$NON-NLS-1$
				out.print(simpleDate.format(finCurrentOpe) + ";"); //$NON-NLS-1$
				out.print(Erreur.S8002 + simpleDate.format(finPreviousOpe)
						+ " -> " + simpleDate.format(debutCurrentOpe) + ";\n"); //$NON-NLS-1$ //$NON-NLS-2$
			}

			finPreviousOpe = finCurrentOpe;
		}

		out.flush();
		out.close();

		return fic;
	} // end enregistre    

	/**
	 * @return le DC
	 */
	public DC getDC() {
		return this.dc;
	}

	/**
	 * @return la date de d�but
	 */
	public Date getDateDebut() {
		return this.dateDebut;
	}

	/**
	 * @return la date de fin
	 */
	public Date getDateFin() {
		return this.dateFin;
	}

	/**
	 * @param _dc le DC
	 */
	public void setDC(DC _dc) {
		this.dc = _dc;
	}

	/**
	 * @param _dateDebut la date de d�but
	 */
	public void setDateDebut(Date _dateDebut) {
		this.dateDebut = _dateDebut;
	}

	/**
	 * @param _dateFin la date de fin
	 */
	public void setDateFin(Date _dateFin) {
		this.dateFin = _dateFin;
	}

	public String toString() {
		String ret;

		ret = Messages.getString("SousListeBilanOpeManquantes.41"); //$NON-NLS-1$
		ret = ret + this.dc.toString();
		ret = ret
				+ Messages.getString("SousListeBilanOpeManquantes.42") + this.dateDebut; //$NON-NLS-1$
		ret = ret
				+ Messages.getString("SousListeBilanOpeManquantes.43") + this.dateFin; //$NON-NLS-1$
		ret = ret + Messages.getString("SousListeBilanOpeManquantes.44"); //$NON-NLS-1$

		return ret;
	}

} // end 

