/*
 **********************************************************************
 *
 * Nom fichier :        SousListeCoefficientConversion.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   22 juillet 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package analyse.referenciel;

import commun.*;
import migration.referenciel.*;
import java.util.Date;

/**
 * Sous liste permettant stocker les coefficients de conversion 
 * @author Samuel Gaudey
 */
public class SousListeCoefficientConversion extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected Object[] objetDeRattachement; // redefinition 

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une SousListeCoefficientConversion
	 */
	public SousListeCoefficientConversion() {
		super();
	} // end SousListeCoefficientConversion

	/**
	 * Construit une SousListeCoefficientConversion avec un objet de rattachement
	 * @param _objetDeRattachement l'objet de rattachement
	 */
	public SousListeCoefficientConversion(Object[] _objetDeRattachement) {
		super();
		this.setObjetDeRattachement(_objetDeRattachement);
	} // end SousListeCoefficientConversion

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	public void chargeSansFiltre() {
		//String sql ;

		// A FAIRE

	}

	public void chargeSansFiltreDetails() {

		// A FAIRE

	}

	protected void chargeObjet(Object _objet) {

		// A FAIRE

	}

	/**
	 * Retourne l'identifiant du DF
	 * @return Tableau de taille 3 : (0) code taxon (1) code stade (2) code type de qté
	 */
	public String[] getObjetDeRattachementID() {

		String ret[] = new String[3];
		ret[0] = ((RefTaxon) this.objetDeRattachement[0]).getCode();
		ret[1] = ((RefStade) this.objetDeRattachement[2]).getCode();
		ret[2] = ((RefTypeQuantite) this.objetDeRattachement[1]).getCode();

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Charge une sous liste de coefficients contenus dans une certaine periode
	 * @param _dateDebut date de d�but
	 * @param _dateFin date de fin
	 */
	public void chargeFiltre(Date _dateDebut, Date _dateFin) {

	} // end chargeFiltre 

} // end SousListeCoefficientConversion

