/*
 **********************************************************************
 *
 * Nom fichier :        RefNiveauEchappement.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   21 juillet 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package analyse.referenciel;

import commun.*;

/**
 * Niveau d'échappement
 */
public class RefNiveauEchappement extends Ref {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	private String equivalenceTaux;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une reference avec un code mais sans libelle et sans mnemonique
	 * @param _code le code
	 */
	public RefNiveauEchappement(String _code) {
		this(_code, null, null);
	} // end RefNiveauEchappement

	/**
	 * Construit une reference avec un code et un libelle
	 * @param _code le code
	 * @param _libelle le libelle
	 */
	public RefNiveauEchappement(String _code, String _libelle) {
		this(_code, _libelle, null);
	} // end RefNiveauEchappement

	/**
	 * Construit une reference avec un code, un libelle et une mnemonique
	 * @param _code le code
	 * @param _libelle le libelle
	 * @param _equivalence le niveau d'échappement (string)
	 */
	public RefNiveauEchappement(String _code, String _libelle,
			String _equivalence) {
		super(_code, _libelle);
		this.setEquivalence(_equivalence);

	} // end RefNiveauEchappement

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	public void insertObjet() {

		// executer requete
	}

	public void majObjet() {

		// executer requete       
	}

	// Surcharge de la methode de la super classe pour verifier equivalenceTaux
	// => inutile car equivalenceTaux n'est pas obligatoire
	/* public boolean verifAttributs() throws DataFormatException {
	 
	 // verification du code et du libelle
	 boolean ret = super.verifAttributs() ;
	 
	 // verification de presence de la mnemonique
	 if ( (this.equivalenceTaux == null) || (this.equivalenceTaux == "") ) {
	 throw new DataFormatException (Erreur.S1002) ;
	 }
	 
	 return ret ;
	 } // end verifAttributs
	 */

	///////////////////////////////////////
	// operations
	///////////////////////////////////////    

	/**
	 * Retourne l'equivalence
	 * @return l'equivalence
	 */
	public String getEquivalence() {
		return this.equivalenceTaux;
	} // end getEquivalence        

	/**
	 * Inistialise l'equivalence
	 * @param _equivalenceTaux l'equivalence
	 */
	public void setEquivalence(String _equivalenceTaux) {
		this.equivalenceTaux = _equivalenceTaux;
	} // end setEquivalence        

} // end RefNiveauEchappement

