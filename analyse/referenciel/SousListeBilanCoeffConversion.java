/*
 **********************************************************************
 *
 * Nom fichier :        SousListeBilanCoeffConversion.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   21 juillet 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package analyse.referenciel;

import analyse.Messages;
import commun.*;
import migration.referenciel.*;
import java.util.Date;
import java.util.zip.DataFormatException;
import java.sql.*;
import java.io.*;
import java.text.SimpleDateFormat;

/**
 * Attention, cette sous liste n'herite pas de la classe SousListe. C'est une variante concue pour les tableaux bilans
 * @author Samuel Gaudey
 */
public class SousListeBilanCoeffConversion {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/** Nom du fichier d'export */
	public static final String fichierExport = Messages
			.getString("SousListeBilanCoeffConversion.0"); //$NON-NLS-1$

	private RefTaxon taxon;

	private RefStade stade;

	private RefTypeQuantite typeQte;

	private Date dateDebut;

	private Date dateFin;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit un bilan avec tous ses attributs
	 * @param _taxon taxon
	 * @param _stade stade
	 * @param _typeQte type de quantit�
	 * @param _dateDebut date de d�but
	 * @param _dateFin date de fin
	 */
	public SousListeBilanCoeffConversion(RefTaxon _taxon, RefStade _stade,
			RefTypeQuantite _typeQte, Date _dateDebut, Date _dateFin) {

		this.setTaxon(_taxon);
		this.setStade(_stade);
		this.setTypeQte(_typeQte);
		this.setDateDebut(_dateDebut);
		this.setDateFin(_dateFin);

	} // end SousListeBilanTauxEchappement   

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * V�ridication des attributs
	 * @return vrai si les attributs sont corrects, soul�ve une exception sinon
	 * @throws DataFormatException
	 */
	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;

		if (!Verification.isRef(this.taxon, true)) {
			throw new DataFormatException(Erreur.S7007);
		}
		if (!Verification.isRef(this.stade, true)) {
			throw new DataFormatException(Erreur.S7008);
		}
		if (!Verification.isRef(this.typeQte, true)) {
			throw new DataFormatException(Erreur.S7009);
		}
		if (!Verification.isDateInf(this.dateDebut, this.dateFin, false, false)) {
			throw new DataFormatException(Erreur.S7004);
		}

		return ret;
	}

	/**
	 * Retourne un ResultSet contenant les donnees du bilan
	 * @return un ResultSet contenant les donnees du bilan
	 * @throws Exception 
	 */
	public ResultSet charge() throws Exception {

		ResultSet rs = null;

		SimpleDateFormat simpleDate = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm:ss"); //$NON-NLS-1$

		// Preparation des criteres de selection

		// Taxon
		String tax = this.taxon.getCode();

		// Stade
		String std = this.stade.getCode();

		// Type
		String typ = this.typeQte.getCode();

		// Date de debut (facultative)
		String complementDateDeb = ""; //$NON-NLS-1$
		if (this.dateDebut != null) {
			complementDateDeb = " AND coe_date_fin >='" + simpleDate.format(this.dateDebut) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		}

		// Date de fin (facultative)
		String complementDateFin = ""; //$NON-NLS-1$
		if (this.dateFin != null) {
			complementDateFin = " AND coe_date_debut <='" + simpleDate.format(this.dateFin) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		}

		String sql = "" + //$NON-NLS-1$
				"SELECT     coe_date_debut, coe_date_fin, coe_valeur_coefficient, coe_commentaires"
				+ //$NON-NLS-1$
				" FROM 	tj_coefficientconversion_coe "
				+ //$NON-NLS-1$
				" WHERE     coe_tax_code = '" + tax + "'" + //$NON-NLS-1$ //$NON-NLS-2$
				"           AND coe_std_code = '" + std + "'" + //$NON-NLS-1$ //$NON-NLS-2$
				"           AND coe_qte_code = '" + typ + "'" + //$NON-NLS-1$ //$NON-NLS-2$
				complementDateDeb + complementDateFin
				+ " ORDER BY  coe_date_debut" + //$NON-NLS-1$
				" ; "; //$NON-NLS-1$

		System.out.println("Requete SQL : \n" + sql); //$NON-NLS-1$

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		return rs;
	} // end charge    

	/**
	 * Extrait de la BD et ecrit le resultat dans un fichier
	 * @return le fichier exporte
	 * @throws Exception 
	 */
	public File enregistre() throws Exception {

		// Extraction
		ResultSet rs = this.charge();

		// Creation d'un fichier de sortie
		File fic = new File(fichierExport);

		// Enregistrement dans le fichier

		// Flux de sortie
		PrintWriter out = new PrintWriter(new BufferedWriter(
				new FileWriter(fic)));

		// Infos sur le bilan
		out.println(this.toString());

		// Passage par un model pour simplifier l'export, et eventuellement permettre l'affichage du resultat
		TableModelBD tm = new TableModelBD(rs);

		// boucle sur chaque colonne pour ecriture des en tete
		for (int j = 0; j < tm.getColumnCount(); j++) {
			out.print(tm.getColumnName(j) + ";"); //$NON-NLS-1$
		}
		out.print("\n"); //$NON-NLS-1$

		// boucle sur chaque ligne
		for (int i = 0; i < tm.getRowCount(); i++) {

			// boucle sur chaque colonne
			for (int j = 0; j < tm.getColumnCount(); j++) {
				out.print(tm.getValueAt(i, j) + ";"); //$NON-NLS-1$
			}
			out.print("\n"); //$NON-NLS-1$

		}
		out.flush();
		out.close();

		return fic;
	} // end enregistre    

	/**
	 * @return le taxon
	 */
	public RefTaxon getTaxon() {
		return this.taxon;
	}

	/**
	 * @return le stade
	 */
	public RefStade getStade() {
		return this.stade;
	}

	/**
	 * @return le type de quantit�
	 */
	public RefTypeQuantite getTypeQte() {
		return this.typeQte;
	}

	/**
	 * @return la date de d�but
	 */
	public Date getDateDebut() {
		return this.dateDebut;
	}

	/**
	 * @return la date de fin
	 */
	public Date getDateFin() {
		return this.dateFin;
	}

	/**
	 * @param _taxon le taxon
	 */
	public void setTaxon(RefTaxon _taxon) {
		this.taxon = _taxon;
	}

	/**
	 * @param _stade le stade
	 */
	public void setStade(RefStade _stade) {
		this.stade = _stade;
	}

	/**
	 * @param _typeQte type de quantit�
	 */
	public void setTypeQte(RefTypeQuantite _typeQte) {
		this.typeQte = _typeQte;
	}

	/**
	 * @param _dateDebut la date de d�but
	 */
	public void setDateDebut(Date _dateDebut) {
		this.dateDebut = _dateDebut;
	}

	/**
	 * @param _dateFin la date de fin
	 */
	public void setDateFin(Date _dateFin) {
		this.dateFin = _dateFin;
	}

	public String toString() {
		String ret;

		ret = Messages.getString("SousListeBilanCoeffConversion.31"); //$NON-NLS-1$
		ret = ret
				+ Messages.getString("SousListeBilanCoeffConversion.25") + this.taxon.toString(); //$NON-NLS-1$
		ret = ret
				+ Messages.getString("SousListeBilanCoeffConversion.26") + this.stade.toString(); //$NON-NLS-1$
		ret = ret
				+ Messages.getString("SousListeBilanCoeffConversion.27") + this.typeQte.toString(); //$NON-NLS-1$
		ret = ret
				+ Messages.getString("SousListeBilanCoeffConversion.28") + this.dateDebut; //$NON-NLS-1$
		ret = ret
				+ Messages.getString("SousListeBilanCoeffConversion.29") + this.dateFin; //$NON-NLS-1$
		ret = ret + Messages.getString("SousListeBilanCoeffConversion.30"); //$NON-NLS-1$

		return ret;
	}

} // end 

