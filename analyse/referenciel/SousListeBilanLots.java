/*
 **********************************************************************
 *
 * Nom fichier :        SousListeBilanLots.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   21 juillet 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package analyse.referenciel;

import analyse.Messages;
import commun.*;
import migration.referenciel.*;
import infrastructure.Station;
import java.util.Date;
import java.util.zip.DataFormatException;
import java.sql.*;
import java.io.*;
import java.text.SimpleDateFormat;

/**
 * Attention, cette sous liste n'herite pas de la classe SousListe. C'est une variante concue pour les tableaux bilans
 * @author Samuel Gaudey
 */
public class SousListeBilanLots {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/** Le nom du fichier de sortie */
	public static final String fichierExport = Messages
			.getString("SousListeBilanLots.0"); //$NON-NLS-1$

	private Station station;

	private RefTaxon[] taxons;

	private RefStade[] stades;

	private RefParametre[] parametres;

	private Date dateDebut;

	private Date dateFin;

	private String fichier;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit un bilan avec tous ses attributs
	 * @param _station la station
	 * @param _taxons les taxons
	 * @param _stades les stades
	 * @param _parametres les param�tres
	 * @param _dateDebut la date de d�but
	 * @param _dateFin la date de fin
	 * @param _fichier le fichier
	 */
	public SousListeBilanLots(Station _station, RefTaxon[] _taxons,
			RefStade[] _stades, RefParametre[] _parametres, Date _dateDebut,
			Date _dateFin, String _fichier) {

		this.setStation(_station);
		this.setTaxons(_taxons);
		this.setStades(_stades);
		this.setParametres(_parametres);
		this.setDateDebut(_dateDebut);
		this.setDateFin(_dateFin);
		this.setFichier(_fichier);

	} // end Operation   

	///////////////////////////////////////
	// operations
	///////////////////////////////////////
	/**
	 * V�rification des attributs
	 * @return vrai si les attributs sont corrects, soul�ve une exception sinon 
	 * @throws DataFormatException 
	 */
	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;

		if (!Verification.isStation(this.station, true)) {
			throw new DataFormatException(Erreur.S7000);
		}
		if (!Verification.isRefs(this.taxons, true)) {
			throw new DataFormatException(Erreur.S7001);
		}
		if (!Verification.isRefs(this.stades, true)) {
			throw new DataFormatException(Erreur.S7002);
		}
		if (!Verification.isRefs(this.parametres, false)) {
			throw new DataFormatException(Erreur.S7003);
		}
		if (!Verification.isDateInf(this.dateDebut, this.dateFin, false, false)) {
			throw new DataFormatException(Erreur.S7004);
		}

		// Si pas de nom de fichier precisie alors, on utilise le nom par defaut
		if (!Verification.isText(this.fichier, true)) {
			this.fichier = fichierExport;
		}

		return ret;
	}

	/**
	 * Retourne un ResultSet contenant les donnees du bilan
	 * @return un ResultSet contenant les donn�es du bilan
	 * @throws Exception 
	 */
	public ResultSet charge() throws Exception {

		ResultSet rs = null;
		SimpleDateFormat simpleDate = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm:ss"); //$NON-NLS-1$

		// Preparation des criteres de selection

		// Station
		String staCode = this.station.getCode();

		// Taxon (1 ou plusieurs)
		String complementTax = " AND ( tax_code='" + this.taxons[0].getCode() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		for (int i = 1; i < this.taxons.length; i++) {
			complementTax = complementTax
					+ " OR tax_code='" + this.taxons[i].getCode() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		}
		complementTax = complementTax + ")"; //$NON-NLS-1$

		// Stade (1 ou plusieurs)
		String complementStade = " AND ( std_code='" + this.stades[0].getCode() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		for (int i = 1; i < this.stades.length; i++) {
			complementStade = complementStade
					+ " OR std_code='" + this.stades[i].getCode() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		}
		complementStade = complementStade + ")"; //$NON-NLS-1$

		// Parametres (0 ou plusieurs)
		String complementPar = ""; //$NON-NLS-1$
		if (this.parametres.length > 0) {
			complementPar = " AND ( par_code='" + this.parametres[0].getCode() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			for (int i = 1; i < this.parametres.length; i++) {
				complementPar = complementPar
						+ " OR par_code='" + this.parametres[i].getCode() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			}
			complementPar = complementPar + ")"; //$NON-NLS-1$
		}

		// Date de debut (facultative)
		String complementDateDeb = ""; //$NON-NLS-1$
		if (this.dateDebut != null) {
			complementDateDeb = " AND ope_date_fin >='" + simpleDate.format(this.dateDebut) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		}

		// Date de fin (facultative)
		String complementDateFin = ""; //$NON-NLS-1$
		if (this.dateFin != null) {
			complementDateFin = " AND ope_date_debut <='" + simpleDate.format(this.dateFin) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		}

		// Extraction des enregistrements concernes

		String sql = "" + //$NON-NLS-1$
				"SELECT "
				+ //$NON-NLS-1$
				"    ouv_libelle, dif_code, dic_code, ope_identifiant, ope_date_debut, ope_date_fin, lot_identifiant, lot_effectif, lot_quantite, qte_libelle, lot_methode_obtention, lot_lot_identifiant AS \"lot_pere\", dev_libelle, tax_code, tax_nom_latin, std_libelle, lot_commentaires, par_nom, val_libelle , car_valeur_quantitatif, car_precision, car_methode_obtention, car_commentaires "
				+ //$NON-NLS-1$
				"FROM "
				+ //$NON-NLS-1$
				"    ((((((((((t_ouvrage_ouv INNER JOIN t_dispositiffranchissement_dif ON ouv_identifiant = dif_ouv_identifiant) "
				+ //$NON-NLS-1$
				"    INNER JOIN t_dispositifcomptage_dic ON dic_dif_identifiant = dif_dis_identifiant) "
				+ //$NON-NLS-1$
				"    INNER JOIN t_operation_ope ON ope_dic_identifiant = dic_dis_identifiant) "
				+ //$NON-NLS-1$
				"    INNER JOIN t_lot_lot ON lot_ope_identifiant = ope_identifiant) "
				+ //$NON-NLS-1$
				"    LEFT JOIN ref.tr_typequantitelot_qte ON qte_code = lot_qte_code) "
				+ //$NON-NLS-1$
				"    LEFT JOIN ref.tr_devenirlot_dev ON dev_code = lot_dev_code) "
				+ //$NON-NLS-1$
				"    INNER JOIN ref.tr_taxon_tax ON tax_code = lot_tax_code) "
				+ //$NON-NLS-1$
				"    INNER JOIN ref.tr_stadedeveloppement_std ON std_code = lot_std_code) "
				+ //$NON-NLS-1$
				"    LEFT JOIN tj_caracteristiquelot_car ON lot_identifiant = car_lot_identifiant) "
				+ //$NON-NLS-1$
				"    LEFT JOIN ref.tg_parametre_par ON car_par_code = par_code) "
				+ //$NON-NLS-1$
				"    LEFT JOIN ref.tr_valeurparametrequalitatif_val ON car_val_identifiant = val_identifiant "
				+ //$NON-NLS-1$
				"WHERE  "
				+ //$NON-NLS-1$
				"    ouv_sta_code='" + staCode
				+ "' " + //$NON-NLS-1$ //$NON-NLS-2$
				complementDateDeb + complementDateFin + complementTax
				+ complementStade + complementPar + "ORDER BY " + //$NON-NLS-1$
				"    ouv_libelle, dif_code, dic_code, ope_date_debut, tax_code "
				+ //$NON-NLS-1$
				" ; "; //$NON-NLS-1$

		System.out.println("Requete SQL : \n" + sql); //$NON-NLS-1$

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		return rs;
	} // end charge    

	/**
	 * Extrait de la BD et ecrit le resultat dans un fichier
	 * @return le fichier exporte
	 * @throws Exception 
	 */
	public File enregistre() throws Exception {

		// Extraction
		ResultSet rs = this.charge();

		// Creation d'un fichier de sortie
		File fic = new File(this.fichier);

		// Enregistrement dans le fichier

		// Flux de sortie
		PrintWriter out = new PrintWriter(new BufferedWriter(
				new FileWriter(fic)));

		// Infos sur le bilan
		out.println(this.toString());

		// Passage par un model pour simplifier l'export, et eventuellement permettre l'affichage du resultat
		TableModelBD tm = new TableModelBD(rs);

		// boucle sur chaque colonne pour ecriture des en tete
		for (int j = 0; j < tm.getColumnCount(); j++) {
			out.print(tm.getColumnName(j) + ";"); //$NON-NLS-1$
		}
		out.print("\n"); //$NON-NLS-1$

		// boucle sur chaque ligne
		for (int i = 0; i < tm.getRowCount(); i++) {

			// boucle sur chaque colonne
			for (int j = 0; j < tm.getColumnCount(); j++) {
				out.print(tm.getValueAt(i, j) + ";"); //$NON-NLS-1$
			}
			out.print("\n"); //$NON-NLS-1$

		}
		out.flush();
		out.close();

		return fic;
	} // end enregistre    

	/**
	 * @return la station
	 */
	public Station getStation() {
		return this.station;
	}

	/**
	 * @return les taxons
	 */
	public RefTaxon[] getTaxons() {
		return this.taxons;
	}

	/**
	 * @return les stades
	 */
	public RefStade[] getStades() {
		return this.stades;
	}

	/**
	 * @return les param�tres
	 */
	public RefParametre[] getParametres() {
		return this.parametres;
	}

	/**
	 * @return la date de d�but
	 */
	public Date getDateDebut() {
		return this.dateDebut;
	}

	/**
	 * @return la date de fin
	 */
	public Date getDateFin() {
		return this.dateFin;
	}

	/**
	 * @return le fichier
	 */
	public String getFichier() {
		return this.fichier;
	}

	/**
	 * @param _station la station
	 */
	public void setStation(Station _station) {
		this.station = _station;
	}

	/**
	 * @param _taxons les taxons
	 */
	public void setTaxons(RefTaxon[] _taxons) {
		this.taxons = _taxons;
	}

	/**
	 * @param _stades les stades
	 */
	public void setStades(RefStade[] _stades) {
		this.stades = _stades;
	}

	/**
	 * @param _parametres les param�tres
	 */
	public void setParametres(RefParametre[] _parametres) {
		this.parametres = _parametres;
	}

	/**
	 * @param _dateDebut la date de d�but
	 */
	public void setDateDebut(Date _dateDebut) {
		this.dateDebut = _dateDebut;
	}

	/**
	 * @param _dateFin la date de fin
	 */
	public void setDateFin(Date _dateFin) {
		this.dateFin = _dateFin;
	}

	/**
	 * @param _fichier le fichier
	 */
	public void setFichier(String _fichier) {
		this.fichier = _fichier;
	}

	public String toString() {
		String ret;

		ret = Messages.getString("SousListeBilanLots.50"); //$NON-NLS-1$
		ret = ret
				+ Messages.getString("SousListeBilanLots.51") + this.station.getCode() + Messages.getString("SousListeBilanLots.52") + this.station.getLibelle(); //$NON-NLS-1$ //$NON-NLS-2$
		ret = ret + Messages.getString("SousListeBilanLots.53"); //$NON-NLS-1$
		for (int i = 0; i < this.taxons.length; i++) {
			ret = ret
					+ Messages.getString("SousListeBilanLots.54") + this.taxons[i]; //$NON-NLS-1$
		}
		ret = ret + Messages.getString("SousListeBilanLots.55"); //$NON-NLS-1$
		for (int i = 0; i < this.stades.length; i++) {
			ret = ret
					+ Messages.getString("SousListeBilanLots.56") + this.stades[i]; //$NON-NLS-1$
		}
		ret = ret + Messages.getString("SousListeBilanLots.57"); //$NON-NLS-1$
		for (int i = 0; i < this.parametres.length; i++) {
			ret = ret
					+ Messages.getString("SousListeBilanLots.58") + this.parametres[i]; //$NON-NLS-1$
		}
		ret = ret
				+ Messages.getString("SousListeBilanLots.59") + this.dateDebut; //$NON-NLS-1$
		ret = ret + Messages.getString("SousListeBilanLots.60") + this.dateFin; //$NON-NLS-1$
		ret = ret + Messages.getString("SousListeBilanLots.61"); //$NON-NLS-1$

		return ret;
	}

} // end 

