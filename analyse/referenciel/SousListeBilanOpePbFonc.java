/*
 **********************************************************************
 *
 * Nom fichier :        SousListeBilanOpePbFonc.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   21 juillet 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package analyse.referenciel;

import analyse.Messages;
import commun.*;
import infrastructure.*;

import java.util.Date;
import java.util.zip.DataFormatException;
import java.sql.*;
import java.io.*;
import java.text.SimpleDateFormat;

/**
 * Attention, cette sous liste n'herite pas de la classe SousListe. C'est une variante concue pour les tableaux bilans
 * @author Samuel Gaudey
 */
public class SousListeBilanOpePbFonc {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/** Nom du fichier d'export */
	public static final String fichierExport = Messages
			.getString("SousListeBilanOpePbFonc.0"); //$NON-NLS-1$

	private DC dc;

	private Date dateDebut;

	private Date dateFin;

	private boolean exportLots;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit un bilan avec tous ses attributs
	 * @param _dc le DC
	 * @param _dateDebut la date de d�but
	 * @param _dateFin la date de fin
	 * @param _exportLots export des lots ?
	 */
	public SousListeBilanOpePbFonc(DC _dc, Date _dateDebut, Date _dateFin,
			boolean _exportLots) {

		this.setDC(_dc);
		this.setDateDebut(_dateDebut);
		this.setDateFin(_dateFin);
		this.setExportLots(_exportLots);

	} // end SousListeBilanOpePbFonc   

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * @return vrai si les attributs sont corrects, soul�ve une exception sinon
	 * @throws DataFormatException
	 */
	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;

		if (!Verification.isDC(this.dc, true)) {
			throw new DataFormatException(Erreur.S7005);
		}
		if (!Verification.isDateInf(this.dateDebut, this.dateFin, false, false)) {
			throw new DataFormatException(Erreur.S7004);
		}

		return ret;
	}

	/**
	 * Retourne un ResultSet contenant les donnees du bilan
	 * @return un ResultSet contenant les donnees du bilan
	 * @throws Exception 
	 */
	public ResultSet charge() throws Exception {

		ResultSet rs = null;
		String sql = ""; //$NON-NLS-1$

		SimpleDateFormat simpleDate = new SimpleDateFormat(Messages
				.getString("SousListeBilanOpePbFonc.2")); //$NON-NLS-1$

		// Preparation des criteres de selection

		// DC
		String dcCode = this.dc.getIdentifiant().toString();

		// DF
		String dfCode = this.dc.getDF().getIdentifiant().toString();

		// Date de debut (facultative)
		String complementDateDeb = ""; //$NON-NLS-1$
		if (this.dateDebut != null) {
			complementDateDeb = " AND dc.per_date_fin >='" + simpleDate.format(this.dateDebut) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		}

		// Date de fin (facultative)
		String complementDateFin = ""; //$NON-NLS-1$
		if (this.dateFin != null) {
			complementDateFin = " AND dc.per_date_debut <='" + simpleDate.format(this.dateFin) + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		}

		// Si tableau du dc uniquement
		if (this.exportLots == false) {

			// Requete sql
			sql = "" + //$NON-NLS-1$
					" SELECT  df.per_date_debut AS \"DF per_date_debut\", df.per_date_fin AS \"DF per_date_fin\", df.per_etat_fonctionnement AS \"DF per_etat_fonctionnement\", dc.per_date_debut AS \"DC per_date_debut\", dc.per_date_fin AS \"DC per_date_fin\", dc.per_etat_fonctionnement AS \"DC per_etat_fonctionnement\", ope_identifiant, ope_date_debut, ope_date_fin"
					+ //$NON-NLS-1$
					" FROM    t_periodefonctdispositif_per dc, t_periodefonctdispositif_per df, t_operation_ope "
					+ //$NON-NLS-1$
					" WHERE   dc.per_dis_identifiant = '"
					+ dcCode
					+ "'" + //$NON-NLS-1$ //$NON-NLS-2$
					"         AND "
					+ //$NON-NLS-1$
					"         df.per_dis_identifiant = '"
					+ dfCode
					+ "'" + //$NON-NLS-1$ //$NON-NLS-2$
					"         AND (dc.per_date_debut, dc.per_date_fin) OVERLAPS (df.per_date_debut, df.per_date_fin)"
					+ //$NON-NLS-1$
					"         AND dc.per_etat_fonctionnement=false "
					+ //$NON-NLS-1$
					"         AND df.per_etat_fonctionnement=true"
					+ //$NON-NLS-1$
					"         AND (ope_date_debut, ope_date_fin) OVERLAPS (dc.per_date_debut, dc.per_date_fin)"
					+ //$NON-NLS-1$
					complementDateDeb + complementDateFin
					+ " ORDER BY ope_date_debut" + //$NON-NLS-1$
					" ;"; //$NON-NLS-1$

		}
		// Sinon, tableau croise du dc avec son default:de rattachement
		else {

			// Requete sql

			sql = "" + //$NON-NLS-1$
					" SELECT  df.per_date_debut AS \"DF per_date_debut\", df.per_date_fin AS \"DF per_date_fin\", df.per_etat_fonctionnement AS \"DF per_etat_fonctionnement\", dc.per_date_debut AS \"DC per_date_debut\", dc.per_date_fin AS \"DC per_date_fin\", dc.per_etat_fonctionnement AS \"DC per_etat_fonctionnement\", ope_identifiant, ope_date_debut, ope_date_fin, lot_identifiant, tax_nom_latin , std_libelle, lot_effectif, lot_quantite, qte_libelle, lot_methode_obtention, lot_lot_identifiant AS \"lot_pere\", dev_libelle, lot_commentaires"
					+ //$NON-NLS-1$
					" FROM    t_periodefonctdispositif_per dc, t_periodefonctdispositif_per df, t_operation_ope "
					+ //$NON-NLS-1$
					"         LEFT JOIN t_lot_lot ON ope_identifiant = lot_ope_identifiant "
					+ //$NON-NLS-1$
					"         LEFT JOIN ref.tr_taxon_tax ON lot_tax_code = tax_code "
					+ //$NON-NLS-1$
					"         LEFT JOIN ref.tr_stadedeveloppement_std ON lot_std_code = std_code "
					+ //$NON-NLS-1$
					"         LEFT JOIN ref.tr_typequantitelot_qte ON qte_code = lot_qte_code"
					+ //$NON-NLS-1$
					"         LEFT JOIN ref.tr_devenirlot_dev ON dev_code = lot_dev_code "
					+ //$NON-NLS-1$
					" WHERE   dc.per_dis_identifiant = '"
					+ dcCode
					+ "'" + //$NON-NLS-1$ //$NON-NLS-2$
					"         AND "
					+ //$NON-NLS-1$
					"         df.per_dis_identifiant = '"
					+ dfCode
					+ "'" + //$NON-NLS-1$ //$NON-NLS-2$
					"         AND (dc.per_date_debut, dc.per_date_fin) OVERLAPS (df.per_date_debut, df.per_date_fin)"
					+ //$NON-NLS-1$
					"         AND dc.per_etat_fonctionnement=false "
					+ //$NON-NLS-1$
					"         AND df.per_etat_fonctionnement=true"
					+ //$NON-NLS-1$
					"         AND (ope_date_debut, ope_date_fin) OVERLAPS (dc.per_date_debut, dc.per_date_fin)"
					+ //$NON-NLS-1$
					complementDateDeb + complementDateFin
					+ " ORDER BY ope_date_debut" + //$NON-NLS-1$
					" ;"; //$NON-NLS-1$
		}

		System.out.println("Requete SQL : \n" + sql);

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		return rs;
	} // end charge    

	/**
	 * Extrait de la BD et ecrit le resultat dans un fichier
	 * @return le fichier exporte
	 * @throws Exception 
	 */
	public File enregistre() throws Exception {

		// Extraction
		ResultSet rs = this.charge();

		// Creation d'un fichier de sortie
		File fic = new File(fichierExport);

		// Enregistrement dans le fichier

		// Flux de sortie
		PrintWriter out = new PrintWriter(new BufferedWriter(
				new FileWriter(fic)));

		// Infos sur le bilan
		out.println(this.toString());

		// Passage par un model pour simplifier l'export, et eventuellement permettre l'affichage du resultat
		TableModelBD tm = new TableModelBD(rs);

		// boucle sur chaque colonne pour ecriture des en tete
		for (int j = 0; j < tm.getColumnCount(); j++) {
			out.print(tm.getColumnName(j) + ";"); //$NON-NLS-1$
		}
		out.print("\n"); //$NON-NLS-1$

		// boucle sur chaque ligne
		for (int i = 0; i < tm.getRowCount(); i++) {

			// boucle sur chaque colonne
			for (int j = 0; j < tm.getColumnCount(); j++) {
				out.print(tm.getValueAt(i, j) + ";"); //$NON-NLS-1$
			}
			out.print("\n"); //$NON-NLS-1$

		}
		out.flush();
		out.close();

		return fic;
	} // end enregistre    

	/**
	 * @return le DC
	 */
	public DC getDC() {
		return this.dc;
	}

	/**
	 * @return la date de d�but
	 */
	public Date getDateDebut() {
		return this.dateDebut;
	}

	/**
	 * @return la date de fin
	 */
	public Date getDateFin() {
		return this.dateFin;
	}

	/**
	 * @param _dateFin la date de fin
	 */
	public void setDateFin(Date _dateFin) {
		this.dateFin = _dateFin;
	}

	/**
	 * @param _dc le DC
	 */
	public void setDC(DC _dc) {
		this.dc = _dc;
	}

	/**
	 * @param _dateDebut la date de d�but
	 */
	public void setDateDebut(Date _dateDebut) {
		this.dateDebut = _dateDebut;
	}

	/**
	 * @param _exportLots exports des lots ?
	 */
	public void setExportLots(boolean _exportLots) {
		this.exportLots = _exportLots;
	}
	
	/**
	 * @return vrai si les lots doivent �tre export�s
	 */
	public boolean getExportLots() {
		return this.exportLots;
	}

	public String toString() {
		String ret;

		ret = Messages.getString("SousListeBilanOpePbFonc.47"); //$NON-NLS-1$
		ret = ret + this.dc.toString();
		ret = ret
				+ Messages.getString("SousListeBilanOpePbFonc.48") + this.dateDebut; //$NON-NLS-1$
		ret = ret
				+ Messages.getString("SousListeBilanOpePbFonc.49") + this.dateFin; //$NON-NLS-1$
		ret = ret
				+ Messages.getString("SousListeBilanOpePbFonc.50") + this.exportLots; //$NON-NLS-1$
		ret = ret + Messages.getString("SousListeBilanOpePbFonc.51"); //$NON-NLS-1$

		return ret;
	}

} // end 

