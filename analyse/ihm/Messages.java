package analyse.ihm;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Messages affich�s dans la page
 * @author sgaudey
 */
public class Messages {
	private static final String BUNDLE_NAME = "analyse.ihm.messages"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private Messages() {
	}

	/**
	 * Retourn la valeur associ�e � la cl�
	 * @param key la cl�
	 * @return la valeur associ�e � la cl�
	 */
	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
