/*
 **********************************************************************
 *
 * Nom fichier :        CoefficientConversion.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   21 juillet 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package analyse;

import commun.* ;
import migration.referenciel.* ;

import java.util.ArrayList;
import java.util.Date;
import java.sql.* ;
//import java.util.Vector ;
import java.util.zip.DataFormatException ;

/**
 * Coefficient de conversion poids-effectif
 */
public class CoefficientConversion implements IBaseDonnees {

    

  //////////////////////////////////////
  // attributes
  //////////////////////////////////////

    private RefTaxon        taxon           ; 
    private RefStade        stade           ; 
    private RefTypeQuantite typeQuantite    ; 
    private Date            dateDebut       ; 
    private Date            dateFin         ; 
    private Float           valeur          ; 
    private String          commentaires    ; 



  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    
    
    /**
     * Construit un coefficient avec un taxon, un stade, un type, une date de debut et de fin
     * @param _taxon le taxon
     * @param _stade le stade
     * @param _typeQuantite le type de quantit�
     * @param _dateDebut la date de d�but
     * @param _dateFin la date de fin
     */
    public CoefficientConversion(RefTaxon _taxon, RefStade _stade, RefTypeQuantite _typeQuantite, Date _dateDebut, Date _dateFin) {    
       this(_taxon, _stade, _typeQuantite, _dateDebut, _dateFin, null, null) ;
    } // end CoefficientConversion   
    
    
    /**
     * Construit un coefficient avec tous ses parametres
     * @param _taxon le taxon
     * @param _stade le stade
     * @param _typeQuantite le type de quantit�
     * @param _dateDebut la date de d�but
     * @param _dateFin la date de fin
     * @param _valeur la valeur
     * @param _commentaires commentaires
     */
    public CoefficientConversion(RefTaxon _taxon, RefStade _stade, RefTypeQuantite _typeQuantite, Date _dateDebut, Date _dateFin, Float _valeur, String _commentaires) {   
       
       this.setTaxon(_taxon) ;
       this.setStade(_stade) ;
       this.setTypeQuantite(_typeQuantite) ;
       this.setDateDebut(_dateDebut);
       this.setDateFin(_dateFin) ;
       this.setValeur(_valeur) ;
       this.setCommentaires(_commentaires) ;
       
    } // end CoefficientConversion    
    
    
    
    
    
  ///////////////////////////////////////
  // interfaces
  ///////////////////////////////////////
    
    
    public void insertObjet() throws SQLException, ClassNotFoundException {
 
        String table        = CoefficientConversion.getNomTable()     ;
        ArrayList<String> nomAttributs = CoefficientConversion.getNomAttributs() ;
        @SuppressWarnings("rawtypes")
		ArrayList valAttributs = this.getValeurAttributs() ; 
        
        System.out.println("CoefficientConversion : insertObjet()") ;  
        
        ConnexionBD.getInstance().executeInsert(table, nomAttributs, valAttributs) ; 
    }
    
    
    public void majObjet() throws SQLException, ClassNotFoundException {
        
        String   table                   = CoefficientConversion.getNomTable()     ;
        String[] nomAttributsSelection   = CoefficientConversion.getNomID()        ;
        Object[] valAttributsSelection   = this.getValeurID()    ;
        ArrayList<String>   nomAttributs    = CoefficientConversion.getNomAttributs() ;
        @SuppressWarnings("rawtypes")
		ArrayList valAttributs  = this.getValeurAttributs() ; 
 
        System.out.println("CoefficientConversion : majObjet()") ;       
        
        ConnexionBD.getInstance().executeUpdate(table, nomAttributsSelection, valAttributsSelection, nomAttributs, valAttributs) ; 
   
    }
    
    
    public void effaceObjet() throws SQLException, ClassNotFoundException {
        
        String   table                   = CoefficientConversion.getNomTable()     ;
        String[] nomAttributsSelection   = CoefficientConversion.getNomID()        ;
        Object[] valAttributsSelection   = this.getValeurID()    ;
 
        System.out.println("CoefficientConversion : effaceObjet()") ;       
        
        ConnexionBD.getInstance().executeDelete(table, nomAttributsSelection, valAttributsSelection) ; 
 
    }
         
    
    
         
    public boolean verifAttributs() throws DataFormatException {
        boolean ret = false ;
        
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // A FAIRE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
        return ret ;
    }    
    
    
    
    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////
    
 
/**
 * Retourne le nom de la table correspondante a la classe
 * @return un le nom
 */
    public static String getNomTable() {  
        return "tj_coefficientconversion_coe" ;
    }
    

/**
 * Retourne le nom des attributs identifiants de la table correspondant a cette classe
 * @return un Vector de String avec les attributs dans l'ordre de la table dans la BD
 */
    public static String[] getNomID() {  
        
        String[] ret = new String[5] ;
        
        ret[0] = "coe_tax_code" ;
        ret[1] = "coe_std_code" ;
        ret[2] = "coe_qte_code" ;
        ret[3] = "coe_date_debut" ;
        ret[4] = "coe_date_fin" ;
        
        return ret ;
    } // end getNomID        
    
/**
 * Retourne la valeur des attributs identifiants de cette classe
 * @return un Vector d'Object avec les attributs dans l'ordre de la table dans la BD
 */
    public Object[] getValeurID() {  
        
        Object[] ret = new Object[5] ;
        
        ret[0] = this.taxon.getCode() ;
        ret[1] = this.stade.getCode() ;
        ret[2] = this.typeQuantite.getCode() ;
        ret[3] = this.dateDebut ;
        ret[4] = this.dateFin ;
        
        return ret ;
    } // end getValeurID  
    

/**
 * Retourne le nom des attributs de la table correspondant a cette classe
 * @return un ArrayList de String avec les attributs dans l'ordre de la table dans la BD
 */
    public static ArrayList<String> getNomAttributs() {  
        // impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau est inconnu
    
    	ArrayList<String> ret = new ArrayList<String>() ;

        ret.add("coe_tax_code") ;
        ret.add("coe_std_code") ;
        ret.add("coe_qte_code") ;
        ret.add("coe_date_debut") ;
        ret.add("coe_date_fin ") ;
        ret.add("coe_valeur_coefficient") ;
        ret.add("coe_commentaires") ;
        
        return ret ;
    } // end getNomAttributs        
    
    
/**
 * Retourne la valeur des attributs de cette classe
 * @return un ArrayList d'Object avec les attributs dans l'ordre de la table dans la BD
 */
    public ArrayList<Object> getValeurAttributs() {  
         
    	ArrayList<Object> ret = new ArrayList<Object>() ;
        
        if (this.taxon != null) { ret.add(this.taxon.getCode()) ; }
        else { ret.add(null) ; }
        if (this.stade != null) { ret.add(this.stade.getCode()) ;  }
        else { ret.add(null) ; }
        if (this.typeQuantite != null) { ret.add(this.typeQuantite.getCode()) ;  }
        else { ret.add(null) ; }
        ret.add(this.dateDebut) ;      
        ret.add(this.dateFin) ; 
        ret.add(this.valeur) ; 
        ret.add(this.commentaires) ;  
        
        return ret ;
    } // end getValeurAttributs   
    
    
    

/**
 * Retourne le taxon
 * @return le taxon
 */
    public RefTaxon getTaxon() {        
        return this.taxon;
    } // end getTaxon        

/**
 * Initialise le taxon
 * @param _taxon le taxon
 */
    public void setTaxon(RefTaxon _taxon) {        
        this.taxon = _taxon;
    } // end setTaxon        

/**
 * Retourne le stade 
 * @return le stade
 */
    public RefStade getStade() {        
        return this.stade;
    } // end getStade        

/**
 * Initialise le stade
 * @param _stade le stade
 */
    public void setStade(RefStade _stade) {        
        this.stade = _stade;
    } // end setStade        

/**
 * 
 * getter de typeQuantite
 * @return typeQuantite
 */
    public RefTypeQuantite getTypeQuantite() {        
        return this.typeQuantite;
    } // end getTypeQuantite        

/**
 * setTypeQuantite setter de type quantite
 * @param _typeQuantite 
 */
    public void setTypeQuantite(RefTypeQuantite _typeQuantite) {        
        this.typeQuantite = _typeQuantite;
    } // end setTypeQuantite        

/**
 * Does ...
 * 
 * 
 * @return dateDebut
 */
    public Date getDateDebut() {        
        return this.dateDebut;
    } // end getDateDebut        

/**
 * Does ...
 * 
 * 
 * @param _dateDebut 
 */
    public void setDateDebut(Date _dateDebut) {        
        this.dateDebut = _dateDebut;
    } // end setDateDebut        

/**
 * Does ...
 * 
 * 
 * @return dateFin
 */
    public Date getDateFin() {        
        return this.dateFin;
    } // end getDateFin        

/**
 * Does ...
 * 
 * 
 * @param _dateFin 
 */
    public void setDateFin(Date _dateFin) {        
        this.dateFin = _dateFin;
    } // end setDateFin        

/**
 * Does ...
 * 
 * 
 * @return valeur
 */
    public Float getValeur() {        
        return this.valeur;
    } // end getValeur        

/**
 * Does ...
 * 
 * 
 * @param _valeur 
 */
    public void setValeur(Float _valeur) {        
        this.valeur = _valeur;
    } // end setValeur        

/**
 * Does ...
 * 
 * 
 * @return commentaires
 */
    public String getCommentaires() {        
        return this.commentaires;
    } // end getCommentaires        

/**
 * Does ...
 * 
 * 
 * @param _commentaires 
 */
    public void setCommentaires(String _commentaires) {        
        this.commentaires = _commentaires;
    } // end setCommentaires        

 } // end CoefficientConversion



