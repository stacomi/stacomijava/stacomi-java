/*
 * PasDeTemps.java
 *
 * Created on 26 juillet 2004, 16:08
 */

package analyse;

import java.util.Calendar;
import java.util.Date;



/**
 * Pas de temps
 * @author Samuel gaudey - C�dric Briand
 */
public class PasDeTemps {

	// EN MILLISECONDES
	/** 1ms */
	public static final long UNE_MILLISECONDE = 1;

	/** 1000 ms */
	public static final long UNE_SECONDE = 1000 * UNE_MILLISECONDE;

	/** 0 sec */
	public static final long ZERO_MINUTE = 0 * UNE_SECONDE;
	
	/** 60 sec */
	public static final long UNE_MINUTE = 60 * UNE_SECONDE;

	/** 10 min */
	public static final long DIX_MINUTES = 10 * UNE_MINUTE;

	/** 15 min */
	public static final long QUINZE_MINUTES = 15 * UNE_MINUTE;

	/** 30 min */
	public static final long TRENTE_MINUTES = 30 * UNE_MINUTE;

	/** 60 min */
	public static final long UNE_HEURE = 60 * UNE_MINUTE;

	/** 12 h */
	public static final long DOUZE_HEURES = 12 * UNE_HEURE;

	/** 1 jour */
	public static final long UN_JOUR = 24 * UNE_HEURE;

	/** 1 semaine */
	public static final long UNE_SEMAINE = 7 * UN_JOUR;

	/** 2 semaines */
	public static final long DEUX_SEMAINES = 2 * UNE_SEMAINE;

	/** 1 mois */
	public static final long UN_MOIS = 30 * UN_JOUR;
	
	// C�dric pas d'int�r�t pour ces classes sur les stations de contr�le

	//public static final long TROIS_MOIS = 91 * UN_JOUR;

//	public static final long SIX_MOIS = 182 * UN_JOUR;

	/** 1 an */
	public static final long UN_AN = 365 * UN_JOUR;

	/** 0 min */
	public static final String LIB_ZERO_MINUTE = Messages
	.getString("PasDeTemps.10"); //$NON-NLS-1$
		
	/** 1 min */
	public static final String LIB_UNE_MINUTE = Messages
			.getString("PasDeTemps.0"); //$NON-NLS-1$

	/** 10 min */
	public static final String LIB_DIX_MINUTES = Messages
			.getString("PasDeTemps.1"); //$NON-NLS-1$

	/** 15 min */
	public static final String LIB_QUINZE_MINUTES = Messages
			.getString("PasDeTemps.2"); //$NON-NLS-1$

	/** 30 min */
	public static final String LIB_TRENTE_MINUTES = Messages
			.getString("PasDeTemps.3"); //$NON-NLS-1$

	/** 1 h */
	public static final String LIB_UNE_HEURE = Messages
			.getString("PasDeTemps.4"); //$NON-NLS-1$

	/** 12 h */
	public static final String LIB_DOUZE_HEURES = Messages
			.getString("PasDeTemps.5"); //$NON-NLS-1$

	/** 1 jour */
	public static final String LIB_UN_JOUR = Messages.getString("PasDeTemps.6"); //$NON-NLS-1$

	/** 1 semaine */
	public static final String LIB_UNE_SEMAINE = Messages
			.getString("PasDeTemps.7"); //$NON-NLS-1$

	/** 2 semaines */
	public static final String LIB_DEUX_SEMAINES = Messages
			.getString("PasDeTemps.8"); //$NON-NLS-1$

	/** 1 mois */
	public static final String LIB_UN_MOIS = Messages.getString("PasDeTemps.9"); //$NON-NLS-1$

//	public static final String LIB_TROIS_MOIS = Messages
//			.getString("PasDeTemps.10"); //$NON-NLS-1$

//	public static final String LIB_SIX_MOIS = Messages
//			.getString("PasDeTemps.11"); //$NON-NLS-1$

	/** 1 an */
	public static final String LIB_UN_AN = Messages.getString("PasDeTemps.17"); //$NON-NLS-1$

	/** Date de d�but */
	private Date dateDebut;

	private long dureePas;

	private int nbPas;

	private int noPasCourant;

	
	/**
	 * Constructeur
	 * @param _dateDebut
	 * @param _dureePas
	 * @param _nbPas
	 */
	public PasDeTemps(Date _dateDebut, long _dureePas, int _nbPas) {
		this.dateDebut = _dateDebut;
		this.dureePas = _dureePas;
		this.nbPas = _nbPas;
		this.noPasCourant = 0;
	}

	/**
	 * Constructeur utilis� pour l'import des op�rations vid�o
	 * Pour lesquelles on ne sait pas � l'avance le nombre de pas de temps
	 * @param _dateDebut
	 * @param _dureePas
	 */
	public PasDeTemps(Date _dateDebut, long _dureePas) {
		this.dateDebut = _dateDebut;
		this.dureePas = _dureePas;
		this.nbPas = -1;
		this.noPasCourant = 0;
	}
	
	

	/**
	 * @param _dateDebut
	 * @param _dureePas
	 * @param _nbPas
	 */
	public PasDeTemps(Date _dateDebut, String _dureePas, int _nbPas) {
		this.dateDebut = _dateDebut;

		if (_dureePas == LIB_ZERO_MINUTE){
			this.dureePas = ZERO_MINUTE;
		} else if (_dureePas == LIB_UNE_MINUTE) {
			this.dureePas = UNE_MINUTE;
		} else if (_dureePas == LIB_DIX_MINUTES) {
			this.dureePas = DIX_MINUTES;
		} else if (_dureePas == LIB_QUINZE_MINUTES) {
			this.dureePas = QUINZE_MINUTES;
		} else if (_dureePas == LIB_TRENTE_MINUTES) {
			this.dureePas = TRENTE_MINUTES;
		} else if (_dureePas == LIB_UNE_HEURE) {
			this.dureePas = UNE_HEURE;
		} else if (_dureePas == LIB_DOUZE_HEURES) {
			this.dureePas = DOUZE_HEURES;
		} else if (_dureePas == LIB_UN_JOUR) {
			this.dureePas = UN_JOUR;
		} else if (_dureePas == LIB_UNE_SEMAINE) {
			this.dureePas = UNE_SEMAINE;
		} else if (_dureePas == LIB_DEUX_SEMAINES) {
			this.dureePas = DEUX_SEMAINES;
		} else if (_dureePas == LIB_UN_MOIS) {
			this.dureePas = UN_MOIS;
//		} else if (_dureePas == LIB_TROIS_MOIS) {
//			this.dureePas = TROIS_MOIS;
//		} else if (_dureePas == LIB_SIX_MOIS) {
//			this.dureePas = SIX_MOIS;
		} else if (_dureePas == LIB_UN_AN) {
			this.dureePas = UN_AN;
		}

		this.nbPas = _nbPas;
		this.noPasCourant = 0;
	}
	
	
	/**
	 * @param _dateDebut
	 * @param _dureePas
	 */
	public PasDeTemps(Date _dateDebut, String _dureePas) {
		this.dateDebut = _dateDebut;

		if (_dureePas == LIB_ZERO_MINUTE) {
			this.dureePas = ZERO_MINUTE;
		} else if (_dureePas == LIB_UNE_MINUTE) {
			this.dureePas = UNE_MINUTE;
		} else if (_dureePas == LIB_DIX_MINUTES) {
			this.dureePas = DIX_MINUTES;
		} else if (_dureePas == LIB_QUINZE_MINUTES) {
			this.dureePas = QUINZE_MINUTES;
		} else if (_dureePas == LIB_TRENTE_MINUTES) {
			this.dureePas = TRENTE_MINUTES;
		} else if (_dureePas == LIB_UNE_HEURE) {
			this.dureePas = UNE_HEURE;
		} else if (_dureePas == LIB_DOUZE_HEURES) {
			this.dureePas = DOUZE_HEURES;
		} else if (_dureePas == LIB_UN_JOUR) {
			this.dureePas = UN_JOUR;
		} else if (_dureePas == LIB_UNE_SEMAINE) {
			this.dureePas = UNE_SEMAINE;
		} else if (_dureePas == LIB_DEUX_SEMAINES) {
			this.dureePas = DEUX_SEMAINES;
		} else if (_dureePas == LIB_UN_MOIS) {
			this.dureePas = UN_MOIS;
//		} else if (_dureePas == LIB_TROIS_MOIS) {
//			this.dureePas = TROIS_MOIS;
//		} else if (_dureePas == LIB_SIX_MOIS) {
//			this.dureePas = SIX_MOIS;
		} else if (_dureePas == LIB_UN_AN) {
			this.dureePas = UN_AN;
		}

		this.nbPas = -1;
		this.noPasCourant = 0;
	}


	/**
	 * Permet l'arrondi d'une date au premier arrondi sup�rieur du pas de temps
	 * donn�. Par exemple, la date <code>1095767411000</code> (qui repr�sente
	 * 2004-09-21 13:50:11) sera chang�e en <code>1093989600000</code>
	 * (2004-09-01 14:00:00) en utilisant la valeur String (libelle) de dureePas
	 * <code>dureePas="10 min"</code>.
	 * 
	 * 
	 * @return La date avec toutes les dur�es plus pr�cises que
	 *         <code>dureePas</code> arrondies � 0 or 1, exprim�es en
	 *         millisecondes depuis Janvier 1, 1970, 00:00:00 GMT
	 */
	public Date arrondisup() {
      Calendar cal = Calendar.getInstance();
      cal.setTimeInMillis(this.dateDebut.getTime());
      Date datefin=new Date();
      if (getLibellePas(this.dureePas) ==LIB_UN_AN)  {
        // on commence au premier janvier de l'ann�e suivante
        cal.set(Calendar.MONTH, 0);
        cal.roll(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
      } else if (getLibellePas(this.dureePas) == LIB_UN_MOIS) {
    	  // on commence le premier jour du mois suivant
        cal.roll(Calendar.MONTH, 1);
    	cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
      } else if (getLibellePas(this.dureePas) == LIB_UNE_SEMAINE) {
    	  // on commence au d�but de la semaine suivante
    	 cal.roll(Calendar.WEEK_OF_YEAR, 1);
    	 cal.set(Calendar.DAY_OF_WEEK,1);
      	 cal.set(Calendar.HOUR_OF_DAY, 0);
         cal.set(Calendar.MINUTE, 0);
         cal.set(Calendar.SECOND, 0);
         cal.set(Calendar.MILLISECOND, 0); 
      } else if (getLibellePas(this.dureePas)== LIB_UN_JOUR) {
    	  // on commence le jour suivant
        cal.roll(Calendar.DAY_OF_MONTH, 1);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);    
      } else if (getLibellePas(this.dureePas) == LIB_DOUZE_HEURES) {
    	  // on commence 12 h apr�s l'arrondi � 0 ou 12 heure
     	  int arrondi=Math.round(cal.get(Calendar.HOUR)/12)*12;
          cal.set(Calendar.HOUR_OF_DAY,  arrondi); // 0 ou 30
      	  cal.set(Calendar.MINUTE, 0);
          cal.set(Calendar.SECOND, 0);
          cal.set(Calendar.MILLISECOND, 0);
          cal.roll(Calendar.HOUR_OF_DAY,12);
      } else if (getLibellePas(this.dureePas) == LIB_UNE_HEURE) {
    	  // On commence l'heure suivante
        cal.roll(Calendar.HOUR_OF_DAY,1);
    	cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
      } else if (getLibellePas(this.dureePas) == LIB_TRENTE_MINUTES) {
    	  // On commence 30 minutes apr�s l'arrondi � 0 ou 30 minutes
     	  int arrondi=Math.round(cal.get(Calendar.MINUTE)/30)*30;
          cal.set(Calendar.MINUTE,  arrondi); // 0 ou 30
	      cal.set(Calendar.SECOND, 0);
	      cal.set(Calendar.MILLISECOND, 0);
	      cal.roll(Calendar.MINUTE, 30);     	 
      } else if (getLibellePas(this.dureePas) == LIB_QUINZE_MINUTES) {
    	  // On commence 15 minutes apr�s l'arrondi � 0 ou 15 30 ou 45 minutes
     	  int arrondi=Math.round(cal.get(Calendar.MINUTE)/15)*15;
    	  cal.set(Calendar.MINUTE, arrondi);
    	  cal.set(Calendar.SECOND, 0);
          cal.set(Calendar.MILLISECOND, 0);
          cal.roll(Calendar.MINUTE, 15);
      } else if (getLibellePas(this.dureePas) == LIB_DIX_MINUTES) {
     	  // On commence 10 minutes apr�s l'arrondi � 10 minutes
     	  int arrondi=Math.round(cal.get(Calendar.MINUTE)/10)*10;
	      cal.set(Calendar.MINUTE,arrondi);
	      cal.set(Calendar.SECOND, 0);
	      cal.set(Calendar.MILLISECOND, 0);
	      cal.roll(Calendar.MINUTE, 10);    
      } else if (getLibellePas(this.dureePas) == LIB_UNE_MINUTE) {
    	  // On commence la minute qui suit
    	  cal.roll(Calendar.MINUTE,1);
    	  cal.set(Calendar.SECOND, 0);
          cal.set(Calendar.MILLISECOND, 0);
      } else if (getLibellePas(this.dureePas) == LIB_ZERO_MINUTE) {
    	  // On commence pareil
    	  cal.roll(Calendar.MINUTE,0);
    	  cal.set(Calendar.SECOND, 0);
          cal.set(Calendar.MILLISECOND, 0);
      } else {
        throw new IllegalArgumentException("pas de temps inconnu " + dureePas);
      }
      datefin.setTime(cal.getTimeInMillis());
      return datefin;
    }

	/**
	 * Retourne le no du pas de temps courant
	 * @return le no du pas de temps courant
	 */ 
	public int current() {
		return this.noPasCourant;
	}

	/**
	 * Retourne la date de debut correspondant au no de pas courant
	 * @return la date de debut correspondant au no de pas courant
	 */ 
	public Date currentDateDebut() {
		Date ret;
		ret = new Date(this.dateDebut.getTime()
				+ (this.dureePas * this.noPasCourant));
		return ret;
	}

	/**
	 * Retourne la date de fin correspondant au no de pas courant
	 * @return la date de fin correspondant au no de pas courant
	 */ 
	public Date currentDateFin() {
		Date ret;
		ret = new Date(this.dateDebut.getTime()
				+ (this.dureePas * (this.noPasCourant + 1)));
		return ret;
	}

	/**
	 * Avance au pas de temps suivant et retourne le no du nouveau pas de temps
	 * @return le no du nouveau pas de temps
	 */ 
	public int next() {
		this.noPasCourant++;

		// Teste que le no ne deborde pas
		// Seulement si le nb de pas est pr�cis�
		
		if (this.nbPas!=-1 && this.currentDateFin().after(this.getDateFin())) {
			this.noPasCourant = -1;
		}
		return this.noPasCourant;
	}

	/**
	 * @return la date de d�but
	 */
	public Date getDateDebut() {
		return this.dateDebut;
	}

	/**
	 * @return la date de fin
	 */
	public long getDureePas() {
		return this.dureePas;
	}

	/**
	 * @return le nombre de pas
	 */
	public int getNbPas() {
		return this.nbPas;
	}

	/**
	 * @return la date de fin
	 */
	public Date getDateFin() {
		if (this.nbPas!=-1){
		Date ret = new Date(this.dateDebut.getTime()
				+ (this.dureePas * this.nbPas));
		return ret;
		} else {
			Date ret=new Date(0);
			return ret;
		} 
		
	}

	/**
	 * @return les libell�s des pas
	 */
	public static String[] getLibellesPas() {
		String[] ret = { LIB_ZERO_MINUTE, LIB_UNE_MINUTE, LIB_DIX_MINUTES, LIB_QUINZE_MINUTES,
				LIB_TRENTE_MINUTES, LIB_UNE_HEURE, LIB_DOUZE_HEURES,
				LIB_UN_JOUR, LIB_UNE_SEMAINE, LIB_DEUX_SEMAINES, LIB_UN_MOIS,
				//LIB_TROIS_MOIS, LIB_SIX_MOIS, 
				LIB_UN_AN };
		return ret;
	}

	/**
	 * @param _dureePas dur�e du pas
	 * @return la chaine de caractere associ�e � la longueur du pas
	 */
	public static String getLibellePas(long _dureePas) {
		String ret = ""; //$NON-NLS-1$

		if (_dureePas == ZERO_MINUTE) {
			ret = LIB_ZERO_MINUTE;
		} else if (_dureePas == UNE_MINUTE) {
			ret = LIB_UNE_MINUTE;
		} else if (_dureePas == DIX_MINUTES) {
			ret = LIB_DIX_MINUTES;
		} else if (_dureePas == QUINZE_MINUTES) {
			ret = LIB_QUINZE_MINUTES;
		} else if (_dureePas == TRENTE_MINUTES) {
			ret = LIB_TRENTE_MINUTES;
		} else if (_dureePas == UNE_HEURE) {
			ret = LIB_UNE_HEURE;
		} else if (_dureePas == DOUZE_HEURES) {
			ret = LIB_DOUZE_HEURES;
		} else if (_dureePas == UN_JOUR) {
			ret = LIB_UN_JOUR;
		} else if (_dureePas == UNE_SEMAINE) {
			ret = LIB_UNE_SEMAINE;
		} else if (_dureePas == DEUX_SEMAINES) {
			ret = LIB_DEUX_SEMAINES;
		} else if (_dureePas == UN_MOIS) {
			ret = LIB_UN_MOIS;
		//} else if (_dureePas == TROIS_MOIS) {
		//	ret = LIB_TROIS_MOIS;
		//} else if (_dureePas == SIX_MOIS) {
		//	ret = LIB_SIX_MOIS;
		} else if (_dureePas == UN_AN) {
			ret = LIB_UN_AN;
		}

		return ret;
	}

	public String toString() {
		String ret = "D�but : " + this.dateDebut + ", dur�e pas : " + PasDeTemps.getLibellePas(this.dureePas) + ", nb pas : " + this.nbPas; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return ret;
	}

}
