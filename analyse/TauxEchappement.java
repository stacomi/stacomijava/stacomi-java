/*
 **********************************************************************
 *
 * Nom fichier :        TauxEchappement.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   21 juillet 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package analyse;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
//import java.util.Vector ;
import java.util.zip.DataFormatException;

import analyse.referenciel.RefNiveauEchappement;
import commun.ConnexionBD;
import commun.IBaseDonnees;
import infrastructure.Station;
import migration.referenciel.RefStade;
import migration.referenciel.RefTaxon;

/**
 * Taux d'�chappement d'un station
 * 
 * @author Samuel Gaudey
 */
public class TauxEchappement implements IBaseDonnees {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/** La station */
	private Station station;

	/** Le taxon */
	private RefTaxon taxon;

	/** Le stade de d�veloppement */
	private RefStade stade;

	/** La date de d�but */
	private Date dateDebut;

	/** La date de fin */
	private Date dateFin;

	/** La m�thode d'estimation */
	private String methodeEstimation;

	/** Valeur du taux d'�chappement */
	private Integer valeur;

	/** Niveau d'echappement */
	private RefNiveauEchappement niveauEchappement;

	/** Commentaires */
	String commentaires;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit un taux d'echappement avec un station, un taxon, un stade, une
	 * date de debut et de fin
	 * 
	 * @param _station
	 *            la station
	 * @param _taxon
	 *            le taxon
	 * @param _stade
	 *            le stade
	 * @param _dateDebut
	 *            la date de d�but
	 * @param _dateFin
	 *            la date de fin
	 */
	public TauxEchappement(Station _station, RefTaxon _taxon, RefStade _stade, Date _dateDebut, Date _dateFin) {
		this(_station, _taxon, _stade, _dateDebut, _dateFin, null, null, null, null);
	} // end TauxEchappement

	/**
	 * Construit un taux d'echappement avec tous ses parametres
	 * 
	 * @param _station
	 *            la station
	 * @param _taxon
	 *            le taxon
	 * @param _stade
	 *            le stade
	 * @param _dateDebut
	 *            le d�but
	 * @param _dateFin
	 *            la date de fin
	 * @param _methodeEstimation
	 *            la m�thoded'estimation
	 * @param _valeur
	 *            la valeur
	 * @param _niveauEchappement
	 *            le niveau d'�chappement
	 * @param commentaires
	 *            les commentaires
	 */
	public TauxEchappement(Station _station, RefTaxon _taxon, RefStade _stade, Date _dateDebut, Date _dateFin,
			String _methodeEstimation, Integer _valeur, RefNiveauEchappement _niveauEchappement, String commentaires) {

		this.setStation(_station);
		this.setTaxon(_taxon);
		this.setStade(_stade);
		this.setDateDebut(_dateDebut);
		this.setDateFin(_dateFin);
		this.setMethodeEstimation(_methodeEstimation);
		this.setValeur(_valeur);
		this.setNiveauEchappement(_niveauEchappement);
		this.setCommentaires(commentaires);
	} // end TauxEchappement

	///////////////////////////////////////
	// interfaces
	///////////////////////////////////////

	public void insertObjet() throws SQLException, ClassNotFoundException {

		String table = TauxEchappement.getNomTable();
		ArrayList nomAttributs = TauxEchappement.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();

		System.out.println("TauxEchappement : insertObjet()");

		ConnexionBD.getInstance().executeInsert(table, nomAttributs, valAttributs);
	}

	public void majObjet() throws SQLException, ClassNotFoundException {
	}

	/**
	 * Mise a jour d'un taux d'�chappement dans la base
	 * 
	 * @param old
	 *            ancien taux d'�chappement
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void majObjet(TauxEchappement old) throws SQLException, ClassNotFoundException {
		String table = TauxEchappement.getNomTable();
		String[] nomAttributsSelection = TauxEchappement.getNomID();
		Object[] valAttributsSelection = old.getValeurID();
		ArrayList nomAttributs = TauxEchappement.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();

		System.out.println("TauxEchappement : majObjet()");

		ConnexionBD.getInstance().executeUpdate(table, nomAttributsSelection, valAttributsSelection, nomAttributs,
				valAttributs);
	}

	public void effaceObjet() throws SQLException, ClassNotFoundException {

		String table = TauxEchappement.getNomTable();
		String[] nomAttributsSelection = TauxEchappement.getNomID();
		Object[] valAttributsSelection = this.getValeurID();

		System.out.println("TauxEchappement : effaceObjet()");

		ConnexionBD.getInstance().executeDelete(table, nomAttributsSelection, valAttributsSelection);
	}

	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;

		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// A FAIRE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return un le nom
	 */
	public static String getNomTable() {
		return "tj_tauxechappement_txe";
	}

	/**
	 * Retourne le nom des attributs identifiants de la table correspondant a
	 * cette classe
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 * 
	 */
	public static String[] getNomID() {

		String[] ret = new String[5];

		ret[0] = "txe_sta_code";
		ret[1] = "txe_tax_code";
		ret[2] = "txe_std_code";
		ret[3] = "txe_date_debut";
		ret[4] = "txe_date_fin";

		return ret;
	} // end getNomID

	/**
	 * Retourne la valeur des attributs identifiants de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public Object[] getValeurID() {

		Object[] ret = new Object[5];

		ret[0] = this.station.getCode();
		ret[1] = this.taxon.getCode();
		ret[2] = this.stade.getCode();
		ret[3] = this.dateDebut;
		ret[4] = this.dateFin;

		return ret;
	} // end getValeurID

	/**
	 * Retourne le nom des attributs de la table correspondant a cette classe
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		// impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
		// est inconnu

		ArrayList<String> ret = new ArrayList<String>();

		ret.add("txe_sta_code");
		ret.add("txe_tax_code");
		ret.add("txe_std_code");
		ret.add("txe_date_debut");
		ret.add("txe_date_fin ");
		ret.add("txe_methode_estimation");
		ret.add("txe_ech_code");
		ret.add("txe_valeur_taux");
		ret.add("txe_commentaires");
		ret.add("txe_org_code");

		return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public ArrayList<Object> getValeurAttributs() {

		ArrayList<Object> ret = new ArrayList<Object>();

		if (this.station != null)
			ret.add(this.station.getCode());
		else
			ret.add(null);

		if (this.taxon != null)
			ret.add(this.taxon.getCode());
		else
			ret.add(null);

		if (this.stade != null)
			ret.add(this.stade.getCode());
		else
			ret.add(null);

		ret.add(this.dateDebut);
		ret.add(this.dateFin);
		ret.add(this.methodeEstimation);

		if (this.niveauEchappement != null)
			ret.add(this.niveauEchappement.getCode());
		else
			ret.add(null);

		ret.add(this.valeur);

		ret.add(this.commentaires);

		return ret;
	} // end getValeurAttributs

	/**
	 * Retourne la station de rattachement
	 * 
	 * @return la station de rattachement
	 */
	public Station getStation() {
		return this.station;
	} // end getStation

	/**
	 * Modifie la station de rattachement
	 * 
	 * @param _station
	 *            : la station de rattachement
	 */
	public void setStation(Station _station) {
		this.station = _station;
	} // end setStation

	/**
	 * Retourne le taxon
	 * 
	 * @return le taxon
	 */
	public RefTaxon getTaxon() {
		return this.taxon;
	} // end getTaxon

	/**
	 * Modifie le taxon
	 * 
	 * @param _taxon
	 *            : le nouveau taxon
	 */
	public void setTaxon(RefTaxon _taxon) {
		this.taxon = _taxon;
	} // end setTaxon

	/**
	 * Retourne le stade
	 * 
	 * @return le stade
	 */
	public RefStade getStade() {
		return this.stade;
	} // end getStade

	/**
	 * Modifie le stade
	 * 
	 * @param _stade
	 *            : le nouveau stade
	 */
	public void setStade(RefStade _stade) {
		this.stade = _stade;
	} // end setStade

	/**
	 * Retourne la date de d�but
	 * 
	 * @return la date de d�but
	 */
	public Date getDateDebut() {
		return this.dateDebut;
	} // end getDateDebut

	/**
	 * Modifie la date de d�but
	 * 
	 * @param _dateDebut
	 *            : la nouvelle date de d�but
	 */
	public void setDateDebut(Date _dateDebut) {
		this.dateDebut = _dateDebut;
	} // end setDateDebut

	/**
	 * Retourne la date de fin
	 * 
	 * @return la date de fin
	 */
	public Date getDateFin() {
		return this.dateFin;
	} // end getDateFin

	/**
	 * Modifie la date de fin
	 * 
	 * @param _dateFin
	 *            : la nouvelle date de fin
	 */
	public void setDateFin(Date _dateFin) {
		this.dateFin = _dateFin;
	} // end setDateFin

	/**
	 * Retourne la m�thode d'estimation
	 * 
	 * @return la m�thode d'estimation
	 */
	public String getMethodeEstimation() {
		return this.methodeEstimation;
	} // end getMethodeEstimation

	/**
	 * Modifie la m�thode d'estimation
	 * 
	 * @param _methodeEstimation
	 *            : la nouvelle m�thode d'estimation
	 */
	public void setMethodeEstimation(String _methodeEstimation) {
		this.methodeEstimation = _methodeEstimation;
	} // end setMethodeEstimation

	/**
	 * Retourne la valeur du taux d'�chappement
	 * 
	 * @return la valeur du taux d'�chappement
	 */
	public Integer getValeur() {
		return this.valeur;
	} // end getValeur

	/**
	 * Modifie la valeur du taux d'�chappement
	 * 
	 * @param _valeur
	 *            : la nouvelle valeur du taux d'�chappement
	 */
	public void setValeur(Integer _valeur) {
		this.valeur = _valeur;
	} // end setValeur

	/**
	 * Retourne le niveau d'echappement
	 * 
	 * @return le niveau d'�chappement
	 */
	public RefNiveauEchappement getNiveauEchappement() {
		return this.niveauEchappement;
	} // end getNiveauEchappement

	/**
	 * Modifie le niveau d'echappement
	 * 
	 * @param _niveauEchappement
	 *            : le nouveau niveau d'echappement
	 */
	public void setNiveauEchappement(RefNiveauEchappement _niveauEchappement) {
		this.niveauEchappement = _niveauEchappement;
	} // end setNiveauEchappement

	/**
	 * Acc�s aux commentaires
	 * 
	 * @return les commentaires
	 */
	public String getCommentaires() {
		return this.commentaires;
	}

	/**
	 * Modifie les commentaires
	 * 
	 * @param commentaires
	 *            : les nouveaux commentaires
	 */
	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}

	/**
	 * Affichage d'un taux d'�chappement
	 * 
	 * @return la chaine de caract�re qui sera affich�
	 */
	public String toString() {
		String res = "";

		SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		String sep = "  |  ";
		res += simpleDate.format(this.getDateDebut()) + sep;
		res += simpleDate.format(this.getDateFin()) + sep;
		res += taxon.getLibelle() + sep;
		res += stade.getLibelle() + sep;
		res += this.getMethodeEstimation() + sep;

		if (this.niveauEchappement.getCode() != null)
			res += "Niveau = " + this.getNiveauEchappement().getCode() + sep;
		else
			res += "Valeur = " + this.getValeur() + sep;

		res += this.commentaires + "    ";
		return res;
	}
} // end TauxEchappement
