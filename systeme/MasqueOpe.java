/**
 * 
 */
package systeme;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;

/**
 * Classe masque des operations Cette classe contient les informations sur
 * l'affichage et les valeurs par d�faut pour les masques op�ration
 * 
 * @author C�dric Briand
 */

public class MasqueOpe implements IBaseDonnees {

	// /////////////////////////////////////
	// attributes
	// /////////////////////////////////////
	// mao_mas_id character varying(15) NOT NULL,
	// mao_affichage boolean[],
	// 1 Station
	// 2 Ouvrage
	// 3 DF
	// 4 DC
	// 5 heuredebut
	// 6 heurefin
	// 7 operateur
	// 8 organisme
	// 9 heurerearmement
	// -----------------------------------------------------
	// les �lements logiques
	// --------------------------------------------------
	// 9 is_debut_auto (oui non)
	// 11 is_fin_auto (valeur oui non dans le combo)
	// 12 is_rearmement (La date de r�armement sert � renseigner le d�but de
	// l'op�ration suivante si debutauto)
	// 14 is_rearmementDF la valeur sert aussi � renseigner le DF par seulement
	// le DC
	// -----------------------------------------------------
	// les �lements qui n'ont pas de valeur pas d�faut
	// --------------------------------------------------
	// 15 datedebut
	// 16 datefin
	// 17 commentaires
	//

	// mao_valeurdefaut String[]
	// 1 Station
	// 2 Ouvrage
	// 3 DF
	// 4 DC
	// 5 heuredebut
	// 6 heurefin
	// 7 operateur
	// 8 organisme
	// 9 heurerearmement

	private Masque masque;
	private Boolean[] affichage;
	private String[] valeurDefaut;

	// /////////////////////////////////////
	// constructors
	// /////////////////////////////////////
	/**
	 * Construit une classe tous les attributs,
	 * 
	 * @param masque
	 *            le masque
	 * @param affichage
	 *            vecteur indiquant si les infos sont affich�es ou non
	 * @param valeurDefaut
	 *            valeur par d�faut (�ventuellement) du masque
	 */
	public MasqueOpe(Masque _masque, Boolean[] _affichage, String[] _valeurDefaut) {
		this.masque = _masque;
		this.affichage = _affichage;
		this.valeurDefaut = _valeurDefaut;
	}

	/**
	 * Construit une classe avec seulement le masque,
	 * 
	 * @param masque
	 *            le masque
	 * @param affichage
	 *            vecteur indiquant si les infos sont affich�es ou non
	 * @param valeurDefaut
	 *            valeur par d�faut (�ventuellement) du masque
	 */
	public MasqueOpe(Masque _masque) {
		this.masque = _masque;
	}

	// /////////////////////////////////////
	// interfaces
	// /////////////////////////////////////

	/**
	 * insertion des objets de la table utilise la m�thode executeInsertssorg
	 * car pas d'organisme dans les tables syst�me
	 * 
	 * @see commun.IBaseDonnees#insertObjet()
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		ArrayList nomAttributs = this.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		System.out.println("MasqueOpe : insertObjet()");
		ConnexionBD.getInstance().executeInsertWithPreparedStatement(table, nomAttributs, valAttributs);
	}

	/**
	 * M�thode de mise � jour des objets
	 * 
	 * @see commun.IBaseDonnees#majObjet()
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	@Override
	public void majObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		String[] nomAttributsSelection = MasqueOpe.getNomID();
		Object[] valAttributsSelection = this.getValeurID();
		ArrayList nomAttributs = this.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		System.out.println("MasqueOpe : majObjet()");

		ConnexionBD.getInstance().executeUpdatesWithPreparedstatement(table, nomAttributsSelection,
				valAttributsSelection, nomAttributs, valAttributs);

	}

	// /**
	// * Mise a jour d'un masque d'op�ration � partir d'une instance de la
	// classe
	// * Note cette m�thode n'est pas une m�thode de l'interface
	// * @param old : l'ancienne masque ope
	// * @throws SQLException
	// * @throws ClassNotFoundException
	// */
	// public void majObjet(MasqueOpe old) throws SQLException,
	// ClassNotFoundException {
	// String table = this.getNomTable();
	// String[] nomID = MasqueOpe.getNomID();
	// Object[] valueID = this.getValeurID();
	// ArrayList<String> nomsAttributs = this.getNomAttributs();
	// ArrayList<Object> valeursAttributs =this.getValeurAttributs();
	//
	// ConnexionBD.getInstance().executeUpdate(table, nomID, valueID,
	// nomsAttributs, valeursAttributs);
	// }

	/**
	 * Suppression d'un objet de la table
	 */
	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		String[] nomID = MasqueOpe.getNomID();
		Object[] valueID = this.getValeurID();
		System.out.println("MasqueOpe : suppObjet()");
		ConnexionBD.getInstance().executeDelete(table, nomID, valueID);
	}

	/**
	 * Acc�s � l'id du masque (id h�rit� de la table masque *
	 * 
	 * @return un tableau avec l'identifiant du masque (qui est le m�me pour le
	 *         masque lot)
	 */
	private Object[] getValeurID() {
		Object[] res = new Object[1];
		res[0] = this.masque.getIdentifiant();
		return res;
	}

	/**
	 * Acc�s aux noms des attributs qui forment la cl� primaire
	 * 
	 * @return un tableau avec les noms des attributs qui forment la cl�
	 *         primaire
	 */
	private static String[] getNomID() {
		String[] res = new String[1];
		res[0] = "mao_mas_id";
		return res;
	}

	/**
	 * verifie les attributs des champs
	 * 
	 * @see commun.IBaseDonnees#verifAttributs()
	 * @throws DataFormatException()
	 */
	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;
		if (!Verification.isInteger(this.masque.getIdentifiant(), true)) {
			throw new DataFormatException(Erreur.S27000);
		}
		if (!Verification.isVectorLength(this.valeurDefaut, 9, false)) {
			throw new DataFormatException(Erreur.I1014);
			// probl�me de taille des vecteurs
		}
		if (!Verification.isVectorLength(this.affichage, 16, false)) {
			throw new DataFormatException(Erreur.I1014);
			// probl�me de taille des vecteurs
		}

		return (ret);
	}

	// /////////////////////////////////////
	// operations
	// /////////////////////////////////////
	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return le nom
	 */
	private String getNomTable() {
		return "ts_masqueope_mao";
	}

	/**
	 * Retourne le nom des champs de la table
	 * 
	 * @return le noms des champs de la table dans l'ordre d'insertion dans la
	 *         base de donnee
	 */
	private ArrayList<String> getNomAttributs() {
		ArrayList<String> ret = new ArrayList<String>();
		ret.add("mao_mas_id");
		ret.add("mao_affichage");
		ret.add("mao_valeurdefaut");
		return ret;
	}

	/**
	 * Retourne la valeur des attributs de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	private ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret = new ArrayList<Object>();
		ret.add(this.masque.getIdentifiant());
		ret.add(this.affichage);
		ret.add(this.valeurDefaut);
		return ret;
	}

	/**
	 * Affichage d'une Liste masque = son code Attention si cette m�thode n'est
	 * pas pr�sente, le code correct du masque ne s'affiche pas et dans le combo
	 * on a pas les bonnes r�f�rences
	 */
	public String toString() {
		return this.masque.getCode();
	}

	/**
	 * @return the masque
	 */
	public Masque getMasque() {
		return masque;
	}

	/**
	 * @param masque
	 *            the masque to set
	 */
	public void setMasque(Masque masque) {
		this.masque = masque;
	}

	/**
	 * @return the affichage
	 */
	public Boolean[] getAffichage() {
		return affichage;
	}

	/**
	 * @param affichage
	 *            the affichage to set
	 */
	public void setAffichage(Boolean[] affichage) {
		this.affichage = affichage;
	}

	/**
	 * @return the valeurDefaut
	 */
	public String[] getValeurDefaut() {
		return valeurDefaut;
	}

	/**
	 * @param valeurDefaut
	 *            the valeurDefaut to set
	 */
	public void setValeurDefaut(String[] valeurDefaut) {
		this.valeurDefaut = valeurDefaut;
	}

}
