/**
 * 
 */
package systeme;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;

/**
 * @author cedric.briand
 *
 */
public class MasqueLot implements IBaseDonnees {
	/*
	 * Affichage 0 taxon 1 stade 2 quantite 3 type-quantite 4 methode_obtention
	 * 5 devenir 6 commentaires 7 pathologie 8 localisation 9 code pr�l�vement
	 * 10 op�rateur pr�l�vement 11 type_pr�l�vement 12 localisation pr�l�vement
	 * 13 commentaire pr�l�vement 14 operation_marquage 15 localisation marquage
	 * 16 nature marquage 17 action marquage 18 commentaire marquage 19 effectif
	 * 20 importance patho
	 * 
	 * Valeurs d�faut 0 quantite 1 type quantite 2 m�thode obtention 3 devenir 4
	 * code pr�l�vement 5 op�rateur pr�l�vement 6 type pr�l�vement 7
	 * localisation pr�l�vement 8 operation marquage 9 localisation 10 nature
	 * marque 11 action marquage 12 effectif 13 importance patho
	 * 
	 */

	private Masque masque;
	private Boolean[] affichage;

	public MasqueLot(Masque _masque) {
		this.masque = _masque;
	}

	public MasqueLot(Masque _masque, Boolean[] _affichage, String[] _valeurdefaut) {
		this.masque = _masque;
		this.affichage = _affichage;
		this.valeurDefaut = _valeurdefaut;
	}

	// /////////////////////////////////////
	// interfaces
	// /////////////////////////////////////

	/**
	 * insertion des objets de la table utilise la m�thode executeInsertssorg
	 * car pas d'organisme dans les tables syst�me
	 * 
	 * @see commun.IBaseDonnees#insertObjet()
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		ArrayList nomAttributs = this.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		System.out.println("MasqueOpe : insertObjet()");
		ConnexionBD.getInstance().executeInsertWithPreparedStatement(table, nomAttributs, valAttributs);
	}

	/**
	 * M�thode de mise � jour des objets
	 * 
	 * @see commun.IBaseDonnees#majObjet()
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	@Override
	public void majObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		String[] nomAttributsSelection = this.getNomID();
		Object[] valAttributsSelection = this.getValeurID();
		ArrayList nomAttributs = this.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		System.out.println("MasqueOpe : majObjet()");

		ConnexionBD.getInstance().executeUpdatesWithPreparedstatement(table, nomAttributsSelection,
				valAttributsSelection, nomAttributs, valAttributs);

	}

	/**
	 * Suppression d'un objet de la table
	 */
	@Override
	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		String[] nomID = this.getNomID();
		Object[] valueID = this.getValeurID();
		System.out.println("MasqueOpe : suppObjet()");
		ConnexionBD.getInstance().executeDelete(table, nomID, valueID);
	}

	/**
	 * Acc�s � l'id du masque (id h�rit� de la table masque
	 * 
	 * @return un tableau avec l'identifiant du masque
	 */
	private Object[] getValeurID() {
		Object[] res = new Object[1];
		res[0] = this.masque.getIdentifiant();
		return res;
	}

	/**
	 * Acc�s aux noms des attributs qui forment la cl� primaire
	 * 
	 * @return un tableau avec les noms des attributs qui forment la cl�
	 *         primaire
	 */
	private static String[] getNomID() {
		String[] res = new String[1];
		res[0] = "mal_mas_id";
		return res;
	}

	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return le nom
	 */
	private String getNomTable() {
		return "ts_masquelot_mal";
	}

	/**
	 * Retourne le nom des champs de la table
	 * 
	 * @return le noms des champs de la table dans l'ordre d'insertion dans la
	 *         base de donnee
	 */
	private ArrayList<String> getNomAttributs() {
		ArrayList<String> ret = new ArrayList<String>();
		ret.add("mal_mas_id");
		ret.add("mal_affichage");
		ret.add("mal_valeurdefaut");
		return ret;
	}

	/**
	 * Retourne la valeur des attributs de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	private ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret = new ArrayList<Object>();
		ret.add(this.masque.getIdentifiant());
		ret.add(this.affichage);
		ret.add(this.valeurDefaut);
		return ret;
	}

	/**
	 * verifie les attributs des champs
	 * 
	 * @see commun.IBaseDonnees#verifAttributs()
	 * @throws DataFormatException()
	 */
	@Override
	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;
		if (!Verification.isInteger(this.masque.getIdentifiant(), true)) {
			throw new DataFormatException(Erreur.S27000);
		}
		if (!Verification.isVectorLength(this.valeurDefaut, 14, false)) {
			throw new DataFormatException(Erreur.I1014);
			// probl�me de taille des vecteurs
		}
		if (!Verification.isVectorLength(this.affichage, 21, false)) {
			throw new DataFormatException(Erreur.I1014);
			// probl�me de taille des vecteurs
		}

		return (ret);
	}

	// getters and setters
	/**
	 * @return the affichage
	 */
	public Boolean[] getAffichage() {
		return affichage;
	}

	/**
	 * @param affichage
	 *            the affichage to set
	 */
	public void setAffichage(Boolean[] affichage) {
		this.affichage = affichage;
	}

	/**
	 * @return the valeurDefaut
	 */
	public String[] getValeurDefaut() {
		return valeurDefaut;
	}

	/**
	 * @param valeurDefaut
	 *            the valeurDefaut to set
	 */
	public void setValeurDefaut(String[] valeurDefaut) {
		this.valeurDefaut = valeurDefaut;
	}

	/**
	 * @param masque
	 *            the masque to set
	 */
	public void setMasque(Masque masque) {
		this.masque = masque;
	}

	private String[] valeurDefaut;

	public Masque getMasque() {
		return this.masque;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.masque.getCode();
	}

}
