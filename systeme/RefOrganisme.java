/**
 * 
 */
package systeme;

import java.sql.SQLException;

import commun.Ref;

/**
 * Organisme producteur de donn�es
 * @author S�bastien Laigre
 */
public class RefOrganisme extends Ref
{
	/**
	 * Construction d'un organisme � partir de son code 
	 * @param code le code de l'organisme
	 */
	public RefOrganisme(String code)
	{
		super(code);
	}
	
	/**
	 * Construction d'un organisme � partir de son code et libell�
	 * @param code : le code de l'organisme
	 * @param description : la description de l'organisme
	 */
	public RefOrganisme(String code, String description)
	{
		super(code, description);
	}

	@Override
	public void insertObjet() throws SQLException, ClassNotFoundException 
	{ }

	@Override
	public void majObjet() throws SQLException, ClassNotFoundException 
	{ }
	
	/**
	 * Affichage d'un organisme
	 * @return le code de l'organisme (sous forme de string) 
	 */
	public String toString()
	{
		return this.code;
	}
	
//	/**
//	 * Acc�s au nom de la table des utilisateurs
//	 * @return le nom de la table des utilisateurs
//	 */
//	public static String getNomTable()
//	{
//		return "ts_organisme_org";
//	}
//	
//	/**
//	 * Retourne la valeur de la cl� primaire d'un organisme
//	 * @return la valeur de la cl� primaire d'un organisme
//	 */
//	public Object[] getValeurID()
//	{
//		Object[] res = new Object[2];
//		
//		return res;
//	}
//	
//	/**
//	 * Retourne le nom de la cl� primaire d'un organisme
//	 * @return le nom de la cl� primaire d'un organisme
//	 */
//	public static String[] getNomID()
//	{
//		String[] res = new String[2];
//		
//		return res;
//	}
//	
//	/**
//	 * Retourne les noms des champs d'un organisme
//	 * @return les noms des champs d'un organisme
//	 */
//	public static ArrayList<String> getNomAttributes()
//	{
//		ArrayList<String> res = new ArrayList<String>();
//		
//		return res;
//	}
//	
//	/**
//	 * Acc�s aux valeurs de l'organisme
//	 * @return les valeurs de l'organisme
//	 */
//	public static ArrayList<Object> getValeurAttributes()
//	{
//		ArrayList<Object> res = new ArrayList<Object>();
//		
//		return res;
//	}

}
