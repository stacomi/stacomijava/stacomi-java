/**
 * 
 */
package systeme.referenciel;

import java.sql.ResultSet;

import commun.ConnexionBD;
import commun.SousListe;
import infrastructure.Station;
import infrastructure.StationMesure;
import migration.referenciel.RefParametre;
import migration.referenciel.RefParametreQualitatif;
import migration.referenciel.RefParametreQuantitatif;
import systeme.CondEnvOpe;
import systeme.MasqueOpe;

/**
 * SousListe de l'ensemble des affichages de stations de mesures (condenvope)
 * rattach�es � un masqueope
 * 
 * @author cedric.briand
 *
 */
public class SousListeCondEnvOpe extends SousListe {

	/**
	 * 
	 */

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * 
	 * @param _objetDeRattachement
	 *            l'objet de rattachement
	 */
	public SousListeCondEnvOpe(MasqueOpe _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeOuvrages

	/*
	 * (non-Javadoc)
	 * 
	 * @see commun.SousListe#chargeSansFiltre()
	 */
	@Override
	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;
		MasqueOpe masqueope = (MasqueOpe) super.getObjetDeRattachement();
		String mas_code = this.getObjetDeRattachementID()[0];
		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT mas_id, mas_code, mae_stm_identifiant, mae_affichage, mae_valeurdefaut FROM "
				+ ConnexionBD.getSchema() + "ts_masque_mas" + " JOIN " + ConnexionBD.getSchema()
				+ "ts_masqueope_mao on mao_mas_id=mas_id" + " JOIN " + ConnexionBD.getSchema()
				+ "ts_masqueconditionsenvironnementales_mae on mae_mao_id=mas_id" + " WHERE mas_code = '" + mas_code
				+ "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		// le masque ne change pas

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci

		while (rs.next()) {
			int mae_stm_identifiant = rs.getInt("mae_stm_identifiant");
			String cle = mas_code + mae_stm_identifiant;
			StationMesure stationmesure = new StationMesure(mae_stm_identifiant);
			Boolean affichage = rs.getBoolean("mae_affichage");
			String valeurdefaut = rs.getString("mae_valeurdefaut");
			CondEnvOpe condenvope = new CondEnvOpe(masqueope, stationmesure, affichage, valeurdefaut);
			this.put(cle, condenvope);
		} // end while

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see commun.SousListe#chargeObjet(java.lang.Object)
	 */
	@Override
	protected void chargeObjet(Object _objet) throws Exception {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc) Cette m�thode charge �galement les d�tails des stations de
	 * mesure
	 * 
	 * @see commun.SousListe#chargeSansFiltreDetails()
	 */
	@Override
	public void chargeSansFiltreDetails() throws Exception {
		// TODO Auto-generated method stub
		ResultSet rs = null;
		MasqueOpe masqueope = (MasqueOpe) super.getObjetDeRattachement();
		String mas_code = this.getObjetDeRattachementID()[0];
		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT mas_id, mas_code, mae_stm_identifiant, mae_affichage, mae_valeurdefaut, "
				+ "stm_libelle, stm_sta_code, stm_par_code, stm_description, sta_nom, par_nom" + " FROM "
				+ ConnexionBD.getSchema() + "ts_masque_mas" + " JOIN " + ConnexionBD.getSchema()
				+ "ts_masqueope_mao on mao_mas_id=mas_id" + " JOIN " + ConnexionBD.getSchema()
				+ "ts_masqueconditionsenvironnementales_mae on mae_mao_id=mas_id" + " JOIN " + ConnexionBD.getSchema()
				+ "tj_stationmesure_stm on stm_identifiant=mae_stm_identifiant" + " JOIN " + ConnexionBD.getSchema()
				+ "t_station_sta on sta_code=stm_sta_code" + " JOIN "
				+ "ref.tg_parametre_par par ON stm_par_code=par.par_code " + " WHERE mas_code = '" + mas_code + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		// le masque ne change pas

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci

		while (rs.next()) {
			int mae_stm_identifiant = rs.getInt("mae_stm_identifiant");
			String stm_libelle = rs.getString("stm_libelle");
			String stm_sta_code = rs.getString("stm_sta_code");
			String sta_nom = rs.getString("sta_nom");
			String stm_description = rs.getString("stm_description");
			Station station = new Station(stm_sta_code, sta_nom);
			String stm_par_code = rs.getString("stm_par_code");
			String par_nom = rs.getString("par_nom");
			RefParametre parametre;
			if (RefParametre.estQualitatif(stm_par_code)) {
				parametre = new RefParametreQualitatif(stm_par_code);
			} else {
				parametre = new RefParametreQuantitatif(stm_par_code);
			}
			parametre.setLibelle(par_nom);
			String cle = mas_code + mae_stm_identifiant;
			StationMesure stationmesure = new StationMesure(mae_stm_identifiant, stm_libelle, station, parametre,
					stm_description);
			Boolean affichage = rs.getBoolean("mae_affichage");
			String valeurdefaut = rs.getString("mae_valeurdefaut");
			CondEnvOpe condenvope = new CondEnvOpe(masqueope, stationmesure, affichage, valeurdefaut);
			this.put(cle, condenvope);
		} // end while

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see commun.SousListe#getObjetDeRattachementID()
	 */
	@Override
	public String[] getObjetDeRattachementID() {
		// L'identifiant de l'objet de rattachement est le code du masque
		String ret[] = new String[1];
		ret[0] = ((MasqueOpe) super.objetDeRattachement).getMasque().getCode();
		return ret;
	}

}
