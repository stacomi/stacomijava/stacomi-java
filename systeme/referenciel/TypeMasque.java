package systeme.referenciel;



/**
 * M�thode d'obtention
 * retourne les deux types de masque lot ou ope
 * pas besoin d'un r�f�rentiel pour deux masques....
 * @author C�dric BRIAND
 */
public class TypeMasque {

  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////


    private static final String[] typemasque = {"lot", "ope"} ;




  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    

    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////

    /**
     * Retourne les types de masques
     * On va pas mettre en base de donn�es pour deux valeurs
     * @return les methodes
     */
        public static String[] getTypeMasque() {    
            return TypeMasque.typemasque ;
            
        } 
        

 } 



