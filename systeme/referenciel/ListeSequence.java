/*
 **********************************************************************
 * 
 * Nom fichier :        ListeSequence.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * @author             	S�bastien Laigre
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   12 juin 2009
 * Compatibilite :      Java 6/Windows XP/PostgreSQL 8.3
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * @version
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package systeme.referenciel;

import java.sql.ResultSet;

import systeme.Sequence;
import commun.ConnexionBD;
import commun.Erreur;
import commun.Liste;

/**
 * Liste de s�quences
 * @author S�bastien Laigre
 */
public class ListeSequence extends Liste
{
	/**  */
	private static final long serialVersionUID = 1L;

	/**
	 * Initialise une liste de s�quences
	 */
	public ListeSequence()
	{
		super();
	}

	@Override
	protected void chargeObjet(Object _objet) throws Exception 
	{
		ResultSet rs = null;
		
		String seq_sequence = ((String)_objet);
        if (seq_sequence == null)
            throw new NullPointerException(Erreur.I1000) ;

		String sql = "SELECT seq_table, seq_column FROM ref.ts_sequence_seq " +
				"WHERE org_code = '"+seq_sequence+"'";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

        // En principe, il y a un et un seul enregistrement
        if (rs.first() == false)
            throw new Exception(Erreur.I1001) ;

		while(rs.next())
		{
			String seq_table = rs.getString("seq_table");
			String seq_column = rs.getString("seq_column");
			
			Sequence seq = new Sequence(seq_sequence, seq_table, seq_column);
			this.put(seq_sequence, seq);
		}
	}

	@Override
	public void chargeSansFiltre() throws Exception 
	{
		ResultSet rs = null;
		
		String sql = "SELECT seq_sequence FROM ref.ts_sequence ";
		
		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		
		while(rs.next())
		{
			String seq_sequence = rs.getString("seq_sequence");
			
			Sequence seq = new Sequence(seq_sequence);
			this.put(seq_sequence, seq);
		}
	}

	@Override
	public void chargeSansFiltreDetails() throws Exception 
	{
		ResultSet rs = null;
		
		String sql = "SELECT seq_sequence, seq_table, seq_column " +
				"FROM ref.ts_sequence_seq; ";
		
		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		
		while(rs.next())
		{
			String seq_sequence = rs.getString("seq_sequence");
			String seq_table = rs.getString("seq_table");
			String seq_column = rs.getString("seq_column");
			
			Sequence seq = new Sequence(seq_sequence, seq_table, seq_column);
			this.put(seq_sequence, seq);
		}
	}
}
