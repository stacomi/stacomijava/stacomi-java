/*
 **********************************************************************
 *
 *
 */


package systeme.referenciel;



/**
 * ValeurComboMasque
 * Classe utilis�e pour les getter qui renvoient des listes de valeur possibles pour
 * les combos des masques
 * @author C�dric BRIAND
 */
public class ValeursComboMasque {

  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////


    private static final String[] choixdatedebut = {"Non","Oui"} ;
    private static final String[] choixdatefin = {"Non","Oui"} ;
    private static final String[] choixrearmement = {"Non","Oui"} ;




  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    

    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////

    /**
     * Retourne les choix possibles pour la date de d�but
     * On va pas mettre en base de donn�es pour deux valeurs
     * @return le choix dans la date
     */
        public static String[] getchoixdatedebut() {    
            return ValeursComboMasque.choixdatedebut ;
            
        } 
        /**
         * Retourne les choix possibles pour la date de fin
         * On va pas mettre en base de donn�es pour deux valeurs
         * @return le choix dans la date
         */
            public static String[] getchoixdatefin() {    
                return ValeursComboMasque.choixdatefin ;
                
            }        
            /**
             * Retourne les choix possibles le r�armement du dispositif
             */
                public static String[] getchoixrearmement() {    
                    return ValeursComboMasque.choixrearmement ;
                    
                }        
         
 } 



