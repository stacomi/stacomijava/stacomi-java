/**
 * 
 */
package systeme.referenciel;

import java.sql.ResultSet;

import commun.ConnexionBD;
import commun.SousListe;
import systeme.AffichageMasqueLot;
import systeme.MasqueLot;

/**
 * SousListe de l'ensemble des affichages de masqueordreaffichage rattach�es �
 * un masquelot
 * 
 * @author cedric.briand
 *
 */
public class SousListeAffichageMasqueLot extends SousListe {

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * 
	 * @param _objetDeRattachement
	 *            l'objet de rattachement
	 */
	public SousListeAffichageMasqueLot(MasqueLot _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeOuvrages

	/*
	 * (non-Javadoc)
	 * 
	 * @see commun.SousListe#chargeSansFiltre()
	 */
	@Override
	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;
		MasqueLot masquelot = (MasqueLot) super.getObjetDeRattachement();
		String mas_code = this.getObjetDeRattachementID()[0];
		// Extraction de tous les enregistrements de la table concernee
		String sql = "SELECT ts_masqueordreaffichage_maa.* FROM " + ConnexionBD.getSchema() + "ts_masque_mas" + " JOIN "
				+ ConnexionBD.getSchema() + "ts_masquelot_mal on mal_mas_id=mas_id" + " JOIN " + ConnexionBD.getSchema()
				+ "ts_masqueordreaffichage_maa on maa_mal_id=mas_id" + " WHERE mas_code = '" + mas_code
				+ "' ORDER BY maa_table,maa_champdumasque,maa_rang;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while (rs.next()) {

			String maa_table = rs.getString("maa_table");
			String maa_valeur = rs.getString("maa_valeur");
			String maa_champdumasque = rs.getString("maa_champdumasque");
			// Integer maa_rang = rs.getInt("maa_rang");
			String cle = maa_table + "_" + maa_champdumasque + "_" + maa_valeur + "_" + masquelot.getMasque().getCode();
			AffichageMasqueLot aml = new AffichageMasqueLot(masquelot, maa_table, maa_valeur, maa_champdumasque);
			this.put(cle, aml);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see commun.SousListe#chargeObjet(java.lang.Object)
	 */
	@Override
	protected void chargeObjet(Object _objet) throws Exception {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc) Cette m�thode charge �galement les d�tails des stations de
	 * mesure
	 * 
	 * @see commun.SousListe#chargeSansFiltreDetails()
	 */
	@Override
	public void chargeSansFiltreDetails() throws Exception {
		// TODO ?

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see commun.SousListe#getObjetDeRattachementID()
	 */
	@Override
	public String[] getObjetDeRattachementID() {
		// L'identifiant de l'objet de rattachement est le code du masque
		String ret[] = new String[1];
		ret[0] = ((MasqueLot) super.objetDeRattachement).getMasque().getCode();
		return ret;
	}

	/**
	 * construit un masquelot avec uniquement les �l�ments du combo
	 * 
	 * @param champdumasque
	 * @return
	 */
	public static SousListeAffichageMasqueLot get_elements_list(String champdumasque, SousListeAffichageMasqueLot ssl) {
		SousListeAffichageMasqueLot lesaffichagesducombo = new SousListeAffichageMasqueLot(
				(MasqueLot) ssl.objetDeRattachement);
		for (int i = 0; i < ssl.size(); i++) {
			AffichageMasqueLot aml = (AffichageMasqueLot) ssl.get(i);
			if (aml.getChampdumasque().equals(champdumasque)) {
				String maa_table = aml.getTable();
				String maa_champdumasque = aml.getChampdumasque();
				String maa_valeur = aml.getValeur();
				String maa_code = aml.getMasquelot().getMasque().getCode();
				String cle = maa_table + "_" + maa_champdumasque + "_" + maa_valeur + "_" + maa_code;
				lesaffichagesducombo.put(cle, aml);
			}

		}
		return (lesaffichagesducombo);
	}
}
