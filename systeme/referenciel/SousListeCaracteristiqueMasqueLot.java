/**
 * 
 */
package systeme.referenciel;

import java.sql.ResultSet;

import commun.ConnexionBD;
import commun.SousListe;
import migration.Caracteristique;
import migration.referenciel.RefParametre;
import migration.referenciel.RefParametreQualitatif;
import migration.referenciel.RefParametreQuantitatif;
import migration.referenciel.RefValeurParametre;
import systeme.CaracteristiqueMasqueLot;
import systeme.MasqueLot;

/**
 * SousListe de l'ensemble des affichages de stations de mesures (condenvope)
 * rattach�es � un masqueope
 * 
 * @author cedric.briand
 *
 */
public class SousListeCaracteristiqueMasqueLot extends SousListe {

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * 
	 * @param _objetDeRattachement
	 *            l'objet de rattachement
	 */
	public SousListeCaracteristiqueMasqueLot(MasqueLot _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeOuvrages

	/*
	 * (non-Javadoc)
	 * 
	 * @see commun.SousListe#chargeSansFiltre()
	 */
	@Override
	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;
		MasqueLot masquelot = (MasqueLot) super.getObjetDeRattachement();
		String mas_code = this.getObjetDeRattachementID()[0];
		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT ts_masquecaracteristiquelot_mac.* FROM " + ConnexionBD.getSchema() + "ts_masque_mas"
				+ " JOIN " + ConnexionBD.getSchema() + "ts_masquelot_mal on mal_mas_id=mas_id" + " JOIN "
				+ ConnexionBD.getSchema() + "ts_masquecaracteristiquelot_mac on mac_mal_id=mas_id"
				+ " WHERE mas_code = '" + mas_code + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		// le masque ne change pas

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci

		while (rs.next()) {

			String par_code = rs.getString("mac_par_code");
			String cle = par_code + masquelot.getMasque().getCode();
			RefParametre refpar;
			Float valeurparametrequantitatif = null;
			Float precision = rs.getFloat("mac_precisiondefaut");
			if (rs.wasNull()) {
				precision = null;
			}
			String commentaire = rs.getString("mac_commentairedefaut");
			String methodeobtention = rs.getString("mac_methodeobtentiondefaut");
			RefValeurParametre refval = null;
			if (RefParametre.estQualitatif(par_code)) {
				refpar = new RefParametreQualitatif(par_code);
				Integer valeurparametrequalitatif = rs.getInt("mac_valeurqualitatifdefaut");
				refval = new RefValeurParametre(valeurparametrequalitatif);
			} else {
				refpar = new RefParametreQuantitatif(par_code);
				valeurparametrequantitatif = rs.getFloat("mac_valeurquantitatifdefaut");
				if (rs.wasNull()) {
					valeurparametrequantitatif = null;
				}
			}

			Caracteristique car = new Caracteristique(null, refpar, methodeobtention, valeurparametrequantitatif,
					refval, precision, commentaire);
			Boolean affichagecommentaire = rs.getBoolean("mac_affichagecommentaire");
			Boolean affichageprecision = rs.getBoolean("mac_affichageprecision");
			Boolean affichagemethodeobtention = rs.getBoolean("mac_affichagemethodeobtention");
			Boolean affichagevaleur = rs.getBoolean("mac_affichagevaleur");
			CaracteristiqueMasqueLot carmasquelot = new CaracteristiqueMasqueLot(masquelot, car, affichagevaleur,
					affichagecommentaire, affichageprecision, affichagemethodeobtention);
			this.put(cle, carmasquelot);
		} // end while

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see commun.SousListe#chargeObjet(java.lang.Object)
	 */
	@Override
	protected void chargeObjet(Object _objet) throws Exception {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc) Cette m�thode charge �galement les d�tails des stations de
	 * mesure, dont le nom et l'unit� du param�tre (n�cessaire pour l'affichage
	 * des lots)
	 * 
	 * @see commun.SousListe#chargeSansFiltreDetails()
	 */
	@Override
	public void chargeSansFiltreDetails() throws Exception {
		ResultSet rs = null;
		MasqueLot masquelot = (MasqueLot) super.getObjetDeRattachement();
		String mas_code = this.getObjetDeRattachementID()[0];
		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT ts_masquecaracteristiquelot_mac.*,par_nom,par_unite FROM " + ConnexionBD.getSchema()
				+ "ts_masque_mas" + " JOIN " + ConnexionBD.getSchema() + "ts_masquelot_mal on mal_mas_id=mas_id"
				+ " JOIN " + ConnexionBD.getSchema() + "ts_masquecaracteristiquelot_mac on mac_mal_id=mas_id" + " JOIN "
				+ "ref.tg_parametre_par on par_code=mac_par_code" + " WHERE mas_code = '" + mas_code + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		// le masque ne change pas

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci

		while (rs.next()) {

			String par_code = rs.getString("mac_par_code");
			String cle = par_code + masquelot.getMasque().getCode();
			RefParametre refpar;
			Float valeurparametrequantitatif = null;
			Float precision = rs.getFloat("mac_precisiondefaut");
			String commentaire = rs.getString("mac_commentairedefaut");
			String methodeobtention = rs.getString("mac_methodeobtentiondefaut");
			String par_nom = rs.getString("par_nom");
			String par_unite = rs.getString("par_unite");
			RefValeurParametre refval = null;
			if (RefParametre.estQualitatif(par_code)) {
				refpar = new RefParametreQualitatif(par_code, par_nom, par_unite);
				Integer valeurparametrequalitatif = rs.getInt("mac_valeurqualitatifdefaut");
				refval = new RefValeurParametre(valeurparametrequalitatif);
			} else {
				refpar = new RefParametreQuantitatif(par_code, par_nom, par_unite);
				valeurparametrequantitatif = rs.getFloat("mac_valeurquantitatifdefaut");
			}

			Caracteristique car = new Caracteristique(null, refpar, methodeobtention, valeurparametrequantitatif,
					refval, precision, commentaire);
			Boolean affichagecommentaire = rs.getBoolean("mac_affichagecommentaire");
			Boolean affichageprecision = rs.getBoolean("mac_affichageprecision");
			Boolean affichagemethodeobtention = rs.getBoolean("mac_affichagemethodeobtention");
			Boolean affichagevaleur = rs.getBoolean("mac_affichagevaleur");
			CaracteristiqueMasqueLot carmasquelot = new CaracteristiqueMasqueLot(masquelot, car, affichagevaleur,
					affichagecommentaire, affichageprecision, affichagemethodeobtention);
			this.put(cle, carmasquelot);
		} // end while

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see commun.SousListe#getObjetDeRattachementID()
	 */
	@Override
	public String[] getObjetDeRattachementID() {
		// L'identifiant de l'objet de rattachement est le code du masque
		String ret[] = new String[1];
		ret[0] = ((MasqueLot) super.objetDeRattachement).getMasque().getCode();
		return ret;
	}

}
