/*
 **********************************************************************
 *
 * Nom fichier :        ListeMarques.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package systeme.referenciel;

import java.sql.Array;
import java.sql.ResultSet;

import commun.ConnexionBD;
import commun.Erreur;
import commun.Liste;
import systeme.Masque;
import systeme.MasqueOpe;

/**
 * Liste pour les marques
 * 
 * @author C�dric Briand
 */
public class ListeMasqueOpe extends Liste {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une reference avec seulement un code
	 *
	 */
	public ListeMasqueOpe() {
		super();

	}

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	/**
	 * charge une liste avec seulement le code du masque
	 * 
	 * @see commun.Liste#chargeSansFiltre()
	 */
	@Override
	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		// Par d�faut le masque sera de type ope puisqu'il appara�t dans la
		// table ope
		String sql = "SELECT mas_code FROM " + ConnexionBD.getSchema() + "ts_masque_mas" + " JOIN "
				+ ConnexionBD.getSchema() + "ts_masqueope_mao on mao_mas_id=mas_id;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			String code = rs.getString("mas_code");
			// Un masqueOpe est compos� d'un objet masque et des vecteurs de
			// d�tail
			Masque masque = new Masque(code);
			MasqueOpe masqueope = new MasqueOpe(masque);
			this.put(code, masqueope);
		} // end while
	}

	/**
	 * Charge une liste avec tous les d�tails de la table
	 * 
	 * @see commun.Liste#chargeSansFiltreDetails()
	 */
	@Override
	public void chargeSansFiltreDetails() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT mas_code, mas_description,mas_raccourci,mas_type,"
				+ " mao_mas_id,mao_affichage,mao_valeurdefaut FROM " + ConnexionBD.getSchema() + "ts_masque_mas"
				+ "JOIN " + ConnexionBD.getSchema() + "ts_masqueope_mao on mao_mas_id=mas_id;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Donn�es relatives au masque

			String code = rs.getString("mas_code");
			String description = rs.getString("mas_description");
			String raccourci = rs.getString("mas_raccourci");
			String type = rs.getString("mas_type");

			// Donn�es relatives au masqueOpe

			Integer id = rs.getInt("mas_id");
			Array a = rs.getArray("mao_affichage");
			Boolean[] mao_affichage = (Boolean[]) a.getArray();
			Array v = rs.getArray("mao_valeurdefaut");
			String[] mao_valeurdefaut = (String[]) v.getArray();

			// Un masqueOpe est compos� d'un objet masque et des vecteurs de
			// d�tail
			Masque masque = new Masque(code, description, raccourci, type);
			MasqueOpe masqueope = new MasqueOpe(masque, mao_affichage, mao_valeurdefaut);
			this.put(code, masqueope);

		} // end while
	}

	/**
	 * charge les d�tails d'une liste � partir de son code cette m�thode est
	 * utilis�e par chargeFiltre
	 * 
	 * @see commun.Liste#chargeObjet(java.lang.Object)
	 */
	@Override
	protected void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		Masque masque = ((MasqueOpe) _objet).getMasque();
		String code = masque.getCode();
		if (code == null) {
			throw new NullPointerException(Erreur.I1000);
		}

		// Extraction de l'enregistrement concerne

		String sql = "SELECT mas_id,mas_code, mas_description, mas_raccourci, mas_type,"
				+ " mao_affichage, mao_valeurdefaut FROM " + ConnexionBD.getSchema() + "ts_masque_mas" + " JOIN "
				+ ConnexionBD.getSchema() + "ts_masqueope_mao on mao_mas_id=mas_id" + " WHERE mas_code = '" + code
				+ "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// En principe, il y a un ou zero
		boolean isMoreThanOneRow = rs.first() && rs.next();
		boolean isEmpty = !rs.first();
		if (isMoreThanOneRow) {
			throw new Exception(Erreur.I1001);
		} else if (isEmpty) {
			throw new Exception("Masque vide : remplissez les valeurs � l'aide de l'interface");
		} else {
			rs.first();
			Integer id = rs.getInt("mas_id");
			Array a = rs.getArray("mao_affichage");
			Boolean[] mao_affichage = (Boolean[]) a.getArray();
			Array v = rs.getArray("mao_valeurdefaut");
			String[] mao_valeurdefaut = (String[]) v.getArray();

			// Un masqueOpe est compos� d'un objet masque et des vecteurs de
			// d�tail
			MasqueOpe masqueope = new MasqueOpe(masque, mao_affichage, mao_valeurdefaut);
			this.put(code, masqueope);
		}
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Charge un masqueOpe � partir de la r�f�rence de son code Cr�e d'abord le
	 * masque et utilise l'instance pour charger les d�tails du masque � partir
	 * de la m�thode chargeObjet
	 * 
	 * @param _code
	 *            le code du masque
	 * @throws Exception
	 */

	public void chargeFiltre(String _code) throws Exception {

		// creation d'une nouvelle marque avec sa reference et ajout a la liste
		Masque masque = new Masque(_code);
		MasqueOpe masqueope = new MasqueOpe(masque);

		// chargement de cette marque
		this.chargeObjet(masqueope);

	} // end chargeFiltre

} // end ListeMasques
