/**
 * 
 */
package systeme.referenciel;

import java.sql.ResultSet;

import systeme.RefOrganisme;
import commun.ConnexionBD;
import commun.Erreur;
import commun.Liste;

/**
 * Liste permettant de stocker les utilisateurs
 * @author Sébastien Laigre
 */
public class ListeRefUtilisateur extends Liste
{
	/**  */
	private static final long serialVersionUID = 1L;

	/**
	 * Initialise une liste d'utilisateurs
	 */
	public ListeRefUtilisateur()
	{
		super();
	}

	@Override
	protected void chargeObjet(Object _objet) throws Exception 
	{
		ResultSet rs = null;
		
		String code = ((RefOrganisme)_objet).getCode();
        if (code == null)
            throw new NullPointerException(Erreur.I1000) ;

		String sql = "SELECT * FROM ref.ts_organisme_org " +
				"WHERE org_code = '"+code+"'";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

        // En principe, il y a un et un seul enregistrement
        if (rs.first() == false)
            throw new Exception(Erreur.I1001) ;

		while(rs.next())
		{
			String org_code = rs.getString("org_code");
			String org_description = rs.getString("org_description");
			
			RefOrganisme orga = new RefOrganisme(org_code, org_description);
			this.put(org_code, orga);
		}
	}

	@Override
	public void chargeSansFiltre() throws Exception 
	{
		ResultSet rs = null;
		
		String sql = "SELECT org_code FROM ref.ts_organisme_org;";
		
		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		
		while(rs.next())
		{
			String org_code = rs.getString("org_code");
			
			RefOrganisme orga = new RefOrganisme(org_code);
			this.put(org_code, orga);
		}
	}

	@Override
	public void chargeSansFiltreDetails() throws Exception 
	{
		ResultSet rs = null;
		
		String sql = "SELECT * FROM ref.ts_organisme_org;";
		
		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		
		while(rs.next())
		{
			String org_code = rs.getString("org_code");
			String org_description = rs.getString("org_description");
			
			RefOrganisme orga = new RefOrganisme(org_code, org_description);
			this.put(org_code, orga);
		}
	}
}
