/*
 **********************************************************************
 *
 * Nom fichier :        ListeMarques.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package systeme.referenciel;

import java.sql.ResultSet;

import commun.ConnexionBD;
import commun.Erreur;
import commun.Liste;
import infrastructure.StationMesure;
import systeme.CondEnvOpe;
import systeme.Masque;
import systeme.MasqueOpe;

/**
 * Liste des conditions environnementales rattach�es � l'op�ration
 * 
 * @author C�dric Briand
 */
public class ListeCondEnvOpe extends Liste {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Constructeur de la class liste avec un tuple vecteur et cl�
	 *
	 */
	public ListeCondEnvOpe() {
		super();

	}

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	/**
	 * charge une liste avec seulement le code du masque et la condition
	 * environnementales la cl� primaire est construite sur les deux
	 * 
	 * @see commun.Liste#chargeSansFiltre()
	 */
	@Override
	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		// Par d�faut le masque sera de type ope puisqu'il appara�t dans la
		// table ope
		String sql = "SELECT mas_code, mae_stm_identifiant FROM " + ConnexionBD.getSchema() + "ts_masque_mas" + "JOIN "
				+ ConnexionBD.getSchema() + "ts_masqueope_mao on mao_mas_id=mas_id" + "JOIN" + ConnexionBD.getSchema()
				+ "ts_masqueconditionsenvironnementales_mae on mae_mao_id=mas_id ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			String code = rs.getString("mas_code");
			int stm = rs.getInt("mae_stm_identifiant"); // la station de mesure
			StationMesure stationmesure = new StationMesure(stm);
			// todo on pourrait construire la cl� avec le libelle de la station
			// plutot que l'entier
			// mais c'est compliqu�, il faudrait passer par la sousListe pour
			// acc�der � la base
			// en l'�tat du code il faudrait charger la liste avec tous les
			// �lements puis chercher le bon
			// avec son code, c'est un peu compliqu� je laisse tomber
			// les deux composants forment une cl� primaire unique pour cr�er la
			// Liste (cle,valeur)
			String cle_condenv = code + "_" + stm;
			Masque masque = new Masque(code);
			MasqueOpe masqueope = new MasqueOpe(masque);
			CondEnvOpe condenv = new CondEnvOpe(masqueope, stationmesure);
			this.put(cle_condenv, condenv);
		} // end while
	}

	/**
	 * Charge une liste avec tous les d�tails de la table
	 * 
	 * @see commun.Liste#chargeSansFiltreDetails()
	 */
	@Override
	public void chargeSansFiltreDetails() throws Exception {
		ResultSet rs = null;

		// Extraction de touss les enregistrements de la table concernee
		String sql = "SELECT mas_code,	mae_stm_identifiant,mae_affichage, mae_valeurdefaut FROM "
				+ ConnexionBD.getSchema() + "ts_masque_mas" + "JOIN " + ConnexionBD.getSchema()
				+ "ts_masqueope_mao on mao_mas_id=mas_id" + "JOIN" + ConnexionBD.getSchema()
				+ "ts_masqueconditionsenvironnementales_mae on mae_mao_id=mas_id ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Donn�es

			String code = rs.getString("mas_code");
			int stm = rs.getInt("mae_stm_identifiant");
			Boolean affichage = rs.getBoolean("mae_affichage");
			String valeurdefaut = rs.getString("mae_valeurdefaut");
			String cle = code + "_" + stm;
			Masque masque = new Masque(code);
			MasqueOpe masqueope = new MasqueOpe(masque);
			StationMesure stationmesure = new StationMesure(stm);

			CondEnvOpe condenvope = new CondEnvOpe(masqueope, stationmesure, affichage, valeurdefaut);
			this.put(cle, condenvope);

		} // end while
	}

	/**
	 * charge les d�tails d'une liste � partir de son code cette m�thode est
	 * utilis�e par chargeFiltre une conditon environnementale pour le masque
	 * op�ration est construite � la fois sur l'identifiant de la station de
	 * mesure et le code du masque (une ligne par station de mesure et par
	 * masqueope dans la table)
	 * 
	 * @see commun.Liste#chargeObjet(java.lang.Object)
	 */
	@Override
	protected void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		MasqueOpe masqueope = ((CondEnvOpe) _objet).getMasqueope();
		String code = masqueope.getMasque().getCode();
		StationMesure stationmesure = ((CondEnvOpe) _objet).getStationmesure();
		Integer identifiantstm = stationmesure.getIdentifiant();
		if (code == null | stationmesure == null) {
			throw new NullPointerException(Erreur.I1000);
		}

		// Extraction de l'enregistrement concerne

		String sql = "SELECT mas_code,	mae_stm_identifiant,mae_affichage, mae_valeurdefaut FROM "
				+ ConnexionBD.getSchema() + "ts_masque_mas" + "JOIN " + ConnexionBD.getSchema()
				+ "ts_masqueope_mao on mao_mas_id=mas_id" + "JOIN" + ConnexionBD.getSchema()
				+ "ts_masqueconditionsenvironnementales_mae on mae_mao_id=mas_id" + "WHERE mas_code = '" + code
				+ "'and mae_stm_identifiant= " + identifiantstm + " ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// En principe, il y a un et un seul enregistrement
		if (rs.first() == false) {
			throw new Exception(Erreur.I1001);
		}
		String cle = code + rs.getString("mae_stm_identifiant");
		Boolean affichage = rs.getBoolean("mae_affichage");
		String valeurdefaut = rs.getString("mae_valeurdefaut");

		CondEnvOpe condenvope = new CondEnvOpe(masqueope, stationmesure, affichage, valeurdefaut);
		this.put(cle, condenvope);

	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Charge une ListeCondEnvOpe � partir de la r�f�rence du code du masque
	 * Cr�e d'abord le masque et utilise l'instance pour charger les d�tails du
	 * masque � partir de la m�thode chargeObjet
	 * 
	 * @param _code
	 *            le code du masque
	 * @throws Exception
	 */

	public void chargeFiltre(String _code) throws Exception {

		// creation d'une nouvelle marque avec sa reference et ajout a la liste
		Masque masque = new Masque(_code);
		this.put(_code, masque);

		// chargement de cette marque
		this.chargeObjet(masque);

	} // end chargeFiltre

} // end ListeMasques
