/*
 **********************************************************************
 *
 * Nom fichier :        ListeMarques.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package systeme.referenciel;

import java.sql.ResultSet;

import commun.ConnexionBD;
import commun.Erreur;
import commun.Liste;
import systeme.Masque;

/**
 * Liste pour les marques
 * 
 * @author C�dric Briand
 */
public class ListeMasque extends Liste {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une reference avec seulement un code
	 *
	 */
	public ListeMasque() {
		super();

	}

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	/**
	 * charge une liste avec seulement le code du masque
	 * 
	 * @see commun.Liste#chargeSansFiltre()
	 */
	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT mas_code FROM " + ConnexionBD.getSchema() + "ts_masque_mas order by mas_code;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			String code = rs.getString("mas_code");

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			Masque masque = new Masque(code);
			this.put(code, masque);

		} // end while

	}

	/**
	 * Charge une liste avec tous les d�tails de la table
	 * 
	 * @see commun.Liste#chargeSansFiltreDetails()
	 */
	public void chargeSansFiltreDetails() throws Exception {
		ResultSet rs = null;

		// Extraction de tous les enregistrements de la table concernee
		String sql = "SELECT mas_code, mas_description, mas_raccourci, mas_type " + "FROM " + ConnexionBD.getSchema()
				+ "ts_masque_mas order by mas_code;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet

			// Integer id = rs.getInt("mas_id") ;
			String code = rs.getString("mas_code");
			String description = rs.getString("mas_description");
			String raccourci = rs.getString("mas_raccourci");
			String type = rs.getString("mas_type");

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			Masque masque = new Masque(code, description, raccourci, type);
			this.put(code, masque);

		} // end while

	}

	/**
	 * charge les d�tails d'une liste � partir de son code
	 * 
	 * @see commun.Liste#chargeObjet(java.lang.Object)
	 */
	protected void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		// Si l'identifiant de l'objet n'est pas definit, erreur
		// ajout d'une contrainte not null dans la base.
		// construit sur le code et pas sur l'id
		String code = ((Masque) _objet).getCode();
		if (code == null) {
			throw new NullPointerException(Erreur.I1000);
		}

		// Extraction de l'enregistrement concerne
		String sql = "SELECT mas_id, mas_code, mas_description, mas_raccourci, mas_type " + "FROM "
				+ ConnexionBD.getSchema() + "ts_masque_mas " + "WHERE mas_code = '" + code + "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		if (!rs.isBeforeFirst()) {
			throw new Exception(
					"Le masque n'existe pas en base. Si vous avez chang� de sch�ma il faut reselectionner un masque.");
		}

		// En principe, il y a un et un seul enregistrement
		if (rs.first() == false) {
			throw new Exception(Erreur.I1001);
		}

		Integer id = rs.getInt("mas_id");
		String description = rs.getString("mas_description");
		String raccourci = rs.getString("mas_raccourci");
		String type = rs.getString("mas_type");

		// Creation de l'objet d'apres les champs lus et ajout a la liste
		Masque masque = new Masque(code, id, description, raccourci, type);
		this.put(code, masque);

	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Charge un masque � partir de la r�f�rence de son code
	 * 
	 * @param _code
	 *            le code du masque
	 * @throws Exception
	 */
	public void chargeFiltre(String _code) throws Exception {

		Masque masque = new Masque(_code);
		this.chargeObjet(masque);

	} // end chargeFiltre

	// /**
	// * Charge charge la liste des masques en fonction du type
	// * @param _type
	//
	// * @throws Exception
	// */
	// public void chargeFiltre(String _type) throws Exception {
	// ResultSet rs = null ;
	//
	// // Extraction de toutes les enregistrements de la table concernee
	// String sql = "SELECT mas_code " +
	// "FROM " + ConnexionBD.getSchema() + "ts_masque_mas " +
	// "WHERE mas_type = '" + _type + "' ;" ;
	// // type.getCode retourne le type (qui correspond au "code" dans le
	// r�f�rentiel
	//
	// rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
	//
	// // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
	// while (rs.next()) {
	//
	// // Lecture des champs dans le ResultSet
	// String code = rs.getString("mas_code") ;
	//
	// // Creation de l'objet d'apres les champs lus et ajout a la liste
	// Masque masque = new Masque(code) ;
	// this.put(code, masque) ;
	//
	// } // end while
	//
	// } // end chargeFiltre

} // end ListeMasques
