package systeme;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;
import infrastructure.StationMesure;

/**
 * Une instance de CondEnvOpe correspond � l'affichage d'une station de mesure
 * (ex temp�rature) dans un masque d'op�rations
 * 
 * @author cedric.briand
 *
 */
public class CondEnvOpe implements IBaseDonnees {

	// /////////////////////////////////////
	// attributes
	// /////////////////////////////////////
	// drop table if exists iav.ts_masqueconditionsenvironnementales_mae;
	// create table iav.ts_masqueconditionsenvironnementales_mae(
	// mae_mao_id integer,
	// mae_cond_environnementale integer ,
	// mae_affichage boolean[4], -- affichage des champs de chaque condition
	// environnementale
	// mae_valeurdefaut text[4],
	// CONSTRAINT c_pk_mae PRIMARY KEY (mae_mao_id,mae_cond_environnementale),--
	// il ne peut y avoir qu'une condition environnementale par masque (pas deux
	// fois la m�me)
	// CONSTRAINT c_ck_length_mae_affichage CHECK
	// (array_length(mae_affichage,1)=2), -- le vecteur est de longueur 2
	// CONSTRAINT c_fk_mae_mao_id FOREIGN KEY (mae_mao_id) REFERENCES
	// iav.ts_masqueope_mao(mao_mas_id)
	// );

	private MasqueOpe masqueope;
	private StationMesure stationmesure;
	private Boolean affichage;
	private String valeurdefaut;

	/**
	 * Construit une CondEnvOpe avec les deux �l�ments formant la cl� unique :
	 * le masqueOpe et la stationmesure
	 * 
	 * @param _masqueope
	 * @param _stationmesure
	 */
	public CondEnvOpe(MasqueOpe _masqueope, StationMesure _stationmesure) {
		this.masqueope = _masqueope;
		this.stationmesure = _stationmesure;
	}

	/**
	 * Construit une CondEnvOpe avec tous les d�tails
	 * 
	 * @param _masqueope
	 * @param _stationmesure
	 */
	public CondEnvOpe(MasqueOpe _masqueope, StationMesure _stationmesure, Boolean _affichage, String _valeurdefaut) {
		this.masqueope = _masqueope;
		this.stationmesure = _stationmesure;
		this.affichage = _affichage;
		this.valeurdefaut = _valeurdefaut;
	}

	/**
	 * @return le masque d'Op�ration
	 */
	public MasqueOpe getMasqueope() {
		return masqueope;
	}

	/**
	 * @param masqueope
	 *            le masque de saisie des op�rations
	 */
	public void setMasqueope(MasqueOpe masqueope) {
		this.masqueope = masqueope;
	}

	/**
	 * @return la stationmesure
	 */
	public StationMesure getStationmesure() {
		return stationmesure;
	}

	/**
	 * @param stationmesure
	 *            la station de mesure
	 */
	public void setStationmesure(StationMesure stationmesure) {
		this.stationmesure = stationmesure;
	}

	/**
	 * @return affichage
	 */
	public Boolean getAffichage() {
		return affichage;
	}

	/**
	 * @param affichage
	 *            the affichage to set
	 */
	public void setAffichage(Boolean affichage) {
		this.affichage = affichage;
	}

	/**
	 * @return the valeurdefaut
	 */
	public String getValeurdefaut() {
		return valeurdefaut;
	}

	/**
	 * @param valeurdefaut
	 *            the valeurdefaut to set
	 */
	public void setValeurdefaut(String valeurdefaut) {
		this.valeurdefaut = valeurdefaut;
	}

	/**
	 * Suppression
	 */
	@Override
	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = CondEnvOpe.getNomTable();
		String[] nomID = CondEnvOpe.getNomID();
		Object[] valeurID = this.getValeurID();

		ConnexionBD.getInstance().executeDelete(table, nomID, valeurID);
	}

	/**
	 * Insertion
	 */
	@Override
	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = CondEnvOpe.getNomTable();
		ArrayList<String> noms = CondEnvOpe.getNomAttributs();
		ArrayList<Object> valeurs = this.getValeurAttributs();

		ConnexionBD.getInstance().executeInsert(table, noms, valeurs);
	}

	/**
	 * Mise a jour
	 */
	@Override
	public void majObjet() throws SQLException, ClassNotFoundException {
		String table = StationMesure.getNomTable();

		String[] nomID = CondEnvOpe.getNomID();
		Object[] valeurID = this.getValeurID();

		ArrayList<String> noms = CondEnvOpe.getNomAttributs();
		ArrayList<Object> valeurs = this.getValeurAttributs();

		ConnexionBD.getInstance().executeUpdate(table, nomID, valeurID, noms, valeurs);
	}

	/**
	 * V�rification d'une StationMesure V�rifie le format de chacuns des
	 * attributs de l'objet courant
	 */
	@Override
	public boolean verifAttributs() throws DataFormatException {
		// la station de mesure doit �tre d�finie, c'est � dire
		// qu'elle ne peut �tre nulle et son identifiant ne peut �tre null
		if (!Verification.isStationMesure(stationmesure, true))
			throw new DataFormatException(Erreur.I);

		// de m�me le masqueOperation doit exister pour cr�er la station de
		// mesure
		if (!Verification.isMasqueOpe(this.masqueope, true))
			throw new DataFormatException(Erreur.I);

		// pas besoin de v�rifier que l'objet est de type bool�en, il y aura des
		// exception avant
		// de m�me le masqueOperation doit exister pour cr�er la station de
		// mesure

		// en pratique j'autorise les valeurs nulles ici, je pourrais me passer
		// d'�crire cette m�thode
		if (!Verification.isText(valeurdefaut, false))
			throw new DataFormatException(Erreur.I);

		return true;
	}

	/**
	 * Acc�s au nom de la table
	 * 
	 * @return le nom de la table associ�e
	 */
	public static String getNomTable() {
		return "ts_masqueconditionsenvironnementales_mae";
	}

	/**
	 * Retourne le nom des attributs de la table correspondant a la classe
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		// impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
		// est inconnu
		ArrayList<String> ret = new ArrayList<String>();
		ret.add("mae_mao_id");
		ret.add("mae_stm_identifiant");
		ret.add("mae_affichage");
		ret.add("mae_valeurdefaut");
		return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de la classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret = new ArrayList<Object>();
		ret.add(this.getMasqueope().getMasque().getIdentifiant());
		ret.add(this.getStationmesure().getIdentifiant().intValue());
		ret.add(this.getAffichage().booleanValue());
		ret.add(this.getValeurdefaut());
		return ret;
	} // end getValeurAttributs

	/**
	 * Retourne le nom de l'identifiant caract�re de la table
	 * 
	 * @return un vecteur de string de longueur 2 contenant le tuple formant
	 *         l'identifiant de la table
	 */
	public static String[] getNomID() {
		String[] ret = new String[2];
		ret[0] = "mae_mao_id";
		ret[1] = "mae_stm_identifiant";
		return ret;
	} // end getNomID

	/**
	 * Retourne la valeur de l'indenfiant string de la classe
	 * 
	 * @return un vecteur d'objet de longueur 1 contenant le code
	 */
	public Object[] getValeurID() {
		Object[] ret = new Object[2];
		ret[0] = this.getMasqueope().getMasque().getIdentifiant();
		ret[1] = this.getStationmesure().getIdentifiant();
		return ret;
	} // end getValeurID

}
