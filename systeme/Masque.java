/**
 * 
 */
package systeme;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;

/**
 * Classe masque Les masques contiennent les information g�n�riques pour tous
 * les masques, op�ration et lot Les m�thodes de classes sont utilis�es dans les
 * classes MasqueLot et MasqueOpe
 * 
 * @author C�dric Briand
 */

public class Masque implements IBaseDonnees {

	// /////////////////////////////////////
	// attributes
	// /////////////////////////////////////
	// mad_id identifiant integer
	// mas_code character varying(15) NOT NULL,
	// mas_description text,
	// mas_raccourci text,
	// mas_type character varying(10),

	private String code;
	private Integer identifiant;
	private String description;
	private String raccouci;
	private String type;

	// /////////////////////////////////////
	// constructors
	// /////////////////////////////////////
	/**
	 * Construit une marque avec uniquement une reference
	 * 
	 * @param _code
	 *            le code du ma
	 */
	public Masque(String _code) {
		this(_code, null, null, null, null);
	}

	/**
	 * Construit une classe tous les attributs,
	 * 
	 * @param _id
	 *            indentifiant integer
	 * @param _code
	 *            identifiant (10 caract�res du masque)
	 * @param _description
	 *            texte de description
	 * @param _raccourci
	 *            touches de raccourci
	 * @param _type
	 *            ope ou lot
	 */
	public Masque(String _code, Integer _id, String _description, String _raccourci, String _type) {

		this.setCode(_code);
		this.setIdentifiant(_id);
		this.setDescription(_description);
		this.setRaccouci(_raccourci);
		this.setType(_type);
	}

	/**
	 * Construit une classe tous les attributs sans l'identifiant (serial)
	 * 
	 * @param _code
	 *            identifiant (10 caract�res du masque)
	 * @param _description
	 *            texte de description
	 * @param _raccourci
	 *            touches de raccourci
	 * @param _type
	 *            ope ou lot
	 */
	public Masque(String _code, String _description, String _raccourci, String _type) {
		this.setCode(_code);
		this.setDescription(_description);
		this.setRaccouci(_raccourci);
		this.setType(_type);
	}

	// /////////////////////////////////////
	// interfaces
	// /////////////////////////////////////

	/**
	 * insertion des objets de la table
	 * 
	 * @see commun.IBaseDonnees.insertObjet()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		ArrayList nomAttributs = this.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		System.out.println("Masque : insertObjet()");
		// la table ne comporte pas d'organisme, utilisation de la m�thode
		// executeinsertssorg
		ConnexionBD.getInstance().executeInsertssorg(table, nomAttributs, valAttributs);
	}

	/**
	 * Mise � jour des objets de la table commun.IBaseDonnees.majObjet()
	 */
	@Override
	public void majObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		String[] nomAttributsSelection = this.getNomID();
		Object[] valAttributsSelection = this.getValeurID();
		ArrayList nomAttributs = this.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		System.out.println("Masque : majObjet()");

		ConnexionBD.getInstance().executeUpdatessorg(table, nomAttributsSelection, valAttributsSelection, nomAttributs,
				valAttributs);

	}

	// /**
	// * Mise � jour d'un masque
	// * @param old : l'ancien masque
	// * @throws SQLException
	// * @throws ClassNotFoundException
	// */
	//
	// public void majObjet(Masque old) throws SQLException,
	// ClassNotFoundException {
	// String table = this.getNomTable();
	// String[] nomID = Masque.getNomID();
	// Object[] valueID = this.getValeurID();
	// ArrayList<String> nomsAttributs = this.getNomAttributs();
	// ArrayList<Object> valeursAttributs =this.getValeurAttributs();
	//
	// ConnexionBD.getInstance().executeUpdate(table, nomID, valueID,
	// nomsAttributs, valeursAttributs);
	// }

	/**
	 * Suppression d'un objet de la table
	 */
	@Override
	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		String[] nomID = Masque.getNomID();
		Object[] valueID = this.getValeurID();
		ConnexionBD.getInstance().executeDelete(table, nomID, valueID);
	}

	/**
	 * Acc�s aux valeurs des attributs ici il ne s'agit pas de l'id (cl�
	 * primaire) mais du code, qui forme l'identifiant de l'objet
	 * 
	 * @return un tableau avec les valeurs des attributs qui forment la cl�
	 *         primaire (ici le code)
	 */
	private Object[] getValeurID() {
		Object[] res = new Object[1];
		res[0] = this.code;
		return res;
	}

	/**
	 * Acc�s aux noms des attributs qui forment la cl� primaire
	 * 
	 * @return un tableau avec les noms des attributs qui forment la cl�
	 *         primaire
	 */
	private static String[] getNomID() {
		String[] res = new String[1];
		res[0] = "mas_code";
		return res;
	}

	/**
	 * verifie les attributs des champs
	 * 
	 * @see commun.IBaseDonnees#verifAttributs()
	 */
	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;
		// if (!Verification.isInteger(this.id, true)&&!this.id.) {
		// throw new DataFormatException (Erreur.S27000) ;
		// }
		if (!Verification.isText(this.code, 1, 10, true)) {
			throw new DataFormatException(Erreur.S27001);
		}
		Vector possible = new Vector();
		possible.add("lot");
		possible.add("ope");
		if (!Verification.isPossibleText(type.toString(), possible, true)) {
			throw new DataFormatException(Erreur.S27002);
		}

		return (ret);
	}

	// /////////////////////////////////////
	// operations
	// /////////////////////////////////////
	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return le nom
	 */
	private String getNomTable() {
		return "ts_masque_mas";
	}

	/**
	 * Retourne le nom des champs de la table
	 * 
	 * @return le noms des champs de la table dans l'ordre d'insertion dans la
	 *         base de donnee
	 */
	private ArrayList<String> getNomAttributs() {
		ArrayList<String> ret = new ArrayList<String>();

		// ret.add( "mas_id"); # l'identifiant est auto incr�ment�
		ret.add("mas_code");
		ret.add("mas_description");
		ret.add("mas_raccourci");
		ret.add("mas_type");
		return ret;
	}

	/**
	 * Retourne la valeur des attributs de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	private ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret = new ArrayList<Object>();
		// ret.add(this.id);
		ret.add(this.code);
		ret.add(this.description);
		ret.add(this.raccouci);
		ret.add(this.type);
		return ret;
	}

	/**
	 * Affichage d'une Liste masque = son code Attention si cette m�thode n'est
	 * pas pr�sente, le code correct du masque ne s'affiche pas et dans le combo
	 * on a pas les bonnes r�f�rences
	 */
	public String toString() {
		return this.code;
	}

	/**
	 * @return l'identifiant du masque
	 */
	public Integer getIdentifiant() {
		return identifiant;
	}

	/**
	 * Initialise l'identifiant du masque dans la classe
	 * 
	 * @param id
	 */
	public void setIdentifiant(Integer id) {
		this.identifiant = id;
	}

	/**
	 * @return le code du masque
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Initialise le code du masque dans la classe
	 * 
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Retourne la description du masque
	 * 
	 * @return description du masque
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Initialise la description du masque
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return String = les touches de raccourci
	 */
	public String getRaccouci() {
		return raccouci;
	}

	/**
	 * @param String
	 *            les touches de raccourci
	 */
	public void setRaccouci(String raccouci) {
		this.raccouci = raccouci;
	}

	/**
	 * @return un objet de la classe RefTypeMasque : type de masque lot ou ope
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            de masque lot ou ope (RefTypeMasque
	 */
	public void setType(String type) {
		this.type = type;
	}

}
