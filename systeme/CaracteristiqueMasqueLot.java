package systeme;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.IBaseDonnees;
import migration.Caracteristique;

/**
 * Affichage des caract�ristiques dans le masque lot Subtilit� des �l�ments
 * comme la pr�cision, la m�thode d'obtention et le commentaire sont stock�s
 * dans la classe caract�ristique. Mais en base chaque �l�ment correspond � une
 * ligne.
 * 
 * @author cedric.briand
 *
 */
public class CaracteristiqueMasqueLot implements IBaseDonnees {
	private Integer identifiant;
	private MasqueLot masquelot;
	private Caracteristique caracteristiquedefaut;
	// les valeurs par d�faut sont stock�es dans la caracteristique
	// la pr�cision et la m�thode d'obtention aussi
	private boolean affichage_valeur;
	private boolean affichage_commentaire;
	private boolean affichage_precision;
	private boolean affichage_methodeobtention;
	// private String valeurdefaut;

	/**
	 * Construit une CaracteristiqueMasqueLot avec les deux �l�ments formant la
	 * cl� unique : le masquelot et la caract�ristique
	 * 
	 * @param _masquelot
	 * @param _caracteristique
	 */
	public CaracteristiqueMasqueLot(MasqueLot _masquelot, Caracteristique _caracteristique) {
		this.masquelot = _masquelot;
		this.caracteristiquedefaut = _caracteristique;
	}

	/**
	 * Construit une CaracteristiqueMasqueLot avec tous les d�tails
	 * 
	 * @param _masqueope
	 * @param _stationmesure
	 */

	/**
	 * Construit une CaracteristiqueMasqueLot avec tous les d�tails sauf
	 * l'identifiant
	 * 
	 * @param _masqueope
	 * @param _stationmesure
	 */
	public CaracteristiqueMasqueLot(MasqueLot _masquelot, Caracteristique _caracteristique, boolean _affichage_valeur,
			boolean _affichage_commentaire, boolean _affichage_precision, boolean _affichage_methodeobtention) {
		this.masquelot = _masquelot;
		this.caracteristiquedefaut = _caracteristique;
		this.affichage_valeur = _affichage_valeur;
		this.affichage_commentaire = _affichage_commentaire;
		this.affichage_precision = _affichage_precision;
		this.affichage_methodeobtention = _affichage_methodeobtention;
	}

	/**
	 * Suppression
	 */
	@Override
	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = CaracteristiqueMasqueLot.getNomTable();
		String[] nomID = CaracteristiqueMasqueLot.getNomID();
		Object[] valeurID = this.getValeurID();

		ConnexionBD.getInstance().executeDelete(table, nomID, valeurID);
	}

	/**
	 * Insertion
	 */
	@Override
	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = CaracteristiqueMasqueLot.getNomTable();
		ArrayList<String> noms = CaracteristiqueMasqueLot.getNomAttributs();
		ArrayList<Object> valeurs = this.getValeurAttributs();

		ConnexionBD.getInstance().executeInsert(table, noms, valeurs);
	}

	/**
	 * Mise a jour
	 */
	@Override
	public void majObjet() throws SQLException, ClassNotFoundException {
		String table = CaracteristiqueMasqueLot.getNomTable();

		String[] nomID = CaracteristiqueMasqueLot.getNomID();
		Object[] valeurID = this.getValeurID();

		ArrayList<String> noms = CaracteristiqueMasqueLot.getNomAttributs();
		ArrayList<Object> valeurs = this.getValeurAttributs();

		ConnexionBD.getInstance().executeUpdate(table, nomID, valeurID, noms, valeurs);
	}

	/**
	 * V�rification d'une StationMesure V�rifie le format de chacuns des
	 * attributs de l'objet courant
	 */
	@Override
	public boolean verifAttributs() throws DataFormatException {
		// TODO
		return (true);
	}

	/**
	 * Acc�s au nom de la table
	 * 
	 * @return le nom de la table associ�e
	 */
	public static String getNomTable() {
		return "ts_masquecaracteristiquelot_mac";
	}

	/**
	 * Retourne le nom des attributs de la table correspondant a la classe
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		// impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
		// est inconnu
		ArrayList<String> ret = new ArrayList<String>();
		ret.add("mac_id");
		ret.add("mac_mal_id");
		ret.add("mac_par_code");
		ret.add("mac_affichagevaleur");
		ret.add("mac_affichageprecision");
		ret.add("mac_affichagecommentaire");
		ret.add("mac_affichagemethodeobtention");
		ret.add("mac_valeurquantitatifdefaut");
		ret.add("mac_valeurqualitatifdefaut");
		ret.add("mac_precisiondefaut");
		ret.add("mac_methodeobtentiondefaut");
		ret.add("mac_commentairedefaut");
		return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de la classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret = new ArrayList<Object>();
		ret.add(this.identifiant);
		ret.add(this.getMasquelot().getMasque().getIdentifiant());
		ret.add(this.getCaracteristiquedefaut().getParametre().getCode());
		ret.add(this.affichage_valeur);
		ret.add(this.affichage_precision);
		ret.add(this.affichage_commentaire);
		ret.add(this.affichage_methodeobtention);
		ret.add(this.getCaracteristiquedefaut().getValeurParametreQuantitatif());
		if (this.getCaracteristiquedefaut().getValeurParametreQualitatif() != null) {
			ret.add(this.getCaracteristiquedefaut().getValeurParametreQualitatif().getCode());
		} else {
			ret.add(null);
		}
		ret.add(this.getCaracteristiquedefaut().getPrecision());
		ret.add(this.getCaracteristiquedefaut().getMethodeObtention());
		ret.add(this.getCaracteristiquedefaut().getCommentaires());

		return ret;
	} // end getValeurAttributs

	/**
	 * Retourne le nom de l'identifiant caract�re de la table
	 * 
	 * @return un vecteur de string de longueur 2 contenant le tuple formant
	 *         l'identifiant de la table
	 */
	public static String[] getNomID() {
		String[] ret = new String[2];
		ret[0] = "mac_mal_id";
		ret[1] = "mac_par_code";
		return ret;
	} // end getNomID

	/**
	 * Retourne la valeur de l'indenfiant string de la classe *
	 * 
	 * @return un vecteur d'objet de longueur 2 contenant le code
	 */
	public Object[] getValeurID() {
		Object[] ret = new Object[2];
		ret[0] = this.getMasquelot().getMasque().getIdentifiant();
		ret[1] = this.getCaracteristiquedefaut().getParametre().getCode();
		return ret;
	} // end getValeurID

	/**
	 * @return the masquelot
	 */
	public MasqueLot getMasquelot() {
		return masquelot;
	}

	/**
	 * @param masquelot
	 *            the masquelot to set
	 */
	public void setMasquelot(MasqueLot masquelot) {
		this.masquelot = masquelot;
	}

	/**
	 * @return the caracteristique
	 */
	public Caracteristique getCaracteristiquedefaut() {
		return caracteristiquedefaut;
	}

	/**
	 * @param caracteristique
	 *            the caracteristique to set
	 */
	public void setCaracteristiquedefaut(Caracteristique caracteristiquedefaut) {
		this.caracteristiquedefaut = caracteristiquedefaut;
	}

	/**
	 * @return the affichage_valeur
	 */
	public boolean isAffichage_valeur() {
		return affichage_valeur;
	}

	/**
	 * @param affichage_valeur
	 *            the affichage_valeur to set
	 */
	public void setAffichage_valeur(boolean affichage_valeur) {
		this.affichage_valeur = affichage_valeur;
	}

	/**
	 * @return the affichage_commentaire
	 */
	public boolean isAffichage_commentaire() {
		return affichage_commentaire;
	}

	/**
	 * @param affichage_commentaire
	 *            the affichage_commentaire to set
	 */
	public void setAffichage_commentaire(boolean affichage_commentaire) {
		this.affichage_commentaire = affichage_commentaire;
	}

	/**
	 * @return the affichage_precision
	 */
	public boolean isAffichage_precision() {
		return affichage_precision;
	}

	/**
	 * @param affichage_precision
	 *            the affichage_precision to set
	 */
	public void setAffichage_precision(boolean affichage_precision) {
		this.affichage_precision = affichage_precision;
	}

	/**
	 * @return the affichage_methodeobtention
	 */
	public boolean isAffichage_methodeobtention() {
		return affichage_methodeobtention;
	}

	/**
	 * @param affichage_methodeobtention
	 *            the affichage_methodeobtention to set
	 */
	public void setAffichage_methodeobtention(boolean affichage_methodeobtention) {
		this.affichage_methodeobtention = affichage_methodeobtention;
	}

}
