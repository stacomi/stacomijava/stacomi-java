package systeme;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.IBaseDonnees;

/**
 * Affichage des caract�ristiques dans le masque lot
 * 
 * 
 * @author cedric.briand
 *
 */
public class AffichageMasqueLot implements IBaseDonnees {
	private Integer identifiant;
	private MasqueLot masquelot;
	private String table;
	private String valeur;
	private String champdumasque; // dans le cas ou la m�me table de ref est
									// utilis�e plusieurs fois, ex localisation
									// pr�l�vement, marque...
	private int rang;

	/**
	 * @param masquelot
	 * @param table
	 * @param nom_colonne_code
	 * @param valeur
	 * @param champdumasque
	 * @param rang
	 */
	public AffichageMasqueLot(MasqueLot masquelot, String table, String valeur, String champdumasque, int rang) {
		this.masquelot = masquelot;
		this.table = table;
		this.valeur = valeur;
		this.champdumasque = champdumasque;
		this.rang = rang;
	}

	/**
	 * @param masquelot
	 * @param table
	 * @param nom_colonne_code
	 * @param champdumasque
	 */
	public AffichageMasqueLot(MasqueLot masquelot, String table, String valeur, String champdumasque) {
		this.masquelot = masquelot;
		this.table = table;
		this.valeur = valeur;
		this.champdumasque = champdumasque;
	}

	/**
	 * @param identifiant
	 * @param masquelot
	 * @param table
	 * @param nom_colonne_code
	 * @param valeur
	 * @param champdumasque
	 * @param rang
	 */
	public AffichageMasqueLot(Integer identifiant, MasqueLot masquelot, String table, String valeur,
			String champdumasque, int rang) {
		this.identifiant = identifiant;
		this.masquelot = masquelot;
		this.table = table;
		this.valeur = valeur;
		this.champdumasque = champdumasque;
		this.rang = rang;
	}

	/**
	 * Suppression
	 */
	@Override
	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = AffichageMasqueLot.getNomTable();
		String[] nomID = AffichageMasqueLot.getNomID();
		Object[] valeurID = this.getValeurID();

		ConnexionBD.getInstance().executeDelete(table, nomID, valeurID);
	}

	/**
	 * Insertion
	 */
	@Override
	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = AffichageMasqueLot.getNomTable();
		ArrayList<String> noms = AffichageMasqueLot.getNomAttributs();
		ArrayList<Object> valeurs = this.getValeurAttributs();

		ConnexionBD.getInstance().executeInsert(table, noms, valeurs);
	}

	/**
	 * Mise a jour
	 */
	@Override
	public void majObjet() throws SQLException, ClassNotFoundException {
		String table = AffichageMasqueLot.getNomTable();

		String[] nomID = AffichageMasqueLot.getNomID();
		Object[] valeurID = this.getValeurID();

		ArrayList<String> noms = AffichageMasqueLot.getNomAttributs();
		ArrayList<Object> valeurs = this.getValeurAttributs();

		ConnexionBD.getInstance().executeUpdate(table, nomID, valeurID, noms, valeurs);
	}

	/**
	 * V�rification d'une StationMesure V�rifie le format de chacuns des
	 * attributs de l'objet courant
	 */
	@Override
	public boolean verifAttributs() throws DataFormatException {
		// TODO
		return (true);
	}

	/**
	 * Acc�s au nom de la table
	 * 
	 * @return le nom de la table associ�e
	 */
	public static String getNomTable() {
		return "ts_masqueordreaffichage_maa";
	}

	/**
	 * Retourne le nom des attributs de la table correspondant a la classe
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		// impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
		// est inconnu
		ArrayList<String> ret = new ArrayList<String>();
		// ret.add("maa_id"); Pas d'identifiant
		ret.add("maa_mal_id");
		ret.add("maa_table");
		ret.add("maa_valeur");
		ret.add("maa_champdumasque");
		ret.add("maa_rang");
		return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de la classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret = new ArrayList<Object>();
		// je ne met pas l'identifiant
		ret.add(this.getMasquelot().getMasque().getIdentifiant());
		ret.add(this.table);
		ret.add(this.valeur);
		ret.add(this.champdumasque);
		ret.add(this.rang);
		return ret;
	} // end getValeurAttributs

	/**
	 * Retourne le nom de l'identifiant caract�re de la table
	 * 
	 * @return un vecteur de string de longueur 2 contenant le tuple formant
	 *         l'identifiant de la table
	 */
	public static String[] getNomID() {
		String[] ret = new String[4];
		ret[0] = "maa_mal_id";
		ret[1] = "maa_table";
		ret[2] = "maa_valeur";
		ret[3] = "maa_champdumasque";

		return ret;
	} // end getNomID

	/**
	 * Retourne la valeur de l'indenfiant string de la classe *
	 * 
	 * @return un vecteur d'objet de longueur 2 contenant le code
	 */
	public Object[] getValeurID() {
		Object[] ret = new Object[4];
		ret[0] = this.getMasquelot().getMasque().getIdentifiant();
		ret[1] = this.table;
		ret[2] = this.valeur;
		ret[3] = this.champdumasque;
		return ret;
	} // end getValeurID

	/**
	 * @return the identifiant
	 */
	private Integer getIdentifiant() {
		return identifiant;
	}

	/**
	 * @param identifiant
	 *            the identifiant to set
	 */
	private void setIdentifiant(Integer identifiant) {
		this.identifiant = identifiant;
	}

	/**
	 * @return the masquelot
	 */
	public MasqueLot getMasquelot() {
		return masquelot;
	}

	/**
	 * @param masquelot
	 *            the masquelot to set
	 */
	public void setMasquelot(MasqueLot masquelot) {
		this.masquelot = masquelot;
	}

	/**
	 * @return the table
	 */
	public String getTable() {
		return table;
	}

	/**
	 * @param table
	 *            the table to set
	 */
	public void setTable(String table) {
		this.table = table;
	}

	/**
	 * @return the valeur
	 */
	public String getValeur() {
		return valeur;
	}

	/**
	 * @param valeur
	 *            the valeur to set
	 */
	public void setValeur(String valeur) {
		this.valeur = valeur;
	}

	/**
	 * @return the champdumasque
	 */
	public String getChampdumasque() {
		return champdumasque;
	}

	/**
	 * @param champdumasque
	 *            the champdumasque to set
	 */
	public void setChampdumasque(String champdumasque) {
		this.champdumasque = champdumasque;
	}

	/**
	 * @return the rang
	 */
	public int getRang() {
		return rang;
	}

	/**
	 * @param rang
	 *            the rang to set
	 */
	public void setRang(int rang) {
		this.rang = rang;
	}

}
