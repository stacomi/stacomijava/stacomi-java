/*
 **********************************************************************
 * 
 * Nom fichier :        Sequence.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * @author             	S�bastien Laigre
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   12 juin 2009
 * Compatibilite :      Java 6/Windows XP/PostgreSQL 8.3
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * @version
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */
package systeme;

/**
 * S�quence
 * @author S�bastien Laigre
 */
public class Sequence 
{
	/** Le nom de la sequence */
	private String sequence;
	
	/** Le nom de la table */
	private String table;
	
	/** Le nom de la colonne */
	private String colonne;

	/**
	 * Initialise une s�quence avec tous ses attributs
	 * @param sequence : le nom de la s�quence
	 * @param table : le nom de la table
	 * @param colonne : le nom de la colonne
	 */
	public Sequence(String sequence, String table, String colonne)
	{
		this.setSequence(sequence);
		this.setTable(table);
		this.setColonne(colonne);
	}

	/**
	 * Initialise une s�quence avec son nom seulement
	 * @param sequence : le nom de la s�quence 
	 */
	public Sequence(String sequence)
	{
		this.sequence = sequence;
	}

	/**
	 * Retourne le nom de la s�quence
	 * @return le nom de la s�quence 
	 */
	public String getSequence()
	{
		return this.sequence;
	}

	/**
	 * Retourne le nom de la table
	 * @return le nom de la table 
	 */
	public String getTable()
	{
		return this.table;
	}

	/**
	 * Retourne le nom de la colonne
	 * @return le nom de la colonne 
	 */
	public String getColonne()
	{
		return this.colonne;
	}
	
	/**
	 * Initialise le nom de la s�quence
	 * @param sequence : le nouveau nom de la s�quence 
	 */
	public void setSequence(String sequence)
	{
		this.sequence = sequence;
	}
	
	/**
	 * Initialise le nom de la table
	 * @param table : le nouveau nom de la table 
	 */
	public void setTable(String table)
	{
		this.table = table;
	}
	
	/**
	 * Initialise le nom de la colonne
	 * @param colonne : le nouveau nom de la colonne 
	 */
	public void setColonne(String colonne)
	{
		this.colonne = colonne;
	}
}
