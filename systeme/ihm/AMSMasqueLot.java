/**
 * 
 */
package systeme.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import commun.Accueil;
import commun.DualListBox;
import commun.IhmAppli;
import commun.Lanceur;
import commun.Message;
import migration.Caracteristique;
import migration.OperationMarquage;
import migration.referenciel.ListeOperationMarquage;
import migration.referenciel.ListeRefDevenir;
import migration.referenciel.ListeRefImportancePathologie;
import migration.referenciel.ListeRefLocalisation;
import migration.referenciel.ListeRefNatureMarque;
import migration.referenciel.ListeRefParamQualBio;
import migration.referenciel.ListeRefParamQuantBio;
import migration.referenciel.ListeRefPathologie;
import migration.referenciel.ListeRefPrelevement;
import migration.referenciel.ListeRefStade;
import migration.referenciel.ListeRefTaxon;
import migration.referenciel.ListeRefTypeQuantite;
import migration.referenciel.RefDevenir;
import migration.referenciel.RefImportancePathologie;
import migration.referenciel.RefLocalisation;
import migration.referenciel.RefNatureMarque;
import migration.referenciel.RefParametre;
import migration.referenciel.RefParametreQualitatif;
import migration.referenciel.RefParametreQuantitatif;
import migration.referenciel.RefPathologie;
import migration.referenciel.RefPrelevement;
import migration.referenciel.RefStade;
import migration.referenciel.RefTaxon;
import migration.referenciel.RefTypeQuantite;
import systeme.AffichageMasqueLot;
import systeme.CaracteristiqueMasqueLot;
import systeme.Masque;
import systeme.MasqueLot;
import systeme.referenciel.ListeMasqueLot;
import systeme.referenciel.SousListeAffichageMasqueLot;
import systeme.referenciel.SousListeCaracteristiqueMasqueLot;

/**
 * @author cedric.briand
 *
 */
public class AMSMasqueLot extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -206621306113587373L;

	private final static Logger logger = Logger.getLogger(AMSMasqueLot.class.getName());

	private Masque masque;
	private MasqueLot masquelot;
	private ListeMasqueLot listemasquelot;
	private SousListeCaracteristiqueMasqueLot caracteristiquemasquelot;
	private SousListeCaracteristiqueMasqueLot caracteristiquemasquelotenbase;
	private SousListeAffichageMasqueLot affichagemasquelot; // liste des
	// elements
	// selectionn�es
	// dans les dualist
	private SousListeAffichageMasqueLot affichagemasquelotenbase;// liste des
	// elements
	// selectionn�es
	// dans les
	// dualist
	// en base

	// param�tres pour l'affichage
	private Boolean[] affichage = new Boolean[21];
	private String[] valeurdefaut = new String[14];

	// Listes de r�f�rence pour les listes et combos
	// Listes pour les combos onglet lots
	private ListeRefTaxon lesTaxons;
	private ListeRefStade lesStades;
	private ListeRefDevenir lesDevenirs;
	private ListeRefTypeQuantite lesTypeQuantite;
	private String[] methodesObtentionLot;

	// Listes pour les combos onglet caracteristiques
	private ListeRefParamQualBio lesParametreQualitatifs;
	private ListeRefParamQuantBio lesParametreQuantitatifs;
	private DefaultListModel<RefParametre> listModelparametres;// un listmodel
	// qui rassemble
	// les
	// param�tres
	// quant et qual
	// pour
	// l'affichage
	// private SousListeRefValeurParametre lesValeursParametre;
	// private String[] methodesObtentionCaracteristique;

	// Listes pour les combos onglet taille poids stade
	// private SousListeRefValeurParametre lesValeursParametreStade;
	private ListeRefLocalisation lesLocalisations;
	private ListeRefPathologie lesPathologies;
	private ListeRefImportancePathologie lesImportancesPatho;
	// private SousListeMarquages lesMarquages;
	private ListeOperationMarquage lesOperationsMarquages;
	private ListeRefNatureMarque lesNaturesMarques;
	// private ListeOperationMarquage listeDesOperationsMarquages;
	private ListeRefPrelevement lesPrelevements;
	// private SousListePrelevement lesPrelevementsDuLot;

	// Interface graphique
	private JPanel top;
	private JTextArea resultat;
	private JPanel center;
	private JPanel bottom;
	private BoxLayout centerlayout;
	private JScrollPane scroll_center;

	private JPanel panel_masque;
	private FlowLayout flowlayout_masque;
	private JLabel label_codemasque;
	private JTextField textfield_libellemasque;

	private JPanel panel_taxon;
	private FlowLayout flowlayout_taxon;
	private JLabel label_taxon;
	private JCheckBox checkbox_taxon;
	private DualListBox duallist_taxon;
	private JPanel panel_stade;
	private FlowLayout flowlayout_stade;
	private JLabel label_stade;
	private JCheckBox checkbox_stade;
	private DualListBox duallist_stade;

	private JPanel panel_quantite;
	private FlowLayout flowlayout_quantite;
	// private JLabel label_effectif;
	// private JCheckBox checkbox_effectif;
	// private JFormattedTextField textfield_effectif;
	private JLabel label_quantite;

	private JCheckBox checkbox_quantite;
	private JTextField textfield_quantite;
	private JCheckBox checkbox_typequantite;
	private JComboBox<RefTypeQuantite> combo_typequantite;

	private JPanel panel_methodeobtention;
	private FlowLayout flowlayout_methodeobtention;
	private JLabel label_methodeobtention;
	private JCheckBox checkbox_methodeobtention;
	private JComboBox<String> combo_methodeobtention;

	private JPanel panel_devenir;
	private FlowLayout flowlayout_devenir;
	private JLabel label_devenir;
	private JCheckBox checkbox_devenir;
	private JComboBox<RefDevenir> combo_devenir;

	private JPanel panel_commentaire;
	private FlowLayout flowlayout_commentaire;
	private JLabel label_commentaire;
	private JCheckBox checkbox_commentaire;

	private JPanel panel_carlot;
	private FlowLayout flowlayout_carlot;
	private JLabel label_carlot;
	private DualListBox duallist_carlot;

	private JPanel panel_pathologie;
	private GridBagLayout gbl_pathologie;
	private JLabel label_pathologie;
	private JCheckBox checkbox_pathologie;
	private DualListBox duallist_pathologie;
	private JLabel label_localisationpatho;
	private JCheckBox checkbox_localisationpatho;
	private DualListBox duallist_localisationpatho;
	private JLabel label_importancepatho;
	private JCheckBox checkbox_importancepatho;
	private JComboBox<RefImportancePathologie> combo_importancepathologie;

	private JPanel panel_prelevement;
	private GridBagLayout gbl_prelevement;
	private JLabel label_prelevement;
	private JLabel label_codeprel;
	private JLabel label_operateurprel;
	private JLabel label_typeprel;
	private JLabel label_localisationprel;
	private JLabel label_commentaireprel;
	private JCheckBox checkbox_codeprel;
	private JCheckBox checkbox_operateurprel;
	private JCheckBox checkbox_typeprel;
	private JCheckBox checkbox_localisationprel;
	private JCheckBox checkbox_commentaireprel;
	private JTextField textfield_codeprel;
	private JTextField textfield_operateurprel;
	private JComboBox<RefPrelevement> combo_typeprel;
	private JComboBox<RefLocalisation> combo_localisationprel;

	private JPanel panel_marquage;
	private GridBagLayout gbl_marquage;
	private JLabel label_marquage;
	private JLabel label_operationmarquage;
	private JLabel label_localisationmarque;
	private JLabel label_naturemarque;
	private JLabel label_actionmarquage;
	private JLabel label_commentairemarquage;
	private JCheckBox checkbox_operationmarquage;
	private JCheckBox checkbox_localisationmarquage;
	private JCheckBox checkbox_naturemarque;
	private JCheckBox checkbox_actionmarquage;
	private JCheckBox checkbox_commentairemarquage;
	private JComboBox<OperationMarquage> combo_operationmarquage;
	private JComboBox<RefLocalisation> combo_localisationmarquage;
	private JComboBox<RefNatureMarque> combo_naturemarque;
	private ButtonGroup button_actionmarquage;
	private JRadioButton radio_pose;
	private JRadioButton radio_lecture;
	private JRadioButton radio_retrait;
	private JRadioButton radio_posemult;
	private boolean is_masquelot_existing;

	/**
	 * Constructeur du masque lot, � partir d'un Masque comme argument (on passe
	 * forcement par AMS Masque pour y acc�der)
	 * 
	 * @return
	 */
	public AMSMasqueLot(Masque _masque) {
		this.masque = _masque;
		this.masquelot = new MasqueLot(masque);
		// je repasse tous les d�tails � masque �galement
		this.masque = masquelot.getMasque();
		this.initComponents();
		this.initCombos();
		is_masquelot_existing = this.chargemasque();
		// charge les valeurs du masque correspondant en base et remplit la
		// valeur test
	}

	private void initComponents() {
		setPreferredSize(new Dimension(800, 600));
		setBackground(new Color(240, 240, 240));
		setLayout(new BorderLayout(0, 0));
		// top
		top = new JPanel();
		add(top, BorderLayout.NORTH);
		top.setLayout(new BorderLayout(0, 0));
		JLabel titre = new JLabel();
		titre.setText("Edition d'un masque Lot");
		titre.setOpaque(true);
		titre.setHorizontalAlignment(SwingConstants.CENTER);
		titre.setForeground(Color.WHITE);
		titre.setFont(new Font("Dialog", Font.BOLD, 14));
		titre.setBackground(new Color(0, 51, 153));
		top.add(titre, BorderLayout.NORTH);

		this.resultat = new JTextArea();
		resultat.setOpaque(true);
		resultat.setForeground(Color.RED);
		resultat.setBackground(Color.WHITE);
		top.add(resultat, BorderLayout.SOUTH);

		// center

		center = new JPanel();
		scroll_center = new JScrollPane(center);
		scroll_center.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroll_center.setMinimumSize(new Dimension(600, 300));
		scroll_center.setPreferredSize(new Dimension(800, 600));
		add(scroll_center, BorderLayout.WEST);
		centerlayout = new BoxLayout(center, BoxLayout.Y_AXIS);
		center.setLayout(centerlayout);

		// masque
		panel_masque = new JPanel();
		center.add(panel_masque);
		panel_masque.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		panel_masque.setPreferredSize(new Dimension(800, 50));
		flowlayout_masque = new FlowLayout(FlowLayout.CENTER, 5, 5);
		panel_masque.setLayout(flowlayout_masque);
		label_codemasque = new JLabel("Masque=" + masque.getCode());
		panel_masque.add(label_codemasque);
		textfield_libellemasque = new JTextField(masque.getDescription());
		textfield_libellemasque.setEnabled(false);
		textfield_libellemasque.setForeground(Color.blue);

		panel_masque.add(textfield_libellemasque);

		// taxon
		panel_taxon = new JPanel();
		center.add(panel_taxon);
		panel_taxon.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		flowlayout_taxon = new FlowLayout(FlowLayout.LEFT, 5, 5);
		panel_taxon.setLayout(flowlayout_taxon);
		label_taxon = new JLabel("Taxon");
		panel_taxon.add(label_taxon);
		checkbox_taxon = new JCheckBox();
		panel_taxon.add(checkbox_taxon);
		duallist_taxon = new DualListBox();
		duallist_taxon.setSourceChoicesTitle("Taxon");
		panel_taxon.add(duallist_taxon);

		// stade
		panel_stade = new JPanel();
		center.add(panel_stade);
		panel_stade.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		flowlayout_stade = new FlowLayout(FlowLayout.LEFT, 5, 5);
		panel_stade.setLayout(flowlayout_stade);
		label_stade = new JLabel("Stade");
		panel_stade.add(label_stade);
		checkbox_stade = new JCheckBox();
		panel_stade.add(checkbox_stade);
		duallist_stade = new DualListBox();
		duallist_stade.setSourceChoicesTitle("Stade");
		panel_stade.add(duallist_stade);

		// quantite
		panel_quantite = new JPanel();
		panel_quantite.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		center.add(panel_quantite);
		flowlayout_quantite = new FlowLayout(FlowLayout.LEFT, 5, 5);
		panel_quantite.setLayout(flowlayout_quantite);
		// Note l'effectif n'est pas utilis� dans le masque lot, c'est un combo
		// commun aux effectifs et aux
		// quantit�s de lots qui est utilis�.
		// label_effectif = new JLabel("Effectif");
		// checkbox_effectif = new JCheckBox();
		// NumberFormat integerFieldFormatter =
		// NumberFormat.getIntegerInstance();
		// integerFieldFormatter.setGroupingUsed(false);
		// textfield_effectif = new JFormattedTextField(integerFieldFormatter);
		// textfield_effectif.setPreferredSize(new Dimension(50, 20));
		// textfield_effectif.setMinimumSize(new Dimension(40, 20));
		// panel_quantite.add(label_effectif);
		// panel_quantite.add(checkbox_effectif);
		// panel_quantite.add(textfield_effectif);
		label_quantite = new JLabel("Quantit�");
		panel_quantite.add(label_quantite);
		checkbox_quantite = new JCheckBox();
		panel_quantite.add(checkbox_quantite);
		textfield_quantite = new JTextField();
		textfield_quantite.setPreferredSize(new Dimension(50, 20));
		textfield_quantite.setMinimumSize(new Dimension(40, 20));

		panel_quantite.add(textfield_quantite);
		checkbox_typequantite = new JCheckBox();
		panel_quantite.add(checkbox_typequantite);
		combo_typequantite = new JComboBox<RefTypeQuantite>();
		combo_typequantite.setMinimumSize(new Dimension(50, 20));
		combo_typequantite.setPreferredSize(new Dimension(150, 20));
		panel_quantite.add(combo_typequantite);
		// methodeobtention
		panel_methodeobtention = new JPanel();
		center.add(panel_methodeobtention);
		panel_methodeobtention
				.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		flowlayout_methodeobtention = new FlowLayout(FlowLayout.LEFT, 5, 5);
		panel_methodeobtention.setLayout(flowlayout_methodeobtention);
		label_methodeobtention = new JLabel("M�th. Obt.");
		panel_methodeobtention.add(label_methodeobtention);
		checkbox_methodeobtention = new JCheckBox();
		panel_methodeobtention.add(checkbox_methodeobtention);
		combo_methodeobtention = new JComboBox<String>();
		panel_methodeobtention.add(combo_methodeobtention);

		// devenir
		panel_devenir = new JPanel();
		center.add(panel_devenir);
		panel_devenir.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		flowlayout_devenir = new FlowLayout(FlowLayout.LEFT, 5, 5);
		panel_devenir.setLayout(flowlayout_devenir);
		label_devenir = new JLabel("Devenir");
		panel_devenir.add(label_devenir);
		checkbox_devenir = new JCheckBox();
		panel_devenir.add(checkbox_devenir);

		combo_devenir = new JComboBox<RefDevenir>();
		panel_devenir.add(combo_devenir);

		// commentaire
		panel_commentaire = new JPanel();
		center.add(panel_commentaire);
		panel_commentaire.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		flowlayout_commentaire = new FlowLayout(FlowLayout.LEFT, 5, 5);
		panel_commentaire.setLayout(flowlayout_commentaire);
		label_commentaire = new JLabel("Commentaires");
		panel_commentaire.add(label_commentaire);
		checkbox_commentaire = new JCheckBox();
		panel_commentaire.add(checkbox_commentaire);

		// carlot
		panel_carlot = new JPanel();
		center.add(panel_carlot);
		panel_carlot.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		flowlayout_carlot = new FlowLayout(FlowLayout.LEFT, 5, 5);
		panel_carlot.setLayout(flowlayout_carlot);
		label_carlot = new JLabel("Caract. Lot");
		panel_carlot.add(label_carlot);
		duallist_carlot = new DualListBox();
		duallist_carlot.setSourceChoicesTitle("Caract�ristiques � �diter");
		panel_carlot.add(duallist_carlot);

		// pathologie
		panel_pathologie = new JPanel();
		center.add(panel_pathologie);

		panel_pathologie.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		gbl_pathologie = new GridBagLayout();
		panel_pathologie.setLayout(gbl_pathologie);

		label_pathologie = new JLabel("Pathologies");
		GridBagConstraints gbc_pathologie0 = new GridBagConstraints();
		gbc_pathologie0.insets = new Insets(0, 5, 0, 0);
		gbc_pathologie0.gridx = 0;
		gbc_pathologie0.gridy = 0;
		panel_pathologie.add(label_pathologie, gbc_pathologie0);

		checkbox_pathologie = new JCheckBox();
		GridBagConstraints gbc_pathologie10 = new GridBagConstraints();
		gbc_pathologie10.insets = new Insets(2, 2, 2, 2);
		gbc_pathologie10.gridx = 1;
		gbc_pathologie10.gridy = 0;
		panel_pathologie.add(checkbox_pathologie, gbc_pathologie10);

		duallist_pathologie = new DualListBox();
		duallist_pathologie.setSourceChoicesTitle("Selection/ordre des pathologies");
		GridBagConstraints gbc_pathologie20 = new GridBagConstraints();
		gbc_pathologie20.insets = new Insets(2, 2, 2, 2);
		gbc_pathologie20.gridx = 2;
		gbc_pathologie20.gridy = 0;
		panel_pathologie.add(duallist_pathologie, gbc_pathologie20);

		label_localisationpatho = new JLabel("Localisation");
		GridBagConstraints gbc_pathologie01 = new GridBagConstraints();
		gbc_pathologie01.insets = new Insets(0, 5, 0, 0);
		gbc_pathologie01.gridx = 0;
		gbc_pathologie01.gridy = 1;
		panel_pathologie.add(label_localisationpatho, gbc_pathologie01);

		checkbox_localisationpatho = new JCheckBox();
		GridBagConstraints gbc_pathologie11 = new GridBagConstraints();
		gbc_pathologie11.insets = new Insets(2, 2, 2, 2);
		gbc_pathologie11.gridx = 1;
		gbc_pathologie11.gridy = 1;
		panel_pathologie.add(checkbox_localisationpatho, gbc_pathologie11);

		duallist_localisationpatho = new DualListBox();
		duallist_localisationpatho.setSourceChoicesTitle("Selection/ordre des localisations");
		GridBagConstraints gbc_pathologie21 = new GridBagConstraints();
		gbc_pathologie21.insets = new Insets(2, 2, 2, 2);
		gbc_pathologie21.gridx = 2;
		gbc_pathologie21.gridy = 1;
		panel_pathologie.add(duallist_localisationpatho, gbc_pathologie21);

		label_importancepatho = new JLabel("Importance");
		GridBagConstraints gbc_pathologie02 = new GridBagConstraints();
		gbc_pathologie02.insets = new Insets(0, 5, 0, 0);
		gbc_pathologie02.gridx = 0;
		gbc_pathologie02.gridy = 2;
		panel_pathologie.add(label_importancepatho, gbc_pathologie02);

		checkbox_importancepatho = new JCheckBox();
		GridBagConstraints gbc_pathologie12 = new GridBagConstraints();
		gbc_pathologie12.insets = new Insets(2, 2, 2, 2);
		gbc_pathologie12.gridx = 1;
		gbc_pathologie12.gridy = 2;
		panel_pathologie.add(checkbox_importancepatho, gbc_pathologie12);

		combo_importancepathologie = new JComboBox<RefImportancePathologie>();
		GridBagConstraints gbc_pathologie22 = new GridBagConstraints();
		gbc_pathologie22.insets = new Insets(2, 2, 2, 2);
		gbc_pathologie22.gridx = 2;
		gbc_pathologie22.gridy = 2;
		panel_pathologie.add(combo_importancepathologie, gbc_pathologie22);

		// prelevement
		panel_prelevement = new JPanel();
		center.add(panel_prelevement);
		gbl_prelevement = new GridBagLayout();
		panel_prelevement.setLayout(gbl_prelevement);
		GridBagConstraints gbc_prel0 = new GridBagConstraints();
		gbc_prel0.gridx = 0;
		gbc_prel0.gridy = 0;
		gbc_prel0.gridwidth = 3;
		gbc_prel0.insets = new Insets(2, 0, 5, 0);
		label_prelevement = new JLabel("Pr�l�vements");
		panel_prelevement.add(label_prelevement, gbc_prel0);

		GridBagConstraints gbc_prel1 = new GridBagConstraints();
		gbc_prel1.gridx = 0;
		gbc_prel1.gridy = 1;
		gbc_prel1.insets = new Insets(2, 5, 2, 5);
		label_codeprel = new JLabel("Code");
		panel_prelevement.add(label_codeprel, gbc_prel1);

		GridBagConstraints gbc_prel2 = new GridBagConstraints();
		gbc_prel2.gridx = 0;
		gbc_prel2.gridy = 2;
		gbc_prel2.insets = new Insets(2, 5, 2, 5);
		label_operateurprel = new JLabel("Op�rateur");
		panel_prelevement.add(label_operateurprel, gbc_prel2);

		GridBagConstraints gbc_prel3 = new GridBagConstraints();
		gbc_prel3.gridx = 0;
		gbc_prel3.gridy = 3;
		gbc_prel3.insets = new Insets(2, 5, 2, 5);
		label_typeprel = new JLabel("Type pr�l�vement");
		panel_prelevement.add(label_typeprel, gbc_prel3);

		GridBagConstraints gbc_prel4 = new GridBagConstraints();
		gbc_prel4.gridx = 0;
		gbc_prel4.gridy = 4;
		gbc_prel4.insets = new Insets(2, 5, 2, 5);
		label_localisationprel = new JLabel("Localisation pr�l�vement");
		panel_prelevement.add(label_localisationprel, gbc_prel4);

		GridBagConstraints gbc_prel5 = new GridBagConstraints();
		gbc_prel5.gridx = 0;
		gbc_prel5.gridy = 5;
		gbc_prel5.insets = new Insets(2, 5, 2, 5);
		label_commentaireprel = new JLabel("Commentaire pr�l�vement");
		panel_prelevement.add(label_commentaireprel, gbc_prel5);

		GridBagConstraints gbc_prel11 = new GridBagConstraints();
		gbc_prel11.gridx = 1;
		gbc_prel11.gridy = 1;
		gbc_prel11.insets = new Insets(2, 5, 2, 5);
		checkbox_codeprel = new JCheckBox();
		panel_prelevement.add(checkbox_codeprel, gbc_prel11);

		GridBagConstraints gbc_prel12 = new GridBagConstraints();
		gbc_prel12.gridx = 1;
		gbc_prel12.gridy = 2;
		gbc_prel12.insets = new Insets(2, 5, 2, 5);
		checkbox_operateurprel = new JCheckBox();
		panel_prelevement.add(checkbox_operateurprel, gbc_prel12);

		GridBagConstraints gbc_prel13 = new GridBagConstraints();
		gbc_prel13.gridx = 1;
		gbc_prel13.gridy = 3;
		gbc_prel13.insets = new Insets(2, 5, 2, 5);
		checkbox_typeprel = new JCheckBox();
		panel_prelevement.add(checkbox_typeprel, gbc_prel13);

		GridBagConstraints gbc_prel14 = new GridBagConstraints();
		gbc_prel14.gridx = 1;
		gbc_prel14.gridy = 4;
		gbc_prel14.insets = new Insets(2, 5, 2, 5);
		checkbox_localisationprel = new JCheckBox();
		panel_prelevement.add(checkbox_localisationprel, gbc_prel14);

		GridBagConstraints gbc_prel15 = new GridBagConstraints();
		gbc_prel15.gridx = 1;
		gbc_prel15.gridy = 5;
		gbc_prel15.insets = new Insets(2, 5, 2, 5);
		checkbox_commentaireprel = new JCheckBox();
		panel_prelevement.add(checkbox_commentaireprel, gbc_prel15);

		GridBagConstraints gbc_prel21 = new GridBagConstraints();
		gbc_prel21.fill = GridBagConstraints.HORIZONTAL;
		gbc_prel21.gridx = 2;
		gbc_prel21.gridy = 1;
		gbc_prel21.insets = new Insets(2, 5, 2, 5);
		textfield_codeprel = new JTextField();
		textfield_codeprel.setPreferredSize(new Dimension(200, 20));
		textfield_codeprel.setMinimumSize(new Dimension(40, 20));
		panel_prelevement.add(textfield_codeprel, gbc_prel21);

		GridBagConstraints gbc_prel22 = new GridBagConstraints();
		gbc_prel22.fill = GridBagConstraints.HORIZONTAL;
		gbc_prel22.gridx = 2;
		gbc_prel22.gridy = 2;
		gbc_prel22.insets = new Insets(2, 5, 2, 5);
		textfield_operateurprel = new JTextField();
		textfield_operateurprel.setPreferredSize(new Dimension(200, 20));
		panel_prelevement.add(textfield_operateurprel, gbc_prel22);

		GridBagConstraints gbc_prel23 = new GridBagConstraints();
		gbc_prel23.fill = GridBagConstraints.HORIZONTAL;
		gbc_prel23.gridx = 2;
		gbc_prel23.gridy = 3;
		gbc_prel23.insets = new Insets(2, 5, 2, 5);
		combo_typeprel = new JComboBox<RefPrelevement>();
		combo_typeprel.setPreferredSize(new Dimension(200, 20));
		panel_prelevement.add(combo_typeprel, gbc_prel23);

		GridBagConstraints gbc_prel24 = new GridBagConstraints();
		gbc_prel24.fill = GridBagConstraints.HORIZONTAL;
		gbc_prel24.gridx = 2;
		gbc_prel24.gridy = 4;
		gbc_prel24.insets = new Insets(2, 5, 2, 5);
		combo_localisationprel = new JComboBox<RefLocalisation>();
		combo_localisationprel.setPreferredSize(new Dimension(200, 20));
		panel_prelevement.add(combo_localisationprel, gbc_prel24);

		// marquage
		panel_marquage = new JPanel();
		gbl_marquage = new GridBagLayout();
		panel_marquage.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_marquage.setLayout(gbl_marquage);
		center.add(panel_marquage);

		GridBagConstraints gbc_marquage00 = new GridBagConstraints();
		gbc_marquage00.gridx = 0;
		gbc_marquage00.gridy = 0;
		gbc_marquage00.gridwidth = 6;
		gbc_marquage00.insets = new Insets(5, 5, 5, 5);
		label_marquage = new JLabel("Marquages");
		panel_marquage.add(label_marquage, gbc_marquage00);

		GridBagConstraints gbc_marquage01 = new GridBagConstraints();
		gbc_marquage01.gridx = 0;
		gbc_marquage01.gridy = 1;
		gbc_marquage01.insets = new Insets(2, 5, 2, 5);
		label_operationmarquage = new JLabel("Op�ration de marquage");
		panel_marquage.add(label_operationmarquage, gbc_marquage01);

		GridBagConstraints gbc_marquage02 = new GridBagConstraints();
		gbc_marquage02.gridx = 0;
		gbc_marquage02.gridy = 2;
		gbc_marquage02.insets = new Insets(2, 5, 2, 5);
		label_localisationmarque = new JLabel("Localisation");
		panel_marquage.add(label_localisationmarque, gbc_marquage02);

		GridBagConstraints gbc_marquage03 = new GridBagConstraints();
		gbc_marquage03.gridx = 0;
		gbc_marquage03.gridy = 3;
		gbc_marquage03.insets = new Insets(2, 5, 2, 5);
		label_naturemarque = new JLabel("Nature de marque");
		panel_marquage.add(label_naturemarque, gbc_marquage03);

		GridBagConstraints gbc_marquage04 = new GridBagConstraints();
		gbc_marquage04.gridx = 0;
		gbc_marquage04.gridy = 4;
		gbc_marquage04.insets = new Insets(2, 5, 2, 5);
		label_actionmarquage = new JLabel("Action marquage");
		panel_marquage.add(label_actionmarquage, gbc_marquage04);

		GridBagConstraints gbc_marquage05 = new GridBagConstraints();
		gbc_marquage05.gridx = 0;
		gbc_marquage05.gridy = 5;
		gbc_marquage05.insets = new Insets(2, 5, 2, 5);
		label_commentairemarquage = new JLabel("Commentaires");
		panel_marquage.add(label_commentairemarquage, gbc_marquage05);

		GridBagConstraints gbc_marquage11 = new GridBagConstraints();
		gbc_marquage11.gridx = 1;
		gbc_marquage11.gridy = 1;
		gbc_marquage11.insets = new Insets(2, 5, 2, 5);
		checkbox_operationmarquage = new JCheckBox();
		panel_marquage.add(checkbox_operationmarquage, gbc_marquage11);

		GridBagConstraints gbc_marquage12 = new GridBagConstraints();
		gbc_marquage12.gridx = 1;
		gbc_marquage12.gridy = 2;
		gbc_marquage12.insets = new Insets(2, 5, 2, 5);
		checkbox_localisationmarquage = new JCheckBox();
		panel_marquage.add(checkbox_localisationmarquage, gbc_marquage12);

		GridBagConstraints gbc_marquage13 = new GridBagConstraints();
		gbc_marquage13.gridx = 1;
		gbc_marquage13.gridy = 3;
		gbc_marquage13.insets = new Insets(2, 5, 2, 5);
		checkbox_naturemarque = new JCheckBox();
		panel_marquage.add(checkbox_naturemarque, gbc_marquage13);

		GridBagConstraints gbc_marquage14 = new GridBagConstraints();
		gbc_marquage14.gridx = 1;
		gbc_marquage14.gridy = 4;
		gbc_marquage14.insets = new Insets(2, 5, 2, 5);
		checkbox_actionmarquage = new JCheckBox();
		panel_marquage.add(checkbox_actionmarquage, gbc_marquage14);

		GridBagConstraints gbc_marquage15 = new GridBagConstraints();
		gbc_marquage15.gridx = 1;
		gbc_marquage15.gridy = 5;
		gbc_marquage15.insets = new Insets(2, 5, 2, 5);
		checkbox_commentairemarquage = new JCheckBox();
		panel_marquage.add(checkbox_commentairemarquage, gbc_marquage15);

		GridBagConstraints gbc_marquage21 = new GridBagConstraints();
		gbc_marquage21.fill = GridBagConstraints.HORIZONTAL;
		gbc_marquage21.gridx = 2;
		gbc_marquage21.gridy = 1;
		gbc_marquage21.gridwidth = 3;
		gbc_marquage21.insets = new Insets(2, 5, 2, 5);
		combo_operationmarquage = new JComboBox<OperationMarquage>();
		panel_marquage.add(combo_operationmarquage, gbc_marquage21);

		GridBagConstraints gbc_marquage22 = new GridBagConstraints();
		gbc_marquage22.fill = GridBagConstraints.HORIZONTAL;
		gbc_marquage22.gridx = 2;
		gbc_marquage22.gridy = 2;
		gbc_marquage22.insets = new Insets(2, 5, 2, 5);
		gbc_marquage22.gridwidth = 3;
		combo_localisationmarquage = new JComboBox<RefLocalisation>();
		panel_marquage.add(combo_localisationmarquage, gbc_marquage22);

		GridBagConstraints gbc_marquage23 = new GridBagConstraints();
		gbc_marquage23.fill = GridBagConstraints.HORIZONTAL;
		gbc_marquage23.gridx = 2;
		gbc_marquage23.gridy = 3;
		gbc_marquage23.gridwidth = 3;
		gbc_marquage23.insets = new Insets(2, 5, 2, 5);
		combo_naturemarque = new JComboBox<RefNatureMarque>();
		panel_marquage.add(combo_naturemarque, gbc_marquage23);

		GridBagConstraints gbc_marquage24 = new GridBagConstraints();
		gbc_marquage24.fill = GridBagConstraints.HORIZONTAL;
		gbc_marquage24.gridx = 2;
		gbc_marquage24.gridy = 4;
		gbc_marquage24.insets = new Insets(2, 5, 2, 5);
		GridBagConstraints gbc_marquage34 = new GridBagConstraints();
		gbc_marquage34.gridx = 3;
		gbc_marquage34.gridy = 4;
		gbc_marquage34.insets = new Insets(2, 5, 2, 5);
		GridBagConstraints gbc_marquage44 = new GridBagConstraints();
		gbc_marquage44.gridx = 4;
		gbc_marquage44.gridy = 4;
		gbc_marquage44.insets = new Insets(2, 5, 2, 5);
		GridBagConstraints gbc_marquage54 = new GridBagConstraints();
		gbc_marquage54.gridx = 5;
		gbc_marquage54.gridy = 4;
		gbc_marquage54.insets = new Insets(2, 5, 2, 5);
		GridBagConstraints gbc_marquage64 = new GridBagConstraints();
		gbc_marquage64.gridx = 6;
		gbc_marquage64.gridy = 4;
		gbc_marquage64.insets = new Insets(2, 5, 2, 5);
		button_actionmarquage = new ButtonGroup();

		radio_pose = new JRadioButton("Pose");
		radio_pose.setActionCommand("Pose");
		radio_lecture = new JRadioButton("Lecture");
		radio_lecture.setActionCommand("Lecture");
		radio_retrait = new JRadioButton("Retrait");
		radio_retrait.setActionCommand("Retrait");
		radio_posemult = new JRadioButton("Pose Mult.");
		radio_posemult.setActionCommand("Pose Mult.");
		JRadioButton radio_vide = new JRadioButton("");
		radio_vide.setToolTipText("cliquez pour d�selectionner");
		button_actionmarquage.add(radio_pose);
		button_actionmarquage.add(radio_lecture);
		button_actionmarquage.add(radio_retrait);
		button_actionmarquage.add(radio_posemult);
		button_actionmarquage.add(radio_vide);
		radio_vide.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				button_actionmarquage.clearSelection();

			}
		});
		panel_marquage.add(radio_pose, gbc_marquage24);
		panel_marquage.add(radio_lecture, gbc_marquage34);
		panel_marquage.add(radio_retrait, gbc_marquage44);
		panel_marquage.add(radio_posemult, gbc_marquage54);
		panel_marquage.add(radio_vide, gbc_marquage64);
		// bottom
		bottom = new JPanel();
		bottom.setBackground(UIManager.getColor("ToolBar.background"));
		add(bottom, BorderLayout.SOUTH);
		// si l'op�ration existe elle est renseign�e et charg�e

		JButton button_V = new JButton();
		button_V.setText("Valider");
		bottom.add(button_V);

		JButton button_annuler = new JButton();
		button_annuler.setText("Annuler");
		bottom.add(button_annuler);

		button_V.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				V_MasqueLot();
			}
		});

		button_annuler.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				quitte();
			}
		});

	}

	private void initCombos() {

		this.lesTaxons = Lanceur.getListeRefTaxon();
		DefaultListModel<RefTaxon> listModeltaxon = new DefaultListModel<RefTaxon>();
		for (int i = 0; i < lesTaxons.size(); i++) {
			RefTaxon taxon = (RefTaxon) lesTaxons.get(i);
			listModeltaxon.addElement(taxon);
		}
		try {

			this.duallist_taxon.addSourceElements(listModeltaxon);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement de la liste taxons" + e.toString());
		}
		// ===========================================
		this.lesStades = Lanceur.getListeRefStade();
		DefaultListModel<RefStade> listModelstade = new DefaultListModel<RefStade>();
		for (int i = 0; i < lesStades.size(); i++) {
			RefStade stade = (RefStade) lesStades.get(i);
			listModelstade.addElement(stade);
		}
		try {

			this.duallist_stade.addSourceElements(listModelstade);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement de la liste stade" + e.toString());
		}
		// ===========================================
		this.lesTypeQuantite = Lanceur.getListeRefTypeQuantite();
		try {

			DefaultComboBoxModel<RefTypeQuantite> combomodeltypequantite = new DefaultComboBoxModel<RefTypeQuantite>();
			RefTypeQuantite refquantnull = new RefTypeQuantite("aucun", "");
			combomodeltypequantite.addElement(refquantnull);
			for (int i = 0; i < lesTypeQuantite.size(); i++) {
				RefTypeQuantite reftypequantite = (RefTypeQuantite) lesTypeQuantite.get(i);
				combomodeltypequantite.addElement(reftypequantite);
			}
			this.combo_typequantite.setModel(combomodeltypequantite);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement du combo" + e.toString());
		}
		// ===========================================
		this.methodesObtentionLot = Lanceur.getMethodesObtentionLot();

		try {
			DefaultComboBoxModel<String> combomodelmethodeobtention = new DefaultComboBoxModel<String>();
			combomodelmethodeobtention.addElement("");
			for (int i = 0; i < methodesObtentionLot.length; i++) {
				String refmethodeobtention = (String) methodesObtentionLot[i];
				combomodelmethodeobtention.addElement(refmethodeobtention);
			}
			this.combo_methodeobtention.setModel(combomodelmethodeobtention);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement du combo" + e.toString());
		}

		// ===========================================
		this.lesDevenirs = Lanceur.getListeRefDevenir();
		try {
			DefaultComboBoxModel<RefDevenir> combomodeldevenir = new DefaultComboBoxModel<RefDevenir>();
			RefDevenir refdevenir0 = new RefDevenir("Inconnu", "");// il faut
			// construire
			// avec deux
			// sinon le
			// libelle
			// qui est
			// r�cup�r�
			// avec
			// toString
			// d�clenche un nullpointerexception
			combomodeldevenir.addElement(refdevenir0);
			for (int i = 0; i < lesDevenirs.size(); i++) {
				RefDevenir refdevenir = (RefDevenir) lesDevenirs.get(i);
				combomodeldevenir.addElement(refdevenir);
			}
			this.combo_devenir.setModel(combomodeldevenir);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement du combo" + e.toString());
		}

		this.lesParametreQualitatifs = Lanceur.getListeRefParamQualBio();
		this.lesParametreQuantitatifs = Lanceur.getListeRefParamQuantBio();
		listModelparametres = new DefaultListModel<RefParametre>();
		for (int i = 0; i < lesParametreQualitatifs.size(); i++) {
			RefParametreQualitatif paramqual = (RefParametreQualitatif) lesParametreQualitatifs.get(i);
			listModelparametres.addElement(paramqual);
		}
		for (int i = 0; i < lesParametreQuantitatifs.size(); i++) {
			RefParametreQuantitatif paramquant = (RefParametreQuantitatif) lesParametreQuantitatifs.get(i);
			listModelparametres.addElement(paramquant);
		}
		try {
			this.duallist_carlot.addSourceElements(listModelparametres);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement de la liste dese param�tres" + e.toString());
		}

		// ===========================================
		this.lesLocalisations = Lanceur.getListeRefLocalisationPathologie();
		DefaultListModel<RefLocalisation> listModellocalisation = new DefaultListModel<RefLocalisation>();
		for (int i = 0; i < lesLocalisations.size(); i++) {
			RefLocalisation localisation = (RefLocalisation) lesLocalisations.get(i);
			listModellocalisation.addElement(localisation);
		}
		try {
			this.duallist_localisationpatho.addSourceElements(listModellocalisation);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement de la liste" + e.toString());
		}
		// ===========================================
		this.lesPathologies = Lanceur.getListeRefPathologie();
		DefaultListModel<RefPathologie> listModelpathologie = new DefaultListModel<RefPathologie>();
		for (int i = 0; i < lesPathologies.size(); i++) {
			RefPathologie pathologie = (RefPathologie) lesPathologies.get(i);
			listModelpathologie.addElement(pathologie);
		}
		try {

			this.duallist_pathologie.addSourceElements(listModelpathologie);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement de la liste" + e.toString());
		}
		// ===========================================
		this.lesImportancesPatho = Lanceur.getListeRefImportancePathologie();
		try {
			DefaultComboBoxModel<RefImportancePathologie> combomodelImportancePatho = new DefaultComboBoxModel<RefImportancePathologie>();
			RefImportancePathologie refimportancepatho0 = new RefImportancePathologie("", "");
			combomodelImportancePatho.addElement(refimportancepatho0);
			for (int i = 0; i < lesImportancesPatho.size(); i++) {
				RefImportancePathologie refimportancepatho = (RefImportancePathologie) lesImportancesPatho.get(i);
				combomodelImportancePatho.addElement(refimportancepatho);
			}
			this.combo_importancepathologie.setModel(combomodelImportancePatho);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement du combo" + e.toString());
		}
		// Pr�l�vements
		// ===========================================
		this.lesPrelevements = Lanceur.getListeRefPrelevement();
		try {
			DefaultComboBoxModel<RefPrelevement> combomodelPrelevement = new DefaultComboBoxModel<RefPrelevement>();
			RefPrelevement refprelevement0 = new RefPrelevement("", "");
			combomodelPrelevement.addElement(refprelevement0);
			for (int i = 0; i < lesPrelevements.size(); i++) {
				RefPrelevement refprelevement = (RefPrelevement) lesPrelevements.get(i);
				combomodelPrelevement.addElement(refprelevement);
			}
			this.combo_typeprel.setModel(combomodelPrelevement);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement du combo" + e.toString());
		}

		DefaultComboBoxModel<RefLocalisation> combomodelLocalisationprelevement = new DefaultComboBoxModel<RefLocalisation>();
		RefLocalisation localisation0 = new RefLocalisation("");
		combomodelLocalisationprelevement.addElement(localisation0);
		try {
			for (int i = 0; i < lesLocalisations.size(); i++) {
				RefLocalisation reflocalisation = (RefLocalisation) lesLocalisations.get(i);
				combomodelLocalisationprelevement.addElement(reflocalisation);
			}
			this.combo_localisationprel.setModel(combomodelLocalisationprelevement);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement du combo" + e.toString());
		}
		// Marquage
		// ===========================================
		this.lesOperationsMarquages = new ListeOperationMarquage();
		try {
			lesOperationsMarquages.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " chargelesoperationsMarquage  ", e);
		}
		try {
			DefaultComboBoxModel<OperationMarquage> comboModelOperationMarquage = new DefaultComboBoxModel<OperationMarquage>();
			// ajout d'un �l�ment vide en t�te de liste
			OperationMarquage opemarquage0 = new OperationMarquage(null);
			comboModelOperationMarquage.addElement(opemarquage0);
			for (int i = 0; i < lesOperationsMarquages.size(); i++) {
				OperationMarquage opemarquage = (OperationMarquage) lesOperationsMarquages.get(i);
				comboModelOperationMarquage.addElement(opemarquage);
			}
			this.combo_operationmarquage.setModel(comboModelOperationMarquage);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement du combo" + e.toString());
		}

		this.lesNaturesMarques = Lanceur.getListeRefNatureMarque();
		try {
			DefaultComboBoxModel<RefNatureMarque> combomodelnaturemarque = new DefaultComboBoxModel<RefNatureMarque>();
			RefNatureMarque refnaturemarque0 = new RefNatureMarque("", "");
			combomodelnaturemarque.addElement(refnaturemarque0);
			for (int i = 0; i < lesNaturesMarques.size(); i++) {
				RefNatureMarque refnaturemarque = (RefNatureMarque) lesNaturesMarques.get(i);
				combomodelnaturemarque.addElement(refnaturemarque);
			}
			this.combo_naturemarque.setModel(combomodelnaturemarque);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement du combo" + e.toString());
		}
		DefaultComboBoxModel<RefLocalisation> combomodelLocalisationmarquage = new DefaultComboBoxModel<RefLocalisation>();
		// localisation 0, valeur nulle d�finie dans les pr�l�vements
		combomodelLocalisationmarquage.addElement(localisation0);
		try {
			for (int i = 0; i < lesLocalisations.size(); i++) {
				RefLocalisation reflocalisation = (RefLocalisation) lesLocalisations.get(i);
				combomodelLocalisationmarquage.addElement(reflocalisation);
			}
			this.combo_localisationmarquage.setModel(combomodelLocalisationmarquage);
		} catch (Exception e) {
			logger.log(Level.SEVERE, " initCombos  ", e);
			this.resultat.setText("Echec du chargement du combo (localisation marquage)" + e.toString());
		}

	}

	/**
	 * charge le masque et met � jour les combos (pas les listes)
	 * 
	 * @return boolean le masque existe
	 */
	private boolean chargemasque() {
		this.listemasquelot = new ListeMasqueLot();
		try {
			listemasquelot.chargeFiltre(masque.getCode());
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " chargeMasque  ", e);
		}
		this.caracteristiquemasquelotenbase = new SousListeCaracteristiqueMasqueLot(masquelot);
		try {
			caracteristiquemasquelotenbase.chargeSansFiltre();
			// la m�thode chargeSansFiltre charge tous les objets de la sous
			// liste du masqueope
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " chargeMasque  ", e);
		}
		this.affichagemasquelotenbase = new SousListeAffichageMasqueLot(masquelot);
		try {
			affichagemasquelotenbase.chargeSansFiltre();

		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " chargeMasque  ", e);
		}
		// si il y aune valeur, le masque existe en base
		if (listemasquelot.size() == 1) {
			int index; // indice de la valeur s�lectionn�e dans le masque pour
			this.masquelot = (MasqueLot) this.listemasquelot.get(0);
			// je dois quand m�me aller r�cup�rer l'identifiant integer du
			// masque
			// sinon plante a l'update ou � l'insertion.
			// il a �t� pass� par masque...
			this.masquelot.setMasque(this.masque);
			this.affichage = masquelot.getAffichage();
			this.valeurdefaut = masquelot.getValeurDefaut();
			// ---------------------------------------------------------------------
			// mise � jour des combos et textfields � partir des valeurs par
			// d�faut
			// ---------------------------------------------------------------------
			// effectif
			// if (valeurdefaut[12] != null) {
			// if (!valeurdefaut[12].isEmpty()) {
			// this.textfield_effectif.setValue(new Integer(valeurdefaut[12]));
			// }
			// }
			// quantite
			if (valeurdefaut[0] != null) {
				this.textfield_quantite.setText(valeurdefaut[0]);
			}
			// typequantite
			if (valeurdefaut[1] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[1], this.combo_typequantite);
				this.combo_typequantite.setSelectedIndex(index);
			}
			// methodeobtention
			if (valeurdefaut[2] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[2], this.combo_methodeobtention);
				this.combo_methodeobtention.setSelectedIndex(index);
			}
			// devenir
			if (valeurdefaut[3] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[3], this.combo_devenir);
				this.combo_devenir.setSelectedIndex(index);
			}
			// code prelevement
			if (valeurdefaut[4] != null) {
				this.textfield_codeprel.setText(valeurdefaut[4]);
			}
			// operateur prelevement
			if (valeurdefaut[5] != null) {
				this.textfield_operateurprel.setText(valeurdefaut[5]);
			}
			// type pr�l�vemement
			if (valeurdefaut[6] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[6], this.combo_typeprel);
				this.combo_typeprel.setSelectedIndex(index);
			}
			// localisation pr�l�vemement
			if (valeurdefaut[7] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[7], this.combo_localisationprel);
				this.combo_localisationprel.setSelectedIndex(index);
			}
			// operation marquage
			if (valeurdefaut[8] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[8], this.combo_operationmarquage);
				this.combo_operationmarquage.setSelectedIndex(index);
			}
			// localisation marquage
			if (valeurdefaut[9] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[9], this.combo_localisationmarquage);
				this.combo_localisationmarquage.setSelectedIndex(index);
			}
			// naturemarque
			if (valeurdefaut[10] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[10], this.combo_naturemarque);
				this.combo_naturemarque.setSelectedIndex(index);
			}
			// action marquage
			if (valeurdefaut[11] != null) {
				switch (valeurdefaut[11]) {
				case "Pose":
					radio_pose.setSelected(true);
					;
					break;
				case "Lecture":
					radio_lecture.setSelected(true);
					break;
				case "Retrait":
					radio_retrait.setSelected(true);
					break;
				case "PoseMultiple":
					radio_posemult.setSelected(true);
					break;
				}
				// importance patho
				if (valeurdefaut[13] != null) {
					index = IhmAppli.getIndexforString(valeurdefaut[13], this.combo_importancepathologie);
					this.combo_importancepathologie.setSelectedIndex(index);
				}
			}

			// ---------------------------------------------------------------------
			// charge les valeus des checkboxes
			// ---------------------------------------------------------------------
			this.checkbox_taxon.setSelected(affichage[0]);
			this.checkbox_stade.setSelected(affichage[1]);
			// this.checkbox_effectif.setSelected(affichage[19]);
			this.checkbox_quantite.setSelected(affichage[2]);
			this.checkbox_typequantite.setSelected(affichage[3]);
			this.checkbox_methodeobtention.setSelected(affichage[4]);
			this.checkbox_devenir.setSelected(affichage[5]);
			this.checkbox_commentaire.setSelected(affichage[6]);
			this.checkbox_pathologie.setSelected(affichage[7]);
			this.checkbox_localisationpatho.setSelected(affichage[8]);
			this.checkbox_codeprel.setSelected(affichage[9]);
			this.checkbox_operateurprel.setSelected(affichage[10]);
			this.checkbox_typeprel.setSelected(affichage[11]);
			this.checkbox_localisationprel.setSelected(affichage[12]);
			this.checkbox_commentaireprel.setSelected(affichage[13]);
			this.checkbox_operationmarquage.setSelected(affichage[14]);
			this.checkbox_localisationmarquage.setSelected(affichage[15]);
			this.checkbox_naturemarque.setSelected(affichage[16]);
			this.checkbox_actionmarquage.setSelected(affichage[17]);
			this.checkbox_commentairemarquage.setSelected(affichage[18]);
			this.checkbox_importancepatho.setSelected(affichage[20]);

			// ---------------------------------------------------------------------
			// Chargement des affichages dans les diff�rents combos
			// ---------------------------------------------------------------------
			this.setlesaffichages();

			// ---------------------------------------------------------------------
			// Chargement de la dualliste des caract�ristiques
			// ---------------------------------------------------------------------
			this.setlescaracteristiques();
			// ---------------------------------------------------------------------
			// retourne la valeur indiquant que le masque �tait effectivement en
			// base
			// ---------------------------------------------------------------------
			return (true);
		} else {
			return (false); // le masque n'est pas en base
		}
	}

	/**
	 * affiche les �l�ments des dualistbox
	 */
	private void setlesaffichages() {
		if (affichagemasquelotenbase.size() >= 1) {
			// extrait les �l�ments de affichagemasquelotenbase seulement pour
			// le combo_taxon, normalement
			// ils sont ordonn�s par rang asc dans la requ�te de la sous liste.
			SousListeAffichageMasqueLot saml = SousListeAffichageMasqueLot.get_elements_list("duallist_taxon",
					affichagemasquelotenbase);
			// dans le combo on a remplit un listModeltaxon a partir de
			// listReftaxon qui contient tous les details
			// les objets RefTaxons qui remplissent le combo sont donc complets,
			// et il faut aller les chercher a partir
			// de leur code qui est stock� dans maa_valeur
			DefaultListModel<RefTaxon> listModeltaxon = new DefaultListModel<RefTaxon>();
			for (int i = 0; i < saml.size(); i++) {
				String code = ((AffichageMasqueLot) saml.get(i)).getValeur();
				boolean match = false;
				int j = 0;
				// on va chercher les �l�ments de lesTaxons qui sont les objets
				// ayant servi � remplir le combo
				while (j < this.lesTaxons.size()) {

					match = ((RefTaxon) this.lesTaxons.get(j)).getCode().equals(code);
					if (match)
						listModeltaxon.addElement((RefTaxon) this.lesTaxons.get(j));
					j++;

				}

			}
			try {
				this.duallist_taxon.setDestinationElements(listModeltaxon);
			} catch (Exception e) {
				System.out.println(e);
				logger.log(Level.SEVERE, " setlesAffichage : taxon  ", e);
			}
			// STADES
			saml = SousListeAffichageMasqueLot.get_elements_list("duallist_stade", affichagemasquelotenbase);
			DefaultListModel<RefStade> listModelstade = new DefaultListModel<RefStade>();
			for (int i = 0; i < saml.size(); i++) {
				String code = ((AffichageMasqueLot) saml.get(i)).getValeur();
				boolean match = false;
				int j = 0;
				while (j < this.lesStades.size()) {

					match = ((RefStade) this.lesStades.get(j)).getCode().equals(code);
					if (match)
						listModelstade.addElement((RefStade) this.lesStades.get(j));
					j++;

				}

			}
			try {
				this.duallist_stade.setDestinationElements(listModelstade);
			} catch (Exception e) {
				System.out.println(e);
				logger.log(Level.SEVERE, " setlesAffichage : stade  ", e);
			}
			// PATHOLOGIES
			saml = SousListeAffichageMasqueLot.get_elements_list("duallist_pathologie", affichagemasquelotenbase);
			DefaultListModel<RefPathologie> listModelpathologie = new DefaultListModel<RefPathologie>();
			for (int i = 0; i < saml.size(); i++) {
				String code = ((AffichageMasqueLot) saml.get(i)).getValeur();
				boolean match = false;
				int j = 0;
				while (j < this.lesPathologies.size()) {
					match = ((RefPathologie) this.lesPathologies.get(j)).getCode().equals(code);
					if (match)
						listModelpathologie.addElement((RefPathologie) this.lesPathologies.get(j));
					j++;

				}

			}
			try {
				this.duallist_pathologie.setDestinationElements(listModelpathologie);
			} catch (Exception e) {
				System.out.println(e);
				logger.log(Level.SEVERE, " setlesAffichage : pathologie  ", e);
			}

			// LOCALISATION PATHOLOGIES
			saml = SousListeAffichageMasqueLot.get_elements_list("duallist_localisationpatho",
					affichagemasquelotenbase);
			DefaultListModel<RefLocalisation> listModellocalisationpatho = new DefaultListModel<RefLocalisation>();
			for (int i = 0; i < saml.size(); i++) {
				String code = ((AffichageMasqueLot) saml.get(i)).getValeur();
				boolean match = false;
				int j = 0;
				while (j < this.lesLocalisations.size()) {
					match = ((RefLocalisation) this.lesLocalisations.get(j)).getCode().equals(code);
					if (match)
						listModellocalisationpatho.addElement((RefLocalisation) this.lesLocalisations.get(j));
					j++;

				}

			}
			try {
				this.duallist_localisationpatho.setDestinationElements(listModellocalisationpatho);
			} catch (Exception e) {
				System.out.println(e);
				logger.log(Level.SEVERE, " setlesAffichage : localisationpathologie  ", e);
			}
		}

	}

	/**
	 * charge les caract�ristiques du masque � partir de celles sauv�es en base
	 * et remplit le combo
	 */
	private void setlescaracteristiques() {
		if (caracteristiquemasquelotenbase.size() >= 1) {
			DefaultListModel<RefParametre> listModelcar = new DefaultListModel<RefParametre>();
			for (int i = 0; i < caracteristiquemasquelotenbase.size(); i++) {
				Caracteristique car = ((CaracteristiqueMasqueLot) caracteristiquemasquelotenbase.get(i))
						.getCaracteristiquedefaut();
				String code = car.getParametre().getCode();
				boolean match = false;
				int j = 0;
				while (j < this.listModelparametres.size()) {
					match = ((RefParametre) this.listModelparametres.get(j)).getCode().equals(code);
					if (match)
						break;
					j++;
				}
				listModelcar.addElement(((RefParametre) this.listModelparametres.get(j)));
			} // end for
			try {
				this.duallist_carlot.setDestinationElements(listModelcar);
			} catch (Exception e) {
				System.out.println(e);
				logger.log(Level.SEVERE, " setlesAffichage : dualist_carlot  ", e);
			}

		} // end if dualist.size>1
	}

	/**
	 * M�thode de la validation / �criture de la classe MasqueLot
	 */
	private void V_MasqueLot() {
		String result = null;

		try {

			affichage[0] = this.checkbox_taxon.isSelected();
			affichage[1] = this.checkbox_stade.isSelected();
			affichage[2] = this.checkbox_quantite.isSelected();
			// affichage[19] = this.checkbox_effectif.isSelected();
			affichage[3] = this.checkbox_typequantite.isSelected();
			affichage[4] = this.checkbox_methodeobtention.isSelected();
			affichage[5] = this.checkbox_devenir.isSelected();
			affichage[6] = this.checkbox_commentaire.isSelected();
			affichage[7] = this.checkbox_pathologie.isSelected();
			affichage[8] = this.checkbox_localisationpatho.isSelected();
			affichage[9] = this.checkbox_codeprel.isSelected();
			affichage[10] = this.checkbox_operateurprel.isSelected();
			affichage[11] = this.checkbox_typeprel.isSelected();
			affichage[12] = this.checkbox_localisationprel.isSelected();
			affichage[13] = this.checkbox_commentaireprel.isSelected();
			affichage[14] = this.checkbox_operationmarquage.isSelected();
			affichage[15] = this.checkbox_localisationmarquage.isSelected();
			affichage[16] = this.checkbox_naturemarque.isSelected();
			affichage[17] = this.checkbox_actionmarquage.isSelected();
			affichage[18] = this.checkbox_commentairemarquage.isSelected();
			affichage[20] = this.checkbox_importancepatho.isSelected();
			// mise � jour des valeurs du masqueope avec les valeurs bool�ennes
			// collect�es
			this.masquelot.setAffichage(affichage);

		} catch (Exception e) {
			result = e.toString();
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " V_Masquelot  ", e);
		}

		try { // quantite

			valeurdefaut[0] = this.textfield_quantite.getText();

			// typequantite

			valeurdefaut[1] = this.combo_typequantite.getSelectedItem().toString();

			// methodeobtention

			valeurdefaut[2] = this.combo_methodeobtention.getSelectedItem().toString();

			// devenir
			valeurdefaut[3] = this.combo_devenir.getSelectedItem().toString();

			// code prelevement

			valeurdefaut[4] = this.textfield_codeprel.getText();

			// operateur prelevement
			valeurdefaut[5] = this.textfield_operateurprel.getText();

			// type pr�l�vemement
			valeurdefaut[6] = this.combo_typeprel.getSelectedItem().toString();

			// localisation pr�l�vemement
			if (this.combo_localisationprel.getSelectedItem() != null) {
				valeurdefaut[7] = this.combo_localisationprel.getSelectedItem().toString();
			}
			// operation marquage
			if (this.combo_operationmarquage.getSelectedItem() != null) {
				valeurdefaut[8] = this.combo_operationmarquage.getSelectedItem().toString();
			}

			// localisation marquage
			if (this.combo_localisationmarquage.getSelectedItem() != null) {
				valeurdefaut[9] = this.combo_localisationmarquage.getSelectedItem().toString();
			}
			// naturemarque
			if (this.combo_naturemarque.getSelectedItem() != null) {
				valeurdefaut[10] = this.combo_naturemarque.getSelectedItem().toString();
			}
			// action marquage
			if (button_actionmarquage.getSelection() != null) {
				valeurdefaut[11] = button_actionmarquage.getSelection().getActionCommand();
			}
			// effectif
			valeurdefaut[12] = "";// this.textfield_effectif.getText();

			// importance patho
			valeurdefaut[13] = this.combo_importancepathologie.getSelectedItem().toString();

		} catch (Exception e) {
			result = e.toString();
			this.resultat.setText(e.getMessage());
			logger.log(Level.SEVERE, " V_Masquelot  ", e);
		}
		for (int i = 0; i < valeurdefaut.length; i++) {
			if (valeurdefaut[i] == "")
				valeurdefaut[i] = null;
		}
		// mise � jour des valeurs du masquelot avec les valeurs par d�faut
		this.masquelot.setValeurDefaut(valeurdefaut);

		// les listes ne sont initialis�es qu'avec le code du masqueLot
		// et du parm�tre Il faudra attendre l'interface utilisateur
		// EditerCaracterisiqueMasqueLot
		// pour que les d�tails des caracteristiques de lots soient mises � jour

		DefaultListModel<RefParametre> les_parametres = duallist_carlot.getDestListModel();
		this.caracteristiquemasquelot = new SousListeCaracteristiqueMasqueLot(masquelot);
		for (int i = 0; i < les_parametres.getSize(); i++) {
			RefParametre par = (RefParametre) les_parametres.getElementAt(i);
			// la caracteristique est initialis�e sans lot ni valeur par d�faut
			Caracteristique car = new Caracteristique(null, par, null, null);
			String cle_parm = this.masque.getCode() + "_" + par.getCode();
			CaracteristiqueMasqueLot carmasque = new CaracteristiqueMasqueLot(masquelot, car);
			caracteristiquemasquelot.put(cle_parm, carmasque);
		}

		this.affichagemasquelot = new SousListeAffichageMasqueLot(masquelot);
		// taxons
		DefaultListModel<RefTaxon> les_taxons = duallist_taxon.getDestListModel();
		for (int i = 0; i < les_taxons.getSize(); i++) {
			RefTaxon reftax = (RefTaxon) les_taxons.getElementAt(i);
			String code = reftax.getCode();
			String cle = "tr_taxon_tax_duallist_taxon_" + code + "_" + masquelot.getMasque().getCode();
			int rang = i + 1;
			AffichageMasqueLot aff = new AffichageMasqueLot(masquelot, "tr_taxon_tax", code, "duallist_taxon", rang);
			affichagemasquelot.put(cle, aff);
		}
		// stades
		DefaultListModel<RefStade> les_stades = duallist_stade.getDestListModel();
		for (int i = 0; i < les_stades.getSize(); i++) {
			RefStade refstade = (RefStade) les_stades.getElementAt(i);
			String code = refstade.getCode();
			String cle = "tr_stadedeveloppement_std_duallist_stade_" + code + "_" + masquelot.getMasque().getCode();
			int rang = i + 1;
			AffichageMasqueLot aff = new AffichageMasqueLot(masquelot, "tr_stadedeveloppement_std", code,
					"duallist_stade", rang);
			affichagemasquelot.put(cle, aff);
		}

		DefaultListModel<RefPathologie> les_pathologies = duallist_pathologie.getDestListModel();
		for (int i = 0; i < les_pathologies.getSize(); i++) {
			RefPathologie refpathologie = (RefPathologie) les_pathologies.getElementAt(i);
			String code = refpathologie.getCode();
			String cle = "tr_pathologie_pat_duallist_pathologie_" + code + "_" + masquelot.getMasque().getCode();
			int rang = i + 1;
			AffichageMasqueLot aff = new AffichageMasqueLot(masquelot, "tr_pathologie_pat", code, "duallist_pathologie",
					rang);
			affichagemasquelot.put(cle, aff);
		}

		DefaultListModel<RefLocalisation> les_localisationpatho = duallist_localisationpatho.getDestListModel();
		for (int i = 0; i < les_localisationpatho.getSize(); i++) {
			RefLocalisation refloc = (RefLocalisation) les_localisationpatho.getElementAt(i);
			String code = refloc.getCode();
			String cle = "tr_localisation_loc_duallist_localisationpatho_" + code + "_"
					+ masquelot.getMasque().getCode();
			int rang = i + 1;
			AffichageMasqueLot aff = new AffichageMasqueLot(masquelot, "tr_localisation_loc", code,
					"duallist_localisationpatho", rang);
			affichagemasquelot.put(cle, aff);
		}
		// Ecriture du masque lot
		String message = new String();
		if (result == null) {

			try {
				boolean res = masquelot.verifAttributs();
			} catch (Exception e) {
				result = e.getMessage();
				this.resultat.setText(e.getMessage());
				logger.log(Level.SEVERE, " V_Masquelot verifAttributs  ", e);
			}
		}
		if (result == null) {
			if (this.is_masquelot_existing) {
				try {
					masquelot.majObjet();
					message = Message.M1019;
				} catch (Exception e) {
					result = e.getMessage();
					this.resultat.setText(e.getMessage());
					logger.log(Level.SEVERE, " V_Masquelot maj  ", e);
				}
			} else {

				try {
					masquelot.insertObjet();
					message = Message.A1019;
				} catch (Exception e) {
					result = e.getMessage();
					this.resultat.setText(e.getMessage());
					logger.log(Level.SEVERE, " V_Masquelot insert  ", e);
				}
			}
		}
		// a ce stade le masque lot est ins�r�, si pas d'erreur on continue
		if (result == null) {
			for (int i = 0; i < affichagemasquelot.size(); i++) {
				AffichageMasqueLot aml = (AffichageMasqueLot) affichagemasquelot.get(i);
				boolean b = affichagemasquelotenbase.containsKey(affichagemasquelot.getKey(i));
				if (b) {// la cl� existe d�j� en base et dans le combo on ne
					// fait rien !
					try {
						aml.majObjet();
					} catch (Exception e) {
						result = e.getMessage();
						this.resultat.setText(e.getMessage());
						logger.log(Level.SEVERE, " V_Masquelot affichagemasquelot update ", e);
					}
				} else { // la cl� n'existe pas en base mais elle est
					// s�lectionn�e
					try {
						aml.insertObjet();
					} catch (Exception e) {
						result = e.getMessage();
						this.resultat.setText(e.getMessage());
						logger.log(Level.SEVERE, " V_Masquelot affichagemasquelot insert  ", e);
					}
				} // end if b
			} // end for
			for (int i = 0; i < affichagemasquelotenbase.size(); i++) {
				AffichageMasqueLot amlb = (AffichageMasqueLot) affichagemasquelotenbase.get(i);
				boolean b = affichagemasquelot.containsKey(affichagemasquelotenbase.getKey(i));
				if (!b) {
					// apr�s l'insertion, tous les �l�ments en base qui ne
					// sont pas s�lectionn�s doivent �tre retir�s
					try {
						amlb.effaceObjet();
					} catch (Exception e) {
						result = e.getMessage();
						this.resultat.setText(e.getMessage());
						logger.log(Level.SEVERE, " V_Masquelot affichagemasquelot suppression  ", e);
					}
				} // end if b
			} // end for
		} // end if
		if (result == null) {
			charge_editercaracteristique(message);

		} else {// il y a eu une erreur � l'�tape pr�c�dente, il faut
			// supprimer un objet
			try {
				masquelot.effaceObjet();
				message = Message.S1018 + " (insertion annul�e)";
				logger.log(Level.INFO, " Erreur d'�criture du masque lot, effacement de l'objet");
			} catch (Exception e) {
				result = e.getMessage();
				this.resultat.setText(e.getMessage());
				logger.log(Level.SEVERE, " V_Masquelot efface masque lot apr�s erreur affichage  ", e);
			}
		}
	}

	/**
	 * Charge la classe de l'affichage suivant qui permet de rentrer le d�tail
	 * des caract�ristiques de lot
	 * 
	 * @param _message
	 */
	private void charge_editercaracteristique(String _message) {
		Component ihm = this.getParent();
		while ((ihm != null) && !(ihm instanceof IhmAppli)) {
			ihm = ihm.getParent();
		}
		// Passe a l'ecran d'ajout d'une operation
		((IhmAppli) ihm).changeContenu(new EditerCaracteristiqueMasqueLot(masquelot, caracteristiquemasquelot, _message,
				caracteristiquemasquelotenbase));

	}

	/**
	 * Vide le contenu et charge le panneau d'accueil
	 */
	private void quitte() {
		Component ihm = this.getParent();
		while ((ihm != null) && !(ihm instanceof IhmAppli)) {
			ihm = ihm.getParent();
		}
		// Passe a l'ecran d'ajout d'une operation
		((IhmAppli) ihm).removeContenu();
		((IhmAppli) ihm).changeContenu(new Accueil());

	}

}
