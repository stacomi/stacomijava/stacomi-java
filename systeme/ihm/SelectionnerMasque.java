package systeme.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import commun.IhmAppli;
import systeme.MasqueLot;
import systeme.MasqueOpe;
import systeme.referenciel.ListeMasqueLot;
import systeme.referenciel.ListeMasqueOpe;

/**
 * @author cedric.briand
 * @deprecated
 *
 */
public class SelectionnerMasque extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTextField textFieldNomMasque = new JTextField();
	private JComboBox<MasqueOpe> comboMasqueOpe = new JComboBox<MasqueOpe>();
	private JComboBox<MasqueLot> comboMasqueLot = new JComboBox<MasqueLot>();
	private JLabel resultat = new JLabel();
	private JButton button_V = new JButton("Valide");
	private MasqueOpe selectedmasqueope;
	private MasqueLot selectedmasquelot;

	// private MasqueLot masquelotselectionne;
	public SelectionnerMasque() {
		this.initComponents();
		this.chargeMasqueOpe();
		this.chargeMasqueLot();
	}

	private void initComponents() {
		setPreferredSize(new Dimension(800, 600));
		setBackground(new Color(240, 240, 240));
		setLayout(new BorderLayout(0, 0));

		JPanel top = new JPanel();
		add(top, BorderLayout.NORTH);
		top.setLayout(new BorderLayout(0, 0));

		JLabel titreselectionnerunmasque = new JLabel();
		titreselectionnerunmasque.setText("Choisir les masques par d�faut");
		titreselectionnerunmasque.setOpaque(true);
		titreselectionnerunmasque.setHorizontalAlignment(SwingConstants.CENTER);
		titreselectionnerunmasque.setForeground(Color.WHITE);
		titreselectionnerunmasque.setFont(new Font("Dialog", Font.BOLD, 14));
		titreselectionnerunmasque.setBackground(new Color(0, 51, 153));
		top.add(titreselectionnerunmasque, BorderLayout.NORTH);

		resultat.setOpaque(true);
		resultat.setHorizontalAlignment(SwingConstants.CENTER);
		resultat.setForeground(Color.RED);
		resultat.setBackground(Color.WHITE);
		top.add(resultat, BorderLayout.SOUTH);

		JPanel center = new JPanel();
		center.setPreferredSize(new Dimension(600, 400));
		center.setSize(new Dimension(600, 400));
		add(center, BorderLayout.CENTER);
		GridBagLayout gbl_center = new GridBagLayout();
		gbl_center.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_center.columnWidths = new int[] { 0, 0 };
		gbl_center.columnWeights = new double[] { 0.0, 0.0 };
		gbl_center.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		center.setLayout(gbl_center);

		JLabel lblmasqueope = new JLabel("Masque op�ration");
		GridBagConstraints gbc_lblmasqueope = new GridBagConstraints();
		gbc_lblmasqueope.insets = new Insets(5, 5, 5, 5);
		gbc_lblmasqueope.gridx = 0;
		gbc_lblmasqueope.gridy = 0;
		center.add(lblmasqueope, gbc_lblmasqueope);

		comboMasqueOpe = new JComboBox<MasqueOpe>();
		GridBagConstraints gbc_combomasqueope = new GridBagConstraints();
		gbc_combomasqueope.anchor = GridBagConstraints.WEST;
		gbc_combomasqueope.insets = new Insets(5, 5, 5, 5);
		gbc_combomasqueope.gridx = 0;
		gbc_combomasqueope.gridy = 1;
		center.add(comboMasqueOpe, gbc_combomasqueope);

		JLabel lblmasquelot = new JLabel("Masque lot");
		GridBagConstraints gbc_lblmasquelot = new GridBagConstraints();
		gbc_lblmasquelot.insets = new Insets(5, 5, 5, 5);
		gbc_lblmasquelot.gridx = 1;
		gbc_lblmasquelot.gridy = 0;
		center.add(lblmasquelot, gbc_lblmasquelot);

		comboMasqueLot = new JComboBox<MasqueLot>();
		GridBagConstraints gbc_combomasquelot = new GridBagConstraints();
		gbc_combomasquelot.anchor = GridBagConstraints.WEST;
		gbc_combomasquelot.insets = new Insets(5, 5, 5, 5);
		gbc_combomasquelot.gridx = 1;
		gbc_combomasquelot.gridy = 1;
		center.add(comboMasqueLot, gbc_combomasquelot);

		JPanel bottom = new JPanel();
		bottom.setBackground(UIManager.getColor("ToolBar.background"));
		add(bottom, BorderLayout.SOUTH);

		button_V = new JButton();
		button_V.setText("Valider");
		bottom.add(button_V);

		JButton button_annuler = new JButton();
		button_annuler.setText("Annuler");
		bottom.add(button_annuler);

		// this.comboMasqueOpe.addMouseListener(new MouseAdapter() {
		// @Override
		// public void mouseClicked(MouseEvent e) {
		// jcomboMasqueMouseClicked(e);
		// }
		//
		// private void jcomboMasqueMouseClicked(MouseEvent e) {
		// setPreference();
		//
		// }
		//
		// });
		//
		// this.comboMasqueOpe.addActionListener(new ActionListener() {
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// setPreference();
		//
		// }
		// });
		//
		// this.comboMasqueLot.addMouseListener(new MouseAdapter() {
		// @Override
		// public void mouseClicked(MouseEvent e) {
		// jcomboMasqueMouseClicked(e);
		// }
		//
		// private void jcomboMasqueMouseClicked(MouseEvent e) {
		// setPreference();
		//
		// }
		//
		// });
		//
		// this.comboMasqueLot.addActionListener(new ActionListener() {
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// setPreference();
		//
		// }
		// });

		button_annuler.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				quitte();
			}
		});

		button_V.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				setPreference();
			}
		});
	}

	/**
	 * Charge la liste des masquesope et l'affiche dans le combo
	 */
	private void chargeMasqueOpe() {
		ListeMasqueOpe listemasqueope = new ListeMasqueOpe();
		try {
			listemasqueope.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
		}
		this.comboMasqueOpe.setModel(new DefaultComboBoxModel(listemasqueope.toArray()));
		Preferences prefs = Preferences.userRoot();
		String ope_code = prefs.get("masqueope", "opedefaut");
		for (int i = 0; i < listemasqueope.size(); i++) {
			MasqueOpe masqueope = (MasqueOpe) listemasqueope.get(i);
			if (masqueope.getMasque().getCode().equals(ope_code)) {
				this.comboMasqueOpe.setSelectedIndex(i);
			}
		}
	}

	/**
	 * Charge la liste des masques lots et l'affiche dans le combo
	 */
	private void chargeMasqueLot() {
		ListeMasqueLot listemasquelot = new ListeMasqueLot();
		try {
			listemasquelot.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
		}
		this.comboMasqueLot.setModel(new DefaultComboBoxModel(listemasquelot.toArray()));
		Preferences prefs = Preferences.userRoot();
		String lot_code = prefs.get("masquelot", "lotdefaut");
		for (int i = 0; i < listemasquelot.size(); i++) {
			MasqueLot masquelot = (MasqueLot) listemasquelot.get(i);
			if (masquelot.getMasque().getCode().equals(lot_code)) {
				this.comboMasqueLot.setSelectedIndex(i);
			}
		}
	}

	/**
	 * Ecrit le masque s�lectionn� dans les pr�f�rences de l'utilisateur. Ce
	 * masque sera charg� lors de la prochaine ouverture de l'application.
	 */
	private void setPreference() {
		String result = null;
		try {
			selectedmasqueope = (MasqueOpe) this.comboMasqueOpe.getSelectedItem();
			selectedmasquelot = (MasqueLot) this.comboMasqueLot.getSelectedItem();
		} catch (Exception e) {
			result = e.toString();
			this.resultat.setText(e.getMessage());
		}
		if (result == null) {
			Preferences prefs = Preferences.userRoot();
			String ope_mas_code = selectedmasqueope.getMasque().getCode();
			String lot_mas_code = selectedmasquelot.getMasque().getCode();
			// les preferences sont stock�es dans les cl�s masqueope et
			// masquelot
			prefs.put("masqueope", ope_mas_code);
			prefs.put("masquelot", lot_mas_code);
			System.out.println("masques selectionn�s :" + ope_mas_code + " " + lot_mas_code);
			this.resultat.setText("masques selectionn�s :" + ope_mas_code + " " + lot_mas_code);
		}

	}

	/**
	 * Quitte le menu de selection du masque en chargeant la page de d�part
	 */
	private void quitte() {
		Component ihm = this.getParent();
		while ((ihm != null) && !(ihm instanceof IhmAppli)) {
			ihm = ihm.getParent();
		}
		// Passe a l'ecran d'ajout d'une operation
		((IhmAppli) ihm).removeContenu();

	}

}
