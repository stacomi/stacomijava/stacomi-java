/**
 * 
 */
package systeme.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import commun.Accueil;
import commun.DualListBox;
import commun.Erreur;
import commun.IhmAppli;
import commun.JTextFieldDataType;
import commun.Message;
import infrastructure.DC;
import infrastructure.DF;
import infrastructure.Ouvrage;
import infrastructure.Station;
import infrastructure.StationMesure;
import infrastructure.referenciel.ListeStations;
import infrastructure.referenciel.SousListeDC;
import infrastructure.referenciel.SousListeDF;
import infrastructure.referenciel.SousListeOuvrages;
import infrastructure.referenciel.SousListeStationMesure;
import systeme.CondEnvOpe;
import systeme.Masque;
import systeme.MasqueOpe;
import systeme.referenciel.ListeCondEnvOpe;
import systeme.referenciel.ListeMasqueOpe;
import systeme.referenciel.SousListeCondEnvOpe;
import systeme.referenciel.ValeursComboMasque;

/**
 * @author cedric.briand
 *
 */
public class AMSMasqueOpe extends JPanel {

	private static final long serialVersionUID = 1L;
	private Masque masque;
	private MasqueOpe masqueope;
	// le masque op�ration en cours
	private JLabel resultat = new JLabel();
	private JLabel lbCodeMasque = new JLabel();
	private JPanel infra_ope = new JPanel();
	private JComboBox<Station> comboStation;
	private JComboBox<Ouvrage> comboOuvrage;
	private JComboBox<DF> comboDF;
	private JComboBox<DC> comboDC;
	private JTextFieldDataType textField_operateur;
	private JSpinner timeSpinnerheuredebut;
	private JSpinner.DateEditor heuredebut;
	private JSpinner timeSpinnerheurefin;
	private JSpinner.DateEditor heurefin;
	private JSpinner timeSpinnerheure_rearmement;
	private JSpinner.DateEditor heure_rearmement;
	private JCheckBox checkBox_Heure_rearmement;
	private JComboBox<String> combodebutauto;
	private JCheckBox checkBox_Datedebut;
	private JCheckBox checkBox_Heuredebut;
	private JCheckBox checkBox_Datefin;
	private JCheckBox checkBox_Heurefin;
	private JCheckBox checkBox_Operateur;
	private JCheckBox checkBox_Station;
	private JCheckBox checkBox_Ouvrage;
	private JComboBox<String> combofinauto;
	private DualListBox dualstm;
	private JComboBox<String> combo_rearmement;
	private JCheckBox checkBox_DF;
	private JCheckBox checkBox_DC;
	private JCheckBox checkBox_Organismeop;
	private JCheckBox checkBox_Commentaires;

	// Listes et Objets

	private ListeStations lesStations;
	private SousListeOuvrages lesOuvrages;
	private SousListeDF lesDF;
	private SousListeDC lesDC;
	private SousListeDF leDFselectionne;
	private SousListeStationMesure lesStationsMesure;
	private JLabel lbldf;
	private JLabel lbl_heure_rearmement;
	private JCheckBox checkBox_Rearm_renseigne_df;
	private JTextFieldDataType textField_organisme;
	private ListeCondEnvOpe listecondenvope; // liste des conditions
	private SousListeCondEnvOpe les_condenvope_en_base;
	// environnementales (CondEnv)
	private String result;
	private DefaultListModel les_stations_choisies; // DefautListModel =
	// r�sultat du combo
	private String debutauto;
	private String finauto;
	private String rearmement;
	private boolean is_masqueope_existing = false;
	private ListeMasqueOpe listemasqueope;
	private String[] valeurdefaut = new String[9];
	private Boolean[] affichage = new Boolean[16];// Boolean can be yes no or
	// null boolean can be yes
	// or no

	/**
	 * Constructeur du masque Ope, � partir d'un Masque comme argument (on passe
	 * forcement par AMS Masque pour y acc�der)
	 */
	public AMSMasqueOpe(Masque _masque) {
		this.masque = _masque;
		this.masqueope = new MasqueOpe(masque);
		// je repasse tous les d�tails � masque �galement
		this.lbCodeMasque.setText(masque.getCode());
		this.masque = masqueope.getMasque();
		// si l'op�ration existe elle est renseign�e et charg�e
		this.chargeListeStations();
		this.initComponents();
		// charge les valeurs du masque correspondant en base et remplit la
		// valeur test
		this.is_masqueope_existing = this.chargeMasqueOpe();

	}

	@SuppressWarnings("unchecked")
	private void initComponents() {
		setPreferredSize(new Dimension(800, 600));
		setBackground(new Color(240, 240, 240));
		setLayout(new BorderLayout(0, 0));

		JPanel top = new JPanel();
		add(top, BorderLayout.NORTH);
		top.setLayout(new BorderLayout(0, 0));

		JLabel titre = new JLabel();
		titre.setText("Edition d'un masque Op\u00E9rations");
		titre.setOpaque(true);
		titre.setHorizontalAlignment(SwingConstants.CENTER);
		titre.setForeground(Color.WHITE);
		titre.setFont(new Font("Dialog", Font.BOLD, 14));
		titre.setBackground(new Color(0, 51, 153));
		top.add(titre, BorderLayout.NORTH);

		resultat.setOpaque(true);
		resultat.setHorizontalAlignment(SwingConstants.CENTER);
		resultat.setForeground(Color.RED);
		resultat.setBackground(Color.WHITE);
		top.add(resultat, BorderLayout.SOUTH);

		Panel center = new Panel();
		add(center, BorderLayout.WEST);
		center.setLayout(new BoxLayout(center, BoxLayout.X_AXIS));
		infra_ope.setPreferredSize(new Dimension(500, 600));

		center.add(infra_ope);
		infra_ope.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagLayout gbl_infra_ope = new GridBagLayout();
		gbl_infra_ope.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_infra_ope.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gbl_infra_ope.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0 };
		gbl_infra_ope.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0 };
		infra_ope.setLayout(gbl_infra_ope);

		JLabel lbCodeMasque = new JLabel("masque : " + this.masque.getCode());
		lbCodeMasque.setForeground(Color.BLUE);
		GridBagConstraints gbc_lbCodeMasque = new GridBagConstraints();
		gbc_lbCodeMasque.gridwidth = 4;
		gbc_lbCodeMasque.insets = new Insets(0, 0, 5, 5);
		gbc_lbCodeMasque.gridx = 0;
		gbc_lbCodeMasque.gridy = 0;
		infra_ope.add(lbCodeMasque, gbc_lbCodeMasque);

		Label lbinfrastructure = new Label("Infrastructure");
		GridBagConstraints gbc_lbinfrastructure = new GridBagConstraints();
		gbc_lbinfrastructure.gridwidth = 5;
		gbc_lbinfrastructure.insets = new Insets(0, 0, 5, 0);
		gbc_lbinfrastructure.gridx = 0;
		gbc_lbinfrastructure.gridy = 1;
		infra_ope.add(lbinfrastructure, gbc_lbinfrastructure);
		// ===================STATION==================================================
		JLabel labelSta = new JLabel("Station");
		GridBagConstraints gbc_labelSta = new GridBagConstraints();
		gbc_labelSta.insets = new Insets(0, 0, 5, 5);
		gbc_labelSta.gridx = 0;
		gbc_labelSta.gridy = 2;
		infra_ope.add(labelSta, gbc_labelSta);
		labelSta.setLabelFor(comboStation);

		checkBox_Station = new JCheckBox("");
		checkBox_Station.setToolTipText("Cochez la case si vous voulez que le champ soit \u00E9ditable.");
		GridBagConstraints gbc_chckbxStation = new GridBagConstraints();
		gbc_chckbxStation.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxStation.gridx = 1;
		gbc_chckbxStation.gridy = 2;
		infra_ope.add(checkBox_Station, gbc_chckbxStation);
		comboStation = new JComboBox<Station>();
		comboStation.setToolTipText("Station de contr\u00F4le des migrations, choisissez la valeur par d\u00E9faut.");
		comboStation.setPreferredSize(new Dimension(100, 20));
		GridBagConstraints gbc_comboStation = new GridBagConstraints();
		gbc_comboStation.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboStation.insets = new Insets(2, 0, 5, 5);
		gbc_comboStation.gridx = 3;
		gbc_comboStation.gridy = 2;
		infra_ope.add(comboStation, gbc_comboStation);
		this.comboStation.setModel(new DefaultComboBoxModel(this.lesStations.toArray()));

		comboStation.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				this.comboStationActionPerformed(e);
			}

			private void comboStationActionPerformed(java.awt.event.ActionEvent evt) {
				chargeSousListeOuvrages();
				Station station;
				station = (Station) comboStation.getSelectedItem();
				VidangeListeStationsMesures();
				chargeListeStationsMesures(station);
			}
		});

		comboStation.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				this.comboStationmouseClicked(e);
			}

			private void comboStationmouseClicked(MouseEvent e) {
				chargeSousListeOuvrages();
				Station station;
				station = (Station) comboStation.getSelectedItem();
				VidangeListeStationsMesures();
				chargeListeStationsMesures(station);

			}
		});
		// ============================OUVRAGE============================================
		JLabel labelOuv = new JLabel("Ouvrage");
		GridBagConstraints gbc_labelOuv = new GridBagConstraints();
		gbc_labelOuv.insets = new Insets(0, 0, 5, 5);
		gbc_labelOuv.gridx = 0;
		gbc_labelOuv.gridy = 3;
		infra_ope.add(labelOuv, gbc_labelOuv);
		labelOuv.setLabelFor(comboOuvrage);

		checkBox_Ouvrage = new JCheckBox("");
		checkBox_Ouvrage.setToolTipText("Cochez la case si vous voulez que la valeur soit \u00E9ditable");
		GridBagConstraints gbc_checkBoxOuvrage = new GridBagConstraints();
		gbc_checkBoxOuvrage.insets = new Insets(0, 0, 5, 5);
		gbc_checkBoxOuvrage.gridx = 1;
		gbc_checkBoxOuvrage.gridy = 3;
		infra_ope.add(checkBox_Ouvrage, gbc_checkBoxOuvrage);
		comboOuvrage = new JComboBox<Ouvrage>();
		comboOuvrage.setToolTipText("Ouvrage : choisissez la valeur par d\u00E9faut");
		comboOuvrage.setPreferredSize(new Dimension(100, 20));
		GridBagConstraints gbc_comboOuvrage = new GridBagConstraints();
		gbc_comboOuvrage.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboOuvrage.insets = new Insets(2, 0, 5, 5);
		gbc_comboOuvrage.gridx = 3;
		gbc_comboOuvrage.gridy = 3;
		comboOuvrage.setEnabled(false);
		infra_ope.add(comboOuvrage, gbc_comboOuvrage);
		comboOuvrage.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				this.comboOuvragemouseClicked(e);
			}

			private void comboOuvragemouseClicked(MouseEvent e) {
				chargeSousListeDF();

			}
		});
		this.comboOuvrage.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				this.comboOuvrageActionPerformed(e);
			}

			private void comboOuvrageActionPerformed(java.awt.event.ActionEvent evt) {
				chargeSousListeDF();
			}
		});
		// ===============================DF======================================
		JLabel labelDF = new JLabel("DF");
		GridBagConstraints gbc_labelDF = new GridBagConstraints();
		gbc_labelDF.insets = new Insets(0, 0, 5, 5);
		gbc_labelDF.gridx = 0;
		gbc_labelDF.gridy = 4;
		infra_ope.add(labelDF, gbc_labelDF);
		labelDF.setLabelFor(comboDF);

		checkBox_DF = new JCheckBox("");
		checkBox_DF.setToolTipText("Cochez la case si vous voulez que la valeur soit \u00E9ditable");
		GridBagConstraints gbc_checkBoxDF = new GridBagConstraints();
		gbc_checkBoxDF.insets = new Insets(0, 0, 5, 5);
		gbc_checkBoxDF.gridx = 1;
		gbc_checkBoxDF.gridy = 4;
		infra_ope.add(checkBox_DF, gbc_checkBoxDF);
		comboDF = new JComboBox<DF>();
		comboDF.setPreferredSize(new Dimension(100, 20));
		comboDF.setEnabled(false);
		GridBagConstraints gbc_comboDF = new GridBagConstraints();
		gbc_comboDF.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboDF.insets = new Insets(2, 0, 5, 5);
		gbc_comboDF.gridx = 3;
		gbc_comboDF.gridy = 4;
		infra_ope.add(comboDF, gbc_comboDF);
		comboDF.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				this.comboDFmouseClicked(e);
			}

			private void comboDFmouseClicked(MouseEvent e) {
				chargeSousListeDC();

			}
		});
		this.comboDF.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				this.comboDFActionPerformed(e);
			}

			private void comboDFActionPerformed(java.awt.event.ActionEvent evt) {
				chargeSousListeDC();
			}
		});
		// ===============================DC======================================
		JLabel labelDC = new JLabel("DC");
		GridBagConstraints gbc_labelDC = new GridBagConstraints();
		gbc_labelDC.insets = new Insets(0, 0, 5, 5);
		gbc_labelDC.gridx = 0;
		gbc_labelDC.gridy = 5;
		infra_ope.add(labelDC, gbc_labelDC);
		labelDC.setLabelFor(comboDC);

		checkBox_DC = new JCheckBox("");
		checkBox_DC.setToolTipText("Cochez la case si vous voulez que la valeur soit \u00E9ditable");
		GridBagConstraints gbc_checkBoxDC = new GridBagConstraints();
		gbc_checkBoxDC.insets = new Insets(0, 0, 5, 5);
		gbc_checkBoxDC.gridx = 1;
		gbc_checkBoxDC.gridy = 5;
		infra_ope.add(checkBox_DC, gbc_checkBoxDC);
		comboDC = new JComboBox<DC>();
		comboDC.setPreferredSize(new Dimension(100, 20));
		comboDC.setEnabled(false);
		GridBagConstraints gbc_comboDC = new GridBagConstraints();
		gbc_comboDC.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboDC.insets = new Insets(2, 0, 5, 5);
		gbc_comboDC.gridx = 3;
		gbc_comboDC.gridy = 5;
		infra_ope.add(comboDC, gbc_comboDC);
		comboDC.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				this.comboDCmouseClicked(e);
			}

			private void comboDCmouseClicked(MouseEvent e) {
				chargeSousListeDC();

			}
		});
		this.comboDC.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				this.comboDCActionPerformed(e);
			}

			private void comboDCActionPerformed(java.awt.event.ActionEvent evt) {
				chargeSousListeDC();
			}
		});
		// =======================================================================
		JSeparator separator_1 = new JSeparator();
		// ===============================infraOP======================================
		GridBagConstraints gbc_separator_1 = new GridBagConstraints();
		gbc_separator_1.insets = new Insets(0, 0, 5, 5);
		gbc_separator_1.gridx = 2;
		gbc_separator_1.gridy = 6;
		infra_ope.add(separator_1, gbc_separator_1);

		JLabel lbOperation = new JLabel("Op\u00E9ration");
		GridBagConstraints gbc_lbOperation = new GridBagConstraints();
		gbc_lbOperation.gridwidth = 4;
		gbc_lbOperation.insets = new Insets(0, 0, 5, 5);
		gbc_lbOperation.gridx = 0;
		gbc_lbOperation.gridy = 8;
		infra_ope.add(lbOperation, gbc_lbOperation);

		// ===============================Combodebutauto======================================

		JLabel lblRemplissage = new JLabel("D\u00E9but auto ?");
		GridBagConstraints gbc_lblRemplissage = new GridBagConstraints();
		gbc_lblRemplissage.insets = new Insets(0, 0, 5, 5);
		gbc_lblRemplissage.gridx = 0;
		gbc_lblRemplissage.gridy = 9;
		infra_ope.add(lblRemplissage, gbc_lblRemplissage);

		combodebutauto = new JComboBox<String>();
		combodebutauto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (combodebutauto.getSelectedItem() != "Non") {
					timeSpinnerheuredebut.setEnabled(false);
				} else {
					timeSpinnerheuredebut.setEnabled(true);
				}
			}
		});
		combodebutauto.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (combodebutauto.getSelectedItem() != "Non") {
					timeSpinnerheuredebut.setEnabled(false);
				} else {
					timeSpinnerheuredebut.setEnabled(true);
				}
			}
		});
		String[] valeurscombodatedebut = ValeursComboMasque.getchoixdatedebut();
		combodebutauto.setModel(new DefaultComboBoxModel<String>(valeurscombodatedebut));
		combodebutauto.setPreferredSize(new Dimension(100, 20));
		GridBagConstraints gbc_combodebutauto = new GridBagConstraints();
		gbc_combodebutauto.fill = GridBagConstraints.BOTH;
		gbc_combodebutauto.insets = new Insets(0, 0, 5, 5);
		gbc_combodebutauto.gridx = 3;
		gbc_combodebutauto.gridy = 9;
		infra_ope.add(combodebutauto, gbc_combodebutauto);

		// ===============================Date
		// d�but======================================

		JLabel lblDatedebut = new JLabel("Date d\u00E9but");
		GridBagConstraints gbc_lblDatedebut = new GridBagConstraints();
		gbc_lblDatedebut.insets = new Insets(0, 0, 5, 5);
		gbc_lblDatedebut.gridx = 0;
		gbc_lblDatedebut.gridy = 10;
		infra_ope.add(lblDatedebut, gbc_lblDatedebut);

		checkBox_Datedebut = new JCheckBox("");
		checkBox_Datedebut.setToolTipText(
				"Cochez la case si vous voulez que la valeur soit \u00E9ditable. Si vous ne souhaitez pas qu'elle soit \u00E9ditable, laissez d�coch� et choisissez le mode de calcul du d\u00E9but.");
		checkBox_Datedebut.setSelected(true);
		checkBox_Datedebut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				checkvaleurdate();
			}
		});
		GridBagConstraints gbc_checkBox_datedebut = new GridBagConstraints();
		gbc_checkBox_datedebut.insets = new Insets(0, 0, 5, 5);
		gbc_checkBox_datedebut.gridx = 1;
		gbc_checkBox_datedebut.gridy = 10;
		infra_ope.add(checkBox_Datedebut, gbc_checkBox_datedebut);
		// ========Heured�but======================================
		// Note pour la date pas de Textfield, il ne peut pas y avoir de "date
		// par d�faut"
		// Ca change tous les jours....

		JLabel lblHeuredebut = new JLabel("Heure d\u00E9but");
		GridBagConstraints gbc_lblHeuredebut = new GridBagConstraints();
		gbc_lblHeuredebut.insets = new Insets(0, 0, 5, 5);
		gbc_lblHeuredebut.gridx = 0;
		gbc_lblHeuredebut.gridy = 11;
		infra_ope.add(lblHeuredebut, gbc_lblHeuredebut);

		checkBox_Heuredebut = new JCheckBox("");
		checkBox_Heuredebut.setToolTipText(
				"Cochez la case si vous voulez que la valeur soit \u00E9ditable. Sinon renseignez l'heure par d\u00E9faut");
		checkBox_Heuredebut.setSelected(true);
		checkBox_Heuredebut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				checkvaleurdate();
			}
		});
		GridBagConstraints gbc_checkBox_Heuredebut = new GridBagConstraints();
		gbc_checkBox_Heuredebut.insets = new Insets(0, 0, 5, 5);
		gbc_checkBox_Heuredebut.gridx = 1;
		gbc_checkBox_Heuredebut.gridy = 11;
		infra_ope.add(checkBox_Heuredebut, gbc_checkBox_Heuredebut);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 9);
		cal.set(Calendar.MINUTE, 0);
		Date date = cal.getTime();

		timeSpinnerheuredebut = new JSpinner(new SpinnerDateModel(date, null, null, Calendar.MINUTE));
		heuredebut = new JSpinner.DateEditor(timeSpinnerheuredebut, "HH:mm"); // HH:mm
		// 24
		// h
		heuredebut.getTextField().setEditable(true);
		timeSpinnerheuredebut.setEditor(heuredebut);

		// textField_heuredebut = new JTextFieldDataType();
		GridBagConstraints gbc_textField_heuredebut = new GridBagConstraints();
		gbc_textField_heuredebut.insets = new Insets(0, 0, 5, 5);
		gbc_textField_heuredebut.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_heuredebut.gridx = 3;
		gbc_textField_heuredebut.gridy = 11;
		infra_ope.add(timeSpinnerheuredebut, gbc_textField_heuredebut);
		// ===============================Fin
		// auto======================================
		JLabel lblFinAuto = new JLabel("Fin auto ?");
		GridBagConstraints gbc_lblFinAuto = new GridBagConstraints();
		gbc_lblFinAuto.insets = new Insets(0, 0, 5, 5);
		gbc_lblFinAuto.gridx = 0;
		gbc_lblFinAuto.gridy = 12;
		infra_ope.add(lblFinAuto, gbc_lblFinAuto);

		combofinauto = new JComboBox<String>();
		String[] valeurscombodatefin = ValeursComboMasque.getchoixdatefin();
		combofinauto.setModel(new DefaultComboBoxModel<String>(valeurscombodatefin));
		combofinauto.setPreferredSize(new Dimension(100, 20));
		GridBagConstraints gbc_combofinauto = new GridBagConstraints();
		gbc_combofinauto.fill = GridBagConstraints.BOTH;
		gbc_combofinauto.insets = new Insets(0, 0, 5, 5);
		gbc_combofinauto.gridx = 3;
		gbc_combofinauto.gridy = 12;
		infra_ope.add(combofinauto, gbc_combofinauto);
		combofinauto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (combofinauto.getSelectedItem() != "Non") {
					timeSpinnerheurefin.setEnabled(false);
				} else {
					timeSpinnerheurefin.setEnabled(true);
				}
			}
		});
		combofinauto.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (combofinauto.getSelectedItem() != "Non") {
					timeSpinnerheurefin.setEnabled(false);
				} else {
					timeSpinnerheurefin.setEnabled(true);
				}
			}
		});

		// ===============================Date
		// fin======================================
		JLabel lblDatefin = new JLabel("Date fin");
		GridBagConstraints gbc_lblDatefin = new GridBagConstraints();
		gbc_lblDatefin.insets = new Insets(0, 0, 5, 5);
		gbc_lblDatefin.gridx = 0;
		gbc_lblDatefin.gridy = 13;
		infra_ope.add(lblDatefin, gbc_lblDatefin);

		checkBox_Datefin = new JCheckBox("");
		checkBox_Datefin.setToolTipText(
				"Cochez la case si vous voulez que la valeur soit \u00E9ditable. Si vous ne souhaitez pas qu'elle soit \u00E9ditable, choisissez le mode de calcul de la fin.");
		checkBox_Datefin.setSelected(true);
		checkBox_Datefin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				checkvaleurdate();
			}
		});
		GridBagConstraints gbc_checkBox_datefin = new GridBagConstraints();
		gbc_checkBox_datefin.insets = new Insets(0, 0, 5, 5);
		gbc_checkBox_datefin.gridx = 1;
		gbc_checkBox_datefin.gridy = 13;
		infra_ope.add(checkBox_Datefin, gbc_checkBox_datefin);
		// ===============================Heure
		// fin======================================
		JLabel lblHeurefin = new JLabel("Heure fin");
		GridBagConstraints gbc_lblHeurefin = new GridBagConstraints();
		gbc_lblHeurefin.insets = new Insets(0, 0, 5, 5);
		gbc_lblHeurefin.gridx = 0;
		gbc_lblHeurefin.gridy = 14;
		infra_ope.add(lblHeurefin, gbc_lblHeurefin);

		checkBox_Heurefin = new JCheckBox("");
		checkBox_Heurefin.setToolTipText(
				"Cochez la case si vous voulez que la valeur soit \u00E9ditable. Sinon renseignez l'heure par d\u00E9faut");
		checkBox_Heurefin.setSelected(true);
		checkBox_Heurefin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				checkvaleurdate();
			}
		});
		GridBagConstraints gbc_checkBox_Heurefin = new GridBagConstraints();
		gbc_checkBox_Heurefin.insets = new Insets(0, 0, 5, 5);
		gbc_checkBox_Heurefin.gridx = 1;
		gbc_checkBox_Heurefin.gridy = 14;
		infra_ope.add(checkBox_Heurefin, gbc_checkBox_Heurefin);

		// la date est pr�c�demment initialis�e � 9:00 dans heure d�but
		timeSpinnerheurefin = new JSpinner(new SpinnerDateModel(date, null, null, Calendar.MINUTE));
		heurefin = new JSpinner.DateEditor(timeSpinnerheurefin, "HH:mm"); // HH:mm
		heurefin.getTextField().setEditable(true);
		timeSpinnerheurefin.setEditor(heurefin);

		// textField_heurefin = new JTextFieldDataType();
		// textField_heurefin.setColumns(10);
		GridBagConstraints gbc_textField_heurefin = new GridBagConstraints();
		gbc_textField_heurefin.insets = new Insets(0, 0, 5, 5);
		gbc_textField_heurefin.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_heurefin.gridx = 3;
		gbc_textField_heurefin.gridy = 14;
		infra_ope.add(timeSpinnerheurefin, gbc_textField_heurefin);
		// ===============================Op�rateur======================================
		JLabel lblOprateur = new JLabel("Op\u00E9rateur");
		GridBagConstraints gbc_lblOprateur = new GridBagConstraints();
		gbc_lblOprateur.insets = new Insets(0, 0, 5, 5);
		gbc_lblOprateur.gridx = 0;
		gbc_lblOprateur.gridy = 15;
		infra_ope.add(lblOprateur, gbc_lblOprateur);

		checkBox_Operateur = new JCheckBox("");
		checkBox_Operateur.setToolTipText(
				"Cochez la case si vous voulez que la valeur soit \u00E9ditable. Sinon renseignez dans le champ texte la valeur par d\u00E9faut et laissez la case d\u00E9coch\u00E9e");
		GridBagConstraints gbc_checkBox_Operateur = new GridBagConstraints();
		gbc_checkBox_Operateur.insets = new Insets(0, 0, 5, 5);
		gbc_checkBox_Operateur.gridx = 1;
		gbc_checkBox_Operateur.gridy = 15;
		infra_ope.add(checkBox_Operateur, gbc_checkBox_Operateur);

		textField_operateur = new JTextFieldDataType();
		GridBagConstraints gbc_textField_operateur = new GridBagConstraints();
		gbc_textField_operateur.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_operateur.insets = new Insets(0, 0, 5, 5);
		gbc_textField_operateur.gridx = 3;
		gbc_textField_operateur.gridy = 15;
		infra_ope.add(textField_operateur, gbc_textField_operateur);
		textField_operateur.setColumns(10);
		// ===============================Organsime======================================
		JLabel lblOrganisme = new JLabel("Organisme");
		GridBagConstraints gbc_lblOrganisme = new GridBagConstraints();
		gbc_lblOrganisme.insets = new Insets(0, 0, 5, 5);
		gbc_lblOrganisme.gridx = 0;
		gbc_lblOrganisme.gridy = 16;
		infra_ope.add(lblOrganisme, gbc_lblOrganisme);

		checkBox_Organismeop = new JCheckBox("");
		checkBox_Organismeop.setToolTipText(
				"conseil : la valeur de l'organisme peut \u00EAtre diff\u00E9rente de la valeur rentr\u00E9e par d\u00E9faut dans toutes les tables (ex : IAV), a utiliser lorsque plusieurs \"organismes op\u00E9rateurs\" interviennent sur une m\u00EAme passe (ex : INRA et MIGRADOUR)");
		GridBagConstraints gbc_checkBox_Organismeop = new GridBagConstraints();
		gbc_checkBox_Organismeop.insets = new Insets(0, 0, 5, 5);
		gbc_checkBox_Organismeop.gridx = 1;
		gbc_checkBox_Organismeop.gridy = 16;
		infra_ope.add(checkBox_Organismeop, gbc_checkBox_Organismeop);

		textField_organisme = new JTextFieldDataType();
		GridBagConstraints gbc_textFieldorganisme = new GridBagConstraints();
		gbc_textFieldorganisme.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldorganisme.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldorganisme.gridx = 3;
		gbc_textFieldorganisme.gridy = 16;
		infra_ope.add(textField_organisme, gbc_textFieldorganisme);
		// ===============================Commentaires======================================
		JLabel lblCommentaires = new JLabel("Commentaires");
		GridBagConstraints gbc_lblCommentaires = new GridBagConstraints();
		gbc_lblCommentaires.insets = new Insets(0, 0, 0, 5);
		gbc_lblCommentaires.gridx = 0;
		gbc_lblCommentaires.gridy = 17;
		infra_ope.add(lblCommentaires, gbc_lblCommentaires);

		checkBox_Commentaires = new JCheckBox("");
		checkBox_Commentaires.setToolTipText(
				"Laissez la case d\u00E9coch\u00E9e si vous n'entrez pas de commentaire sur l'op\u00E9ration de contr\u00F4le des migrations.");
		GridBagConstraints gbc_checkBox_commentairesop = new GridBagConstraints();
		gbc_checkBox_commentairesop.insets = new Insets(0, 0, 0, 5);
		gbc_checkBox_commentairesop.gridx = 1;
		gbc_checkBox_commentairesop.gridy = 17;
		infra_ope.add(checkBox_Commentaires, gbc_checkBox_commentairesop);
		// =====================================================================
		// ==========================Disp_cond_env===================================
		// =====================================================================
		JPanel disp_cond_env = new JPanel();
		disp_cond_env.setPreferredSize(new Dimension(500, 600));
		disp_cond_env.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		center.add(disp_cond_env);
		GridBagLayout gbl_disp_cond_env = new GridBagLayout();
		gbl_disp_cond_env.columnWidths = new int[] { 0, 100, 10, 100 };
		gbl_disp_cond_env.rowHeights = new int[] { 0, 0, 0, 0, 61, 0, 150, 3, 3, 0, 0, 0, 0, 0, 0 };
		gbl_disp_cond_env.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0 };
		gbl_disp_cond_env.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0 };
		disp_cond_env.setLayout(gbl_disp_cond_env);
		// ===============================R�armement======================================
		Label label_5 = new Label("R\u00E9armement");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.gridwidth = 4;
		gbc_label_5.insets = new Insets(0, 0, 5, 0);
		gbc_label_5.gridx = 0;
		gbc_label_5.gridy = 0;
		disp_cond_env.add(label_5, gbc_label_5);
		String[] valeurscomborearmement = ValeursComboMasque.getchoixrearmement();

		combo_rearmement = new JComboBox<String>();
		combo_rearmement.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// v�rifie si la valeur est oui et active les composants
				checkrearmement();
			}
		});

		combo_rearmement.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				checkrearmement();
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
		combo_rearmement.setModel(new DefaultComboBoxModel<String>(valeurscomborearmement));
		combo_rearmement.setPreferredSize(new Dimension(100, 20));
		GridBagConstraints gbc_comborearmement = new GridBagConstraints();
		gbc_comborearmement.anchor = GridBagConstraints.EAST;

		gbc_comborearmement.fill = GridBagConstraints.VERTICAL;
		gbc_comborearmement.insets = new Insets(0, 0, 5, 5);
		gbc_comborearmement.gridx = 1;
		gbc_comborearmement.gridy = 1;
		disp_cond_env.add(combo_rearmement, gbc_comborearmement);
		combo_rearmement.setToolTipText("L'heure de r�armement du pi�ge renseigne sur les temps de fct du DF et DC.");

		lbl_heure_rearmement = new JLabel("Heure");
		lbl_heure_rearmement.setEnabled(false);
		GridBagConstraints gbc_lblHeure = new GridBagConstraints();
		gbc_lblHeure.anchor = GridBagConstraints.EAST;
		gbc_lblHeure.insets = new Insets(0, 0, 5, 5);
		gbc_lblHeure.gridx = 0;
		gbc_lblHeure.gridy = 2;
		disp_cond_env.add(lbl_heure_rearmement, gbc_lblHeure);

		checkBox_Heure_rearmement = new JCheckBox("");
		checkBox_Heure_rearmement.setEnabled(false);
		checkBox_Heure_rearmement.setToolTipText("Voulez vous afficher l'heure de r�armement ?");
		GridBagConstraints gbc_checkBox_heure_rearmement = new GridBagConstraints();
		gbc_checkBox_heure_rearmement.insets = new Insets(0, 0, 5, 5);
		gbc_checkBox_heure_rearmement.gridx = 1;
		gbc_checkBox_heure_rearmement.gridy = 2;
		disp_cond_env.add(checkBox_Heure_rearmement, gbc_checkBox_heure_rearmement);

		timeSpinnerheure_rearmement = new JSpinner(new SpinnerDateModel(date, null, null, Calendar.MINUTE));
		timeSpinnerheure_rearmement.setEnabled(false);
		heure_rearmement = new JSpinner.DateEditor(timeSpinnerheure_rearmement, "HH:mm"); // HH:mm
		// for
		// 24
		// h
		// mode
		timeSpinnerheure_rearmement.setEditor(heure_rearmement);

		timeSpinnerheure_rearmement.setToolTipText(
				" Par d\u00E9faut renseigne sur le fonctionnement du DC mais deux options sont possibles ci-dessous. Renseignez l'heure si vous voulez une heure par d\u00E9faut\r\n");
		GridBagConstraints gbc_textField_heure_rearmement = new GridBagConstraints();
		gbc_textField_heure_rearmement.gridwidth = 2;
		gbc_textField_heure_rearmement.insets = new Insets(0, 0, 5, 0);
		gbc_textField_heure_rearmement.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_heure_rearmement.gridx = 2;
		gbc_textField_heure_rearmement.gridy = 2;
		disp_cond_env.add(timeSpinnerheure_rearmement, gbc_textField_heure_rearmement);

		lbldf = new JLabel("DF");
		lbldf.setEnabled(false);
		GridBagConstraints gbc_lblOpration = new GridBagConstraints();
		gbc_lblOpration.insets = new Insets(0, 0, 5, 5);
		gbc_lblOpration.gridx = 0;
		gbc_lblOpration.gridy = 3;
		disp_cond_env.add(lbldf, gbc_lblOpration);

		checkBox_Rearm_renseigne_df = new JCheckBox("");
		checkBox_Rearm_renseigne_df.setEnabled(false);
		checkBox_Rearm_renseigne_df.setToolTipText(
				"La date de r\u00E9-armement du dispositif renseigne aussi  sur le fonctionnement du DF (cas de grilles qui sont descendues pour bloquer les poissons lors de la rel\u00E8ve).");
		GridBagConstraints gbc_checkBox_rearm_renseigne_df = new GridBagConstraints();
		gbc_checkBox_rearm_renseigne_df.insets = new Insets(0, 0, 5, 5);
		gbc_checkBox_rearm_renseigne_df.gridx = 1;
		gbc_checkBox_rearm_renseigne_df.gridy = 3;
		disp_cond_env.add(checkBox_Rearm_renseigne_df, gbc_checkBox_rearm_renseigne_df);

		JSeparator separator_2 = new JSeparator();
		GridBagConstraints gbc_separator_2 = new GridBagConstraints();
		gbc_separator_2.insets = new Insets(0, 0, 5, 5);
		gbc_separator_2.gridx = 0;
		gbc_separator_2.gridy = 4;
		disp_cond_env.add(separator_2, gbc_separator_2);

		// ===============================Station de
		// mesure======================================

		dualstm = new DualListBox();
		dualstm.setSourceChoicesTitle("Stations Mesure");

		JLabel lblConditionsEnvironnementales = new JLabel("Conditions environnementales");
		GridBagConstraints gbc_lblConditionsEnvironnementales = new GridBagConstraints();
		gbc_lblConditionsEnvironnementales.insets = new Insets(0, 0, 5, 5);
		gbc_lblConditionsEnvironnementales.gridx = 1;
		gbc_lblConditionsEnvironnementales.gridy = 5;
		disp_cond_env.add(lblConditionsEnvironnementales, gbc_lblConditionsEnvironnementales);
		GridBagConstraints gbc_dualstm = new GridBagConstraints();
		gbc_dualstm.gridwidth = 4;
		gbc_dualstm.fill = GridBagConstraints.BOTH;
		gbc_dualstm.insets = new Insets(0, 0, 5, 0);
		gbc_dualstm.gridx = 0;
		gbc_dualstm.gridy = 6;
		// disp_cond_env.add(scrollPane_1, gbc_scrollPane_1);
		disp_cond_env.add(dualstm, gbc_dualstm);

		// ===============================Bottom======================================
		JPanel bottom = new JPanel();
		bottom.setBackground(UIManager.getColor("ToolBar.background"));
		add(bottom, BorderLayout.SOUTH);
		// ===============================Buttons======================================

		JButton button_V = new JButton();
		button_V.setText("Valider");
		bottom.add(button_V);

		JButton button_annuler = new JButton();
		button_annuler.setText("Annuler");
		bottom.add(button_annuler);
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[] { checkBox_Station, comboStation,
				checkBox_Ouvrage, comboOuvrage, checkBox_DF, comboDF, checkBox_DC, comboDC, combodebutauto,
				checkBox_Datedebut, checkBox_Heuredebut, timeSpinnerheuredebut, combofinauto, checkBox_Datefin,
				checkBox_Heurefin, timeSpinnerheurefin, checkBox_Operateur, textField_operateur, checkBox_Organismeop,
				textField_organisme, checkBox_Commentaires, disp_cond_env, combo_rearmement, checkBox_Heure_rearmement,
				timeSpinnerheure_rearmement, checkBox_Rearm_renseigne_df, separator_2, dualstm, button_V,
				button_annuler }));

		button_V.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				V_MasqueOP();
			}
		});

		button_annuler.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				quitte();

			}
		});

	}

	/**
	 * M�thode de chargement des donn�es du masque en cours
	 */
	/**
	 * @return
	 */
	private boolean chargeMasqueOpe() {
		this.listemasqueope = new ListeMasqueOpe();
		try {
			listemasqueope.chargeFiltre(masque.getCode());
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
		}
		this.les_condenvope_en_base = new SousListeCondEnvOpe(masqueope);
		try {
			les_condenvope_en_base.chargeSansFiltre();
			// la m�thode chargeSansFiltre charge tous les objets de la sous
			// liste du masqueope
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
		}
		// si il y aune valeur, le masque existe en base
		if (listemasqueope.size() == 1) {
			int index; // indice de la valeur s�lectionn�e dans le masque pour
			// chaque combo
			// r�cup�ration des donn�es du masque en cours
			this.masqueope = (MasqueOpe) this.listemasqueope.get(0);
			// je dois quand m�me aller r�cup�rer l'identifiant integer du
			// masque
			// sinon plante a l'update ou � l'insertion.
			// il a �t� pass� par masque...
			this.masqueope.setMasque(this.masque);
			this.affichage = masqueope.getAffichage();
			this.valeurdefaut = masqueope.getValeurDefaut();
			// ---------------------------------------------------------------------
			// mise � jour des combos et textfields � partir des valeurs par
			// d�faut
			// ---------------------------------------------------------------------
			if (valeurdefaut[0] != null) {
				index = IhmAppli.getIndexforString(valeurdefaut[0], this.comboStation);
				this.comboStation.setSelectedIndex(index);
				this.chargeListeStationsMesures((Station) this.comboStation.getSelectedItem());
				this.chargeSousListeOuvrages();
				if (valeurdefaut[1] != null) {
					index = IhmAppli.getIndexforString(valeurdefaut[1], this.comboOuvrage);
					this.comboOuvrage.setSelectedIndex(index);
					this.chargeSousListeDF();
					if (valeurdefaut[2] != null) {
						index = IhmAppli.getIndexforString(valeurdefaut[2], this.comboDF);
						this.comboDF.setSelectedIndex(index);
						this.chargeSousListeDC();
						if (valeurdefaut[3] != null) {
							index = IhmAppli.getIndexforString(valeurdefaut[3], this.comboDC);
							this.comboDC.setSelectedIndex(index);
						}
					} else {
						// si le comboDF est null on lui met false
						this.comboDF.setEnabled(false);
						;
					}
				} else {
					this.comboOuvrage.setEnabled(false);
				}

			}

			String str_heuredebut = valeurdefaut[4];
			if (!(str_heuredebut == null)) {
				try {
					int intheuredebut = Integer.parseInt(str_heuredebut.split(":")[0]);
					int intminutedebut = Integer.parseInt(str_heuredebut.split(":")[1]);
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.HOUR_OF_DAY, intheuredebut);
					cal.set(Calendar.MINUTE, intminutedebut);
					Date date = cal.getTime();
					// SimpleDateFormat formatter = heuredebut.getFormat();
					// String formattedheuredebut = formatter.format(date);
					timeSpinnerheuredebut.setValue(date);
				} catch (IllegalArgumentException e) {
					resultat.setText(e.toString());
				}
			}
			String str_heurefin = valeurdefaut[5];
			if (!(str_heurefin == null)) {
				try {
					int intheurefin = Integer.parseInt(str_heurefin.split(":")[0]);
					int intminutefin = Integer.parseInt(str_heurefin.split(":")[1]);
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.HOUR_OF_DAY, intheurefin);
					cal.set(Calendar.MINUTE, intminutefin);
					Date date = cal.getTime();
					timeSpinnerheurefin.setValue(date);// HH:mm
				} catch (IllegalArgumentException e) {
					resultat.setText(e.toString());
				}
			}
			String str_heurerearmement = valeurdefaut[8];
			if (!(str_heurerearmement == null)) {
				try {
					int intheurerearmement = Integer.parseInt(str_heurerearmement.split(":")[0]);
					int intminuterearmement = Integer.parseInt(str_heurerearmement.split(":")[1]);
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.HOUR_OF_DAY, intheurerearmement);
					cal.set(Calendar.MINUTE, intminuterearmement);
					Date date = cal.getTime();
					timeSpinnerheure_rearmement.setValue(date);// HH:mm
				} catch (IllegalArgumentException e) {
					resultat.setText(e.toString());
				}
			}
			;
			String str_operateur = valeurdefaut[6];
			this.textField_operateur.setText(str_operateur);
			String str_organisme = valeurdefaut[7];
			this.textField_organisme.setText(str_organisme);
			// ---------------------------------------------------------------------
			// charge les valeus des checkboxes
			// ---------------------------------------------------------------------
			this.checkBox_Station.setSelected(affichage[0]);
			this.checkBox_Ouvrage.setSelected(affichage[1]);
			this.checkBox_DF.setSelected(affichage[2]);
			this.checkBox_DC.setSelected(affichage[3]);
			this.checkBox_Heuredebut.setSelected(affichage[4]);
			this.checkBox_Heurefin.setSelected(affichage[5]);
			this.checkBox_Operateur.setSelected(affichage[6]);
			this.checkBox_Organismeop.setSelected(affichage[7]);
			this.checkBox_Heure_rearmement.setSelected(affichage[8]);
			if (affichage[9])
				this.combodebutauto.setSelectedIndex(1); // "oui"
			if (affichage[10])
				this.combofinauto.setSelectedIndex(1);// "oui"
			if (affichage[11]) {
				this.combo_rearmement.setSelectedIndex(1);// "oui"
				timeSpinnerheure_rearmement.setEnabled(true);
				checkBox_Heure_rearmement.setEnabled(true);
				checkBox_Rearm_renseigne_df.setEnabled(true);
				lbl_heure_rearmement.setEnabled(true);
				lbldf.setEnabled(true);
			}
			this.checkBox_Rearm_renseigne_df.setSelected(affichage[12]);
			this.checkBox_Datedebut.setSelected(affichage[12]);
			this.checkBox_Datefin.setSelected(affichage[13]);
			this.checkBox_Commentaires.setSelected(affichage[14]);
			// ---------------------------------------------------------------------
			// Chargement des stations selectionn�es dans le masque
			// ---------------------------------------------------------------------
			// la m�thode ci dessous va mettre � jourles_condenvope_en_base
			// en supprimant les stations qui n'existent plus
			this.setLesStationsMesureSelectionnees();

			// ---------------------------------------------------------------------
			// retourne la valeur indiquant que le masque �tait effectivement en
			// base
			// ---------------------------------------------------------------------
			return (true);
		} else {
			return (false); // le masque n'est pas en base
		}

	}

	/**
	 * Chargement des Stations de mesure associ�es � une station (s�lectionn�e
	 * dans le combo de d�part)
	 * 
	 * @param station
	 */

	private void chargeListeStationsMesures(Station station) {
		// chargement de la liste des stations de mesures rattach�es � la
		// station
		this.lesStationsMesure = new SousListeStationMesure(station);

		try {
			lesStationsMesure.chargeSansFiltreDetails();
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
		}
		DefaultListModel<StationMesure> listModel = new DefaultListModel<StationMesure>();
		for (int i = 0; i < lesStationsMesure.size(); i++) {
			StationMesure stm = (StationMesure) lesStationsMesure.get(i);
			listModel.addElement(stm);
		}
		try {

			this.dualstm.addSourceElements(listModel);
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	/**
	 * Vidange des stations de mesure
	 * 
	 */

	private void VidangeListeStationsMesures() {
		this.dualstm.clearSourceListModel();
		this.dualstm.clearDestinationListModel();
	}

	// charge la liste des stations
	private void chargeListeStations() {
		this.lesStations = new ListeStations();
		try {
			this.lesStations.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1003);
		}

	}

	// charge la sous liste des ouvrages
	private void chargeSousListeOuvrages() {
		Station station; // la station selectionnee

		// Recupere la station selectionne dans le combo
		station = (Station) this.comboStation.getSelectedItem();

		// Vide la liste des station pour garder uniquement la station
		// selectionnee
		this.lesStations = new ListeStations();
		this.lesStations.put(station.getCode(), station);

		// charge la sous liste des ouvrages
		this.lesOuvrages = new SousListeOuvrages(station);

		try {
			this.lesOuvrages.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1004);
		}

		// Affectation de la sous liste des ouvrages a la station
		station.setLesElementsConstitutifs(this.lesOuvrages);

		// actualiste les combo
		this.comboOuvrage.setModel(new DefaultComboBoxModel(this.lesOuvrages.toArray()));

		this.comboOuvrage.setEnabled(true);
		this.comboDF.setEnabled(false);
		this.comboDC.setEnabled(false);
	}

	// charge la sous liste des DF
	// Lanc� par OK ouvrage ou modif combo box ouvrage
	private void chargeSousListeDF() {
		Ouvrage ouvrage; // l'ouvrage selectionne

		// Recupere l'ouvrage selectionne dans le combo
		ouvrage = (Ouvrage) this.comboOuvrage.getSelectedItem();

		// Vide la liste des ouvrages pour garder uniquement celui selectionne
		this.lesOuvrages = new SousListeOuvrages();
		this.lesOuvrages.put(ouvrage.getIdentifiant(), ouvrage);

		// charge la sous liste des df
		this.lesDF = new SousListeDF(ouvrage);

		try {
			this.lesDF.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1005);
		}

		// Affectation de la sous liste des df a l'ouvrage
		ouvrage.setLesElementsConstitutifs(this.lesDF);

		// actualiste les combo
		this.comboDF.setModel(new DefaultComboBoxModel(this.lesDF.toArray()));
		this.comboDF.setEnabled(true);
		this.comboDC.setEnabled(false);
		// Essai de renvoi de la valeur dans le combo
		// C'est dans ouvrage que je dois activer l'affichage
		// charge les le d�tail des autres champs du DF
		DF df = (DF) this.comboDF.getSelectedItem();
		// Vide la liste des df pour garder uniquement celui selectionne

		this.leDFselectionne = new SousListeDF();
		this.leDFselectionne.put(df.getIdentifiant(), df);
		// Charge les caract�ristiques du DF, dont celles r�cup�r�es dans la
		// liste abstaite dispositif
		try {
			this.leDFselectionne.chargeObjets();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1005);
		}
		df = (DF) this.leDFselectionne.elements().nextElement();
		this.comboDF.setToolTipText(df.getCommentaires());
	}

	// charge la sous liste des DC
	private void chargeSousListeDC() {
		// le DF selectionn�
		DF df = (DF) this.comboDF.getSelectedItem();

		// charge la sous liste des dc
		this.lesDC = new SousListeDC(df);

		try {
			this.lesDC.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1006);
		}

		// Affectation de la sous liste des dc au df
		df.setLesDC(this.lesDC);

		// actualiste le combo
		this.comboDC.setModel(new DefaultComboBoxModel(this.lesDC.toArray()));
		this.comboDC.setEnabled(true);
	}

	// vide la liste des dc pour ne garder que ceului selectionne
	private void videListeDC() {
		DC dc; // le dc selectionne

		// Recupere le dc selectionne dans le combo
		dc = (DC) this.comboDC.getSelectedItem();

		// Vide la liste des dc pour garder uniquement celui selectionne
		this.lesDC = new SousListeDC();
		this.lesDC.put(dc.getIdentifiant(), dc);

	}

	/**
	 * V�rification que la date est doit remplie par d�faut, soit d�coch�e
	 * l'utilisateur doit avoir une valeur de date de d�but et de date de fin
	 * pour pouvoir saisir son op�ration
	 */
	private void checkvaleurdate() {
		// si pas de valeur par d�faut pour heure d�but
		// ET pas de calcul par d�faut ("Non")
		// ET une des deux cases est d�coch�e (pas d'affichage) => probl�me

		String debutauto = (String) this.combodebutauto.getSelectedItem();
		String finauto = (String) this.combofinauto.getSelectedItem();
		Boolean datedebutselected = this.checkBox_Datedebut.isSelected();
		Boolean heuredebutselected = this.checkBox_Heuredebut.isSelected();
		Boolean datefinselected = this.checkBox_Datefin.isSelected();
		Boolean heurefinselected = this.checkBox_Heurefin.isSelected();
		if (debutauto == "Non" && (!datedebutselected | !heuredebutselected)) {
			JOptionPane.showMessageDialog(this.infra_ope,
					"Attention il faudra toujours saisir une valeur de date et heure pour le d�but de l'op�ration, "
							+ "cochez la case ou choisissez une valeur par d�faut",
					" Message d'information", JOptionPane.WARNING_MESSAGE);
		}
		if (finauto == "Non" && (!datefinselected | !heurefinselected)) {
			JOptionPane.showMessageDialog(this.infra_ope,
					"Attention il faudra toujours saisir une valeur de date et heure pour la fin de l'op�ration, "
							+ "cochez la case ou choisissez une valeur par d�faut",
					" Message d'information", JOptionPane.WARNING_MESSAGE);
		}
	}

	/**
	 * en fonction du choix oui/ non dans r�armement, l'utilisateur �dite ou non
	 * les valeurs par d�faut
	 */
	private void checkrearmement() {

		String rearmement = (String) this.combo_rearmement.getSelectedItem();
		if (rearmement == "Oui") {
			timeSpinnerheure_rearmement.setEnabled(true);
			checkBox_Heure_rearmement.setEnabled(true);
			checkBox_Rearm_renseigne_df.setEnabled(true);
			lbl_heure_rearmement.setEnabled(true);
			lbldf.setEnabled(true);
		} else {
			timeSpinnerheure_rearmement.setEnabled(false);
			checkBox_Heure_rearmement.setEnabled(false);
			lbl_heure_rearmement.setEnabled(false);
			lbldf.setEnabled(false);
			checkBox_Rearm_renseigne_df.setEnabled(false);
		}

	}

	/**
	 * Vide le contenu et charge le panneau d'accueil
	 */
	private void quitte() {
		Component ihm = this.getParent();
		while ((ihm != null) && !(ihm instanceof IhmAppli)) {
			ihm = ihm.getParent();
		}
		// Passe a l'ecran d'ajout d'une operation
		((IhmAppli) ihm).removeContenu();
		((IhmAppli) ihm).changeContenu(new Accueil());

	}

	/**
	 * Cette m�thode construit un objet de class ListCondEnvOpe
	 * 
	 * @return une liste d'objet de classe CondEnvOpe initialis�s avec avec le
	 *         masqueOpe r�cup�r� depuis cette instance de AMSMasqueOpe et
	 *         chaque station de Mesure correspondant aux objets list�s dans le
	 *         dualjlistbox
	 */
	private ListeCondEnvOpe getLesStationsMesureSelectionnees() {
		// la liste correspond aux condititons environnementales (stations de
		// mesure)
		// rattach�es au masque op�ration
		ListeCondEnvOpe _listecondenvope = new ListeCondEnvOpe();
		this.les_stations_choisies = dualstm.getDestListModel();
		int size = les_stations_choisies.getSize();
		for (int i = 0; i < size; i++) {
			StationMesure stm = (StationMesure) les_stations_choisies.getElementAt(i);
			String cle_condenv = this.masque.getCode() + "_" + stm.getIdentifiant();
			CondEnvOpe condenvope = new CondEnvOpe(this.masqueope, stm);
			_listecondenvope.put(cle_condenv, condenvope);
		}
		return _listecondenvope;
	}

	/**
	 * Cette methode remplit le combo de destination � partir de valeurs en base
	 * et renvoit la sslit revoy�e � partir de la selection parmi les stm de la
	 * station des stm pr�sentes dans masque en base ... On filtre les stations
	 * suprim�es entre temps ou qui seraient par erreur dans le masque.
	 *
	 */
	private void setLesStationsMesureSelectionnees() {
		if (this.lesStationsMesure != null) {
			DefaultListModel<StationMesure> listModel = new DefaultListModel<StationMesure>();
			SousListeCondEnvOpe sslist = intersect_souslistestationmesure_les_condenvope_en_base();
			for (int i = 0; i < sslist.size(); i++) {
				CondEnvOpe ceo = (CondEnvOpe) sslist.get(i);
				StationMesure stm = ceo.getStationmesure();
				listModel.addElement(stm);
			}
			try {
				this.dualstm.setDestinationElements(listModel);
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		;
	}

	/**
	 * Cette m�thode s�lectionne parmis les elements ceux qui sont d�j�
	 * s�lectionn�s attention de ne pas modifier les �l�ments, la modif avec les
	 * �l�ments "en base" devra se faire plus tard sinon il ne s'agit plus des
	 * m�mes objets et on ne peut plus les passer au combo.
	 * 
	 * @param stm
	 *            une station de mesure (en fait elle est ici construite juste
	 *            avec son identifiant)
	 * @return un entier avec la position de cette station dans la liste des
	 *         stations de mesure, -1 sinon
	 */
	private SousListeCondEnvOpe intersect_souslistestationmesure_les_condenvope_en_base() {

		Integer[] les_id_station_mesure = new Integer[lesStationsMesure.size()];
		for (int i = 0; i < this.lesStationsMesure.size(); i++) {
			StationMesure stm1 = (StationMesure) lesStationsMesure.get(i);
			les_id_station_mesure[i] = stm1.getIdentifiant();
		}
		Set<Integer> s_les_id_station_mesure = new HashSet<Integer>(Arrays.asList(les_id_station_mesure));

		Integer[] les_id_stm_condenvope = new Integer[les_condenvope_en_base.size()];
		for (int i = 0; i < this.les_condenvope_en_base.size(); i++) {
			les_id_stm_condenvope[i] = ((CondEnvOpe) les_condenvope_en_base.get(i)).getStationmesure().getIdentifiant();
		}
		Set<Integer> s_les_id_stm_condenvope = new HashSet<Integer>(Arrays.asList(les_id_stm_condenvope));

		// renvoi l'intersection des deux vecteurs
		s_les_id_station_mesure.retainAll(s_les_id_stm_condenvope);
		// creation d'un nouveau DefaultListModel et remplissage � partir des
		// StationMesure

		SousListeCondEnvOpe _listecondenvope = new SousListeCondEnvOpe(this.masqueope);
		for (int i = 0; i < this.lesStationsMesure.size(); i++) {
			StationMesure stm = (StationMesure) lesStationsMesure.get(i);
			if (s_les_id_station_mesure.contains(stm.getIdentifiant())) {
				String cle_condenv = this.masque.getCode() + "_" + stm.getIdentifiant();
				CondEnvOpe condenvope = new CondEnvOpe(this.masqueope, stm);
				_listecondenvope.put(cle_condenv, condenvope);
			}
		}

		return (_listecondenvope);
	}

	// /**
	// * Chargement des �l�ments d'une marque
	// */
	// private void chargemasque() {
	// ListeMasque listemasque=new ListeMasqueOpe();
	//
	// try {
	// listemasqueope.chargeFiltre(this.masqueope.getMasque().getCode());
	// this.masqueope=(MasqueOpe) listemasqueope.get(masqueope);
	//
	// } catch (Exception e) {
	// this.resultat.setText(e.getMessage());
	// }
	// }
	/**
	 * M�thode de la validation / �criture du masque ope
	 */
	private void V_MasqueOP() {
		String result = null;
		// r�cup�ration des �l�ments du masque concernant l'affichage

		// r�cup�ration des �l�ments de la liste affichage
		// mao_affichage boolean[],
		// 1 Station (defaut 0)
		// 2 Ouvrage (defaut 0)
		// 3 DF (defaut 0)
		// 4 DC (defaut 0)
		// 5 heuredebut (defaut 1)
		// 6 heurefin (defaut 1)
		// 7 operateur (defaut 0)
		// 8 organisme (defaut 0)
		// 9 heurerearmement (defaut 0)
		// -----------------------------------------------------
		// les �lements logiques
		// --------------------------------------------------
		// 10 is_debut_auto (defaut 0) (oui non)
		// 11 is_fin_auto (defaut 0)(valeur oui non dans le combo)
		// 12 is_rearmement (defaut 0)(La date de r�armement sert � renseigner
		// le d�but de l'op�ration suivante si debutauto)
		// 13 is_rearmementDF (defaut 0) la valeur sert aussi � renseigner le DF
		// par seulement le DC
		// -----------------------------------------------------
		// les �lements qui n'ont pas de valeur pas d�faut
		// --------------------------------------------------
		// 14 datedebut (defaut 1)
		// 15 datefin (defaut 1)
		// 16 commentaires (defaut 0)

		try {

			affichage[0] = this.checkBox_Station.isSelected();
			affichage[1] = this.checkBox_Ouvrage.isSelected();
			affichage[2] = this.checkBox_DF.isSelected();
			affichage[3] = this.checkBox_DC.isSelected();
			affichage[4] = this.checkBox_Heuredebut.isSelected();
			affichage[5] = this.checkBox_Heurefin.isSelected();
			affichage[6] = this.checkBox_Operateur.isSelected();
			affichage[7] = this.checkBox_Organismeop.isSelected();
			affichage[8] = this.checkBox_Heure_rearmement.isSelected();
			debutauto = (String) this.combodebutauto.getSelectedItem();
			affichage[9] = debutauto.equals("Oui");
			finauto = (String) this.combofinauto.getSelectedItem();
			affichage[10] = finauto.equals("Oui");
			rearmement = (String) this.combo_rearmement.getSelectedItem();
			affichage[11] = rearmement.equals("Oui");
			affichage[12] = this.checkBox_Rearm_renseigne_df.isSelected();
			affichage[13] = this.checkBox_Datedebut.isSelected();
			affichage[14] = this.checkBox_Datefin.isSelected();
			affichage[15] = this.checkBox_Commentaires.isSelected();
			// mise � jour des valeurs du masqueope avec les valeurs bool�ennes
			// collect�es
			this.masqueope.setAffichage(affichage);

		} catch (Exception e) {
			result = e.toString();
			this.resultat.setText(e.getMessage());
		}
		// mise � jour du vecteur affichage dans le masqueope

		// r�cup�ration des valeurs par d�faut
		// mao_valeurdefaut String[]
		// 1 Station
		// 2 Ouvrage
		// 3 DF
		// 4 DC
		// 5 heuredebut
		// 6 heurefin
		// 7 operateur
		// 8 organisme
		// 9 heurerearmement

		try {
			Station station = (Station) this.comboStation.getSelectedItem();
			valeurdefaut[0] = station.getCode();
			Ouvrage ouvrage = (Ouvrage) this.comboOuvrage.getSelectedItem();
			if (this.comboOuvrage.isEnabled()) {
				valeurdefaut[1] = ouvrage.getCode();
			} else {
				valeurdefaut[1] = null;
			}
			DF df = (DF) this.comboDF.getSelectedItem();
			if (this.comboDF.isEnabled()) {
				valeurdefaut[2] = df.getCode();
			} else {
				valeurdefaut[2] = null;
			}
			DC dc = (DC) this.comboDC.getSelectedItem();
			if (this.comboDC.isEnabled()) {
				valeurdefaut[3] = dc.getCode();
			} else {
				valeurdefaut[3] = null;
			}
			// conversion de l'heure de d�but en heure minute seconde
			SimpleDateFormat formatheuredebut = heuredebut.getFormat();
			String formattedheuredebut = formatheuredebut.format(timeSpinnerheuredebut.getValue());
			// sdf.setTimeZone(TimeZone.getDefault());
			// String formattedheuredebut = sdf.format(heuredebut);
			// si le d�but est auto il n'y a pas besoin d'une valeur par d�faut
			if (debutauto.equals("Non")) {
				valeurdefaut[4] = formattedheuredebut;
			} else {
				valeurdefaut[4] = null;
			}
			// // heure de fin
			SimpleDateFormat formatheurefin = heurefin.getFormat();
			String formattedheurefin = formatheurefin.format(timeSpinnerheurefin.getValue());
			// String formattedheurefin = sdf.format(heurefin);
			// // si la fin est auto il n'y a pas besoin d'une valeur par d�faut
			if (finauto.equals("Non")) {
				valeurdefaut[5] = formattedheurefin;
			} else {
				valeurdefaut[5] = null;
			}

			String operateur = (String) this.textField_operateur.getText();

			valeurdefaut[6] = operateur;
			String organisme = (String) this.textField_organisme.getText();
			valeurdefaut[7] = organisme;
			//
			SimpleDateFormat formatheurerearmement = heure_rearmement.getFormat();
			String formattedheurerearmement = formatheurerearmement.format(timeSpinnerheure_rearmement.getValue());
			// // on ne renseigne le r�armement que sur les pi�ges
			if (rearmement.equals("Oui")) {
				valeurdefaut[8] = formattedheurerearmement;
			} else {
				valeurdefaut[8] = null;
			}
		} catch (Exception e) {
			result = e.toString();
			this.resultat.setText(e.getMessage());
		}

		// mise � jour des valeurs du masqueope avec les valeurs par d�faut du
		// masque op�ration
		this.masqueope.setValeurDefaut(valeurdefaut);

		// les listes ne sont initialis�es qu'avec le code du masqueOpe
		// et de la station de mesure. Il faut attendre l'interface utilisateur
		// EditerCondEnvOpe
		// pour que les valeurs vrai/faux soient mises � jour.

		listecondenvope = this.getLesStationsMesureSelectionnees();
		String message = new String();
		if (result == null) {

			try {
				boolean res = masqueope.verifAttributs();
			} catch (Exception e) {
				result = e.getMessage();
				this.resultat.setText(e.getMessage());
			}
			if (result == null) {
				if (this.is_masqueope_existing) {
					try {
						masqueope.majObjet();
						message = Message.M1018;
					} catch (Exception e) {
						result = e.getMessage();
						this.resultat.setText(e.getMessage());
					}
				} else {

					try {
						masqueope.insertObjet();
						message = Message.A1018;
					} catch (Exception e) {
						result = e.getMessage();
						this.resultat.setText(e.getMessage());
					}
				}
				if (result == null) {
					Component ihm = this.getParent();
					while ((ihm != null) && !(ihm instanceof IhmAppli)) {
						ihm = ihm.getParent();
					}
					// Passe a l'ecran d'ajout d'une operation
					((IhmAppli) ihm).changeContenu(
							new EditerCondEnvOpe(masqueope, listecondenvope, message, les_condenvope_en_base));
				}
			}
		}

	}

}
