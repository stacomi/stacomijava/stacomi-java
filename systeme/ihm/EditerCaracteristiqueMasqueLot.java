/**
 * 
 */
package systeme.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;

import commun.IhmAppli;
import commun.SousListeRefValeurParametre;
import migration.Caracteristique;
import migration.MethodeObtention;
import migration.referenciel.RefParametre;
import migration.referenciel.RefValeurParametre;
import systeme.CaracteristiqueMasqueLot;
import systeme.MasqueLot;
import systeme.referenciel.SousListeCaracteristiqueMasqueLot;

/**
 * @author cedric.briand
 *
 */
public class EditerCaracteristiqueMasqueLot extends JPanel {

	// Fields
	private final static Logger logger = Logger.getLogger(EditerCaracteristiqueMasqueLot.class.getName());
	private MasqueLot masquelot;
	private SousListeCaracteristiqueMasqueLot carmasquelot;
	private SousListeCaracteristiqueMasqueLot les_carmasquelot_en_base;
	private JLabel resultat = new JLabel();
	private JPanel top = new JPanel();
	private JPanel center = new JPanel();
	private JPanel bottom = new JPanel();
	private JLabel titre = new JLabel();
	private JButton button_OK = new JButton("OK");
	private JButton button_close = new JButton("Annuler");
	private static final Insets WEST_INSETS = new Insets(5, 0, 5, 5);
	private static final Insets EAST_INSETS = new Insets(5, 5, 5, 5);
	private String message;
	private ArrayList<JCheckBox> listofcheckboxvaleur = new ArrayList<JCheckBox>();
	private ArrayList<JCheckBox> listofcheckboxcommentaire = new ArrayList<JCheckBox>();
	private ArrayList<JCheckBox> listofcheckboxprecision = new ArrayList<JCheckBox>();
	private ArrayList<JCheckBox> listofcheckboxmethodeobtention = new ArrayList<JCheckBox>();
	// private ArrayList<JFormattedTextField> listofvaleur = new
	// ArrayList<JFormattedTextField>();
	private ArrayList<JFormattedTextField> listofprecision = new ArrayList<JFormattedTextField>();
	private GridBagLayout gbl_center = new GridBagLayout();
	private GridBagConstraints gbc = new GridBagConstraints();
	private boolean[] vect_estqualitatif;
	private SousListeRefValeurParametre[] vect_lesvaleurparametre;
	// liste pour stocker les composants correspondant aux param�tres
	// qualitatifs ou quantitatifs
	// JTextFieldDataType ou JComboBox
	private ArrayList<JComponent> listofcomponent = new ArrayList<JComponent>();
	private String[] methodeobtention = MethodeObtention.getMethodesCaracteristiques();
	private ArrayList<JComboBox<String>> listofcombomethodesobtention = new ArrayList<JComboBox<String>>();

	/**
	 * Constructeur
	 */
	public EditerCaracteristiqueMasqueLot(MasqueLot _masquelot, SousListeCaracteristiqueMasqueLot _carmasquelot,
			String _message, SousListeCaracteristiqueMasqueLot les_carmasquelot_en_base) {
		// lors de la classe pr�c�dente, les caract�ristiques du masqueLot ont
		// �t� initialis�es
		// je les r�cup�re ici.
		this.masquelot = _masquelot; // pass� par AMSMasqueLot.java
		this.carmasquelot = _carmasquelot; // pass� par AMSMasqueLot.java
		this.les_carmasquelot_en_base = les_carmasquelot_en_base; // pass� par
		// AMSMasqueLot.java
		this.message = _message;
		this.resultat.setText(message);
		this.initComponents();

	}

	private void initComponents() {

		setPreferredSize(new Dimension(800, 600));
		setBackground(new Color(240, 240, 240));
		setLayout(new BorderLayout(0, 0));
		// ===============================TOP=====================================
		JPanel top = new JPanel();
		add(top, BorderLayout.NORTH);
		top.setLayout(new BorderLayout(0, 0));

		JLabel titre = new JLabel();
		titre.setText("Edition des caract�ristiques de lot du masque Lot :" + masquelot.getMasque().getCode());
		titre.setOpaque(true);
		titre.setHorizontalAlignment(SwingConstants.CENTER);
		titre.setForeground(Color.WHITE);
		titre.setFont(new Font("Dialog", Font.BOLD, 14));
		titre.setBackground(new Color(0, 51, 153));
		top.add(titre, BorderLayout.NORTH);
		resultat.setOpaque(true);
		resultat.setHorizontalAlignment(SwingConstants.CENTER);
		resultat.setForeground(Color.RED);
		resultat.setBackground(Color.WHITE);
		top.add(resultat, BorderLayout.SOUTH);
		// ===============================Center=====================================
		JPanel center = new JPanel();
		center.setPreferredSize(new Dimension(600, 500));
		center.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		add(center, BorderLayout.CENTER);
		center.setLayout(gbl_center);

		gbc.anchor = GridBagConstraints.CENTER;
		// gbc.fill = GridBagConstraints.BOTH;

		if (carmasquelot.size() == 0) {
			JLabel label = new JLabel("Pas de caract�ristiques de lot s�lectionn�es pour ce masque, cliquez OK");
			gbc.gridx = 0;
			gbc.gridy = 0;
			// addComponent(label, 0, 0, 1, 1, inset);
			center.add(label, gbc);

		} else {
			// initiatilisation des valeurs des param�tres, qualitatifs ou
			// quantitatifs
			vect_estqualitatif = new boolean[carmasquelot.size()];
			vect_lesvaleurparametre = new SousListeRefValeurParametre[carmasquelot.size()];
			logger.log(Level.INFO, "traitement de " + carmasquelot.size() + " param�tres");
			DecimalFormat decimalformatter = new DecimalFormat("###.###");
			DecimalFormatSymbols unpoint = new DecimalFormatSymbols();
			unpoint.setDecimalSeparator('.');
			decimalformatter.setDecimalFormatSymbols(unpoint);
			// NumberFormat numberFieldFormatter =
			// NumberFormat.getNumberInstance();
			for (int i = 0; i < carmasquelot.size(); i++) {

				CaracteristiqueMasqueLot car = (CaracteristiqueMasqueLot) carmasquelot.get(i);
				RefParametre param = car.getCaracteristiquedefaut().getParametre();
				String codepar = param.getCode();

				try {
					boolean estqualitatif = RefParametre.estQualitatif(codepar);
					vect_estqualitatif[i] = estqualitatif;
				} catch (Exception e) {
					this.resultat.setText(e.getMessage());
					logger.log(Level.SEVERE, " EditerCaracteristiqueLot. teste si la valeur est qualitative  ", e);
				}
				String unite;
				if (!vect_estqualitatif[i]) {
					unite = " (" + param.getUnite() + ")";
				} else
					unite = "";
				// ligne de titres///////////////////////////////////////

				JLabel titre_nom = new JLabel("Nom");
				gbc = createGbc(0, 0);
				center.add(titre_nom, gbc);

				JLabel titre_choix = new JLabel("Valeur d�faut");
				gbc = createGbc(2, 0);
				center.add(titre_choix, gbc);

				JLabel titre_precision = new JLabel("Pr�cision");
				gbc = createGbc(3, 0);
				gbc.gridwidth = 2;
				center.add(titre_precision, gbc);

				JLabel titre_commentaire = new JLabel("Commentaire");
				gbc = createGbc(5, 0);
				center.add(titre_commentaire, gbc);

				JLabel titre_methode_obtention = new JLabel("M�thode obtention");
				gbc = createGbc(6, 0);
				gbc.gridwidth = 2;
				center.add(titre_methode_obtention, gbc);

				////////////////////////////////////////////////////////////////////////////

				JLabel label = new JLabel(
						"Caract�ristique : " + car.getCaracteristiquedefaut().getParametre().getLibelle() + unite);
				gbc = createGbc(0, i + 1);
				center.add(label, gbc);
				// addComponent(label, i, 0, 1, 1, inset);

				JCheckBox checkBoxvaleur = new JCheckBox("");
				checkBoxvaleur.setToolTipText("Cochez la case si vous voulez que la valeur soit \u00E9ditable");
				gbc = createGbc(1, i + 1);
				center.add(checkBoxvaleur, gbc);
				listofcheckboxvaleur.add(checkBoxvaleur);

				// la gbc 2 sera remplie plus loin soit avec un param�tre qual

				JCheckBox checkBoxcommentaire = new JCheckBox();
				checkBoxcommentaire
						.setToolTipText("Cochez la case si vous voulez que le commentaire soit \u00E9ditable");
				gbc = createGbc(5, i + 1);
				center.add(checkBoxcommentaire, gbc);
				listofcheckboxcommentaire.add(checkBoxcommentaire);

				// methode d'obtention
				JCheckBox checkBoxmethodeobtention = new JCheckBox();
				checkBoxvaleur
						.setToolTipText("Cochez la case si vous voulez que la m�thode d'obtention soit \u00E9ditable");
				gbc = createGbc(6, i + 1);
				center.add(checkBoxmethodeobtention, gbc);
				listofcheckboxmethodeobtention.add(checkBoxmethodeobtention);

				DefaultComboBoxModel<String> combomodelmethodeobtention = new DefaultComboBoxModel<String>();
				for (String m : methodeobtention) {
					combomodelmethodeobtention.addElement(m);
				}
				JComboBox<String> combomethodeobtention = new JComboBox<String>(combomodelmethodeobtention);
				gbc = createGbc(7, i + 1);
				center.add(combomethodeobtention, gbc);
				listofcombomethodesobtention.add(combomethodeobtention);

				if (vect_estqualitatif[i]) {

					// Cas ou la station de mesure correspond � un caract�re
					// qualitatif
					JComboBox<RefValeurParametre> combo = new JComboBox<RefValeurParametre>();
					SousListeRefValeurParametre lesValeurQualitatives = new SousListeRefValeurParametre(param);
					try {
						lesValeurQualitatives.chargeFiltreDetails();
					} catch (Exception e) {
						this.resultat.setText(e.getMessage());
						logger.log(Level.SEVERE, " EditerCaracteristiqueMasqueLot chargevaleurqualitative  ", e);
					}
					vect_lesvaleurparametre[i] = lesValeurQualitatives;
					DefaultComboBoxModel<RefValeurParametre> combomodelparm = new DefaultComboBoxModel<RefValeurParametre>();
					for (int j = 0; j < lesValeurQualitatives.size(); j++) {
						RefValeurParametre refvaleurparm = (RefValeurParametre) lesValeurQualitatives.get(j);
						combomodelparm.addElement(refvaleurparm);
					}
					combo.setModel(combomodelparm);
					combo.addMouseListener(new java.awt.event.MouseAdapter() {
						public void mouseClicked(java.awt.event.MouseEvent e) {
							if (e.getClickCount() == 2 && !e.isConsumed()) {
								e.consume();
								combo.setSelectedIndex(-1);
							}
						}
					});
					combo.setToolTipText("Double clic pour vider");

					gbc = createGbc(2, i + 1);
					center.add(combo, gbc);
					listofcomponent.add(combo);
					listofprecision.add(null);
					listofcheckboxprecision.add(null);
				} else {
					// Cas ou la station de mesure est un caract�re
					// quantitatif
					vect_lesvaleurparametre[i] = null;
					// on veut garder la structure et acc�der
					// aux �l�ments avec i
					JFormattedTextField textfield_valeur = new JFormattedTextField(decimalformatter);
					gbc = createGbc(2, i + 1);
					center.add(textfield_valeur, gbc);
					listofcomponent.add(textfield_valeur);
					textfield_valeur.setToolTipText("La valeur 0 sera ignor�e");
					JCheckBox checkBoxprecision = new JCheckBox("");
					checkBoxprecision
							.setToolTipText("Cochez la case si vous voulez que la precision soit \u00E9ditable");
					gbc = createGbc(3, i + 1);
					center.add(checkBoxprecision, gbc);
					listofcheckboxprecision.add(checkBoxprecision);
					JFormattedTextField textfield_precision = new JFormattedTextField(decimalformatter);
					// textfield_precision.setSize(new Dimension(40, 50));
					textfield_precision.setColumns(6);
					textfield_valeur.setToolTipText("La valeur 0 sera ignor�e");
					gbc = createGbc(4, i + 1);
					center.add(textfield_precision, gbc);
					listofprecision.add(textfield_precision);
				} // else caract quant

				////////////////////////////////
				// Mise � jour des donn�es � partir des �l�ments en base
				// on est toujours la boucle d'affichage des caracteristiques
				//////////////////////////////// pour la caract�ristique i
				//////////////////////////////
				for (int j = 0; j < les_carmasquelot_en_base.size(); j++) {
					CaracteristiqueMasqueLot cmlb = (CaracteristiqueMasqueLot) les_carmasquelot_en_base.get(j);
					Caracteristique carb = cmlb.getCaracteristiquedefaut();
					// on boucle sur les valeurs en base et on va chercher celle
					// dont le code correspond au code de l'�l�ment
					if (carb.getParametre().getCode().equals(codepar)) {
						listofcheckboxvaleur.get(i).setSelected(cmlb.isAffichage_valeur());
						listofcheckboxcommentaire.get(i).setSelected(cmlb.isAffichage_commentaire());
						listofcheckboxmethodeobtention.get(i).setSelected(cmlb.isAffichage_methodeobtention());
						String methodeobtentionenbase = carb.getMethodeObtention();
						for (int k = 0; k < methodeobtention.length; k++) {
							if (methodeobtention[k].equals(methodeobtentionenbase))
								combomethodeobtention.setSelectedIndex(k);
						}
						if (vect_estqualitatif[i]) {// param�tre qualitatif =
							// mise � jour de la valeur
							// du combo
							// r�cup�ration du param�tre qualitatif s�lectionn�
							RefValeurParametre refvaleurparmenbase = carb.getValeurParametreQualitatif();
							// on cherche au sein des valeurs du combo celle qui
							// matche la valeur du param�tre qualitatif en base
							for (int k = 0; k < vect_lesvaleurparametre[i].size(); k++) {
								RefValeurParametre refvaleurparm = (RefValeurParametre) vect_lesvaleurparametre[i]
										.get(k);
								if (refvaleurparmenbase.getCode().equals(refvaleurparm.getCode())) {
									JComboBox<RefValeurParametre> combo = (JComboBox<RefValeurParametre>) listofcomponent
											.get(i);
									combo.setSelectedIndex(k);
								} // end if
							} // boucle

						} else { // parametre quantitatif
							Float refvaleurparmenbase = carb.getValeurParametreQuantitatif();
							JFormattedTextField textfield_valeur = (JFormattedTextField) listofcomponent.get(i);
							textfield_valeur.setValue(refvaleurparmenbase);
							listofcheckboxprecision.get(i).setSelected(cmlb.isAffichage_precision());
							Float refvaleurprecisionenbase = carb.getPrecision();
							JFormattedTextField textfield_precision = (JFormattedTextField) listofprecision.get(i);
							textfield_precision.setValue(refvaleurprecisionenbase);
						}
					} // end if
				} // boucle sur les composants
					////////////////////////////////
					// Selection des �l�ments en base
					//////////////////////////////

			}
		} // else (il y a des valeurs masquelot caract
			// ===============================Bottom======================================
		JPanel bottom = new JPanel();
		bottom.setBackground(UIManager.getColor("ToolBar.background"));
		add(bottom, BorderLayout.SOUTH);
		bottom.add(button_OK);
		bottom.add(button_close);
		button_OK.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				V_CaracteristiqueMasqueLot();
			}

		});
		button_close.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				chargemasque();
			}

		});
	}// end initcomponent

	private GridBagConstraints createGbc(int x, int y) {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;

		gbc.anchor = (x == 0) ? GridBagConstraints.WEST : GridBagConstraints.EAST;
		gbc.fill = (x == 0) ? GridBagConstraints.BOTH : GridBagConstraints.HORIZONTAL;

		gbc.insets = (x == 0) ? WEST_INSETS : EAST_INSETS;
		// gbc.weightx = (x == 0) ? 0.5 : 1.0;
		// gbc.weighty = 1.0;
		return (gbc);
	}

	private void V_CaracteristiqueMasqueLot() {
		// suppression initale de tous les objets en base
		for (int j = 0; j < les_carmasquelot_en_base.size(); j++) {
			CaracteristiqueMasqueLot cml = (CaracteristiqueMasqueLot) les_carmasquelot_en_base.get(j);
			try {
				cml.effaceObjet();
			} catch (Exception e) {
				this.resultat.setText(e.getMessage());
			}
		}
		int i = 0;
		for (Object SousListeCaracteristiqueMasqueLot : carmasquelot) {

			// System.out.println(ccc.isSelected());

			CaracteristiqueMasqueLot cml = (CaracteristiqueMasqueLot) carmasquelot.get(i);
			Caracteristique car = cml.getCaracteristiquedefaut();
			car.setMethodeObtention(listofcombomethodesobtention.get(i).getSelectedItem().toString());
			cml.setAffichage_valeur(listofcheckboxvaleur.get(i).isSelected());
			cml.setAffichage_commentaire(listofcheckboxcommentaire.get(i).isSelected());
			cml.setAffichage_methodeobtention(listofcheckboxmethodeobtention.get(i).isSelected());

			if (vect_estqualitatif[i]) {
				// dans ce cas le component est un combo
				// dans lequel est stock� la valeur du param�tre qualitatif
				// on prend la caracterisique et on la met � jour.
				JComboBox<RefValeurParametre> combo = (JComboBox<RefValeurParametre>) listofcomponent.get(i);
				RefValeurParametre refvaleurpar = (RefValeurParametre) combo.getSelectedItem();
				car.setValeurParametreQualitatif(refvaleurpar);
				cml.setCaracteristiquedefaut(car);
			} else {
				// sinon on va chercher la valeur du formattedtextfield
				cml.setAffichage_precision(listofcheckboxprecision.get(i).isSelected());
				JFormattedTextField textfield_valeur = (JFormattedTextField) listofcomponent.get(i);
				Number valeurpar = (Number) textfield_valeur.getValue();
				// Float valeurparfloat = null;
				// if (valeurpar instanceof Double)
				// valeurparfloat = ((Double) valeurpar).floatValue();
				// if (valeurpar instanceof Long)
				// valeurparfloat = ((Long) valeurpar).floatValue();
				if (valeurpar != null) {
					if (valeurpar.floatValue() != 0) {
						car.setValeurParametreQuantitatif(valeurpar.floatValue());
					}
				}
				Number precision = (Number) listofprecision.get(i).getValue();
				if (precision != null) {
					if (precision.floatValue() != 0) {
						car.setPrecision(precision.floatValue());
					}
				}
				cml.setCaracteristiquedefaut(car);
			}

			try {
				boolean res = cml.verifAttributs();
			} catch (Exception e) {
				message = message + "\n" + e.getMessage();
				this.resultat.setText(e.getMessage());
			}

			try {
				cml.insertObjet();
			} catch (Exception e) {
				this.resultat.setText(e.getMessage());
				message = message + "\n" + e.getMessage();
			}
			i = i + 1;
		}
		message = message + " " + i + " Caract�ristiques enregistr�es ";
		chargemasque();

	}

	private void chargemasque() {
		Component ihm = this.getParent();
		while ((ihm != null) && !(ihm instanceof IhmAppli)) {
			ihm = ihm.getParent();
		}

		((IhmAppli) ihm).changeContenu(new AMSMasque(message));
	}

}
