/**
 * 
 */
package systeme.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;

import commun.IhmAppli;
import systeme.CondEnvOpe;
import systeme.MasqueOpe;
import systeme.referenciel.ListeCondEnvOpe;
import systeme.referenciel.SousListeCondEnvOpe;

/**
 * @author cedric.briand
 *
 */
public class EditerCondEnvOpe extends JPanel {

	/**
	 * 
	 */
	public EditerCondEnvOpe(MasqueOpe _masqueop, ListeCondEnvOpe _listecondenvope, String _message,
			SousListeCondEnvOpe les_condenvope_en_base) {
		// lors de la classe pr�c�dente, les caract�ristiques du masqueOpe ont
		// �t� initialis�es
		// je les r�cup�re ici.
		// pour lancer EditerCondEnv, on passe par AMSMasqueOpe et l'�dition des
		// d�tails se fait lorsque
		// on envoie le bouton ajouter ou modifier.
		this.masqueope = _masqueop; // pass� par AMSMasqueOpe.java
		this.listecondenvope = _listecondenvope; // pass� par AMSMasqueOpe.java
		this.lescondenvopeenbase = les_condenvope_en_base; // pass� par
		// AMSMasqueOpe.java
		this.message = _message;
		this.resultat.setText(message);
		this.initComponents();

	}

	// Fields
	private MasqueOpe masqueope;
	private ListeCondEnvOpe listecondenvope;
	private SousListeCondEnvOpe lescondenvopeenbase;
	private JLabel resultat = new JLabel();
	private JPanel top = new JPanel();
	private JPanel center = new JPanel();
	private JPanel bottom = new JPanel();
	private JLabel titre = new JLabel();
	private JButton button_OK = new JButton("OK");
	private JButton button_close = new JButton("Annuler");
	private static final Insets WEST_INSETS = new Insets(1, 0, 1, 1);
	private static final Insets EAST_INSETS = new Insets(1, 1, 1, 0);
	private String message;
	private ArrayList<JCheckBox> listofcheckbox = new ArrayList<JCheckBox>();
	private GridBagLayout gbl_center = new GridBagLayout();
	private GridBagConstraints gbc = new GridBagConstraints();

	private void initComponents() {

		setPreferredSize(new Dimension(800, 600));
		setBackground(new Color(240, 240, 240));
		setLayout(new BorderLayout(0, 0));
		// ===============================TOP=====================================
		JPanel top = new JPanel();
		add(top, BorderLayout.NORTH);
		top.setLayout(new BorderLayout(0, 0));

		JLabel titre = new JLabel();
		titre.setText("Edition des caract�ristiques environnementales du masque Op�ration :"
				+ masqueope.getMasque().getCode());
		titre.setOpaque(true);
		titre.setHorizontalAlignment(SwingConstants.CENTER);
		titre.setForeground(Color.WHITE);
		titre.setFont(new Font("Dialog", Font.BOLD, 14));
		titre.setBackground(new Color(0, 51, 153));
		top.add(titre, BorderLayout.NORTH);
		resultat.setOpaque(true);
		resultat.setHorizontalAlignment(SwingConstants.CENTER);
		resultat.setForeground(Color.RED);
		resultat.setBackground(Color.WHITE);
		top.add(resultat, BorderLayout.SOUTH);
		// ===============================Center=====================================
		JPanel center = new JPanel();
		center.setPreferredSize(new Dimension(600, 500));
		center.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		add(center, BorderLayout.CENTER);
		center.setLayout(gbl_center);

		gbc.anchor = GridBagConstraints.CENTER;
		// gbc.fill = GridBagConstraints.BOTH;
		if (listecondenvope.size() == 0) {
			JLabel label = new JLabel(
					"Comme vous n'avez pas s�lectionn� de station de mesure, cette page n'affiche rien (c'est normal), cliquez OK");
			gbc.gridx = 0;
			gbc.gridy = 0;
			// addComponent(label, 0, 0, 1, 1, inset);
			center.add(label, gbc);

		}

		for (int i = 0; i < listecondenvope.size(); i++) {

			CondEnvOpe condenvope = (CondEnvOpe) listecondenvope.get(i);
			JLabel label = new JLabel("Station : " + condenvope.getStationmesure().getLibelle());
			gbc = createGbc(0, i);
			center.add(label, gbc);
			// addComponent(label, i, 0, 1, 1, inset);

			JCheckBox checkBox = new JCheckBox("");
			checkBox.setToolTipText("Cochez la case si vous voulez que la valeur soit \u00E9ditable");
			gbc = createGbc(1, i);

			center.add(checkBox, gbc);
			// addComponent(checkBox, i, 1, 1, 1, inset);
			// a ce stade on ne sait pas si les condenvope s�lectionn�es par
			// l'utilisateur correspondent aux valeurs en base
			// si c'est le cas on va les chercher.
			// mais l'�galit� doit �tre recherch�e sur l'identifiant, pas
			// l'objet qui est diff�rent.

			for (int j = 0; j < lescondenvopeenbase.size(); j++) {
				CondEnvOpe condenvopeenbase = (CondEnvOpe) lescondenvopeenbase.get(j);
				if (condenvopeenbase.getStationmesure().getIdentifiant() == condenvope.getStationmesure()
						.getIdentifiant()) {
					if (condenvopeenbase.getAffichage() != null) {
						checkBox.setSelected(condenvopeenbase.getAffichage());
					}
				}

			}
			// je stocke la liste (compil�e at runtime) dans une liste qui est
			// dans ma classe !
			listofcheckbox.add(checkBox);

		}
		// ===============================Bottom======================================
		JPanel bottom = new JPanel();
		bottom.setBackground(UIManager.getColor("ToolBar.background"));
		add(bottom, BorderLayout.SOUTH);
		bottom.add(button_OK);
		bottom.add(button_close);
		button_OK.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				V_CondEnvOpe();
			}

		});
		button_close.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				chargemasque();
			}

		});
	}

	// button_OK.addMouseListener(new MouseListener() {
	//
	// @Override
	// public void mouseReleased(MouseEvent e) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// public void mousePressed(MouseEvent e) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// public void mouseExited(MouseEvent e) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// public void mouseEntered(MouseEvent e) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// public void mouseClicked(MouseEvent e) {
	// V_CondEnvOpe();
	// }
	// });
	//
	//
	private GridBagConstraints createGbc(int x, int y) {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;

		gbc.anchor = (x == 0) ? GridBagConstraints.WEST : GridBagConstraints.EAST;
		gbc.fill = (x == 0) ? GridBagConstraints.BOTH : GridBagConstraints.HORIZONTAL;

		gbc.insets = (x == 0) ? WEST_INSETS : EAST_INSETS;
		// gbc.weightx = (x == 0) ? 0.5 : 1.0;
		// gbc.weighty = 1.0;
		return (gbc);
	}

	private void V_CondEnvOpe() {
		// suppression initale de tous les objets en base
		for (int j = 0; j < lescondenvopeenbase.size(); j++) {
			CondEnvOpe ceo = (CondEnvOpe) lescondenvopeenbase.get(j);
			try {
				ceo.effaceObjet();
			} catch (Exception e) {
				this.resultat.setText(e.getMessage());
			}
		}
		int i = 0;
		for (JCheckBox ccc : listofcheckbox) {

			// System.out.println(ccc.isSelected());

			CondEnvOpe condenvope = (CondEnvOpe) listecondenvope.get(i);
			condenvope.setAffichage(ccc.isSelected());

			try {
				boolean res = condenvope.verifAttributs();
			} catch (Exception e) {
				message = message + "\n" + e.getMessage();
				this.resultat.setText(e.getMessage());
			}

			try {
				condenvope.insertObjet();
			} catch (Exception e) {
				this.resultat.setText(e.getMessage());
				message = message + "\n" + e.getMessage();
			}
			i = i + 1;
		}

		chargemasque();

	}

	private void chargemasque() {
		Component ihm = this.getParent();
		while ((ihm != null) && !(ihm instanceof IhmAppli)) {
			ihm = ihm.getParent();
		}

		((IhmAppli) ihm).changeContenu(new AMSMasque(message));
	}

}
