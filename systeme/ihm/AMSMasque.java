package systeme.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import commun.Accueil;
import commun.Erreur;
import commun.IhmAppli;
import commun.Lanceur;
import commun.Message;
import systeme.Masque;
import systeme.referenciel.ListeMasque;
import systeme.referenciel.TypeMasque;

public class AMSMasque extends JPanel implements KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 15L;
	private JTextField textFieldNomMasque = new JTextField();
	private JTextArea textAreaDescription = new JTextArea();
	private JList jListMasque = new JList();
	private TypeMasque lestypes;
	private ListeMasque listeMasques;
	private javax.swing.JLabel resultat = new JLabel();
	private JComboBox comboBoxTypeMasqueType = new JComboBox();
	private JButton buttonEditMasque = new JButton("Editer");

	/**
	 * Create the panel.
	 */
	public AMSMasque() {
		this.initComponents();
		this.chargeListeMasques();
	}

	/**
	 * Constructeur pour passer le message avec masque ajout� !
	 * 
	 * @param message
	 */
	public AMSMasque(String message) {
		this.initComponents();
		this.chargeListeMasques();
		this.resultat.setText(message);
	}

	private void initComponents() {
		setPreferredSize(new Dimension(800, 600));
		setBackground(new Color(240, 240, 240));
		setLayout(new BorderLayout(0, 0));

		JPanel top = new JPanel();
		add(top, BorderLayout.NORTH);
		top.setLayout(new BorderLayout(0, 0));

		JLabel titreAjouterUnMasque = new JLabel();
		titreAjouterUnMasque.setText("Ajouter / Modifier / Supprimer un masque");
		titreAjouterUnMasque.setOpaque(true);
		titreAjouterUnMasque.setHorizontalAlignment(SwingConstants.CENTER);
		titreAjouterUnMasque.setForeground(Color.WHITE);
		titreAjouterUnMasque.setFont(new Font("Dialog", Font.BOLD, 14));
		titreAjouterUnMasque.setBackground(new Color(0, 51, 153));
		top.add(titreAjouterUnMasque, BorderLayout.NORTH);

		resultat.setOpaque(true);
		resultat.setHorizontalAlignment(SwingConstants.CENTER);
		resultat.setForeground(Color.RED);
		resultat.setBackground(Color.WHITE);
		top.add(resultat, BorderLayout.SOUTH);
		// resultat.setText("sortie");

		JPanel center = new JPanel();
		center.setPreferredSize(new Dimension(600, 400));
		center.setSize(new Dimension(600, 400));
		add(center, BorderLayout.CENTER);
		GridBagLayout gbl_center = new GridBagLayout();
		gbl_center.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_center.columnWidths = new int[] { 0, 0 };
		gbl_center.columnWeights = new double[] { 0.0, 0.0 };
		gbl_center.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		center.setLayout(gbl_center);

		JLabel lblListeDesMasques = new JLabel("Liste des Masques");
		GridBagConstraints gbc_lblListeDesMasques = new GridBagConstraints();
		gbc_lblListeDesMasques.insets = new Insets(0, 0, 5, 5);
		gbc_lblListeDesMasques.gridx = 0;
		gbc_lblListeDesMasques.gridy = 0;
		center.add(lblListeDesMasques, gbc_lblListeDesMasques);

		// scrollpane autour de la liste des masques
		JScrollPane jscrollpane_listemasque = new JScrollPane();
		jscrollpane_listemasque.setPreferredSize(new Dimension(300, 100));
		jscrollpane_listemasque.setViewportView(this.jListMasque);

		GridBagConstraints gbc_jListMasque = new GridBagConstraints();
		gbc_jListMasque.anchor = GridBagConstraints.WEST;
		gbc_jListMasque.insets = new Insets(0, 0, 5, 0);
		gbc_jListMasque.gridx = 1;
		gbc_jListMasque.gridy = 0;
		center.add(jscrollpane_listemasque, gbc_jListMasque);

		JLabel LabelNomMasque = new JLabel("R\u00E9f\u00E9rence du Masque ");
		LabelNomMasque.setToolTipText("");
		GridBagConstraints gbc_LabelNomMasque = new GridBagConstraints();
		gbc_LabelNomMasque.insets = new Insets(0, 0, 5, 5);
		gbc_LabelNomMasque.gridx = 0;
		gbc_LabelNomMasque.gridy = 1;
		center.add(LabelNomMasque, gbc_LabelNomMasque);
		LabelNomMasque.setLabelFor(textFieldNomMasque);

		textFieldNomMasque.setToolTipText("Nom du masque (dix caract\u00E8res)");
		GridBagConstraints gbc_textFieldNomMasque = new GridBagConstraints();
		gbc_textFieldNomMasque.anchor = GridBagConstraints.WEST;
		gbc_textFieldNomMasque.insets = new Insets(2, 0, 5, 0);
		gbc_textFieldNomMasque.gridx = 1;
		gbc_textFieldNomMasque.gridy = 1;
		center.add(textFieldNomMasque, gbc_textFieldNomMasque);
		textFieldNomMasque.setColumns(10);

		/*
		 * JLabel LabelToucheRaccourci = new JLabel("Touche Raccourci");
		 * GridBagConstraints gbc_LabelToucheRaccourci = new
		 * GridBagConstraints(); gbc_LabelToucheRaccourci.insets = new Insets(0,
		 * 0, 5, 5); gbc_LabelToucheRaccourci.gridx = 0;
		 * gbc_LabelToucheRaccourci.gridy = 2; center.add(LabelToucheRaccourci,
		 * gbc_LabelToucheRaccourci);
		 * LabelToucheRaccourci.setLabelFor(this.textFieldToucheRaccourci);
		 * 
		 * GridBagConstraints gbc_textFieldToucheRaccourci = new
		 * GridBagConstraints(); gbc_textFieldToucheRaccourci.anchor =
		 * GridBagConstraints.WEST; gbc_textFieldToucheRaccourci.insets = new
		 * Insets(2, 0, 5, 0); gbc_textFieldToucheRaccourci.gridx = 1;
		 * gbc_textFieldToucheRaccourci.gridy = 2;
		 * center.add(textFieldToucheRaccourci, gbc_textFieldToucheRaccourci);
		 * textFieldToucheRaccourci.setColumns(10); // la methode Keylistener
		 * est n�cessaire pour aller r�cup�rer les // touches de raccourci //
		 * comme je n'ai pas d�sactiv� tab, je ne peux pas utiliser cette touche
		 * textFieldToucheRaccourci.addKeyListener(this);
		 * textFieldToucheRaccourci.setToolTipText(
		 * "Combinaison de touche, Ctrl Alt Maj et F1 a F12 cliquez pour effacer"
		 * );
		 */
		JLabel lblListeTypeMasque = new JLabel("Type de masque");
		GridBagConstraints gbc_lblListeTypeMasque = new GridBagConstraints();
		gbc_lblListeTypeMasque.insets = new Insets(0, 0, 5, 5);
		gbc_lblListeTypeMasque.gridx = 0;
		gbc_lblListeTypeMasque.gridy = 3;
		center.add(lblListeTypeMasque, gbc_lblListeTypeMasque);

		GridBagConstraints gbc_comboBoxTypeMasqueType = new GridBagConstraints();
		gbc_comboBoxTypeMasqueType.anchor = GridBagConstraints.WEST;
		gbc_comboBoxTypeMasqueType.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxTypeMasqueType.gridx = 1;
		gbc_comboBoxTypeMasqueType.gridy = 3;
		center.add(this.comboBoxTypeMasqueType, gbc_comboBoxTypeMasqueType);
		this.comboBoxTypeMasqueType.setModel(new DefaultComboBoxModel(Lanceur.getTypeMasque()));

		JLabel lblDescription = new JLabel("Description");
		GridBagConstraints gbc_lblDescription = new GridBagConstraints();
		gbc_lblDescription.insets = new Insets(0, 0, 5, 5);
		gbc_lblDescription.gridx = 0;
		gbc_lblDescription.gridy = 4;
		center.add(lblDescription, gbc_lblDescription);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setPreferredSize(new Dimension(300, 100));
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.anchor = GridBagConstraints.WEST;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 4;
		center.add(scrollPane, gbc_scrollPane);

		scrollPane.setViewportView(this.textAreaDescription);

		buttonEditMasque.setEnabled(false);
		GridBagConstraints gbc_buttonEditMasque = new GridBagConstraints();
		gbc_buttonEditMasque.gridx = 1;
		gbc_buttonEditMasque.gridy = 5;
		center.add(buttonEditMasque, gbc_buttonEditMasque);

		JPanel bottom = new JPanel();
		bottom.setBackground(UIManager.getColor("ToolBar.background"));
		add(bottom, BorderLayout.SOUTH);

		JButton button_A = new JButton();
		button_A.setText("Ajouter");
		bottom.add(button_A);

		JButton button_M = new JButton();
		button_M.setText("Modifier");
		bottom.add(button_M);

		JButton button_S = new JButton();
		button_S.setText("Supprimer");
		bottom.add(button_S);

		JButton button_annuler = new JButton();
		button_annuler.setText("Sortie/maj liste");
		bottom.add(button_annuler);

		this.jListMasque.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jListMasqueMouseClicked(e);
			}

			private void jListMasqueMouseClicked(MouseEvent e) {
				chargemasques();

			}
		});
		/*
		 * this.textFieldToucheRaccourci.addMouseListener(new MouseAdapter() {
		 * 
		 * @Override public void mouseClicked(MouseEvent e) {
		 * textFieldToucheRaccourciMouseClicked(e); }
		 * 
		 * private void textFieldToucheRaccourciMouseClicked(MouseEvent e) {
		 * nettoyeraccourci();
		 * 
		 * }
		 * 
		 * });
		 */
		button_A.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				A_Masque();
			}
		});
		button_M.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				M_Masque();
			}
		});
		button_S.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				S_Masque();
			}
		});
		button_annuler.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				quitte();
			}
		});
		buttonEditMasque.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				editer_Masque();
			}

		});

	}

	/**
	 * Chargement des masques
	 */
	private void chargeListeMasques() {

		this.listeMasques = new ListeMasque();
		// ChargeSansFiltre renvoit une exception
		// Surround with try catch
		try {
			listeMasques.chargeSansFiltreDetails();
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
		}

		// R�cup�ration de la listes de codes
		// Il faut la m�thode toString dans la classe qui compose la liste
		try {
			jListMasque.setModel(new DefaultComboBoxModel(listeMasques.toArray()));
		} catch (Exception e) {
			this.resultat.setText(e.getMessage());
		}

	}

	/**
	 * Insertion modification d'un masque
	 */
	private void A_Masque() {
		String result = null;
		String code = null;
		String description = null;
		String raccourci = null;
		String strtype = null;
		TypeMasque reftype = null;

		try {
			code = textFieldNomMasque.getText();
			description = textAreaDescription.getText();
			// raccourci = textFieldToucheRaccourci.getText();
			strtype = (String) comboBoxTypeMasqueType.getSelectedItem();

		} catch (Exception e) {
			result = e.toString();
			this.resultat.setText(e.getMessage());
		}

		if (result == null) {
			Masque masque = new Masque(code, description, raccourci, strtype);
			try {
				masque.verifAttributs();
			} catch (Exception e) {
				this.resultat.setText(e.getMessage());
			}
			try {
				masque.insertObjet();
			} catch (Exception e) {
				this.resultat.setText(e.getMessage());
			}
			this.resultat.setText(Message.A1017);
			this.chargeListeMasques();
			this.chargemasque(masque);
		}

	}

	/**
	 * Modification d'un masque
	 */
	private void M_Masque() {
		String result = null;
		String code = null;
		String description = null;
		String raccourci = null;
		String strtype = null;

		try {
			Masque oldmasque = (Masque) this.jListMasque.getSelectedValue();
			try {
				ListeMasque listemasque = new ListeMasque();
				listemasque.chargeFiltre(oldmasque.getCode());
				oldmasque = (Masque) listemasque.get(0);

			} catch (Exception e) {
				this.resultat.setText(e.getMessage());
			}

			code = textFieldNomMasque.getText();
			description = textAreaDescription.getText();
			// raccourci = textFieldToucheRaccourci.getText();
			strtype = (String) comboBoxTypeMasqueType.getSelectedItem();

			if (result == null) {
				Masque masque = new Masque(code, oldmasque.getIdentifiant(), description, raccourci, strtype);
				try {
					masque.verifAttributs();
					masque.majObjet();
				} catch (Exception e) {
					this.resultat.setText(e.getMessage());
				}
				this.resultat.setText(Message.M1017);
				this.chargeListeMasques();
				int index;
				for (int i = 0; i < listeMasques.size(); i++) {
					Masque mas = (Masque) listeMasques.elementAt(i);
					if (mas.getCode().equals(code)) {
						this.jListMasque.setSelectedIndex(i);
						break;
					}
				}

			}
		} catch (Exception e) {
			result = e.toString();
		}

	}

	/**
	 * Suppression d'un masque
	 */
	private void S_Masque() {
		String result = null;
		try {
			if (!jListMasque.isSelectionEmpty()) {
				Masque masque = (Masque) this.jListMasque.getSelectedValue();
				masque.effaceObjet();
				result = Message.S1015;
			} else
				result = Erreur.S28000;
		} catch (Exception e) {
			result = e.getMessage();
		} finally {
			this.resultat.setText(result);
			this.chargeListeMasques();
			nettoietout();
		}
	}

	/**
	 * Chargement du d�tails (notamment d'id) du masque charg� M�thode lanc�e
	 * avant d'afficher les d�tails de l'op�ration ou du lot
	 */
	private void editer_Masque() {
		Masque masque = (Masque) jListMasque.getSelectedValue(); // true pour
		// dire de
		// scroller
		// jusqu'au
		// niveau du
		// masque
		if (masque != null) {
			ListeMasque listemasque = new ListeMasque();
			if (masque.getType().equals("ope")) {

				try {
					listemasque.chargeFiltre(masque.getCode());
					masque = (Masque) listemasque.get(0);
				} catch (Exception e) {
					this.resultat.setText(e.getMessage());
				}
				affichemasqueope(masque);
			} else if (masque.getType().equals("lot")) {
				try {
					listemasque.chargeFiltre(masque.getCode());
					masque = (Masque) listemasque.get(0);
				} catch (Exception e) {
					this.resultat.setText(e.getMessage());
				}
				affichemasquelot(masque);
			}
		} else {
			this.resultat.setText("Vous devez d'abord s�lectionner un masque dans la liste");
		}
	}

	/**
	 * Chargement des �l�ments d'un masque s�lectionn� dans le combo
	 */
	/**
	 * 
	 */
	private void chargemasques() {

		try {
			// chargement des masques
			Masque masque = (Masque) jListMasque.getSelectedValue();
			this.textAreaDescription.setText(masque.getDescription());
			// this.textFieldToucheRaccourci.setText(masque.getRaccouci());
			this.textFieldNomMasque.setText(masque.getCode());
			// this.textFieldNomMasque.setEditable(true);
			this.comboBoxTypeMasqueType.setSelectedItem(masque.getType());
			this.buttonEditMasque.setEnabled(true);
		} catch (Exception e) {

			this.resultat.setText(e.getMessage());
		}
	}

	/**
	 * Chargement des �l�ments d'un masque
	 */
	private void chargemasque(Masque masque) {

		try {
			// chargement des masques
			jListMasque.setSelectedValue(masque, true); // true pour dire de
			// scroller jusqu'au
			// niveau du masque
			// this.textAreaDescription.setText(masque.getDescription());
			// this.textFieldToucheRaccourci.setText(masque.getRaccouci());
			// this.textFieldNomMasque.setText(masque.getCode());
			// // this.textFieldNomMasque.setEditable(true);
			// this.comboBoxTypeMasqueType.setSelectedItem(masque.getType());

		} catch (Exception e) {

			this.resultat.setText(e.getMessage());
		}
	}

	/**
	 * Chargement de AMSMasqueOpe
	 */
	private void affichemasqueope(Masque masque) {
		// Recherche du parent qui est un IhmAppli
		Component ihm = this.getParent();
		while ((ihm != null) && !(ihm instanceof IhmAppli)) {
			ihm = ihm.getParent();
		}
		// Passe a l'ecran d'ajout d'une operation
		((IhmAppli) ihm).changeContenu(new AMSMasqueOpe(masque));
	}

	/**
	 * Chargement de AMSMasqueLot
	 */
	private void affichemasquelot(Masque masque) {
		// Recherche du parent qui est un IhmAppli
		Component ihm = this.getParent();
		while ((ihm != null) && !(ihm instanceof IhmAppli)) {
			ihm = ihm.getParent();
		}
		// Passe a l'ecran d'ajout d'une operation
		((IhmAppli) ihm).changeContenu(new AMSMasqueLot(masque));
	}

	/**
	 * Fonction de vidange des combos
	 */
	private void nettoietout() {

		comboBoxTypeMasqueType.setSelectedIndex(1);
		// textFieldToucheRaccourci.setText(null);
		textAreaDescription.setText(null);
		// textFieldNomMasque.setEditable(true);
	}

	private void nettoyeraccourci() {
		// textFieldToucheRaccourci.setText(null);

	}

	/**
	 * Vide le contenu et charge le panneau d'accueil
	 */
	private void quitte() {
		Component ihm = this.getParent();
		while ((ihm != null) && !(ihm instanceof IhmAppli)) {
			ihm = ihm.getParent();
		}
		// Passe a l'ecran d'ajout d'une operation
		((IhmAppli) ihm).removeContenu();
		((IhmAppli) ihm).changeContenu(new Accueil());
		((IhmAppli) ihm).chargeMasqueOpe();
		((IhmAppli) ihm).chargeMasqueLot();
	}
	// M�thodes de l'interface Keylistener

	@Override
	public void keyTyped(KeyEvent e) {
		// String oldtext=this.textFieldToucheRaccourci.getText();
		// this.textFieldToucheRaccourci.setText(oldtext+e.getKeyChar());

	}

	@Override
	public void keyPressed(KeyEvent e) {

	}
	/*
	 * @Override public void keyPressed(KeyEvent e) { String oldtext =
	 * this.textFieldToucheRaccourci.getText(); if (oldtext.contains("Ctrl") |
	 * oldtext.contains("Alt") | oldtext.contains("Maj")) { oldtext = oldtext +
	 * "+"; } if (e.getKeyCode() < 65 | e.getKeyCode() > 90) {
	 * this.textFieldToucheRaccourci.setText(oldtext +
	 * e.getKeyText(e.getKeyCode())); } }
	 */

	// Cette m�thode doit �tre pr�sente pour l'inteface, mais elle n'est pas
	// utilis�e
	@Override
	public void keyReleased(KeyEvent e) {

	}

}
