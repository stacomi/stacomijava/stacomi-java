/*
 **********************************************************************
 *
 * Nom fichier :        DC.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 *
 */

package infrastructure;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
//import java.util.Vector;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;
import infrastructure.referenciel.RefTypeDC;
import infrastructure.referenciel.SousListePeriodes;
import migration.TailleVideo;
import migration.referenciel.ListeTailleVideo;
import migration.referenciel.SousListeOperations;
import migration.referenciel.SousListeTailleVideo;

/**
 * Dispositif de comptage
 */
public class DC extends Dispositif implements IBaseDonnees {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	private DF DF;
	private String code;
	private RefTypeDC type;
	private SousListeOperations lesOperations;
	private ListeTailleVideo lesTaillesVideo;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit un DC avec uniquement un identifiant
	 * 
	 * @param _identifiant
	 *            l'identifiant du DC
	 */
	public DC(Integer _identifiant) {
		this(_identifiant, null, null, null, null, null, null, null, null);
	} // end DC

	/**
	 * Construit un DC avec uniquement un identifiant et un code (code au sein
	 * du DF)
	 * 
	 * @param _identifiant
	 *            l'identifiant du DC
	 * @param _code
	 *            le code du DC
	 */
	public DC(Integer _identifiant, String _code) {
		this(_identifiant, null, null, null, null, null, _code, null, null);
	} // end DC

	/**
	 * Construit un DC avec tous ses attributs
	 * 
	 * @param _identifiant
	 *            identifiant du DC
	 * @param _dateCreation
	 *            date de cr�ation
	 * @param _dateSuppression
	 *            date de suppression
	 * @param _commentaires
	 *            commentaires
	 * @param _lesPeriodes
	 *            periodes de fonctionnement
	 * @param _df
	 *            DF de rattachement
	 * @param _code
	 *            code du DC
	 * @param _type
	 *            type de DC
	 * @param _lesOperations
	 *            op�rations de contr�le
	 */
	public DC(Integer _identifiant, Date _dateCreation, Date _dateSuppression, String _commentaires,
			SousListePeriodes _lesPeriodes, DF _df, String _code, RefTypeDC _type, SousListeOperations _lesOperations) {

		super(_identifiant, _dateCreation, _dateSuppression, _commentaires, _lesPeriodes);

		this.setDF(_df);
		this.setCode(_code);
		this.setType(_type);
		this.setLesOperations(_lesOperations);

		// chargement des tailles vid�o si le DC en poss�de
		chargeListeTailleVideo();
	} // end DC

	/**
	 * Chargement de la liste des tailles du DC
	 */
	public void chargeListeTailleVideo() {
		SousListeTailleVideo sltv = new SousListeTailleVideo(this);
		try {
			sltv.chargeSansFiltre();
			// recopie de la sousListe dans la liste
			lesTaillesVideo = new ListeTailleVideo();
			for (int i = 0; i < sltv.size(); i++) {
				TailleVideo tv = (TailleVideo) sltv.get(i);
				this.lesTaillesVideo.put(this.getIdentifiant() + tv.getDistance(), tv);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return le nom de la table
	 */
	public static String getNomTable() {
		return "t_dispositifcomptage_dic";
	}

	/**
	 * Retourne le nom des attributs de la table correspondant a la classe
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		// impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
		// est inconnu
		ArrayList<String> ret = new ArrayList<String>();
		ret.add("dic_dis_identifiant");
		ret.add("dic_dif_identifiant");
		ret.add("dic_code");
		ret.add("dic_tdc_code");
		ret.add("dic_org_code");
		return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de la classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret = new ArrayList<Object>();
		ret.add(this.getIdentifiant());
		ret.add(this.DF.getIdentifiant());
		ret.add(this.getCode());
		ret.add(this.getType().getCode());
		return ret;
	} // end getValeurAttributs

	///////////////////////////////////////
	// interfaces
	///////////////////////////////////////

	/*
	 * Insertion d'un DC dans la DBB
	 */
	public void insertObjet() throws SQLException, ClassNotFoundException {

		// Insertion des objets de la table tg_dispositif_dis
		String table = Dispositif.getNomTable();
		ArrayList nomAttributs = Dispositif.getNomAttributs();
		ArrayList valAttributs = super.getValeurAttributs();

		ConnexionBD.getInstance().executeInsert(table, nomAttributs, valAttributs);

		// La v�rification des attributs du dispositif se fait par appel � la
		// m�thode super dans DC
		// initialise l'identifiant avec l'indentifiant de la table de
		// groupement des dc et des df (super)
		super.setIdentifiant((Integer) ConnexionBD.getInstance().getGeneratedKey(Dispositif.getNomSequences()[0]));

		// insertion des objets de la table t_dispositifcomptage_dic
		String tableDC = DC.getNomTable();
		ArrayList nomAttributsDC = DC.getNomAttributs();
		ArrayList valAttributsDC = this.getValeurAttributs();
		ConnexionBD.getInstance().executeInsert(tableDC, nomAttributsDC, valAttributsDC);
	}

	public void majObjet() throws SQLException, ClassNotFoundException {

		// Modification des objets de la table tg_dispositif_dis
		String dispositifTable = Dispositif.getNomTable();
		String[] disNomAttributsSelection = Dispositif.getNomID();
		Object[] disValAttributsSelection = super.getValeurID();
		ArrayList disNomAttributsModifies = Dispositif.getNomAttributs();
		ArrayList disValAttributsModifies = super.getValeurAttributs();

		ConnexionBD.getInstance().executeUpdate(dispositifTable, disNomAttributsSelection, disValAttributsSelection,
				disNomAttributsModifies, disValAttributsModifies); // OK pour la
																	// modification
																	// de la
																	// table
																	// tg_dispositif_dis

		// Modification des objets de la table t_dispositifcomptage_dic
		String DCtable = DC.getNomTable();
		String[] DCnomAttributsSelection = DC.getNomID();
		Object[] DCvalAttributsSelection = this.getValeurID();
		ArrayList DCnomAttributs = DC.getNomAttributs();
		ArrayList DCvalAttributs = this.getValeurAttributs();
		ConnexionBD.getInstance().executeUpdate(DCtable, DCnomAttributsSelection, DCvalAttributsSelection,
				DCnomAttributs, DCvalAttributs);
	}

	public void effaceObjet() {
	}

	/**
	 * V�rification des attributs d'un DC
	 * 
	 * @return vrai si le DC est valide, soul�ve une exception sinon
	 */
	public boolean verifAttributs() throws DataFormatException {
		// identifiant autoincremente
		// if(!Verification.isInteger(super.getidentifiant(), 1, true)) {
		// throw new DataFormatException (Erreur.S10000) ;
		// }

		if (!Verification.isInteger(this.DF.getIdentifiant(), 0, false)) {
			throw new DataFormatException(Erreur.S10002);
		}

		// verifie que la date de supression est post�rieure � la date de
		// creation
		// false false => la saisie de la date de creation ou de la date de
		// supression n'est pas obligatoire
		if (!Verification.isDateInf(super.getDateCreation(), super.getDateSuppression(), true, false)) {
			throw new DataFormatException(Erreur.S10005);
		}

		if (!Verification.isText(super.getCommentaires(), 1, 60, false)) {
			throw new DataFormatException(Erreur.S10006);
		}

		if (!Verification.isText(this.code, 1, 16, true)) {
			throw new DataFormatException(Erreur.S11000);
		}

		return true;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Retourne le DF que le DC equipe
	 * 
	 * @return le DF
	 */
	public DF getDF() {
		return this.DF;
	} // end getDF

	/**
	 * Retourne le code du DC au sein du DF
	 * 
	 * @return le code
	 */
	public String getCode() {
		return this.code;
	} // end getCode

	/**
	 * Retourne le type du DC
	 * 
	 * @return le type
	 */
	public RefTypeDC getType() {
		return this.type;
	} // end getType

	/**
	 * Retourne la sous-liste des operations realisees sur le DC
	 * 
	 * @return la sous-liste des operations
	 */
	public SousListeOperations getLesOperations() {
		return this.lesOperations;
	} // end getLesOperations

	/**
	 * Initialise le DF que le DC equipe
	 * 
	 * @param _DF
	 *            le DF
	 */
	public void setDF(DF _DF) {
		this.DF = _DF;
	} // end setDF

	/**
	 * Initialise le code du DC au sein du DF
	 * 
	 * @param _code
	 *            le code
	 */
	public void setCode(String _code) {
		this.code = _code;
	} // end setCode

	/**
	 * Initialise le type du DC
	 * 
	 * @param _type
	 *            le type
	 */
	public void setType(RefTypeDC _type) {
		this.type = _type;
	} // end setType

	/**
	 * Initialise la sous-liste des operations realisees sur le DC
	 * 
	 * @param _lesOperations
	 *            la sous-liste des operations
	 */
	public void setLesOperations(SousListeOperations _lesOperations) {
		this.lesOperations = _lesOperations;
	} // end setLesOperations

	/**
	 * Retourne les attributs du dc sous forme textuelle
	 * 
	 * @return ret un string
	 */
	public String toString() {

		String ret = this.code;
		ret = org.apache.commons.lang3.StringUtils.abbreviate(ret, 30);
		return ret;
	}

	/**
	 * Retourne le nom de l'identifiant caract�re de la table
	 * 
	 * @return un vecteur de string de longueur 1 contenant le nom de
	 *         l'identifiant de la table
	 */
	public static String[] getNomID() {
		String[] ret = new String[1];
		ret[0] = "dic_dis_identifiant";
		return ret;
	} // end getNomID

	/**
	 * Retourne la valeur de l'indenfiant string de la classe
	 * 
	 * @return un vecteur d'objet de longueur 1 contenant le code
	 */
	public Object[] getValeurID() {
		Object[] ret = new Object[1];
		ret[0] = this.getIdentifiant();
		return ret;
	} // end getValeurID

	/**
	 * Arrete le DC (fixe la date de suppression...)
	 * 
	 * @param d
	 *            : la date de suppression du DC
	 * @throws DataFormatException
	 *             Format des donn�es incorrect
	 * @throws SQLException
	 *             Erreur avec la BDD
	 * @throws ClassNotFoundException
	 *             Classe non trouv�e
	 */
	public void stop(Date d) throws DataFormatException, SQLException, ClassNotFoundException {
		// s'il n'y a aucune date de suppression, on en met une
		if (super.getDateSuppression() == null)
			super.setDateSuppression(d);

		// v�rification des attributs et mise a jour...
		this.verifAttributs();
		this.majObjet();
	}

	/**
	 * Retourne la liste des tailles video
	 * 
	 * @return la liste des tailles video
	 */
	public ListeTailleVideo getListeTailleVideo() {
		return this.lesTaillesVideo;
	}

	/**
	 * Affecte la liste des tailles vid�o
	 * 
	 * @param ltv
	 *            la liste des tailles vid�o
	 */
	public void setListeTailleVideo(ListeTailleVideo ltv) {
		this.lesTaillesVideo = ltv;
	}

} // end DC
