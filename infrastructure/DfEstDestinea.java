/**
 * 
 */
package infrastructure;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import migration.referenciel.RefTaxon;
import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;

/**
 * Classe servant � �crire / modifier la table jointure entre les DF les taxons de destination
 * @author C�dric Briand
 */
public class DfEstDestinea implements IBaseDonnees {

	// /////////////////////////////////////
	// attributes
	// /////////////////////////////////////
	//  dtx_dif_identifiant integer NOT NULL,
	//  dtx_tax_code character varying(6) NOT NULL,
	private Integer dfidentifiant;
	private RefTaxon refTaxon;


	// /////////////////////////////////////
	// constructors
	// /////////////////////////////////////
	/**
	 * Construit une classe tous les attributs, l'identifiant et le taxon
	 * 
	 * @param _dfidentifiant
	 * @param _refTaxon
	 */
	public DfEstDestinea(Integer _dfidentifiant, RefTaxon _refTaxon) {
		
		this.setDfidentifiant(_dfidentifiant);
		this.setrefTaxon(_refTaxon);
	}




	// /////////////////////////////////////
	// interfaces
	// /////////////////////////////////////

	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		String[] nomSelection = this.getNomID();
		Object[] valSelection = this.getValeurID();
		ConnexionBD.getInstance().executeDelete(table,nomSelection,valSelection);
	}

	/** (non-Javadoc)
	 * insertion des objets de la table tj_dfestdestinea_dtx
	 * @see commun.IBaseDonnees#insertObjet()
	 */
	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		ArrayList nomAttributs = this.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		ConnexionBD.getInstance().executeInsert(table, nomAttributs,
				valAttributs);
	}
	  

	public void majObjet() throws SQLException, ClassNotFoundException {
		// � faire

	}

	/**
	 * Met � jour le type de DF dans la base de donn�es.
	 * La m�thode majObjet() de l'interface IBaseDonnees ne peut pas etre utilis�e car
	 * la cl� primaire de la table tj_dfestdestinea_dtx est compos�e du doublet (dtx_dif_identifiant , dtx_tax_code).
	 * On a donc besoin de ces 2 informations pour mettre � jour dans la BDD
	 * @param oldTaxon l'ancien taxon de destination pour avoir les anciennes valeurs (pour la clause WHERE)
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public void majObjet(DfEstDestinea oldTaxon) throws SQLException, ClassNotFoundException {
		
		String table = this.getNomTable();
		ArrayList nomAttributs = this.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();

		String[] nomSelection = oldTaxon.getNomID();
		Object[] valSelection = oldTaxon.getValeurID();
		
		ConnexionBD.getInstance().executeUpdate(table, 
				nomSelection, valSelection, 
				nomAttributs, valAttributs);
	}
	
	/* verifie les attributs des champs rentr�s dans la table de jointure
	 * @see commun.IBaseDonnees#verifAttributs()
	 * 
	 */
	public boolean verifAttributs() throws DataFormatException {
       boolean ret = false;
		if (!Verification.isInteger(this.dfidentifiant,0,false)) {
            throw new DataFormatException (Erreur.S10000) ;
            }               
        if (!Verification.isText(this.refTaxon.getCode().toString(), 1, 6, true)) {
            throw new DataFormatException (Erreur.S10007) ;
        }
        return(ret);
	}
	
    // /////////////////////////////////////
    // operations
    // /////////////////////////////////////
	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return le nom
	 */
	private String getNomTable() {
		return "tj_dfestdestinea_dtx";
	}

	/**
	 * Retourne le nom des champs de la table
	 * @return le noms des champs de la table dans l'ordre d'insertion dans la base de donnee
	 */
	private ArrayList<String> getNomAttributs() {
		ArrayList<String> ret = new ArrayList<String>();
		ret.add("dtx_dif_identifiant");
		ret.add("dtx_tax_code");
		ret.add("dtx_org_code");
		return ret;
	}
	
	 /**
	 * Retourne la valeur des attributs de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	private ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret = new ArrayList<Object>();
		ret.add(this.dfidentifiant);
		ret.add(this.refTaxon.getCode());
		return ret;
	}

	/**
	 * retourne l'identifiant du DF
	 * 
	 * @return l'indentifiant du DF
	 */
	public Integer getDfidentifiant() {
		return this.dfidentifiant;
	}

	/**
	 * retourne la liste de r�f�rence du DF
	 * 
	 * @return le taxon de r�f�rence
	 */
	public RefTaxon getrefTaxon() {
		return this.refTaxon;
	}


	/**
	 * Initialise la  r�f�rence du taxon de destination du DF
	 * @param refTaxon taxon
	 */
	public void setrefTaxon(RefTaxon refTaxon) {
		this.refTaxon = refTaxon;
	}

	/**
	 * Initialise l'identifiant du dispositif de franchissement
	 * 
	 * @param dfidentifiant
	 */
	public void setDfidentifiant(Integer dfidentifiant) {
		this.dfidentifiant = dfidentifiant;
	}

	/**
	 * Recup�ration des noms des champs d'un type de taxon
	 * @return les noms des champs d'un type de taxon
	 */
	private String[] getNomID()
	{
		String[] res = new String[2];
		res[0] = "dtx_dif_identifiant";
		res[1] = "dtx_tax_code";
		return res;
	}
	
	/**
	 * Recup�ration des valeurs des champs d'un type de taxon 
	 * @return les valeurs des champs d'un type de taxon
	 */
	private Object[] getValeurID()
	{
		Object[] res = new Object[2];
		res[0] = this.dfidentifiant;
		res[1] = this.refTaxon.getCode();
		return res;
	}

}
