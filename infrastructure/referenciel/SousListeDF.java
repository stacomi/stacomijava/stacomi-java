/*
 **********************************************************************
 *
 * Nom fichier :        SousListeDF.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package infrastructure.referenciel;

import commun.*;

import infrastructure.DF;
import infrastructure.Ouvrage;

import java.sql.*;

import migration.referenciel.ListeRefTaxon;
import migration.referenciel.RefTaxon;

/**
 * Sous-liste pour les DF
 * @author C�dric Briand
 */
public class SousListeDF extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 4041256577415309978L;

	/**
	 * Construit une sousliste
	 */
	public SousListeDF() {
		super();
	} // end sousListeDF

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * @param _objetDeRattachement l'objet de rattachement
	 */
	public SousListeDF(Ouvrage _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeDF

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		// Extraction de tous les enregistrements de la table concernee pour l'ouvrage donn�
		String sql = "SELECT dif_dis_identifiant, dif_code " +
				"FROM " + ConnexionBD.getSchema() + "t_dispositiffranchissement_dif WHERE dif_ouv_identifiant='"
				+ this.getObjetDeRattachementID()[0] + "' ORDER BY dif_code ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			Integer identifiant = new Integer(rs.getInt("dif_dis_identifiant"));
			String code = rs.getString("dif_code");

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			DF df = new DF(identifiant, code);
			df.setOuvrage((Ouvrage) super.objetDeRattachement);

			this.put(identifiant, df);

		} // end while
	}

	public void chargeSansFiltreDetails() throws Exception {
		//    	ResultSet   rs     	= null ;
		//
		//        String sql = "SELECT dis_identifiant as DF, dis_date_creation, dis_date_suppression, "+
		//        "dis_commentaires, dif_ouv_identifiant, ouv_code, ouv_libelle, dif_code as DF_code, "+
		//        "dif_localisation, dif_orientation, tdf_code as type_DF "+
		//        "FROM tg_dispositif_dis "+
		//        "JOIN t_dispositiffranchissement_dif ON dif_dis_identifiant=dis_identifiant "+
		//        "JOIN tj_dfesttype_dft ON dif_dis_identifiant=dft_df_identifiant "+
		//        "JOIN t_ouvrage_ouv ON dif_ouv_identifiant=ouv_identifiant "+
		//        "JOIN tr_typedf_tdf ON tdf_code=dft_tdf_code "+
		//        "WHERE dif_ouv_identifiant = '"+this.getObjetDeRattachementID()[0]+ " "+
		//        "ORDER BY dis_identifiant";
		//        
		//        // En principe, il y a un et un seul enregistrement
		//        if (rs.next()) {
		//            // Lecture des champs dans le ResultSet
		//			Integer identifiant = rs.getInt("DF_code");
		//			Date dateCreation = rs.getDate("dis_date_creation");
		//			Date dateSuppression = rs.getDate("dis_date_suppression");
		//			String commentaires = rs.getString("dis_commentaires");
		//			Integer ouv_identifiant = rs.getInt("dif_ouv_identifiant");
		//			String ouv_code = rs.getString("ouv_libelle");
		//			String ouv_libelle = rs.getString("ouv_libelle");
		//			String code = rs.getString("DF_code");
		//			String localisation = rs.getString("dif_localisation");
		//			String orientation = rs.getString("dif_orientation");
		//			String tdf_code = rs.getString("tdf_code");
		//			
		//			RefTypeDF refTypeDF = new RefTypeDF(tdf_code);
		//			ListeRefTypeDF listeRefTypeDF = new ListeRefTypeDF();
		//			listeRefTypeDF.put(refTypeDF.getCode(), refTypeDF);
		//			
		//			Ouvrage ouv = new Ouvrage(ouv_identifiant,ouv_code, ouv_libelle);
		//			DF df = new DF(identifiant, dateCreation, dateSuppression, commentaires,
		//					null,ouv, code, localisation, orientation,null, null, listeRefTypeDF);
		//			this.put(df.getCode(), df);
		//		} 
		//        else 
		//        {
		//			throw new Exception(Erreur.I1001);
		//		}
	}

	/**
	 * Methode a appeler sur un objet de la liste, quand celui-ci a ete
	 * initialise avec son identifiant. Permet de charger tous les autres
	 * attributs de cet objet (sauf les listes) ced : le code du DF et son
	 * ouvrage de rattachement permettent de le pr�ciser.
	 * Charge les informations � propos du type de DF et taxon (primaires...)
	 * 
	 */
	public void chargeObjet(Object _objet) throws Exception {

		ResultSet rs = null;

		// Si l'identifiant de l'objet n'est pas definit, erreur
		Integer identifiant = ((DF) _objet).getOuvrage().getIdentifiant();
		if (identifiant == null) {
			throw new NullPointerException(Erreur.I1000);
		}
		// Extraction de l'enregistrement concerne
		// cle unique dif_ouv_identifiant dif_code
		String sql = "SELECT dis_identifiant as DF, dis_date_creation, dis_date_suppression, "
				+ "dis_commentaires, dif_ouv_identifiant, ouv_code, ouv_libelle, dif_code as DF_code, "
				+ "dif_localisation, dif_orientation, tax_code as type_Taxon, tdf_code as type_DF "
				+ "FROM " + ConnexionBD.getSchema() + "tg_dispositif_dis "
				+ "JOIN " + ConnexionBD.getSchema() + "t_dispositiffranchissement_dif ON dif_dis_identifiant=dis_identifiant "
				+ "JOIN " + ConnexionBD.getSchema() + "tj_dfesttype_dft ON dif_dis_identifiant=dft_df_identifiant "
				+ "JOIN ref.tr_typedf_tdf ON tdf_code=dft_tdf_code "
				+ "JOIN " + ConnexionBD.getSchema() + "t_ouvrage_ouv ON dif_ouv_identifiant=ouv_identifiant "
				+ "JOIN " + ConnexionBD.getSchema() + "tj_dfestdestinea_dtx ON dif_dis_identifiant=dtx_dif_identifiant "
				+ "JOIN ref.tr_taxon_tax ON tax_code=dtx_tax_code "
				+ "WHERE dif_ouv_identifiant = '"
				+ identifiant
				+ "' "
				+ "ORDER BY dis_identifiant";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// En principe, il y a un et un seul enregistrement
		if (rs.next()) {

			Date dateCreation = rs.getDate("dis_date_creation");
			Date dateSuppression = rs.getDate("dis_date_suppression");
			String commentaires = rs.getString("dis_commentaires");
			int ouv_identifiant = rs.getInt("dif_ouv_identifiant");
			String ouv_code = rs.getString("ouv_libelle");
			String ouv_libelle = rs.getString("ouv_libelle");
			String code = rs.getString("DF_code");
			String localisation = rs.getString("dif_localisation");
			String orientation = rs.getString("dif_orientation");
			String tdf_code = rs.getString("type_DF");
			String tax_code = rs.getString("type_Taxon");

			Ouvrage ouv = new Ouvrage(new Integer(ouv_identifiant), ouv_code,
					ouv_libelle);

			RefTypeDF refTypeDF = new RefTypeDF(tdf_code);
			ListeRefTypeDF listeRefTypeDF = new ListeRefTypeDF();
			listeRefTypeDF.put(refTypeDF.getCode(), refTypeDF);

			RefTaxon refTaxon = new RefTaxon(tax_code);
			ListeRefTaxon listeRefTaxon = new ListeRefTaxon(Lanceur
					.getListeRefNiveauTaxonomique());
			listeRefTaxon.put(tax_code, refTaxon);

			// Lecture des champs dans le ResultSet
			((DF) _objet).setCode(code);
			((DF) _objet).setDateCreation(dateCreation);
			((DF) _objet).setDateSuppression(dateSuppression);
			((DF) _objet).setCommentaires(commentaires);
			((DF) _objet).setLocalisation(localisation);
			((DF) _objet).setOrientation(orientation);
			((DF) _objet).setOuvrage(ouv);
			((DF) _objet).setLesTypesDeDF(listeRefTypeDF);
			((DF) _objet).setLesTaxonsDeDestination(listeRefTaxon);
		} else {
			throw new Exception(Erreur.I1001);
		}
	}

	/**
	 * Retourne l'identifiant de l'ouvrage du df
	 * @return Tableau de taille 1 : (0) identifiant
	 */
	public String[] getObjetDeRattachementID() {

		// L'identifiant de Ouvrage est : identifiant
		String ret[] = new String[1];
		ret[0] = ((Ouvrage) super.objetDeRattachement).getIdentifiant()
				.toString();

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

} // end SousListeDF

