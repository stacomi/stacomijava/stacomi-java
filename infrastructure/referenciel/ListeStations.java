/*
 **********************************************************************
 *
 * Nom fichier :        ListeStations.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package infrastructure.referenciel;

import commun.* ;
import infrastructure.Station;

import java.sql.* ;
import java.util.Date ;

/**
 * Liste pour les station de controle des migrations
 * @author C�dric Briand
 */

public class ListeStations extends Liste {

   ///////////////////////////////////////
   // attributes
   ///////////////////////////////////////
    



  ///////////////////////////////////////
  // operations heritees
  ///////////////////////////////////////

    /**
	 * 
	 */
	private static final long serialVersionUID = 8657958250337800070L;




	public void chargeSansFiltre() throws Exception {
        ResultSet   rs      = null ;
        
        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT sta_code, sta_nom FROM " + ConnexionBD.getSchema() + "t_station_sta ORDER BY sta_nom ;" ;
        
        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String code = rs.getString("sta_code") ;
            String libelle = rs.getString("sta_nom") ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste
            Station sta = new Station(code, libelle) ;
            this.put(code, sta) ;
        
        } // end while

    }
    
    
    public void chargeSansFiltreDetails() throws Exception {
        ResultSet   rs                          = null ;

        // Extraction de toutes les enregistrements de la table concernee
        String sql = "SELECT sta_code, sta_nom, sta_localisation, sta_coordonnee_x, sta_coordonnee_y, sta_altitude, sta_carte_localisation, sta_superficie, sta_distance_mer, sta_date_creation, sta_date_suppression, sta_commentaires, sta_dernier_import_conditions " +
        		"FROM  "+ConnexionBD.getSchema()+"t_station_sta ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String          code                        = rs.getString("sta_code")                      ;
            String          libelle                     = rs.getString("sta_nom")                       ;
            String          localisation                = rs.getString("sta_localisation")              ;
            Integer         coordonneeX                 = new Integer( rs.getInt("sta_coordonnee_x") )  ;
            Integer         coordonneeY                 = new Integer( rs.getInt("sta_coordonnee_y") )  ;
            Short           altitude                    = new Short( rs.getShort("sta_altitude") )      ;
//            InputStream     inCarteLocalisation         = rs.getBinaryStream("sta_carte_localisation")  ; // flux temporaire pour la lecture de l'image
//            BufferedImage   carteLocalisation           = ImageIO.read(inCarteLocalisation)             ;
            Integer         superficieBassin            = new Integer( rs.getInt("sta_superficie") )    ;
            Float           distanceMer                 = new Float( rs.getFloat("sta_distance_mer") )  ;
            Date            dateCreation                = rs.getDate("sta_date_creation")               ;
            Date            dateSuppression             = rs.getDate("sta_date_suppression")            ;
            String          commentaires                = rs.getString("sta_commentaires")              ;
            Date            dernierImportConditionsEnv  = rs.getDate("sta_dernier_import_conditions")   ;
            
            // Creation de l'objet d'apres les champs lus et ajout a la liste

            Station sta = new Station(code, libelle, localisation, coordonneeX, coordonneeY, altitude, null, superficieBassin, distanceMer, dateCreation, dateSuppression, commentaires, dernierImportConditionsEnv) ;
            this.put(code, sta) ;
        
        } // end while     
    }
    
    
    public void chargeObjet(Object _objet) throws Exception {
        ResultSet   rs                          = null ;
        
        // Si l'identifiant de l'objet n'est pas definit, erreur
        String code = ((Station)_objet).getCode() ;
        if (code == null) {
            throw new NullPointerException(Erreur.I1000) ;
        }
        
        // Extraction de l'enregistrement concerne
        String sql = "SELECT sta_nom, sta_localisation, sta_coordonnee_x, sta_coordonnee_y, sta_altitude, sta_carte_localisation, sta_superficie, sta_distance_mer, sta_date_creation, sta_date_suppression, sta_commentaires, sta_dernier_import_conditions " +
        		"FROM  "+ConnexionBD.getSchema()+"t_station_sta WHERE sta_code = '" + code + "' ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // En principe, il y a un et un seul enregistrement
        if (rs.first() == false) 
            throw new Exception(Erreur.I1001) ;

        // Lecture des champs dans le ResultSet
        String          libelle                     = rs.getString("sta_nom")                       ;
        String          localisation                = rs.getString("sta_localisation")              ;
        Integer         coordonneeX                 = new Integer( rs.getInt("sta_coordonnee_x") )  ;
        Integer         coordonneeY                 = new Integer( rs.getInt("sta_coordonnee_y") )  ;
        Short           altitude                    = new Short( rs.getShort("sta_altitude") )      ;
        //InputStream     inCarteLocalisation         = rs.getBinaryStream("sta_carte_localisation")  ; //flux
        //BufferedImage   carteLocalisation           = ImageIO.read(inCarteLocalisation)             ;
        Integer         superficieBassin            = new Integer( rs.getInt("sta_superficie") )    ;
        Float           distanceMer                 = new Float( rs.getFloat("sta_distance_mer") )  ;
        Date            dateCreation                = rs.getDate("sta_date_creation")               ;
        Date            dateSuppression             = rs.getDate("sta_date_suppression")            ;
        String          commentaires                = rs.getString("sta_commentaires")              ;
        Date            dernierImportConditionsEnv  = rs.getDate("sta_dernier_import_conditions")   ;

        
        // Initialisation de l'objet d'apres les champs lus

        ((Station)_objet).setLibelle(libelle) ;
        ((Station)_objet).setLocalisation(localisation) ;
        ((Station)_objet).setCoordonneeX(coordonneeX) ;
        ((Station)_objet).setCoordonneeY(coordonneeY) ;
        ((Station)_objet).setAltitude(altitude) ;
       // ((Station)_objet).setCarteLocalisation(carteLocalisation) ;
        ((Station)_objet).setSuperficieBassin(superficieBassin) ;
        ((Station)_objet).setDistanceMer(distanceMer) ;
        ((Station)_objet).setDateCreation(dateCreation) ;
        ((Station)_objet).setDateSuppression(dateSuppression) ;
        ((Station)_objet).setCommentaires(commentaires) ;
        ((Station)_objet).setDernierImportConditionsEnv(dernierImportConditionsEnv) ;
              
    }
    
    

    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////    
    
/**
 * 
 * 
 * 
 * @param code 
 * @param libelle 
 */
    public void chargeFiltre(String code, String libelle) {   
        
        
        // A FAIRE
        
        
    } // end chargeFiltre        

    
    
 } // end ListeStations



