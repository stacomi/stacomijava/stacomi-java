/*
 **********************************************************************
 *
 * Nom fichier :        SousListeConditionsEnv.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package infrastructure.referenciel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import commun.ConnexionBD;
import commun.Erreur;
import commun.SousListe;
import infrastructure.ConditionEnvironnementale;
import infrastructure.Station;
import infrastructure.StationMesure;
import migration.referenciel.RefParametre;
import migration.referenciel.RefParametreQualitatif;
import migration.referenciel.RefParametreQuantitatif;
import migration.referenciel.RefValeurParametre;

/**
 * Sous-liste pour les conditions environnementales d'une station
 * 
 * @author C�dric Briand
 */
public class SousListeConditionsEnv extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 6709778172008174978L;

	/**
	 * Construit une sousliste
	 */
	public SousListeConditionsEnv() {
		super();
	} // end sousListeStationMesure

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * 
	 * @param _objetDeRattachement
	 *            l'objet de rattachement
	 */
	public SousListeConditionsEnv(StationMesure _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeStationMesure

	public void chargeSansFiltre() throws SQLException, ClassNotFoundException {
		ResultSet rs = null;

		String sql = "SELECT env_stm_identifiant, env_date_debut, env_date_fin " + "FROM " + ConnexionBD.getSchema()
				+ "tj_conditionenvironnementale_env " + "WHERE env_stm_code ='"
				+ ((StationMesure) this.objetDeRattachement).getIdentifiant() + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while (rs.next()) {
			Integer env_stm_identifiant = rs.getInt("env_stm_identifiant");
			Date env_date_debut = new Date(rs.getTimestamp("env_date_debut").getTime());
			Date env_date_fin = new Date(rs.getTimestamp("env_date_fin").getTime());

			ConditionEnvironnementale ce = new ConditionEnvironnementale();
			ce.setStationMesure(new StationMesure(env_stm_identifiant));
			ce.setDateDebut(env_date_debut);
			ce.setDateFin(env_date_fin);

			this.put(env_stm_identifiant.toString() + env_date_debut + env_date_fin, ce);
		}
	}

	public void chargeSansFiltreDetails() throws Exception, SQLException, ClassNotFoundException {
		ResultSet rs = null;

		String sql = "SELECT env_stm_identifiant, env_date_debut, env_date_fin, env_methode_obtention, "
				+ "env_val_identifiant, env_valeur_quantitatif, stm_sta_code, stm_par_code, stm_libelle, stm_description, "
				+ "par_nom " + "FROM " + ConnexionBD.getSchema() + "tj_conditionenvironnementale_env " + "JOIN 	"
				+ ConnexionBD.getSchema() + "tj_stationmesure_stm st ON st.stm_identifiant=env_stm_identifiant "
				+ "JOIN 	ref.tg_parametre_par par ON par.par_code=stm_par_code " + "WHERE env_stm_identifiant ='"
				+ ((StationMesure) this.objetDeRattachement).getIdentifiant() + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while (rs.next()) {
			Integer env_stm_identifiant = rs.getInt("env_stm_identifiant");
			Date env_date_debut = new Date(rs.getTimestamp("env_date_debut").getTime());
			Date env_date_fin = new Date(rs.getTimestamp("env_date_fin").getTime());
			String stm_libelle = rs.getString("stm_libelle");
			String par_nom = rs.getString("par_nom");
			Integer env_val_identifiant = rs.getInt("env_val_identifiant");
			Float env_valeur_quantitatif = rs.getFloat("env_valeur_quantitatif");
			String env_methode_obtention = rs.getString("env_methode_obtention");
			String stm_sta_code = rs.getString("stm_sta_code");
			Station station = new Station(stm_sta_code, null);
			String paramCode = rs.getString("stm_par_code");
			RefParametre parametre;
			boolean estqualitatif = RefParametre.estQualitatif(paramCode);
			if (estqualitatif)
				parametre = new RefParametreQualitatif(paramCode, par_nom, null);
			else
				parametre = new RefParametreQuantitatif(paramCode);
			String stm_description = rs.getString("stm_description");

			ConditionEnvironnementale ce = new ConditionEnvironnementale();
			ce.setStationMesure(
					new StationMesure(env_stm_identifiant, stm_libelle, station, parametre, stm_description));
			ce.setDateDebut(env_date_debut);
			ce.setDateFin(env_date_fin);
			ce.setMethodeObtention(env_methode_obtention);

			if (estqualitatif)// parametre qualitatif
			{
				ce.setValeurParametreQualitatif(new RefValeurParametre(env_val_identifiant, par_nom, null));
				ce.setValeurParametreQuantitatif(null);
			} else {
				ce.setValeurParametreQualitatif(null);
				ce.setValeurParametreQuantitatif(env_valeur_quantitatif);
			}

			this.put(env_stm_identifiant.toString() + env_date_debut + env_date_fin, ce);
		}
	}

	protected void chargeObjet(Object _objet) throws SQLException, ClassNotFoundException, Exception {
		ResultSet rs = null;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sql = "SELECT env_stm_identifiant, env_date_debut, env_date_fin, env_methode_obtention, env_val_identifiant, env_valeur_quantitatif "
				+ "FROM " + ConnexionBD.getSchema() + "tj_conditionenvironnementale_env " + "WHERE env_stm_code ='"
				+ ((ConditionEnvironnementale) _objet).getStationMesure().getIdentifiant() + "' "
				+ "AND env_date_debut ='" + sdf.format(((ConditionEnvironnementale) _objet).getDateDebut()) + "' "
				+ "AND env_date_fin ='" + sdf.format(((ConditionEnvironnementale) _objet).getDateFin()) + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// en principe, il n'y a qu'un seul enregistrement
		if (!rs.first())
			throw new Exception(Erreur.I1001);

		Integer env_stm_identifiant = rs.getInt("env_stm_identifiant");
		Date env_date_debut = rs.getTimestamp("env_date_debut");
		Date env_date_fin = rs.getTimestamp("env_date_fin");
		String env_methode_obtention = rs.getString("env_methode_obtention");
		Integer env_val_identifiant = rs.getInt("env_val_identifiant");
		Float env_valeur_quantitatif = rs.getFloat("env_valeur_quantitatif");

		RefValeurParametre valeurParametre = new RefValeurParametre(env_val_identifiant);

		((ConditionEnvironnementale) _objet).setStationMesure(new StationMesure(env_stm_identifiant));
		((ConditionEnvironnementale) _objet).setDateDebut(env_date_debut);
		((ConditionEnvironnementale) _objet).setDateFin(env_date_fin);
		((ConditionEnvironnementale) _objet).setMethodeObtention(env_methode_obtention);
		((ConditionEnvironnementale) _objet).setValeurParametreQualitatif(valeurParametre);
		((ConditionEnvironnementale) _objet).setValeurParametreQuantitatif(env_valeur_quantitatif);
	}

	/**
	 * Retourne l'identifiant de la Station
	 * 
	 * @return Tableau de taille 1 : (0) code
	 */
	public String[] getObjetDeRattachementID() {

		// L'identifiant de Station est : code
		String ret[] = new String[1];
		ret[0] = ((StationMesure) super.objetDeRattachement).getIdentifiant().toString();

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Charge les conditions environnementales pour la station de rattachement.
	 * Ne sont chargees que les conditions d'un parametre, et pour une date
	 * donn�e donnee. Cette methode est utilis�e pour l'affichage des param�tres
	 * environnementaux dans op�ration
	 * 
	 * @param dateDebut
	 *            la date de debut de la periode a charger
	 * 
	 */

	public boolean chargeFiltre(Date date) throws SQLException, ClassNotFoundException, Exception {
		ResultSet rs = null;
		Date sqldate = new java.sql.Timestamp(date.getTime());
		String sql = "SELECT env_stm_identifiant, env_date_debut, "
				+ "env_val_identifiant, env_valeur_quantitatif, stm_sta_code, stm_par_code " + "FROM "
				+ ConnexionBD.getSchema() + "tj_conditionenvironnementale_env " + "JOIN 	" + ConnexionBD.getSchema()
				+ "tj_stationmesure_stm st ON st.stm_identifiant=env_stm_identifiant "
				+ "JOIN 	ref.tg_parametre_par par ON par.par_code=stm_par_code " + " WHERE env_stm_identifiant ='"
				+ ((StationMesure) this.objetDeRattachement).getIdentifiant() + "' AND env_date_debut='"
				+ sqldate.toString() + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		// en principe, il n'y a qu'un seul enregistrement
		boolean isMoreThanOneRow = rs.first() && rs.next();
		// pas utilis� je ne vais chercher que le premier
		boolean isEmpty = !rs.first();
		if (!isEmpty) {
			rs.first();
			Integer env_stm_identifiant = rs.getInt("env_stm_identifiant");
			Date env_date_debut = new Date(rs.getTimestamp("env_date_debut").getTime());
			Integer env_val_identifiant = rs.getInt("env_val_identifiant");
			Float env_valeur_quantitatif = rs.getFloat("env_valeur_quantitatif");
			String stm_sta_code = rs.getString("stm_sta_code");
			Station station = new Station(stm_sta_code, null);
			String paramCode = rs.getString("stm_par_code");
			boolean estqualitatif = RefParametre.estQualitatif(paramCode);
			RefParametre parametre;
			if (estqualitatif)
				parametre = new RefParametreQualitatif(paramCode, null, null);
			else
				parametre = new RefParametreQuantitatif(paramCode);

			ConditionEnvironnementale ce = new ConditionEnvironnementale();
			ce.setStationMesure(new StationMesure(env_stm_identifiant, null, station, parametre, null));
			ce.setDateDebut(env_date_debut);

			if (estqualitatif)// parametre qualitatif
			{
				ce.setValeurParametreQualitatif(new RefValeurParametre(env_val_identifiant, null, null));
				ce.setValeurParametreQuantitatif(null);
			} else {
				ce.setValeurParametreQualitatif(null);
				ce.setValeurParametreQuantitatif(env_valeur_quantitatif);
			}

			this.put(env_stm_identifiant.toString() + env_date_debut, ce);
		}
		return (isMoreThanOneRow);
	}

	/**
	 * teste si la condition environnementale (stationmesure.identifiant,
	 * datedebut,datefin) est d�j� en base
	 * 
	 * @param ce
	 * @return
	 * @throws Exception
	 */
	public static boolean exists(ConditionEnvironnementale ce) throws Exception {
		ResultSet rs = null;

		String sql = "SELECT env_stm_identifiant,  " + "env_val_identifiant, env_valeur_quantitatif " + " FROM "
				+ ConnexionBD.getSchema() + "tj_conditionenvironnementale_env " + "JOIN 	" + ConnexionBD.getSchema()
				+ "tj_stationmesure_stm st ON st.stm_identifiant=env_stm_identifiant " + "WHERE env_stm_identifiant ='"
				+ ce.getStationMesure().getIdentifiant() + "' AND env_date_debut='" + ce.getDateDebut()
				+ "' AND env_date_fin='" + ce.getDateFin() + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		if (rs.next()) {
			return (true);
		} else {
			return (false);
		}

	}

} // end SousListeConditionsEnv
