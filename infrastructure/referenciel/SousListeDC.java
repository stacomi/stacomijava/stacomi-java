/*
 **********************************************************************
 *
 * Nom fichier :        SousListeDC.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package infrastructure.referenciel;

import java.sql.ResultSet;

import commun.ConnexionBD;
import commun.Erreur;
import commun.SousListe;
import infrastructure.DC;
import infrastructure.DF;

/**
 * Sous-liste pour les DC
 * 
 * @author C�dric Briand
 */
public class SousListeDC extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = -5955565646599548086L;

	/**
	 * Construit une sousliste
	 */
	public SousListeDC() {
		super();
	} // end sousListeDC

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * 
	 * @param _objetDeRattachement
	 *            l'objet de rattachement
	 */
	public SousListeDC(DF _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeDC

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT dic_dis_identifiant, dic_code FROM " + ConnexionBD.getSchema()
				+ "t_dispositifcomptage_dic WHERE dic_dif_identifiant='" + this.getObjetDeRattachementID()[0]
				+ "' ORDER BY dic_code ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			Integer identifiant = new Integer(rs.getInt("dic_dis_identifiant"));
			String code = rs.getString("dic_code");

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			DC dc = new DC(identifiant, code);
			dc.setDF((DF) super.objetDeRattachement);

			this.put(identifiant, dc);
		} // end while
	}

	public void chargeSansFiltreDetails() throws Exception {
		// A FAIRE
	}

	public void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		// Si l'identifiant de l'objet n'est pas definit, erreur
		Integer identifiant = ((DC) _objet).getIdentifiant();
		if (identifiant == null) {
			throw new NullPointerException(Erreur.I1000);
		}
		// Extraction de l'enregistrement concerne cle unique
		String sql = "SELECT dis_identifiant, dic_dif_identifiant, dis_date_creation , dis_date_suppression , dis_commentaires , dic_code, dic_tdc_code "
				+ " FROM " + ConnexionBD.getSchema() + "t_dispositifcomptage_dic dic JOIN " + ConnexionBD.getSchema()
				+ "tg_dispositif_dis dis ON dis.dis_identifiant=dic.dic_dis_identifiant"
				+ " WHERE dic_dis_identifiant= '" + identifiant + "' ORDER BY dic_code ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// en principe, il n'y a qu'un seul enregistrement
		if (!rs.first())
			throw new Exception(Erreur.I1001);

		// En principe, il y a un et un seul enregistrement
		// Lecture des champs dans le ResultSet
		int identifiantDF = rs.getInt("dic_dif_identifiant");
		DF df = new DF(new Integer(identifiantDF), null);
		((DC) _objet).setDF(df);
		((DC) _objet).setIdentifiant(new Integer(rs.getInt("dis_identifiant")));
		((DC) _objet).setCode(new String(rs.getString("dic_code")));
		((DC) _objet).setDateCreation(rs.getTimestamp("dis_date_creation"));
		((DC) _objet).setDateSuppression(rs.getDate("dis_date_suppression"));
		((DC) _objet).setCommentaires(rs.getString("dis_commentaires"));
		((DC) _objet).setType(new RefTypeDC(rs.getString("dic_tdc_code")));
	}

	/**
	 * Retourne l'identifiant du DF
	 * 
	 * @return Tableau de taille 1 : (0) identifiant
	 */
	public String[] getObjetDeRattachementID() {

		// L'identifiant de DF est : identifiant
		String ret[] = new String[1];
		ret[0] = ((DF) super.objetDeRattachement).getIdentifiant().toString();

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

} // end SousListeDC
