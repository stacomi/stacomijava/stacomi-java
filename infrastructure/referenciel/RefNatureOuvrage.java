/*
 **********************************************************************
 *
 * Nom fichier :        RefNatureOuvrage.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package infrastructure.referenciel;

import commun.*;

/**
 * Nature d'ouvrage
 * @author C�dric Briand
 */
public class RefNatureOuvrage extends Ref {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une reference avec un code mais sans libelle
	 * @param _code le code
	 */
	public RefNatureOuvrage(String _code) {
		super(_code, null);
	} // end RefNatureOuvrage

	/**
	 * Construit une reference avec un code et un libelle
	 * @param _code le code
	 * @param _libelle le libelle
	 */
	public RefNatureOuvrage(String _code, String _libelle) {
		super(_code, _libelle);
	} // end RefNatureOuvrage

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	public void insertObjet() {
		super.insertObjet("ref.tr_natureouvrage_nov");
	}

	public void majObjet() {
		super.majObjet("ref.tr_natureouvrage_nov");
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////    

} // end RefNatureOuvrage

