/*
 **********************************************************************
 *
 * Nom fichier :        SousListeOuvrages.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package infrastructure.referenciel;

import commun.*;

import infrastructure.Ouvrage;
import infrastructure.Station;

import java.sql.*;

/**
 * Sous-liste pour les ouvrages d'une station
 * @author C�dric Briand
 */
public class SousListeOuvrages extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/** */
	private static final long serialVersionUID = 4913597970282415350L;

	/**
	 * Construit une sousliste
	 */
	public SousListeOuvrages() {
		super();
	} // end sousListeOuvrages

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * @param _objetDeRattachement l'objet de rattachement
	 */
	public SousListeOuvrages(Station _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeOuvrages

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT ouv_identifiant, ouv_code, ouv_libelle " +
				"FROM " + ConnexionBD.getSchema() + "t_ouvrage_ouv WHERE ouv_sta_code='"
				+ this.getObjetDeRattachementID()[0] + "' ORDER BY ouv_code ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			Integer identifiant = new Integer(rs.getInt("ouv_identifiant"));
			String code = rs.getString("ouv_code");
			String libelle = rs.getString("ouv_libelle");

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			Ouvrage ouv = new Ouvrage(identifiant, code, libelle);
			ouv.setStation((Station) super.objetDeRattachement);

			this.put(identifiant, ouv);

		} // end while

	}

	/**
	 * Charge les ouvrages tri�s par identifiant
	 * @throws Exception
	 */
	public void chargeFiltreDerniere() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT ouv_identifiant, ouv_code, ouv_libelle " +
				"FROM " + ConnexionBD.getSchema() + "t_ouvrage_ouv WHERE ouv_sta_code='"
				+ this.getObjetDeRattachementID()[0] + "' ORDER BY ouv_code ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			Integer identifiant = new Integer(rs.getInt("ouv_identifiant"));
			String code = rs.getString("ouv_code");
			String libelle = rs.getString("ouv_libelle");

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			Ouvrage ouv = new Ouvrage(identifiant, code, libelle);
			ouv.setStation((Station) super.objetDeRattachement);

			this.put(identifiant, ouv);

		} // end while

	}

	public void chargeSansFiltreDetails() throws Exception {

		// A FAIRE

	}

	protected void chargeObjet(Object _objet) throws Exception {

		// A FAIRE

	}

	/**
	 * Retourne l'identifiant de la Station
	 * @return Tableau de taille 1 : (0) code
	 */
	public String[] getObjetDeRattachementID() {

		// L'identifiant de Station est : code
		String ret[] = new String[1];
		ret[0] = ((Station) super.objetDeRattachement).getCode();

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

} // end SousListeOuvrages

