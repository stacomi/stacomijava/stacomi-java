/*
 **********************************************************************
 *
 * Nom fichier :        RefTypeDC.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package infrastructure.referenciel;

import commun.* ;

/**
 * Type de DC
 * @author C�dric Briand
 */
public class RefTypeDC extends Ref {

    
  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////
    
    
    
  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    
    
/**
 * Construit une reference avec un code mais sans libelle
 * @param _code le code
 */    
    public RefTypeDC(String _code) {
        super(_code, null) ;
    } // end RefTypeDC
        
    
/**
 * Construit une reference avec un code et un libelle
 * @param _code le code
 * @param _libelle le libelle
 */    
    public RefTypeDC(String _code, String _libelle) {
        super(_code, _libelle) ;
    } // end RefTypeDC
    
   
    
    
    
  ///////////////////////////////////////
  // operations heritees
  ///////////////////////////////////////
    
    public void insertObjet() {
        super.insertObjet("ref.tr_typedc_tdc") ;
    }
    
    
    public void majObjet() {
        super.majObjet("ref.tr_typedc_tdc") ;
    }
    
    
    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////    
    
    
       
    
 } // end RefTypeDC



