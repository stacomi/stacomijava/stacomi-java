/*
 **********************************************************************
 *
 * Nom fichier :        SousListeRefTypeDF.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package infrastructure.referenciel;

import java.sql.ResultSet;

import commun.*;
import infrastructure.DF;

/**
 * Sous-liste pour les types de DF d'un certain DF
 * @author C�dric Briand
 */
public class SousListeRefTypeDF extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**  */
	private static final long serialVersionUID = 652341512633763919L;

	/**
	 * Construit une sousliste
	 */
	public SousListeRefTypeDF() {
		super();
	} // end sousListeRefTypeDF

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * @param _objetDeRattachement l'objet de rattachement
	 */
	public SousListeRefTypeDF(DF _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeRefTypeDF

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	public void chargeSansFiltre() {

		// A FAIRE

	}

	public void chargeSansFiltreDetails() {

		// A FAIRE

	}

	protected void chargeObjet(Object _objet) throws Exception {

		// A FAIRE

	}

	/**
	 * Retourne l'identifiant du DF
	 * @return Tableau de taille 1 : (0) identifiant
	 */
	public String[] getObjetDeRattachementID() {

		// L'identifiant de DF est : identifiant
		String ret[] = new String[1];
		ret[0] = ((DF) super.objetDeRattachement).getIdentifiant().toString();

		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * @param o le type de DF
	 * @return la liste des types du DF
	 * @throws Exception
	 */
	public ListeRefTypeDF chargeFiltreObjet(Object o) throws Exception {
		// Le DF
		DF df = (DF) o;

		// la liste des types de DF du DF
		ListeRefTypeDF resultat = new ListeRefTypeDF();

		// le resultset
		ResultSet rs = null;

		if ((df == null) || ((df.getIdentifiant() == null)))
			throw new NullPointerException(Erreur.I1000);

		// Extraction de l'enregistrement concerne cle unique
		String sql = "SELECT dft_tdf_code, dft_rang "
				+ " FROM " + ConnexionBD.getSchema() + "tj_dfesttype_dft " + " WHERE dft_df_identifiant= '"
				+ df.getIdentifiant().toString() + "' ; ";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// on parcourt l'ensemble des types de DF
		while (rs.next()) {
			// Lecture des champs dans le ResultSet
			String codeDF = rs.getString("dft_tdf_code");

			RefTypeDF rtdf = new RefTypeDF(codeDF);

			resultat.put(df.getIdentifiant() + codeDF, rtdf);
		}

		return resultat;
	}

} // end SousListeRefTypeDF

