/*
 **********************************************************************
 *
 * Nom fichier :        ListeDC.java
 * Projet :             Controle migrateurs 2009
 * Organisme :          IAV/CSP
 * Auteur :             S�bastien Laigre
 * Date de creation :   22 avril 2009
 * Etat :               a tester et a valider
 ********************************************************************** 
 */

package infrastructure.referenciel;

import commun.*;
import infrastructure.DC;
import infrastructure.DF;

import java.sql.*;
import java.util.Date;

/**
 * Liste pour les DC d�velopp�e par s�bastien a v�rifier
 * @author S�bastien Laigre
 */
public class ListeDC extends Liste {

	// /////////////////////////////////////
	// attributes
	// /////////////////////////////////////

	// /////////////////////////////////////
	// operations heritees
	// /////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 8657958250337800070L;

	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT dic_dis_identifiant " +
				"FROM " + ConnexionBD.getSchema() + "t_dispositifcomptage_dic " +
				"ORDER BY dic_dis_identifiant ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			int dic_identifiant = rs.getInt("dic_dis_identifiant");

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			DC dc = new DC(new Integer(dic_identifiant));
			this.put(dc.getIdentifiant(), dc);
		} // end while
	}

	public void chargeSansFiltreDetails() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT dic_dis_identifiant, dic_dif_identifiant, dis_date_creation, dis_date_suppression, dis_commentaires , dic_code, dic_tdc_code "
				+ "FROM " + ConnexionBD.getSchema() + "t_dispositifcomptage_dic dic"
				+ "JOIN " + ConnexionBD.getSchema() + "tg_dispositif_dis dis ON dis.dis_identifiant=dic.dic_dis_identifiant "
				+ "ORDER BY dic_dis_identifiant;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			int identifiant = rs.getInt("dic_dis_identifiant");
			int id_DF = rs.getInt("dic_dif_identifiant");
			DF df = new DF(new Integer(id_DF), null);

			String code = rs.getString("dic_code");
			String dic_tdc_code = rs.getString("dic_tdc_code");
			RefTypeDC rtdc = new RefTypeDC(dic_tdc_code);

			Date dateCreation = rs.getDate("dis_date_creation");
			Date dateSuppression = rs.getDate("dis_date_suppression");
			String commentaires = rs.getString("dis_commentaires");

			DC dc = new DC(new Integer(identifiant), dateCreation,
					dateSuppression, commentaires, null, df, code, rtdc, null);
			this.put(dc.getIdentifiant(), dc);
		} // end while
	}

	public void chargeObjet(Object _objet) throws Exception {

		ResultSet rs = null;

		// Si l'identifiant de l'objet n'est pas definit, erreur
		Integer identifiant = ((DC) _objet).getIdentifiant();
		if (identifiant == null) {
			throw new NullPointerException(Erreur.I1000);
		}
		// Extraction de l'enregistrement concerne
		// cle unique dif_ouv_identifiant dif_code
		String sql = "SELECT dis_date_creation, dis_date_suppression, dis_commentaires, dic_dif_identifiant, dif_code, dic_code, dic_tdc_code as type_DC "
				+ "FROM " + ConnexionBD.getSchema() + "tg_dispositif_dis "
				+ "JOIN " + ConnexionBD.getSchema() + "t_dispositifcomptage_dic ON dic_dis_identifiant=dis_identifiant "
				+ "JOIN " + ConnexionBD.getSchema() + "t_dispositiffranchissement_dif ON dif_dis_identifiant=dic_dif_identifiant "
				+ "WHERE dis_identifiant = '"
				+ identifiant
				+ "' "
				+ "ORDER BY dis_identifiant";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// en principe, il n'y a qu'un seul enregistrement
		if (!rs.first())
			throw new Exception(Erreur.I1001);

		// En principe, il y a un et un seul enregistrement
		// Lecture des champs dans le ResultSet
		Date dateCreation = rs.getDate("dis_date_creation");
		Date dateSuppression = rs.getDate("dis_date_suppression");
		String commentaires = rs.getString("dis_commentaires");

		int dif_identifiant = rs.getInt("dic_dif_identifiant");
		String dif_code = rs.getString("dif_code");
		DF df = new DF(new Integer(dif_identifiant), dif_code);

		String dic_code = rs.getString("dic_code");

		String type_DC = rs.getString("type_DC");
		RefTypeDC rtdc = new RefTypeDC(type_DC);

		((DC) _objet).setCode(dic_code);
		((DC) _objet).setDateCreation(dateCreation);
		((DC) _objet).setDateSuppression(dateSuppression);
		((DC) _objet).setCommentaires(commentaires);
		((DC) _objet).setDF(df);
		((DC) _objet).setType(rtdc);
	}

	// /////////////////////////////////////
	// operations
	// /////////////////////////////////////

	/**
	 * 
	 * @param code
	 * @param libelle
	 */
	public void chargeFiltre(String code, String libelle) {
		// A FAIRE
	} // end chargeFiltre

} // end ListeStations

