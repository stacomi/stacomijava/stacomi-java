/*
 **********************************************************************
 *
 * Nom fichier :        Infrastructure.java
 * Projet :             Controle migrateurs 2007
 * Organisme :          IAV
 * Auteur :             Sebastien Laigre
 * Contact :           cedric.briand@lavilaine.com
 * Date de creation :   
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package infrastructure.referenciel;

//package infrastructure.referenciel;

import infrastructure.Station;
import infrastructure.StationMesure;
import migration.referenciel.RefParametre;
import migration.referenciel.RefParametreQualitatif;
import migration.referenciel.RefParametreQuantitatif;

import java.sql.ResultSet;

import commun.ConnexionBD;
import commun.Erreur;
import commun.SousListe;

/**
 * Sous liste permettant de r�cup�rer les stations de mesure rattach�es � une station
 * @author S�bastien Laigre
 */
/**
 * @author cedric.briand
 *
 */
/**
 * @author cedric.briand
 *
 */
public class SousListeStationMesure extends SousListe {

	/** */
	private static final long serialVersionUID = 1L;

	/**
	 * Construit une sousliste
	 */
	public SousListeStationMesure() {
		super();
	} // end sousListeStationMesure

	/**
	 * Construit une sousliste avec un objet de rattachement
	 * 
	 * @param _objetDeRattachement
	 *            l'objet de rattachement
	 */
	public SousListeStationMesure(Station _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListeStationMesure
	    




	/** 
	 * Charge une station de mesure avec tous ses d�tails
	 * @see commun.SousListe#chargeObjet(java.lang.Object)
	 */
	 @Override
	protected void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		String sql = "SELECT stm_code, stm_libelle, stm_sta_code, stm_par_code, stm_description " +
				"FROM " + ConnexionBD.getSchema() + "tj_stationmesure_stm "
				+ "WHERE stm_sta_code='" + ((StationMesure) _objet).getIdentifiant() + "'";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// en principe, il n'y a qu'un seul enregistrement
		if(!rs.first())
			throw new Exception(Erreur.I1001);
		
		Integer stm_code = rs.getInt("stm_identifiant");
		String stm_libelle = rs.getString("stm_libelle");
		String stm_sta_code = rs.getString("stm_sta_code");
		String mes_description = rs.getString("stm_description");
		String stm_par_code = rs.getString("stm_par_code");

		// le code et libelle
		((StationMesure) _objet).setCode(stm_code);
		((StationMesure) _objet).setLibelle(stm_libelle);

		// la station
		Station sta = new Station();
		sta.setCode(stm_sta_code);
		((StationMesure) _objet).setStation(sta);

		// le parametre
		if(RefParametre.estQualitatif(stm_par_code))
			((StationMesure)_objet).setParametre(new RefParametreQuantitatif(stm_par_code));
		else
			((StationMesure)_objet).setParametre(new RefParametreQualitatif(stm_par_code));

		// les commentaires
		((StationMesure) _objet).setCommentaires(mes_description);
	}


	/** 
	 * Charge sans filtre charge les identifiants de toutes les stations de mesure
	 * rattach�es � une station
	 * @see commun.SousListe#chargeObjet(java.lang.Object)
	 */	
	 @Override
	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		String sql = "SELECT stm_code " +
				"FROM " + ConnexionBD.getSchema() + "tj_stationmesure_stm "
				+ "WHERE stm_sta_code='"
				+ ((Station) getObjetDeRattachement()).getCode() + "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while (rs.next()) {
			Integer stm_code = rs.getInt("stm_code");
			StationMesure mesure = new StationMesure(stm_code);
			this.put(stm_code, mesure);
		}
	}



	/** 
	 * Charge une station de mesure avec tous ses d�tails y compris le nom
	 * du param�tre rattach� � la station de mesure
	 * @see commun.SousListe#chargeSansFiltreDetails()
	 */	
	@Override
 public void chargeSansFiltreDetails() throws Exception {
		ResultSet rs = null;

		String sql = "SELECT stm_identifiant, stm_libelle, stm_sta_code, stm_par_code, stm_description, par_nom "
				+ "FROM " + ConnexionBD.getSchema() + "tj_stationmesure_stm JOIN ref.tg_parametre_par par ON stm_par_code=par.par_code "
				+ "WHERE stm_sta_code='"
				+ ((Station) getObjetDeRattachement()).getCode() + "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while (rs.next()) 
		{
			Integer stm_code = rs.getInt("stm_identifiant");
			String stm_libelle = rs.getString("stm_libelle");
			String stm_sta_code = rs.getString("stm_sta_code");
			String stm_par_code = rs.getString("stm_par_code");
			String stm_commentaires = rs.getString("stm_description");
			String par_nom = rs.getString("par_nom");

			RefParametre param;
			if(RefParametre.estQualitatif(stm_par_code))
				param = new RefParametreQualitatif(stm_par_code);
			else
				param = new RefParametreQuantitatif(stm_par_code);
			param.setLibelle(par_nom);
			
			Station station = new Station();
			station.setCode(stm_sta_code);
			StationMesure mesure = new StationMesure(stm_code, stm_libelle, station, param, stm_commentaires);
			this.put(stm_code, mesure);
		}
	}

	
	/** 
	 * Retourne le code de l'objet de rattachement de la station de mesure
	 * 
	 */	
	public String[] getObjetDeRattachementID() {
		String[] res = new String[1];
		res[0]= ((Station)super.objetDeRattachement).getCode();
		return res;
	}
}
