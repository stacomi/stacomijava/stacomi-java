/*
 **********************************************************************
 *
 * Nom fichier :        SousListeOuvrages.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package infrastructure.referenciel;

import java.sql.ResultSet;
import java.util.Date;

import commun.ConnexionBD;
import commun.Erreur;
import commun.SousListe;
import infrastructure.Dispositif;
import infrastructure.PeriodeFonctionnement;

/**
 * Sous-liste pour les periodes de fonctionnement et d'arret d'un dispositif
 * 
 * @author C�dric Briand
 */
public class SousListePeriodes extends SousListe {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/** */
	private static final long serialVersionUID = 5819094873408561907L;

	/**
	 * Construit une sousliste
	 */
	public SousListePeriodes() {
		super();
	} // end sousListePeriodes

	/**
	 * Construit une sousliste avec un objet de rattachement (i.e 1 dispositif)
	 * 
	 * @param _objetDeRattachement
	 *            l'objet de rattachement
	 */
	public SousListePeriodes(Dispositif _objetDeRattachement) {
		super(_objetDeRattachement);
	} // end sousListePeriodes

	///////////////////////////////////////
	// operation heritees
	///////////////////////////////////////

	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;
		// Extraction de tous les enregistrements de la table concernee pour
		// l'ouvrage donn�
		String sql = "SELECT per_dis_identifiant, per_date_debut, per_date_fin, per_etat_fonctionnement " + "FROM "
				+ ConnexionBD.getSchema() + "t_periodefonctdispositif_per " + "WHERE dif_ouv_identifiant='"
				+ this.getObjetDeRattachementID()[0] + "' ORDER BY per_dis_identifiant ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {
			// Lecture des champs dans le ResultSet
			Integer identifiant = new Integer(rs.getInt("per_dis_identifiant"));
			Date dateDebut = new Date(rs.getTimestamp("per_date_debut").getTime());
			Date dateFin = new Date(rs.getTimestamp("per_date_fin").getTime());
			boolean etat = rs.getBoolean("per_etat_fonctionnement");

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			PeriodeFonctionnement pf = new PeriodeFonctionnement(dateDebut, dateFin, new Boolean(etat));
			pf.setDispositif((Dispositif) super.objetDeRattachement);

			this.put(identifiant, pf);
		} // end while
	}

	/**
	 * Charge dans la sous-liste la derniere periode de fonctionnement
	 */
	public void chargeFiltreDerniere() {

		// A FAIRE v0.4

	} // end chargeFiltreDerniere

	public void chargeSansFiltreDetails() {
		// A FAIRE
	}

	protected void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		// Si l'identifiant de l'objet n'est pas definit, erreur
		Integer identifiant = ((Dispositif) _objet).getIdentifiant();
		if (identifiant == null) {
			throw new NullPointerException(Erreur.I1000);
		}
		// Extraction de l'enregistrement concerne
		// cle unique dif_ouv_identifiant dif_code
		String sql = "SELECT per_dis_identifiant, per_date_debut, per_date_fin, per_commentaires, per_etat_fonctionnement, per_tar_code "
				+ "FROM " + ConnexionBD.getSchema() + "t_periodefonctdispositif_per per "
				+ "WHERE per_dis_identifiant= '" + identifiant + "' " + "ORDER BY per_dis_identifiant ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// en principe, il n'y a qu'un seul enregistrement
		if (!rs.first())
			throw new Exception(Erreur.I1001);

		// Lecture des champs dans le ResultSet
		((PeriodeFonctionnement) _objet).setDateDebut(new Date(rs.getTimestamp("per_date_debut").getTime()));
		((PeriodeFonctionnement) _objet).setDateFin(new Date(rs.getTimestamp("per_date_fin").getTime()));
		((PeriodeFonctionnement) _objet).setCommentaires(rs.getString("dis_commentaires"));
		((PeriodeFonctionnement) _objet).setEtat(new Boolean(rs.getBoolean("per_etat_fonctionnement")));
		RefTypeFonctionnement rtf = new RefTypeFonctionnement(rs.getString("per_tar_code"));
		((PeriodeFonctionnement) _objet).setType(rtf);
		((PeriodeFonctionnement) _objet).setDispositif((Dispositif) super.objetDeRattachement);
	}

	/**
	 * Retourne l'identifiant du Dispositif
	 * 
	 * @return Tableau de taille 1 : (0) identifiant
	 */
	public String[] getObjetDeRattachementID() {

		// L'identifiant de Ouvrage est : identifiant
		String ret[] = new String[1];
		ret[0] = ((Dispositif) super.objetDeRattachement).getIdentifiant().toString();

		return ret;
	}

	/**
	 * Teste si la p�riode (dispositif, datedebut, datefin existe dans la base)
	 * 
	 * @param une
	 *            p�riode de fonctionnement
	 * @return boolean true si la p�riode existe
	 * @author cedric.briand
	 * @throws Exception
	 */
	public static boolean exists(PeriodeFonctionnement pf) throws Exception {
		ResultSet rs = null;
		// Extraction de tous les enregistrements de la table concernee pour
		// l'ouvrage donn�
		String sql = "SELECT per_dis_identifiant" + " FROM " + ConnexionBD.getSchema() + "t_periodefonctdispositif_per "
				+ "WHERE per_dis_identifiant='" + pf.getDispositif().getIdentifiant() + "' AND per_date_debut='"
				+ pf.getDateDebut() + "' AND per_date_fin='" + pf.getDateFin() + "';";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		if (rs.next()) {
			return (true);
		} else {
			return (false);
		}
	}

	/**
	 * Charge dans la liste les infos de la derniere operations d'un dc, en se
	 * basant sur la date de fin la plus elevee
	 * 
	 * @author C�dric
	 * @param _dis
	 *            le dispositif *
	 * @return l'identifiant de l'operation qui a ete chargee ou Null s'il n'y a
	 *         aucune operation
	 * @throws Exception
	 */
	public static PeriodeFonctionnement chargeFiltreDerniere(Dispositif _dis) throws Exception {
		PeriodeFonctionnement per = null;
		ResultSet rs = null;
		Date datedebut;
		Date datefin;
		boolean etat;
		RefTypeFonctionnement ref;
		String sql = "SELECT per_dis_identifiant, " + "per_date_debut," + " per_date_fin, "
				+ "per_etat_fonctionnement, " + "per_tar_code " + "FROM " + ConnexionBD.getSchema()
				+ "t_periodefonctdispositif_per per " + "WHERE per_dis_identifiant= '"
				+ _dis.getIdentifiant().toString() + "' " + " ORDER BY per_date_fin DESC;";
		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
		boolean isEmpty = !rs.first();
		if (!isEmpty) {
			rs.first(); // le premier = derni�re date
			datedebut = new Date(rs.getTimestamp("per_date_debut").getTime());
			datefin = new Date(rs.getTimestamp("per_date_fin").getTime());
			etat = rs.getBoolean("per_etat_fonctionnement");
			ref = new RefTypeFonctionnement(rs.getString("per_tar_code"));
			per = new PeriodeFonctionnement(_dis, datedebut, datefin, null, etat, ref);
		} else {
			per = null;
		}
		return per;
	} // end chargeFiltreDerniere

} // end SousListePeriodes
