/*
 **********************************************************************
 *
 * Nom fichier :        ListeOuvrage.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package infrastructure.referenciel;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.sql.ResultSet;

import javax.imageio.ImageIO;

import commun.ConnexionBD;
import commun.Erreur;
import commun.Liste;
import infrastructure.Ouvrage;
import infrastructure.Station;

/**
 * Liste des ouvrages
 * 
 * @author C�dric Briand
 */
public class ListeOuvrage extends Liste {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Charge dans la liste les infos concernant le dernier ouvrage rentr�, en
	 * se basant sur l'identifiant le plus �lev�
	 * 
	 * @return l'identifiant de l'operation qui a ete chargee ou Null s'il n'y a
	 *         aucune operation
	 * @throws Exception
	 */
	public Integer chargeFiltreDerniere() throws Exception {
		Integer ret = this.chargeFiltreDerniere(null);
		return ret;
	} // end chargeFiltreDerniere

	/**
	 * Charge dans la liste les infos concernant le dernier ouvrage d'une
	 * station, en se basant sur l'identifiant le plus �lev� TODO A TESTER : Non
	 * fonctionnnelle...
	 * 
	 * @param _sta
	 *            la station
	 * @return l'identifiant de l'ouvrage ou Null s'il n'y a aucun ouvrage
	 * @throws Exception
	 */
	public Integer chargeFiltreDerniere(Station _sta) throws Exception {
		Integer ret = null;
		Ouvrage ouv = null;
		ResultSet rs = null;

		String complementWhere = "";

		if (_sta != null) {
			complementWhere = " ouv_sta_code='" + _sta.getCode().toString() + "' ";
		}

		// Extraction de l'enregistrement concerne
		// avec ou sans le compl�ment where
		String sql = "SELECT ouv_identifiant, ouv_sta_code, ouv_code, ouv_libelle, ouv_localisation, ouv_coordonnee_x, ouv_coordonnee_y, ouv_altitude,ouv_carte_localisation,ouv_commentaires,ouv_nov_code "
				+ "FROM " + ConnexionBD.getSchema() + "t_ouvrage_ouv "
				+ "WHERE ouv_identifiant = (SELECT MAX(ope_date_fin) " + complementWhere + " FROM "
				+ ConnexionBD.getSchema() + "t_operation_ope) ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Lecture du premier enregistrement (en principe, il y a 0 ou 1
		// enregistrement)
		// si plus d'un tuple trouve, on ne tient compte que du premier

		if (rs.next()) {

			// Lecture des champs dans le ResultSet ;
			Integer identifiant = new Integer(rs.getInt("ouv_identifiant"));
			Station station = new Station();
			station.setCode(new String(rs.getString("ouv_sta_code")));
			// essai je passe par le setter
			// Pour la date, utilisation de timestamp, sinon ne recupere pas les
			// heures, minutes, secondes
			// autre possibilite, mais deconseille : Date dateDebut=
			// rs.getTimestamp("ope_date_debut") ;
			String code = rs.getString("ouv_code");
			String libelle = rs.getString("ouv_libelle");
			String localisation = rs.getString("ouv_localisation");
			Integer coordonnes_x = new Integer(rs.getInt("ouv_coordonnee_x"));
			Integer coordonnes_y = new Integer(rs.getInt("ouv_coordonnee_y"));
			Short ouv_altitude = new Short(rs.getShort("ouv_altitude"));
			InputStream inCarteLocalisation = rs.getBinaryStream("sta_carte_localisation"); // flux
																							// temporaire
																							// pour
																							// la
																							// lecture
																							// de
																							// l'image
			BufferedImage carteLocalisation = ImageIO.read(inCarteLocalisation);
			String commentaires = rs.getString("ope_commentaires");
			RefNatureOuvrage ouv_nov_code = new RefNatureOuvrage(rs.getString("ouv_nov_code"));

			// Initialisation de l'objet d'apres les champs lus et ajout a la
			// liste
			ouv = new Ouvrage(identifiant, station, code, libelle, localisation, coordonnes_x, coordonnes_y,
					ouv_altitude, carteLocalisation, commentaires, ouv_nov_code);
			this.put(identifiant, ouv);

			ret = identifiant;
		} else {
			ret = null;
		}

		return ret;
	} // end chargeFiltreDerniere

	protected void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		Integer id = ((Ouvrage) _objet).getIdentifiant();
		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT ouv_identifiant, ouv_sta_code, ouv_code, ouv_libelle, ouv_localisation, ouv_coordonnee_x, ouv_coordonnee_y, ouv_altitude, ouv_commentaires, ouv_nov_code "
				+ "FROM " + ConnexionBD.getSchema() + "t_ouvrage_ouv " + "WHERE ouv_identifiant = '" + id + "' ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// En principe, il y a un et un seul enregistrement
		if (!rs.first())
			throw new Exception(Erreur.I1001);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		// Lecture des champs dans le ResultSet ;
		Integer identifiant = new Integer(rs.getInt("ouv_identifiant"));
		Station station = new Station();
		station.setCode(new String(rs.getString("ouv_sta_code")));
		String code = rs.getString("ouv_code");
		String libelle = rs.getString("ouv_libelle");
		String localisation = rs.getString("ouv_localisation");
		Integer coordonnes_x = new Integer(rs.getInt("ouv_coordonnee_x"));
		Integer coordonnes_y = new Integer(rs.getInt("ouv_coordonnee_y"));
		Short ouv_altitude = new Short(rs.getShort("ouv_altitude"));
		String commentaires = rs.getString("ouv_commentaires");
		RefNatureOuvrage ouv_nov_code = new RefNatureOuvrage(rs.getString("ouv_nov_code"));

		((Ouvrage) _objet).setAltitude(ouv_altitude);
		((Ouvrage) _objet).setCode(code);
		((Ouvrage) _objet).setCommentaires(commentaires);
		((Ouvrage) _objet).setCoordonneeX(coordonnes_x);
		((Ouvrage) _objet).setCoordonneeY(coordonnes_y);
		((Ouvrage) _objet).setIdentifiant(identifiant);
		((Ouvrage) _objet).setLibelle(libelle);
		((Ouvrage) _objet).setLocalisation(localisation);
		((Ouvrage) _objet).setNature(ouv_nov_code);
	}

	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		String sql = "SELECT ouv_identifiant, ouv_code, ouv_libelle FROM t_ouvrage_ouv ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		while (rs.next()) {
			Integer ouv_identifiant = rs.getInt("ouv_identifiant");
			String ouv_code = rs.getString("ouv_code");
			String ouv_libelle = rs.getString("ouv_libelle");
			Ouvrage o = new Ouvrage(ouv_identifiant, ouv_code, ouv_libelle);
			this.put(o.getIdentifiant(), o);
		}
	}

	public void chargeSansFiltreDetails() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT ouv_identifiant, ouv_sta_code, ouv_libelle, ouv_localisation, ouv_coordonnee_x, ouv_coordonnee_y, ouv_altitude,  ouv_commentaires, ouv_nov_code "
				+ "FROM " + ConnexionBD.getSchema() + "t_ouvrage_ouv ;";
		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {
			// Lecture des champs dans le ResultSet ;
			Integer identifiant = new Integer(rs.getInt("ouv_identifiant"));
			Station station = new Station();
			station.setCode(new String(rs.getString("ouv_sta_code")));
			String code = rs.getString("ouv_code");
			String libelle = rs.getString("ouv_libelle");
			String localisation = rs.getString("ouv_localisation");
			Integer coordonnes_x = new Integer(rs.getInt("ouv_coordonnee_x"));
			Integer coordonnes_y = new Integer(rs.getInt("ouv_coordonnee_y"));
			Short ouv_altitude = new Short(rs.getShort("ouv_altitude"));
			String commentaires = rs.getString("ouv_commentaires");
			RefNatureOuvrage ouv_nov_code = new RefNatureOuvrage(rs.getString("ouv_nov_code"));

			Ouvrage o = new Ouvrage(identifiant, station, code, libelle, localisation, coordonnes_x, coordonnes_y,
					ouv_altitude, null, commentaires, ouv_nov_code);
			this.put(code, o);
		}
	}
}
