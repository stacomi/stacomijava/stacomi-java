/*
 **********************************************************************
 *
 * Nom fichier :        RefTypeFonctionnement.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package infrastructure.referenciel;

import java.util.ArrayList;

import commun.* ;


/**
 * Type de periode de fonctionnement ou d'arret
 * @author C�dric Briand
 */
public class RefTypeFonctionnement extends Ref {
    
    
  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////
    
    
    
  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    
    
/**
 * Construit une reference avec un code mais sans libelle
 * @param _code le code
 */    
    public RefTypeFonctionnement(String _code) {
        super(_code, null) ;
    } // end RefTypeFonctionnement
        
    
/**
 * Construit une reference avec un code et un libelle
 * @param _code le code
 * @param _libelle le libelle
 */    
    public RefTypeFonctionnement(String _code, String _libelle) {
        super(_code, _libelle) ;
    } // end RefTypeFonctionnement
    
   
    
    
    
  ///////////////////////////////////////
  // operations heritees
  ///////////////////////////////////////
    
    public void insertObjet() {
        super.insertObjet("ref.tr_typearretdisp_tar") ;
    }
    
    
    public void majObjet() {
        super.majObjet("ref.tr_typearretdisp_tar") ;
    }


	/**
	 * @return le nom de la table concern�e
	 */
	public static String getNomTable() {
		return "ref.tr_typearretdisp_tar";
	}

	/**
	 * @return le nom des attributs concern�s
	 */
	public static ArrayList<String> getNomAttributs() {
		ArrayList<String> res = new ArrayList<String>();
		res.add("tar_code");
		res.add("tar_libelle");
		return null;
	}
	
    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////    
    
    
    
    
 } // end RefTypeFonctionnement



