/*
 **********************************************************************
 *
 * Nom fichier :        ListeDF.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             S�bastien Laigre
 * Date de creation :   20 avril 2009
 * Etat :               a tester et a valider
 ********************************************************************** 
 */

package infrastructure.referenciel;

import java.sql.ResultSet;
import java.util.Date;

import commun.ConnexionBD;
import commun.Erreur;
import commun.Lanceur;
import commun.Liste;
import infrastructure.DF;
import infrastructure.Ouvrage;
import migration.referenciel.ListeRefTaxon;
import migration.referenciel.RefTaxon;

/**
 * Liste pour les DF d�velopp�e par s�bastien a v�rifier
 * 
 * @author S�bastien Laigre
 */
public class ListeDF extends Liste {

	// /////////////////////////////////////
	// attributes
	// /////////////////////////////////////

	// /////////////////////////////////////
	// operations heritees
	// /////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 8657958250337800070L;

	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT dif_dis_identifiant, dif_code " + "FROM " + ConnexionBD.getSchema()
				+ "t_dispositiffranchissement_dif " + "ORDER BY dif_dis_identifiant ;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			int dif_identifiant = rs.getInt("dif_dis_identifiant");
			String dif_code = rs.getString("dif_code");

			// Creation de l'objet d'apres les champs lus et ajout a la liste
			DF df = new DF(new Integer(dif_identifiant), dif_code);
			this.put(dif_code, df);
		} // end while
	}

	public void chargeSansFiltreDetails() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String sql = "SELECT dif_dis_identifiant, dif_ouv_identifiant, dis_date_creation, dis_date_suppression, dis_commentaires ,  dif_code, dif_localisation,  dif_orientation ,dif_code, dis_commentaires "
				+ "FROM " + ConnexionBD.getSchema() + "t_dispositiffranchissement_dif dif" + "JOIN "
				+ ConnexionBD.getSchema() + "tg_dispositif_dis dis ON dis.dis_identifiant=dif.dif_dis_identifiant "
				+ "ORDER BY dif_dis_identifiant;";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {

			// Lecture des champs dans le ResultSet
			int identifiant = rs.getInt("dif_dis_identifiant");
			int id_ouvrage = rs.getInt("dif_ouv_identifiant");
			Ouvrage ouvrage = new Ouvrage(new Integer(id_ouvrage), null, null);

			String code = rs.getString("dif_code");
			String localisation = rs.getString("dif_localisation");
			String orientation = rs.getString("dif_orientation");

			Date dateCreation = rs.getDate("dis_date_creation");
			Date dateSuppression = rs.getDate("dis_date_suppression");
			String commentaires = rs.getString("dis_commentaires");

			DF df = new DF(new Integer(identifiant), dateCreation, dateSuppression, commentaires, null, ouvrage, code,
					localisation, orientation, null, null, null);
			this.put(new Integer(identifiant), df);

		} // end while
	}

	public void chargeObjet(Object _objet) throws Exception {

		ResultSet rs = null;

		// Si l'identifiant de l'objet n'est pas definit, erreur
		Integer identifiant = ((DF) _objet).getIdentifiant();
		if (identifiant == null) {
			throw new NullPointerException(Erreur.I1000);
		}
		// Extraction de l'enregistrement concerne
		// cle unique dif_ouv_identifiant dif_code
		String sql = "SELECT dis_identifiant as DF, dis_date_creation, dis_date_suppression, "
				+ "dis_commentaires, dif_ouv_identifiant, ouv_code, ouv_libelle, dif_code as DF_code, "
				+ "dif_localisation, dif_orientation, tax_code as type_Taxon, tdf_code as type_DF " + "FROM "
				+ ConnexionBD.getSchema() + "tg_dispositif_dis " + "JOIN " + ConnexionBD.getSchema()
				+ "t_dispositiffranchissement_dif ON dif_dis_identifiant=dis_identifiant " + "JOIN "
				+ ConnexionBD.getSchema() + "tj_dfesttype_dft ON dif_dis_identifiant=dft_df_identifiant "
				+ "JOIN ref.tr_typedf_tdf ON tdf_code=dft_tdf_code " + "JOIN " + ConnexionBD.getSchema()
				+ "t_ouvrage_ouv ON dif_ouv_identifiant=ouv_identifiant " + "JOIN " + ConnexionBD.getSchema()
				+ "tj_dfestdestinea_dtx ON dif_dis_identifiant=dtx_dif_identifiant "
				+ "JOIN ref.tr_taxon_tax ON tax_code=dtx_tax_code " + "WHERE dis_identifiant = '" + identifiant + "' "
				+ "ORDER BY dis_identifiant";

		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

		// en principe, il n'y a qu'un seul enregistrement
		if (!rs.first())
			throw new Exception(Erreur.I1001);

		// En principe, il y a un et un seul enregistrement
		Date dateCreation = rs.getDate("dis_date_creation");
		Date dateSuppression = rs.getDate("dis_date_suppression");
		String commentaires = rs.getString("dis_commentaires");
		int ouv_identifiant = rs.getInt("dif_ouv_identifiant");
		String ouv_code = rs.getString("ouv_libelle");
		String ouv_libelle = rs.getString("ouv_libelle");
		String code = rs.getString("DF_code");
		String localisation = rs.getString("dif_localisation");
		String orientation = rs.getString("dif_orientation");
		String tdf_code = rs.getString("type_DF");
		String tax_code = rs.getString("type_Taxon");

		Ouvrage ouv = new Ouvrage(new Integer(ouv_identifiant), ouv_code, ouv_libelle);

		RefTypeDF refTypeDF = new RefTypeDF(tdf_code);
		ListeRefTypeDF listeRefTypeDF = new ListeRefTypeDF();
		listeRefTypeDF.put(refTypeDF.getCode(), refTypeDF);

		RefTaxon refTaxon = new RefTaxon(tax_code);
		ListeRefTaxon listeRefTaxon = new ListeRefTaxon(Lanceur.getListeRefNiveauTaxonomique());
		listeRefTaxon.put(tax_code, refTaxon);

		// Lecture des champs dans le ResultSet
		((DF) _objet).setCode(code);
		((DF) _objet).setDateCreation(dateCreation);
		((DF) _objet).setDateSuppression(dateSuppression);
		((DF) _objet).setCommentaires(commentaires);
		((DF) _objet).setLocalisation(localisation);
		((DF) _objet).setOrientation(orientation);
		((DF) _objet).setOuvrage(ouv);
		((DF) _objet).setLesTypesDeDF(listeRefTypeDF);
		((DF) _objet).setLesTaxonsDeDestination(listeRefTaxon);
	}

	// /////////////////////////////////////
	// operations
	// /////////////////////////////////////

	/**
	 * 
	 * @param code
	 * @param libelle
	 */
	public void chargeFiltre(String code, String libelle) {
		// A FAIRE
	} // end chargeFiltre

} // end ListeStations
