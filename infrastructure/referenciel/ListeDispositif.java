/**
 * 
 */
package infrastructure.referenciel;

import java.util.Date;

import infrastructure.Dispositif;

import java.sql.ResultSet;

import commun.ConnexionBD;
import commun.Liste;
import infrastructure.*;

/**
 * Liste des dispositifs
 * @author S�bastien Laigre
 */
public class ListeDispositif extends Liste {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	///////////////////////////////////////
	// operations heritees
	///////////////////////////////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Charge dans la liste les infos concernant le dernier ouvrage rentr�, en se basant sur l'identifiant le plus �lev�
	 * @return l'identifiant de l'operation qui a ete chargee ou Null s'il n'y a aucune operation
	 * @throws Exception 
	 */
	public Integer chargeFiltreDerniere() throws Exception {
		Integer ret = this.chargeFiltreDerniere(null);
		return ret;
	} // end chargeFiltreDerniere        

	/**
	 * Charge dans la liste les infos concernant le dernier ouvrage d'une station, en se basant sur l'identifiant le plus �lev�
	 * @param _d dispositif
	 * @return l'identifiant de l'ouvrage ou Null s'il n'y a aucun ouvrage
	 * @throws Exception 
	 */
	public Integer chargeFiltreDerniere(Dispositif _d) throws Exception {
		// TODO A impl�menter
		return new Integer(0);
	} // end chargeFiltreDerniere        

	public void chargeObjet(Object _objet) throws Exception {
		ResultSet rs = null;

		int id = ((Dispositif) _objet).getIdentifiant();
		// Extraction de toutes les enregistrements de la table concernee
		String infoDispositif = "SELECT dis_identifiant, dis_date_creation, dis_date_suppression, dis_commentaires " +
				"FROM " + ConnexionBD.getSchema() + "tg_dispositif_dis WHERE dis_identifiant ='"
				+ id + "';";
		rs = ConnexionBD.getInstance().getStatement().executeQuery(
				infoDispositif);

		// 1 seul enregistrement normalement...
		if (rs.next()) {
			// lecture des champs
			Integer identifiant = new Integer(rs.getInt("dis_identifiant"));
			Date dateCreation = rs.getDate("dis_date_creation");
			Date dateSuppression = rs.getDate("dis_date_suppression");
			String commentaires = rs.getString("dis_commentaires");

			String periodeFonct = "SELECT * "
					+ "FROM " + ConnexionBD.getSchema() + "t_periodefonctdispositif_per "
					+ "WHERE per_dis_identifiant='" + identifiant + "' ;";
			ResultSet rs2 = ConnexionBD.getInstance().getStatement()
					.executeQuery(periodeFonct);

			SousListePeriodes sousListePeriode = new SousListePeriodes();

			// on parcourt l'ensemble des p�riodes pour les ajouter au dispositif
			while (rs2.next()) {
				Date dateDeb = new Date(rs2.getTimestamp("per_date_debut")
						.getTime());
				Date dateFin = new Date(rs2.getTimestamp("per_date_fin")
						.getTime());
				boolean etat = rs2.getBoolean("per_etat_fonctionnement");
				String etat_code = rs2.getString("per_tar_code");
				String commentairesPeriode = rs2.getString("per_commentaires");
				RefTypeFonctionnement refTypeFonctionnement = new RefTypeFonctionnement(
						etat_code);
				PeriodeFonctionnement pf = new PeriodeFonctionnement(null,
						dateDeb, dateFin, commentairesPeriode,
						new Boolean(etat), refTypeFonctionnement);
				sousListePeriode.put(identifiant.toString()
						+ dateDeb.toString() + dateFin.toString(), pf);
			}

			// on teste si c'est un DF ou 1 DC
			if (Dispositif.estUnDf(identifiant)) // si c'est un DF...
			{
				// Extraction de toutes les enregistrements de la table concernee
				_objet = new DF(identifiant, null);
				ListeDF ldf = new ListeDF();
				ldf.chargeObjet(_objet);
			} else // sinon c'est un DC
			{
				_objet = new DC(identifiant);
				ListeDC ldc = new ListeDC();
				ldc.chargeObjet(_objet);

			}
			// on fixe les informations qui sont propres au dispositif
			((Dispositif) _objet).setCommentaires(commentaires);
			((Dispositif) _objet).setDateCreation(dateCreation);
			((Dispositif) _objet).setDateSuppression(dateSuppression);

			// parcours du vecteur pour mettre � jour les p�riodes le dispositif... (car il �tait impossible de le faire + haut)
			for (int i = 0; i < sousListePeriode.size(); i++)
				((PeriodeFonctionnement) sousListePeriode.get(i))
						.setDispositif(((Dispositif) _objet));

			// mise a jour des p�riodes de fonctionnement
			((Dispositif) _objet).setLesPeriodes(sousListePeriode);
			_sousListePeriode = sousListePeriode;
		}
	}

	SousListePeriodes _sousListePeriode;

	/**
	 * @return la sous-liste des p�riodes de fonctionnement du dispositif
	 */
	public SousListePeriodes getSousListePeriode() {
		return _sousListePeriode;
	}

	public void chargeSansFiltre() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		String infoDispositif = "SELECT dis_identifiant, dis_date_creation, dis_date_suppression, dis_commentaires " +
				"FROM " + ConnexionBD.getSchema() + "tg_dispositif_dis ;";
		rs = ConnexionBD.getInstance().getStatement().executeQuery(
				infoDispositif);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {
			// lecture des champs
			Integer identifiant = new Integer(rs.getInt("dis_identifiant"));
			Date dateCreation = rs.getDate("dis_date_creation");
			Date dateSuppression = rs.getDate("dis_date_suppression");
			String commentaires = rs.getString("dis_commentaires");

			String periodeFonct = "SELECT * "
					+ "FROM " + ConnexionBD.getSchema() + "t_periodefonctdispositif_per "
					+ "WHERE per_dis_identifiant='" + identifiant + "' ;";
			ResultSet rs2 = ConnexionBD.getInstance().getStatement()
					.executeQuery(periodeFonct);

			SousListePeriodes sousListePeriode = new SousListePeriodes();

			// on parcourt l'ensemble des p�riodes pour les ajouter au dispositif
			while (rs2.next()) {
				Date dateDeb = new Date(rs2.getTimestamp("per_date_debut")
						.getTime());
				Date dateFin = new Date(rs2.getTimestamp("per_date_fin")
						.getTime());
				boolean etat = rs2.getBoolean("per_etat_fonctionnement");
				String etat_code = rs2.getString("per_tar_code");
				String commentairesPeriode = rs2.getString("per_commentaires");
				RefTypeFonctionnement refTypeFonctionnement = new RefTypeFonctionnement(
						etat_code);
				PeriodeFonctionnement pf = new PeriodeFonctionnement(null,
						dateDeb, dateFin, commentairesPeriode,
						new Boolean(etat), refTypeFonctionnement);
				sousListePeriode.put(identifiant.toString()
						+ dateDeb.toString() + dateFin.toString(), pf);
			}

			Dispositif d = null;

			// on teste si c'est un DF ou 1 DC
			if (Dispositif.estUnDf(identifiant)) // si c'est un DF...
			{
				// Extraction de toutes les enregistrements de la table concernee
				d = new DF(identifiant, null);
				ListeDF ldf = new ListeDF();
				ldf.chargeObjet(d);
			} else // sinon c'est un DC
			{
				d = new DC(identifiant);
				ListeDC ldc = new ListeDC();
				ldc.chargeObjet(d);

			}
			// on fixe les informations qui sont propres au dispositif
			d.setCommentaires(commentaires);
			d.setDateCreation(dateCreation);
			d.setDateSuppression(dateSuppression);

			// parcours du vecteur pour mettre � jour les p�riodes le dispositif... (car il �tait impossible de le faire + haut)
			for (int i = 0; i < sousListePeriode.size(); i++)
				((PeriodeFonctionnement) sousListePeriode.get(i))
						.setDispositif(d);

			// mise a jour des p�riodes de fonctionnement
			d.setLesPeriodes(sousListePeriode);

			this.put(d.getIdentifiant(), d);
		}
	}

	public void chargeSansFiltreDetails() throws Exception {
		ResultSet rs = null;

		// Extraction de toutes les enregistrements de la table concernee
		// cedric : la requ�te ci dessous permet de charger dans l'ordre les DF puis les DC et chacun dans l'ordre alphab�tique
		String infoDispositif = 
			"SELECT dis_identifiant,  dis_date_creation, dis_date_suppression, dis_commentaires " +  
			"FROM 		(select      dis_identifiant,  dis_date_creation, dis_date_suppression, dis_commentaires, "+ 
			 "dif_code AS CODE, dif_dis_identifiant<=0 as dic "+
			 "FROM " + ConnexionBD.getSchema() + "tg_dispositif_dis   INNER JOIN t_dispositiffranchissement_dif ON dif_dis_identifiant=dis_identifiant "+
			 "UNION "+
			 "select      dis_identifiant, dis_date_creation, dis_date_suppression, dis_commentaires, "+ 
			 "dic_code as CODE, dic_dis_identifiant>0 as DIC "+
			"FROM " + ConnexionBD.getSchema() + "tg_dispositif_dis  INNER JOIN t_dispositifcomptage_dic ON dic_dis_identifiant=dis_identifiant) " +
			"AS requetecroisee "+
			"ORDER BY dic,code;" ;	
		rs = ConnexionBD.getInstance().getStatement().executeQuery(
				infoDispositif);

		// Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
		while (rs.next()) {
			// lecture des champs
			Integer identifiant = new Integer(rs.getInt("dis_identifiant"));
			Date dateCreation = rs.getDate("dis_date_creation");
			Date dateSuppression = rs.getDate("dis_date_suppression");
			String commentaires = rs.getString("dis_commentaires");

			String periodeFonct = "SELECT * "
					+ "FROM " + ConnexionBD.getSchema() + "t_periodefonctdispositif_per "
					+ "WHERE per_dis_identifiant='" + identifiant + "'"+
					"ORDER BY per_date_debut;";
			ResultSet rs2 = ConnexionBD.getInstance().getStatement()
					.executeQuery(periodeFonct);

			SousListePeriodes sousListePeriode = new SousListePeriodes();

			// on parcourt l'ensemble des p�riodes pour les ajouter au dispositif
			while (rs2.next()) {
				Date dateDeb = new Date(rs2.getTimestamp("per_date_debut")
						.getTime());
				Date dateFin = new Date(rs2.getTimestamp("per_date_fin")
						.getTime());
				boolean etat = rs2.getBoolean("per_etat_fonctionnement");
				String etat_code = rs2.getString("per_tar_code");
				String commentairesPeriode = rs2.getString("per_commentaires");
				RefTypeFonctionnement refTypeFonctionnement = new RefTypeFonctionnement(
						etat_code);
				PeriodeFonctionnement pf = new PeriodeFonctionnement(null,
						dateDeb, dateFin, commentairesPeriode,
						new Boolean(etat), refTypeFonctionnement);
				sousListePeriode.put(identifiant.toString()
						+ dateDeb.toString() + dateFin.toString(), pf);
			}

			Dispositif d = null;

			// on teste si c'est un DF ou 1 DC
			if (Dispositif.estUnDf(identifiant)) // si c'est un DF...
			{
				// Extraction de toutes les enregistrements de la table concernee
				d = new DF(identifiant, null);
				ListeDF ldf = new ListeDF();
				ldf.chargeObjet(d);
			} else // sinon c'est un DC
			{
				d = new DC(identifiant);
				ListeDC ldc = new ListeDC();
				ldc.chargeObjet(d);

			}
			// on fixe les informations qui sont propres au dispositif
			d.setCommentaires(commentaires);
			d.setDateCreation(dateCreation);
			d.setDateSuppression(dateSuppression);

			// parcours du vecteur pour mettre � jour les p�riodes le dispositif... (car il �tait impossible de le faire + haut)
			for (int i = 0; i < sousListePeriode.size(); i++)
				((PeriodeFonctionnement) sousListePeriode.get(i))
						.setDispositif(d);

			// mise a jour des p�riodes de fonctionnement
			d.setLesPeriodes(sousListePeriode);

			this.put(d.getIdentifiant(), d);
		}
	}
}
