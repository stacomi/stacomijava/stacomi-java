/**
 * 
 */
package infrastructure;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;
import migration.referenciel.RefParametre;

/**
 * Station de mesure d'une station de contr�le des migrations
 * La classse impl�mente Comparable<StationMesure>
 * Elle a une m�thode compareTo(StationMesure) qui se base sur les libell�s pour pouvoir ordonner les textes dans la JList
 * Par exemple dans la classe AMSMasqueOpe.java
 * @author S�bastien Laigre/ cedric
 */
public class StationMesure implements IBaseDonnees,  Comparable<StationMesure>{

	/** Son code */
	Integer identifiant;
	
	/** Son libell� */
	String libelle;
	
	/** La station */
	Station station;

	/** Le parametre */
	private RefParametre parametre;

	/** Commentaires */
	private String commentaires;

	/**
	 * Initialise 1 RefMesureEnvironnementale avec tous ses attributs
	 * @param code : son code
	 * @param libelle : libell�
	 * @param station : station de rattachement
	 * @param parametre : son param�tre
	 * @param commentaires : commentaires associ�s
	 */
	public StationMesure(Integer code, String libelle, Station station, RefParametre parametre, String commentaires)
	{
		this.station = station;
		this.libelle = libelle;
		this.identifiant = code;
		this.parametre = parametre;
		this.commentaires = commentaires;
	}
	
	/**
	 * Initialise 1 StationMesure avec son code seulement
	 * @param code : son code
	 */
	public StationMesure(Integer code)
	{
		this(code,null,null,null,"");
	}
	
	/**
	 * Suppression
	 */
	public void effaceObjet() throws SQLException, ClassNotFoundException 
	{
		String table = StationMesure.getNomTable();
		String[] nomID = StationMesure.getNomID();
		Object[] valeurID = this.getValeurID();

		ConnexionBD.getInstance().executeDelete(table, nomID, valeurID);
	}

	/**
	 * Insertion
	 */
	public void insertObjet() throws SQLException, ClassNotFoundException 
	{
		String table = StationMesure.getNomTable();
		ArrayList<String> noms = StationMesure.getNomAttributs();
		ArrayList<Object> valeurs = this.getValeurAttributs();
				
		ConnexionBD.getInstance().executeInsert(table, noms, valeurs);
		this.identifiant = ConnexionBD.getInstance().getGeneratedKey(StationMesure.getNomSequences()[0]);
	}

	/**
	 * Mise a jour
	 */
	public void majObjet() throws SQLException, ClassNotFoundException 
	{
		String table = StationMesure.getNomTable();
		
		String[] nomID = StationMesure.getNomID();
		Object[] valeurID = this.getValeurID();
		
		ArrayList<String> noms = StationMesure.getNomAttributs();
		ArrayList<Object> valeurs = this.getValeurAttributs();
		
		ConnexionBD.getInstance().executeUpdate(table, nomID, valeurID, noms, valeurs);
	}

	/**
	 * V�rification d'une StationMesure
	 * V�rifie le format de chacuns des attributs de l'objet courant
	 */
	public boolean verifAttributs() throws DataFormatException 
	{
		// v�rification de l'identifiant
		if(!Verification.isInteger(this.identifiant,false))
			throw new DataFormatException(Erreur.S15000);

		// v�rification du libell�
		if(!Verification.isText(this.libelle, 1, 12, false))
			throw new DataFormatException(Erreur.S15010);

		// v�rification de la station
		if(!Verification.isStation(this.getStation(), true))
			throw new DataFormatException(Erreur.S15003);

		// v�rification de code du parametre
		if(!Verification.isText(this.parametre.getCode(), 1, 8, true))
			throw new DataFormatException(Erreur.S15001);

		// v�rification de la description
		if(!Verification.isText(this.commentaires, 0, false))
			throw new DataFormatException(Erreur.S15002);

		return true;
	}

	/**
	 * Acc�s a la station
	 * @return la station
	 */
	public Station getStation()
	{
		return this.station;
	}
	
	/**
	 * Affecte la station
	 * @param station : la nouvelle station
	 */
	public void setStation(Station station)
	{
		this.station = station;
	}
	
	/**
	 * Acc�s au commentaire
	 * @return le commentaire
	 */
	public String getCommentaires()
	{
		return this.commentaires;
	}
	
	/**
	 * Affecte le commentaires
	 * @param commentaires : le commentaire
	 */
	public void setCommentaires(String commentaires)
	{
		this.commentaires = commentaires;
	}
	
	/**
	 * Acc�s au param�tre
	 * @return le param�tre
	 */
	public RefParametre getParametre()
	{
		return this.parametre;
	}
	
	/**
	 * Affecte le param�tre
	 * @param parametre
	 */
	public void setParametre(RefParametre parametre)
	{
		this.parametre = parametre;
	}
	
	/**
	 * Acc�s au code
	 * @return le code
	 */
	public Integer getIdentifiant()
	{
		return this.identifiant;
	}
	
	/**
	 * Affecte le code
	 * @param code
	 */
	public void setCode(Integer code)
	{
		this.identifiant = code;
	}
	
	/**
	 * Acc�s au libell�
	 * @return le libell�
	 */
	public String getLibelle()
	{
		return this.libelle;
	}
	
	/**
	 * @param libelle : nouveau libell�
	 */
	public void setLibelle(String libelle)
	{
		this.libelle = libelle;
	}
	
	/**
	 * Acc�s au nom de la table
	 * @return le nom de la table associ�e
	 */
	public static String getNomTable()
	{
		return "tj_stationmesure_stm";
	}
	
	/**
	 * Retourne le nom des attributs de la table correspondant a la classe 
	 * @return un Vector de String avec les attributs dans l'ordre de la table dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		// impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
		// est inconnu
		ArrayList<String> ret = new ArrayList<String>();
		ret.add("stm_identifiant");
		ret.add("stm_libelle");
		ret.add("stm_sta_code");
		ret.add("stm_par_code");
		ret.add("stm_description");
		ret.add("stm_org_code");
		
		return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de la classe
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table dans la BD
	 */
	public ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret = new ArrayList<Object>();
		ret.add(this.getIdentifiant());
		ret.add(this.getLibelle());
		ret.add(this.getStation().getCode());
		ret.add(this.getParametre().getCode());
		ret.add(this.getCommentaires());
		return ret;
	} // end getValeurAttributs
	
	/**
	 * Retourne le nom de l'identifiant caract�re de la table
	 * @return un vecteur de string de longueur 1 contenant le nom de l'identifiant de la table
	 */
	public static String[] getNomID() {
		String[] ret = new String[1];
		ret[0] = "stm_identifiant";
		return ret;
	} // end getNomID

	/**
	 * Retourne la valeur de l'indenfiant string de la classe
	 * @return un vecteur d'objet de longueur 1 contenant le code
	 */
	public Object[] getValeurID() {
		Object[] ret = new Object[1];
		ret[0] = this.getIdentifiant();
		return ret;
	} // end getValeurID
	
	
	/**
	 * Retourne le nom de la s�quence associ�e
	 * @return le nom de la s�quence associ�e
	 */
	public static String[] getNomSequences()
	{
		String[] res = {"tj_stationmesure_stm_stm_identifiant_seq"};
		return res;
	}
	
	public String toString()
	{
		return this.getLibelle();
	}
	/**
	 * M�thode de l'interface pour Comparable pour pouvoir utiliser la classe comme objet dans une JList
	 * @return le nom de la s�quence associ�e
	 */
	@Override
	public int compareTo(StationMesure o) {
		return this.libelle.compareTo(o.libelle);
	}
}
