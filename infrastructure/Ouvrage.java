/*
 **********************************************************************
 *
 * Nom fichier :        Ouvrage.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package infrastructure;

import java.awt.image.BufferedImage;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;
import infrastructure.referenciel.ListeStations;
import infrastructure.referenciel.RefNatureOuvrage;

/**
 * Ouvrage de la station de controle des migrations
 * 
 * @author C�dric Briand
 */
public class Ouvrage extends EmplacementGeographique implements IBaseDonnees {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	private Integer identifiant;

	private String code;

	private Station station;

	private String commentaires;

	private RefNatureOuvrage nature;

	// Les autres champs sont herites de la classe abstraite
	// Emplacementgeographique

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit un ouvrage avec un identifiant un code et un libelle
	 * 
	 * @param _identifiant
	 *            l'identifiant de l'ouvrage
	 * @param _code
	 *            le code de l'ouvrage dans la station
	 * @param _libelle
	 *            le libelle de l'ouvrage
	 */
	public Ouvrage(Integer _identifiant, String _code, String _libelle) {
		this(_identifiant, null, _code, _libelle, null, null, null, null, null, null, null);
	} // end Ouvrage

	/**
	 * Construit un ouvrage avec tous ses attributs
	 * 
	 * @param _identifiant
	 * @param _station
	 * @param _code
	 * @param _libelle
	 * @param _localisation
	 * @param _coordonneeX
	 * @param _coordonneeY
	 * @param _altitude
	 * @param _carteLocalisation
	 * @param _commentaires
	 * @param _nature
	 */
	public Ouvrage(Integer _identifiant, Station _station, String _code, String _libelle, String _localisation,
			Integer _coordonneeX, Integer _coordonneeY, Short _altitude, BufferedImage _carteLocalisation,
			String _commentaires, RefNatureOuvrage _nature) {
		super(_libelle, _localisation, _coordonneeX, _coordonneeY, _altitude, _carteLocalisation);

		this.setIdentifiant(_identifiant);
		this.setStation(_station);
		this.setCode(_code);
		this.setCommentaires(_commentaires);
		this.setNature(_nature);

	} // end Ouvrage

	///////////////////////////////////////
	// interfaces
	///////////////////////////////////////

	public void insertObjet() throws SQLException, ClassNotFoundException {

		String table = Ouvrage.getNomTable();
		ArrayList nomAttributs = Ouvrage.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		ConnexionBD.getInstance().executeInsert(table, nomAttributs, valAttributs);
		// initialise l'identifiant de l'operation avec le nombre qui vient
		// d'etre autoincremente
		this.identifiant = ConnexionBD.getInstance().getGeneratedKey(Ouvrage.getNomSequences()[0]);
	}

	public void majObjet() throws SQLException, ClassNotFoundException {
		String table = Ouvrage.getNomTable();
		String[] nomAttributsSelection = Ouvrage.getNomID();
		Object[] valAttributsSelection = this.getValeurID();
		ArrayList nomAttributs = Ouvrage.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		ConnexionBD.getInstance().executeUpdate(table, nomAttributsSelection, valAttributsSelection, nomAttributs,
				valAttributs);
	}

	public void effaceObjet() {
	}

	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;
		// identifiant autoincremente
		// if (!Verification.isInteger(this.identifiant, 1, true)) {
		// throw new DataFormatException (Erreur.S9000) ;
		// }
		if (!Verification.isText(this.station.getCode(), 1, 8, true)) {
			throw new DataFormatException(Erreur.S9001);
		}
		if (!Verification.isText(this.code, 1, 13, true)) {
			throw new DataFormatException(Erreur.S9002);
		}
		if (!Verification.isText(super.libelle, 1, 40, true)) {
			throw new DataFormatException(Erreur.S9003);
		}
		if (!Verification.isText(super.localisation, 1, 2000, false)) {
			throw new DataFormatException(Erreur.S9004);
		}
		if (!Verification.isInteger(super.coordonneeX, -40000, 1400000, false)) {
			throw new DataFormatException(Erreur.S9005);
		}
		if (!Verification.isInteger(super.coordonneeY, 6000000, 7100000, false)) {
			throw new DataFormatException(Erreur.S9006);
		}
		if (!Verification.isShort(super.altitude, 0, false)) {
			throw new DataFormatException(Erreur.S9007);
		}
		// if (!Verification.is???(super.carteLocalisation, false)) {
		// throw new DataFormatException (Erreur.S2008) ;
		// }

		if (!Verification.isText(this.commentaires, 0, false)) {
			throw new DataFormatException(Erreur.S9010);
		}
		if (!Verification.isText(this.nature.getCode(), 0, 4, false)) {
			throw new DataFormatException(Erreur.S9011);
		}
		return ret;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return un le nom
	 */
	public static String getNomTable() {
		return "t_ouvrage_ouv";
	}

	/**
	 * Retourne le nom de l'identifiant caract�re de la table
	 * 
	 * @return un vecteur de string de longueur 1 contenant le nom de
	 *         l'identifiant de la table
	 */
	public static String[] getNomID() {
		String[] ret = new String[1];
		ret[0] = "ouv_identifiant";
		return ret;
	} // end getNomID

	/**
	 * Retourne la valeur de l'indenfiant string de la classe
	 * 
	 * @return un vecteur d'objet de longueur 1 contenant le code
	 */
	public Object[] getValeurID() {
		Object[] ret = new Object[1];
		ret[0] = this.identifiant;
		return ret;
	} // end getValeurID

	/**
	 * Retourne le nom des attributs de la table correspondant a cette classe
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		ArrayList<String> ret = new ArrayList<String>();

		ret.add("ouv_identifiant");// normalement null car autoincremente
		ret.add("ouv_sta_code");
		ret.add("ouv_code");
		ret.add("ouv_libelle");
		ret.add("ouv_localisation");
		ret.add("ouv_coordonnee_x");
		ret.add("ouv_coordonnee_y");
		ret.add("ouv_altitude");
		ret.add("ouv_carte_localisation");
		ret.add("ouv_commentaires");
		ret.add("ouv_nov_code");
		ret.add("ouv_org_code");

		return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public ArrayList<Object> getValeurAttributs() {

		ArrayList<Object> ret = new ArrayList<Object>();

		ret.add(this.identifiant);// normalement null car autoincremente
		ret.add(this.station.getCode());
		ret.add(this.code);
		ret.add(super.libelle);
		ret.add(super.localisation);
		ret.add(super.coordonneeX);
		ret.add(super.coordonneeY);
		ret.add(super.altitude);
		ret.add(super.carteLocalisation);
		ret.add(this.commentaires);
		ret.add(this.nature.getCode());
		return ret;
	} // end getValeurAttributs

	/**
	 * Retourne les noms des sequences utilisee pour l'incrementation des
	 * attributs de la table
	 * 
	 * @return un Tableau de chaines avec les nom des sequences
	 */
	public static String[] getNomSequences() {

		String[] ret = new String[1];

		ret[0] = "t_ouvrage_ouv_ouv_identifiant_seq";

		return ret;
	} // end getNomSequences

	/**
	 * Retourne l'identifiant de l'ouvrage
	 * 
	 * @return l'identifiant
	 */
	public Integer getIdentifiant() {
		return this.identifiant;
	} // end getIdentifiant

	/**
	 * Retourne le code de l'ouvrage au sein de la station
	 * 
	 * @return le code
	 */
	public String getCode() {
		return this.code;
	} // end getCode

	/**
	 * Retourne la station de rattachement. Charge les informations dans la BDD
	 * si elles sont n�cessaires...
	 * 
	 * @return la station de rattachement
	 * @throws Exception
	 */
	public Station getStation() throws Exception {
		if (this.station != null)
			return this.station;
		else {
			ResultSet rs = null;

			// Si l'identifiant de l'objet n'est pas definit, erreur
			Integer identifiant = this.getIdentifiant();
			if (identifiant == null) {
				throw new NullPointerException(Erreur.I1000);
			}
			// Extraction de l'enregistrement concerne
			// cle unique dif_ouv_identifiant dif_code

			String sql = "SELECT ouv_sta_code " + "FROM t_ouvrage_ouv WHERE ouv_identifiant='" + this.getIdentifiant()
					+ "' ;";

			rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);
			// En principe, il y a un et un seul enregistrement
			if (rs.next()) {
				String tmp = rs.getString("ouv_sta_code");
				String sql2 = "SELECT sta_code, sta_nom " + "FROM t_station_sta WHERE sta_code='" + tmp + "' ;";
				ResultSet rs2 = null;
				rs2 = ConnexionBD.getInstance().getStatement().executeQuery(sql2);

				if (rs2.next()) {
					String sta_code = rs2.getString("sta_code");
					String sta_nom = rs2.getString("sta_nom");
					this.station = new Station(sta_code, sta_nom);
					ListeStations lo = new ListeStations();
					lo.add(this.station);
					lo.chargeObjets();
					return this.station;
				}
			}
			return null;
		}
	} // end getStation

	/**
	 * Retourne les commentaires sur l'ouvrage
	 * 
	 * @return les commentaires
	 */
	public String getCommentaires() {
		return this.commentaires;
	} // end getCommentaires

	/**
	 * Retourne la nature de l'ouvrage
	 * 
	 * @return la nature
	 */
	public RefNatureOuvrage getNature() {
		return this.nature;
	} // end getNature

	/**
	 * Initialise l'identifiant de l'ouvrage
	 * 
	 * @param _identifiant
	 *            l'identifiant
	 */
	public void setIdentifiant(Integer _identifiant) {
		this.identifiant = _identifiant;
	} // end setIdentifiant

	/**
	 * Initialise le code de l'ouvrage au sein de la station
	 * 
	 * @param _code
	 *            le code
	 */
	public void setCode(String _code) {
		this.code = _code;
	} // end setCode

	/**
	 * Initialise la station de rattechement
	 * 
	 * @param _station
	 *            la station
	 */
	public void setStation(Station _station) {
		this.station = _station;
	} // end setStation

	/**
	 * Initialise les commentaires de l'ouvrage
	 * 
	 * @param _commentaires
	 *            les commentaires
	 */
	public void setCommentaires(String _commentaires) {
		this.commentaires = _commentaires;
	} // end setCommentaires

	/**
	 * Initialise la nature de l'ouvrage
	 * 
	 * @param _nature
	 *            la nature
	 */
	public void setNature(RefNatureOuvrage _nature) {
		this.nature = _nature;
	} // end setNature

	/**
	 * Retourne les attributs de l'ouvrage sous forme textuelle
	 * 
	 * @return l'�criture d'un ouvrage
	 */
	public String toString() {

		String ret = this.code + ", " + super.libelle;
		ret = org.apache.commons.lang3.StringUtils.abbreviate(ret, 30);
		return ret;
	}

} // end Ouvrage
