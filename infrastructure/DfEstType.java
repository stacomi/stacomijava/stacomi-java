/**
 * 
 */
package infrastructure;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

import infrastructure.referenciel.*;
import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;

/**
 * Classe servant � �crire / modifier la table jointure entre les DF et leurs types
 * @author C�dric Briand
 */
public class DfEstType implements IBaseDonnees {

	// /////////////////////////////////////
	// attributes
	// /////////////////////////////////////
	private Integer dfidentifiant;

	private RefTypeDF refTypeDF;

	private Integer rang;

	// /////////////////////////////////////
	// constructors
	// /////////////////////////////////////
	/**
	 * Construit une classe avec l'identifiant et le type de DF
	 * 
	 * @param _dfidentifiant
	 * @param _refTypeDF
	 */
	public DfEstType(Integer _dfidentifiant, RefTypeDF _refTypeDF) {
		this(_dfidentifiant, _refTypeDF, null);
	}

	/**
	 * Construit une classe avec tous ses attributs
	 * 
	 * @param _dfidentifiant
	 *            l'indentifiant du dispositif
	 * @param _refTypeDF
	 *            la liste des types de DF
	 * @param _rang
	 *            le rang dans la liste de DF
	 */
	public DfEstType(Integer _dfidentifiant, RefTypeDF _refTypeDF, Integer _rang) {
		this.setDfidentifiant(_dfidentifiant);
		this.setrefTypeDF(_refTypeDF);
		this.setRang(_rang);
	}

	// /////////////////////////////////////
	// interfaces
	// /////////////////////////////////////

	public void effaceObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		String[] nomSelection = this.getNomID();
		Object[] valSelection = this.getValeurID();
		ConnexionBD.getInstance().executeDelete(table,nomSelection,valSelection);
	}

	public void insertObjet() throws SQLException, ClassNotFoundException {
		// insertion des objets de la table tj_dfesttype_dft
		String table = this.getNomTable();
		ArrayList nomAttributs = this.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		ConnexionBD.getInstance().executeInsert(table, nomAttributs,
				valAttributs);
	}
	  

	public void majObjet() { }
	
	/**
	 * Met � jour le type de DF dans la base de donn�es.
	 * La m�thode majObjet() de l'interface IBaseDonnees ne peut pas etre utilis�e car
	 * la cl� primaire de la table tj_dfesttype_dft est compos�e du triplet (dft_df_identifiant , dft_tdf_code , dft_rang).
	 * On a donc besoin de ces 3 informations pour mettre � jour dans la BDD
	 * @param oldType : l'ancien DfEstType pour avoir les anciennes valeurs (pour la clause WHERE)
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public void majObjet(DfEstType oldType) throws SQLException, ClassNotFoundException {
		
		String table = this.getNomTable();
		ArrayList nomAttributs = this.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();

		String[] nomSelection = oldType.getNomID();
		Object[] valSelection = oldType.getValeurID();
		
		ConnexionBD.getInstance().executeUpdate(table, 
				nomSelection, valSelection, 
				nomAttributs, valAttributs);
		
		//(table, nomAttributs, valAttributs);
	}

	/* verifie les attributs des champs rentr�s dans la table de jointure
	 * La m�thode Verification retourn ret=TRUE si pas de probl�me
	 * @see commun.IBaseDonnees#verifAttributs()
	 * 
	 */
	public boolean verifAttributs() throws DataFormatException {
        boolean ret = false ;
     
         if (!Verification.isInteger(this.dfidentifiant,0,false)) {
            throw new DataFormatException (Erreur.S10000) ;
            }               
        if (!Verification.isText(this.refTypeDF.getCode(), 1, 4, true)) {
            throw new DataFormatException (Erreur.S10008) ;
        }             
        if (!Verification.isInteger(this.rang,1, true)) {
            throw new DataFormatException (Erreur.S10012) ;
        }          
        return ret ;
	}
	
    // /////////////////////////////////////
    // operations
    // /////////////////////////////////////
	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return le nom
	 */
	private String getNomTable() {
		return "tj_dfesttype_dft";
	}

	/**
	 * Retourne le nom des champs de la table
	 * @return le noms des champs de la table dans l'ordre d'insertion dans la base de donnee
	 */
	private ArrayList<String> getNomAttributs() {
		ArrayList<String> ret =new ArrayList<String>();
		ret.add("dft_df_identifiant");
		ret.add("dft_tdf_code");
		ret.add("dft_rang");
		ret.add("dft_org_code");
		return ret;
	}
	
	 /**
	 * Retourne la valeur des attributs de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	private ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret =new ArrayList<Object>();
		ret.add(this.dfidentifiant);
		ret.add(this.refTypeDF.getCode());
		ret.add(this.rang);
		return ret;
	}

	/**
	 * retourne l'identifiant du DF
	 * 
	 * @return l'indentifiant du DF
	 */
	public Integer getDfidentifiant() {
		return this.dfidentifiant;
	}

	/**
	 * retourne la liste de r�f�rence du DF
	 * 
	 * @return la liste de r�f�rence du DF
	 */
	public RefTypeDF getrefTypeDF() {
		return this.refTypeDF;
	}

	/**
	 * retourne le rang du DF
	 * 
	 * @return le rang du DF dans la liste
	 */
	public Integer getRang() {
		return this.rang;
	}

	/**
	 * Initialise la  r�f�rence du type de DF
	 * @param refTypeDF type de DF
	 */
	public void setrefTypeDF(RefTypeDF refTypeDF) {
		this.refTypeDF = refTypeDF;
	}

	/**
	 * Initialise l'identifiant du dispositif de franchissement
	 * 
	 * @param dfidentifiant
	 */
	public void setDfidentifiant(Integer dfidentifiant) {
		this.dfidentifiant = dfidentifiant;
	}

	/**
	 * Initialise le rang dans la liste de r�f�rence du type de df Par exemple
	 * une passe � bassins peut �tre �quip�e d'un pr�-barrage
	 * 
	 * @param rang
	 */
	public void setRang(Integer rang) {
		this.rang = rang;
	}
	
	/**
	 * Recup�ration des noms des champs d'un type de DF
	 * @return les noms des champs d'un type de DF
	 */
	private String[] getNomID()
	{
		String[] res = new String[3];
		res[0] = "dft_df_identifiant";
		res[1] = "dft_tdf_code";
		res[2] = "dft_rang";
		return res;
	}
	
	/**
	 * Recup�ration des valeurs des champs d'un type de DF 
	 * @return les valeurs des champs d'un type de DF
	 */
	private Object[] getValeurID()
	{
		Object[] res = new Object[3];
		res[0] = this.dfidentifiant;
		res[1] = this.refTypeDF.getCode();
		res[2] = this.rang;
		return res;
	}

}
