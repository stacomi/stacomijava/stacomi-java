/*
 **********************************************************************
 *
 * Nom fichier :        Dispositif.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package infrastructure;

import infrastructure.referenciel.SousListePeriodes;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;

/**
 * Classe generique pour les dispositif de franchissement et dispositifs de
 * comptage
 * @author C�dric Briand
 */
public abstract class Dispositif implements IBaseDonnees{

	// /////////////////////////////////////
	// attributes
	// /////////////////////////////////////

	private Integer identifiant;

	private Date dateCreation;

	private Date dateSuppression;

	private String commentaires;

	private SousListePeriodes lesPeriodes;

	// /////////////////////////////////////
	// constructors
	// /////////////////////////////////////

	/**
	 * Construit un Dispositif avec tous ses attributs
	 * @param _identifiant identifiant du dispositif
	 * @param _dateCreation date de cr�ation du dispositif
	 * @param _dateSuppression date d'arret du dispositif
	 * @param _commentaires commentaires
	 * @param _lesPeriodes p�riodes de fonctionnement
	 */
	public Dispositif(Integer _identifiant, Date _dateCreation,
			Date _dateSuppression, String _commentaires,
			SousListePeriodes _lesPeriodes) {

		this.setIdentifiant(_identifiant);
		this.setDateCreation(_dateCreation);
		this.setDateSuppression(_dateSuppression);
		this.setCommentaires(_commentaires);
		this.setLesPeriodes(_lesPeriodes);

	} // end Dispositif
	
	
	// /////////////////////////////////////
	// interfaces
	// /////////////////////////////////////
	 
	/**
	 * ins�re les objets dans la base de donnee
	 */


	// /////////////////////////////////////
	// operations
	// /////////////////////////////////////

	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return un le nom
	 */
	public static String getNomTable() {
		return "tg_dispositif_dis";
	}

	/**
	 * Retourne le nom des attributs de la table correspondant a la classe 
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		// impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
		// est inconnu
		ArrayList<String> ret = new ArrayList<String>();
		ret.add("dis_date_creation");
		ret.add("dis_date_suppression");
		ret.add("dis_commentaires");
		ret.add("dis_org_code");
	    return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de la classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public ArrayList<Object> getValeurAttributs() {

		ArrayList<Object> ret = new ArrayList<Object>();
		ret.add(this.dateCreation);
		ret.add(this.dateSuppression);
		ret.add(this.commentaires);
		return ret;
	} // end getValeurAttributs

	/**
	 * Retourne les noms des sequences utilisee pour l'incrementation des
	 * attributs de la table
	 * 
	 * @return un Tableau de chaines avec les nom des sequences
	 */
	public static String[] getNomSequences() {

		String[] ret = new String[1];

		ret[0] = "tg_dispositif_dis_dis_identifiant_seq";

		return ret;
	} // end getNomSequences

	/**
	 * Retourne l'identifiant du dispositif
	 * 
	 * @return L'indentifiant du dispositif
	 */
	public Integer getIdentifiant() {
		return this.identifiant;
	} // end getIdentifiant

	/**
	 * Retourne la date de mise en service du dispositif
	 * 
	 * @return la date de mise en service
	 */
	public Date getDateCreation() {
		return this.dateCreation;
	} // end getDateCreation

	/**
	 * Retourne la date d'arret du dispositif
	 * 
	 * @return la date d'arret
	 */
	public Date getDateSuppression() {
		return this.dateSuppression;
	} // end getDateSuppression

	/**
	 * Retourne les commentaires sur le dispositif
	 * 
	 * @return les commentaires
	 */
	public String getCommentaires() {
		return this.commentaires;
	} // end getCommentaires

	/**
	 * Retourne les periodes de fonctionnement et d'arret du dispositif
	 * 
	 * @return les periodes
	 */
	public SousListePeriodes getLesPeriodes() {
		return this.lesPeriodes;
	} // end getLesPeriodes

	/**
	 * Initialise l'identifiant du dispositif
	 * 
	 * @param _identifiant
	 */
	public void setIdentifiant(Integer _identifiant) {
		this.identifiant = _identifiant;
	} // end setIdentifiant

	/**
	 * Initialise la date de mise en service du dispositif
	 * 
	 * @param _dateCreation
	 *            la date de mise en service
	 */
	public void setDateCreation(Date _dateCreation) {
		this.dateCreation = _dateCreation;
	} // end setDateCreation

	/**
	 * Initialise la date d'arret du dispositif
	 * 
	 * @param _dateSuppression
	 *            la date d'arret
	 */
	public void setDateSuppression(Date _dateSuppression) {
		this.dateSuppression = _dateSuppression;
	} // end setDateSuppression

	/**
	 * Initialise les commentaires sur le ispositif
	 * 
	 * @param _commentaires
	 *            les commentaires
	 */
	public void setCommentaires(String _commentaires) {
		this.commentaires = _commentaires;
	} // end setCommentaires

	/**
	 * Initialise la sous-liste des periodes de fonctionnement et d'arret du
	 * dispositif
	 * 
	 * @param _lesPeriodes
	 *            la sous-liste des periodes
	 */
	public void setLesPeriodes(SousListePeriodes _lesPeriodes) {
		this.lesPeriodes = _lesPeriodes;
	} // end setLesPeriodes
	
	/**
	 * Acces a la cl� primaire de la table
	 * @return la cl� primaire de la table
	 */
	public static String[] getNomID() {
		String[] ret = new String[1];
		ret[0] = "dis_identifiant";
		return ret;
	} // end getNomID
	
	/**
	 * Acces aux valeurs la cl� primaire de la table
	 * @return les valeurs la cl� primaire de la table
	 */
	public Object[] getValeurID() {
		Object[] ret = new Object[1];
		ret[0] = this.identifiant;
		return ret;
	} // end getNomID
	
	/**
	 * Permet de connaitre le type d'un dispositif (DC ou DF)
	 * @param identifiant : l'identifiant du dispositif
	 * @return vrai si le dispositif est un DF, faux si c'est un DC
	 * @throws Exception
	 */
	public static boolean estUnDf(Integer identifiant) throws Exception
	{
		boolean result = false;
		
		ResultSet rs = null;
		
		// la requete
		String sql = 
		     "SELECT DIF FROM ( "+
    		 "select      dis_identifiant, " +
    		 "dif_dis_identifiant>0 AS DIF " +
    		 "FROM " + ConnexionBD.getSchema() + "tg_dispositif_dis " +
    		 "INNER JOIN " + ConnexionBD.getSchema() + "t_dispositiffranchissement_dif ON dif_dis_identifiant=dis_identifiant " +
    		 "UNION " +
    		 "select      dis_identifiant, " +
    		 "dic_dis_identifiant<=0 as DIF " +
    		 "FROM " + ConnexionBD.getSchema() + "tg_dispositif_dis " +
    		 "INNER JOIN " + ConnexionBD.getSchema() + "t_dispositifcomptage_dic ON dic_dis_identifiant=dis_identifiant) AS requetecroisee " +
    		 "WHERE dis_identifiant='"+identifiant+"' " ;
		
		rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
		
		// en principe, il n'y a qu'un seul enregistrement
		if(!rs.first())
			throw new Exception(Erreur.I1001);
		
		result = rs.getBoolean("DIF");

		return result;
	}

} // end Dispositif

