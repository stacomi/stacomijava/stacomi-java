/*
 **********************************************************************
 *
 * Nom fichier :        Station.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package infrastructure;

import java.awt.image.BufferedImage;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.Erreur;
import commun.Verification;
import infrastructure.referenciel.SousListeConditionsEnv;
import infrastructure.referenciel.SousListeDF;
import infrastructure.referenciel.SousListeOuvrages;

/**
 * Station de controle des migrations
 * 
 * @author C�dric Briand
 */
public class Station extends EmplacementGeographique {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	private String code;
	private Integer superficieBassin;
	private Float distanceMer;
	private Date dateCreation;
	private Date dateSuppression;
	private String commentaires;
	private Date dernierImportConditionsEnv;
	private SousListeConditionsEnv lesConditionsEnv;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une station sans attributs
	 */
	public Station() {
		this(null, null, null, null, null, null, null, null, null, null, null, null, null);
	} // end Station

	/**
	 * Construit une station avec uniquement un code et un libelle
	 * 
	 * @param _code
	 *            le code de la station
	 * @param _libelle
	 *            le libelle de la station
	 */
	public Station(String _code, String _libelle) {
		this(_code, _libelle, null, null, null, null, null, null, null, null, null, null, null);
	} // end Station

	/**
	 * Construit une station avec tous ses attributs
	 * 
	 * @param _code
	 *            code de la station
	 * @param _libelle
	 *            libell� de la station
	 * @param _localisation
	 *            localisation
	 * @param _coordonneeX
	 *            coordonn�e X
	 * @param _coordonneeY
	 *            coordonn�e Y
	 * @param _altitude
	 *            altitude
	 * @param _carteLocalisation
	 *            carte de localisation
	 * @param _superficieBassin
	 *            superficie du bassin
	 * @param _distanceMer
	 *            distance � la mer
	 * @param _dateCreation
	 *            date de cr�ation
	 * @param _dateSuppression
	 *            date de suppression
	 * @param _commentaires
	 *            commentaires
	 * @param _dernierImportConditionsEnv
	 *            conditions environnementales
	 */
	public Station(String _code, String _libelle, String _localisation, Integer _coordonneeX, Integer _coordonneeY,
			Short _altitude, BufferedImage _carteLocalisation, Integer _superficieBassin, Float _distanceMer,
			Date _dateCreation, Date _dateSuppression, String _commentaires, Date _dernierImportConditionsEnv) {
		super(_libelle, _localisation, _coordonneeX, _coordonneeY, _altitude, _carteLocalisation);

		this.setCode(_code);
		this.setSuperficieBassin(_superficieBassin);
		this.setDistanceMer(_distanceMer);
		this.setDateCreation(_dateCreation);
		this.setDateSuppression(_dateSuppression);
		this.setCommentaires(_commentaires);
		this.setDernierImportConditionsEnv(_dernierImportConditionsEnv);

	} // end Station

	///////////////////////////////////////
	// interfaces
	///////////////////////////////////////

	public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = Station.getNomTable();
		ArrayList nomAttributs = Station.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		ConnexionBD.getInstance().executeInsert(table, nomAttributs, valAttributs);
	}

	public void majObjet() throws SQLException, ClassNotFoundException {
		String table = Station.getNomTable();
		String[] nomAttributsSelection = Station.getNomID();
		Object[] valAttributsSelection = this.getValeurID();
		ArrayList nomAttributs = Station.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		ConnexionBD.getInstance().executeUpdate(table, nomAttributsSelection, valAttributsSelection, nomAttributs,
				valAttributs);
	}

	/**
	 * maj avec une valeur nouvelle de code pour pouvoir changer le code, le
	 * _code correspond � l'ancien code � mettre � jour
	 * 
	 * @param _code
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void majObjet(String _code) throws SQLException, ClassNotFoundException {
		String table = Station.getNomTable();
		String[] nomAttributsSelection = Station.getNomID();
		Object[] valAttributsSelection = new Object[1];
		valAttributsSelection[0] = _code;
		ArrayList nomAttributs = Station.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		ConnexionBD.getInstance().executeUpdate(table, nomAttributsSelection, valAttributsSelection, nomAttributs,
				valAttributs);
	}

	public void effaceObjet() {
	}

	public boolean verifAttributs() throws DataFormatException {

		// v�rification des attributs 1 par 1...
		if (!Verification.isText(super.libelle, 1, 40, true))
			throw new DataFormatException(Erreur.S2001);

		if (!Verification.isText(super.localisation, 1, 60, false))
			throw new DataFormatException(Erreur.S2002);

		if (!Verification.isInteger(super.coordonneeX, -40000, 1400000, false))
			throw new DataFormatException(Erreur.S2003);

		if (!Verification.isInteger(super.coordonneeY, 6000000, 7100000, false))
			throw new DataFormatException(Erreur.S2004);

		if (!Verification.isShort(super.altitude, 0, false))
			throw new DataFormatException(Erreur.S2005);

		// if (!Verification.is???(super.carteLocalisation, false)) {
		// throw new DataFormatException (Erreur.S2006) ;
		// }
		if (!Verification.isText(this.code, 1, 8, true))
			throw new DataFormatException(Erreur.S2000);

		if (!Verification.isInteger(this.superficieBassin, 0, false))
			throw new DataFormatException(Erreur.S2007);

		if (!Verification.isFloat(this.distanceMer, 0, false))
			throw new DataFormatException(Erreur.S2008);

		// Rien a verifier
		// Date this.dateCreation ;

		if (!Verification.isDateInf(this.dateCreation, this.dateSuppression, false, false)) {
			throw new DataFormatException(Erreur.S2009 + " ou " + Erreur.S2010);
		}

		if (!Verification.isText(this.commentaires, false))
			throw new DataFormatException(Erreur.S2011);

		// Rien a verifier
		// Date this.dernierImportConditionsEnv ;

		return true;
	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return un le nom
	 */
	public static String getNomTable() {
		return "t_station_sta";
	}

	/**
	 * Retourne le nom de l'identifiant caract�re de la table
	 * 
	 * @return un vecteur de string de longueur 1 contenant le nom de
	 *         l'identifiant de la table
	 */
	public static String[] getNomID() {
		String[] ret = new String[1];
		ret[0] = "sta_code";
		return ret;
	} // end getNomID

	/**
	 * Retourne la valeur de l'indenfiant string de la classe
	 * 
	 * @return un vecteur d'objet de longueur 1 contenant le code
	 */
	public Object[] getValeurID() {
		Object[] ret = new Object[1];
		ret[0] = this.getCode();
		return ret;
	} // end getValeurID

	/**
	 * Retourne le nom des attributs de la table correspondant a cette classe
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		// impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
		// est inconnu

		ArrayList<String> ret = new ArrayList<String>();

		ret.add("sta_code");
		ret.add("sta_nom");
		ret.add("sta_localisation");
		ret.add("sta_coordonnee_x");
		ret.add("sta_coordonnee_y");
		ret.add("sta_altitude");
		ret.add("sta_carte_localisation");
		ret.add("sta_superficie");
		ret.add("sta_distance_mer");
		ret.add("sta_date_creation");
		ret.add("sta_date_suppression");
		ret.add("sta_commentaires");
		ret.add("sta_dernier_import_conditions");
		ret.add("sta_org_code");

		return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public ArrayList<Object> getValeurAttributs() {

		ArrayList<Object> ret = new ArrayList<Object>();

		ret.add(this.code);
		ret.add(super.libelle);
		ret.add(super.localisation);
		ret.add(super.coordonneeX);
		ret.add(super.coordonneeY);
		ret.add(super.altitude);
		ret.add(super.carteLocalisation);
		ret.add(this.superficieBassin);
		ret.add(this.distanceMer);
		ret.add(this.dateCreation);
		ret.add(this.dateSuppression);
		ret.add(this.commentaires);
		ret.add(this.dernierImportConditionsEnv);

		return ret;
	} // end getValeurAttributs

	/**
	 * Retourne le code de la station
	 * 
	 * @return le code
	 */
	public String getCode() {
		return this.code;
	} // end getCode

	/**
	 * Retourne la superficie du bassin versant
	 * 
	 * @return la superficie
	 */
	public Integer getSuperficieBassin() {
		return this.superficieBassin;
	} // end getSuperficieBassin

	/**
	 * Retourne la distance de la station a la mer
	 * 
	 * @return la distance
	 */
	public Float getDistanceMer() {
		return this.distanceMer;
	} // end getDistanceMer

	/**
	 * Retourne la date de creation de la station
	 * 
	 * @return la date de creation
	 */
	public Date getDateCreation() {
		return this.dateCreation;
	} // end getDateCreation

	/**
	 * Retourne la date d'arret de la station
	 * 
	 * @return la date d'arret
	 */
	public Date getDateSuppression() {
		return this.dateSuppression;
	} // end getDateSuppression

	/**
	 * Retourne les commentaires sur la stations
	 * 
	 * @return les commentaires
	 */
	public String getCommentaires() {
		return this.commentaires;
	} // end getCommentaires

	/**
	 * Retourne la date a laquelle les conditions environnementales de la
	 * station ont ete importees
	 * 
	 * @return la date d'import
	 */
	public Date getDernierImportConditionsEnv() {
		return this.dernierImportConditionsEnv;
	} // end getDernierImportConditionsEnv

	/**
	 * Retourne la sous-liste des conditions environnementales de la station
	 * 
	 * @return la sous-liste des conditions
	 */
	public SousListeConditionsEnv getLesConditionsEnv() {
		return this.lesConditionsEnv;
	} // end getLesConditionsEnv

	/**
	 * Initialise le code de la station
	 * 
	 * @param _code
	 *            le code
	 */
	public void setCode(String _code) {
		this.code = _code;
	} // end setCode

	/**
	 * Initialise la superficie du bassin versant
	 * 
	 * @param _superficieBassin
	 *            la superficie
	 */
	public void setSuperficieBassin(Integer _superficieBassin) {
		this.superficieBassin = _superficieBassin;
	} // end setSuperficieBassin

	/**
	 * Initialise la distance de la station a la mer
	 * 
	 * @param _distanceMer
	 *            la distance
	 */
	public void setDistanceMer(Float _distanceMer) {
		this.distanceMer = _distanceMer;
	} // end setDistanceMer

	/**
	 * Initialise la date de creation de la station
	 * 
	 * @param _dateCreation
	 *            la date de creation
	 */
	public void setDateCreation(Date _dateCreation) {
		this.dateCreation = _dateCreation;
	} // end setDateCreation

	/**
	 * Initialise la date d'arret de la station
	 * 
	 * @param _dateSuppression
	 *            la date d'arret
	 */
	public void setDateSuppression(Date _dateSuppression) {
		this.dateSuppression = _dateSuppression;
	} // end setDateSuppression

	/**
	 * Initialise les commentaires sur la station
	 * 
	 * @param _commentaires
	 */
	public void setCommentaires(String _commentaires) {
		this.commentaires = _commentaires;
	} // end setCommentaires

	/**
	 * Initialise la date a laquelle les conditions environnementales ont été
	 * importee pour la derniere fois
	 * 
	 * @param _dernierImportConditionsEnv
	 *            la date d'import
	 */
	public void setDernierImportConditionsEnv(Date _dernierImportConditionsEnv) {
		this.dernierImportConditionsEnv = _dernierImportConditionsEnv;
	} // end setDernierImportConditionsEnv

	/**
	 * Initialise la sous-liste des conditions environnementales de la station
	 * 
	 * @param _lesConditionsEnv
	 *            la sous-liste des conditions
	 */
	public void setLesConditionsEnv(SousListeConditionsEnv _lesConditionsEnv) {
		this.lesConditionsEnv = _lesConditionsEnv;
	} // end setLesConditionsEnv

	/**
	 * Retourne les attributs de la station sous forme textuelle
	 * 
	 * @return un string les attributs de la station sous forme textuelle
	 */
	public String toString() {

		String ret = this.code + ", " + super.libelle;
		ret = org.apache.commons.lang3.StringUtils.abbreviate(ret, 30);

		/*
		 * ret = ret + "code : " + this.code + "\n" ; ret = ret + "libelle : " +
		 * super.libelle + "\n" ; ret = ret + "localisation : " +
		 * super.localisation + "\n" ; ret = ret + "coordonneeX : " +
		 * super.coordonneeX + "\n" ; ret = ret + "coordonneeY : " +
		 * super.coordonneeY + "\n" ; ret = ret + "altitude : " + super.altitude
		 * + "\n" ; ret = ret + "carteLocalisation : " + super.carteLocalisation
		 * + "\n" ; ret = ret + "superficieBassin : " + this.superficieBassin +
		 * "\n" ; ret = ret + "distanceMer : " + this.distanceMer + "\n" ; ret =
		 * ret + "dateCreation : " + this.dateCreation + "\n" ; ret = ret +
		 * "dateSuppression : " + this.dateSuppression + "\n" ; ret = ret +
		 * "commentaires : " + this.commentaires + "\n" ; ret = ret +
		 * "dernierImportConditionsEnv : " + this.dernierImportConditionsEnv +
		 * "\n" ; ret = ret + "lesConditionsEnv : " + this.lesConditionsEnv ;
		 */

		return ret;
	} // end toString

	/**
	 * Arrete le station (fixe la date de suppression...) mais aussi les DF de
	 * la station et les DC
	 * 
	 * @param d
	 *            : la date de suppression du DF
	 * @throws Exception
	 * @throws DataFormatException
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void stop(Date d) throws Exception, DataFormatException, SQLException, ClassNotFoundException {
		SousListeOuvrages slo = new SousListeOuvrages(this);
		// chargement des ouvrages appartenant � la station

		slo.chargeSansFiltre();

		// on parcourt l'ensemble des ouvrages de la station
		for (int i = 0; i < slo.size(); i++) {
			Ouvrage o = (Ouvrage) slo.get(i);
			// chargement des DF de l'ouvrage
			SousListeDF sldf = new SousListeDF(o);

			sldf.chargeSansFiltre();
			// parcourt de l'ensemble des DF
			for (int j = 0; j < sldf.size(); j++) {
				// si le DF est d�j� clos, on n'y touche pas...
				DF df = (DF) sldf.get(j);

				sldf.chargeObjet(df);
				df.stop(d);
			}
		}

		// s'il n'y a aucune date de suppression, on en met une
		if (this.getDateSuppression() == null)
			this.setDateSuppression(d);

		// v�rification des attributs et mise a jour...
		this.verifAttributs();
		this.majObjet();
	}
} // end Station
