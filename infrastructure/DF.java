/*
 **********************************************************************
 *
 * Nom fichier :        DF.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */

package infrastructure;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.zip.DataFormatException;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;
import infrastructure.referenciel.ListeOuvrage;
import infrastructure.referenciel.ListeRefTypeDF;
import infrastructure.referenciel.SousListeDC;
import infrastructure.referenciel.SousListePeriodes;
import migration.referenciel.ListeRefTaxon;

/**
 * Dispositif de franchissement
 */
public class DF extends Dispositif implements IBaseDonnees {

	// /////////////////////////////////////
	// attributes
	// /////////////////////////////////////

	private Ouvrage ouvrage;
	private String code;
	private String localisation;
	private String orientation;
	private SousListeDC lesDC;
	private ListeRefTaxon lesTaxonsDeDestination;
	private ListeRefTypeDF lesTypesDeDF;

	// /////////////////////////////////////
	// constructors
	// /////////////////////////////////////

	/**
	 * Construit un DF avec uniquement un identifiant et un code (code au sein
	 * de l'ouvrage)
	 * 
	 * @param _identifiant
	 *            identifiant du DF
	 * @param _code
	 *            code du DF
	 */
	public DF(Integer _identifiant, String _code) {
		this(_identifiant, null, null, null, null, null, _code, null, null, null, null, null);
	} // end DF

	/**
	 * Construit un DF avec quelques attributs
	 * 
	 * @param _identifiant
	 *            identifiant du DF
	 * @param _dateCreation
	 *            date de cr�ation
	 * @param _dateSuppression
	 *            date de suppression
	 * @param _commentaires
	 *            commentaires
	 * @param _code
	 *            code du DF
	 * @param _localisation
	 *            localisation du DF
	 * @param _orientation
	 *            orientation du DF (mont�e ou descente)
	 */
	public DF(Integer _identifiant, Date _dateCreation, Date _dateSuppression, String _commentaires, String _code,
			String _localisation, String _orientation) {
		this(_identifiant, _dateCreation, _dateSuppression, _commentaires, null, null, _code, _localisation,
				_orientation, null, null, null);
	} // end DF

	/**
	 * Construit un DF avec tous ses attributs
	 * 
	 * @param _identifiant
	 *            identifiant du DF
	 * @param _dateCreation
	 *            date de cr�ation
	 * @param _dateSuppression
	 *            date de suppression
	 * @param _commentaires
	 *            commentaires
	 * @param _lesPeriodes
	 *            p�riodes de fonctionnement
	 * @param _ouvrage
	 *            ouvrage
	 * @param _code
	 *            code
	 * @param _localisation
	 *            localisation
	 * @param _orientation
	 *            orientation
	 * @param _lesDC
	 *            les DC associ�s
	 * @param _lesTaxonsDeDestination
	 *            taxons de destination
	 * @param _lesTypesDeDF
	 *            types de DF
	 */
	public DF(Integer _identifiant, Date _dateCreation, Date _dateSuppression, String _commentaires,
			SousListePeriodes _lesPeriodes, Ouvrage _ouvrage, String _code, String _localisation, String _orientation,
			SousListeDC _lesDC, ListeRefTaxon _lesTaxonsDeDestination, ListeRefTypeDF _lesTypesDeDF) {

		super(_identifiant, _dateCreation, _dateSuppression, _commentaires, _lesPeriodes);

		this.setOuvrage(_ouvrage);
		this.setCode(_code);
		this.setLocalisation(_localisation);
		this.setOrientation(_orientation);
		this.setLesDC(_lesDC);
		this.setLesTaxonsDeDestination(_lesTaxonsDeDestination);
		this.setLesTypesDeDF(_lesTypesDeDF);

	} // end DF

	// /////////////////////////////////////
	// interfaces
	// /////////////////////////////////////

	/*
	 * L'insertion d'objets pour cette classe se fait � plusieurs endroits dans
	 * la base de donnee La table de groupement (qui est la classe super) La
	 * table du dispositif Les tables de jointure indiquant le taxon et le type
	 * de dispositif de franchissement
	 * 
	 * @see infrastructure.Dispositif#insertObjet()
	 */
	public void insertObjet() throws SQLException, ClassNotFoundException {

		// insertion des objets de la table dispositif
		String table = Dispositif.getNomTable();
		ArrayList nomAttributs = Dispositif.getNomAttributs();
		ArrayList valAttributs = super.getValeurAttributs();
		ConnexionBD.getInstance().executeInsert(table, nomAttributs, valAttributs);

		// La v�rification des attributs du dispositif se fait par appel � la
		// m�thode super dans DF

		// initialise l'identifiant avec l'indentifiant de la table de
		// groupement des dc et des df (super)
		super.setIdentifiant((Integer) ConnexionBD.getInstance().getGeneratedKey(Dispositif.getNomSequences()[0]));

		// insertion des objets de la table t_dispositiffranchissement_dif
		String tableDF = DF.getNomTable();
		ArrayList nomAttributsDF = DF.getNomAttributs();
		ArrayList valAttributsDF = this.getValeurAttributs();
		ConnexionBD.getInstance().executeInsert(tableDF, nomAttributsDF, valAttributsDF);
	}

	public void majObjet() throws SQLException, ClassNotFoundException {
		// Il y a plusieurs �critures � effectuer :
		// 1) Ecriture dans la table tg_dispositif_dis
		// 2) Ecriture dans la tablet_dispositiffranchissement_dif

		// Modification des objets de la table tj_dfesttype_dft
		String typeDFTable = super.getNomTable();
		String[] disNomAttributsSelection = Dispositif.getNomID();
		Object[] disValAttributsSelection = super.getValeurID();
		ArrayList disNomAttributsModifies = Dispositif.getNomAttributs();
		ArrayList disValAttributsModifies = super.getValeurAttributs();
		ConnexionBD.getInstance().executeUpdate(typeDFTable, disNomAttributsSelection, disValAttributsSelection,
				disNomAttributsModifies, disValAttributsModifies); // OK pour la
																	// modification
																	// de la
																	// table
																	// tg_dispositif_dis

		// Modification des objets de la table t_dispositifcomptage_dic
		String DFtable = DF.getNomTable();
		String[] DFnomAttributsSelection = DF.getNomID();
		Object[] DFvalAttributsSelection = this.getValeurID();
		ArrayList DFnomAttributs = DF.getNomAttributs();
		ArrayList DFvalAttributs = this.getValeurAttributs();
		ConnexionBD.getInstance().executeUpdate(DFtable, DFnomAttributsSelection, DFvalAttributsSelection,
				DFnomAttributs, DFvalAttributs);

	}

	public void effaceObjet() {
	}

	/*
	 * verification des attributs du DF
	 * 
	 * @author cedric
	 * 
	 * @see commun.IBaseDonnees#verifAttributs()
	 */
	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;
		// identifiant autoincremente
		// if (!Verification.isInteger(super.getidentifiant(), 1, true)) {
		// throw new DataFormatException (Erreur.S10000) ;
		// }
		if (!Verification.isInteger(this.ouvrage.getIdentifiant(), 0, false)) {
			throw new DataFormatException(Erreur.S10001);
		}
		// verifie que la date de supression est post�rieure � la date de
		// creation
		// false false => la saisie de la date de creation ou de la date de
		// supression n'est pas obligatoire

		if (!Verification.isDateInf(super.getDateCreation(), super.getDateSuppression(), false, false)) {
			throw new DataFormatException(Erreur.S10005);
		}

		if (!Verification.isText(super.getCommentaires(), 0, false)) {
			throw new DataFormatException(Erreur.S10006);
		}

		if (!Verification.isText(this.code, 1, 16, true)) {
			throw new DataFormatException(Erreur.S10002);
		}
		if (!Verification.isText(this.localisation, true)) {
			throw new DataFormatException(Erreur.S10003);
		}
		if (!Verification.isText(this.orientation, 1, 20, false)) {
			throw new DataFormatException(Erreur.S10004);
		}
		Vector<String> vector = new Vector<String>();
		vector.add("MONTEE");
		vector.add("DESCENTE");
		if (!Verification.isPossibleText(this.orientation, vector, true)) {
			throw new DataFormatException(Erreur.S10004);
		}
		// les infos qui suivent vont �tre �crites dans des tables s�par�es
		// if (!Verification.isText(this.lesTaxonsDeDestination, 0, false)) {
		// throw new DataFormatException (Erreur.S10007) ;
		// }
		// if (!Verification.isText(this.lesTypesDeDF,0, false)) {
		// throw new DataFormatException (Erreur.S10008) ;
		// }
		return ret;
	}

	// /////////////////////////////////////
	// operations
	// /////////////////////////////////////

	/**
	 * Retourne le nom de la table correspondante a la classe
	 * 
	 * @return un le nom
	 */
	public static String getNomTable() {
		return "t_dispositiffranchissement_dif";
	}

	/**
	 * Retourne le nom de l'identifiant caract�re de la table
	 * 
	 * @return un vecteur de string de longueur 1 contenant le nom de
	 *         l'identifiant de la table
	 */
	public static String[] getNomID() {
		String[] ret = new String[1];
		ret[0] = "dif_dis_identifiant";
		return ret;
	} // end getNomID

	/**
	 * Retourne la valeur de l'indenfiant string de la classe
	 * 
	 * @return un vecteur d'objet de longueur 1 contenant le code
	 */
	public Object[] getValeurID() {
		Object[] ret = new Object[1];
		ret[0] = this.getIdentifiant();
		return ret;
	} // end getValeurID

	/**
	 * Retourne le nom des attributs de la table correspondant a cette classe
	 * 
	 * @return un Vector de String avec les attributs dans l'ordre de la table
	 *         dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		// impossible d'utiliser getDeclaredFields() car l'ordre dans le
		// tableau est inconnu

		ArrayList<String> ret = new ArrayList<String>();
		ret.add("dif_dis_identifiant");
		ret.add("dif_ouv_identifiant");
		ret.add("dif_code");
		ret.add("dif_localisation");
		ret.add("dif_orientation");
		ret.add("dif_org_code");
		return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de cette classe
	 * 
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table
	 *         dans la BD pour la table
	 */
	public ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret = new ArrayList<Object>();
		ret.add(this.getIdentifiant());// normalement null car autoincremente
		try {
			ret.add(this.getOuvrage().getIdentifiant());// On rentre
														// l'identifiant
														// (integer de
														// l'ouvrage)
		} catch (Exception e) {
			System.out.println(Erreur.B1020);
		}
		ret.add(this.code);
		ret.add(this.localisation);
		ret.add(this.orientation);
		return ret;
	} // end getValeurAttributs

	/**
	 * Retourne l'ouvrage sur lequel se trouve le DF Si l'ouvrage n'a pas �t�
	 * initialis�, charge l'information dans le BDD
	 * 
	 * @return l'ouvrage
	 * @throws Exception
	 */
	public Ouvrage getOuvrage() throws Exception {
		if (this.ouvrage != null)
			return this.ouvrage;
		else {
			ResultSet rs = null;

			// Si l'identifiant de l'objet n'est pas definit, erreur
			Integer identifiant = this.getIdentifiant();
			if (identifiant == null) {
				throw new NullPointerException(Erreur.I1000);
			}

			// requete pour charger l'ouvrage de rattachement du DF
			String sql = "SELECT dif_ouv_identifiant "
					+ "FROM t_dispositiffranchissement_dif WHERE dif_dis_identifiant='" + this.getIdentifiant() + "'";

			rs = ConnexionBD.getInstance().getStatement().executeQuery(sql);

			// En principe, il y a un et un seul enregistrement
			if (rs.next()) {
				String sql2 = "SELECT ouv_identifiant, ouv_code, ouv_libelle " + "FROM " + ConnexionBD.getSchema()
						+ "t_ouvrage_ouv WHERE ouv_identifiant='" + rs.getInt("dif_ouv_identifiant") + "'";
				ResultSet rs2 = ConnexionBD.getInstance().getStatement().executeQuery(sql2);

				// En principe, il y a un et un seul enregistrement
				if (rs2.next()) {
					int ouvrage_identifiant = rs.getInt("dif_ouv_identifiant");
					Integer ouv_identifiant = new Integer(ouvrage_identifiant);
					String ouv_code = rs2.getString("ouv_code");
					String ouv_libelle = rs2.getString("ouv_libelle");
					this.ouvrage = new Ouvrage(ouv_identifiant, ouv_code, ouv_libelle);
					ListeOuvrage lo = new ListeOuvrage();
					lo.add(this.ouvrage);
					lo.chargeObjets();
					return this.ouvrage;
				} else // plusieurs tuples => pas normal
				{
					throw new Exception(Erreur.I1001);
				}
			} else // plusieurs tuples => pas normal
			{
				throw new Exception(Erreur.I1001);
			}
		}
	} // end getOuvrage

	/**
	 * Retourne le code du DF au sein de l'ouvrage
	 * 
	 * @return le code
	 */
	public String getCode() {
		return this.code;
	} // end getCode

	/**
	 * Retourne la localisation du DF
	 * 
	 * @return la localisation
	 */
	public String getLocalisation() {
		return this.localisation;
	} // end getLocalisation

	/**
	 * Retourne l'orientation du DF
	 * 
	 * @return l'orientation (MONTE | DESCENTE)
	 */
	public String getOrientation() {
		return this.orientation;
	} // end getOrientation

	/**
	 * Retourne la sous-liste des DC qui equipent le DF
	 * 
	 * @return la sous-liste des DC
	 */
	public SousListeDC getLesDC() {
		return this.lesDC;
	} // end getLesDC

	/**
	 * Retourne la sous-liste des taxons sur lequels portent le DF
	 * 
	 * @return la sous-liste des taxons
	 */
	public ListeRefTaxon getLesTaxonsDeDestination() {
		return this.lesTaxonsDeDestination;
	} // end getLesTaxonsDeDestination

	/**
	 * Retourne la sous-liste des types du DF. Dans la sous liste, la position
	 * du type indique son rang.
	 * 
	 * @return la sous-liste des types
	 */
	public ListeRefTypeDF getLesTypesDeDF() {
		return this.lesTypesDeDF;
	} // end getLesTypesDeDF

	/**
	 * Initialise l'ouvrage sur lequel se trouve le DF
	 * 
	 * @param _ouvrage
	 *            l'ouvrage
	 */
	public void setOuvrage(Ouvrage _ouvrage) {
		this.ouvrage = _ouvrage;
	} // end setOuvrage

	/**
	 * Initialise le code du DF au sein de l'ouvrage
	 * 
	 * @param _code
	 *            le code
	 */
	public void setCode(String _code) {
		this.code = _code;
	} // end setCode

	/**
	 * Initialise la localisation du DF
	 * 
	 * @param _localisation
	 *            la localisation
	 */
	public void setLocalisation(String _localisation) {
		this.localisation = _localisation;
	} // end setLocalisation

	/**
	 * Initialise l'orientation du DF
	 * 
	 * @param _orientation
	 *            l'orientation (MONTE | DESCENTE)
	 */
	public void setOrientation(String _orientation) {
		this.orientation = _orientation;
	} // end setOrientation

	/**
	 * Initialise la sous-liste des DC qui equipent le DF
	 * 
	 * @param _lesDC
	 *            la sous-liste des DC
	 */
	public void setLesDC(SousListeDC _lesDC) {
		this.lesDC = _lesDC;
	} // end setLesDC

	/**
	 * Initialise la sous-liste des taxons sur lequels portent le DF
	 * 
	 * @param _lesTaxonsDeDestination
	 *            la sous-liste des taxons
	 */
	public void setLesTaxonsDeDestination(ListeRefTaxon _lesTaxonsDeDestination) {
		this.lesTaxonsDeDestination = _lesTaxonsDeDestination;
	} // end setLesTaxonsDeDestination

	/**
	 * Initialise la sous-liste des types du DF. Dans la sous liste, la position
	 * du type indique son rang.
	 * 
	 * @param _lesTypesDeDF
	 *            la sous-liste des types
	 */
	public void setLesTypesDeDF(ListeRefTypeDF _lesTypesDeDF) {
		this.lesTypesDeDF = _lesTypesDeDF;
	} // end setLesTypesDeDF

	/**
	 * Retourne les attributs du df sous forme textuelle
	 * 
	 * @return ret un string
	 */
	public String toString() {

		String ret = this.code;
		ret = org.apache.commons.lang3.StringUtils.abbreviate(ret, 30);
		return ret;
	}

	/**
	 * Arrete le DF (fixe la date de suppression...), arrete aussi les DC
	 * associ�s au DF
	 * 
	 * @param d
	 *            : la date de suppression du DF
	 * @throws Exception
	 *             Erreur
	 * @throws DataFormatException
	 *             Format des donn�es incorrect
	 * @throws SQLException
	 *             Erreur avec le BDD
	 * @throws ClassNotFoundException
	 *             Classe non trouv�e
	 */
	public void stop(Date d) throws Exception, DataFormatException, SQLException, ClassNotFoundException {
		// cr�ation de la sousListe pour charger les informations sur le DC
		SousListeDC sldc = new SousListeDC(this);

		// chargement des DC
		sldc.chargeSansFiltre();

		// parcourt de l'ensemble des DC pour mettre � jour leur date de fin
		for (int i = 0; i < sldc.size(); i++) {
			DC dc = (DC) sldc.get(i);
			sldc.chargeObjet(dc);// chargement de l'objet complet
			dc.stop(d);
		}

		// s'il n'y a aucune date de suppression, on en met une
		if (super.getDateSuppression() == null)
			super.setDateSuppression(d);

		// v�rification des attributs et mise a jour...
		this.verifAttributs();
		this.majObjet();
	}

} // end DF
