/*
 **********************************************************************
 *
 * Nom fichier :        EmplacementGeographique.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */


package infrastructure;


import commun.* ;
import java.awt.image.BufferedImage ;


/**
 * Classe abstraite utilisable pour un emplacement physique. Par exemple, une station de controle.
 * @author C�dric Briand
 */
public abstract class EmplacementGeographique implements IBaseDonnees {

    
  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////

    protected String        libelle                 ; 
    protected String        localisation            ; 
    protected Integer       coordonneeX             ; 
    protected Integer       coordonneeY             ; 
    protected Short         altitude                ; 
    protected BufferedImage carteLocalisation       ; 
    protected SousListe     lesElementsConstitutifs ; 


    
    
    
  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    
    
/**
 * Construit un emplacement geographique avec seulement un libelle
 * @param _libelle le libelle 
 */    
    public EmplacementGeographique(String _libelle) {
        this(_libelle, null, null, null, null, null) ;
    } // end EmplacementGeographique
   
    
/**
 * Construit un emplacement geographique avec tous ses attributs
 * @param _libelle le libelle 
 * @param _localisation localisation
 * @param _coordonneeX coordonn�e X
 * @param _coordonneeY coordonn�e Y
 * @param _altitude altitude
 * @param _carteLocalisation carte de localisation
 */    
    public EmplacementGeographique(String _libelle, String _localisation, Integer _coordonneeX, Integer _coordonneeY, Short _altitude, BufferedImage _carteLocalisation) {
        this.setLibelle (_libelle) ;
        this.setLocalisation (_localisation) ;
        this.setCoordonneeX (_coordonneeX) ;
        this.setCoordonneeY (_coordonneeY) ;
        this.setAltitude (_altitude) ;
        this.setCarteLocalisation (_carteLocalisation) ;

    } // end EmplacementGeographique
    
  
    
    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////

/**
 * Retourne le libelle
 * @return le libelle
 */
    public String getLibelle() {        
        return this.libelle;
    } // end getLibelle     
    
    
/**
 * Retourne la localisation
 * @return la localisation
 */
    public String getLocalisation() {        
        return this.localisation;
    } // end getLocalisation   
    

/**
 * Retourne la coordonnee X
 * @return la coordonnee X
 */
    public Integer getCoordonneeX() {        
        return this.coordonneeX;
    } // end getCoordonneeX        

/**
 * Retourne la coordonnee Y
 * @return la coordonnee Y
 */
    public Integer getCoordonneeY() {        
        return this.coordonneeY;
    } // end getCoordonneeY        
 
 
/**
 * Retourne l'altitude
 * @return l'altitude
 */
    public Short getAltitude() {        
        return this.altitude;
    } // end getAltitude        
   

/**
 * Retourne la carte de localisation
 * @return la carte de localisation
 */
    public BufferedImage getCarteLocalisation() {        
        return this.carteLocalisation;
    } // end getCarteLocalisation        

 
/**
 * Retourne la sous-liste des elements rattaches
 * @return la sous-liste
 */
    public SousListe getLesElementsConstitutifs() {        
        return this.lesElementsConstitutifs;
    } // end getLesElementsConstitutifs        
   
    
/**
 * Initialise le libelle
 * @param _libelle le libelle
 */
    public void setLibelle(String _libelle) {        
        this.libelle = _libelle;
    } // end setLibelle        

     

/**
 * Initialise la localisation
 * @param _localisation la localisation
 */
    public void setLocalisation(String _localisation) {        
        this.localisation = _localisation;
    } // end setLocalisation        

/**
 * Initialise la coordonnee X
 * @param _coordonneeX la coordonnee X
 */
    public void setCoordonneeX(Integer _coordonneeX) {        
        this.coordonneeX = _coordonneeX;
    } // end setCoordonneeX        


/**
 * Initialise la coordonnee Y
 * @param _coordonneeY la coordonnee Y
 */
    public void setCoordonneeY(Integer _coordonneeY) {        
        this.coordonneeY = _coordonneeY;
    } // end setCoordonneeY        

/**
 * Initialise l'altitude
 * @param _altitude l'altitude
 */
    public void setAltitude(Short _altitude) {        
        this.altitude = _altitude;
    } // end setAltitude        

/**
 * Initialise la carte de localisation
 * @param _carteLocalisation la carte de localisation
 */
    public void setCarteLocalisation(BufferedImage _carteLocalisation) {        
        this.carteLocalisation = _carteLocalisation;
    } // end setCarteLocalisation        

/**
 * Initialise la sous-liste
 * @param _lesElementsConstitutifs la sous-liste
 */
    public void setLesElementsConstitutifs(SousListe _lesElementsConstitutifs) {        
        this.lesElementsConstitutifs = _lesElementsConstitutifs;
    }
    
    public void effaceObjet() throws java.sql.SQLException, ClassNotFoundException {
    }
    
 // end setLesElementsConstitutifs        

 } // end EmplacementGeographique



