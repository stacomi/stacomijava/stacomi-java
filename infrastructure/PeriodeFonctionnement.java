/*
 **********************************************************************
 *
 * Nom fichier :        PeriodeFonctionnement.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */


package infrastructure;


import infrastructure.referenciel.* ;
import commun.* ;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.DataFormatException;

/**
 * Periode de fonctionnement ou d'arret d'un dispositif
 * @author C�dric Briand
 */
public class PeriodeFonctionnement implements IBaseDonnees {

  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////

    private Dispositif              dispositif      ; 
    private Date                    dateDebut       ; 
    private Date                    dateFin         ; 
    private String                  commentaires    ; 
    private Boolean                 etat            ; 
    private RefTypeFonctionnement   type            ; 

    

  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    
/**
 * Construit une periode avec uniquement une date de debut, une date de fin et un etat 
 * @param _dateDebut date de d�but de la p�riode de fonctionnement
 * @param _dateFin date de fin de la p�riode de fonctionnement
 * @param _etat �tat
 */    
    public PeriodeFonctionnement(Date _dateDebut, Date _dateFin, Boolean _etat) {
        this( null, _dateDebut, _dateFin, null, _etat, null) ;
    } // end PeriodeFonctionnement
    
/**
 * Construit une periode avec tous ses attributs
 * @param _dispositif dispositif concern�
 * @param _dateDebut date de d�but de la p�riode de fonctionnement
 * @param _dateFin date de fin de la p�riode de fonctionnement
 * @param _commentaires commentaires
 * @param _etat �tat
 * @param _type type de fonctionnement
 */  
    public PeriodeFonctionnement(Dispositif _dispositif, Date _dateDebut, Date _dateFin, String _commentaires, Boolean _etat, RefTypeFonctionnement _type) {   
        this.setDispositif(_dispositif) ;
        this.setDateDebut(_dateDebut) ;
        this.setDateFin(_dateFin) ;
        this.setCommentaires(_commentaires) ;
        this.setEtat(_etat) ;
        this.setType(_type) ;       
    } // end PeriodeFonctionnement   
    
    
    
  ///////////////////////////////////////
  // interfaces
  ///////////////////////////////////////
    
    /**
     * Insertion d'une periode de fonctionnemennt dans la BDD 
     */
    public void insertObjet() throws SQLException, ClassNotFoundException {
		String table = this.getNomTable();
		ArrayList nomAttributs = this.getNomAttributs();
		ArrayList valAttributs = this.getValeurAttributs();
		ConnexionBD.getInstance().executeInsert(table, nomAttributs, valAttributs);
    }
    
	public void majObjet() { }
    
    /**
     * Met a jour une periode de fonctionnement dans la BDD
     * Cette fonction est indispensable car la cl� primaire d'une p�riode de fonctionnement
     * est le triplet (per_dis_identifiant, per_date_debut, per_date_fin)
     * @param oldpf : l'ancienne periode de fonctionnement (celle qu'on modifie)
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public void majObjet(PeriodeFonctionnement oldpf)throws SQLException, ClassNotFoundException 
    {
		String table = this.getNomTable();
		ArrayList<String> nomAttributs = this.getNomAttributs();
		ArrayList<Object> valAttributs = this.getValeurAttributs();

		String[] nomSelection = oldpf.getNomID();
		Object[] valSelection = oldpf.getValeurID();
		
		ConnexionBD.getInstance().executeUpdate(table, 
				nomSelection, valSelection, 
				nomAttributs, valAttributs);
    }

    /**
     * Efface 1 periode de fonctionnement dans la BDD
     */
    public void effaceObjet() throws SQLException, ClassNotFoundException
    {
    	String table = this.getNomTable();
		String[] nomSelection = this.getNomID();
		Object[] valSelection = this.getValeurID();
    	ConnexionBD.getInstance().executeDelete(table, 
    			nomSelection, valSelection);
    }
        
    /**
     * Verifie les attributs d'une periode de fonctionnement
     */
    public boolean verifAttributs() throws DataFormatException {
    	// v�rification du code de dispositif
    	if(!Verification.isInteger(this.getDispositif().getIdentifiant(), 0, true))
    		throw new DataFormatException(Erreur.S13005);
    	
    	// Comparaison des dates
    	if(!Verification.isDateInf(this.dateDebut, this.dateFin, true, true))
    		throw new DataFormatException(Erreur.S13007);
    	
    	// verification du commentaire
		if (!Verification.isText(this.commentaires, 0, false))
			throw new DataFormatException(Erreur.S13006);
		
//		// verification de l'etat de fonctionnement
//		if (!Verification.isBool(this.etat, 0, false))
//			throw new DataFormatException(Erreur.S10006);
		
		// verification du code de fonctionnement
		if (!Verification.isRefTypeFonctionnement(this.getType(), false))
			throw new DataFormatException(Erreur.S13003);

		return true;
    }    
    
    
    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////

    
/**
 * Retourne le dispositif concerne par la periode
 * @return le dispositif
 */
    public Dispositif getDispositif() {        
        return this.dispositif ;
    } // end getDispositif        

/**
 * Retourne la date de debut de la periode
 * @return la date de debut
 */
    public Date getDateDebut() {        
        return this.dateDebut;
    } // end getDateDebut        

/**
 * Retourne la date de fin de la periode
 * @return la date de fin
 */
    public Date getDateFin() {        
        return this.dateFin;
    } // end getDateFin        

/**
 * Retourne les commentaires sur la periode
 * @return les commentaires
 */
    public String getCommentaires() {        
        return this.commentaires;
    } // end getCommentaires        

/**
 * Retourne l'etat de fonctionnement
 * @return True si le dispositif est en fonctionnement, False s'il est a l'arret
 */
    public Boolean getEtat() {        
        return this.etat;
    } // end isEtat        

/**
 * Retourne le type de fonctionnement ou d'arret
 * @return le type
 */
    public RefTypeFonctionnement getType() {        
        return this.type;
    } // end getType        
    
    

/**
 * Initialise le dispositif concerne par la periode
 * @param _dispositif le dispositif 
 */
    public void setDispositif(Dispositif _dispositif) {        
        this.dispositif = _dispositif ;
    } // end setDispositif   

/**
 * Initialise la date de debut de la periode
 * @param _dateDebut la date de debut
 */
    public void setDateDebut(Date _dateDebut) {        
        this.dateDebut = _dateDebut;
    } // end setDateDebut     
    
/**
 * Initialise la date de fin de la periode
 * @param _dateFin la date de fin
 */
    public void setDateFin(Date _dateFin) {        
        this.dateFin = _dateFin;
    } // end setDateFin        

/**
 * Initialise les commentaires sur la periode
 * @param _commentaires les commentaires 
 */
    public void setCommentaires(String _commentaires) {        
        this.commentaires = _commentaires;
    } // end setCommentaires      
    
/**
 * Initialise l'etat de fonctionnement
 * @param _etat True si le dispositif est en fonctionnement, False s'il est a l'arret
 */
    public void setEtat(Boolean _etat) {        
        this.etat = _etat;
    } // end setEtat      
     
/**
 * Initialise le type de fonctionnement ou d'arret
 * @param _type le type
 */
    public void setType(RefTypeFonctionnement _type) {        
        this.type = _type;
    }
    

    /**
     * Retourne les attributs de la p�riode de fonctionnement 
     * sous forme textuelle
     * @return les attributs de la periode de fonctionnement 
     * sous forme textuelle
     */
    public String toString() {        
        SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yy HH:mm:ss") ;
        String ret = simpleDate.format(this.dateDebut) + " -> " + simpleDate.format(this.dateFin) ;
        ret += " | ";
        if(this.etat.booleanValue())
        	ret += " M ";
        else
        	ret += " A ";
        ret += " | " + this.type.getCode();
        if(this.commentaires!=null)
        	ret += " |  " + this.commentaires;
        return ret;
    }
        
    /**
     * Cr�� 1 liste avec les valeurs des parametres de la table t_periodefonctdispositif_per
     * @return la liste avec les valeurs des parametres de la table t_periodefonctdispositif_per
     */
	private ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> res = new ArrayList<Object>();
		res.add(this.getDispositif().getIdentifiant());
		res.add(this.getDateDebut());
		res.add(this.getDateFin());
		res.add(this.getCommentaires());
		res.add(this.getEtat());
		res.add(this.getType().getCode());
		return res;
	}

	/**
	 * Cr�� 1 liste avec les noms des parametres de la table t_periodefonctdispositif_per
	 * @return la liste avec les noms des parametres de la table t_periodefonctdispositif_per
	 */
	private ArrayList<String> getNomAttributs() {
		ArrayList<String> res = new ArrayList<String>();
		res.add("per_dis_identifiant");
		res.add("per_date_debut");
		res.add("per_date_fin");
		res.add("per_commentaires");
		res.add("per_etat_fonctionnement");
		res.add("per_tar_code");
		res.add("per_org_code");
		
		return res;
	}

	/**
	 * Nom de la table dans laquelle sont �crites les informations 
	 * d'une periode de fonctionnement
	 */
	private String getNomTable() {
		return "t_periodefonctdispositif_per";
	}
        
	/**
	 * Recup�ration des noms des champs d'une p�riode de fonctionnement 
	 * @return les noms des champs d'une p�riode de fonctionnement 
	 */
	private String[] getNomID()
	{
		String[] res = new String[3];
		res[0] = "per_dis_identifiant";
		res[1] = "per_date_debut";
		res[2] = "per_date_fin";
		return res;
	}
	
	/**
	 * Recup�ration des valeurs des champs d'une p�riode de fonctionnement 
	 * @return les valeurs des champs d'une p�riode de fonctionnement 
	 */
	private Object[] getValeurID()
	{
		Object[] res = new Object[3];
		res[0] = this.dispositif.getIdentifiant();
		res[1] = this.dateDebut;
		res[2] = this.dateFin;
		return res;
	}
	
 // end setType        

 } // end PeriodeFonctionnement



