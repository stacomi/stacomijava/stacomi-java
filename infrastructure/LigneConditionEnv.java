/*
 **********************************************************************
 *
 * Nom fichier :        LigneConditionEnv.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            C�dric Briand
 * Date de creation :   Septembre 2008
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */
package infrastructure;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.SimpleTimeZone;

import commun.Erreur;

/**
 * Cette classe sert notamment � v�rifier l'int�grit� des champs du fichier condition environnementale
 * @author S�bastien Laigre
 */
public class LigneConditionEnv {

	// /////////////////////////////////////
	// attributes
	// /////////////////////////////////////
	private String fichierCE;

	private Integer ligne;

	private String stationMesure;
	
	private Date dateDebut;
	
	private Date dateFin;
	
	private Float valeurParametre;
	
	private String methodeObtention;
	
	// /////////////////////////////////////
	// constructors
	// /////////////////////////////////////

	/**
	 * Construit une LigneConditionEnv
	 * @param fichierCE : le fichier contenant les donn�es
	 * @param ligne : l'indice de la ligne
	 * @param stationMesure : la station de mesure
	 * @param methodeObtention m�thode d'obtention
	 * @param dateDebut : la date de d�but de la CE
	 * @param dateFin : la date de fin de la CE
	 * @param valeurParam : la valeur du param�tre
	 */
	public LigneConditionEnv(String fichierCE, Integer ligne, String stationMesure, String methodeObtention, Date dateDebut, Date dateFin, Float valeurParam) {
		this.fichierCE = fichierCE;
		this.ligne = ligne;
		this.stationMesure = stationMesure;
		this.methodeObtention = methodeObtention;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.valeurParametre = valeurParam;
	}

	/**
	 * Initialise une ligne (mais pas ses attributs). L'initialisation des
	 * attributs se fera par appel de la fonction "lireLigne()"
	 */
	public LigneConditionEnv() { }

	// /////////////////////////////////////
	// operations
	// /////////////////////////////////////

	/**
	 * Lis une ligne d'un fichier csv contenant les donn�es environnementales
	 * @param line la ligne avec ses informations correspondantes
	 * @throws Exception 
	 */
	public void lireLigne(String line) throws Exception {

		Scanner scan_line = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		java.util.Calendar cal = Calendar.
		getInstance(new SimpleTimeZone(0, "GMT"));
		sdf.setCalendar(cal);
		
		// lecture des champs de la ligne
		scan_line = new Scanner(line);
		scan_line.useDelimiter(";");// normalement ";"
		
		if (scan_line.hasNext())
			this.stationMesure = scan_line.next();
		else
			throw new InputMismatchException(Erreur.V1001);// String attendu
		
		if(scan_line.hasNext())
	         dateDebut = sdf.parse(scan_line.next());// new Date(date);
		else
			throw new InputMismatchException(Erreur.V1008);// Date attendue
		
		if(scan_line.hasNext())
			this.dateFin = sdf.parse(scan_line.next());
		else
			throw new InputMismatchException(Erreur.V1008);// Date attendue
		
		if(scan_line.hasNext())
			this.methodeObtention = scan_line.next();
		else
			throw new InputMismatchException(Erreur.V1001);// Float attendue
		
		if(scan_line.hasNext())
			this.valeurParametre = Float.parseFloat(scan_line.next());
		else
			throw new InputMismatchException(Erreur.V1003);// Float attendue
	}

	/**
	 * @return le fichier
	 */
	public String getFichierCE() {
		return fichierCE;
	}
	
	/**
	 * @return la ligne
	 */
	public Integer getLigne() {
		return ligne;
	}
	
	/**
	 * @return la station de mesure
	 */
	public String getStationMesure()
	{
		return this.stationMesure;
	}
	
	/**
	 * @return la date de d�but de la ligne
	 */
	public Date getDateDebut() {
		return dateDebut;
	}
	
	/**
	 * @return la date de fin de la ligne
	 */
	public Date getDateFin() {
		return dateFin;
	}

	/**
	 * @return la valeur du parametre
	 */
	public Float getValeurParametre() {
		return valeurParametre;
	}

	/**
	 * @param fichierCE fichier contenant les CE
	 */
	public void setFichierVideo(String fichierCE) {
		this.fichierCE = fichierCE;
	}

	/**
	 * @param ligne ligne
	 */
	public void setLigne(Integer ligne) {
		this.ligne = ligne;
	}
	
	/**
	 * @param stationMesure station de mesure
	 */
	public void setStationMesure(String stationMesure)
	{
		this.stationMesure = stationMesure;
	}
	
	/**
	 * @param date date de d�but
	 */
	public void setDateDebut(Date date) {
		this.dateDebut = date;
	}
	
	/**
	 * @param date date de fin le la ligne
	 */
	public void setDateFin(Date date) {
		this.dateFin = date;
	}
	
	/**
	 * @param val valeur du parametre
	 */
	public void setValeurParametre(Float val) {
		valeurParametre = val;
	}
	
	/**
	 * @return la m�thode d'obtention
	 */
	public String getMethodeObtention()
	{
		return this.methodeObtention;
	}
	
	/**
	 * @param methodeObtention m�thode d'obtention
	 */
	public void setMethodeObtention(String methodeObtention)
	{
		this.methodeObtention = methodeObtention;
	}
}
