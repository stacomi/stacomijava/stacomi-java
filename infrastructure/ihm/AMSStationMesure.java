/*
 **********************************************************************
 *
 * Nom fichier :        AMSStationMesure.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             S�bastien Laigre
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   27 mai 2009, 10:00
 * Compatibilite :      Java6/ Windows XP/ PostgreSQL 8.3
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */
package infrastructure.ihm;

import commun.*;
import infrastructure.*;
import infrastructure.referenciel.ListeRefParamQualEnv;
import infrastructure.referenciel.ListeRefParamQuantEnv;
import infrastructure.referenciel.ListeStations;
import infrastructure.referenciel.SousListeStationMesure;
import migration.referenciel.RefParametre;
import migration.referenciel.RefParametreQualitatif;
import migration.referenciel.RefParametreQuantitatif;

import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.awt.GridBagConstraints;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.MouseEvent;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

/**
 *  Classe permettant l'ajout/modification/suppression d'une station de mesure dans la base
 * @author S�bastien Laigre
 */
public class AMSStationMesure extends javax.swing.JPanel {

	/** */
	private static final long serialVersionUID = 1L;

	
	/**
	 * Creates new form AMSStationMesure constructeur
	 */
	public AMSStationMesure() {
		this.initComponents();
		this.chargeListes();
	}
	
	
	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	// <editor-fold defaultstate="collapsed" desc="Generated
	// Code">//GEN-BEGIN:initComponents
	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		center = new javax.swing.JPanel();
		top = new javax.swing.JPanel();
		titre = new javax.swing.JLabel();
		resultat = new javax.swing.JLabel();
		bottom = new javax.swing.JPanel();

		setBackground(new java.awt.Color(255, 255, 255));
		setPreferredSize(new java.awt.Dimension(600, 400));
		setLayout(new java.awt.BorderLayout());

		center.setBackground(new java.awt.Color(230, 230, 230));
		center.setPreferredSize(new Dimension(100, 100));
		center.setLayout(new java.awt.GridBagLayout());

		add(center, java.awt.BorderLayout.CENTER);

		top.setLayout(new java.awt.BorderLayout());

		titre.setBackground(new java.awt.Color(0, 51, 153));
		titre.setText(Messages.getString("AMSStationMesure.Titre"));
		titre.setFont(new java.awt.Font("Dialog", 1, 14));
		titre.setForeground(new java.awt.Color(255, 255, 255));
		titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		titre.setOpaque(true);
		top.add(titre, java.awt.BorderLayout.CENTER);
		titre.getAccessibleContext().setAccessibleName(Messages.getString("AMSStationMesure.Titre"));

		resultat.setBackground(new java.awt.Color(255, 255, 255));
		resultat.setForeground(new java.awt.Color(255, 0, 0));
		resultat.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		resultat.setOpaque(true);
		top.add(resultat, java.awt.BorderLayout.SOUTH);

		add(top, java.awt.BorderLayout.NORTH);

		bottom.setBackground(new java.awt.Color(191, 191, 224));
		bottom.add(getJButton_AjouterModifier());
		bottom.add(getJButton_Supprimer());
		
		
		
		jLabel_station = new JLabel();
		jComboBox_Station = new javax.swing.JComboBox();

		jLabel_listeStationMesure = new JLabel();
		jScrollPane_stationMesure = new JScrollPane();
		jList_stationMesure = new JList();

		jLabel_TypeDeParametre = new JLabel();
		JRadioButton_type_quantitatif = new JRadioButton();
		JRadioButton_type_qualitatif = new JRadioButton();
		buttonGroup_type_parametre = new ButtonGroup();
		
		jLabel_parametre = new JLabel();

		jComboBox_parametreQualEnv = new JComboBox();
		jComboBox_parametreQuantEnv = new JComboBox();

		jLabel_libelle = new JLabel();
		jTextField_libelle = new JTextFieldDataType();
		
		jLabel_description = new JLabel();
		jScrollPane_description = new JScrollPane();
		jTextField_description = new JTextAreaDataType();
		
		jButton_AjouterModifier = new javax.swing.JButton();
		jButton_Supprimer = new javax.swing.JButton();
		jButton_annuler = new javax.swing.JButton();
		

		jButton_annuler.setText(Messages.getString("Annuler"));
		jButton_annuler.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AMSStationMesure.this.chargeStationMesure();
				AMSStationMesure.this.clearFields();
			}
		});
		jButton_annuler.setToolTipText(Messages.getString("AMSStationMesure.Annuler"));
		bottom.add(jButton_annuler);
		
		add(bottom, java.awt.BorderLayout.SOUTH);
		
		jComboBox_Station.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AMSStationMesure.this.chargeStationMesure();
			}
		});
		
		jComboBox_Station.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				liste_StationMesureMouseClicked(evt);
			}
		
			private void liste_StationMesureMouseClicked(MouseEvent evt) {
				try
				{
					AMSStationMesure.this.chargeStationMesure();
				}
				catch (Exception ex) {
					Logger.getLogger(AMSPeriodeFonctionnement.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});
		
		jScrollPane_stationMesure = new JScrollPane();
		jScrollPane_stationMesure.setPreferredSize(new Dimension(400, 150));
		jScrollPane_stationMesure.setViewportView(jList_stationMesure);
		jList_stationMesure.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		jList_stationMesure.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				listePeriodeMouseClicked(evt);
			}
		
			private void listePeriodeMouseClicked(MouseEvent evt) {
				try{
					chargeInformationStationMesure();
				}
				catch (Exception ex) {
					Logger.getLogger(AMSPeriodeFonctionnement.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});
		
		// placement des composants dans center...
		jLabel_station.setText(Messages.getString("Station"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 10;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,5);
		center.add(jLabel_station, gridBagConstraints);
		
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 10;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,5);
		center.add(jComboBox_Station, gridBagConstraints);

		jLabel_listeStationMesure.setText(Messages.getString("ListeStationMesures"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,5);
		center.add(jLabel_listeStationMesure , gridBagConstraints);
		
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 10;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,5);
		center.add(jScrollPane_stationMesure , gridBagConstraints);

		jLabel_libelle.setText(Messages.getString("Libelle"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,5);
		center.add(jLabel_libelle , gridBagConstraints);

		jTextField_libelle.setPreferredSize(new Dimension(120, 20));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,10);
		center.add(jTextField_libelle , gridBagConstraints);
		
		jLabel_TypeDeParametre.setText(Messages.getString("TypeDeParametre"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,5);
		center.add(jLabel_TypeDeParametre , gridBagConstraints);
		
		buttonGroup_type_parametre.add(JRadioButton_type_qualitatif);
		buttonGroup_type_parametre.add(JRadioButton_type_quantitatif);
		
		this.JRadioButton_type_qualitatif.setText(Messages.getString("Qualitatif"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,5);
		center.add(JRadioButton_type_qualitatif , gridBagConstraints);
		
		this.JRadioButton_type_quantitatif.setText(Messages.getString("Quantitatif"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,5);
		center.add(JRadioButton_type_quantitatif , gridBagConstraints);
        
        this.JRadioButton_type_quantitatif.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AMSStationMesure.this.type_quantitatifActionPerformed();
            }
        });

        this.JRadioButton_type_qualitatif.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	AMSStationMesure.this.type_qualitatifActionPerformed();
            }
        });
        
        jLabel_parametre.setText(Messages.getString("Parametre"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,5);
		center.add(jLabel_parametre , gridBagConstraints);
		
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,5);
		center.add(jComboBox_parametreQualEnv , gridBagConstraints);
		
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 4;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,5);
		center.add(jComboBox_parametreQuantEnv , gridBagConstraints);
        
		jLabel_description.setText(Messages.getString("Description"));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,5);
		center.add(jLabel_description , gridBagConstraints);
		
		jTextField_description.setColumns(30);
		jTextField_description.setLineWrap(true);
		jTextField_description.setRows(4);
		jScrollPane_description.setViewportView(jTextField_description);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 5;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3,5,3,5);	
		center.add(jScrollPane_description , gridBagConstraints);
		
	}// </editor-fold>//GEN-END:initComponents

	/**
	 * Action effectu�e lorsque l'on clique sur un param�tre qualitatif
	 */
	private void type_qualitatifActionPerformed() {
		jComboBox_parametreQualEnv.setModel(new DefaultComboBoxModel(this.lesParamQualitatifsEnv.toArray()));
		jComboBox_parametreQualEnv.setVisible(true);
		jComboBox_parametreQuantEnv.setVisible(false);
	}

	/**
	 * Action effectu�e lorsque l'on clique sur un param�tre quantitatif
	 */
	private void type_quantitatifActionPerformed() {
		jComboBox_parametreQuantEnv.setModel(new DefaultComboBoxModel(this.lesParamQuantitatifsEnv.toArray()));
		jComboBox_parametreQualEnv.setVisible(false);
		jComboBox_parametreQuantEnv.setVisible(true);
	}

	private javax.swing.JPanel bottom = null;
	private javax.swing.JPanel center = null;
	private javax.swing.JPanel top = null;

	private javax.swing.JLabel resultat = null;
	private javax.swing.JLabel titre = null;

	private javax.swing.JLabel jLabel_station = null;
	private javax.swing.JComboBox jComboBox_Station = null;
	private javax.swing.JLabel jLabel_listeStationMesure = null;
	private javax.swing.JScrollPane jScrollPane_stationMesure = null;
	private javax.swing.JList jList_stationMesure = null;

	private javax.swing.JLabel jLabel_TypeDeParametre = null;
	private javax.swing.JRadioButton JRadioButton_type_quantitatif = null;
	private javax.swing.JRadioButton JRadioButton_type_qualitatif = null;
	private javax.swing.ButtonGroup buttonGroup_type_parametre = null;
	
	private javax.swing.JLabel jLabel_parametre = null;
	private javax.swing.JComboBox jComboBox_parametreQualEnv = null;
	private javax.swing.JComboBox jComboBox_parametreQuantEnv = null;

	private javax.swing.JLabel jLabel_libelle = new JLabel();
	private commun.JTextFieldDataType jTextField_libelle = null;
	
	private javax.swing.JLabel jLabel_description = null;
	private javax.swing.JScrollPane jScrollPane_description = null;
	private commun.JTextAreaDataType jTextField_description = null;
	
	private javax.swing.JButton jButton_AjouterModifier = null;
	private javax.swing.JButton jButton_Supprimer = null;
	private javax.swing.JButton jButton_annuler = null;

	// vecteur de string pour le pr�-remplissage des combo
	Vector<String> vectorParamQuant;
	Vector<String> vectorParamQual;

	ListeStations lesStations = null;
	ListeRefParamQualEnv lesParamQualitatifsEnv = null; // les param�tres qualitatifs
	ListeRefParamQuantEnv lesParamQuantitatifsEnv = null;  // les param�tres quantitatifs
	SousListeStationMesure lesStationMesure = null;
	
	/**
	 * Ajout/mise � jour d'une stationMesure dans la base
	 */
	private void ajouterModifier() 
	{
		String result = null;

		Station station = null;
		String libelle = null;
		RefParametre parametre = null;
		String description = null;
		
		// la station
		try { station = (Station)jComboBox_Station.getSelectedItem(); }
		catch(Exception e) { result = Erreur.S22000; }
		
		// le libell�
		try { libelle = jTextField_libelle.getText(); }
		catch(Exception e) { result = Erreur.S22001; }
		
		// le param�tre
		try 
		{
			if(JRadioButton_type_qualitatif.isSelected())
				parametre = (RefParametre)jComboBox_parametreQualEnv.getSelectedItem();
			else
				parametre = (RefParametre)jComboBox_parametreQuantEnv.getSelectedItem();
		}
		catch(Exception e) { result = Erreur.S22002; }
		
		// la description
		try { description = jTextField_description.getText(); }
		catch(Exception e) { result = Erreur.S22003; }
		
		if(result==null)
		{
			try
			{
				// si aucune stationMesure n'est selectionn�e : insertion 
				if(jList_stationMesure.getSelectedValue()==null)
				{
					StationMesure stm = new StationMesure(null, libelle, station, parametre, description);
					stm.verifAttributs();
					stm.insertObjet();
					result = Message.A1015;
					chargeStationMesure();
				}
				else // sinon mise a jour
				{
					StationMesure old = (StationMesure)jList_stationMesure.getSelectedValue();
					StationMesure stm = new StationMesure(old.getIdentifiant(), libelle, station, parametre, description);
					stm.verifAttributs();
					stm.majObjet();
					result = Message.M1015;
					chargeStationMesure();
				}
			}
			catch(Exception e)
			{
				result = e.getMessage();
			}
			finally
			{
				this.resultat.setText(result);
			}
		}
		else
		{
			this.resultat.setText(result);
		}
	}

	/**
	 * Suppression d'un enregistrement de StationMesure de la BDD
	 */
	private void supprimer() 
	{
		String result = null;
		
		// si aucune stationMesure n'est selectionn�e => erreur
		if(jList_stationMesure.getSelectedValue()!=null)
		{
			// la stationMesure selectionn�e par l'utilisateur
			StationMesure stm = (StationMesure)jList_stationMesure.getSelectedValue();
			try
			{
				// suppression et mise a jour de l'affichage
				stm.effaceObjet(); 
				result = Message.S1013;
				chargeStationMesure();
			}
			catch(Exception e)
			{
				result = e.getMessage();
			}
			finally
			{
				this.resultat.setText(result);
			}
		}
		else
		{
			this.resultat.setText(Erreur.S22004);
		}
	}

	/**
	 * Vide les champs de texte et cache les combo qui doivent l'etre...
	 */
	private void clearFields() 
	{
		jTextField_description.setText("");
		jTextField_libelle.setText("");
		jComboBox_parametreQualEnv.setVisible(false);
		jComboBox_parametreQuantEnv.setVisible(false);
		JRadioButton_type_qualitatif.setSelected(false);
		JRadioButton_type_quantitatif.setSelected(false);
	}

	/**
	 * Chargement les listes (param�tres, stations) de la bdd
	 */
	private void chargeListes() {
		
		this.lesStations = new ListeStations();
		this.lesParamQualitatifsEnv = Lanceur.getListeRefParamQualEnv();
		this.lesParamQuantitatifsEnv = Lanceur.getListeRefParamQuantEnv();
		this.vectorParamQuant = convertToVector(lesParamQuantitatifsEnv);
		this.vectorParamQual = convertToVector(lesParamQualitatifsEnv);
		try {
			this.lesStations.chargeSansFiltreDetails();
			this.jComboBox_Station.setModel(new DefaultComboBoxModel(this.lesStations.toArray()));
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1019);
		}
	}
	
	/**
	 * Charge les informations � propos d'une StationMesure
	 * c'est-a-dire sa StationMesure, date de d�but, fin etc.
	 */
	private void chargeStationMesure()
	{
		lesStationMesure = new SousListeStationMesure((Station)this.jComboBox_Station.getSelectedItem());
		
		try
		{
			lesStationMesure.chargeSansFiltreDetails();
			jList_stationMesure.setModel(new DefaultComboBoxModel(this.lesStationMesure.toArray()));
		}
		catch(Exception e)
		{
			this.resultat.setText(e.getMessage());
		}
	}

	/**
	 * Charge les informations d'une StationMesure lorsqu'elle est selectionn�e
	 */
	private void chargeInformationStationMesure()
	{
		StationMesure stm = (StationMesure)this.jList_stationMesure.getSelectedValue();
		
		jTextField_libelle.setText(stm.getLibelle());
		jTextField_description.setText(stm.getCommentaires());

		String code = stm.getParametre().getCode();
		try
		{
			if(RefParametre.estQualitatif(code))
			{
				this.type_qualitatifActionPerformed();
				this.JRadioButton_type_qualitatif.setSelected(true);
				int i = vectorParamQual.indexOf(code);
				this.jComboBox_parametreQualEnv.setSelectedIndex(i);
			}
			else
			{
				this.type_quantitatifActionPerformed();
				this.JRadioButton_type_quantitatif.setSelected(true);
				int i = vectorParamQuant.indexOf(code);
				this.jComboBox_parametreQuantEnv.setSelectedIndex(i);
			}
		}
		catch(Exception e)
		{
			this.resultat.setText(e.getMessage());
		}
		
	}

	/**
	 * Conversion d'une ListeRefParamQuantEnv en un vecteur de String (pour le pr�-remplissage des combo)
	 * @param la liste 
	 * @return le vecteur de String
	 */
	private Vector<String> convertToVector(ListeRefParamQuantEnv liste)
	{
		Vector<String> res = new Vector<String>();
		
		for(int i=0 ; i<liste.size(); i++)
			res.add(((RefParametreQuantitatif)liste.get(i)).getCode());
		
		return res;
	}
	
	/**
	 * Conversion d'une ListeRefParamQualEnv en un vecteur de String (pour le pr�-remplissage des combo)
	 * @param la liste 
	 * @return le vecteur de String
	 */
	private Vector<String> convertToVector(ListeRefParamQualEnv liste)
	{
		Vector<String> res = new Vector<String>();
		
		for(int i=0 ; i<liste.size(); i++)
			res.add(((RefParametreQualitatif)liste.get(i)).getCode());
		
		return res;
	}
	
	/**
	 * This method initializes jButton_Ajouter	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton_AjouterModifier() {
		if (jButton_AjouterModifier == null) {
			jButton_AjouterModifier = new JButton();
			jButton_AjouterModifier.setPreferredSize(new Dimension(150, 26));
			jButton_AjouterModifier.setText(Messages.getString("AjouterModifier"));
			jButton_AjouterModifier.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					AMSStationMesure.this.ajouterModifier();
				}
			});
			jButton_AjouterModifier.setToolTipText(Messages.getString("AMSStationMesure.AM"));
		}
		return jButton_AjouterModifier;
	}

	/**
	 * This method initializes jButton_Supprimer	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton_Supprimer() {
		if (jButton_Supprimer == null) {
			jButton_Supprimer = new JButton();
			jButton_Supprimer.setPreferredSize(new Dimension(100, 26));
			jButton_Supprimer.setText(Messages.getString("Supprimer"));
			jButton_Supprimer.setToolTipText(Messages.getString("AMSStationMesure.Supprimer"));
			jButton_Supprimer.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					AMSStationMesure.this.supprimer();
				}
			});
		}
		return jButton_Supprimer;
	}
}
