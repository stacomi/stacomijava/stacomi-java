/*
 **********************************************************************
 *
 * Nom fichier :        ModifierOuvrage.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             S�bastien Laigre
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   10 avril 2009, 09:58
 * Compatibilite :      Java6/ Windows XP/ PostgreSQL 8.3
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */
package infrastructure.ihm;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import commun.Erreur;
import commun.JTextAreaDataType;
import commun.JTextFieldDataType;
import commun.Message;
import infrastructure.Ouvrage;
import infrastructure.Station;
import infrastructure.referenciel.ListeOuvrage;
import infrastructure.referenciel.ListeRefNatureOuvrage;
import infrastructure.referenciel.ListeStations;
import infrastructure.referenciel.RefNatureOuvrage;
import infrastructure.referenciel.SousListeOuvrages;

/**
 * Modification d'un ouvrage
 * 
 * @author S�bastien Laigre
 */
public class ModifierOuvrage extends javax.swing.JPanel {

	/** */
	private static final long serialVersionUID = 1L;

	private void okActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_okActionPerformed
		try {
			this.valide();// GEN-LAST:event_okActionPerformed
		} catch (Exception ex) {
			Logger.getLogger(AjouterOuvrage.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void effacerActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_effacerActionPerformed
		this.reInitComponents();
	}// GEN-LAST:event_effacerActionPerformed

	private javax.swing.JLabel JLabelStation;
	private javax.swing.JPanel bottom;
	private javax.swing.JPanel center;
	private javax.swing.JButton effacer;
	private javax.swing.JComboBox liste_stations;
	private javax.swing.JButton ok;
	private javax.swing.JLabel resultat;
	private javax.swing.JLabel titre;
	private javax.swing.JPanel top;
	private JLabel JLabel_code = null;
	private JLabel JLabel_nom = null;
	private JLabel JLabel_localisation = null;
	private JLabel JLabel_coordonneeX = null;
	private JLabel JLabel_coordonneeY = null;
	private JLabel JLabel_altitude = null;
	private JLabel JLabel_carteLocalisation = null;
	private JLabel JLabel_nature = null;
	private JLabel JLabel_commentaires = null;
	private JTextFieldDataType JTextField_ouvrage_libelle = null;
	private JTextFieldDataType JTextField_ouvrage_localisation = null;
	private JTextFieldDataType JTextField_ouvrage_coordonneeX = null;
	private JTextFieldDataType JTextField_ouvrage_coordonneeY = null;
	private JTextFieldDataType JTextField_ouvrage_altitude = null;
	private JTextAreaDataType JTextField_ouvrage_commentaires = null;
	private JTextFieldDataType JTextField_ouvrage_code = null;
	private JComboBox jComboBox_Nature = null;
	private JLabel jLabel_Ouvrage = null;
	private JComboBox liste_ouvrages = null;
	private JScrollPane jScrollPane = null;

	// listes a� charger
	private ListeStations lesStations; // @jve:decl-index=0:
	private SousListeOuvrages lesOuvrages;
	private ListeRefNatureOuvrage listeRefNatureOuvrage;

	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		GridBagConstraints gridBagConstraints32 = new GridBagConstraints();
		gridBagConstraints32.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints32.gridy = 1;
		gridBagConstraints32.insets = new Insets(2, 0, 2, 0);
		gridBagConstraints32.anchor = GridBagConstraints.WEST;
		gridBagConstraints32.gridx = 1;
		GridBagConstraints gridBagConstraints24 = new GridBagConstraints();
		gridBagConstraints24.gridx = 0;
		gridBagConstraints24.anchor = GridBagConstraints.WEST;
		gridBagConstraints24.gridy = 1;
		gridBagConstraints24.insets = new Insets(2, 10, 2, 10);
		jLabel_Ouvrage = new JLabel();
		jLabel_Ouvrage.setText(Messages.getString("Ouvrage"));
		GridBagConstraints gridBagConstraints13 = new GridBagConstraints();
		gridBagConstraints13.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints13.gridy = 11;
		gridBagConstraints13.insets = new Insets(2, 0, 2, 0);
		gridBagConstraints13.anchor = GridBagConstraints.WEST;
		gridBagConstraints13.gridx = 1;
		GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
		gridBagConstraints1.gridx = 1;
		gridBagConstraints1.anchor = GridBagConstraints.WEST;
		gridBagConstraints1.gridy = 2;
		GridBagConstraints gridBagConstraints26 = new GridBagConstraints();
		gridBagConstraints26.fill = GridBagConstraints.BOTH;
		gridBagConstraints26.gridy = 12;
		gridBagConstraints26.gridx = 1;
		GridBagConstraints gridBagConstraints23 = new GridBagConstraints();
		gridBagConstraints23.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints23.gridy = 10;
		gridBagConstraints23.anchor = GridBagConstraints.WEST;
		gridBagConstraints23.insets = new Insets(2, 0, 2, 0);
		gridBagConstraints23.gridx = 1;
		GridBagConstraints gridBagConstraints20 = new GridBagConstraints();
		gridBagConstraints20.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints20.gridy = 7;
		gridBagConstraints20.anchor = GridBagConstraints.WEST;
		gridBagConstraints20.insets = new Insets(2, 0, 2, 0);
		gridBagConstraints20.gridx = 1;
		GridBagConstraints gridBagConstraints19 = new GridBagConstraints();
		gridBagConstraints19.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints19.gridy = 6;
		gridBagConstraints19.anchor = GridBagConstraints.WEST;
		gridBagConstraints19.insets = new Insets(2, 0, 2, 0);
		gridBagConstraints19.gridx = 1;
		GridBagConstraints gridBagConstraints18 = new GridBagConstraints();
		gridBagConstraints18.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints18.gridy = 5;
		gridBagConstraints18.anchor = GridBagConstraints.WEST;
		gridBagConstraints18.insets = new Insets(2, 0, 2, 0);
		gridBagConstraints18.gridx = 1;
		GridBagConstraints gridBagConstraints17 = new GridBagConstraints();
		gridBagConstraints17.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints17.gridy = 4;
		gridBagConstraints17.anchor = GridBagConstraints.WEST;
		gridBagConstraints17.insets = new Insets(2, 0, 2, 0);
		gridBagConstraints17.gridx = 1;
		GridBagConstraints gridBagConstraints15 = new GridBagConstraints();
		gridBagConstraints15.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints15.gridy = 3;
		gridBagConstraints15.anchor = GridBagConstraints.WEST;
		gridBagConstraints15.insets = new Insets(2, 0, 2, 0);
		gridBagConstraints15.gridwidth = 1;
		gridBagConstraints15.gridx = 1;
		GridBagConstraints gridBagConstraints121 = new GridBagConstraints();
		gridBagConstraints121.gridx = 0;
		gridBagConstraints121.anchor = GridBagConstraints.NORTHWEST;
		gridBagConstraints121.gridy = 12;
		gridBagConstraints121.insets = new Insets(2, 10, 2, 10);
		JLabel_commentaires = new JLabel();
		JLabel_commentaires.setText(Messages.getString("Commentaires"));
		GridBagConstraints gridBagConstraints111 = new GridBagConstraints();
		gridBagConstraints111.gridx = 0;
		gridBagConstraints111.anchor = GridBagConstraints.WEST;
		gridBagConstraints111.gridy = 11;
		gridBagConstraints111.insets = new Insets(2, 10, 2, 10);
		JLabel_nature = new JLabel();
		JLabel_nature.setText(Messages.getString("Nature"));
		GridBagConstraints gridBagConstraints10 = new GridBagConstraints();
		gridBagConstraints10.gridx = 0;
		gridBagConstraints10.anchor = GridBagConstraints.WEST;
		gridBagConstraints10.gridy = 10;
		gridBagConstraints10.insets = new Insets(2, 10, 2, 10);
		GridBagConstraints gridBagConstraints8 = new GridBagConstraints();
		gridBagConstraints8.gridx = 0;
		gridBagConstraints8.anchor = GridBagConstraints.WEST;
		gridBagConstraints8.gridy = 8;
		gridBagConstraints8.insets = new Insets(2, 10, 2, 10);
		JLabel_carteLocalisation = new JLabel();
		JLabel_carteLocalisation.setText(Messages.getString("CarteDeLocalisation"));
		GridBagConstraints gridBagConstraints71 = new GridBagConstraints();
		gridBagConstraints71.gridx = 0;
		gridBagConstraints71.anchor = GridBagConstraints.WEST;
		gridBagConstraints71.gridy = 7;
		gridBagConstraints71.insets = new Insets(2, 10, 2, 10);
		JLabel_altitude = new JLabel();
		JLabel_altitude.setText(Messages.getString("Altitude"));
		GridBagConstraints gridBagConstraints61 = new GridBagConstraints();
		gridBagConstraints61.gridx = 0;
		gridBagConstraints61.anchor = GridBagConstraints.WEST;
		gridBagConstraints61.gridy = 6;
		gridBagConstraints61.insets = new Insets(2, 10, 2, 10);
		JLabel_coordonneeY = new JLabel();
		JLabel_coordonneeY.setText(Messages.getString("CoordonneeY"));
		GridBagConstraints gridBagConstraints51 = new GridBagConstraints();
		gridBagConstraints51.gridx = 0;
		gridBagConstraints51.anchor = GridBagConstraints.WEST;
		gridBagConstraints51.gridy = 5;
		gridBagConstraints51.insets = new Insets(2, 10, 2, 10);
		JLabel_coordonneeX = new JLabel();
		JLabel_coordonneeX.setText(Messages.getString("CoordonneeX"));
		GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
		gridBagConstraints4.gridx = 0;
		gridBagConstraints4.anchor = GridBagConstraints.WEST;
		gridBagConstraints4.gridy = 4;
		gridBagConstraints4.insets = new Insets(2, 10, 2, 10);
		JLabel_localisation = new JLabel();
		JLabel_localisation.setText(Messages.getString("Localisation"));
		GridBagConstraints gridBagConstraints31 = new GridBagConstraints();
		gridBagConstraints31.gridx = 0;
		gridBagConstraints31.anchor = GridBagConstraints.WEST;
		gridBagConstraints31.gridy = 3;
		gridBagConstraints31.insets = new Insets(2, 10, 2, 10);
		JLabel_nom = new JLabel();
		JLabel_nom.setText(Messages.getString("Libelle"));
		GridBagConstraints gridBagConstraints21 = new GridBagConstraints();
		gridBagConstraints21.gridx = 0;
		gridBagConstraints21.anchor = GridBagConstraints.WEST;
		gridBagConstraints21.gridy = 2;
		gridBagConstraints21.insets = new Insets(2, 10, 2, 10);
		JLabel_code = new JLabel();
		JLabel_code.setText(Messages.getString("Code"));
		GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
		gridBagConstraints7.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints7.gridy = 1;
		gridBagConstraints7.anchor = GridBagConstraints.WEST;
		gridBagConstraints7.insets = new Insets(2, 2, 2, 2);
		gridBagConstraints7.gridx = 2;
		GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
		gridBagConstraints6.gridx = 2;
		gridBagConstraints6.anchor = GridBagConstraints.WEST;
		gridBagConstraints6.insets = new Insets(2, 2, 2, 0);
		gridBagConstraints6.gridy = 0;
		GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
		gridBagConstraints5.gridx = 2;
		gridBagConstraints5.anchor = GridBagConstraints.WEST;
		gridBagConstraints5.insets = new Insets(2, 2, 2, 2);
		gridBagConstraints5.gridy = 3;
		GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
		gridBagConstraints2.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints2.gridy = 4;
		gridBagConstraints2.gridwidth = 1;
		gridBagConstraints2.anchor = GridBagConstraints.WEST;
		gridBagConstraints2.insets = new Insets(2, 2, 2, 2);
		gridBagConstraints2.gridx = 2;
		GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
		gridBagConstraints12.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints12.gridy = 8;
		gridBagConstraints12.gridx = 2;
		GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
		gridBagConstraints3.fill = GridBagConstraints.VERTICAL;
		gridBagConstraints3.gridy = 0;
		gridBagConstraints3.anchor = GridBagConstraints.WEST;
		gridBagConstraints3.insets = new Insets(2, 0, 2, 0);
		gridBagConstraints3.gridx = 1;
		center = new javax.swing.JPanel();
		JLabelStation = new javax.swing.JLabel();
		liste_stations = new javax.swing.JComboBox();
		top = new javax.swing.JPanel();
		titre = new javax.swing.JLabel();
		resultat = new javax.swing.JLabel();
		bottom = new javax.swing.JPanel();
		ok = new javax.swing.JButton();
		effacer = new javax.swing.JButton();

		setBackground(new java.awt.Color(255, 255, 255));
		setPreferredSize(new java.awt.Dimension(600, 400));
		setLayout(new java.awt.BorderLayout());

		center.setBackground(new Color(230, 230, 230));
		center.setPreferredSize(new java.awt.Dimension(600, 400));
		center.setLayout(new java.awt.GridBagLayout());

		JLabelStation.setText(Messages.getString("Station"));
		JLabelStation.setHorizontalAlignment(SwingConstants.LEADING);
		GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
		gridBagConstraints11.gridx = 0;
		gridBagConstraints11.gridy = 0;
		gridBagConstraints11.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints11.insets = new Insets(2, 10, 2, 10);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 10;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 11;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(2, 0, 2, 0);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(2, 0, 2, 0);
		center.add(JLabelStation, gridBagConstraints11);
		center.add(liste_stations, gridBagConstraints);
		liste_stations.setModel(new DefaultComboBoxModel(lesStations.toArray()));

		center.add(JLabel_nom, gridBagConstraints31);
		center.add(JLabel_localisation, gridBagConstraints4);
		center.add(JLabel_coordonneeX, gridBagConstraints51);
		center.add(JLabel_coordonneeY, gridBagConstraints61);
		center.add(JLabel_altitude, gridBagConstraints71);
		center.add(JLabel_carteLocalisation, gridBagConstraints8);
		center.add(JLabel_nature, gridBagConstraints111);
		center.add(JLabel_commentaires, gridBagConstraints121);
		center.add(getJTextField_ouvrage_code(), gridBagConstraints21);
		center.add(getJTextField_ouvrage_libelle(), gridBagConstraints15);
		center.add(getJTextField_ouvrage_localisation(), gridBagConstraints17);
		center.add(getJTextField_ouvrage_coordonneeX(), gridBagConstraints18);
		center.add(getJTextField_ouvrage_coordonneeY(), gridBagConstraints19);
		center.add(getJTextField_ouvrage_altitude(), gridBagConstraints20);
		center.add(getJTextField_ouvrage_commentaires(), gridBagConstraints26);
		liste_stations.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				liste_stationsActionPerformed(evt);
			}
		});
		liste_stations.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				liste_stationsMouseClicked(evt);
			}

			private void liste_stationsMouseClicked(MouseEvent evt) {
				chargeSousListeOuvrage();
			}
		});
		center.add(liste_stations, gridBagConstraints3);
		center.add(JTextField_ouvrage_code, gridBagConstraints1);
		center.add(getJComboBox_Nature(), gridBagConstraints13);
		center.add(jLabel_Ouvrage, gridBagConstraints24);
		center.add(getListe_ouvrages(), gridBagConstraints32);
		liste_stations.setModel(new DefaultComboBoxModel(this.lesStations.toArray()));

		add(center, java.awt.BorderLayout.CENTER);

		top.setLayout(new java.awt.BorderLayout());

		titre.setBackground(new java.awt.Color(0, 51, 153));
		titre.setFont(new java.awt.Font("Dialog", 1, 14));
		titre.setForeground(new java.awt.Color(255, 255, 255));
		titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		titre.setText(Messages.getString("ModifierOuvrage.Titre"));
		titre.setOpaque(true);
		top.add(titre, java.awt.BorderLayout.CENTER);
		titre.getAccessibleContext().setAccessibleName("Modifier une station de contr\u00f4le");

		resultat.setBackground(new java.awt.Color(255, 255, 255));
		resultat.setForeground(new java.awt.Color(255, 0, 0));
		resultat.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		resultat.setOpaque(true);
		top.add(resultat, java.awt.BorderLayout.SOUTH);

		add(top, java.awt.BorderLayout.NORTH);

		bottom.setBackground(new java.awt.Color(191, 191, 224));

		jComboBox_Nature.setModel(new DefaultComboBoxModel(this.listeRefNatureOuvrage.toArray()));
		ok.setText(Messages.getString("Ok"));
		ok.setActionCommand("test");
		ok.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				okActionPerformed(evt);
			}
		});
		bottom.add(ok);

		effacer.setText(Messages.getString("Annuler"));
		effacer.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				effacerActionPerformed(evt);
			}
		});
		bottom.add(effacer);
		liste_ouvrages.setEnabled(false);
		liste_ouvrages.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				liste_ouvragesActionPerformed(evt);
			}
		});
		liste_ouvrages.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				liste_ouvragesMouseClicked(evt);
			}

			private void liste_ouvragesMouseClicked(MouseEvent evt) {
				chargeInfoOuvrage();
			}
		});

		add(bottom, java.awt.BorderLayout.SOUTH);

		jScrollPane = new javax.swing.JScrollPane();
		jScrollPane.setPreferredSize(new java.awt.Dimension(350, 150));
		jScrollPane.setViewportView(JTextField_ouvrage_commentaires);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 12;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.insets = new Insets(3, 0, 3, 0);
		center.add(jScrollPane, gridBagConstraints);

	}// </editor-fold>//GEN-END:initComponents

	private void liste_stationsActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_liste_stationsActionPerformed
		chargeSousListeOuvrage();
	}// GEN-LAST:event_liste_stationsActionPerformed

	private void liste_ouvragesActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_liste_stationsActionPerformed
		chargeInfoOuvrage();
	}// GEN-LAST:event_liste_stationsActionPerformed

	/**
	 * Constructeur de ModifierOuvrage
	 */
	public ModifierOuvrage() {
		this.chargeListes();
		this.chargeListeRefNatureOuvrage();
		this.initComponents();
	}

	/**
	 * Reinitialise les composants du jPanel courant
	 */
	private void reInitComponents() {
		this.removeAll();
		this.chargeListes();
		this.initComponents();
		this.validate();
		this.repaint();
		this.videChampsDetexte();
		this.liste_ouvrages.setModel(new DefaultComboBoxModel());
	}

	/**
	 * Chargement des diff�rentes listes du panel sans interaction
	 */
	private void chargeListes() {
		this.lesStations = new ListeStations();
		try {
			this.lesStations.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1003);
		}
	}

	/**
	 * Charge la sous liste des ouvrages
	 */
	private void chargeSousListeOuvrage() {
		Station station; // la station selectionnee

		// Recupere la station selectionne dans le combo
		station = (Station) this.liste_stations.getSelectedItem();

		// Vide la liste des station pour garder uniquement la station
		// selectionnee
		this.lesStations = new ListeStations();
		this.lesStations.put(station.getCode(), station);

		// charge la sous liste des ouvrages
		this.lesOuvrages = new SousListeOuvrages(station);

		try {
			this.lesOuvrages.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1004);
		}

		// Affectation de la sous liste des ouvrages a la station
		station.setLesElementsConstitutifs(this.lesOuvrages);

		// actualiste les combo
		this.liste_ouvrages.setModel(new DefaultComboBoxModel(this.lesOuvrages.toArray()));
		this.liste_ouvrages.setEnabled(true);
	}

	/**
	 * Charge les informations relatives � l'ouvrage
	 */
	private void chargeInfoOuvrage() {
		if (liste_ouvrages.isEnabled()) {
			Ouvrage o = (Ouvrage) this.liste_ouvrages.getSelectedItem();

			// cr�ation de la sousListe pour charger les informations sur le DC
			ListeOuvrage lo = new ListeOuvrage();
			lo.add(o);

			// chargement des infos
			try {
				lo.chargeObjets();
			} catch (Exception e) {
				this.resultat.setText(Erreur.B1015);
			}

			// mise � jour des champs correspondant aux infos charg�es
			// pr�c�demment
			if (o.getCode() != null)
				JTextField_ouvrage_code.setText(o.getCode());
			if (o.getLibelle() != null)
				JTextField_ouvrage_libelle.setText(o.getLibelle());
			if (o.getAltitude() != null)
				JTextField_ouvrage_altitude.setText(o.getAltitude().toString());
			if (o.getCommentaires() != null)
				JTextField_ouvrage_commentaires.setText(o.getCommentaires());
			if (o.getCoordonneeX() != null)
				JTextField_ouvrage_coordonneeX.setText(o.getCoordonneeX().toString());
			if (o.getCoordonneeY() != null)
				JTextField_ouvrage_coordonneeY.setText(o.getCoordonneeY().toString());
			if (o.getLocalisation() != null)
				JTextField_ouvrage_localisation.setText(o.getLocalisation());
			RefNatureOuvrage nat = (RefNatureOuvrage) listeRefNatureOuvrage.get(o.getNature().getCode());
			jComboBox_Nature.setSelectedItem(nat);
		}
	}

	/**
	 * Mise a jour de l'ouvrage
	 */
	private void valide() throws Exception {

		// le DC selectionn�
		Ouvrage o = (Ouvrage) this.liste_ouvrages.getSelectedItem();

		// Resultat affiche a l'utilisateur
		String result = null;

		// Attributs de l'objet a creer
		// Integer identifiant = null ; autoincremente
		// mise � jour des champs correspondant aux infos charg�es pr�c�demment
		String code = null;
		String nom = null;
		String localisation = null;
		Integer coordonneeX = new Integer(0);
		Integer coordonneeY = new Integer(0);
		Short altitude = null;
		RefNatureOuvrage natureOuvrage = null;
		String commentaires = null;

		try {
			code = (String) this.JTextField_ouvrage_code.getText();
		} catch (Exception e) {
			result = Erreur.S9002 + this.JLabel_code.getText();
		}
		try {
			nom = (String) this.JTextField_ouvrage_libelle.getText();
		} catch (Exception e) {
			result = Erreur.S9003 + this.JLabel_nom.getText();
		}

		try {
			localisation = (String) this.JTextField_ouvrage_localisation.getText();
		} catch (Exception e) {
			result = Erreur.S9004 + this.JLabel_localisation.getText();
		}

		try {
			coordonneeX = this.JTextField_ouvrage_coordonneeX.getInteger();
		} catch (Exception e) {
			result = Erreur.S9005 + this.JLabel_coordonneeX.getText();
		}

		try {
			coordonneeY = this.JTextField_ouvrage_coordonneeY.getInteger();
		} catch (Exception e) {
			result = Erreur.S9006 + this.JLabel_coordonneeY.getText();
		}

		try {
			altitude = this.JTextField_ouvrage_altitude.getShort();
		} catch (Exception e) {
			result = Erreur.S9007 + this.JLabel_altitude.getText();
		}

		try {
			natureOuvrage = (RefNatureOuvrage) this.jComboBox_Nature.getSelectedItem();
		} catch (Exception e) {
			result = Erreur.S9011 + this.JLabel_nature.getText();
		}

		try {
			commentaires = this.JTextField_ouvrage_commentaires.getString();
		} catch (Exception e) {
			result = Erreur.S9010 + this.JTextField_ouvrage_commentaires.getText();
		}
		// Si aucune erreur jusque la, les champs sont soit null soit du bon
		// type, mais les contraintes de domaine ne sont pas forcement verifiees
		if (result == null) {
			try {
				o.setCode(code);
				o.setLibelle(nom);
				o.setLocalisation(localisation);
				o.setCoordonneeX(coordonneeX);
				o.setCoordonneeY(coordonneeY);
				o.setAltitude(altitude);
				o.setNature(natureOuvrage);
				o.setCommentaires(commentaires);

				// v�rification des �l�ments de la station
				o.verifAttributs();

				// maj dans la BDD
				o.majObjet();

				// Message = succ�s
				result = Message.M1003; // reussite

				// Reinitialisation de la fenetre
				this.reInitComponents();
			} catch (Exception e) {
				result = e.getMessage(); // erreur
			} finally {
				// Affichage du resultat
				this.resultat.setText(result);
			}
		} else {
			// Affichage du resultat sans reinitialisation : les champs ne sont
			// pas remplis
			this.resultat.setText(result);
		}
	}

	/**
	 * This method initializes JTextField_ouvrage_code
	 * 
	 * @return commun.JTextFieldDataType
	 */
	private JTextFieldDataType getJTextField_ouvrage_code() {
		if (JTextField_ouvrage_code == null) {
			JTextField_ouvrage_code = new JTextFieldDataType();
			JTextField_ouvrage_code.setColumns(10);
		}
		return JTextField_ouvrage_code;
	}

	/**
	 * This method initializes JTextField_ouvrage_libelle
	 * 
	 * @return commun.JTextFieldDataType
	 */
	private JTextFieldDataType getJTextField_ouvrage_libelle() {
		if (JTextField_ouvrage_libelle == null) {
			JTextField_ouvrage_libelle = new JTextFieldDataType();
			JTextField_ouvrage_libelle.setColumns(20);
		}
		return JTextField_ouvrage_libelle;
	}

	/**
	 * This method initializes JTextField_ouvrage_localisation
	 * 
	 * @return commun.JTextFieldDataType
	 */
	private JTextFieldDataType getJTextField_ouvrage_localisation() {
		if (JTextField_ouvrage_localisation == null) {
			JTextField_ouvrage_localisation = new JTextFieldDataType();
			JTextField_ouvrage_localisation.setColumns(20);
		}
		return JTextField_ouvrage_localisation;
	}

	/**
	 * This method initializes JTextField_ouvrage_coordonneeX
	 * 
	 * @return commun.JTextFieldDataType
	 */
	private JTextFieldDataType getJTextField_ouvrage_coordonneeX() {
		if (JTextField_ouvrage_coordonneeX == null) {
			JTextField_ouvrage_coordonneeX = new JTextFieldDataType();
			JTextField_ouvrage_coordonneeX.setColumns(7);
			JTextField_ouvrage_coordonneeX.setToolTipText(Messages.getString("ToolTipX"));
		}
		return JTextField_ouvrage_coordonneeX;
	}

	/**
	 * This method initializes JTextField_ouvrage_coordonneeY
	 * 
	 * @return commun.JTextFieldDataType
	 */
	private JTextFieldDataType getJTextField_ouvrage_coordonneeY() {
		if (JTextField_ouvrage_coordonneeY == null) {
			JTextField_ouvrage_coordonneeY = new JTextFieldDataType();
			JTextField_ouvrage_coordonneeY.setColumns(7);
			JTextField_ouvrage_coordonneeY.setToolTipText(Messages.getString("ToolTipY"));
		}
		return JTextField_ouvrage_coordonneeY;
	}

	/**
	 * This method initializes JTextField_ouvrage_altitude
	 * 
	 * @return commun.JTextFieldDataType
	 */
	private JTextFieldDataType getJTextField_ouvrage_altitude() {
		if (JTextField_ouvrage_altitude == null) {
			JTextField_ouvrage_altitude = new JTextFieldDataType();
			JTextField_ouvrage_altitude.setColumns(7);
			JTextField_ouvrage_altitude.setToolTipText(Messages.getString("ValPositive"));
		}
		return JTextField_ouvrage_altitude;
	}

	/**
	 * This method initializes JTextField_ouvrage_commentaires
	 * 
	 * @return commun.JTextAreaDataType
	 */
	private JTextAreaDataType getJTextField_ouvrage_commentaires() {
		if (JTextField_ouvrage_commentaires == null) {
			JTextField_ouvrage_commentaires = new JTextAreaDataType();
			JTextField_ouvrage_commentaires.setColumns(20);
			JTextField_ouvrage_commentaires.setRows(4);
			JTextField_ouvrage_commentaires.setLineWrap(true);
		}
		return JTextField_ouvrage_commentaires;
	}

	/**
	 * This method initializes jComboBox_Nature
	 * 
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJComboBox_Nature() {
		if (jComboBox_Nature == null) {
			jComboBox_Nature = new JComboBox();
		}
		return jComboBox_Nature;
	}

	/**
	 * This method initializes liste_ouvrages
	 * 
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getListe_ouvrages() {
		if (liste_ouvrages == null) {
			liste_ouvrages = new JComboBox();
			liste_ouvrages.setEnabled(false);
		}
		return liste_ouvrages;
	}

	/**
	 * Retourne le code de l'ouvrage
	 * 
	 * @param dc
	 *            : le Dispositif de controle
	 * @return le type de DC
	 */
	private String getCode(RefNatureOuvrage o) {
		return o.getCode();
	}

	/**
	 * charge la liste de nature des ouvrages
	 */
	private void chargeListeRefNatureOuvrage() {
		this.listeRefNatureOuvrage = new ListeRefNatureOuvrage();
		try {
			this.listeRefNatureOuvrage.chargeSansFiltre();
		} catch (Exception e) {
			this.resultat.setText(Erreur.B1003);
		}
	}

	private void videChampsDetexte() {
		JTextField_ouvrage_altitude.setText(null);
		JTextField_ouvrage_commentaires.setText(null);
		JTextField_ouvrage_coordonneeX.setText(null);
		JTextField_ouvrage_coordonneeY.setText(null);
		JTextField_ouvrage_libelle.setText(null);
		JTextField_ouvrage_localisation.setText(null);
		jComboBox_Nature.setSelectedIndex(0);
	}
}
