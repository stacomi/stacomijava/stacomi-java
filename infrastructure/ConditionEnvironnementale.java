/** Java class "ConditionEnvironnementale.java" generated from Poseidon for UML.
 *  Poseidon for UML is developed by <A HREF="http://www.gentleware.com">Gentleware</A>.
 *  Generated with <A HREF="http://jakarta.apache.org/velocity/">velocity</A> template engine.
 */

package infrastructure;

import commun.ConnexionBD;
import commun.Erreur;
import commun.IBaseDonnees;
import commun.Verification;
import migration.referenciel.RefValeurParametre;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.DataFormatException;

/**
 * Description d'un condition environnementale
 * @author S�bastien Laigre
 */
public class ConditionEnvironnementale implements IBaseDonnees {

	///////////////////////////////////////
	// attributes

	/** La mesure de la station */
	private StationMesure stationMesure;

	/** La date de d�but */
	private Date dateDebut;

	/** La date de fin */
	private Date dateFin;

	/** La m�thode d'obtention */
	private String methodeObtention;

	/** La valeur du parametre quantitatif */
	private Float valeurParametreQuantitatif;

	/** La valeur du parametre qualitatif */
	private RefValeurParametre valeurParametreQualitatif;

	/**
	 * Construction d'une condition environnementale
	 * @param debut d�but de la condition environnementale
	 * @param fin fin de la condition environnementale
	 * @param methodeObtention m�thode d'obtention
	 * @param stationMesure station de mesure
	 * @param valParametreQual valeur du parametre qualitatif
	 * @param valParametreQuant valeur du parametre quantitatif
	 */
	public ConditionEnvironnementale(Date debut, Date fin, String methodeObtention, 
			StationMesure stationMesure, RefValeurParametre valParametreQual, Float valParametreQuant)
	{
		this.setDateDebut(debut);
		this.setDateFin(fin);
		this.setMethodeObtention(methodeObtention);
		this.setStationMesure(stationMesure);
		this.setValeurParametreQualitatif(valParametreQual);
		this.setValeurParametreQuantitatif(valParametreQuant);
	}
	
	/**
	 * Condition environnementale par d�faut
	 */
	public ConditionEnvironnementale()
	{

	}
	
	///////////////////////////////////////
	// associations

	///////////////////////////////////////
	// operations
	
	/**
	 * Acc�s a la mesure de la station
	 * @return la mesure de la station 
	 */
	public StationMesure getStationMesure() {
		return this.stationMesure;
	} // end getStation        

	/**
	 * Affecte la mesure de la station
	 * @param _stationMesure la station de mesure
	 */
	public void setStationMesure(StationMesure _stationMesure) {
		this.stationMesure = _stationMesure;
	} // end setStation    
	
	/**
	 * Acc�s a la date de d�but
	 * @return la date de d�but
	 */
	public Date getDateDebut() {
		return this.dateDebut;
	} // end getDateDebut        

	/**
	 *	Affecte la date de d�but 
	 * @param _dateDebut : la date de d�but
	 */
	public void setDateDebut(Date _dateDebut) {
		this.dateDebut = _dateDebut;
	} // end setDateDebut        

	/**
	 * Acc�s � la date de fin
	 * @return la date de fin
	 */
	public Date getDateFin() {
		return this.dateFin;
	} // end getDateFin        

	/**
	 * Affecte la date de fin
	 * @param _dateFin : la date de fin
	 */
	public void setDateFin(Date _dateFin) {
		this.dateFin = _dateFin;
	} // end setDateFin        

	/**
	 * Acc�s � la m�thode d'obtention
	 * @return la m�thode d'obtention
	 */
	public String getMethodeObtention() {
		return this.methodeObtention;
	} // end getMethodeObtention        

	/**
	 * Affecte la m�thode d'obtention
	 * @param _methodeObtention : la m�thode d'obtention 
	 */
	public void setMethodeObtention(String _methodeObtention) {
		this.methodeObtention = _methodeObtention;
	} // end setMethodeObtention        

	/**
	 * Acc�s � la valeur du param�tre quantitatif
	 * @return la valeur du param�tre quantitatif
	 */
	public Float getValeurParametreQuantitatif() {
		return this.valeurParametreQuantitatif;
	} // end getValeurParametreQualitatif        

	/**
	 * Affecte la valeur du param�tre quantitatif
	 * @param _valeurParametreQualitatif : la valeur du param�tre quantitatif
	 */
	public void setValeurParametreQualitatif(RefValeurParametre _valeurParametreQualitatif) {
		valeurParametreQualitatif = _valeurParametreQualitatif;
	} // end setValeurParametreQualitatif        

	/**
	 * Acc�s � la valeur du param�tre qualitatif
	 * @return la valeur du param�tre qualitatif
	 */
	public RefValeurParametre getValeurParametreQualitatif() {
		return valeurParametreQualitatif;
	} // end getValeurParametreQuantitatif        

	/**
	 * Affecte la valeur du param�tre qualitatif
	 * @param _valeurParametreQuantitatif : la valeur du param�tre qualitatif
	 */
	public void setValeurParametreQuantitatif( Float _valeurParametreQuantitatif) {
		this.valeurParametreQuantitatif = _valeurParametreQuantitatif;
		
	}

	/**
	 * V�rification d'une ConditionEnvironnementale
	 * V�rifie le format de chacuns des attributs de l'objet courant
	 */
	public boolean verifAttributs() throws DataFormatException {

		// v�rification des dates
		if(!Verification.isDateInf(this.dateDebut,this.dateFin,true,true))
			throw new DataFormatException(Erreur.S15005);

		// v�rification de la m�thode d'obtention
		if(!Verification.isMethodeObtentionLot(this.methodeObtention,true))
			throw new DataFormatException(Erreur.S15007);

		// v�rification des valeurs du param (qualitatif ou quantitatif)
		if(!Verification.isValeurParametre(this.valeurParametreQuantitatif, this.valeurParametreQualitatif, true))
			throw new DataFormatException(Erreur.S15006);

		// v�rification de la mesure de la station
		//this.stationMesure.verifAttributs();

		return true;
	}

	/**
	 * Effacement d'une condition environnementale
	 */
	public void effaceObjet() throws java.sql.SQLException, ClassNotFoundException {
		// suppression de la condition environnementale
		String table = ConditionEnvironnementale.getNomTable();
		String[] nomID = ConditionEnvironnementale.getNomID();
		Object[] valeurID = this.getValeurID();

		ConnexionBD.getInstance().executeDelete(table, nomID, valeurID);
		System.out.println("Condition environnementale : effaceObjet()");
	}

	/**
	 * Insertion d'une condition environnementale
	 */
	public void insertObjet() throws SQLException, ClassNotFoundException 
	{
//		this.stationMesure.insertObjet();

		String table = ConditionEnvironnementale.getNomTable();
		ArrayList<String> nomAttributs = ConditionEnvironnementale.getNomAttributs();
		ArrayList<Object> valeurAttributs = this.getValeurAttributs();
		
		ConnexionBD.getInstance().executeInsert(table,nomAttributs,valeurAttributs);
		System.out.println("Condition environnementale : insertObjet()");
	}

	/**
	 * Mise � jour d'une condition environnementale
	 */
	public void majObjet() throws SQLException, ClassNotFoundException 
	{ }

	/**
	 * Mise � jour d'une condition environnementale
	 * @param old : l'ancienne condition environnementale (celle que l'on met � jour)
	 * @throws SQLException Erreur avec la BDD
	 * @throws ClassNotFoundException Classe non trouv�e
	 */
	public void majObjet(ConditionEnvironnementale old) throws SQLException, ClassNotFoundException 
	{
//		this.stationMesure.majObjet();
		String table = ConditionEnvironnementale.getNomTable();
		ArrayList<String> nomAttributs = ConditionEnvironnementale.getNomAttributs();
		ArrayList<Object> valeurAttributs = this.getValeurAttributs();
		
		String[] nomID = ConditionEnvironnementale.getNomID();
		Object[] valeurID = old.getValeurID();
		
		ConnexionBD.getInstance().executeUpdate(table,nomID,valeurID,nomAttributs,valeurAttributs);
		System.out.println("Condition environnementale : majObjet()");
	}

	/**
	 * Retourne le nom de la table correspondante a la classe
	 * @return le nom de la table
	 */
	public static String getNomTable() {
		return "tj_conditionenvironnementale_env";
	}

	/**
	 * Retourne le nom des attributs de la table correspondant a la classe 
	 * @return un Vector de String avec les attributs dans l'ordre de la table dans la BD
	 */
	public static ArrayList<String> getNomAttributs() {
		// impossible d'utiliser getDeclaredFields() car l'ordre dans le tableau
		// est inconnu
		ArrayList<String> ret = new ArrayList<String>();
		ret.add("env_stm_identifiant");
		ret.add("env_date_debut");
		ret.add("env_date_fin");
		ret.add("env_methode_obtention");
		ret.add("env_val_identifiant");
		ret.add("env_valeur_quantitatif");
		ret.add("env_org_code");
		return ret;
	} // end getNomAttributs

	/**
	 * Retourne la valeur des attributs de la classe
	 * @return un Vector d'Object avec les attributs dans l'ordre de la table dans la BD
	 */
	public ArrayList<Object> getValeurAttributs() {
		ArrayList<Object> ret = new ArrayList<Object>();
		ret.add(this.getStationMesure().getIdentifiant());
		ret.add(this.getDateDebut());
		ret.add(this.getDateFin());
		ret.add(this.getMethodeObtention());
		
		// si getValeurParametreQualitatif() renvoie null => on �crit null "� la main"
		if(this.getValeurParametreQualitatif()!=null)
			ret.add(this.getValeurParametreQualitatif().getCode());
		else
			ret.add(null);
		
		ret.add(this.getValeurParametreQuantitatif());
		return ret;
	} // end getValeurAttributs

	/**
	 * Retourne le nom de l'identifiant caract�re de la table
	 * @return un vecteur de string de longueur 1 contenant le nom de l'identifiant de la table
	 */
	public static String[] getNomID() {
		String[] ret = new String[3];
		ret[0] = "env_stm_identifiant";
		ret[1] = "env_date_debut";
		ret[2] = "env_date_fin";
		return ret;
	} // end getNomID

	/**
	 * Retourne la valeur de l'indenfiant string de la classe
	 * @return un vecteur d'objet de longueur 1 contenant le code
	 */
	public Object[] getValeurID() {
		Object[] ret = new Object[3];
		ret[0] = this.getStationMesure().getIdentifiant();
		ret[1] = this.getDateDebut();
		ret[2] = this.getDateFin();
		return ret;
	} // end getValeurID

	/**
	 * Affichage d'une condition environnementale
	 */
	public String toString()
	{
		String res = "";

		SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		String sep = "  |  ";
		res += simpleDate.format(this.dateDebut) + sep;
		res += simpleDate.format(this.dateFin) + sep;
		
		res += this.stationMesure + sep;
		
//		res += this.stationMesure.getParametre().getLibelle() + sep;
//		res += this.stationMesure.getLibelle() + sep;
		res += this.methodeObtention + sep;

		if(this.valeurParametreQuantitatif==null)
			res += this.valeurParametreQualitatif.getCode() + sep; 
		else 
			res += valeurParametreQuantitatif + sep;

		res += this.stationMesure.getCommentaires();
		
		return res;
	}

	/**
	 * Remise a null de valeurParametreQuantitatif
	 */
	public void resetValQuant() {
		this.valeurParametreQuantitatif = null;
	}
	
	// end setValeurParametreQuantitatif        

} // end ConditionEnvironnementale

