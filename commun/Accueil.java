/*
 * AjouterOperation.java
 * ResultSet
 * Created on 26 mai 2004, 09:42
 */

package commun;

import java.awt.GridBagConstraints;

import javax.swing.ImageIcon;

/**
 *
 * @author sgaudey
 */
public class Accueil extends javax.swing.JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3950763215955993348L;
	private javax.swing.JLabel image;

	/** Creates new form AjouterOperation */
	public Accueil() {

		this.initComponents();

	}

	/**
	 * initComponents method for Accueil
	 */
	private void initComponents() {
		image = new javax.swing.JLabel();
		setLayout(new java.awt.GridBagLayout());

		setBackground(new java.awt.Color(230, 230, 230));
		setPreferredSize(new java.awt.Dimension(600, 400));

		try {
			ImageIcon cadre = new ImageIcon(getClass().getResource("/images/stacomi.png"));
			image.setIcon(cadre);
		} catch (Exception e) {
			System.err.println("l'image d'initialisation stacomi.png n'est pas pr�sente");
			System.exit(-1);// donne le code d'erreur en sortie pas tr�s
							// important
		}
		GridBagConstraints gbc = new java.awt.GridBagConstraints();
		add(image, gbc);

	}// GEN-END:initComponents

}
