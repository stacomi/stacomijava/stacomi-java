/*
 **********************************************************************
 *
 * Nom fichier :        Message.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package commun;

/**
 * Classe statique regroupant les messages sous forme textuelle, destinees a
 * l'utilisateur * qui ne concerne que les relations � la base de donn�es
 * (messages de succ�s)
 */
public final class Message {

	// Message de reussite d'ajout d'un enregistrement
	public static String A = " Enregistrement ajout� : ";
	public static String A1000 = A + "station de contr�le (A1000)";
	public static String A1001 = A + "op�ration de contr�le (A1001)";
	public static String A1002 = A + "lot pour une op�ration (A1002)";
	public static String A1003 = A + "caract�ristique de lot (A1003)";
	public static String A1004 = A + "Ouvrage (A1004)";
	public static String A1005 = A + "DF (A1005)";
	public static String A1006 = A + "DC (A1006)";
	public static String A1007 = A + "Periode de fonctionnement (A1007)";
	public static String A1008 = A + "Tailles vid�o (A1008)";
	public static String A1009 = A + "Taxon vid�o (A1009)";
	public static String A1010 = A + "Pathologie constat�e (A1010)";
	public static String A1011 = A + "Condition environnementale (A1011)";
	public static String A1012 = A + "Marque (A1012)";
	public static String A1013 = A + "Op�ration de marquage (A1013)";
	public static String A1014 = A + "Taux d'�chappement (A1014)";
	public static String A1015 = A + "Station de mesure (A1015)";
	public static String A1016 = A + "Pr�l�vement (A1016)";
	public static String A1017 = A + "Masque (A1017)";
	public static String A1018 = A + "Masque Op�ration (A1018)";
	public static String A1019 = A + "Masque Lot (A1019)";
	// Message de reussite de mise � jour d'un enregistrement
	public static String M = "Enregistrement modifi� : ";
	public static String M1000 = M + "lot d'une op�ration (M1000)";
	public static String M1001 = M + "op�ration de contr�le (M1001)";
	public static String M1002 = M + "caract�ristique de lot (M1002)";
	public static String M1003 = M + "ouvrage (M1003)";
	public static String M1004 = M + "DF (M1004)";
	public static String M1005 = M + "DC (M1005)";
	public static String M1006 = M + "Station de controle (M1006)";
	public static String M1007 = M + "Periode de fonctionnement (M1007)";
	public static String M1008 = M + "Tailles vid�o (M1008)";
	public static String M1009 = M + "Taxon vid�o (M1009)";
	public static String M1010 = M + "Pathologie constat�e (M1010)";
	public static String M1011 = M + "Condition environnementale (M1011)";
	public static String M1012 = M + "Marque (M1012)";
	public static String M1013 = M + "Op�ration de marquage (M1013)";
	public static String M1014 = M + "Taux d'�chappement (M1014)";
	public static String M1015 = M + "Mesure � la station (M1015)";
	public static String M1016 = M + "Pr�l�vement (M1016)";
	public static String M1017 = M + "Masque (M1017)";
	public static String M1018 = M + "Masque Op�ration (M1018)";
	public static String M1019 = M + "Masque Lot (M1019)";
	// Message de reussite de suppression d'un enregistrement
	public static String S = "Enregistrement supprim� : ";
	public static String S1000 = S + "lot d'une op�ration (S1000)";
	public static String S1001 = S + "op�ration de contr�le (S1001)";
	public static String S1002 = S + "caract�ristique du lot (S1002)";
	public static String S1003 = S + "Periode de fonctionnement du dispositif (S1003)";
	public static String S1004 = S + "Tailles vid�o (S1004)";
	public static String S1005 = S + "Taxon vid�o (S1005)";
	public static String S1006 = S + "Pathologie constat�e (S1006)";
	public static String S1007 = S + "enregistrements video (car echec � l'insertion) (S1007)";
	public static String S1008 = S + "enregistrements video (S1008)";
	public static String S1009 = S + "Condition environnementale (S1009)";
	public static String S1010 = S + "Marque (S1010)";
	public static String S1011 = S + "Op�ration de marquage (S1011)";
	public static String S1012 = S + "taux d'�chappement (S1012)";
	public static String S1013 = S + "Mesure � la station (S1013)";
	public static String S1014 = S + "Pr�l�vement (S1014)";
	public static String S1015 = S + "Masque (S1015)";
	public static String S1018 = S + "Masque Lot (S1018)";

	// Message de reussite d'import
	public static String I = "Import effectu� : ";
	public static String I1000 = I + "d'apres fichier source (I1000)";
	public static String I1001 = I + "import vid�o (I1001)";
	public static String I1002 = I + "import des conditions environnementales effectu� (I1002)";

	// Message de reussite d'export
	public static String E = "Export effectu� : ";

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Retourne le contenu d'un vecteur de chaines
	 * 
	 * @param _vecteur
	 *            le vecteur de String
	 * @return le contenu du vecteur, avec chaque indice du vecteur separe par
	 *         un retour chariot
	 */
	/*
	 * public static String getString(Vector _vecteur) { String ret = "" ;
	 * 
	 * for (Enumeration e = _vecteur.elements(); e.hasMoreElements(); ) { ret =
	 * ret + (String)e.nextElement() + "\n" ; }
	 * 
	 * return ret ; }
	 */
}
