/*
 **********************************************************************
 *
 * Nom fichier :        LoginWindow.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             S�bastien Laigre
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   02 juin 2009
 * Compatibilite :      Java6/windows XP/ postgreSQL 8.3
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package commun;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import systeme.RefOrganisme;
import systeme.referenciel.ListeRefUtilisateur;

/**
 * Fenetre de connexion
 * 
 * @author S�bastien Laigre
 */
public class LoginWindow extends JFrame {
	/** */
	private static final long serialVersionUID = 1L;

	// Variables declaration
	private JLabel jLabel1;
	private JLabel jLabel2;
	private static JComboBox jComboboxOrganisme;
	private static JPasswordField jPasswordField1;
	private JButton jButton1;
	private JPanel contentPane;

	// End of variables declaration

	/**
	 * Initialise une fenetre de connexion
	 * 
	 * @param lesUtilisateurs
	 */
	public LoginWindow(Liste lesUtilisateurs) {
		super();
		create(lesUtilisateurs);
		this.setVisible(true);
	}

	private void create(Liste lesUtilisateurs) {
		jLabel1 = new JLabel();
		jLabel2 = new JLabel();
		jComboboxOrganisme = new JComboBox();
		jPasswordField1 = new JPasswordField();
		jButton1 = new JButton();
		contentPane = (JPanel) this.getContentPane();

		// jLabel1
		jLabel1.setHorizontalAlignment(SwingConstants.LEFT);
		jLabel1.setForeground(new Color(0, 0, 255));
		jLabel1.setText(Messages.getString("LoginWindow.Utilisateur"));

		// jLabel2
		jLabel2.setHorizontalAlignment(SwingConstants.LEFT);
		jLabel2.setForeground(new Color(0, 0, 255));
		jLabel2.setText(Messages.getString("LoginWindow.Mdp"));

		// jTextField1
		Object[] tab = lesUtilisateurs.toArray();
		jComboboxOrganisme.setModel(new DefaultComboBoxModel(tab));

		// jPasswordField1
		jPasswordField1.setForeground(new Color(0, 0, 255));
		jPasswordField1.setToolTipText(Messages.getString("LoginWindow.EntrezMdp"));

		jPasswordField1.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					jButton1_actionPerformed(null);
			}
		});

		// jButton1
		jButton1.setBackground(new Color(204, 204, 204));
		jButton1.setForeground(new Color(0, 0, 255));
		jButton1.setText(Messages.getString("LoginWindow.Login"));
		jButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton1_actionPerformed(e);
			}
		});

		// contentPane
		contentPane.setLayout(null);
		contentPane.setBorder(BorderFactory.createEtchedBorder());
		contentPane.setBackground(new Color(204, 204, 204));
		addComponent(contentPane, jLabel1, 5, 10, 106, 18);
		addComponent(contentPane, jLabel2, 5, 47, 97, 18);
		addComponent(contentPane, jComboboxOrganisme, 110, 10, 183, 22);
		addComponent(contentPane, jPasswordField1, 110, 45, 183, 22);
		addComponent(contentPane, jButton1, 150, 75, 83, 28);

		// login
		this.setTitle(Messages.getString("LoginWindow.Connexion"));
		this.setLocation(new Point(76, 182));
		this.setSize(new Dimension(335, 141));
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setResizable(false);
	}

	/** Add Component Without a Layout Manager (Absolute Positioning) */
	private void addComponent(Container container, Component c, int x, int y, int width, int height) {
		c.setBounds(x, y, width, height);
		container.add(c);
	}

	// /**
	// * Eventuellement la possibilit� d'enregistrer le mot de passe...
	// * @param e
	// */
	// @SuppressWarnings("unused")
	// private void jComboBoxActionPerformed(ActionEvent e) {
	// // chargement du mot de passe associ� � l'utilisateur selectionn�...
	// }

	// /**
	// * Eventuellement la possibilit� d'enregistrer le mot de passe...
	// * @param e
	// */
	// private void jPasswordField1_actionPerformed(ActionEvent e) {
	//
	// }

	@SuppressWarnings("deprecation")
	private void jButton1_actionPerformed(ActionEvent e) {

		try {
			String username = (String) jComboboxOrganisme.getSelectedItem().toString();
			String password = new String(jPasswordField1.getPassword());

			// If password and username is empty > Do this >>>
			if (username == null || username.equals("") || password.equals("")) {
				jButton1.setEnabled(false);
				JLabel errorFields = new JLabel(Messages.getString("LoginWindow.SelectUserPassword"));
				JOptionPane.showMessageDialog(null, errorFields);
				jPasswordField1.setText("");
				jButton1.setEnabled(true);
				this.setVisible(true);
			} else { // l'utilisateur et le mot de passe ont �t� saisi
				jButton1.setEnabled(false); // Set button enable to false to
											// prevent 2 login attempts

				// conversion de l'utilisateur en minuscule

				// tentative de connexion. Soul�ve une exception si le mdp est
				// erron�
				ConnexionBD.getInstance().setUser(username.toLowerCase(), password);
				// les sequences sont mises a jour pour l'utilisateur connect�
				// Remarque : elle sont mises � jour avec postgres/postgres

				new SequenceUpdater(username.toLowerCase());
				// lancement de l'application
				new Lanceur();

				// fermeture de la fenetre de connexion
				this.dispose();
			}
		} catch (Exception exc) {
			jButton1.setEnabled(false);
			JLabel errorFields = new JLabel(Messages.getString("LoginWindow.PasswordErrone"));
			JOptionPane.showMessageDialog(null, errorFields);
			jPasswordField1.setText("");
			jButton1.setEnabled(true);
			this.setVisible(true);
		}
	}

	/**
	 * Ouverture de la fenetre de connexion pour la selection du login +mot de
	 * passe
	 */
	private static void buildLoginDialog() {
		ListeRefUtilisateur users = new ListeRefUtilisateur();

		try {
			users.chargeSansFiltreDetails();
		} catch (Exception e) {
			JLabel errorFields = new JLabel(e.getMessage());
			JOptionPane.showMessageDialog(null, errorFields);
		}
		LoginWindow l = new LoginWindow(users);
		setdefaut(users);
		l.setLocationRelativeTo(null);
	}

	/**
	 * remplit les valeurs par d�faut � partir des champs du xml
	 */
	private static void setdefaut(ListeRefUtilisateur users) {
		XMLParser parser = new XMLParser();
		String user = parser.getUser().toUpperCase();

		String password = parser.getPassword();
		Integer index = null;

		for (int i = 0; i < users.size(); i++) {
			RefOrganisme reforg = (RefOrganisme) users.get(i);
			if (reforg.getCode().equals(user)) {
				index = i;
				break;
			}
		}

		if (index != null) {
			jComboboxOrganisme.setSelectedIndex(index);
			jPasswordField1.setText(password);
		}

	}

	/**
	 * Lanceur de l'application
	 * 
	 * @param args
	 *            les arguments au lancements
	 */
	public static void main(String[] args) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			// UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception ex) {
			System.out.println("Failed loading L&F: ");
			System.out.println(ex);
		}

		buildLoginDialog();
		// new Lanceur();
	};

}