/*
 **********************************************************************
 *
 * Nom fichier :        IhmAppli.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY, C�dric BRIAND, S�bastien LAIGRE
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package commun;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.UIManager;
import javax.swing.text.MaskFormatter;

import analyse.ihm.AMSTauxEchappement;
import analyse.ihm.BilanCoeffConversion;
import analyse.ihm.BilanConditionsEnv;
import analyse.ihm.BilanFonctionnementDisp;
import analyse.ihm.BilanLots;
import analyse.ihm.BilanMigration;
import analyse.ihm.BilanOpeManquantes;
import analyse.ihm.BilanOpePbFonctionnement;
import analyse.ihm.BilanTauxEchappement;
import infrastructure.ihm.AMSConditionEnv;
import infrastructure.ihm.AMSPeriodeFonctionnement;
import infrastructure.ihm.AMSStationMesure;
import infrastructure.ihm.AjouterDC;
import infrastructure.ihm.AjouterDF;
import infrastructure.ihm.AjouterOuvrage;
import infrastructure.ihm.AjouterStation;
import infrastructure.ihm.ArreterDC;
import infrastructure.ihm.ArreterDF;
import infrastructure.ihm.ArreterStation;
import infrastructure.ihm.ImportConditionEnv;
import infrastructure.ihm.ModifierDC;
import infrastructure.ihm.ModifierDF;
import infrastructure.ihm.ModifierOuvrage;
import infrastructure.ihm.ModifierStation;
import migration.ihm.AMSMarque;
import migration.ihm.AMSOperationMarquage;
import migration.ihm.AMSTailleVideo;
import migration.ihm.AMSTaxonVideo;
import migration.ihm.AjouterLot;
import migration.ihm.AjouterOperation;
import migration.ihm.ImporterVideo;
import migration.ihm.IncrementerOperation;
import migration.ihm.ModifierOperation;
import migration.ihm.SelectionnerOperation;
import systeme.MasqueLot;
import systeme.MasqueOpe;
import systeme.ihm.AMSMasque;
import systeme.ihm.SelectionnerMasque;
import systeme.referenciel.ListeMasqueLot;
import systeme.referenciel.ListeMasqueOpe;

/**
 * @author Samuel Gaudey Interface graphique de l'application
 */
public class IhmAppli extends javax.swing.JFrame {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	/** */
	private static final long serialVersionUID = -1036235755187706202L;
	private final static Logger logger = Logger.getLogger(IhmAppli.class.getName());
	private javax.swing.JMenuItem a_lot;
	private javax.swing.JMenuItem a_operation;
	private javax.swing.JMenuItem a_requete;
	private javax.swing.JMenuItem a_table;
	private javax.swing.JMenuItem aboutMenuItem;
	private javax.swing.JMenu mnAnalyse;
	private javax.swing.JMenuItem b_coeffConversion;
	private javax.swing.JMenuItem b_conditionsEnv;
	private javax.swing.JMenuItem b_lots;
	private javax.swing.JMenuItem b_migration;
	private javax.swing.JMenuItem b_operationsManquantes;
	private javax.swing.JMenuItem b_operationsPbFonc;
	private javax.swing.JMenuItem b_periodesFonctDisp;
	private javax.swing.JMenuItem b_tauxEchappement;
	private javax.swing.JMenuItem contentsMenuItem;
	private javax.swing.JPanel contenu;
	private javax.swing.JMenu mnHelp;
	private javax.swing.JMenuItem i_fichier;
	private javax.swing.JMenuItem i_operation;
	private javax.swing.JMenu mnInfrastructure;
	private javax.swing.JSeparator jSeparator1;
	private javax.swing.JMenuItem m_lot;
	private javax.swing.JMenuItem ams_taxonvideo;
	private javax.swing.JMenuItem ams_taillevideo;
	private javax.swing.JMenuItem m_operation;
	private javax.swing.JMenuBar menuBar;
	private javax.swing.JMenu mnMigration;
	private javax.swing.JMenu mnReferentiel;
	private javax.swing.JMenu opeCourante;
	private javax.swing.JMenu mnOutils;
	private javax.swing.JMenuItem s_operation;
	private JMenuItem a_ouvrage = null;
	private JMenuItem a_DF = null;
	private JMenuItem a_DC = null;
	private JMenu Station = null;
	private JMenu Ouvrage = null;
	private JMenu DF = null;
	private JMenu DC = null;
	private JMenuItem m_station = null;
	private JMenuItem m_ouvrage = null;
	private JMenuItem a_station = null;
	private JMenuItem m_DF = null;
	private JMenuItem a_periode_DF = null;
	private JMenuItem i_periode_DF = null;
	private JMenuItem m_DC = null;
	private JMenuItem a_periode_DC = null;
	private JMenuItem i_periode_DC = null;
	private JMenuItem s_DF = null;
	private JMenuItem s_DC = null;
	private JMenuItem s_station = null;
	private JMenuItem i_operationlots_video = null;
	private JMenuItem ams_CondEnv = null;
	private JMenuItem i_CondEnv = null;
	private JMenuItem ams_opeMarquage = null;
	private JMenuItem ams_tauxEchappement = null;
	private JMenuItem ams_StationMesure = null;
	private JMenuItem ams_Marque = null;
	private JMenu mnMasque;
	private JMenuItem mnMasqueGestion;
	private JMenuItem mnSelectionMasqueOpe;
	private JMenu mnSelectionMasqueLot;
	// private ArrayList<JRadioButtonMenuItem> listofRadioMenuItem = new
	// ArrayList<JRadioButtonMenuItem>();
	private ButtonGroup groupradiomasqueope;
	private MasqueOpe selectedmasqueope;
	private MasqueLot selectedmasquelot;
	/** Le lanceur de l'application */
	Lanceur lanceur; // le lanceur qui a cree l'interface IhmAppli
	private ListeMasqueOpe listemasqueope;
	private ListeMasqueLot listemasquelot;
	private ButtonGroup groupradiomasquelot;

	/**
	 * constructors
	 * 
	 * @param _lanceur
	 *            le lanceur
	 */
	public IhmAppli(Lanceur _lanceur) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (Exception ex) {
			System.out.println("Failed loading L&F: ");
			System.out.println(ex);
		}
		this.lanceur = _lanceur;
		this.initComponents();
		this.chargeMasqueOpe();// affichage de la liste des masques ope en cours
		this.chargeMasqueLot();// affichage de la liste des masques lot en cours
		this.changeContenu(new Accueil());

	}
	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	private void initComponents() {// GEN-BEGIN:initComponents

		contenu = new javax.swing.JPanel();
		menuBar = new javax.swing.JMenuBar();
		mnInfrastructure = new javax.swing.JMenu();
		mnAnalyse = new javax.swing.JMenu();
		mnMigration = new javax.swing.JMenu();
		mnReferentiel = new javax.swing.JMenu();
		mnOutils = new javax.swing.JMenu();
		mnHelp = new javax.swing.JMenu();
		mnMasque = new javax.swing.JMenu();
		b_lots = new javax.swing.JMenuItem();
		b_periodesFonctDisp = new javax.swing.JMenuItem();
		b_tauxEchappement = new javax.swing.JMenuItem();
		b_coeffConversion = new javax.swing.JMenuItem();
		b_conditionsEnv = new javax.swing.JMenuItem();
		b_operationsPbFonc = new javax.swing.JMenuItem();
		b_migration = new javax.swing.JMenuItem();
		b_operationsManquantes = new javax.swing.JMenuItem();
		a_operation = new javax.swing.JMenuItem();
		m_operation = new javax.swing.JMenuItem();
		m_lot = new javax.swing.JMenuItem();
		ams_taxonvideo = new javax.swing.JMenuItem();
		ams_taillevideo = new javax.swing.JMenuItem();
		jSeparator1 = new javax.swing.JSeparator();
		opeCourante = new javax.swing.JMenu();
		i_operation = new javax.swing.JMenuItem();
		s_operation = new javax.swing.JMenuItem();
		a_lot = new javax.swing.JMenuItem();
		a_table = new javax.swing.JMenuItem();
		a_requete = new javax.swing.JMenuItem();
		i_fichier = new javax.swing.JMenuItem();
		contentsMenuItem = new javax.swing.JMenuItem();
		aboutMenuItem = new javax.swing.JMenuItem();

		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		setTitle("Contr\u00f4le migrateurs 2016 - v. 0.5");
		setName("principal");
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				formWindowClosing(evt);
			}
		});

		contenu.setLayout(null);
		contenu.setBackground(new java.awt.Color(255, 255, 255));
		getContentPane().add(contenu, java.awt.BorderLayout.CENTER);
		mnInfrastructure.setText("Infrastructure");
		mnInfrastructure.add(getStation());
		mnInfrastructure.add(getOuvrage());
		mnInfrastructure.add(getDF());
		mnInfrastructure.add(getDC());
		menuBar.add(mnInfrastructure);

		mnAnalyse.setText("Analyse");
		b_lots.setText("Bilan par lot");
		b_lots.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				b_lotsActionPerformed(evt);
			}
		});

		mnAnalyse.add(b_lots);

		b_periodesFonctDisp.setText("Bilan fonctionnement DC");
		b_periodesFonctDisp.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				b_periodesFonctDispActionPerformed(evt);
			}
		});

		mnAnalyse.add(b_periodesFonctDisp);

		b_tauxEchappement.setText("Bilan taux d'\u00e9chappement");
		b_tauxEchappement.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				b_tauxEchappementActionPerformed(evt);
			}
		});

		mnAnalyse.add(b_tauxEchappement);

		b_coeffConversion.setText("Bilan coeff conversion");
		b_coeffConversion.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				b_coeffConversionActionPerformed(evt);
			}
		});

		mnAnalyse.add(b_coeffConversion);

		b_conditionsEnv.setText("Bilan conditions env.");
		b_conditionsEnv.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				b_conditionsEnvActionPerformed(evt);
			}
		});

		mnAnalyse.add(b_conditionsEnv);

		b_operationsPbFonc.setText("Bilan pb operations");
		b_operationsPbFonc.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				b_operationsPbFoncActionPerformed(evt);
			}
		});

		mnAnalyse.add(b_operationsPbFonc);

		b_migration.setText("Bilan migration");

		URL loc = getClass().getResource("/images/saumon.png");
		if (loc == null) {
			System.out.println("Probl�me avec les resources image");
		} else {
			b_migration.setIcon(new ImageIcon(getClass().getResource("/images/saumon.png")));

		}
		b_migration.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				b_migrationActionPerformed(evt);
			}
		});

		mnAnalyse.add(b_migration);

		b_operationsManquantes.setText("Bilan op\u00e9rations manquantes");
		b_operationsManquantes.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				b_operationsManquantesActionPerformed(evt);
			}
		});

		mnAnalyse.add(b_operationsManquantes);

		menuBar.add(mnAnalyse);

		mnMigration.setText("Migrations");
		a_operation.setText("Ajouter op\u00e9ration");
		a_operation.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				a_operationActionPerformed(evt);
			}
		});

		mnMigration.add(a_operation);

		m_operation.setText("Modif/Supp op\u00e9ration");
		m_operation.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				m_operationActionPerformed(evt);
			}
		});

		mnMigration.add(m_operation);

		m_lot.setText("Ajouter/Modif/Supp lots");
		m_lot.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				m_lotActionPerformed(evt);
			}
		});

		mnMigration.add(m_lot);

		mnMigration.add(getI_operationlots_video());
		mnMigration.add(getAMS_OpeMarquage());
		mnMigration.add(getAS_Marque());
		mnMigration.add(jSeparator1);

		opeCourante.setText("Op\u00e9ration courante");
		i_operation.setText("Incr\u00e9menter operation");
		i_operation.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				i_operationActionPerformed(evt);
			}
		});

		opeCourante.add(i_operation);

		s_operation.setText("Modif/Supp op\u00e9ration");
		s_operation.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				s_operationActionPerformed(evt);
			}
		});

		opeCourante.add(s_operation);

		a_lot.setText("Ajouter/Modif/Supp lots");
		a_lot.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				a_lotActionPerformed(evt);
			}
		});

		opeCourante.add(a_lot);

		mnMigration.add(opeCourante);

		menuBar.add(mnMigration);

		mnOutils.setText("Outils");
		a_table.setText("Afficher/Exporter table");
		a_table.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				a_tableActionPerformed(evt);
			}
		});

		mnOutils.add(a_table);

		a_requete.setText("Afficher/Exporter SQL");
		a_requete.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				a_requeteActionPerformed(evt);
			}
		});

		mnReferentiel.add(ams_taxonvideo);
		mnReferentiel.setText("R�f�rentiel");
		ams_taxonvideo.setText("Ajouter/Modif/Supp Taxon vid�o");
		ams_taxonvideo.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ams_taxonVideoActionPerformed(evt);
			}
		});
		mnReferentiel.add(ams_taillevideo);
		ams_taillevideo.setText("Ajouter/Modif/Supp Taille vid�o");
		ams_taillevideo.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ams_tailleVideoActionPerformed(evt);
			}
		});

		menuBar.add(mnReferentiel);

		mnOutils.add(a_requete);

		i_fichier.setText("Importer fichier");
		i_fichier.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				i_fichierActionPerformed(evt);
			}
		});

		mnOutils.add(i_fichier);

		menuBar.add(mnOutils);

		menuBar.add(mnMasque);
		mnMasque.setText("Masque");

		mnMasqueGestion = new JMenuItem("Gestion");
		mnMasque.add(mnMasqueGestion);

		mnMasqueGestion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mnMasqueAjouterActionPerformed(e);
			}
		});

		mnSelectionMasqueOpe = new JMenu("Select. (masq. ope)");
		mnSelectionMasqueOpe.setIcon(new ImageIcon(IhmAppli.class.getResource("/images/operation.png")));
		mnMasque.add(mnSelectionMasqueOpe);
		groupradiomasqueope = new ButtonGroup();
		mnSelectionMasqueLot = new JMenu("Select. (masq. lot)");
		mnMasque.add(mnSelectionMasqueLot);
		mnSelectionMasqueLot.setIcon(new ImageIcon(IhmAppli.class.getResource("/images/truite_mer.png")));
		groupradiomasquelot = new ButtonGroup();
		// mnSelectionMasqueOpe.addActionListener(new ActionListener() {
		// public void actionPerformed(ActionEvent e) {
		// mnMasqueSelectionActionPerformed(e);
		// }
		// });

		setJMenuBar(menuBar);
		java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		setBounds((screenSize.width - 1024) / 2, (screenSize.height - 800) / 2, 1024, 800);
		// setBounds((screenSize.width-868)/2, (screenSize.height-662)/2, 868,
		// 662);

	}// GEN-END:initComponents

	private void b_operationsManquantesActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_b_operationsManquantesActionPerformed
		this.changeContenu(new BilanOpeManquantes());
	}// GEN-LAST:event_b_operationsManquantesActionPerformed

	private void b_migrationActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_b_migrationActionPerformed
		this.changeContenu(new BilanMigration());
	}// GEN-LAST:event_b_migrationActionPerformed

	private void b_operationsPbFoncActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_b_operationsPbFoncActionPerformed
		this.changeContenu(new BilanOpePbFonctionnement());
	}// GEN-LAST:event_b_operationsPbFoncActionPerformed

	private void b_conditionsEnvActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_b_conditionsEnvActionPerformed
		this.changeContenu(new BilanConditionsEnv());
	}// GEN-LAST:event_b_conditionsEnvActionPerformed

	private void b_coeffConversionActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_b_coeffConversionActionPerformed
		this.changeContenu(new BilanCoeffConversion());
	}// GEN-LAST:event_b_coeffConversionActionPerformed

	private void ams_taxonVideoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_ams_taxonVideoActionPerformed
		this.changeContenu(new AMSTaxonVideo());
	}// GEN-LAST:event_ams_taxonVideoActionPerformed

	private void ams_tailleVideoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_ams_tailleVideoActionPerformed
		this.changeContenu(new AMSTailleVideo());
	}// GEN-LAST:event_ams_tailleVideoActionPerformed

	private void b_tauxEchappementActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_b_tauxEchappementActionPerformed
		this.changeContenu(new BilanTauxEchappement());
	}// GEN-LAST:event_b_tauxEchappementActionPerformed

	private void b_periodesFonctDispActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_b_periodesFonctDispActionPerformed
		this.changeContenu(new BilanFonctionnementDisp());
	}// GEN-LAST:event_b_periodesFonctDispActionPerformed

	private void b_lotsActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_b_lotsActionPerformed
		this.changeContenu(new BilanLots());
	}// GEN-LAST:event_b_lotsActionPerformed

	private void a_requeteActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_a_requeteActionPerformed
		this.changeContenu(new AfficherSqlLibre());
	}// GEN-LAST:event_a_requeteActionPerformed

	private void a_tableActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_a_tableActionPerformed
		this.changeContenu(new AfficherTable());
	}// GEN-LAST:event_a_tableActionPerformed

	private void i_fichierActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_i_fichierActionPerformed
		this.changeContenu(new ImporterFichier());
	}// GEN-LAST:event_i_fichierActionPerformed

	private void s_operationActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_s_operationActionPerformed
		this.changeContenu(new ModifierOperation(Lanceur.getOperationCourante()));
	}// GEN-LAST:event_s_operationActionPerformed

	private void m_operationActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_m_operationActionPerformed
		this.changeContenu(new SelectionnerOperation());
	}// GEN-LAST:event_m_operationActionPerformed

	private void m_lotActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_m_lotActionPerformed
		this.changeContenu(new SelectionnerOperation());
	}// GEN-LAST:event_m_lotActionPerformed

	private void a_lotActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_a_lotActionPerformed
		this.changeContenu(new AjouterLot(Lanceur.getOperationCourante()));
	}// GEN-LAST:event_a_lotActionPerformed

	private void i_operationActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_i_operationActionPerformed
		this.changeContenu(new IncrementerOperation(Lanceur.getOperationCourante()));
	}// GEN-LAST:event_i_operationActionPerformed

	/*
	 * 03-04-2009 S�bastien Laigre Ajout de la fonction ajouterDF
	 */
	private void a_DFActionPerformed(ActionEvent e) {
		this.changeContenu(new AjouterDF());
	}

	/*
	 * 03-04-2009 S�bastien Laigre Ajout de la fonction ModifierDF
	 */
	private void m_DFActionPerformed(ActionEvent e) {
		this.changeContenu(new ModifierDF());
	}

	/*
	 * 03-04-2009 S�bastien Laigre Ajout de la fonction ArreterDF
	 */
	private void s_DFActionPerformed(ActionEvent e) {
		this.changeContenu(new ArreterDF());
	}

	/*
	 * 03-04-2009 S�bastien Laigre Ajout de la fonction ajouterDC
	 */
	private void a_DCActionPerformed(ActionEvent e) {
		this.changeContenu(new AjouterDC());
	}

	/*
	 * 03-04-2009 S�bastien Laigre Ajout de la fonction ModifierDC
	 */
	private void m_DCActionPerformed(ActionEvent e) {
		this.changeContenu(new ModifierDC());
	}

	/*
	 * 03-04-2009 S�bastien Laigre Ajout de la fonction ModifierDF
	 */
	private void ams_PeriodeDFActionPerformed(ActionEvent e) {
		this.changeContenu(
				new AMSPeriodeFonctionnement(Messages.getString("AjouterPeriodeFonctionnement.AMSPeriodeFonc")));
	}

	/*
	 * 03-04-2009 S�bastien Laigre Ajout de la fonction ModifierDF
	 */
	private void ams_PeriodeDCActionPerformed(ActionEvent e) {
		this.changeContenu(
				new AMSPeriodeFonctionnement(Messages.getString("AjouterPeriodeFonctionnement.AMSPeriodeFonc")));
	}

	/*
	 * 03-04-2009 S�bastien Laigre Ajout de la fonction ArreterStation
	 */
	private void s_stationActionPerformed(ActionEvent e) {
		this.changeContenu(new ArreterStation());
	}

	/*
	 * 02-01-2015 C�dric Briand Ajout de la fonction AjouterMasque
	 */
	private void mnMasqueAjouterActionPerformed(ActionEvent e) {
		this.changeContenu(new AMSMasque());
	}

	/*
	 * 10-03-2015 C�dric Briand Ajout de la fonction Selectionner Masque
	 */
	private void mnMasqueSelectionActionPerformed(ActionEvent e) {
		this.changeContenu(new SelectionnerMasque());
	}

	/**
	 * Changement du contenu de la fen�tre
	 * 
	 * @param _ihmContenu
	 *            la fen�tre
	 */
	public void changeContenu(javax.swing.JPanel _ihmContenu) {

		// Retire le jPanel courant et le remplace par celui demande
		this.getContentPane().remove(this.contenu);

		this.contenu = _ihmContenu;
		this.getContentPane().add(contenu, java.awt.BorderLayout.CENTER);

		// Mise a jour de la fenetre
		this.validate();
		this.repaint();
	}

	/**
	 * Retire le contenu de la fen�tre
	 * 
	 * @param _ihmContenu
	 *            la fen�tre
	 */
	public void removeContenu() {

		// Retire le jPanel courant et le remplace par celui demande
		this.getContentPane().remove(this.contenu);

		// Mise a jour de la fenetre
		this.validate();
		this.repaint();
	}

	/**
	 * @author cedric.briand. 2016 M�thode statique utilis�e dans les interfaces
	 *         pour chercher la valeur d'une chaine dans le combo comme
	 *         certaines classes passent la m�thode toString() avec deux champs
	 *         s�par�s par une virgule, le champ selectionn� est le premier (le
	 *         code).
	 * @param _string
	 * @param _combo
	 * @return
	 */
	public static int getIndexforString(String _string, JComboBox _combo) {
		int index;
		ComboBoxModel combomodel = _combo.getModel();
		String[] valeurscombo = new String[combomodel.getSize()];
		for (int i = 0; i < combomodel.getSize(); i++) {
			String code_libelle = combomodel.getElementAt(i).toString();
			// les combos peuvent �tre remplies avec
			// plusieurs �l�ments s�par�s par des virgules
			// ici je ne r�cup�re que le code (premier �l�ment)
			if (code_libelle != null) {
				String[] vec_code_libelle = code_libelle.split(",");
				valeurscombo[i] = vec_code_libelle[0];
			} else
				valeurscombo[i] = "";
			// certains combos peuvent �tre pr�-remplis avec la valeur nulle
			// je prends ce cas en compte ici
		}
		index = Arrays.asList(valeurscombo).indexOf(_string);
		return (index);
	}

	private void a_operationActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_a_operationActionPerformed
		this.changeContenu(new AjouterOperation());
	}// GEN-LAST:event_a_operationActionPerformed

	private void a_stationActionPerformed(java.awt.event.ActionEvent evt) {
		this.changeContenu(new AjouterStation());
	}

	private void m_ouvrageActionPerformed(java.awt.event.ActionEvent evt) {
		this.changeContenu(new ModifierOuvrage());
	}

	private void m_stationActionPerformed(java.awt.event.ActionEvent evt) {
		this.changeContenu(new ModifierStation());
	}

	private void a_ouvrageActionPerformed(ActionEvent e) {
		this.changeContenu(new AjouterOuvrage());
	}

	private void i_operationlots_video(ActionEvent e) {
		this.changeContenu(new ImporterVideo());
	}

	private void i_periodeDFActionPerformed(ActionEvent e) {
		// on lui fournit le nom de la table pour �viter a l'utilisateur de la
		// saisir
		this.changeContenu(new ImporterFichier("t_periodefonctdispositif_per"));
	}

	private void i_periodeDCActionPerformed(ActionEvent e) {
		// on lui fournit le nom de la table pour �viter a l'utilisateur de la
		// saisir
		this.changeContenu(new ImporterFichier("t_periodefonctdispositif_per"));
	}

	private void ams_condEnvActionPerformed(java.awt.event.ActionEvent evt) {
		this.changeContenu(new AMSConditionEnv());
	}

	private void ams_opeMarquageActionPerformed(java.awt.event.ActionEvent evt) {
		this.changeContenu(new AMSOperationMarquage());
	}

	private void as_MarqueActionPerformed(java.awt.event.ActionEvent evt) {
		this.changeContenu(new AMSMarque());
	}

	private void ams_stationMesureActionPerformed(java.awt.event.ActionEvent evt) {
		this.changeContenu(new AMSStationMesure());
	}

	private void ams_tauxEchappementActionPerformed(java.awt.event.ActionEvent evt) {
		this.changeContenu(new AMSTauxEchappement());
	}

	private void i_condEnvActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_a_stationActionPerformed
		// on lui fournit le nom de la table pour �viter à l'utilisateur de la
		// saisir
		this.changeContenu(new ImportConditionEnv());
	}// GEN-LAST:event_a_stationActionPerformed

	/*
	 * 03-04-2009 S�bastien Laigre Ajout de la fonction ArreterStation
	 */
	private void s_DCActionPerformed(ActionEvent e) {
		this.changeContenu(new ArreterDC());
	}

	private void formWindowClosing(java.awt.event.WindowEvent evt) {// GEN-FIRST:event_formWindowClosing
		// Quitte proprement le programme en liberant les ressources
		this.lanceur.quitter();
	}// GEN-LAST:event_formWindowClosing

	// private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt)
	// {//GEN-FIRST:event_exitMenuItemActionPerformed
	// // Quitte proprement le programme en liberant les ressources
	// this.lanceur.quitter() ;
	// }//GEN-LAST:event_exitMenuItemActionPerformed

	// Variables declaration - do not modify

	/**
	 * This method initializes a_ouvrage
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getA_ouvrage() {
		if (a_ouvrage == null) {
			a_ouvrage = new JMenuItem();
			a_ouvrage.setActionCommand("ajouter ouvrage");
			a_ouvrage.setText("Ajouter");
			a_ouvrage.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					a_ouvrageActionPerformed(e);
				}

			});
		}
		return a_ouvrage;
	}

	/**
	 * This method initializes a_DF
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getA_DF() {
		if (a_DF == null) {
			a_DF = new JMenuItem();
			a_DF.setText("Ajouter");
			a_DF.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					a_DFActionPerformed(e);
				}

			});
		}
		return a_DF;
	}

	/**
	 * This method initializes a_DC
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getA_DC() {
		if (a_DC == null) {
			a_DC = new JMenuItem();
			a_DC.setText("Ajouter");
			a_DC.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					a_DCActionPerformed(e);
				}
			});
		}
		return a_DC;
	}

	/**
	 * This method initializes Station
	 * 
	 * @return javax.swing.JMenu
	 */
	private JMenu getStation() {
		if (Station == null) {
			Station = new JMenu();
			Station.setText("Station");
			Station.add(getA_station());
			Station.add(getM_station());
			Station.add(getS_station());
			Station.add(getAMS_StationMesure());
			Station.add(getAMS_CondEnv());
			Station.add(getI_CondEnv());
		}
		return Station;
	}

	private JMenuItem getI_CondEnv() {
		if (i_CondEnv == null) {
			i_CondEnv = new JMenuItem();
			i_CondEnv.setText("Importer un condition envi.");
			i_CondEnv.setActionCommand("Importer un condition envi.");
			i_CondEnv.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					i_condEnvActionPerformed(e);
				}
			});
		}
		return i_CondEnv;
	}

	private JMenuItem getAMS_CondEnv() {
		if (ams_CondEnv == null) {
			ams_CondEnv = new JMenuItem();
			ams_CondEnv.setText("A/M/S une condition envi.");
			ams_CondEnv.setActionCommand("A/M/S une condition envi.");
			ams_CondEnv.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					ams_condEnvActionPerformed(e);
				}
			});
		}
		return ams_CondEnv;
	}

	private JMenuItem getAMS_OpeMarquage() {
		if (ams_opeMarquage == null) {
			ams_opeMarquage = new JMenuItem();
			ams_opeMarquage.setText("A/M/S une op�. de marquage");
			ams_opeMarquage.setActionCommand("A/M/S une op�. de marquage");
			ams_opeMarquage.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					ams_opeMarquageActionPerformed(e);
				}
			});
		}
		return ams_opeMarquage;
	}

	private JMenuItem getAMS_StationMesure() {
		if (ams_StationMesure == null) {
			ams_StationMesure = new JMenuItem();
			ams_StationMesure.setText("A/M/S une station de mesure");
			ams_StationMesure.setActionCommand("A/M/S une station de mesure");
			ams_StationMesure.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					ams_stationMesureActionPerformed(e);
				}
			});
		}
		return ams_StationMesure;
	}

	private JMenuItem getAS_Marque() {
		if (ams_Marque == null) {
			ams_Marque = new JMenuItem();
			ams_Marque.setText("Ajout. Mod. Supp. une marque");
			ams_Marque.setActionCommand("Ajout. Mod. Supp. une marque");
			ams_Marque.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					as_MarqueActionPerformed(e);
				}
			});
		}
		return ams_Marque;
	}

	private JMenuItem getAMS_TauxEchappement() {
		if (ams_tauxEchappement == null) {
			ams_tauxEchappement = new JMenuItem();
			ams_tauxEchappement.setText("A/M/S un taux d'�chappement");
			ams_tauxEchappement.setActionCommand("A/M/S un taux d'�chappement");
			ams_tauxEchappement.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					ams_tauxEchappementActionPerformed(e);
				}
			});
		}
		return ams_tauxEchappement;
	}

	/**
	 * This method initializes Ouvrage
	 * 
	 * @return javax.swing.JMenu
	 */
	private JMenu getOuvrage() {
		if (Ouvrage == null) {
			Ouvrage = new JMenu();
			Ouvrage.setText("Ouvrage");
			Ouvrage.add(getA_ouvrage());
			Ouvrage.add(getM_ouvrage());
			Ouvrage.add(getAMS_TauxEchappement());
		}
		return Ouvrage;
	}

	/**
	 * This method initializes DF
	 * 
	 * @return javax.swing.JMenu
	 */
	private JMenu getDF() {
		if (DF == null) {
			DF = new JMenu();
			DF.setText("DF");
			DF.add(getA_DF());
			DF.add(getM_DF());
			DF.add(getS_DF());
			DF.add(getAMS_periode_DF());
			DF.add(getI_periode_DF());
		}
		return DF;
	}

	/**
	 * This method initializes DC
	 * 
	 * @return javax.swing.JMenu
	 */
	private JMenu getDC() {
		if (DC == null) {
			DC = new JMenu();
			DC.setText("DC");
			DC.add(getA_DC());
			DC.add(getM_DC());
			DC.add(getS_DC());
			DC.add(getAMS_periode_DC());
			DC.add(getI_periode_DC());
		}
		return DC;
	}

	/**
	 * This method initializes m_station
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getM_station() {
		if (m_station == null) {
			m_station = new JMenuItem();
			m_station.setText("Modifier");
			m_station.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					m_stationActionPerformed(e);
				}
			});
		}
		return m_station;
	}

	/**
	 * This method initializes m_ouvrage
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getM_ouvrage() {
		if (m_ouvrage == null) {
			m_ouvrage = new JMenuItem();
			m_ouvrage.setText("Modifier");
			m_ouvrage.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					m_ouvrageActionPerformed(e);
				}
			});
		}
		return m_ouvrage;
	}

	/**
	 * This method initializes a_station
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getA_station() {
		if (a_station == null) {
			a_station = new JMenuItem();
			a_station.setText("Ajouter");
			a_station.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					a_stationActionPerformed(e);
				}
			});
		}
		return a_station;
	}

	/**
	 * This method initializes m_DF
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getM_DF() {
		if (m_DF == null) {
			m_DF = new JMenuItem();
			m_DF.setText("Modifier");
			m_DF.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					m_DFActionPerformed(e);
				}
			});
		}
		return m_DF;
	}

	/**
	 * This method initializes a_periode_DF
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getAMS_periode_DF() {
		if (a_periode_DF == null) {
			a_periode_DF = new JMenuItem();
			a_periode_DF.setText("A/M/S p\u00e9riode de fonctionnement");
			a_periode_DF.setActionCommand("A/M/S p\u00e9riode de fonctionnement");
			a_periode_DF.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					ams_PeriodeDFActionPerformed(e);
				}
			});
		}
		return a_periode_DF;
	}

	/**
	 * This method initializes i_periode_DF
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getI_periode_DF() {
		if (i_periode_DF == null) {
			i_periode_DF = new JMenuItem();
			i_periode_DF.setText("Importer p\u00e9riodes");
			i_periode_DF.setActionCommand("Importer p\u00e9riodes");
			i_periode_DF.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					i_periodeDFActionPerformed(e);
				}
			});
		}
		return i_periode_DF;
	}

	/**
	 * This method initializes m_DC
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getM_DC() {
		if (m_DC == null) {
			m_DC = new JMenuItem();
			m_DC.setText("Modifier");
			m_DC.setActionCommand("Modifier");
			m_DC.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					m_DCActionPerformed(e);
				}
			});
		}
		return m_DC;
	}

	/**
	 * This method initializes a_periode_DC
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getAMS_periode_DC() {
		if (a_periode_DC == null) {
			a_periode_DC = new JMenuItem();
			a_periode_DC.setText("A/M/S p\u00e9riode de fonctionnement");
			a_periode_DC.setActionCommand("A/M/S p\u00e9riode de fonctionnement");
			a_periode_DC.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					ams_PeriodeDCActionPerformed(e);
				}
			});
		}
		return a_periode_DC;
	}

	/**
	 * This method initializes i_periode_DC
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getI_periode_DC() {
		if (i_periode_DC == null) {
			i_periode_DC = new JMenuItem();
			i_periode_DC.setText("Importer p�riodes");
			i_periode_DC.setActionCommand("Importer p\u00e9riodes");
			i_periode_DC.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					i_periodeDCActionPerformed(e);
				}
			});
		}
		return i_periode_DC;
	}

	/**
	 * This method initializes s_DF
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getS_DF() {
		if (s_DF == null) {
			s_DF = new JMenuItem();
			s_DF.setText("Arr�ter");
			s_DF.setActionCommand("Arreter");
			s_DF.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					s_DFActionPerformed(e);
				}
			});
		}
		return s_DF;
	}

	/**
	 * This method initializes s_DC
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getS_DC() {
		if (s_DC == null) {
			s_DC = new JMenuItem();
			s_DC.setText("Arr�ter");
			s_DC.setActionCommand("Arr\u00eater  un DC");
			s_DC.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					s_DCActionPerformed(e);
				}
			});
		}
		return s_DC;
	}

	/**
	 * This method initializes s_station
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getS_station() {
		if (s_station == null) {
			s_station = new JMenuItem();
			s_station.setText("Arr�ter");
			s_station.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					s_stationActionPerformed(e);
				}
			});
		}
		return s_station;
	}

	/**
	 * This method initializes i_operationlots_video
	 * 
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getI_operationlots_video() {
		if (i_operationlots_video == null) {
			i_operationlots_video = new JMenuItem();
			i_operationlots_video.setText("Import vid�o");
			i_operationlots_video.setActionCommand("Importvideo");
			i_operationlots_video.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					i_operationlots_video(e);
				}
			});
		}
		return i_operationlots_video;
	}

	/**
	 * Charge la liste des masquesope, affiche les radioboutons et s�lectionne
	 * le masque des pr�f�rences, sans s�lection ope_defaut est s�lectionn�
	 */
	public void chargeMasqueOpe() {
		listemasqueope = new ListeMasqueOpe();
		try {
			listemasqueope.chargeSansFiltre();
		} catch (Exception e) {
			logger.log(Level.SEVERE, " chargeMasqueOpe  ", e);
		}
		Preferences prefs = Preferences.userRoot();
		String ope_code = prefs.get("masqueope", "opedefaut");
		mnSelectionMasqueOpe.removeAll();
		for (int i = 0; i < listemasqueope.size(); i++) {
			MasqueOpe masqueope = (MasqueOpe) listemasqueope.get(i);
			JRadioButtonMenuItem rdbtnmntmNewRadioItem = new JRadioButtonMenuItem(masqueope.getMasque().getCode());
			rdbtnmntmNewRadioItem.setActionCommand(masqueope.getMasque().getCode());
			mnSelectionMasqueOpe.add(rdbtnmntmNewRadioItem);
			if (masqueope.getMasque().getCode().equals(ope_code)) {
				rdbtnmntmNewRadioItem.setSelected(true);
			}
			groupradiomasqueope.add(rdbtnmntmNewRadioItem);
			rdbtnmntmNewRadioItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					radiobuttonmasqueopeselected(e);
				}

			});

		}

	}

	/**
	 * Charge la liste des masqueslot, affiche les radioboutons et s�lectionne
	 * le masque des pr�f�rences, sans s�lection lot_defaut est s�lectionn�
	 */
	public void chargeMasqueLot() {
		listemasquelot = new ListeMasqueLot();
		try {
			listemasquelot.chargeSansFiltre();
		} catch (Exception e) {
			logger.log(Level.SEVERE, " chargeMasqueOpe  ", e);
		}
		Preferences prefs = Preferences.userRoot();
		String lot_code = prefs.get("masquelot", "lotdefaut");
		mnSelectionMasqueLot.removeAll();
		for (int i = 0; i < listemasquelot.size(); i++) {
			MasqueLot masquelot = (MasqueLot) listemasquelot.get(i);
			JRadioButtonMenuItem rdbtnmntmNewRadioItem = new JRadioButtonMenuItem(masquelot.getMasque().getCode());
			rdbtnmntmNewRadioItem.setActionCommand(masquelot.getMasque().getCode());
			mnSelectionMasqueLot.add(rdbtnmntmNewRadioItem);
			if (masquelot.getMasque().getCode().equals(lot_code)) {
				rdbtnmntmNewRadioItem.setSelected(true);
			}
			groupradiomasquelot.add(rdbtnmntmNewRadioItem);
			rdbtnmntmNewRadioItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					radiobuttonmasquelotselected(e);
				}

			});

		}

	}

	/**
	 * lorsque le radiobouton masqueope est s�lectionn�, r�cup�re
	 * l'ActionCommand pour s�lectionner le masque dans les pr�f�rences.
	 * 
	 * @param e
	 */
	private void radiobuttonmasqueopeselected(ActionEvent e) {
		String ope_mas_code = e.getActionCommand(); // r�cup�ration du code du
													// masque
		Preferences prefs = Preferences.userRoot();
		// les preferences sont stock�es dans les cl�s masqueope et
		// masquelot
		prefs.put("masqueope", ope_mas_code);
		System.out.println("masque selectionn� :" + ope_mas_code);
		// si la fen�tre en cours est le masque operation, rechargement de ce
		// dernier
		Component[] comps = this.getContentPane().getComponents();
		if (comps[0] != null) {
			if (comps[0].getClass() == AjouterOperation.class) {
				this.changeContenu(new AjouterOperation());
			} else if (comps[0].getClass() == SelectionnerOperation.class) {
				this.changeContenu(new SelectionnerOperation());
			}
		}
	}

	/**
	 * lorsque le radiobouton masqueope est s�lectionn�, r�cup�re
	 * l'ActionCommand pour s�lectionner le masque dans les pr�f�rences.
	 * 
	 * @param e
	 */
	private void radiobuttonmasquelotselected(ActionEvent e) {
		String lot_mas_code = e.getActionCommand(); // r�cup�ration du code du
													// masque
		Preferences prefs = Preferences.userRoot();
		// les preferences sont stock�es dans les cl�s masqueope et
		// masquelot
		prefs.put("masquelot", lot_mas_code);

		System.out.println("masque selectionn� :" + lot_mas_code);
		// si la fen�tre en cours est le masque lot, rechargement de ce dernier
		Component[] comps = this.getContentPane().getComponents();
		if (comps[0] != null) {
			if (comps[0].getClass() == AjouterLot.class) {
				this.changeContenu(new AjouterLot(Lanceur.getOperationCourante()));
			}
		}

	}

	/**
	 * Cr�ation d'un formatteur
	 * 
	 * @param s
	 *            : la chaine de caractere indiquant le format de saisie
	 * @return le formatteur
	 */
	public static MaskFormatter createFormatter(String s) {
		MaskFormatter formatter = null;
		try {
			formatter = new MaskFormatter(s);
		} catch (java.text.ParseException exc) {
			System.err.println("formatter is bad: " + exc.getMessage());
			System.exit(-1);
		}
		return formatter;
	}

}