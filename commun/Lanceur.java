/*
 **********************************************************************
 * 
 * Nom fichier :        Lanceur.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * @author             	Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * @version
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package commun;

import java.util.Hashtable;
import java.util.prefs.Preferences;

import analyse.referenciel.ListeRefNiveauEchappement;
import infrastructure.referenciel.ListeRefNatureOuvrage;
import infrastructure.referenciel.ListeRefParamQualEnv;
import infrastructure.referenciel.ListeRefParamQuantEnv;
import infrastructure.referenciel.ListeRefTypeDC;
import infrastructure.referenciel.ListeRefTypeDF;
import infrastructure.referenciel.ListeRefTypeFonctionnement;
import migration.MethodeObtention;
import migration.Operation;
import migration.referenciel.ListeRefDevenir;
import migration.referenciel.ListeRefImportancePathologie;
import migration.referenciel.ListeRefLocalisation;
import migration.referenciel.ListeRefNatureMarque;
import migration.referenciel.ListeRefNiveauTaxonomique;
import migration.referenciel.ListeRefParamQualBio;
import migration.referenciel.ListeRefParamQuantBio;
import migration.referenciel.ListeRefPathologie;
import migration.referenciel.ListeRefPrelevement;
import migration.referenciel.ListeRefStade;
import migration.referenciel.ListeRefTaxon;
import migration.referenciel.ListeRefTypeQuantite;
import migration.referenciel.ListeTailleVideo;
import migration.referenciel.ListeTaxonVideo;
import migration.referenciel.SousListeOperations;
import systeme.Masque;
import systeme.MasqueLot;
import systeme.MasqueOpe;
import systeme.referenciel.ListeMasque;
import systeme.referenciel.ListeMasqueLot;
import systeme.referenciel.ListeMasqueOpe;
import systeme.referenciel.TypeMasque;

/**
 * Classe avec methode main pour le lancement de l'application
 */
public class Lanceur {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	IhmAppli ihmAppli; // l'interface graphique principale de l'application

	// Listes de reference, gardees en memoire pendant toute l'execution du
	// programme
	private static ListeTailleVideo listeTailleVideo = null;

	private static ListeRefNatureOuvrage listeRefNatureOuvrage = null;

	private static ListeRefTypeFonctionnement listeRefTypeFonctionnement = null;

	private static ListeRefTypeDF listeRefTypeDF = null;

	private static ListeRefTypeDC listeRefTypeDC = null;

	private static ListeRefTypeQuantite listeRefTypeQuantite = null;

	private static ListeRefDevenir listeRefDevenir = null;

	private static ListeRefPathologie listeRefPathologie = null;

	private static ListeRefImportancePathologie listeRefImportancePathologie = null;

	private static ListeRefLocalisation listeRefLocalisationMarque = null;

	private static ListeRefLocalisation listeRefLocalisationPathologie = null;

	private static ListeRefNatureMarque listeRefNatureMarque = null;

	private static ListeRefParamQuantEnv listeRefParamQuantEnv = null;

	private static ListeRefParamQuantBio listeRefParamQuantBio = null;

	private static ListeRefParamQualEnv listeRefParamQualEnv = null;

	private static ListeRefParamQualBio listeRefParamQualBio = null;

	private static ListeRefStade listeRefStade = null;

	private static ListeRefNiveauTaxonomique listeRefNiveauTaxonomique = null;

	private static ListeRefTaxon listeRefTaxon = null; // @jve:decl-index=0:

	private static ListeRefNiveauEchappement listeRefNiveauEchappement = null; // @jve:decl-index=0:

	private static ListeRefPrelevement listeRefPrelevement = null;

	private static ListeMasque listeMasques = null;

	private static String[] methodesObtentionLot = null;

	private static String[] methodesObtentionCaracteristiques = null;

	private static String[] typeMasque = null;

	private static ListeTables listeTables = null;

	private static ListeSequences listeSequences = null;

	// c�dric ces infos aident pour garder de l'info sur les actions dans le
	// lanceur au cours du programme
	private static Operation operationCourante = null; // @jve:decl-index=0:

	private static SousListeOperations souslisteOperationParcourue = null;

	private static Hashtable elementsSelectionnes = null;

	private static ListeTaxonVideo listeTaxonVideo = null;

	private static MasqueOpe masqueopeSelectionne;
	private static MasqueLot masquelotSelectionne;
	private static ListeMasqueOpe listeMasqueOpe = null;
	private static ListeMasqueLot listeMasqueLot = null;

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		new Lanceur();
	}

	/**
	 * Construction du lanceur de l'application
	 */
	public Lanceur() {
		// Creation d'une connexion a la base de donnees. On utilise
		// 'getInstance()' et pas 'new' car c'est un singleton
		try {
			ConnexionBD.getInstance();
			System.out.println("Lanceur : Connexion BD ouverte");
		} catch (Exception e) {
			Erreur.print(e, Erreur.B1000);
		}

		// Chargement des listes de references permanentes
		initListesRef();
		System.out.println("Lanceur : Listes de references chargees");

		// les sequences sont mises a jour
		// new SequenceUpdater();

		// Pour sauvegarder temporairement des selections de menu
		elementsSelectionnes = new Hashtable();

		// Creation de l'interface graphique et affichage de celle-ci
		ihmAppli = new IhmAppli(Lanceur.this);
		ihmAppli.setVisible(true);

		// r�cup�ration des pr�f�rences de l'utilisateur
		// en l'absence de donn�es, la pr�f�rence par d�faut est "ope_defaut" ou
		// "lot_defaut"
		// un trigger dans la base de donn�es emp�che la suppression de cette
		// valeur par d�faut
		Preferences preference = Preferences.userRoot();
		Lanceur.masqueopeSelectionne = new MasqueOpe(new Masque(preference.get("masqueope", "ope_defaut")));
		Lanceur.masquelotSelectionne = new MasqueLot(new Masque(preference.get("masquelot", "lot_defaut")));
		// connexionWindow.setVisible(true);
		System.out.println("Lanceur : Ihm affichee");

	}

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/*
	 * Initialise les listes de reference et charge leur contenu detaille
	 */
	private void initListesRef() {

		// Initialisation des listes
		listeTailleVideo = new ListeTailleVideo();
		listeRefNatureOuvrage = new ListeRefNatureOuvrage();
		listeRefTypeFonctionnement = new ListeRefTypeFonctionnement();
		listeRefTypeDF = new ListeRefTypeDF();
		listeRefTypeDC = new ListeRefTypeDC();
		listeRefTypeQuantite = new ListeRefTypeQuantite();
		listeRefDevenir = new ListeRefDevenir();
		listeRefPathologie = new ListeRefPathologie();
		listeRefImportancePathologie = new ListeRefImportancePathologie();
		listeRefLocalisationMarque = new ListeRefLocalisation("Marque");
		listeRefLocalisationPathologie = new ListeRefLocalisation("Pathologie");
		listeRefParamQuantEnv = new ListeRefParamQuantEnv();
		listeRefNatureMarque = new ListeRefNatureMarque();
		listeRefParamQuantBio = new ListeRefParamQuantBio();
		listeRefParamQualEnv = new ListeRefParamQualEnv();
		listeRefParamQualBio = new ListeRefParamQualBio();
		listeRefStade = new ListeRefStade();
		listeRefNiveauTaxonomique = new ListeRefNiveauTaxonomique();
		listeRefTaxon = new ListeRefTaxon(Lanceur.listeRefNiveauTaxonomique);
		listeRefNiveauEchappement = new ListeRefNiveauEchappement();
		listeRefPrelevement = new ListeRefPrelevement();
		listeTables = new ListeTables();
		listeSequences = new ListeSequences();
		listeTaxonVideo = new ListeTaxonVideo();

		// Chargement du contenu detaille des listes de reference

		try {
			listeTailleVideo.chargeSansFiltre();
			listeRefNatureOuvrage.chargeSansFiltreDetails();
			listeRefTypeFonctionnement.chargeSansFiltreDetails();
			listeRefTypeDF.chargeSansFiltreDetails();
			listeRefTypeDC.chargeSansFiltreDetails();
			listeRefTypeQuantite.chargeSansFiltreDetails();
			listeRefDevenir.chargeSansFiltreDetails();
			listeRefPathologie.chargeSansFiltreDetails();
			listeRefImportancePathologie.chargeSansFiltreDetails();
			listeRefLocalisationMarque.chargeSansFiltreDetails();
			listeRefLocalisationPathologie.chargeSansFiltreDetails();
			listeRefNatureMarque.chargeSansFiltreDetails();
			listeRefParamQuantEnv.chargeSansFiltreDetails();
			listeRefParamQuantBio.chargeSansFiltreDetails();
			listeRefParamQualEnv.chargeSansFiltreDetails();
			listeRefParamQualBio.chargeSansFiltreDetails();
			listeRefStade.chargeSansFiltreDetails();
			listeRefNiveauTaxonomique.chargeSansFiltreDetails();
			listeRefTaxon.chargeSansFiltreDetails();
			listeRefNiveauEchappement.chargeSansFiltreDetails();
			listeRefPrelevement.chargeSansFiltreDetails();
			listeTables.chargeSansFiltre();
			listeSequences.chargeSansFiltre();
			listeTaxonVideo.chargeSansFiltre();
			methodesObtentionLot = MethodeObtention.getMethodesLot();
			methodesObtentionCaracteristiques = MethodeObtention.getMethodesCaracteristiques();
			typeMasque = TypeMasque.getTypeMasque();

		} catch (Exception e) {
			Erreur.print(e, Erreur.B1002);
		}
	}

	/**
	 * Quitte l'application
	 */
	public void quitter() {

		// Fermeture de la connexion a la BD
		try {
			ConnexionBD.getInstance().getConnexion().close();
			System.out.println("Lanceur : Connexion DB fermee");
		} catch (Exception e) {
			Erreur.print(e, Erreur.B1001);
		} finally {
			// Arret de l'interface graphique
			this.ihmAppli.dispose();
			this.ihmAppli.setVisible(false);
			System.out.println("Lanceur : Ihm detruite");

			// Quitte
			System.exit(0);
		}
	}

	/**
	 * @return the masqueope
	 */
	public static MasqueOpe getMasqueope() {
		return masqueopeSelectionne;
	}

	/**
	 * @param masqueope
	 *            the masqueope to set
	 */
	public static void setMasqueope(MasqueOpe masqueope) {
		Lanceur.masqueopeSelectionne = masqueope;
	}

	/**
	 * @return la liste des tailles vid�o
	 */
	public static ListeTailleVideo getListeTailleVideo() {
		return listeTailleVideo;
	}

	/**
	 * @return la liste des natures d'ouvrage
	 */
	public static ListeRefNatureOuvrage getListeRefNatureOuvrage() {
		return listeRefNatureOuvrage;
	}

	/**
	 * @return la liste des types de fonctionnement
	 */
	public static ListeRefTypeFonctionnement getListeRefTypeFonctionnement() {
		return listeRefTypeFonctionnement;
	}

	/**
	 * @return la liste des types de DF
	 */
	public static ListeRefTypeDF getListeRefTypeDF() {
		return listeRefTypeDF;
	}

	/**
	 * @return la liste des types de DC
	 */
	public static ListeRefTypeDC getListeRefTypeDC() {
		return listeRefTypeDC;
	}

	/**
	 * @return la liste des types de quantit�
	 */
	public static ListeRefTypeQuantite getListeRefTypeQuantite() {
		return listeRefTypeQuantite;
	}

	/**
	 * @return la liste des devenirs
	 */
	public static ListeRefDevenir getListeRefDevenir() {
		return listeRefDevenir;
	}

	/**
	 * @return la liste des pathlogies
	 */
	public static ListeRefPathologie getListeRefPathologie() {
		return listeRefPathologie;
	}

	/**
	 * @return la liste des codes d'importance des patho
	 */
	public static ListeRefImportancePathologie getListeRefImportancePathologie() {
		return listeRefImportancePathologie;
	}

	/**
	 * @return la liste des localisations des marques
	 */
	public static ListeRefLocalisation getListeRefLocalisationMarque() {
		return listeRefLocalisationMarque;
	}

	/**
	 * @return la liste des localisations pathologiques
	 */
	public static ListeRefLocalisation getListeRefLocalisationPathologie() {
		return listeRefLocalisationPathologie;
	}

	/**
	 * @return la liste des natures de marque
	 */
	public static ListeRefNatureMarque getListeRefNatureMarque() {
		return listeRefNatureMarque;
	}

	/**
	 * @return la liste des param�tres quantitatifs environnementaux
	 */
	public static ListeRefParamQuantEnv getListeRefParamQuantEnv() {
		return listeRefParamQuantEnv;
	}

	/**
	 * @return la liste des param�tres quantitatifs biologiques
	 */
	public static ListeRefParamQuantBio getListeRefParamQuantBio() {
		return listeRefParamQuantBio;
	}

	/**
	 * @return la liste des param�tres qualitatifs environnementaux
	 */
	public static ListeRefParamQualEnv getListeRefParamQualEnv() {
		return listeRefParamQualEnv;
	}

	/**
	 * @return la liste des param�tres biologiques
	 */
	public static ListeRefParamQualBio getListeRefParamQualBio() {
		return listeRefParamQualBio;
	}

	/**
	 * @return la liste des stades
	 */
	public static ListeRefStade getListeRefStade() {
		return listeRefStade;
	}

	/**
	 * @return la liste des niveau taxonomiques
	 */
	public static ListeRefNiveauTaxonomique getListeRefNiveauTaxonomique() {
		return listeRefNiveauTaxonomique;
	}

	/**
	 * @return la liste des taxons
	 */
	public static ListeRefTaxon getListeRefTaxon() {
		return listeRefTaxon;
	}

	/**
	 * @return la liste des niveau d'�chappement
	 */
	public static ListeRefNiveauEchappement getListeRefNiveauEchappement() {
		return listeRefNiveauEchappement;
	}

	/**
	 * @return la liste des pr�levements
	 */
	public static ListeRefPrelevement getListeRefPrelevement() {
		return listeRefPrelevement;
	}

	/**
	 * @return la liste des taxons vid�o
	 */
	public static ListeTaxonVideo getListeTaxonVideo() {
		return listeTaxonVideo;
	}

	/**
	 * @param ltv
	 *            la liste des taxons vid�o
	 */
	public static void setListeTaxonVideo(ListeTaxonVideo ltv) {
		listeTaxonVideo = ltv;
	}

	/**
	 * @return la liste des tables
	 */
	public static ListeTables getListeTables() {
		return listeTables;
	}

	/**
	 * @return la liste des s�quences
	 */
	public static ListeSequences getListeSequences() {
		return listeSequences;
	}

	/**
	 * @return la m�thode d'obtention des caract�ristiques
	 */
	public static String[] getMethodesObtentionLot() {
		return methodesObtentionLot;
	}

	/**
	 * @return la m�thode d'obtention des caract�ristiques
	 */
	public static String[] getMethodesObtentionCaracteristiques() {
		return methodesObtentionCaracteristiques;
	}

	/**
	 * @return le type de masque lot ou ope
	 */
	public static String[] getTypeMasque() {
		return typeMasque;
	}

	/**
	 * @return the souslisteOperationParcourue
	 */
	public static SousListeOperations getSouslisteOperationParcourue() {
		return souslisteOperationParcourue;
	}

	/**
	 * @param souslisteOperationParcourue
	 *            the souslisteOperationParcourue to set
	 */
	public static void setSouslisteOperationParcourue(SousListeOperations souslisteOperationParcourue) {
		Lanceur.souslisteOperationParcourue = souslisteOperationParcourue;
	}

	/**
	 * @return the listeMasques
	 */
	public static ListeMasque getListeMasques() {
		return listeMasques;
	}

	/**
	 * @return l'op�ration courante
	 */
	public static Operation getOperationCourante() {
		return operationCourante;
	}

	/**
	 * @param _ope
	 *            l'op�ration courante
	 */
	public static void setOperationCourante(Operation _ope) {
		operationCourante = _ope;
	}

	/**
	 * @return les �l�ments s�lectionn�s
	 */
	public static Hashtable getElementsSelectionnes() {
		return elementsSelectionnes;
	}

	/**
	 * @param _elements
	 *            les �l�ments s�lectionn�s
	 */
	public static void setElementsSelectionnes(Hashtable _elements) {
		elementsSelectionnes = _elements;
	}

	/*
	 * public static Lot getLotCourant() { return lotCourant ; }
	 * 
	 * public static void setLotCourant(Lot _lot) { lotCourant = _lot ; }
	 */

}
