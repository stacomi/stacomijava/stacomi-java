/*
 **********************************************************************
 *
 * Nom fichier :        SousListe.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package commun;

/**
 * Classe abstraite permettant de charger une sous-liste d'elements de la base de donnees. 
 * C'est la sous-liste elle-meme qui effectue les requetes vers la base et qui s'initialise. 
 * Une sous-liste sert pour la lecture de donnees et non pas pour l'ecriture dans la base. 
 * Les objets qu'elle contient doivent implementer l'interface IBaseDonnees. 
 * Elle est differente d'une liste car elle contient une sous partie d'un ensemble d'element, 
 * et elle se rattache a un element parent.
 * @author sam
 *
 */
public abstract class SousListe extends Liste {

    
  ///////////////////////////////////////
  // attributes
  ///////////////////////////////////////

    protected Object objetDeRattachement ; // l'objet pere auquel se rattache la sous-liste

    
    
  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////
    
/**
 * Construit une sousliste
 */    
    public SousListe() {
        super() ;
    } // end sousListe
     
    
/**
 * Construit une sousliste avec un objet de rattachement
 * @param _objetDeRattachement l'objet de rattachement
 */    
    public SousListe(Object _objetDeRattachement) {
        super() ;
        this.setObjetDeRattachement(_objetDeRattachement) ;
    } // end sousListe
    
    
    
  ///////////////////////////////////////
  // operations heritees
  ///////////////////////////////////////
    
/**
 * Methode initialisant la sous-liste avec tous les enregistrements d'une table, 
 * en considerant uniquement ceux lies a l'objet de rattachement. 
 * Le chargement porte sur l'identifiant et le libelle seulement. 
 */
    public abstract void chargeSansFiltre() throws Exception ;    
    
    protected abstract void chargeObjet(Object _objet) throws Exception ;    
 
 /**
 * Methode initialisant la sous-liste avec tous les enregistrements d'une table, 
 * en considerant uniquement ceux lies a l'objet de rattachement. 
 * Le chargement porte sur tous les attributs. 
 */   
    public abstract void chargeSansFiltreDetails() throws Exception ;
    
    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////    
    

/**
 * Retourne l'identifiant de l'objet de rattachement.
 * @return le ou les attributs composant l'identifiant
 */
    public abstract String[] getObjetDeRattachementID() ;

/**
 * Retourne l'objet de rattachement
 * @return l'objet de rattachement
 */
    public Object getObjetDeRattachement() {        
        return this.objetDeRattachement ;
    } // end getObjetDeRattachement        

/**
 * Permet de definir l'objet de rattachement, c'est a dire l'objet qui utilise la sous-liste
 * @param _objetDeRattachement l'objet pere
 */
    public void setObjetDeRattachement(Object _objetDeRattachement) {        
        this.objetDeRattachement = _objetDeRattachement ;
    }
    
    
 // end setObjetDeRattachement        

    
 } // end SousListe



