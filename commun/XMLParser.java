/*
 **********************************************************************
 *
 * Nom fichier :        XMLParser.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             S�bastien Laigre
 * Date de creation :   24 avril 2009
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */
package commun;

import org.dom4j.Document;
import org.dom4j.Attribute;

import java.util.List;
import java.io.*;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

/**
 * @author sebastien.laigre
 * Lit un fichier XML contenant le nom / user / password de la base
 */
public class XMLParser {

	/** L'url de la base */
	private String _url;
	
	/** L'utilisateur de la base */
	private String _user;
	
	/** Le mot de passe de l'utilisateur de la base */
	private String _password;
	
	/**
	 * Initialise l'URL, username et password en fonction 
	 * des informations dans le fichier XML
	 */
	public XMLParser()
	{
		try{
			SAXReader saxReader = new SAXReader();
			Document document = saxReader.read(new File("base_local.xml"));

			List list = document.selectNodes("//root//bdd/@bdurl" );
			Attribute attribute=(Attribute)list.iterator().next();
			_url = attribute.getValue();
			
			list = document.selectNodes("//root//bdd/@user" );
			attribute=(Attribute)list.iterator().next();
			_user = attribute.getValue();
			
			list = document.selectNodes("//root//bdd/@password" );
			attribute=(Attribute)list.iterator().next();
			_password = attribute.getValue();
		}
		catch (DocumentException e)
        {
            System.out.println(e.getMessage());
		}
	}
	
	/**
	 * Acc�s � l'URL de la BDD
	 * @return l'URL de la BDD
	 */
	public String getUrl()
	{
		return _url;
	}
	
	/**
	 * Acc�s � l'utilisateur de la BDD
	 * @return l'utilisateur de la BDD
	 */
	public String getUser()
	{
		return _user;
	}
	
	/**
	 * Acc�s au mot de passe de l'utilisateur de la BDD
	 * @return le mot de passe de l'utilisateur de la BDD
	 */
	public String getPassword()
	{
		return _password;
	}
}
