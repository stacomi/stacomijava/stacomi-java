package commun;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JFormattedTextField.AbstractFormatter;

/**
 * exemple venant de datePicker pour cr�er un format
 * 
 * @author cedric.briand
 *
 */
public class DateLabelFormatter extends AbstractFormatter {

	private String datePattern = "dd/MM/yyyy";
	private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

	@Override
	public String valueToString(Object value) throws ParseException {
		Calendar cal = (Calendar) value;
		if (cal == null) {
			return "";
		}
		return dateFormatter.format(cal.getTime());
	}

	@Override
	public Object stringToValue(String text) throws ParseException {
		if (text == null || text.equals("")) {
			return null;
		}
		Date date = dateFormatter.parse(text);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;

	}

}
