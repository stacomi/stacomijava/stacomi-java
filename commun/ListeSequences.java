/*
 **********************************************************************
 *
 * Nom fichier :        ListeSequences.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package commun;

//import commun.* ;
import java.sql.* ;

/**
 * Liste pour les tables de la base
 */

public class ListeSequences extends Liste {

   /**
	 * 
	 */
	private static final long serialVersionUID = -567946702264126881L;


///////////////////////////////////////
   // attributes
   ///////////////////////////////////////
    



  ///////////////////////////////////////
  // operations heritees
   ///////////////////////////////////////

    
    public void chargeSansFiltre() throws Exception {
        ResultSet   rs      = null ;
        
        // Extraction des noms des tables de la base
        String sql = "SELECT relname FROM pg_catalog.pg_statio_user_sequences WHERE relname LIKE 't%_%_%' ;" ;

        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet
            String nom = rs.getString(1) ;
            
            // Ajout a la liste
            this.put(nom, nom) ;
        
        } // end while

    }
    
    
    public void chargeSansFiltreDetails() throws Exception {
 
    }
    
    
    protected void chargeObjet(Object _objet) throws Exception {
  
    }
    
    

    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////    
   

    
    
 } // end ListeStations



