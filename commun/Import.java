/*
 * Import.java
 *
 * Created on 20 juillet 2004, 12:03
 */

package commun;

import java.io.*;
import java.util.zip.DataFormatException;
import java.sql.SQLException;

/**
 *
 * @author  sgaudey
 */
public class Import {

	/*
	 * Attributs
	 */

	File ficSource;

	String tabDestination;

	String separateur;

	String nomSequence;

	Integer nextValSequence;

	/*
	 * Constructeurs
	 */

	/** Creates a new instance of Import */
	public Import() {
		this.setFile((File) null);
		this.setDestination(null);
		this.setSource(null);
		this.setNomSequence(null);
		this.setNextValSequence(null);
	}

	/**
	 * Initialisation d'un import
	 * @param _ficSource le fichier source
	 * @param _tabDestination la table destination
	 * @param _separateur le s�parateur
	 * @param _nomSequence le nom de la s�quence
	 * @param _nextValSequence la prochaine valeur de la s�quence
	 */
	public Import(File _ficSource, String _tabDestination, String _separateur,
			String _nomSequence, Integer _nextValSequence) {
		this.setFile(_ficSource);
		this.setDestination(_tabDestination);
		this.setSource(_separateur);
		this.setNomSequence(_nomSequence);
		this.setNextValSequence(_nextValSequence);
	}

	/**
	 * Insertion de l'import dans la Base de donn�es
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public void insertDonnees() throws SQLException, ClassNotFoundException {
		System.out.println("Import : insertDonnees()");

		ConnexionBD.getInstance().executeCopy(this.getCheminFichierSource(),
				this.tabDestination, this.separateur);

		// Si une sequence est a modifier
		if ((this.nomSequence != null) && (this.nomSequence.length() > 0)
				&& (this.nextValSequence != null)) {
			ConnexionBD.getInstance().executeAlterSequence(this.nomSequence,
					this.nextValSequence.intValue());
		}
	}

	/**
	 * V�rification des attributs
	 * @return vrai si les attributs sont corrects
	 * @throws DataFormatException
	 */
	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;

		if (!Verification.isFile(this.ficSource, true)) {
			throw new DataFormatException(Erreur.S6000);
		}

		if (!Verification.isText(this.tabDestination, true)) {
			throw new DataFormatException(Erreur.S6001);
		}

		if (!Verification.isText(this.separateur, 1, true)) {
			throw new DataFormatException(Erreur.S6002);
		}

		if (!Verification.isText(this.nomSequence, false)) {
			throw new DataFormatException(Erreur.S6002);
		}

		// Si une sequence existe, la prochaine valeur est obligatoire
		boolean obligatoire = false;
		if (this.nomSequence != null) {
			obligatoire = true;
		}
		if (!Verification.isInteger(this.nextValSequence, obligatoire)) {
			throw new DataFormatException(Erreur.S6004);
		}

		return ret;
	}

	/**
	 * Acc�s au fichier source
	 * @return le fichier source
	 */
	public File getFile() {
		return this.ficSource;
	}

	/**
	 * Acc�s au chemin du fichier source
	 * @return le chemin du fichier source
	 */
	public String getCheminFichierSource() {
		return this.ficSource.getAbsolutePath();
	}

	/**
	 * @return � la table destination
	 */
	public String getDestination() {
		return this.tabDestination;
	}

	/**
	 * @return le s�parateur
	 */
	public String getSource() {
		return this.separateur;
	}

	/**
	 * @return le nom de la s�quence
	 */
	public String getNomSequence() {
		return this.nomSequence;
	}

	/**
	 * @return la prochaine valeur de la s�quence
	 */
	public Integer getNextValSequence() {
		return this.nextValSequence;
	}

	/**
	 * @param _ficSource le fichier source
	 */
	public void setFile(File _ficSource) {
		this.ficSource = _ficSource;
	}

	/**
	 * @param _ficSource le fichier source
	 */
	public void setFile(String _ficSource) {
		this.ficSource = new File(_ficSource);
	}

	/**
	 * @param _tabDestination la table destination
	 */
	public void setDestination(String _tabDestination) {
		this.tabDestination = _tabDestination;
	}

	/**
	 * @param _separateur le s�parateur
	 */
	public void setSource(String _separateur) {
		this.separateur = _separateur;
	}

	/**
	 * @param _nomSequence le nom de la s�quence associ�e
	 */
	public void setNomSequence(String _nomSequence) {
		this.nomSequence = _nomSequence;
	}

	/**
	 * @param _nextValSequence la prochaine valeur de la s�quence associ�e
	 */
	public void setNextValSequence(Integer _nextValSequence) {
		this.nextValSequence = _nextValSequence;
	}

}
