/*
 **********************************************************************
 *
 * Nom fichier :        IBaseDonnees.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               Fonctionne
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package commun;

import java.util.zip.DataFormatException;
import java.sql.SQLException;

/** 
 *  Interface utile pour les objets a enregistrer dans la base de donnees. Elle leur permet d'effectuer 
 *  eux-meme les requetes d'ecriture et de modification
 */
/**
 * @author cedric.briand
 *
 */
public interface IBaseDonnees {

	/**
	 * Ajoute l'objet � la base de donn�e en tant que nouvel enregistrement
	 * @throws SQLException Erreur avec la BDD
	 * @throws ClassNotFoundException Class non trouv�e
	 */
	public void insertObjet() throws SQLException, ClassNotFoundException;

	/**
	 * Met a jour l'enregistrement en utilisant les attributs de l'objet courant. L'enregistrement 
	 * doit deja exister dans la base
	 * @throws SQLException Erreur avec la BDD
	 * @throws ClassNotFoundException Class non trouv�e
	 */
	public void majObjet() throws SQLException, ClassNotFoundException;

	/**
	 *  Supprime l'enregistrement qui correspond a l'objet courant. La selection de l'enregistrement 
	 *  a supprimer se base sur l'identifiant.
	 *  @throws SQLException Erreur avec la BDD
	 *  @throws ClassNotFoundException Class non trouv�e
	 */
	public void effaceObjet() throws SQLException, ClassNotFoundException;

	/**
	 * Avant d'inserer ou de mettre a jour un enregistrement dans la base, permet de verifier que 
	 * les attributs sont valides
	 * @return True si tous les attributs sont conformes, False sinon
	 * @throws SQLException Erreur avec la BDD
	 * @throws ClassNotFoundException Class non trouv�e
	 * @throws DataFormatException l'exception generee en cas d'erreur dans un attribut
	 */
	public boolean verifAttributs() throws DataFormatException;

} // end IBaseDonnees

