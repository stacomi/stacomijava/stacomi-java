/*
 **********************************************************************
 *
 * Nom fichier :        Liste.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package commun;

import java.util.Enumeration;
import java.util.Vector;

/**
 * Classe abstraite permettant de charger une liste d'elements de la base de
 * donnees. C'est la liste elle-meme qui effectue les requetes vers la base et
 * qui s'initialise. Une liste sert pour la lecture de donnees et non pas pour
 * l'ecriture dans la base. Les objets qu'elle contient doivent implementer
 * l'interface IBaseDonnees Attention de bien utiliser cette liste : toutes les
 * methodes de la classe Vector sont accessibles mais il est deconseille de les
 * utiliser. Il faut passer par les methodes redefinies ci dessous. En effet, la
 * liste gere en parallele une liste de clefs. Passer directement par les
 * methodes de Vector court-circuite cette liste des clefs.
 */
public abstract class Liste extends Vector<Object> {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	private Vector<Object> keys; // vecteur de chaines de caracteres contenant
	// les clefs associees aux valeurs de this

	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une liste
	 */
	public Liste() {
		super();
		keys = new Vector<Object>();
	} // end liste

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Methode initialisant la liste avec tous les enregistrements d'une table,
	 * sans effectuer de filtre (pas de clause WHERE). Le chargement porte sur
	 * l'identifiant et le libelle seulement, cad le minimun necessaire pour
	 * l'affichage dans une interface graphique.
	 * 
	 * @throws Exception
	 */
	public abstract void chargeSansFiltre() throws Exception;

	/**
	 * Methode initialisant la liste avec tous les enregistrements d'une table.
	 * Les objets ajoutes sont initialises avec tous les attributs, y compris
	 * les sous-listes
	 * 
	 * @throws Exception
	 */
	public abstract void chargeSansFiltreDetails() throws Exception;

	/**
	 * Quand la liste contient des objets, cette methode permet de charger tous
	 * les attributs de ces objets (sauf les attributs de type sous-liste).
	 * 
	 * @throws Exception
	 */
	public void chargeObjets() throws Exception {

		// appelle la methode de chargement pour chacun des objets de la liste
		// e.hasMoreElements() retourne TRUE quand il n'y a plus d'�l�ments
		for (Enumeration e = this.elements(); e.hasMoreElements();) {
			this.chargeObjet((Object) e.nextElement());
		}
	} // end chargeObjets

	/**
	 * Methode a appeler sur un objet de la liste, quand celui-ci a ete
	 * initialise avec son identifiant. Permet de charger tous les autres
	 * attributs de cet objet (sauf les listes)
	 */
	protected abstract void chargeObjet(Object _objet) throws Exception;

	/**
	 * Methode permettant d'ajouter a la liste une valeur
	 * 
	 * @param _key
	 *            la cl�
	 * @param _value
	 *            la valeur
	 */
	public void put(Object _key, Object _value) {
		this.keys.add(_key);
		this.add(_value);
	}

	/**
	 * Methode permettant d'ajouter une valeur en debut de liste
	 * 
	 * @param _key
	 *            la cl�
	 * @param _value
	 *            la valeur
	 */
	public void putFirst(Object _key, Object _value) {
		this.keys.insertElementAt(_key, 0);
		this.insertElementAt(_value, 0);
	}

	/**
	 * Recupere un element du vecteur d'apres une clef
	 * 
	 * @param _key
	 *            la cl�
	 * @return l'objet recherche ou null si l'objet n'a pas ete trouve
	 */
	public Object get(Object _key) {
		Object ret = null;
		if (_key != null) {
			// Recherche la premiere occurence de la clef
			int index = this.keys.indexOf(_key);

			// Si l'objet a ete trouve
			if (index != -1) {
				ret = this.get(index);
			}
		}

		return ret;
	}

	/**
	 * Recupere l'indice de l'objet dans le vecteur d'apr�s la cl�
	 * 
	 * @param _key
	 *            la cl�
	 * @return l'objet recherche ou null si l'objet n'a pas ete trouve
	 * @author cedric.briand
	 */
	public Integer getindice(Object _key) {
		Integer ret = null;
		if (_key != null) {
			// Recherche la premiere occurence de la clef
			int index = this.keys.indexOf(_key);
			ret = index;

		}

		return ret;
	}

	/**
	 * Remplace l'element du vecteur d'apres une clef par un autre element
	 * 
	 * @param _key
	 *            la cl�
	 * @param _newObject
	 *            le nouvel objet
	 */
	public void replace(Object _key, Object _newObject) {

		if (_key != null) {
			// Recherche la premiere occurence de la clef
			int index = this.keys.indexOf(_key);

			// Si l'objet a ete trouve
			if (index != -1) {
				this.setElementAt(_newObject, index);
			}
		}
	}

	/**
	 * Retire l'element du vecteur d'apres sa clef
	 * 
	 * @param _key
	 *            la cl�
	 */
	public void removeObject(Object _key) {

		if (_key != null) {
			// Recherche la premiere occurence de la clef
			int index = this.keys.indexOf(_key);

			// Si l'objet a ete trouve
			if (index != -1) {
				this.keys.remove(index);
				this.remove(index);
			}
		}
	}

	/**
	 * R�cup�ration des cl�s de la liste
	 * 
	 * @author cedric
	 * @return le vecteur de String
	 */
	public Vector<Object> getKeys() {
		return (keys);
	}

	/**
	 * R�cup�ration d'une cl� de la liste
	 * 
	 * @author cedric
	 * @return le vecteur de String
	 */
	public Object getKey(int i) {
		return (keys.elementAt(i));
	}

	/**
	 * V�rifie si une cl� est pr�sente dans la liste Pour v�rifier si une cl� a
	 * d�j� �t� charg�e en base et faire un update au lieu d'un insert
	 * 
	 * @author cedric
	 * @return le vecteur de String
	 */
	public boolean containsKey(Object _key) {
		if (keys.contains(_key)) {
			return (true);
		} else {
			return (false);
		}
	}

	/**
	 * Conversion d'une Liste en vecteur pour pouvoir pr�-remplir les combos
	 * (seulement pour les r�f�rences c'est � dire listes statiques)
	 * 
	 * @param la
	 *            liste a convertir
	 * @return le vecteur de String
	 */
	public static Vector<String> convertToVector(Liste liste) {
		Vector<String> res = new Vector<String>();
		try {
			for (int i = 0; i < liste.size(); i++)
				res.add(((Ref) liste.get(i)).getCode());
		} catch (Exception e) {
			System.out.println(Erreur.I1012);
		}
		return res;

	}

} // end Liste
