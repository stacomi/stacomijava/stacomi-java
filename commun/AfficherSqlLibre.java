/*
 * AfficherTable.java
 *
 * Created on 26 mai 2004, 09:42
 */

package commun;

import java.sql.* ;
import java.io.* ;
import javax.swing.DefaultComboBoxModel ;

/**
 *
 * @author  sgaudey
 */
public class AfficherSqlLibre extends javax.swing.JPanel {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -2520603442808404476L;
	ListeTables lesTables ; 
    TableModelBD tm ;
            
    /** Creates new form ImporterFichier */
    public AfficherSqlLibre() {
        
        this.lesTables = Lanceur.getListeTables() ;
        
        tm = null ;
        
        this.initComponents();
        
        // Pour les copier-coller vers excel
        ExcelAdapter xlsAdapter = new ExcelAdapter(this.contenuTable) ; 
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        top = new javax.swing.JPanel();
        titre = new javax.swing.JLabel();
        resultat = new javax.swing.JLabel();
        center = new javax.swing.JPanel();
        lab_requete = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        requete = new commun.JTextAreaDataType();
        lab_tables = new javax.swing.JLabel();
        table = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        contenuTable = new javax.swing.JTable();
        bottom = new javax.swing.JPanel();
        ok = new javax.swing.JButton();
        exporter = new javax.swing.JButton();
        effacer = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(600, 400));
        top.setLayout(new java.awt.BorderLayout());

        titre.setBackground(new java.awt.Color(0, 51, 153));
        titre.setFont(new java.awt.Font("Dialog", 1, 14));
        titre.setForeground(new java.awt.Color(255, 255, 255));
        titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titre.setText("Afficher/Exporter requ\u00eate SQL");
        titre.setOpaque(true);
        top.add(titre, java.awt.BorderLayout.CENTER);

        resultat.setBackground(new java.awt.Color(255, 255, 255));
        resultat.setForeground(new java.awt.Color(255, 0, 0));
        resultat.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        resultat.setOpaque(true);
        top.add(resultat, java.awt.BorderLayout.SOUTH);

        add(top, java.awt.BorderLayout.NORTH);

        center.setLayout(new java.awt.GridBagLayout());

        center.setBackground(new java.awt.Color(230, 230, 230));
        center.setPreferredSize(new java.awt.Dimension(600, 400));
        lab_requete.setText("Requ\u00eate");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        center.add(lab_requete, gridBagConstraints);

        jScrollPane2.setMinimumSize(new java.awt.Dimension(400, 70));
        jScrollPane2.setPreferredSize(new java.awt.Dimension(400, 70));
        requete.setColumns(35);
        requete.setRows(4);
        jScrollPane2.setViewportView(requete);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        center.add(jScrollPane2, gridBagConstraints);

        lab_tables.setText("Tables");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 20, 3);
        center.add(lab_tables, gridBagConstraints);

        table.setModel(new DefaultComboBoxModel(this.lesTables.toArray()));
        table.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tableActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        center.add(table, gridBagConstraints);

        jScrollPane1.setBackground(new java.awt.Color(230, 230, 230));
        jScrollPane1.setMinimumSize(new java.awt.Dimension(850, 350));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(850, 350));
        contenuTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        contenuTable.setColumnSelectionAllowed(true);
        jScrollPane1.setViewportView(contenuTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        center.add(jScrollPane1, gridBagConstraints);

        add(center, java.awt.BorderLayout.CENTER);

        bottom.setBackground(new java.awt.Color(191, 191, 224));
        ok.setText("Afficher");
        ok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okActionPerformed(evt);
            }
        });

        bottom.add(ok);

        exporter.setText("Exporter .txt");
        exporter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exporterActionPerformed(evt);
            }
        });

        bottom.add(exporter);

        effacer.setText("Annuler");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });

        bottom.add(effacer);

        add(bottom, java.awt.BorderLayout.SOUTH);

    }//GEN-END:initComponents

    private void tableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tableActionPerformed
        try {
            this.requete.insert((String)this.table.getSelectedItem(), this.requete.getCaretPosition()) ;
        }catch (Exception e) {
            this.resultat.setText(Erreur.I1010) ;
        }
    }//GEN-LAST:event_tableActionPerformed

    private void exporterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exporterActionPerformed
        this.exporte() ;
    }//GEN-LAST:event_exporterActionPerformed

    private void okActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okActionPerformed
        this.valide() ;
    }//GEN-LAST:event_okActionPerformed

    
    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        
        this.reInitComponents() ;
  
    }//GEN-LAST:event_effacerActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bottom;
    private javax.swing.JPanel center;
    private javax.swing.JTable contenuTable;
    private javax.swing.JButton effacer;
    private javax.swing.JButton exporter;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lab_requete;
    private javax.swing.JLabel lab_tables;
    private javax.swing.JButton ok;
    private commun.JTextAreaDataType requete;
    private javax.swing.JLabel resultat;
    private javax.swing.JComboBox table;
    private javax.swing.JLabel titre;
    private javax.swing.JPanel top;
    // End of variables declaration//GEN-END:variables
      
    
    
    /*
     * Reinitialise les composants du jPanel courant
     */
    private void reInitComponents() {
        
        this.removeAll() ;
        this.initComponents() ;
        
        // Pour les copier-coller vers excel
        ExcelAdapter xlsAdapter = new ExcelAdapter(this.contenuTable) ; 
        
        this.validate() ;
        this.repaint() ;
        
    }   
    
    
    
    private void valide() {
        
        try {        
            ResultSet   rs      = null ;
            
            String sql = this.requete.getText() ;
        
            // Requete a la base
            rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;
            
            // Affichage du tableau
            this.tm = new TableModelBD(rs) ;
            this.contenuTable.setModel(this.tm) ;
        }
        catch(Exception e) {
            this.resultat.setText(e.getMessage()) ;
        }
    }
    
    
    
    private void exporte() {
        
        try {        
            // Si aucune donnee, erreur
            if (this.tm == null) {
                throw new Exception() ;
            }
            
            try {  
                File ficDest = new File("resRequete.txt") ;
                
                // Flux de sortie
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(ficDest))) ;           

                // boucle sur chaque colonne pour ecriture des en tete
                for (int j = 0; j < this.tm.getColumnCount(); j++) {
                    out.print(this.tm.getColumnName(j) + ";") ;
                }
                out.print("\n") ;
                
                
                // boucle sur chaque ligne
                for (int i = 0; i < this.tm.getRowCount(); i++) {

                    // boucle sur chaque colonne
                    for (int j = 0; j < this.tm.getColumnCount(); j++) {
                        out.print(this.tm.getValueAt(i, j) + ";") ;
                    }
                    out.print("\n") ;

                }
                out.flush() ;
                out.close() ;
                
                this.resultat.setText(Message.E + ficDest.getAbsolutePath()) ;
            }
            catch(Exception e) {
                this.resultat.setText(Erreur.I1009) ;
            }    
        }
        catch(Exception e) {
            this.resultat.setText(Erreur.S6003) ;
        }
    }


}
