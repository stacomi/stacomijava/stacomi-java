/*
 * ConnexionBD.java
 *
 * TRES IMPORTANT : il faut installer le driver PostgreSQL JDBC et modifier le Classpath et le Path
 * Ou sans modifier les var, dans : /usr/local/bin/J2SDK_1.4.2.04/jre/lib/ext/pg74.213.jdbc3.jar
 * http://jdbc.postgresql.org/doc.html
 */

package commun;

import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Classe permettant d'effectuer des requetes vers la base de donnees. La classe
 * est basee sur un design pattern de singleton. L'application utilise la meme
 * connexion tout au long de son execution.
 */
public class ConnexionBD {

	private static ConnexionBD instance; // L'instance statique (singleton)

	private Connection connexion = null;

	private static String _url;

	private static String _user;

	private static String _password;

	private static String _schema;

	// private Statement statement = null ;

	/**
	 * Constructeur prive pour interdire son appel et forcer a passer par la
	 * methode getInstance
	 */
	private ConnexionBD() throws SQLException, ClassNotFoundException {

		XMLParser _parser = new XMLParser();
		_url = _parser.getUrl();
		_user = _parser.getUser();
		_password = _parser.getPassword();

		// Chargement du pilote PostgreSQL
		// Class.forName is a method that finds a class by name. In this case,
		// you look for the Driver.
		// This causes the class loader to search through the CLASSPATH and find
		// a class by that name.
		// If it finds it, the class loader will then read in the binary
		// description of the class.
		// If it does not find it, it will throw a ClassNotFoundException, in
		// which case you can print out an error message to that effect. If you
		// reach this state, you either haven't built the driver correctly, or
		// the .jar file is not in your classpath.

		Class.forName("org.postgresql.Driver");
		// org.postgresql.Driver
		// jdbc.postgresql.Driver
		// postgresql.Driver

		// Ouverture de la connexion
		this.connexion = DriverManager.getConnection(_url, _user, _password);
		// this.statement = connexion.createStatement() ;
	}

	/**
	 * Retourne l'instance unique de la class ConnexionBD
	 * 
	 * @return l'instance unique
	 * @throws SQLException
	 *             Exception lors de l'ouverture de la BDD
	 * @throws ClassNotFoundException
	 *             Classe non trouv�e
	 */
	public static ConnexionBD getInstance() throws SQLException, ClassNotFoundException {
		// Si premier appel, creation d'une instance
		if (instance == null) {
			instance = new ConnexionBD();
		}
		return instance;
	}

	/**
	 * Retourne la connexion a la base
	 * 
	 * @return la connexion
	 */
	public Connection getConnexion() {
		return this.connexion;
	}

	/**
	 * Acc�s au nom du sch�ma r�serv� � l'utilisateur connect�
	 * 
	 * @return le nom du sch�ma r�serv� � l'utilisateur connect�
	 */
	public static final String getSchema() {
		return _schema + ".";
	}

	/**
	 * Par d�faut le statement est forward only Retourne le statement C�dric
	 * modif du type de statement
	 * 
	 * @return le statement
	 * @throws SQLException
	 *             Erreur � la connexion � la BDD
	 */
	public Statement getStatement() throws SQLException {
		return this.connexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
	}

	/**
	 * Retourne le preparestatement C�dric modif du type de statement
	 * 
	 * @return le preparedstatement
	 * @throws SQLException
	 *             Erreur � la connexion � la BDD
	 */
	public PreparedStatement getPreparedStatement(String sql) throws SQLException {
		return this.connexion.prepareStatement(sql);
	}

	/**
	 * Affectation de l'utilisateur qui accede � la base de donn�es
	 * 
	 * @param user
	 *            l'utilisateur
	 * @param password
	 *            le mot de passe de l'utilisateur
	 * @throws SQLException
	 */
	public void setUser(String user, String password) throws SQLException {
		// mise a jour de l'utilisateur
		_user = user;
		_password = password;
		this.connexion = DriverManager.getConnection(_url, user, password);

		// si l'utilisateur s�lectionn� est invit�, on se connecte au sch�ma
		// "All"
		if (_user.equals("invite"))
			_schema = "tout";
		else // sinon on se connecte au sch�ma de l'utilisateur
			_schema = _user;
	}

	/**
	 * Construit une requete SQL d'insertion a partir d'attributs puis l'execute
	 * 
	 * @param _tableDestination
	 *            la table dans laquel l'enregistrement doit etre ajoute
	 * @param _nomAttributs
	 *            un ArrayList de chaines contenant le nom des attributs de la
	 *            table, dans l'ordre d'insertion
	 * @param _valAttributs
	 *            un ArrayList d'objets contenant les objets dans l'ordre
	 *            d'insertion
	 * @throws SQLException
	 *             Erreur avec la BDD
	 */
	public void executeInsert(String _tableDestination, ArrayList _nomAttributs, ArrayList _valAttributs)
			throws SQLException {

		// si l'utilisateur connect� est "invit�",
		// il n'a pas les droits d'ajouter/modifier supprimer des donn�es de la
		// base
		if (!_user.equals("invite")) {
			_valAttributs.add(_user.toUpperCase());

			String sql = ""; // la requete complete
			String sqlNomAttributs = ""; // la partie de requete sql comprenant
			// le nom des attributs de la table
			String sqlValAttributs = ""; // la partie de requete sql comprenant
			// la valeur des attributs

			// Construction de la requete Sql en deux parties : INSERT INTO
			// table (sqlNomAttributs) VALUES (sqlValAttributs) ;

			// Parcourt les vecteurs pour concatener les attributs en une chaine
			// sql. Les attributs 'null' ne sont pas inclus
			for (int i = 0; i < _nomAttributs.size(); i++) {
				if (_valAttributs.get(i) != null) {
					sqlNomAttributs = sqlNomAttributs + (String) _nomAttributs.get(i) + ",";
					sqlValAttributs = sqlValAttributs + "'" + _valAttributs.get(i).toString().replaceAll("'", "`")
							+ "',";
				}
				// Si l'attribut est null, on ne l'inclus pas dans la requete
			}

			// Supression de la derniere virgule
			if (sqlNomAttributs.length() > 0) {
				sqlNomAttributs = sqlNomAttributs.substring(0, sqlNomAttributs.length() - 1);
				sqlValAttributs = sqlValAttributs.substring(0, sqlValAttributs.length() - 1);
			}

			sql = "INSERT INTO " + getSchema() + _tableDestination + "(" + sqlNomAttributs + ") VALUES ("
					+ sqlValAttributs + ") ; ";

			// Execution de la requete
			this.getStatement().execute(sql);
			System.out.println("ConnexionBD : executeInsert()");
		} else
			throw new SQLException(Erreur.B1027);
	}

	/**
	 * Construit une requete SQL d'insertion a partir d'attributs puis
	 * l'execute, m�thode � utiliser pour les tables sans organisme
	 * 
	 * @param _tableDestination
	 *            la table dans laquel l'enregistrement doit etre ajoute
	 * @param _nomAttributs
	 *            un ArrayList de chaines contenant le nom des attributs de la
	 *            table, dans l'ordre d'insertion
	 * @param _valAttributs
	 *            un ArrayList d'objets contenant les objets dans l'ordre
	 *            d'insertion
	 * @throws SQLException
	 *             Erreur avec la BDD
	 */
	public void executeInsertssorg(String _tableDestination, ArrayList _nomAttributs, ArrayList _valAttributs)
			throws SQLException {

		// si l'utilisateur connect� est "invit�",
		// il n'a pas les droits d'ajouter/modifier supprimer des donn�es de la
		// base
		if (!_user.equals("invite")) {

			String sql = ""; // la requete complete
			String sqlNomAttributs = ""; // la partie de requete sql comprenant
			// le nom des attributs de la table
			String sqlValAttributs = ""; // la partie de requete sql comprenant
			// la valeur des attributs

			// Construction de la requete Sql en deux parties : INSERT INTO
			// table (sqlNomAttributs) VALUES (sqlValAttributs) ;

			// Parcourt les vecteurs pour concatener les attributs en une chaine
			// sql. Les attributs 'null' ne sont pas inclus
			for (int i = 0; i < _nomAttributs.size(); i++) {
				if (_valAttributs.get(i) != null) {
					sqlNomAttributs = sqlNomAttributs + (String) _nomAttributs.get(i) + ",";
					sqlValAttributs = sqlValAttributs + "'" + _valAttributs.get(i).toString().replaceAll("'", "`")
							+ "',";
				}
				// Si l'attribut est null, on ne l'inclus pas dans la requete
			}

			// Supression de la derniere virgule
			if (sqlNomAttributs.length() > 0) {
				sqlNomAttributs = sqlNomAttributs.substring(0, sqlNomAttributs.length() - 1);
				sqlValAttributs = sqlValAttributs.substring(0, sqlValAttributs.length() - 1);
			}

			sql = "INSERT INTO " + getSchema() + _tableDestination + "(" + sqlNomAttributs + ") VALUES ("
					+ sqlValAttributs + ") ; ";

			// Execution de la requete
			this.getStatement().execute(sql);
			System.out.println("ConnexionBD : executeInsert()");
		} else
			throw new SQLException(Erreur.B1027);
	}

	/**
	 * Construit une requete SQL d'insertion a partir d'attributs puis
	 * l'execute, m�thode � utiliser pour les arrays, cette methode est cr�ee
	 * pour l'instant sur le mod�le de executessorg (elle agit sur des tables
	 * syst�me sur lesquelles il n'y a pas besoin d'organismes)
	 * 
	 * @param _tableDestination
	 *            la table dans laquel l'enregistrement doit etre ajoute
	 * @param _nomAttributs
	 *            un ArrayList de chaines contenant le nom des attributs de la
	 *            table, dans l'ordre d'insertion
	 * @param _valAttributs
	 *            un ArrayList d'objets contenant les objets dans l'ordre
	 *            d'insertion
	 * @throws SQLException
	 *             Erreur avec la BDD
	 */
	public void executeInsertWithPreparedStatement(String _tableDestination, ArrayList _nomAttributs,
			ArrayList _valAttributs) throws SQLException {

		// si l'utilisateur connect� est "invit�",
		// il n'a pas les droits d'ajouter/modifier supprimer des donn�es de la
		// base
		if (!_user.equals("invite")) {

			String sql = ""; // la requete complete
			String sqlNomAttributs = ""; // la partie de requete sql comprenant
			// le nom des attributs de la table
			String sqlValAttributs = ""; // la partie de requete sql comprenant
			// la valeur des attributs

			// Construction de la requete Sql en deux parties : INSERT INTO
			// table (sqlNomAttributs) VALUES (sqlValAttributs) ;

			// Parcourt les vecteurs pour concatener les attributs en une chaine
			// sql. Les attributs 'null' ne sont pas inclus
			for (int i = 0; i < _nomAttributs.size(); i++) {
				if (_valAttributs.get(i) != null) {
					sqlNomAttributs = sqlNomAttributs + (String) _nomAttributs.get(i) + ",";
					sqlValAttributs = sqlValAttributs + "?,";
				}
				// Si l'attribut est null, on ne l'inclus pas dans la requete
			}

			// Supression de la derniere virgule
			if (sqlNomAttributs.length() > 0) {
				sqlNomAttributs = sqlNomAttributs.substring(0, sqlNomAttributs.length() - 1);
				sqlValAttributs = sqlValAttributs.substring(0, sqlValAttributs.length() - 1);
			}

			sql = "INSERT INTO " + getSchema() + _tableDestination + "(" + sqlNomAttributs + ") VALUES ("
					+ sqlValAttributs + ") ; ";

			// Execution de la requete
			// un prepared statement doit avoir la forme
			// "insert into Table (champ1, champ2) " + "VALUES (?, ?)")
			// puis on fixe les �l�ments du preparedstatement avec
			// pstmt.setString(1, "astring");
			// String [] examplestring = { "string1", "string2", "string3" };
			// pstmt.setArray(2, examplestring);
			// pstmt.executeUpdate();
			// ci-dessous je suis oblig� de tester le type avant l'insertion
			// attention l'array commence � zero et le pstm � 1
			// J'ai essay� mais la m�thode Boolean[] n'existe pas, alors j'ai
			// fait la conversion en String[] dans la m�thode
			// getValeurAttributs() de MasqueOpe.java
			PreparedStatement pstm = this.getPreparedStatement(sql);
			for (int i = 0; i < _valAttributs.size(); i++) {
				if (_valAttributs.get(i) != null) { // sinon pas dans la chaine
					// sql
					if (_valAttributs.get(i) instanceof String[]) {
						Array arraytoinsert = this.connexion.createArrayOf("varchar", (String[]) _valAttributs.get(i));
						pstm.setArray(i + 1, arraytoinsert);
					} else if (_valAttributs.get(i) instanceof Boolean[]) {
						Array arraytoinsert = this.connexion.createArrayOf("boolean", (Boolean[]) _valAttributs.get(i));
						pstm.setArray(i + 1, arraytoinsert);
					} else if (_valAttributs.get(i) instanceof Integer) {
						pstm.setInt(i + 1, (int) _valAttributs.get(i));
					} else if (_valAttributs.get(i) instanceof String) {
						pstm.setString(i + 1, _valAttributs.get(i).toString());
					} else if (_valAttributs.get(i) instanceof Boolean) {
						pstm.setBoolean(i + 1, (boolean) _valAttributs.get(i));
					}
				}
			}
			this.connexion.setAutoCommit(false);
			pstm.executeUpdate();
			this.connexion.commit();
			this.connexion.setAutoCommit(true);
			System.out.println("ConnexionBD : preparedstatement executeInsert()");
		} else
			throw new SQLException(Erreur.B1027);
	}

	/**
	 * Permet de recuperer le dernier identifiant autoincremente d'une sequence.
	 * Solution pas tres satisfaisante, mais statement.execute(sql,
	 * Statement.RETURN_GENERATED_KEYS) n'est pas implemente...
	 * 
	 * @param _nomSequence
	 *            le nom de la sequence
	 * @param _nomSequence
	 *            la table dans laquel l'enregistrement doit etre ajoute
	 * @return la clef autoincrementee
	 * @throws SQLException
	 *             Erreur avec la BDD
	 * 
	 */
	public Integer getGeneratedKey(String _nomSequence) throws SQLException {
		Integer ret = null;

		String sql = "SELECT currval('" + ConnexionBD.getSchema() + _nomSequence + "')";
		ResultSet rs = this.getStatement().executeQuery(sql);
		// S'il y a eut un retour seulement
		if ((rs != null) && (rs.first() == true)) {
			// Lecture du champ dans le ResultSet
			ret = new Integer(rs.getInt(1));
		}

		return ret;
	}

	/**
	 * Permet de recuperer la valeur courante de la s�quence d'une s�quence
	 * identifiant autoincremente d'une sequence. Solution pas tres
	 * satisfaisante...
	 * 
	 * @param _nomSequence
	 *            le nom de la s�quence
	 * @param _nomSequence
	 *            le nom de la s�quence
	 * @return la valeur courante de la clef autoincrementee
	 * @throws SQLException
	 *             Erreur avec la BDD
	 */
	public Integer getCurrentKey(String _nomSequence) throws SQLException {
		Integer ret = null;

		String sql = "SELECT nextval('" + ConnexionBD.getSchema() + _nomSequence + "')";
		ResultSet rs = this.getStatement().executeQuery(sql);
		// S'il y a eut un retour seulement
		if ((rs != null) && (rs.first() == true)) {
			// Lecture du champ dans le ResultSet
			ret = new Integer(rs.getInt(1));

			// sql = "ALTER SEQUENCE " + ConnexionBD.getSchema()+ _nomSequence +
			// " RESTART WITH " + ret;
			// this.getStatement().execute(sql);
		}

		return ret - 1;
	}

	/**
	 * Permet de mettre a jour la sequence dans la base en fonction des donn�es
	 * entr�es dans la table
	 * 
	 * @param _nomSequence
	 *            le nom de la s�quence
	 * @param _nomTable
	 *            le nom de la table
	 * @param _nomColonne
	 *            le nom de la colonne
	 * @param user
	 *            l'utilisateur qui se connecte et mets � jour ses s�quences
	 * @throws SQLException
	 *             Erreur avec la BDD
	 */
	public void updateSequence(String _nomSequence, String _nomTable, String _nomColonne, String user)
			throws SQLException {
		Integer ret = null;

		// r�cup�ration de la valeur max de la s�quence
		String sql = "SELECT max(" + _nomColonne + ") FROM \"" + user + "\"." + _nomTable + ";";
		ResultSet rs = this.getStatement().executeQuery(sql);

		// S'il y a eut un retour seulement
		if ((rs != null) && (rs.first() == true)) {
			// Lecture de la valeur max de la colonne
			ret = rs.getInt("max") + 1;

			// valeur actuelle de la sequence = ret+1
			sql = "ALTER SEQUENCE \"" + user + "\"." + _nomSequence + " RESTART WITH " + ret;
			this.getStatement().execute(sql);
		}
	}

	/**
	 * Construit une requete SQL de mise a jour a partir d'attributs puis
	 * l'execute
	 * 
	 * @param _tableDestination
	 *            la table dans laquel l'enregistrement doit etre modifie
	 * @param _nomAttributsSelection
	 *            un tableau de nom des attribut sur lesquels il faut
	 *            selectionner (WHERE _nomAttributSelection =
	 *            _valAttributSelection)
	 * @param _valAttributsSelection
	 *            un tableau de valeur des l'attribut sur lequel il faut
	 *            selectionner (WHERE _nomAttributSelection =
	 *            _valAttributSelection)
	 * @param _nomAttributs
	 *            un Arraylist de chaines contenant le nom des attributs de la
	 *            table, dans l'ordre de modification
	 * @param _valAttributs
	 *            un ArrayList d'objets contenant les objets dans l'ordre de
	 *            modification
	 * @throws SQLException
	 *             Erreur avec la BDD
	 */
	public void executeUpdate(String _tableDestination, String[] _nomAttributsSelection,
			Object[] _valAttributsSelection, ArrayList _nomAttributs, ArrayList _valAttributs) throws SQLException {

		// si l'utilisateur connect� est "invit�",
		// il n'a pas les droits d'ajouter/modifier supprimer des donn�es de la
		// base
		if (!_user.equals("invite")) {
			_valAttributs.add(_user.toUpperCase());
			String sql = ""; // la requete complete
			String sqlAttributsModifies = ""; // la partie de requete sql
			// comprenant les modifications
			// des champs
			String sqlAttributsSelection = ""; // la partie de requete sql
			// comprenant la selection des
			// enregistrements

			// Construction de la requete Sql en deux parties : UPDATE table SET
			// (a1='v1', a2='v2'...) WHERE (at1='va1' AND at2='va2'...) ;

			// Parcourt les vecteurs pour concatener les attributs a modifier en
			// une chaine sql. Les attributs 'null' sont inclus
			for (int i = 0; i < _nomAttributs.size(); i++) {
				// tient compte de l'attribut seulement si celui ci ne fait pas
				// partie des attributs de selection
				// if
				// (Arrays.asList(_nomAttributsSelection).contains(_nomAttributs.get(i)))
				// {
				if (_valAttributs.get(i) != null)
					sqlAttributsModifies = sqlAttributsModifies + (String) _nomAttributs.get(i) + "='"
							+ _valAttributs.get(i).toString().replaceAll("'", "`") + "',";
				else
					// Pour le null, traitement separe car il ne faut pas de
					// guillemet
					sqlAttributsModifies = sqlAttributsModifies + (String) _nomAttributs.get(i) + "=NULL,";
				// }
			}

			// Supression de la derniere virgule
			if (sqlAttributsModifies.length() > 0)
				sqlAttributsModifies = sqlAttributsModifies.substring(0, sqlAttributsModifies.length() - 1);

			// Parcourt les vecteurs pour concatener les attributs de selection
			// en une chaine sql. Les attributs 'null' sont inclus
			for (int i = 0; i < _nomAttributsSelection.length; i++) {
				if (_valAttributsSelection[i] != null)
					sqlAttributsSelection = sqlAttributsSelection + _nomAttributsSelection[i] + "='"
							+ _valAttributsSelection[i].toString() + "' AND ";
				// Pour le null, traitement separe car pas la meme syntaxe (pas
				// de '=' mais un 'IS')
				else
					sqlAttributsSelection = sqlAttributsSelection + _nomAttributsSelection[i] + " IS NULL AND ";
			}

			// Supression du dernier 'AND '
			if (sqlAttributsSelection.length() > 0)
				sqlAttributsSelection = sqlAttributsSelection.substring(0, sqlAttributsSelection.length() - 4);

			sql = "UPDATE " + getSchema() + _tableDestination + " SET " + sqlAttributsModifies + " WHERE "
					+ sqlAttributsSelection + " ; ";

			// Execution de la requete
			this.getStatement().execute(sql);
			System.out.println("ConnexionBD : executeUpdate()");
		} else
			throw new SQLException(Erreur.B1027);
	}

	/**
	 * Construit une requete SQL de mise a jour a partir d'attributs puis
	 * l'execute, mais pour les table sans colonne organismes (tables systeme)
	 * 
	 * @param _tableDestination
	 *            la table dans laquel l'enregistrement doit etre modifie
	 * @param _nomAttributsSelection
	 *            un tableau de nom des attribut sur lesquels il faut
	 *            selectionner (WHERE _nomAttributSelection =
	 *            _valAttributSelection)
	 * @param _valAttributsSelection
	 *            un tableau de valeur des l'attribut sur lequel il faut
	 *            selectionner (WHERE _nomAttributSelection =
	 *            _valAttributSelection)
	 * @param _nomAttributs
	 *            un Arraylist de chaines contenant le nom des attributs de la
	 *            table, dans l'ordre de modification
	 * @param _valAttributs
	 *            un ArrayList d'objets contenant les objets dans l'ordre de
	 *            modification
	 * @throws SQLException
	 *             Erreur avec la BDD
	 */
	public void executeUpdatessorg(String _tableDestination, String[] _nomAttributsSelection,
			Object[] _valAttributsSelection, ArrayList _nomAttributs, ArrayList _valAttributs) throws SQLException {

		// si l'utilisateur connect� est "invit�",
		// il n'a pas les droits d'ajouter/modifier supprimer des donn�es de la
		// base
		if (!_user.equals("invite")) {
			// _valAttributs.add(_user.toUpperCase());
			String sql = ""; // la requete complete
			String sqlAttributsModifies = ""; // la partie de requete sql
			// comprenant les modifications
			// des champs
			String sqlAttributsSelection = ""; // la partie de requete sql
			// comprenant la selection des
			// enregistrements

			// Construction de la requete Sql en deux parties : UPDATE table SET
			// (a1='v1', a2='v2'...) WHERE (at1='va1' AND at2='va2'...) ;

			// Parcourt les vecteurs pour concatener les attributs a modifier en
			// une chaine sql. Les attributs 'null' sont inclus
			for (int i = 0; i < _nomAttributs.size(); i++) {
				// tient compte de l'attribut seulement si celui ci ne fait pas
				// partie des attributs de selection
				// if
				// (Arrays.asList(_nomAttributsSelection).contains(_nomAttributs.get(i)))
				// {
				if (_valAttributs.get(i) != null)
					sqlAttributsModifies = sqlAttributsModifies + (String) _nomAttributs.get(i) + "='"
							+ _valAttributs.get(i).toString().replaceAll("'", "`") + "',";
				else
					// Pour le null, traitement separe car il ne faut pas de
					// guillemet
					sqlAttributsModifies = sqlAttributsModifies + (String) _nomAttributs.get(i) + "=NULL,";
				// }
			}

			// Supression de la derniere virgule
			if (sqlAttributsModifies.length() > 0)
				sqlAttributsModifies = sqlAttributsModifies.substring(0, sqlAttributsModifies.length() - 1);

			// Parcourt les vecteurs pour concatener les attributs de selection
			// en une chaine sql. Les attributs 'null' sont inclus
			for (int i = 0; i < _nomAttributsSelection.length; i++) {
				if (_valAttributsSelection[i] != null)
					sqlAttributsSelection = sqlAttributsSelection + _nomAttributsSelection[i] + "='"
							+ _valAttributsSelection[i].toString() + "' AND ";
				// Pour le null, traitement separe car pas la meme syntaxe (pas
				// de '=' mais un 'IS')
				else
					sqlAttributsSelection = sqlAttributsSelection + _nomAttributsSelection[i] + " IS NULL AND ";
			}

			// Supression du dernier 'AND '
			if (sqlAttributsSelection.length() > 0)
				sqlAttributsSelection = sqlAttributsSelection.substring(0, sqlAttributsSelection.length() - 4);

			sql = "UPDATE " + getSchema() + _tableDestination + " SET " + sqlAttributsModifies + " WHERE "
					+ sqlAttributsSelection + " ; ";

			// Execution de la requete
			this.getStatement().execute(sql);
			System.out.println("ConnexionBD : executeUpdate()");
		} else
			throw new SQLException(Erreur.B1027);
	}

	/**
	 * Construit une requete SQL de mise a jour a partir d'attributs puis
	 * l'execute, mais pour les table sans colonne organismes (tables systeme)
	 * 
	 * @param _tableDestination
	 *            la table dans laquel l'enregistrement doit etre modifie
	 * @param _nomAttributsSelection
	 *            un tableau de nom des attribut sur lesquels il faut
	 *            selectionner (WHERE _nomAttributSelection =
	 *            _valAttributSelection)
	 * @param _valAttributsSelection
	 *            un tableau de valeur des l'attribut sur lequel il faut
	 *            selectionner (WHERE _nomAttributSelection =
	 *            _valAttributSelection)
	 * @param _nomAttributs
	 *            un Arraylist de chaines contenant le nom des attributs de la
	 *            table, dans l'ordre de modification
	 * @param _valAttributs
	 *            un ArrayList d'objets contenant les objets dans l'ordre de
	 *            modification
	 * @throws SQLException
	 *             Erreur avec la BDD
	 */
	public void executeUpdatesWithPreparedstatement(String _tableDestination, String[] _nomAttributsSelection,
			Object[] _valAttributsSelection, ArrayList _nomAttributs, ArrayList _valAttributs) throws SQLException {

		// si l'utilisateur connect� est "invit�",
		// il n'a pas les droits d'ajouter/modifier supprimer des donn�es de la
		// base
		if (!_user.equals("invite")) {
			// _valAttributs.add(_user.toUpperCase());
			String sql = ""; // la requete complete
			String sqlAttributsModifies = ""; // la partie de requete sql
			// comprenant les modifications
			// des champs
			String sqlAttributsSelection = ""; // la partie de requete sql
			// comprenant la selection des
			// enregistrements

			// Construction de la requete Sql en deux parties : UPDATE table SET
			// (a1='v1', a2='v2'...) WHERE (at1='va1' AND at2='va2'...) ;

			// Parcourt les vecteurs pour concatener les attributs a modifier en
			// une chaine sql. Les attributs 'null' sont inclus
			for (int i = 0; i < _nomAttributs.size(); i++) {
				// tient compte de l'attribut seulement si celui ci ne fait pas
				// partie des attributs de selection
				// if
				// (Arrays.asList(_nomAttributsSelection).contains(_nomAttributs.get(i)))
				// {
				if (_valAttributs.get(i) != null)
					sqlAttributsModifies = sqlAttributsModifies + (String) _nomAttributs.get(i) + "=" + "?" + ",";
				else
					// Pour le null, traitement separe car il ne faut pas de
					// guillemet
					sqlAttributsModifies = sqlAttributsModifies + (String) _nomAttributs.get(i) + "=NULL,";
				// }
			}

			// Supression de la derniere virgule
			if (sqlAttributsModifies.length() > 0)
				sqlAttributsModifies = sqlAttributsModifies.substring(0, sqlAttributsModifies.length() - 1);

			// Parcourt les vecteurs pour concatener les attributs de selection
			// en une chaine sql. Les attributs 'null' sont inclus
			for (int i = 0; i < _nomAttributsSelection.length; i++) {
				if (_valAttributsSelection[i] != null)

					sqlAttributsSelection = sqlAttributsSelection + _nomAttributsSelection[i] + "='"
							+ _valAttributsSelection[i].toString() + "' AND ";

				// Pour le null, traitement separe car pas la meme syntaxe (pas
				// de '=' mais un 'IS')
				else
					sqlAttributsSelection = sqlAttributsSelection + _nomAttributsSelection[i] + " IS NULL AND ";
			}

			// Supression du dernier 'AND '
			if (sqlAttributsSelection.length() > 0)
				sqlAttributsSelection = sqlAttributsSelection.substring(0, sqlAttributsSelection.length() - 4);

			sql = "UPDATE " + getSchema() + _tableDestination + " SET " + sqlAttributsModifies + " WHERE "
					+ sqlAttributsSelection + " ; ";

			// voir executeInsertWithPreparedStatement pour l'aide

			PreparedStatement pstm = this.getPreparedStatement(sql);
			for (int i = 0; i < _valAttributs.size(); i++) {
				if (_valAttributs.get(i) != null) { // sinon pas dans la chaine
					// sql
					if (_valAttributs.get(i) instanceof String[]) {
						Array arraytoinsert = this.connexion.createArrayOf("varchar", (String[]) _valAttributs.get(i));
						pstm.setArray(i + 1, arraytoinsert);
					} else if (_valAttributs.get(i) instanceof Boolean[]) {
						Array arraytoinsert = this.connexion.createArrayOf("boolean", (Boolean[]) _valAttributs.get(i));
						pstm.setArray(i + 1, arraytoinsert);
					} else if (_valAttributs.get(i) instanceof Integer) {
						pstm.setInt(i + 1, (int) _valAttributs.get(i));
					} else if (_valAttributs.get(i) instanceof String) {
						pstm.setString(i + 1, _valAttributs.get(i).toString());
					} else if (_valAttributs.get(i) instanceof Boolean) {
						pstm.setBoolean(i + 1, (boolean) _valAttributs.get(i));
					}
				}
			}
			this.connexion.setAutoCommit(false);
			pstm.executeUpdate();
			this.connexion.commit();
			this.connexion.setAutoCommit(true);
			System.out.println("ConnexionBD : preparedstatement executeUpdate()");
		} else
			throw new SQLException(Erreur.B1027);
	}

	/**
	 * Construit une requete SQL de suppression a partir d'attributs puis
	 * l'execute
	 * 
	 * @param _tableDestination
	 *            la table dans laquel l'enregistrement doit etre supprime
	 * @param _nomAttributsSelection
	 *            un tableau de nom des attribut sur lesquels il faut
	 *            selectionner (WHERE _nomAttributSelection =
	 *            _valAttributSelection)
	 * @param _valAttributsSelection
	 *            un tableau de valeur des l'attribut sur lequel il faut
	 *            selectionner (WHERE _nomAttributSelection =
	 *            _valAttributSelection)
	 * @throws SQLException
	 *             Erreur avec la BDD
	 */
	public void executeDelete(String _tableDestination, String[] _nomAttributsSelection,
			Object[] _valAttributsSelection) throws SQLException {

		// si l'utilisateur connect� est "invit�",
		// il n'a pas les droits d'ajouter/modifier supprimer des donn�es de la
		// base
		if (!_user.equals("invite")) {
			String sql = ""; // la requete complete
			String sqlAttributsSelection = ""; // la partie de requete sql
			// comprenant la selection des
			// enregistrements

			// Construction de la requete Sql : DELETE FROM table WHERE
			// (at1='va1' AND at2='va2'...) ;

			// Parcourt les vecteurs pour concatener les attributs de selection
			// en une chaine sql. Les attributs 'null' sont inclus
			for (int i = 0; i < _nomAttributsSelection.length; i++) {
				if (_valAttributsSelection[i] != null)
					sqlAttributsSelection = sqlAttributsSelection + _nomAttributsSelection[i] + "='"
							+ _valAttributsSelection[i].toString() + "' AND ";
				else
					// Pour le null, traitement separe car pas la meme syntaxe
					// (pas de '=' mais un 'IS')
					sqlAttributsSelection = sqlAttributsSelection + _nomAttributsSelection[i] + " IS NULL AND ";
			}

			// Supression du dernier 'AND '
			if (sqlAttributsSelection.length() > 0)
				sqlAttributsSelection = sqlAttributsSelection.substring(0, sqlAttributsSelection.length() - 4);

			sql = "DELETE FROM " + getSchema() + _tableDestination + " WHERE " + sqlAttributsSelection + " ; ";

			// Execution de la requete
			this.getStatement().execute(sql);

			System.out.println("ConnexionBD : executeDelete()");
		} else
			throw new SQLException(Erreur.B1027);
	}

	/**
	 * Construit une requete SQL d'insertion a partir d'un fichier, puis
	 * l'execute
	 * 
	 * @param _fichierSource
	 *            le chemin absolu du fichier contenant les donnees a importer
	 * @param _tableDestination
	 *            la table destination
	 * @param _tableDestination
	 *            la table dans laquel les enregistrements doivent etre ajoutes
	 * @param _separateur
	 *            lengthseparateur de champs utilise dans le fichier source
	 * @throws SQLException
	 *             Erreur avec la BDD
	 */
	public void executeCopy(String _fichierSource, String _tableDestination, String _separateur) throws SQLException {

		// si l'utilisateur connect� est "invit�",
		// il n'a pas les droits d'ajouter/modifier supprimer des donn�es de la
		// base
		if (!_user.equals("invite")) {
			String sql;
			_fichierSource = _fichierSource.replace("\\", "/");
			sql = "COPY " + _tableDestination + " FROM '" + _fichierSource + "' WITH DELIMITER AS'" + _separateur
					+ "' NULL AS ''" + " CSV HEADER ;";
			this.getStatement().execute(sql);
		} else
			throw new SQLException(Erreur.B1027);
	}

	/**
	 * Construit une requete SQL d'insertion a partir d'un fichier, puis
	 * l'execute
	 * 
	 * @param _nomSequence
	 *            le nom de la s�quence
	 * @param _nomSequence
	 *            la sequence a mettre a jour
	 * @param _prochaineValeur
	 *            la prochaine valeur utilisee pour l'auto incrementation
	 * @throws SQLException
	 *             Erreur avec la BDD
	 */
	public void executeAlterSequence(String _nomSequence, int _prochaineValeur) throws SQLException {
		String sql;
		sql = "ALTER SEQUENCE " + _nomSequence + " RESTART WITH " + _prochaineValeur + " ;";
		this.getStatement().execute(sql);
		// System.out.println("ConnexionBD : executeAlterSequence()") ;
	}

	/**
	 * Fonction n�cessaire pour passer Boolean a ArrayList
	 * 
	 * @param valueArray
	 *            un vecteur de boolean
	 * @return
	 */
	private ArrayList<Boolean> castToObjectType(Boolean[] valueArray) {
		ArrayList<Boolean> objArray = new ArrayList<Boolean>();

		for (int i = 0; i < valueArray.length; i++) {
			objArray.add(new Boolean(valueArray[i]));
		}

		return objArray;
	}
} // end ConnexionBD
