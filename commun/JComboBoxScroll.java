/*
 * JComboBoxScroll.java
 *
 * Created on 2 juillet 2004, 15:07
 */

package commun;



import javax.swing.* ;
import javax.swing.plaf.basic.*;

/**
 * Combo avec ascenceur
 * @author sgaudey
 */
public class JComboBoxScroll extends JComboBox{
    /**
	 * 
	 */
	private static final long serialVersionUID = -794371457699551673L;

	/**
	 * Construction d'un JComboBoxScroll
	 */
	public JComboBoxScroll(){
        super();
        setUI(new myComboUI());
    }//end of default constructor
    
    /**
     * @author sgaudey
     */
    public class myComboUI extends BasicComboBoxUI{
        protected ComboPopup createPopup(){
            BasicComboPopup popup = new BasicComboPopup(comboBox){
                /**
				 * 
				 */
				private static final long serialVersionUID = 1282023698024388546L;

				protected JScrollPane createScroller() {
                        return new JScrollPane( list, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED );
              }//end of method createScroller
            };
            return popup;
        }//end of method createPopup
    }//end of inner class myComboUI
}