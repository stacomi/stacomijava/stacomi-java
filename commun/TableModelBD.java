/*
 * TableModelBD.java
 *
 * Created on 20 juillet 2004, 14:27
 */

package commun;

import java.sql.*;
import javax.swing.table.*;
import java.text.SimpleDateFormat;

/**
 * Classe utile pour afficher un resultset dans un jTable
 * @author  sgaudey
 */
public class TableModelBD extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4752784444318510831L;

	//private static Object[][] data;
	//private static Object[] colname;
	private ResultSet rs;

	private SimpleDateFormat simpleDate;

	/** 
	 * Creates a new instance of TableModelBD 
	 * @param _rs le resultSet 
	 */
	public TableModelBD(ResultSet _rs) {

		simpleDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		this.rs = _rs;
	}

	public int getColumnCount() {
		int i = 0;
		try {
			i = this.rs.getMetaData().getColumnCount();
		} catch (SQLException e) {
			System.out.println(e);
		}
		return i;
	}

	public int getRowCount() {
		int i = 0;
		try {
			this.rs.last();
			i = rs.getRow();
		} catch (SQLException e) {
			System.out.println(e);
		}
		return i;
	}

	public String getColumnName(int c) {
		String s = "";
		try {
			s = this.rs.getMetaData().getColumnName(c + 1);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return s;
	}

	public Object getValueAt(int row, int column) {
		Object o = "";
		try {
			this.rs.absolute(row + 1);
			o = rs.getObject(column + 1);
			try {
				//System.out.println(o.getClass());
				if ((o != null)
						&& (o.getClass() == Class.forName("java.sql.Timestamp"))) {
					o = simpleDate.format(o);
				}
			} catch (Exception e) {
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
		return o;
	}

	/*
	 public Class getColumnClass(int columnIndex) {
	 Object o = getValueAt(0, columnIndex);
	 if (o == null) {
	 return null ;
	 } else {
	 return o.getClass();
	 }
	 }*/

}
