package commun;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;

public class DualListBox extends JPanel {

	private static final Insets EMPTY_INSETS = new Insets(0, 0, 0, 0);

	private static final String ADD_BUTTON_LABEL = "Ajouter >>";

	private static final String REMOVE_BUTTON_LABEL = "<< Retirer";

	private static final String DEFAULT_SOURCE_CHOICE_LABEL = "Choix dispo";

	private static final String DEFAULT_DEST_CHOICE_LABEL = "Vos choix";

	private JLabel sourceLabel;

	private JList sourceList;

	private DefaultListModel sourceListModel;

	private JList destList;

	private DefaultListModel destListModel;

	private JLabel destLabel;

	private JButton addButton;

	private JButton removeButton;

	public DualListBox() {
		initScreen();
	}

	public String getSourceChoicesTitle() {
		return sourceLabel.getText();
	}

	public void setSourceChoicesTitle(String newValue) {
		sourceLabel.setText(newValue);
	}

	public String getDestinationChoicesTitle() {
		return destLabel.getText();
	}

	public void setDestinationChoicesTitle(String newValue) {
		destLabel.setText(newValue);
	}

	public void clearSourceListModel() {
		sourceListModel.clear();
	}

	public void clearDestinationListModel() {
		destListModel.clear();
	}

	//
	/**
	 * permet de remplir les �l�ments source � partir d'un Listmodel
	 * 
	 * @param newValue
	 *            un ListModel
	 */
	public void addSourceElements(ListModel newValue) {
		int size = newValue.getSize();
		for (int i = 0; i < size; i++) {
			sourceListModel.addElement(newValue.getElementAt(i));
		}
		sourceList.setModel(sourceListModel);
	}

	/**
	 * permet de remplir les �l�ments source � partir d'une Liste note la
	 * m�thode getSelected qui renvoyait un array est deprecated
	 * 
	 * @param newValue
	 *            la list, r�cup�r�e par la m�thode getSelectedValuesList()
	 * 
	 */
	public void addSourceElements(List newValue) {
		int size = newValue.size();
		for (int i = 0; i < size; i++) {
			sourceListModel.addElement(newValue.get(i));
		}
		sourceList.setModel(sourceListModel);
	}

	/**
	 * remplit de combo de destination � partir de la liste r�cup�r�e par
	 * getSelectedValuesList() dans les �l�ments source
	 * 
	 * @param selected
	 *            une liste
	 */
	public void addDestinationElements(List selected) {
		int siz = selected.size();
		// on passe par DefaultListModel pour remplir la liste
		for (int i = 0; i < siz; i++) {
			destListModel.addElement(selected.get(i));
		}
		destList.setModel(destListModel);
	}

	/**
	 * remplit de combo de destination � partir de la liste r�cup�r�e par
	 * getSelectedValuesList() dans les �l�ments source
	 * 
	 * @param selected
	 *            un ListModel
	 */
	public void addDestinationElements(ListModel selected) {
		int siz = selected.getSize();
		// on passe par DefaultListModel pour remplir la liste
		for (int i = 0; i < siz; i++) {
			destListModel.addElement(selected.getElementAt(i));
		}
		destList.setModel(destListModel);
	}

	/**
	 * permet de faire passer des �l�ments de la JList dans les �l�ments
	 * s�lectionn�s
	 * 
	 * @param leselements
	 */
	public void setDestinationElements(ListModel leselements) {
		addDestinationElements(leselements);
		clearSource(leselements);
	}

	/**
	 * r�cup�re un array avec les �l�ments de la liste de destination
	 * 
	 */
	public Object[] getDestinationElements() {
		Object[] arraydestlist = destListModel.toArray();
		return (arraydestlist);
	}

	public void setSourceCellRenderer(ListCellRenderer<Object> newValue) {
		sourceList.setCellRenderer(newValue);
	}

	public ListCellRenderer getSourceCellRenderer() {
		return sourceList.getCellRenderer();
	}

	public void setDestinationCellRenderer(ListCellRenderer<Object> newValue) {
		destList.setCellRenderer(newValue);
	}

	public ListCellRenderer getDestinationCellRenderer() {
		return destList.getCellRenderer();
	}

	public void setVisibleRowCount(int newValue) {
		sourceList.setVisibleRowCount(newValue);
		destList.setVisibleRowCount(newValue);
	}

	public int getVisibleRowCount() {
		return sourceList.getVisibleRowCount();
	}

	public void setSelectionBackground(Color newValue) {
		sourceList.setSelectionBackground(newValue);
		destList.setSelectionBackground(newValue);
	}

	public Color getSelectionBackground() {
		return sourceList.getSelectionBackground();
	}

	public void setSelectionForeground(Color newValue) {
		sourceList.setSelectionForeground(newValue);
		destList.setSelectionForeground(newValue);
	}

	public Color getSelectionForeground() {
		return sourceList.getSelectionForeground();
	}

	private void clearSourceSelected() {
		List selected = sourceList.getSelectedValuesList();
		for (int i = selected.size() - 1; i >= 0; --i) {
			sourceListModel.removeElement(selected.get(i));
		}
		sourceList.getSelectionModel().clearSelection();
	}

	private void clearSource(ListModel selected) {
		for (int i = selected.getSize() - 1; i >= 0; --i) {
			sourceListModel.removeElement(selected.getElementAt(i));
		}
		sourceList.getSelectionModel().clearSelection();
	}

	private void clearDestinationSelected() {
		List selected = destList.getSelectedValuesList();
		for (int i = selected.size() - 1; i >= 0; --i) {
			destListModel.removeElement(selected.get(i));
		}
		destList.getSelectionModel().clearSelection();
	}

	private void initScreen() {
		setBorder(BorderFactory.createEtchedBorder());
		setLayout(new GridBagLayout());
		sourceLabel = new JLabel(DEFAULT_SOURCE_CHOICE_LABEL);
		sourceListModel = new DefaultListModel();
		sourceList = new JList(sourceListModel);
		add(sourceLabel, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				EMPTY_INSETS, 0, 0));
		add(new JScrollPane(sourceList), new GridBagConstraints(0, 1, 1, 5, .5, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, EMPTY_INSETS, 0, 0));

		addButton = new JButton(ADD_BUTTON_LABEL);
		add(addButton, new GridBagConstraints(1, 2, 1, 2, 0, .25, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				EMPTY_INSETS, 0, 0));
		addButton.addActionListener(new AddListener());
		removeButton = new JButton(REMOVE_BUTTON_LABEL);
		add(removeButton, new GridBagConstraints(1, 4, 1, 2, 0, .25, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 5, 0, 5), 0, 0));
		removeButton.addActionListener(new RemoveListener());

		destLabel = new JLabel(DEFAULT_DEST_CHOICE_LABEL);
		destListModel = new DefaultListModel();
		destList = new JList(destListModel);
		add(destLabel, new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				EMPTY_INSETS, 0, 0));
		add(new JScrollPane(destList), new GridBagConstraints(2, 1, 1, 5, .5, 1.0, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, EMPTY_INSETS, 0, 0));
	}

	private class AddListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			// r�cup�ration des �l�ments de la sourcelist dans une liste
			List selected = sourceList.getSelectedValuesList();
			addDestinationElements(selected);
			clearSourceSelected();
		}
	}

	private class RemoveListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			List selected = destList.getSelectedValuesList();
			addSourceElements(selected);
			clearDestinationSelected();
		}
	}

	/**
	 * @return the destListModel
	 */
	public DefaultListModel getDestListModel() {
		return destListModel;
	}

}
