/*
 **********************************************************************
 *
 * Nom fichier :        Erreur.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package commun;

/**
 * Classe statique regroupant les erreurs sous forme textuelle, destinees a
 * l'utilisateur
 */
public final class Erreur {

	///////////////////////////////////////
	// erreurs de saisie
	///////////////////////////////////////

	public static String S = "Erreur de saisie : ";
	public static String S0100 = S + "champ mal form\u00e9 (S0100) : ";

	public static String S1000 = S + "saisir code (S1000)";
	public static String S1001 = S + "saisir libell\u00e9 (S1001)";
	public static String S1002 = S + "saisir mn\u00e9monique (S1002)";
	public static String S1003 = S + "saisir niveau taxonomique (S1003)";
	public static String S1004 = S + "saisir type de localisation : MARQUE | PATHOLOGIE (S1004)";
	public static String S1005 = S + "saisir nature du parametre : BIOLOGIQUE | ENVIRONNEMENTAL (S1005)";
	public static String S1006 = S + "saisir parametre qualitatif pour la valeur (S1006)";
	public static String S1007 = S + "saisir rang de la valeur du parametre qualitatif (S1007)";
	public static String S1008 = S + "saisir le dispositif de contr�le (S1008)";
	public static String S1009 = S + "saisir la distance (S1009)";
	public static String S1010 = S + "saisir la valeur du parametre associ� � la distance (S1010)";

	// Station
	public static String S2000 = S + "saisir code (S2000)";
	public static String S2001 = S + "saisir nom (S2001)";
	public static String S2002 = S + "saisir localisation (S2002)";
	public static String S2003 = S
			+ "saisir ou  v\u00e9rifier coordonn\u00e9e x, Lambert 93 min -40 000 max 1.4 million (S2003)";
	public static String S2004 = S
			+ "saisir ou v\u00e9rifier coordonn\u00e9e y, Lambert 93 min 6 million, 7.1 million (S2004)";
	public static String S2005 = S + "saisir altitude (S2005)";
	public static String S2006 = S + "saisir carte de localisation (S2006)";
	public static String S2007 = S + "saisir superficie (S2007)";
	public static String S2008 = S + "saisir distance a la mer (S2008)";
	public static String S2009 = S + "saisir date de creation (S2009)";
	public static String S2010 = S + "saisir date de suppression (S2010)";
	public static String S2011 = S + "saisir commentaires (S2011)";
	public static String S2012 = S + "saisir dernier import des conditions environnementales (S2012)";

	// Operation
	public static String S3000 = S + "saisir DC (S3000)";
	public static String S3001 = S + "saisir date de d\u00e9but (S3001)";
	public static String S3002 = S + "saisir heure de d\u00e9but (S3002)";
	public static String S3003 = S + "saisir date de fin (S3003)";
	public static String S3004 = S + "saisir heure de fin (S3004)";
	public static String S3005 = S + "saisir organisme (S3005)";
	public static String S3006 = S + "saisir operateur (S3006)";
	public static String S3007 = S + "saisir commentaires (S3007)";
	public static String S3008 = S + "saisir date/heure de d\u00e9but (S3008)";
	public static String S3009 = S + "saisir date/heure de fin (S3009)";
	public static String S3010 = S + "saisir identifiant (S3010)";
	public static String S3011 = S + "Probl�me de date de fin < date debut";
	public static String S3012 = S + "s\u00e9lectionner op\u00e9ration (S3012)";

	// Lot
	public static String S4000 = S + "saisir identifiant (S4000)";
	public static String S4001 = S + "saisir op\u00e9ration (S4001)";
	public static String S4002 = S + "saisir taxon (S4002)";
	public static String S4003 = S + "saisir stade de d\u00e9veloppement(S4003)";
	public static String S4004 = S + "saisir effectif et/ou quantit\u00e9 (S4004)";
	public static String S4005 = S + "saisir m\u00e9thode d'obtention (S4005)";
	public static String S4006 = S + "saisir lot parent (S4006)";
	public static String S4007 = S + "saisir devenir (S4007)";
	public static String S4008 = S + "saisir commentaires (S4008)";
	public static String S4009 = S + "choisir lot de rattachement (S4009)";

	// Caracteristique de lot
	public static String S5000 = S + "s\u00e9lectionner lot de rattachement (S5000)";
	public static String S5001 = S + "saisir param\u00e9tre (S5001)";
	public static String S5002 = S + "saisir m\u00e9thode d'obtention (S5002)";
	public static String S5003 = S + "saisir valeur (S5003)";
	public static String S5004 = S + "saisir pr\u00e9cision de la valeur (S5004)";
	public static String S5005 = S + "saisir commentaires (S5005)";

	// Import et export de fichier
	public static String S6000 = S + "saisir fichier source (S6000)";
	public static String S6001 = S + "saisir table destination (S6001)";
	public static String S6002 = S + "saisir s\u00e9parateur de champs (S6002)";
	public static String S6003 = S + "saisir table � exporter (S6003)";
	public static String S6004 = S + "saisir prochaine valeur (S6004)";

	// Bilans
	public static String S7000 = S + "saisir station de contr\u00f4le (S7000)";
	public static String S7001 = S + "saisir taxon(s) (S7001)";
	public static String S7002 = S + "saisir stade(s) de d\u00e9veloppement (S7002)";
	public static String S7003 = S + "saisir param\u00eatre(s) (S7003)";
	public static String S7004 = S + "saisir date de d\u00e9but et de fin (S7004)";
	public static String S7005 = S + "saisir dispositif de comptage (S7005)";
	public static String S7006 = S + "saisir ouvrage (S7006)";
	public static String S7007 = S + "saisir taxon (S7007)";
	public static String S7008 = S + "saisir stade de d\u00e9veloppement (S7008)";
	public static String S7009 = S + "saisir type de quantit\u00e9 (S7009)";
	public static String S7010 = S + "saisir date de d\u00e9but (S7010)";
	public static String S7011 = S + "saisir date et pas de temps (S7011)";

	// Autres erreurs
	public static String S8000 = S
			+ "L'op\u00e9ration commence apr\u00e8s le d\u00e9but de la p\u00e9riode demand\u00e9e (S8000)";
	public static String S8001 = S
			+ "L'op\u00e9ration se termine avant la fin de la p\u00e9riode demand\u00e9e (S8001)";
	public static String S8002 = S + "Il manque une op\u00e9ration avant (S8002). P\u00e9riode sans op\u00e9ration : ";
	public static String S8003 = S + "Il n'y a aucune op\u00e9ration dans cette p\u00e9riode (S8003)";
	// Ouvrage
	public static String S9000 = S + "saisir identifiant (entier) (S9000)";
	public static String S9001 = S + "choisir une station (S9001)";
	public static String S9002 = S + "saisir code ouvrage (<5 caract\u00e8res) (S9002)";
	public static String S9003 = S + "saisir libelle (<40 caract\u00e8res) (S9003)";
	public static String S9004 = S + "saisir localisation (S9004)";
	public static String S9005 = S + "Erreur coordonn\u00e9e x (entre -40 000 et 130000) (S9005)";
	public static String S9006 = S + "Erreur coordonn\u00e9e y (entre 6 000 000 et 7 100 000 (S9006)";
	public static String S9007 = S + "saisir altitude (S9007)";
	public static String S9008 = S + "saisir carte de localisation (S9008)";
	public static String S9010 = S + "saisir commentaires (S9010)";
	public static String S9011 = S + "choisir nature de l'ouvrage (S9011)";

	// DF
	// normalement idenfifiant autoincr�ment� => la premi�re erreur n'est pas
	// renvoy�e
	public static String S10000 = S + "l'identifiant autoincr�ment� n'est pas (entier) (S10000)";
	public static String S10001 = S + "choisir un ouvrage (S10001)";
	public static String S10002 = S + "saisir code df (5 caract\u00e8res) (S10002)";
	public static String S10003 = S + "saisir localisation (pas de contrainte) (S10003)";
	public static String S10004 = S + "Pour l'orientation deux choix possibles (MONTEE ou  DESCENTE) (S10004)";
	public static String S10005 = S + "saisir date creation ou date supression post�rieure � la date de cr\u00e10ation";
	public static String S10006 = S + "saisir commentaires";// normalement pas
															// de commentaire
															// obligatoire
	public static String S10007 = S + "Choisir un taxon de destination ";
	public static String S10008 = S + "Choisir un type de dispostif";
	public static String S10010 = S + "saisir commentaires (S10010)";
	public static String S10011 = S + "saisir nature de l'ouvrage (S10011)";
	public static String S10012 = S + "Le rang du disposif doit �tre un entier>1 (erreur interne S10012)";
	public static String S10013 = S + "Le type de taxons cible secondaire du DF doit �tre diff�rent du taxon  (10013)";
	public static String S10014 = S + "choisir un DF (S10001)";
	public static String S10015 = S + "choisir un dispositif (S10015)";

	// DC
	public static String S11000 = S + "saisir code dc (5 caract\u00e8res) (S11000)";
	public static String S11001 = S + "saisir DC (S11001)";
	public static String S11002 = S + "code du DF de rattachement incorrect (S11002)";
	public static String S11003 = S + "Il faut d'abord saisir les DF dans la base (S11003)";
	// ImporterVideo
	public static String S12002 = S + "Erreur lors du calcul des champs issus de l'export video (S12002)";
	public static String S12003 = S + "Saisir l'organisme (S12003)";
	public static String S12004 = S + "Saisir l'op\u00e9rateur (S12004)";
	public static String S12005 = S
			+ "Erreur de l'\u00e9valuation du commentaire des op\u00e9rations vid\u00e9o (S12005)";
	public static String S12006 = S + "Erreur l'op�ration de rattachement est nulle (S12006)";
	public static String S12007 = S + "Erreur lors de la cr�ation du taxon (S12007)";
	public static String S12008 = S + "Erreur date de d�but de l'op�ration (S12008)";
	public static String S12009 = S + "Erreur date de fin de l'op�ration (S12009)";
	public static String S12010 = S + "Erreur lors de l'enregistement final des op\u00e9rations (S12011)";
	public static String S12011 = S + "Echec de la suppression des op�rations video (S12011)";
	public static String S12012 = S + "Echec de la suppression des lots (S12012)";
	public static String S12013 = S + "Echec de la suppression des caract�ristiques de lots (S12013)";
	public static String S12014 = S + "Echec lors de la cr�ation du stade du taxon (S12014)";
	public static String S12015 = S + "Pas de tailleVideo associ�e (S12015)";

	// A/M/S p�riode de fonctionnement
	public static String S13000 = S + "Erreur de saisie date de d�but de p�riode (S13000)";
	public static String S13001 = S + "Erreur de saisie date de fin de p�riode (S13001)";
	public static String S13002 = S + "Type de fonctionnement incorrect (S13002)";
	public static String S13003 = S + "Type d'arr�t incorrect (S13003)";
	public static String S13004 = S + "Periode de Fonctionnement (S13004)";
	public static String S13005 = S + "Dispositif incorrect (S13005)";
	public static String S13006 = S + "Erreur de l'\u00e9valuation du commentaire (S13006)";
	public static String S13007 = S + "saisir date d�but ou date fin post�rieure � la date de d�but (S13007)";
	public static String S13008 = S + "Commentaires incorrects (S13008)";
	public static String S13009 = S + "M�thode d'obtention incorrecte (S13009)";

	// Ajouter/Modifier/Supprimer Pathologie
	public static String S14000 = S + "Selectionnez une pathologie � supprimer (S14000)";
	public static String S14001 = S + "Identifiant du lot incorrect (S14001)";
	public static String S14002 = S + "Code de la pathologie constat�e incorrect (S14002)";
	public static String S14003 = S + "Code de la localisatio de la pathologie constat�e incorrect (S14003)";
	public static String S14004 = S + "Commentaire sur la pathologie constat�e incorrect (S14004)";
	public static String S14005 = S + "Code d'importance de la pathologie incorrect (S14005)";
	// A/M/S ConditionEnvironnementale
	public static String S15000 = S + "identifiant de la mesure de la station ind�fini (S15000)";
	public static String S15001 = S + "saisir parametre (<8 caract\u00e8res) (S15001)";
	public static String S15002 = S + "commentaire incorrect (S15002)";
	public static String S15003 = S + "station incorrecte (S15003)";
	public static String S15004 = "ERREUR : selectionnez une condition environnementale (S15004)";
	public static String S15005 = S + "date de d�but sup�rieure � la date de fin (S15005)";
	public static String S15006 = S + "valeur du parametre quantitatif ou qualitatif incorrect (S15006)";
	public static String S15007 = S + "methode d'obtention incorrecte (MESURE ou CALCULE) (S15007)";
	public static String S15008 = S + "date de d�but de condition environnementale (S15008)";
	public static String S15009 = S + "date de fin de condition environnementale (S15009)";
	public static String S15010 = S + "libell� station de mesure (<12 caract\u00e8res) (S15010)";
	public static String S15011 = S + "libell� de la condition environnementale (S15011)";
	public static String S15012 = S
			+ "selectionnez une mesure. Pour en ajouter une, aller dans  Infrastructure/Station/\"A/M/S une station mesure\" (S15012)";
	public static String S15013 = S + "date de d�but incorrecte (S15013)";
	public static String S15014 = S + "date de fin incorrecte (S15014)";
	public static String S15015 = S + "m�thode d'obtention incorrecte (MESURE ou CALCULE)(S15015)";
	public static String S15016 = S + "valeur du parametre quantitatif incorrecte (S15016)";

	// OperationMarquage
	public static String S16000 = S + "identifiant de l'op�ration de marquage incorrect (S16000)";
	public static String S16001 = S + "commentaire de l'op�ration de marquage incorrect (S16001)";

	// Marquage
	public static String S17000 = S + "identifiant du lot de rattachement du marquage incorrect (S17000)";
	public static String S17001 = S + "r�f�rence du marquage incorrecte (S17001)";
	public static String S17002 = S + "action du marquage = 'lecture', 'retrait' ou 'pose' (S17002)";
	public static String S17003 = S + "commentaires du marquage incorrectes (S17003)";

	// Marque
	public static String S18000 = S + "r�f�rence de la marque incorrecte (<30 caract\u00e8res) (S18000)";
	public static String S18001 = S + "code de la localisation de la marque incorrect (<4 caract\u00e8res)(S18001)";
	public static String S18002 = S + "code de la nature de la marque incorrect (<4 caract\u00e8res) (S18002)";
	public static String S18003 = S
			+ "r�f�rence de l'op�. de marquage de la marque incorrect (<30 caract\u00e8res) (S18003)";
	public static String S18004 = S + "commentaire de la marque incorrect (S18004)";

	// A/M/S Action de marquage & marque
	public static String S19000 = S + "action incorrecte (S19000)";
	public static String S19001 = S + "commentaire sur le marquage incorrect (S19001)";
	public static String S19002 = S + "r�f�rence de la marque incorrecte (S19002)";
	public static String S19003 = S + "commentaire sur la marque incorrect (S19003)";
	public static String S19005 = S + "Selectionnez une marque � supprimer (S19005)";
	public static String S19006 = S + "Selectionnez un lot (S19006)";

	// A/M/S Op�ration de marquage
	public static String S20000 = S + "r�f�rence de l'op�ration de marquage incorrecte (S20000)";
	public static String S20001 = S + "commentaire sur l'op�ration de marquage incorrect (S20001)";
	public static String S20002 = S + "selectionnez une op�ration � supprimer (S20002)";

	// A/M/S Taux echappement
	public static String S21000 = S + "selectionnez un taux d'�chappement dans la liste (S21000)";
	public static String S21001 = S + "ouvrage incorrect (S21001)";
	public static String S21002 = S + "taxon incorrect (S21002)";
	public static String S21003 = S + "stade incorrect (S21003)";
	public static String S21004 = S + "date de d�but incorrecte (S21004)";
	public static String S21005 = S + "date de fin incorrecte (S21005)";
	public static String S21006 = S + "m�thode d'estimation (S21006)";
	public static String S21007 = S + "valeur de l'�chappement incorrecte (S21007)";
	public static String S21008 = S + "niveau d'�chappement incorrect (S21008)";
	public static String S21009 = S + "commentaire incorrect (S21009)";

	// A/M/S StationMesure
	public static String S22000 = S + "Station incorrecte (S22000)";
	public static String S22001 = S + "libell� incorrect (S22001)";
	public static String S22002 = S + "Param�tre incorrect (S22002)";
	public static String S22003 = S + "Description incorrecte (S22003)";
	public static String S22004 = S + "Selectionnez une mesure (S22004)";

	// A/M/S un pr�levement
	public static String S23000 = S + "Selectionnez un pr�l�vement (S23000)";
	public static String S23001 = S + "Type de pr�l�vement incorrect (S23001)";
	public static String S23002 = S + "Identifiant du lot incorrect (S23002)";
	public static String S23003 = S + "Code du pr�l�vement incorrect (S23003)";
	public static String S23004 = S + "Op�rateur incorrect (S23004)";
	public static String S23005 = S + "Localisation incorrecte (S23005)";
	public static String S23006 = S + "Commentaires sur le pr�l�vement incorrects (S23006)";
	public static String S23007 = S + "Pr�l�vement incorrect (S23007)";
	public static String S23008 = S + "Selectionnez un lot (S23008)";
	public static String S23009 = S + "Code du pr�l�vement incorrect (S23009)";
	public static String S23010 = S + "Localisation de pr�l�vement incorrecte (S23010)";
	public static String S23011 = S + "Commentaires sur le pr�l�vement incorrects (S23011)";
	public static String S23012 = S + "Op�rateur du pr�l�vement incorrects (S23012)";

	// RefPrelevement
	public static String S24000 = S + "Type de pr�l�vement incorrect (<15 caract\u00e8res) (S24000)";
	public static String S24001 = S + "D�finition du pr�levement incorrecte (S24001)";

	// import de CE
	public static String S25000 = S + "La station de mesure n'existe pas (S25000) ";

	// A/M/S une marque
	public static String S26000 = S + "Veuillez selectionner une marque (S26000)";

	// Masque
	public static String S27000 = S + "L'identifiant du masque doit �tre un entier";
	public static String S27001 = S + "Le code du masque doit avoir une longueur inf�rieure � 10 caract�res";
	public static String S27002 = S + "Le type de masque ne peut �tre que lot ou ope";

	// A/M/S un masque
	public static String S28000 = S + "Veuillez selectionner un masque (S28000)";
	public static String S28001 = S + "Probl�me de vectorisation de la liste des masques (S28001)";

	///////////////////////////////////////
	// erreur dans les parametres
	///////////////////////////////////////

	public static String I = "Erreur interne : ";
	public static String I1000 = I + "identifiant de l'objet non definit (I1000)";
	public static String I1001 = I + "un et un seul tuple attendu (I1001)";
	public static String I1002 = I + "l'affichage de la station, ouvrage, df ou dc a \u00e9chou\u00e9 (I1002)";
	public static String I1003 = I + "l'affichage est impossible (I1003)";
	public static String I1004 = I + "l'incr\u00e9mentation de l'op\u00e9ration a \u00e9chou\u00e9 (I1004)";
	public static String I1005 = I + "l'affichage du lot a \u00e9chou\u00e9 (I1005)";
	public static String I1006 = I + "l'affichage de l'op\u00e9ration a \u00e9chou\u00e9 (I1006)";
	public static String I1007 = I + "l'affichage de la caract\u00e9ristique a \u00e9chou\u00e9 (I1007)";
	public static String I1008 = I + "l'initialisation des param\u00e9tres a \u00e9chou\u00e9 (I1008)";
	public static String I1009 = I + "l'export du fichier a \u00e9chou\u00e9 (I1009)";
	public static String I1010 = I + "l'affichage de la table a \u00e9chou\u00e9 (I1010)";
	public static String I1011 = I + "la recherche de la sequence a \u00e9chou\u00e9  (I1011)";
	public static String I1012 = I
			+ "la methode converttovector est destinee aux listes referentielles uniquement  (I1012)";
	public static String I1013 = I + "probleme de conversion depuis liste vers SortedListModel  (I1013)";
	public static String I1014 = I + "probleme de taille des vecteurs v�rifi�es avant insertion des masques (I1014)";

	///////////////////////////////////////
	// erreur avec la BD
	///////////////////////////////////////
	public static String B = "Erreur BD : ";
	public static String B1000 = B + "l'ouverture de la connexion a \u00e9chou\u00e9 (B1000)";
	public static String B1001 = B + "la fermeture de la connexion a \u00e9chou\u00e9 (B1001)";
	public static String B1002 = B + "le chargement d'une liste de r\u00e9f\u00e9rences a \u00e9chou\u00e9 (B1002)";
	public static String B1003 = B + "le chargement des stations a \u00e9chou\u00e9 (B1003)";
	public static String B1004 = B + "le chargement des ouvrages de la station a \u00e9chou\u00e9 (B1004)";
	public static String B1005 = B + "le chargement des df de l'ouvrage a \u00e9chou\u00e9 (B1005)";
	public static String B1006 = B + "le chargement des dc a \u00e9chou\u00e9 (B1006)";
	public static String B1007 = B + "le chargement de la derni\u00e9re op\u00e9ration a \u00e9chou\u00e9 (B1007)";
	public static String B1008 = B + "le chargement des op\u00e9rations a \u00e9chou\u00e9 (B1008)";
	public static String B1009 = B + "le chargement de l'op\u00e9ration a \u00e9chou\u00e9 (B1009)";
	public static String B1010 = B + "le chargement des param\u00e9tres a \u00e9chou\u00e9 (B1010)";
	public static String B1011 = B + "le chargement des valeurs du param\u00e9tre a \u00e9chou\u00e9 (B1011)";
	public static String B1012 = B + "le chargement de la table a \u00e9chou\u00e9 (B1012)";
	public static String B1013 = B + "le chargement des taxons a \u00e9chou\u00e9 (B1013)";
	public static String B1014 = B + "le chargement des stades a \u00e9chou\u00e9 (B1014)";
	public static String B1015 = B + "le chargement des caracteristiques de l'ouvrage a \u00e9chou\u00e9  (B1015)";
	public static String B1016 = B + "le chargement du df a \u00e9chou\u00e9 (B1016)";
	public static String B1017 = B + "le chargement du dC a \u00e9chou\u00e9 (B1017)";
	public static String B1018 = B + "le chargement des types de DF a \u00e9chou\u00e9 (B1018)";
	public static String B1019 = B + "le chargement des dispositifs a \u00e9chou\u00e9 (B1019)";
	public static String B1020 = B + "le chargement de la station de rattachement a \u00e9chou\u00e9 (B1020)";
	public static String B1021 = B + "le chargement de l'ouvrage de rattachement a \u00e9chou\u00e9 (B1021)";
	public static String B1022 = B
			+ "le chargement des p�riodes de fonctionnement du dispositif a \u00e9chou\u00e9 (B1022)";
	public static String B1023 = B
			+ "renseignez la signification de ce taxon vid�o (dans R�f�rentiel>Ajouter/Modif/Supp Taxon vid�o) (B1023)";
	public static String B1024 = B + "le taxon vid�o n'existe pas dans la base (B1024)";
	public static String B1025 = B + "echec de l'ecriture, suppression des donn�es (B1025)";
	public static String B1026 = B + "Erreur � la r�cup�ration de la station de mesure. (Existe-t-elle ?) (B1026)";
	public static String B1027 = B
			+ "Le compte 'invite' ne permet pas d'ajouter/modifier/supprimer des donn�es (B1027)";
	public static String B1028 = B + "L'op�ration de rattachement est nulle (B1028)";

	///////////////////////////////////////
	// erreur avec le scan d'un fichier texte
	///////////////////////////////////////
	public static String V = "Erreur de scan de fichier : ";
	public static String V1000 = V + "le fichier n'est pas trouv\u00e9 (V1000)";
	public static String V1001 = V + "La valeur attendue est une chaine de caracteres (V1001)";
	public static String V1002 = V + "La valeur attendue est un entier (V1002)";
	public static String V1003 = V + "La valeur attendue est un nombre (long) (V1003)";
	public static String V1008 = V + "La valeur attendue est une date (au format jj/mm/aaaa hh:MM:SS) (V1008)";

	// Taxonvideo et taillevideo
	public static String V1004 = V + "Taxon Video erreur de format (V1004)";
	public static String V1005 = V + "R�f�rence du taxon erreur de format (V1005)";
	public static String V1006 = V + "Distance � la vitre I P ou L erreur de format (V1006)";
	public static String V1007 = V + "Coefficient de converstion erreur de format (V1007)";

	// importerVideo

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	/**
	 * Affiche le detail d'une exception sur la sortie standard
	 * 
	 * @param _ex
	 *            l'exception a afficher
	 * @param _complement
	 *            un message complementaire pour preciser l'erreur
	 */
	public static void print(Exception _ex, String _complement) {
		System.out.println(_complement);
		System.out.println(_ex.toString());
		_ex.printStackTrace(System.out);
	}

	/**
	 * Retourne le detail d'une exception
	 * 
	 * @param _ex
	 *            l'exception a afficher
	 * @param _complement
	 *            un message complementaire pour preciser l'erreur
	 */
	public static String getErreur(Exception _ex, String _complement) {
		String ret = "";
		ret = ret + _complement + "\n";
		ret = _ex.toString();
		// _ex.printStackTrace(System.out) ;
		return ret;
	}

} // end Erreur
