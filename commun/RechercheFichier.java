/*
 * OuvreFichier.java
 *
 * Created on 4 mars 2008, 09:16
 * @author cedric briand cedric.briand@lavilaine.com
 */

package commun ;
import java.io.File;

/**
 *
 * @author  cedric
 */
public class RechercheFichier extends javax.swing.JPanel {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** Creates new form RechercheFichier */
	private File file;  //  @jve:decl-index=0:
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser choixFichier;
    
	/**
	 * Constructeur : initialise 1 RechercheFichier � partir des extensions possibles 
	 * @param extensions : les extensions possibles de fichiers attendus
	 */
    public RechercheFichier(String[] extensions) {
        choixFichier = new javax.swing.JFileChooser();
        ExampleFileFilter filter = new ExampleFileFilter();
        
        String description = "";
        for(int i=0 ; i<extensions.length ; i++)
        {
        	filter.addExtension(extensions[i]);
        	if(i!=extensions.length-1)
        		description += extensions[i] + " & ";
        	else
        		description += extensions[i];
        }
        
        filter.setDescription(description);
        choixFichier.setFileFilter(filter);
        int returnVal = choixFichier.showOpenDialog(choixFichier);
        if(returnVal == javax.swing.JFileChooser.APPROVE_OPTION) {
           System.out.println("Choix du fichier: " +
               choixFichier.getSelectedFile().getName());
           file=choixFichier.getSelectedFile();
        }
    }
    
    // End of variables declaration//GEN-END:variables
	/**
	 * Acc�s au fichier
	 * @return le fichier
	 */
	public File getFile() {
		return file;
	}
	
	
	/**
	 * Affecte le fichier
	 * @param file : le fichier
	 */
	public void setFile(File file) {
		this.file = file;
	}
    
}
