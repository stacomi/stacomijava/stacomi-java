/*
 **********************************************************************
 *
 * Nom fichier :        JTextFieldDataType.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package commun;

import javax.swing.JTextField ;
import java.util.zip.DataFormatException ;
import java.util.Date ;
import java.sql.Time ;
import java.text.SimpleDateFormat ;
import java.text.ParsePosition ;



/**
 * 
 */
public class JTextFieldDataType extends JTextField {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3439389030674975790L;


	/**
     * Retourne le contenu du champ sous forme de String en supprimant les espaces avant et apres
     * @return le String null si le champ est vide
	 * @throws Exception 
     */
    public String getString() throws Exception {
        String ret = null ;
        
        // Si le champ n'est pas vide, essaie de le parser
        if (super.getText().trim().length() > 0) {
                ret = super.getText().trim() ; 
        }
        
        return ret ;
    }
    
    
    /**
     * Retourne le contenu du champ sous forme d'Integer
     * @return l'Integer parse ou null si le champ est vide
     * @throws Exception 
     */
    public Integer getInteger() throws Exception {
        Integer ret = null ;
        
        // Si le champ n'est pas vide, essaie de le parser
        if (super.getText().trim().length() > 0) {
                ret = new Integer(super.getText().trim()) ; 
        }
        
        return ret ;
    }
    
    
    /**
     * Retourne le contenu du champ sous forme de Short
     * @return le Short parse ou null si le champ est vide
     * @throws Exception 
     */
    public Short getShort() throws Exception {
        Short ret = null ;
        
        // Si le champ n'est pas vide, essaie de le parser
        if (super.getText().trim().length() > 0) {
                ret = new Short(super.getText().trim()) ; 
        }
        
        return ret ;
    }
    
    
    /**
     * Retourne le contenu du champ sous forme de Float
     * @return le Float parse ou null si le champ est vide
     * @throws Exception 
     */
    public Float getFloat() throws Exception {
        Float ret = null ;
        
        // Si le champ n'est pas vide, essaie de le parser
        if (super.getText().trim().length() > 0) {
                ret = new Float(super.getText().trim()) ; 
        }
        
        return ret ;
    }    
    
    
    /**
     * Retourne le contenu du champ sous forme de Date
     * @param _simpleDate le format de la date
     * @return la Date parsee ou null si le champ est vide
     * @throws Exception 
     */
    public Date getDate(SimpleDateFormat _simpleDate) throws Exception {
        Date ret = null ;
        
        // Si le champ n'est pas vide, essaie de le parser
        if (super.getText().trim().length() > 0) {
            
                // le parsing avec _simpleDate est un peu lache par defaut
                // pour le contraindre un peu plus (empeche la saisie d'une annee AA au lieu de AAAA) :
            /*
                 if ((super.getText().trim().length()) != (_simpleDate.toPattern().length())) {
                    throw new DataFormatException("Invalid date format") ;
                }
             */
                 
                ret = _simpleDate.parse(super.getText().trim(), new ParsePosition(0)) ;
                
                // Si le parsing a echoue, le retour est null
                if (ret == null) {;
                    throw new DataFormatException("Invalid date format") ;
                }
        }     
        
        return ret ;
    }    
    
    
    /**
     * Retourne le contenu du champ (au format 'hh:mm:ss') sous forme de Time
     * @return l'heure parsee ou null si le champ est vide
     * @throws Exception 
     */
    public Time getTime() throws Exception {
        Time ret = null ;
        
        // Si le champ n'est pas vide, essaie de le parser
        if (super.getText().trim().length() > 0) {

                ret = Time.valueOf(super.getText().trim()) ;
                
                // Si le parsing a echoue, le retour est null
                if (ret == null) {
                    throw new DataFormatException("Invalid time format") ;
                }
        }     
        
        return ret ;
    }    
}
