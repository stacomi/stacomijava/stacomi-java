/**
 * 
 */
package commun;

import java.awt.Color;
import java.awt.Component;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;

import migration.Caracteristique;
import migration.Lot;
import migration.Marquage;
import migration.PathologieConstatee;

/**
 * @author cedric.briand
 *
 */
public class LotTreeCellRenderer extends DefaultTreeCellRenderer implements TreeCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4558555593373309286L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.tree.TreeCellRenderer#getTreeCellRendererComponent(javax.
	 * swing.JTree, java.lang.Object, boolean, boolean, boolean, int, boolean)
	 */
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
			boolean leaf, int row, boolean hasFocus) {

		super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

		// Des chaines de caract�res sont stock�es dans le noeud p�re
		// (op�ration)
		Object node = (Object) ((DefaultMutableTreeNode) value).getUserObject();
		String codeimage;
		if (node instanceof String) {
			String operation_du_noeud = (String) ((DefaultMutableTreeNode) value).getUserObject();
			URL imageURL = getClass().getResource("/images/" + "operation" + ".png");
			setIcon(new ImageIcon(imageURL));
		}
		if (node instanceof Lot) {
			Lot lot_du_noeud = (Lot) ((DefaultMutableTreeNode) value).getUserObject();
			String taxon = lot_du_noeud.getTaxon().getCode();
			String stade = lot_du_noeud.getStadeDeveloppement().getCode();
			switch (taxon) {
			case "2038":
				codeimage = "anguille";
				break;
			case "2014":
				codeimage = "lamproie";
				break;
			case "2055":
				codeimage = "alose";
				break;
			case "2056":
				codeimage = "alose";
				break;
			case "2057":
				codeimage = "alose";
				break;
			case "2058":
				codeimage = "alose";
				break;
			case "2096":
				codeimage = "barbeau_2096";
				break;
			case "2108":
				codeimage = "carpe";
				break;
			case "2109":
				codeimage = "carpe";
				break;
			case "2137":
				codeimage = "tanche";
				break;
			case "2151":
				codeimage = "brochet";
				break;
			case "2181":
				codeimage = "mulet";
				break;
			case "2183":
				codeimage = "mulet";
				break;
			case "2195":
				codeimage = "sandre";
				break;
			case "2219":
				codeimage = "saumon";
				break;
			case "2220":
				codeimage = "saumon";
				break;
			case "2221":
				codeimage = "truite";
				break;
			case "2234":
				codeimage = "bar";
				break;
			case "2238":
				codeimage = "silure";
				break;
			case "2224":
				codeimage = "truite_mer";
				break;
			case "2105":
				codeimage = "2105_toxostome_Parachondrostoma_toxostoma";
				break;
			case "2133":
				codeimage = "2133_gardon_Rutilus_rutilus";
				break;
			case "2120":
				codeimage = "2120_Chevaine_Leuciscus_cephalus";
				break;
			default:
				codeimage = "inconnu";
				break;
			}
			if (stade.equals("CIV")) {
				codeimage = "civelle";
			}
			if (stade.equals("AGG")) {
				codeimage = "anguille_argentee";
			}
			if (stade.equals("PRS") | stade.equals("POS") | stade.equals("SML")) {
				codeimage = "smolt";
			}
			URL imageURL = getClass().getResource("/images/" + codeimage + ".png");
			if (imageURL == null) {
				Icon icon = getClosedIcon();
				setIcon(icon);
			} else {
				setIcon(new ImageIcon(imageURL));
			}
			if (lot_du_noeud.getLotParent() == null) {
				setForeground(new Color(96, 36, 35));

			} else {
				setForeground(new Color(194, 111, 98));
			}
		}
		if (node instanceof Caracteristique) {
			Caracteristique car = (Caracteristique) ((DefaultMutableTreeNode) value).getUserObject();
			URL imageURL = getClass().getResource("/images/" + "scale" + ".png");
			setIcon(new ImageIcon(imageURL));
			if (car.getParametre().getCode().equals("1786") | car.getParametre().getCode().equals("1785")) {
				// taille
				setForeground(new Color(17, 0, 187));
			} else if (car.getParametre().getCode().equals("1783")) {
				// sexe
				setForeground(new Color(0, 120, 246));
			} else if (car.getParametre().getCode().equals("A111")) {
				// poids
				setForeground(new Color(34, 119, 170));
			} else {
				setForeground(new Color(0, 51, 238));
			}
		}
		if (node instanceof Marquage) {
			Marquage marquage = (Marquage) ((DefaultMutableTreeNode) value).getUserObject();
			URL imageURL = getClass().getResource("/images/" + "marquage" + ".png");
			setForeground(new Color(25, 161, 95));
			setIcon(new ImageIcon(imageURL));
		}

		if (node instanceof PathologieConstatee) {
			PathologieConstatee patho = (PathologieConstatee) ((DefaultMutableTreeNode) value).getUserObject();
			URL imageURL = getClass().getResource("/images/" + "pathologie" + ".png");
			setForeground(new Color(224, 53, 132));
			setIcon(new ImageIcon(imageURL));
		}

		return this;
	}

}
