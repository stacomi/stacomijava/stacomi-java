/*
 **********************************************************************
 *
 * Nom fichier :        Ref.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package commun;

import java.sql.SQLException;
import java.util.zip.DataFormatException;

/**
 * Classe abstraite permettant l'utilisation de references. En general, une
 * reference est un couple code-libelle dont le code est l'identifiant.
 * 
 */
public abstract class Ref implements IBaseDonnees {

	///////////////////////////////////////
	// attributes
	///////////////////////////////////////

	protected String code; // code du tuple
	protected String libelle; // libelle du tuple
	protected String statut; // le statut du tuple
	///////////////////////////////////////
	// constructors
	///////////////////////////////////////

	/**
	 * Construit une reference avec un code mais sans libelle
	 * 
	 * @param _code
	 *            le code
	 */
	public Ref(String _code) {
		this(_code, null);
	} // end ref

	/**
	 * Construit une reference avec un code et un libelle
	 * 
	 * @param _code
	 *            le code
	 * @param _libelle
	 *            le libelle
	 */
	public Ref(String _code, String _libelle) {
		this.setCode(_code);
		this.setLibelle(_libelle);
	} // end ref

	/**
	 * Construit une reference avec un code, un libelle et un statut (
	 * "gel� ou NULL")
	 * 
	 * @param _code
	 *            le code
	 * @param _libelle
	 *            le libelle
	 * @param _statut
	 *            le statut
	 */
	public Ref(String _code, String _libelle, String _statut) {
		this.setCode(_code);
		this.setLibelle(_libelle);
		this.setStatut(_statut);
	} // end ref

	///////////////////////////////////////
	// interfaces
	///////////////////////////////////////

	public abstract void insertObjet() throws SQLException, ClassNotFoundException;

	public abstract void majObjet() throws SQLException, ClassNotFoundException;

	public void effaceObjet() throws java.sql.SQLException, ClassNotFoundException {

		// A FAIRE !!!!!!!!!!!!!!!
	}

	public boolean verifAttributs() throws DataFormatException {
		boolean ret = false;

		// Verification de presence du code
		if ((this.code == null) || (this.code == "")) {
			throw new DataFormatException(Erreur.S1000);
		}
		// Verification de presence du libelle
		else if ((this.libelle == null) || (this.libelle == "")) {
			throw new DataFormatException(Erreur.S1001);
		}
		// Si tout est bon
		else {
			ret = true;
		}

		return ret;
	} // end verifAttributs

	///////////////////////////////////////
	// operations
	///////////////////////////////////////

	// Inutile maintenant que chargeObjet() n'est plus dans IBaseDonnees
	/**
	 * Methode a appeler depuis chargeObjet() dans une sous classe de Ref dans
	 * le cas ou la sous classe ne définit pas d'autres attributs que code et
	 * libelle
	 * 
	 * @param _table
	 *            la table a interroger
	 */
	/*
	 * protected void chargeObjet(String _table) { String sql ; sql =
	 * "SELECT libelle FROM " + _table + " WHERE code = " + this.code + " ;" ;
	 * 
	 * // executer requete // this.setLibelle(...) ;
	 * 
	 * } // end chargeObjet
	 */

	/**
	 * Methode a appeler depuis insertObjet() dans une sous classe de Ref dans
	 * le cas ou la sous classe ne d�finit pas d'autres attributs que code et
	 * libelle
	 * 
	 * @param _table
	 *            la table de destination
	 */
	protected void insertObjet(String _table) {

		// executer requete

	} // end insertObjet

	/**
	 * Methode a appeler depuis majObjet() dans une sous classe de Ref dans le
	 * cas ou la sous classe ne d�finit pas d'autres attributs que code et
	 * libelle
	 * 
	 * @param _table
	 *            la table de destination
	 */
	protected void majObjet(String _table) {

		// executer requete

	} // end majObjet

	// Inutile maintenant que chargeObjet() n'est plus dans IBaseDonnees
	/**
	 * Does ...
	 * 
	 * 
	 * @param _code
	 *            le code
	 * @param _libelle
	 *            le libelle
	 */
	/*
	 * public abstract void chargeFiltre(String _code, String _libelle) ;
	 */

	/**
	 * Retourne le code de l'enregistrement
	 * 
	 * @return le code
	 */
	public String getCode() {
		return this.code;
	} // end getCode

	/**
	 * Retourne le libelle de l'enregistrement
	 * 
	 * @return le libelle
	 */
	public String getLibelle() {
		return this.libelle;
	} // end getLibelle

	/**
	 * Initialise le code de l'enregistrement
	 * 
	 * @param _code
	 *            le code
	 */
	public void setCode(String _code) {
		this.code = _code;
	} // end setCode

	/**
	 * Initialise le libelle de l'enregistrement
	 * 
	 * @param _libelle
	 *            le libelle
	 */
	public void setLibelle(String _libelle) {
		this.libelle = _libelle;
	} // end setLibelle

	/**
	 * @return the statut (�ventuel statut gel� du r�f�rentiel)
	 */
	public String getStatut() {
		return statut;
	}

	/**
	 * @param �ventuel
	 *            statut gel� du r�f�rentiel
	 */
	public void setStatut(String statut) {
		this.statut = statut;
	}

	/**
	 * Retourne les principaux attributs sous forme textuelle
	 * 
	 * @return le libell�
	 */
	public String toString() {

		String ret = this.libelle;

		// Limite la taille de la chaine de retour pour l'affichage
		// try { ret = ret.substring(0,30) ; } catch (Exception e) {} ;

		return ret;
	}

} // end Ref
