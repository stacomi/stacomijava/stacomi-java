/*
 **********************************************************************
 *
 * Nom fichier :        Verification.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * 2015/18/01 Changement de la m�thode Vector > Vector<String>
 *    C�dric
 *
 **********************************************************************
 */

package commun;

import java.io.File;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import analyse.PasDeTemps;
import infrastructure.DC;
import infrastructure.Ouvrage;
import infrastructure.Station;
import infrastructure.StationMesure;
import infrastructure.referenciel.RefTypeFonctionnement;
import migration.Lot;
import migration.MethodeObtention;
import migration.Operation;
import migration.referenciel.RefTypeQuantite;
import migration.referenciel.RefValeurParametre;
import systeme.MasqueOpe;

/**
 * Classe statique permettant de verifier le formatage des attributs
 */
public final class Verification {

	/**
	 * Teste qu'une chaine comporte au moins un caractere
	 * 
	 * @param _chaine
	 *            la chaine a verifier
	 * @param _obligatoire
	 *            true si la chaine doit etre definie, false si elle peut etre
	 *            nulle
	 * @return True si la chaine est correcte, False sinon
	 */
	public static boolean isText(String _chaine, boolean _obligatoire) {
		boolean ret = true;

		ret = Verification.isText(_chaine, 1, _obligatoire);

		return ret;
	}

	/**
	 * Teste qu'une chaine est d'une taille minimale
	 * 
	 * @param _chaine
	 *            la chaine a verifier
	 * @param _tailleMin
	 *            la taille minimale que la chaine doit avoir
	 * @param _obligatoire
	 *            true si la chaine doit etre definie, false si elle peut etre
	 *            nulle
	 * @return True si la chaine est correcte, False sinon
	 */
	public static boolean isText(String _chaine, int _tailleMin, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_chaine == null)) {
			ret = false;
		} else if ((_chaine != null) && (_chaine.length() < _tailleMin) && (_chaine.length() > 0)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'une chaine correspond � des valeurs donn�es
	 * 
	 * @param _chaine
	 *            la chaine a verifier
	 * @param _vector
	 *            le vecteur contenant les valeurs possibles
	 * @param _obligatoire
	 *            true si la chaine doit etre definie, false si elle peut etre
	 *            nulle
	 * @return false si un probleme est survenu, true sinon
	 * @author C�dric
	 */
	public static boolean isPossibleText(String _chaine, Vector _vector, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_chaine == null)) {
			ret = false;
		} else if ((_chaine != null) && !_vector.contains(_chaine)) {
			ret = false;
		}
		return ret;
	}

	/**
	 * Teste qu'une chaine est d'une taille minimale et maximale
	 * 
	 * @param _chaine
	 *            la chaine a verifier
	 * @param _tailleMin
	 *            la taille minimale que la chaine doit avoir
	 * @param _tailleMax
	 *            la taille maximale que la chaine doit avoir
	 * @param _obligatoire
	 *            true si la chaine doit etre definie, false si elle peut etre
	 *            nulle
	 * @return True si la chaine est correcte, False sinon
	 */
	public static boolean isText(String _chaine, int _tailleMin, int _tailleMax, boolean _obligatoire) {
		boolean ret = true;

		if (!(Verification.isText(_chaine, _tailleMin, _obligatoire))
				|| ((_chaine != null) && (_chaine.length() > _tailleMax))) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'une chaine est un Integer
	 * 
	 * @param _chaine
	 *            la chaine a verifier
	 * @param _obligatoire
	 *            true si la chaine doit etre definie, false si elle peut etre
	 *            nulle
	 * @return True si la chaine est correcte, False sinon
	 */
	public static boolean isInteger(String _chaine, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_chaine == null)) {
			ret = false;
		} else if (_chaine != null) {
			try {
				Integer.valueOf(_chaine);
			} catch (Exception e) {
				ret = false;
			}
		}

		return ret;
	}

	/**
	 * Teste qu'une chaine est un Integer superieur a une certaine valeur
	 * 
	 * @param _chaine
	 *            la chaine a verifier
	 * @param _valMin
	 *            la valeur minimale de l'Integer
	 * @param _obligatoire
	 *            true si la chaine doit etre definie, false si elle peut etre
	 *            nulle
	 * @return True si la chaine est correcte, False sinon
	 */
	public static boolean isInteger(String _chaine, int _valMin, boolean _obligatoire) {
		boolean ret = true;

		if (!(Verification.isInteger(_chaine, _obligatoire))
				|| ((_chaine != null) && ((Integer.parseInt(_chaine) < _valMin)))) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'une chaine est un Integer compris entre deux valeurs
	 * 
	 * @param _chaine
	 *            la chaine a verifier
	 * @param _valMin
	 *            la valeur minimale de l'Integer
	 * @param _valMax
	 *            la valeur maximale de l'Integer
	 * @param _obligatoire
	 *            true si la chaine doit etre definie, false si elle peut etre
	 *            nulle
	 * @return True si la chaine est correcte, False sinon
	 */
	public static boolean isInteger(String _chaine, int _valMin, int _valMax, boolean _obligatoire) {
		boolean ret = true;

		if (!(Verification.isInteger(_chaine, _valMin, _obligatoire))
				|| ((_chaine != null) && (Integer.parseInt(_chaine) > _valMax))) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste un Integer
	 * 
	 * @param _integer
	 *            l'objet a verifier
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isInteger(Integer _integer, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_integer == null)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'un Integer est sup�rieur a une certaine valeur
	 * 
	 * @param _integer
	 *            l'objet a verifier
	 * @param _valMin
	 *            la valeur minimale de l'objet
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isInteger(Integer _integer, int _valMin, boolean _obligatoire) {
		boolean ret = true;

		if (!(Verification.isInteger(_integer, _obligatoire))
				|| ((_integer != null) && (_integer.intValue() < _valMin))) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'un Integer compris entre deux valeurs
	 * 
	 * @param _integer
	 *            l'objet a verifier
	 * @param _valMin
	 *            la valeur minimale de l'objet
	 * @param _valMax
	 *            la valeur maximale de l'objet
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isInteger(Integer _integer, int _valMin, int _valMax, boolean _obligatoire) {
		boolean ret = true;

		if (!(Verification.isInteger(_integer, _valMin, _obligatoire))
				|| ((_integer != null) && (_integer.intValue() > _valMax))) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'une chaine est un Short
	 * 
	 * @param _chaine
	 *            la chaine a verifier
	 * @param _obligatoire
	 *            true si la chaine doit etre definie, false si elle peut etre
	 *            nulle
	 * @return True si la chaine est correcte, False sinon
	 */
	public static boolean isShort(String _chaine, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_chaine == null)) {
			ret = false;
		} else if (_chaine != null) {
			try {
				Short.valueOf(_chaine);
			} catch (Exception e) {
				ret = false;
			}
		}

		return ret;
	}

	/**
	 * Teste qu'une chaine est un Short sup�rieur a une certaine valeur
	 * 
	 * @param _chaine
	 *            la chaine a verifier
	 * @param _valMin
	 *            la valeur minimale du Short
	 * @param _obligatoire
	 *            true si la chaine doit etre definie, false si elle peut etre
	 *            nulle
	 * @return True si la chaine est correcte, False sinon
	 */
	public static boolean isShort(String _chaine, int _valMin, boolean _obligatoire) {
		boolean ret = true;

		if (!(Verification.isShort(_chaine, _obligatoire))
				|| ((_chaine != null) && (Short.parseShort(_chaine) < _valMin))) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'une chaine est un Short compris entre deux valeurs
	 * 
	 * @param _chaine
	 *            la chaine a verifier
	 * @param _valMin
	 *            la valeur minimale du Short
	 * @param _valMax
	 *            la valeur maximale du Short
	 * @param _obligatoire
	 *            true si la chaine doit etre definie, false si elle peut etre
	 *            nulle
	 * @return True si la chaine est correcte, False sinon
	 */
	public static boolean isShort(String _chaine, int _valMin, int _valMax, boolean _obligatoire) {
		boolean ret = true;

		if (!(Verification.isShort(_chaine, _valMin, _obligatoire))
				|| ((_chaine != null) && (Short.parseShort(_chaine) > _valMax))) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste un Short
	 * 
	 * @param _short
	 *            l'objet a verifier
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isShort(Short _short, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_short == null)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'un Short est sup�rieur a une certaine valeur
	 * 
	 * @param _short
	 *            l'objet a verifier
	 * @param _valMin
	 *            la valeur minimale de l'objet
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isShort(Short _short, int _valMin, boolean _obligatoire) {
		boolean ret = true;

		if (!(Verification.isShort(_short, _obligatoire)) || ((_short != null) && (_short.intValue() < _valMin))) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'un Short compris entre deux valeurs
	 * 
	 * @param _short
	 *            l'objet a verifier
	 * @param _valMin
	 *            la valeur minimale de l'objet
	 * @param _valMax
	 *            la valeur maximale de l'objet
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isShort(Short _short, int _valMin, int _valMax, boolean _obligatoire) {
		boolean ret = true;

		if (!(Verification.isShort(_short, _valMin, _obligatoire))
				|| ((_short != null) && (_short.intValue() > _valMax))) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste un Float
	 * 
	 * @param _float
	 *            l'objet a verifier
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isFloat(Float _float, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_float == null)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'un Float est sup�rieur a une certaine valeur
	 * 
	 * @param _float
	 *            l'objet a verifier
	 * @param _valMin
	 *            la valeur minimale de l'objet
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isFloat(Float _float, float _valMin, boolean _obligatoire) {
		boolean ret = true;

		if (!(Verification.isFloat(_float, _obligatoire)) || ((_float != null) && (_float.floatValue() < _valMin))) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'un Float compris entre deux valeurs
	 * 
	 * @param _float
	 *            l'objet a verifier
	 * @param _valMin
	 *            la valeur minimale de l'objet
	 * @param _valMax
	 *            la valeur maximale de l'objet
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isFloat(Float _float, float _valMin, int _valMax, boolean _obligatoire) {
		boolean ret = true;

		if (!(Verification.isFloat(_float, _valMin, _obligatoire))
				|| ((_float != null) && (_float.floatValue() > _valMax))) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'une date est inferieure ou egale a une autre
	 * 
	 * @param _date1
	 *            la premiere date
	 * @param _date2
	 *            la deuxieme date
	 * @param _obligatoireD1
	 *            true si la date1 doit etre definie, false si elle peut etre
	 *            nulle
	 * @param _obligatoireD2
	 *            true si la date2 doit etre definie, false si elle peut etre
	 *            nulle
	 * @return True si l'objet est correct, False sinon
	 * 
	 */
	public static boolean isDateInf(Date _date1, Date _date2, boolean _obligatoireD1, boolean _obligatoireD2) {
		boolean ret = true;

		if (((_obligatoireD1 == true) && (_date1 == null)) || ((_obligatoireD2 == true) && (_date2 == null))) {
			ret = false;
		} else if ((_date1 != null) && (_date2 != null)) {
			if (_date2.getTime() < _date1.getTime()) { // si d2 strictement < a
														// d1
				// if (_date2.before(_date1)) { // si d2 strictement < a d1
				ret = false;
			}
		}

		return ret;
	}

	/**
	 * Teste qu'une date est inferieure ou egale a une autre
	 * 
	 * @param _date1
	 *            la premiere date
	 * @param _date2
	 *            la deuxieme date
	 * @param _obligatoireD1
	 *            true si la date1 doit etre definie, false si elle peut etre
	 *            nulle
	 * @param _obligatoireD2
	 *            true si la date2 doit etre definie, false si elle peut etre
	 *            nulle
	 * @return True si l'objet est correct, False sinon
	 * 
	 */
	public static boolean isDateInf(Calendar _date1, Calendar _date2, boolean _obligatoireD1, boolean _obligatoireD2) {
		boolean ret = true;

		if (((_obligatoireD1 == true) && (_date1 == null)) || ((_obligatoireD2 == true) && (_date2 == null))) {
			ret = false;
		} else if ((_date1 != null) && (_date2 != null)) {
			if (_date2.getTimeInMillis() < _date1.getTimeInMillis()) { // si d2
																		// strictement
																		// < a
																		// d1
				// if (_date2.before(_date1)) { // si d2 strictement < a d1
				ret = false;
			}
		}

		return ret;
	}

	/**
	 * Teste qu'un vecteur est de la bonne longueur
	 * 
	 * @param _object[]
	 *            le vecteur � tester
	 * @param length
	 *            la longueur de l'objet
	 * @param _obligatoire
	 *            true si le vecteur peut �tre null
	 * @return True si l'objet est correct, False sinon
	 * 
	 */
	public static boolean isVectorLength(Object[] _object, int length, Boolean _obligatoire) {
		boolean ret = true;
		// teste si l'objet peut �tre null, autoris� si _obligatoire == false
		if ((_obligatoire == true) && (_object == null)) {
			ret = false;
		} else if (_object.length != length) {
			ret = false;
		}
		return ret;
	}

	/**
	 * Teste qu'une date est bien d�finie
	 * 
	 * @param _date
	 *            la date
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 * @deprecated Les dates doivent �tre remplac�es par des calendar dans le
	 *             prog
	 */
	/*
	 * public static boolean isDate(Date _date, boolean _obligatoire) { boolean
	 * ret = true ;
	 * 
	 * if ( (_obligatoire == true) && (_date == null) ) { ret = false ; }
	 * 
	 * return ret ; }
	 */

	/**
	 * Teste qu'un dc est bien d�fini
	 * 
	 * @param _DC
	 *            le DC
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isDC(DC _DC, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_DC == null)) {
			ret = false;
		} else if ((_obligatoire == true) && (_DC.getIdentifiant() == null)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'une operation est bien d�finie
	 * 
	 * @param _operation
	 *            l'operation
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isOperation(Operation _operation, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_operation == null)) {
			ret = false;
		} else if ((_obligatoire == true) && (_operation.getIdentifiant() == null)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'un lot est bien d�fini
	 * 
	 * @param _lot
	 *            le lot
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isLot(Lot _lot, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_lot == null)) {
			ret = false;
		} else if ((_obligatoire == true) && (_lot.getIdentifiant() == null)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'une reference est bien d�finie
	 * 
	 * @param _ref
	 *            la reference
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isRef(Ref _ref, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_ref == null)) {
			ret = false;
		} else if ((_obligatoire == true) && (_ref.getCode() == null)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'un tableau de references est bien d�fini
	 * 
	 * @param _refs
	 *            les references
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isRefs(Ref[] _refs, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && ((_refs == null) || ((_refs != null) && (_refs.length == 0)))) {
			ret = false;
		} else if (_obligatoire == true) {

			for (int i = 0; i < _refs.length; i++) {
				if ((_refs[i] == null) || (_refs[i].getCode() == null)) {
					ret = false;
					break;
				}
			}
		}

		return ret;
	}

	/**
	 * Teste qu'un effectif ou qu'une quantite est bien d�fini
	 * 
	 * @param _effectif
	 *            l'effectif (nombre d'individus)
	 * @param _valeurQuantite
	 *            la valeur d'une quantite (un poids par ex)
	 * @param _typeQuantite
	 *            le type de la quantite
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isRefTypeQuantite(Float _effectif, Float _valeurQuantite, RefTypeQuantite _typeQuantite,
			boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_effectif == null) && (_valeurQuantite == null)) {
			ret = false;
		} else if ((_valeurQuantite != null) && ((_typeQuantite == null) || (_typeQuantite.getCode() == null))) {
			ret = false;
		} else if ((_effectif != null) && (_valeurQuantite != null)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'une methode d'obtention de lot est bien d�finie
	 * 
	 * @param _methode
	 *            la methode
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isMethodeObtentionLot(String _methode, boolean _obligatoire) {
		boolean ret = true;
		List lesMethodes = Arrays.asList(MethodeObtention.getMethodesLot());

		if ((_obligatoire == true) && (_methode == null)) {
			ret = false;
		} else if (lesMethodes.contains(_methode) == false) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'une methode d'obtention de caracteristique est bien d�finie
	 * 
	 * @param _methode
	 *            la methode
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isMethodeObtentionCaracteristique(String _methode, boolean _obligatoire) {
		boolean ret = true;
		List lesMethodes = Arrays.asList(MethodeObtention.getMethodesCaracteristiques());

		if ((_obligatoire == true) && (_methode == null) && lesMethodes.contains(_methode) == false) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'une valeur qualitative ou quantitative de parametre de lot est
	 * bien d�finie
	 * 
	 * @param valeurParametreQuantitatif
	 *            la valeur quantitative, ou null si la valeur est qualitative
	 * @param valeurParametreQualitatif
	 *            la valeur qualitative, ou null si la valeur est quantitative
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isValeurParametre(Float valeurParametreQuantitatif,
			RefValeurParametre valeurParametreQualitatif, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (valeurParametreQuantitatif == null) && (valeurParametreQualitatif == null)) {
			ret = false;
		} else if ((valeurParametreQuantitatif != null) && (valeurParametreQualitatif != null)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'une valeur qualitative ou quantitative de parametre de lot est
	 * bien d�finie
	 * 
	 * @param fichier
	 *            le fichier
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isFile(File fichier, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (fichier == null)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'une station est bien d�finie
	 * 
	 * @param _station
	 *            la station
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isStation(Station _station, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_station == null)) {
			ret = false;
		} else if ((_obligatoire == true) && (_station.getCode() == null)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'un ouvrage est bien d�fini
	 * 
	 * @param _ouvrage
	 *            l'ouvrage
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isOuvrage(Ouvrage _ouvrage, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_ouvrage == null)) {
			ret = false;
		} else if ((_obligatoire == true) && (_ouvrage.getCode() == null)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'un pas de temps est bien d�fini
	 * 
	 * @param _pasDeTemps
	 *            le pas de temps
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isPasDeTemps(PasDeTemps _pasDeTemps, boolean _obligatoire) {
		boolean ret = true;

		if (_obligatoire && (_pasDeTemps == null))
			ret = false;
		else if (_obligatoire && ((_pasDeTemps.getDateDebut() == null) || (_pasDeTemps.getDureePas() == 0)
				|| (_pasDeTemps.getNbPas() == 0)))
			ret = false;

		return ret;
	}

	/**
	 * Teste qu'un type de fonctionnement est bien d�fini
	 * 
	 * @param rtf
	 *            le type de fonctionnement
	 * @param _obligatoire
	 *            true si l'objet doit etre d�fini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isRefTypeFonctionnement(RefTypeFonctionnement rtf, boolean _obligatoire) {
		if (_obligatoire && rtf.getCode() == null)
			return false;
		else if (_obligatoire && rtf.getLibelle() == null)
			return false;

		return true;
	}

	/**
	 * Teste qu'une action de marquage est bien "POSE", "LECTURE" ou "RETRAIT"
	 * 
	 * @param act
	 *            l'action
	 * @param _obligatoire
	 *            _obligatoire true si l'objet doit etre d�fini, false s'il peut
	 *            etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isActionMarquage(String act, boolean _obligatoire) {
		if (_obligatoire && !(act.toLowerCase().equals("lecture") || act.toLowerCase().equals("pose")
				|| act.toLowerCase().equals("retrait")))
			return false;
		return true;
	}

	/**
	 * Teste qu'une station de mesure est bien d�finie
	 * 
	 * @param _stationmesure
	 *            la station de mesure
	 * @param _obligatoire
	 *            true si l'objet doit etre defini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isStationMesure(StationMesure _stationmesure, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_stationmesure == null)) {
			ret = false;
		} else if ((_obligatoire == true) && (_stationmesure.getIdentifiant() == null)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * Teste qu'un masqueope est bien d�fini
	 * 
	 * @param _masqueope
	 *            le masque operation
	 * @param _obligatoire
	 *            true si l'objet doit etre defini, false s'il peut etre null
	 * @return True si l'objet est correct, False sinon
	 */
	public static boolean isMasqueOpe(MasqueOpe _masqueope, boolean _obligatoire) {
		boolean ret = true;

		if ((_obligatoire == true) && (_masqueope == null)) {
			ret = false;
		} else if ((_obligatoire == true) && (_masqueope.getMasque().getCode() == null)) {
			ret = false;
		}

		return ret;
	}

}
