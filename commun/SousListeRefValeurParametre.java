/*
 **********************************************************************
 *
 * Nom fichier :        SousListeRefValeurParametre.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 *
 */
package commun;


import java.sql.* ;

import migration.referenciel.RefParametre;
import migration.referenciel.RefParametreQualitatif;
import migration.referenciel.RefValeurParametre;

/**
 * Sous-liste pour valeurs possibles d'un parametre
 */
public class SousListeRefValeurParametre extends SousListe {

 

   ///////////////////////////////////////
   // attributes
   ///////////////////////////////////////
    
    
    
  ///////////////////////////////////////
  // constructors
  ///////////////////////////////////////  
    
/**
	 * 
	 */
	private static final long serialVersionUID = -5475470262115445380L;


/**
 * Construit une sousliste sans objet de rattachement
 */    
    public SousListeRefValeurParametre() {
        this(null) ;
    } // end sousListeCaracteristiques       
    
    
/**
 * Construit une sousliste avec un objet de rattachement
 * @param _objetDeRattachement l'objet de rattachement
 */    
    public SousListeRefValeurParametre(RefParametre _objetDeRattachement) {
        super(_objetDeRattachement) ;
    } // end sousListeCaracteristiques       
    
    
    
  ///////////////////////////////////////
  // operation heritees
  ///////////////////////////////////////
    
    
    public void chargeSansFiltre() {
        
        // A FAIRE
        
    }
    
    public void chargeSansFiltreDetails() {
    
        
        // A FAIRE
    
        
    }
    
    protected void chargeObjet(Object _objet) {
        
        // A FAIRE
        
    }
    
    
    /**
     * Retourne l'identifiant du parametre qualitatif
     * @return Tableau de taille 1 : (0) code
     */
    public String[] getObjetDeRattachementID() {
        
        // L'identifiant de RefParametreQualitatif est : code
        String ret[] = new String[1] ;
        ret[0] = ((RefParametreQualitatif)super.objetDeRattachement).getCode() ;
        
        return ret ;
    }    
    
    
  ///////////////////////////////////////
  // operations
  ///////////////////////////////////////

/**
 * Charge tous les attributs des valeurs d'un parametre qualitatif
 * @throws Exception 
 */
    public void chargeFiltreDetails() throws Exception {        
        
        ResultSet   rs                          = null ;
        
        
        // Si l''objet de rattachement n'est pas definit, erreur
        if ((super.objetDeRattachement == null) || (((RefParametre)super.objetDeRattachement).getCode() == null)) {
            throw new NullPointerException(Erreur.I1000) ;
        }
 
        // Extraction des enregistrements concernes
        String sql = "SELECT val_identifiant, val_rang, val_libelle FROM ref.tr_valeurparametrequalitatif_val WHERE val_qal_code = '" + ((RefParametre)super.objetDeRattachement).getCode() + "' ORDER BY val_rang ;" ;
        //System.out.println(sql) ;
        
        rs = ConnexionBD.getInstance().getStatement().executeQuery(sql) ;

        
        // Boucle sur le ResultSet pour lire chaque enregistrement de celui-ci
        while (rs.next()) {
            
            // Lecture des champs dans le ResultSet  ;
            Integer   identifiant  = new Integer(rs.getInt("val_identifiant")) ; 
            Short     rang         = new Short(rs.getShort("val_rang")) ;
            String    libelle      = rs.getString("val_libelle") ; 
            

            // Creation de l'objet d'apres les champs lus et ajout a la liste
            RefValeurParametre val = new RefValeurParametre(identifiant, libelle, (RefParametreQualitatif)super.objetDeRattachement, rang) ;

            //this.put(((RefParametreQualitatif)super.objetDeRattachement).getCode(), val) ;
            this.put(identifiant, val) ;
            //System.out.println(libelle) ;
        }
        
    } // end chargeFiltreDetails        
    
  
    
 } // end SousListeRefValeurParametre



