/*
 **********************************************************************
 * 
 * Nom fichier :        SequenceUpdater.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * @author             	S�bastien Laigre
 * Contact :            cedric.briand@lavilaine.com
 * Date de creation :   12 juin 2009
 * Compatibilite :      Java 6/Windows XP/PostgreSQL 8.3
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * @version
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package commun;

import systeme.Sequence;
import systeme.referenciel.ListeSequence;

/**
 * Objet permettant la mise � jour des s�quences. 
 * La valeur max de chaque table est recherch�e, 
 * et elle est mise a jour � l'aide d'un "ALTER sequence RESTART WITH x"
 * @author sebastien.laigre
 */
public class SequenceUpdater 
{
	/**
	 * Initialise le SequenceUpdater
	 * Met a jour les sequences de la table
	 * @param user l'utilisateur qui se connecte et mets � jour ses s�quences
	 */
	public SequenceUpdater(String user)
	{
		this.build(user);
	}
	
	/**
	 * Parcours de toutes les lignes de la table ts_sequence_seq pour mettre 
	 * a jour chacunes des s�quences
	 * @param user l'utilisateur qui se connecte et mets � jour ses s�quences
	 */
	private void build(String user)
	{
		ListeSequence ls = new ListeSequence();
		
		try
		{
			// chargement de toutes les s�quences
			ls.chargeSansFiltreDetails();
	
			// parcours 1 par 1 pour les mettre a jour
			for(int i=0 ; i<ls.size() ; i++)
			{
				// la sequence
				Sequence seq = ((Sequence)ls.get(i));
				
				// mise a jour
				ConnexionBD.getInstance().updateSequence(seq.getSequence(), seq.getTable(), seq.getColonne(), user);
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
}
