package commun;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author Samuel Gaudey
 *
 */
public class Messages {
	private static final String BUNDLE_NAME = "commun.messages"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private Messages() {
	}

	/**
	 * @param key la cl�
	 * @return la chaine de caract�res
	 */
	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
