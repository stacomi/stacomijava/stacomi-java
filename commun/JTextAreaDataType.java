/*
 **********************************************************************
 *
 * Nom fichier :        JTextAreaDataType.java
 * Projet :             Controle migrateurs 2004
 * Organisme :          IAV/CSP
 * Auteur :             Samuel GAUDEY
 * Contact :            s.gaudey@free.fr | cedric.briand@lavilaine.com
 * Date de creation :   17 mai 2004
 * Compatibilite :      J2SDK 1.4.2/Linux Fedora Core 1/PostgreSQL 7.4
 * Etat :               a tester et a valider
 *  
 ********************************************************************** 
 *
 * Modifications :
 * ---------------
 * JJ-MM-AAAA #No Prenom NOM [INITIALES] :
 *    explication de la modification
 *
 **********************************************************************
 */

package commun;

import javax.swing.JTextArea ;
//import java.util.zip.DataFormatException ;
//import java.util.Date ;
//import java.text.SimpleDateFormat ;
//import java.text.ParsePosition ;


/**
 * 
 */
public class JTextAreaDataType extends JTextArea {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7397693132286357873L;

	/**
     * Retourne le contenu du champ sous forme de String en supprimant les espaces avant et apres
     * @return le String null si le champ est vide
	 * @throws Exception 
     */
    public String getString() throws Exception {
        String ret = null ;
        
        // Si le champ n'est pas vide, essaie de le parser
        if (super.getText().trim().length() > 0) {
                ret = super.getText().trim() ; 
        }
        
        return ret ;
    }


}
